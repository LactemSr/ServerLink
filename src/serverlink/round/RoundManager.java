package serverlink.round;

import serverlink.network.nstatus;

import java.util.HashSet;
import java.util.Set;

public class RoundManager {
    private static Set<Round> listeners = new HashSet<>();

    public static void registerListener(Round listener) {
        if (!listeners.contains(listener)) listeners.add(listener);
    }

    public static void startLobbyCountdown() {
        nstatus.setLobbyCountdown();
        listeners.forEach(Round::onLobbyCountdownStart);
    }

    public static void cancelLobbyCountdown() {
        nstatus.setLobby();
        listeners.forEach(Round::onLobbyCountdownCancel);
    }

    public static void startMapCountdown() {
        nstatus.setMapCountdown();
        listeners.forEach(Round::onMapCountdownStart);
    }

    public static void cancelMapCountdown() {
        nstatus.setLobby();
        listeners.forEach(Round::onMapCountdownCancel);
    }

    public static void startGame() {
        nstatus.setGame();
        listeners.forEach(Round::onGameStart);
    }

    public static void startEndCountdown() {
        nstatus.setEndCountdown();
        listeners.forEach(Round::onEndCountdownStart);
    }

    public static void endRound() {
        nstatus.setLobby();
        listeners.forEach(Round::onRoundEnd);
    }
}

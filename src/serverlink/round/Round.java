package serverlink.round;

public interface Round {

    void onLobbyCountdownStart();

    void onLobbyCountdownCancel();

    void onMapCountdownStart();

    void onMapCountdownCancel();

    void onGameStart();

    void onEndCountdownStart();

    void onRoundEnd();
}

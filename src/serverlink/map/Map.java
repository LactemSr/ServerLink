package serverlink.map;

import org.bukkit.Bukkit;
import org.bukkit.World;
import serverlink.network.ncount;

public class Map {
    World world = Bukkit.getWorlds().get(0);
    private String name = "loading...";
    private int border = 50;
    private int startPlayers = 0;
    private int maxPlayers = 0;

    public Map(World world) {
        this.world = world;
        if (world == null) this.world = Bukkit.getServer().getWorlds().get(0);
        this.border = 50;
        this.startPlayers = 0;
        this.maxPlayers = ncount.getMaxPlayers();
    }

    public Map(World world, String name, int border, int startPlayers, int maxPlayers) {
        this.world = world;
        if (world == null) this.world = Bukkit.getServer().getWorlds().get(0);
        this.name = name;
        this.border = border;
        this.startPlayers = startPlayers;
        this.maxPlayers = maxPlayers;
    }

    public Map(World world, int border) {
        this.world = world;
        if (world == null) this.world = Bukkit.getServer().getWorlds().get(0);
        this.border = border;
    }

    public int getBorder() {
        return border;
    }

    public void setBorder(int border) {
        this.border = border;
    }

    public int getStartPlayers() {
        return startPlayers;
    }

    public void setStartPlayers(int startPlayers) {
        this.startPlayers = startPlayers;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public World getWorld() {
        world = Bukkit.getWorld(world.getName());
        return world;
    }

    public void setWorld(World world) {
        this.world = world;
    }
}

package serverlink.map;

public class Border {
    private static Integer border = 200;

    public static int getBorder() {
        return border;
    }

    public static void setBorder(int radius) {
        border = radius;
    }
}

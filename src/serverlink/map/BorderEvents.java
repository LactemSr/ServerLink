package serverlink.map;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.server.Messages;

public class BorderEvents implements Listener {

    @EventHandler
    void onMove(PlayerMoveEvent e) {
        if (e.isCancelled()) return;
        if (!PlayerManager.get(e.getPlayer()).isFullyLoggedIn()) return;
        // horizontal border
        int distX = Math.abs(e.getPlayer().getLocation().getBlockX() - e.getTo().getWorld().getSpawnLocation().getBlockX());
        if (distX > Border.getBorder() && distX < Border.getBorder() + 30) {
            e.getPlayer().teleport(e.getTo().getWorld().getSpawnLocation().setDirection(e.getFrom().getDirection()));
            e.getPlayer().sendMessage(chat.color(chat.getServer() + Messages.border));
            return;
        }
        int distZ = Math.abs(e.getPlayer().getLocation().getBlockZ() - e.getTo().getWorld().getSpawnLocation().getBlockZ());
        if (distZ > Border.getBorder() && distZ < Border.getBorder() + 30) {
            e.getPlayer().teleport(e.getTo().getWorld().getSpawnLocation().setDirection(e.getFrom().getDirection()));
            e.getPlayer().sendMessage(chat.color(chat.getServer() + Messages.border));
        }
    }
}

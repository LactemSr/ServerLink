package serverlink.map;

import org.bukkit.*;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.CraftChunk;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.WorldUnloadEvent;
import org.jnbt.NBTInputStream;
import org.jnbt.Tag;
import serverlink.network.ncount;
import serverlink.network.nmap;
import serverlink.util.ConsoleOutput;
import serverlink.util.YamlFile;

import java.io.*;
import java.util.ArrayList;
import java.util.Set;
import java.util.SplittableRandom;
import java.util.concurrent.ThreadLocalRandom;

public class MapManager implements Listener {
    private static ArrayList<Map> gameMaps = new ArrayList<>();
    private static Map lobbyMap;
    private static Map currentGameMap = null;

    public static void setup(YamlFile yamlFile) {
        disable();
        YamlConfiguration config = (YamlConfiguration) yamlFile.getFile();
        Set<String> maps = config.getConfigurationSection("maps").getKeys(false);
        if (maps.isEmpty()) {
            ConsoleOutput.printWarning("No maps defined or maps improperly defined!");
            lobbyMap = new Map(Bukkit.getWorlds().get(0), 300);
            return;
        }
        for (String s : maps) {
            if (!config.contains("maps." + s + ".world"))
                config.set("maps." + s + ".world", Bukkit.getServer().getWorlds().get(0).getName());
            if (!config.contains("maps." + s + ".name"))
                config.set("maps." + s + ".name", "unnamedmap");
            if (!config.contains("maps." + s + ".startPlayers"))
                config.set("maps." + s + ".startPlayers", 0);
            if (!config.contains("maps." + s + ".maxPlayers"))
                config.set("maps." + s + ".maxPlayers", 20);
            if (!config.contains("maps." + s + ".border"))
                config.set("maps." + s + ".border", 200);
            if (!config.contains("maps." + s + ".lobby"))
                config.set("maps." + s + ".lobby", false);
            //check if map exists
            String worldName = config.getString("maps." + s + ".world");
            if (!worldExists(worldName)) {
                ConsoleOutput.printError("Could not load map " + s + " (world called \"" + worldName + "\") from config!");
                continue;
            }
            loadWorld(worldName);
            Map map = new Map(Bukkit.getWorld(worldName),
                    config.getString("maps." + s + ".name"), config.getInt("maps." + s + ".border"),
                    config.getInt("maps." + s + ".startPlayers"), config.getInt("maps." + s + ".maxPlayers"));
            if (config.getBoolean("maps." + s + ".lobby")
                    || config.getString("maps." + s + ".lobby").equals("true"))
                lobbyMap = map;
            else gameMaps.add(map);
        }
        yamlFile.saveFile();
        try {
            currentGameMap = gameMaps.get(ThreadLocalRandom.current().nextInt(gameMaps.size()));
            loadWorld(currentGameMap.getWorld().getName());
        } catch (Exception e) {
            ConsoleOutput.printWarning("No game maps defined in config.");
        }
        try {
            loadWorld(lobbyMap.getWorld().getName());
        } catch (Exception e) {
            ConsoleOutput.printWarning("No lobby map defined in config.");
        }
    }

    public static void disable() {
        try {
            unloadWorld(lobbyMap.getWorld());
        } catch (Exception ignored) {
        }
        try {
            for (Map map : gameMaps) unloadWorld(map.getWorld());
        } catch (Exception ignored) {
        }
    }

    public static Map getCurrentGameMap() {
        return currentGameMap;
    }

    public static ArrayList<Map> getGameMaps() {
        return gameMaps;
    }

    public static void addGameMap(Map map) {
        if (!gameMaps.contains(map)) gameMaps.add(map);
        currentGameMap = gameMaps.get(ThreadLocalRandom.current().nextInt(gameMaps.size()));
        loadWorld(currentGameMap.getWorld().getName());
    }

    public static Map getLobbyMap() {
        return lobbyMap;
    }

    public static void setLobbyMap(Map map) {
        lobbyMap = map;
    }

    public static void chooseNextMap() {
        Map previousMap = currentGameMap;
        int lastMapInList = gameMaps.size() - 1;
        if (gameMaps.indexOf(currentGameMap) == lastMapInList) currentGameMap = gameMaps.get(0);
        else currentGameMap = gameMaps.get(gameMaps.indexOf(currentGameMap) + 1);
        // random chance to skip a map (for semi-randomization)
        SplittableRandom r = new SplittableRandom();
        if (r.nextBoolean() && gameMaps.size() != 2) {
            if (gameMaps.indexOf(currentGameMap) == lastMapInList) currentGameMap = gameMaps.get(0);
            else currentGameMap = gameMaps.get(gameMaps.indexOf(currentGameMap) + 1);
        }
        if (previousMap != null) unloadWorld(previousMap.getWorld());
        loadWorld(currentGameMap.getWorld().getName());
        nmap.setMap(currentGameMap.getName());
        ncount.setStartPlayers(currentGameMap.getStartPlayers());
        ncount.setMaxPlayers(currentGameMap.getMaxPlayers());
    }

    /**
     * Gets up to 49 locations (including spawnBlock).
     * May only return spawnBlock if it can't find any safe locations (very unlikely).
     */
    public static ArrayList<Location> getSafeLocations(Location spawnBlock, int radius) {
        ArrayList<Location> list = new ArrayList<>();
        list.add(spawnBlock);
        int dist1 = ((Double) ((radius * 0.8) / 10)).intValue();
        int dist2 = ((Double) (((radius * 0.65) / 10) * 2)).intValue();
        ArrayList<Location> t = new ArrayList<>();
        SplittableRandom r = new SplittableRandom();
        if (r.nextBoolean()) {
            t.add(spawnBlock.clone().add(dist1, 0, 0));
            t.add(spawnBlock.clone().add(-dist1, 0, 0));
        } else {
            t.add(spawnBlock.clone().add(-dist1, 0, 0));
            t.add(spawnBlock.clone().add(dist1, 0, 0));
        }
        if (r.nextBoolean()) {
            t.add(spawnBlock.clone().add(0, 0, dist1));
            t.add(spawnBlock.clone().add(0, 0, -dist1));
        } else {
            t.add(spawnBlock.clone().add(0, 0, -dist1));
            t.add(spawnBlock.clone().add(0, 0, dist1));
        }
        if (r.nextBoolean()) {
            t.add(spawnBlock.clone().add(dist2, 0, 0));
            t.add(spawnBlock.clone().add(-dist2, 0, 0));
        } else {
            t.add(spawnBlock.clone().add(-dist2, 0, 0));
            t.add(spawnBlock.clone().add(dist2, 0, 0));
        }
        if (r.nextBoolean()) {
            t.add(spawnBlock.clone().add(0, 0, dist2));
            t.add(spawnBlock.clone().add(0, 0, -dist2));
        } else {
            t.add(spawnBlock.clone().add(0, 0, -dist2));
            t.add(spawnBlock.clone().add(0, 0, dist2));
        }
        // if we don't use this list a concurrentmodificationexception will be thrown
        // because we add the location to the list while iterating over it
        ArrayList<Location> t2 = new ArrayList<>();
        for (Location loc : t) {
            t2.add(loc);
            t2.add(loc.clone().add(3, 0, 0));
            t2.add(loc.clone().add(-3, 0, 0));
            t2.add(loc.clone().add(0, 0, 3));
            t2.add(loc.clone().add(0, 0, -3));
        }
        for (Location loc : t2) {
            Location block = block(loc);
            if (block != null) list.add(block);
        }
        return list;
    }

    public static boolean worldExists(String worldName) {
        for (File file : Bukkit.getWorldContainer().listFiles()) {
            if (file.isDirectory()) {
                for (File file2 : file.listFiles()) {
                    if (file2.getName().equals("level.dat")) {
                        InputStream iS;
                        try {
                            iS = new FileInputStream(file2.getAbsolutePath());
                            try {
                                NBTInputStream i = new NBTInputStream(iS);
                                Tag tag = i.readTag();
                                i.close();
                                iS.close();
                                if (tag.toString().contains("TAG_String(\"LevelName\"): " + worldName)) return true;
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        } catch (FileNotFoundException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }
        return false;
    }

    public static void loadWorld(String worldName) {
        if (worldName == null || worldName.equals("world")) return;
        if (worldName.equals("world")) return;
        Bukkit.getServer().unloadWorld(Bukkit.getWorld(worldName), false);
        WorldCreator wc = new WorldCreator(worldName).type(WorldType.FLAT).generatorSettings("3;minecraft:air;2").generateStructures(false);
        World world = Bukkit.getServer().createWorld(wc);
        for (Map map : gameMaps) {
            if (map.getName().equals(worldName)) map.world = world;
        }
        if (lobbyMap.getWorld().getName().equals(worldName)) lobbyMap.world = world;
        world.setAutoSave(false);
        world.setSpawnFlags(false, false);
        world.setStorm(false);
        world.setThunderDuration(1);
        world.setThundering(false);
        world.setWeatherDuration(1);
    }

    /**
     * @return whether or not the world was or already was unloaded
     */
    public static boolean unloadWorld(World world) {
        if (world == null || world.getName() == null || world.getName().equals("world"))
            return true;
        world = Bukkit.getWorld(world.getName());
        if (world == null || world.getName() == null || world.getName().equals("world"))
            return true;
        for (Player p : world.getPlayers()) {
            ConsoleOutput.printWarning("A player was teleported out of a world so it could be unloaded.");
            p.teleport(Bukkit.getWorlds().get(0).getSpawnLocation());
        }
        ((CraftWorld) world).getHandle().players.clear();
        for (Chunk chunk : world.getLoadedChunks()) {
            ((CraftChunk) chunk).getHandle().mustSave = false;
            chunk.unload(false);
        }
        if (!Bukkit.getServer().unloadWorld(world, false) && world.getLoadedChunks().length > 0) {
            Bukkit.getServer().unloadWorld(world, false);
            ConsoleOutput.printError("Could not unload world " + world.getName() + ". Is it the default world? Is it loaded? Does it have players in it? (NMS Players: " + ((CraftWorld) world).getHandle().players.size() + ") Was it ever loaded?");
            return false;
        }
        loadWorld(world.getName());
        for (Chunk chunk : world.getLoadedChunks()) {
            ((CraftChunk) chunk).getHandle().mustSave = false;
            chunk.unload(false);
        }
        if (!Bukkit.getServer().unloadWorld(world, false) && world.getLoadedChunks().length > 0)
            return false;
        return true;
    }

    private static Location block(Location loc) {
        int i = 1;
        while (i <= 10) {
            if (loc.clone().add(0, i, 0).getBlock().getType() == Material.AIR && loc.clone().add(0, i + 1, 0).getBlock().getType() == Material.AIR) {
                if (loc.clone().add(0, i - 1, 0).getBlock().getType() == Material.AIR)
                    if (loc.clone().add(0, i - 2, 0).getBlock().getType() == Material.AIR)
                        if (loc.clone().add(0, i - 3, 0).getBlock().getType() == Material.AIR)
                            if (loc.clone().add(0, i - 4, 0).getBlock().getType() == Material.AIR) {
                                return null;
                            }
                return loc.clone().add(0, i, 0);
            }
            i++;
        }
        return null;
    }

    @EventHandler(priority = EventPriority.MONITOR)
    private void onWorldUnload(WorldUnloadEvent e) {
        if (e.isCancelled()) ConsoleOutput.printWarning("World unload event canceled for " +
                "world: " + e.getWorld().getName() + ". (A PLUGIN COULD BE INTERFERING WITH MAP LOADING/UNLOADING/RESETTING)");
    }
}

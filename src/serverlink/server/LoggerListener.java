package serverlink.server;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.core.Filter;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.Logger;
import org.apache.logging.log4j.message.Message;
import serverlink.ServerLink;
import serverlink.bot.BotManager;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.LogRecord;

public class LoggerListener implements java.util.logging.Filter {
    // Only filters messages that are sent from the main thread.
    private static final List<String> messagesToHide = new ArrayList<String>() {{
        add("Tip> ");
        add("Using epoll channel type");
        add("ran command Message of the Day");
        add("UUID of player ");
        add("Saving...");
        add("Has been detected as hacking!");
        add("Arrow Despawn Rate: 1200");
        add("Experience Merge Radius: 3.0");
        add("Zombie Aggressive Towards Villager:");
        add("Allow Zombie Pigmen to spawn from portal blocks:");
        add("Chunks to Grow per Tick:");
        add("Clear tick list:");
        add(" Growth Modifier: ");
        add("Entity Activation Range: An 32 / Mo 32 / Mi 16");
        add("Entity Tracking Range: Pl 48 / An 48 / Mo 48 / Mi 32 / Other 64");
        add("Hopper Transfer:");
        add("Mob Spawn Range: 4");
        add("Anti X-Ray: false");
        add("Engine Mode: 1");
        add("Hidden Blocks: [");
        add("Replace Blocks: [");
        add("Nerfing mobs spawned from spawners:");
        add("Random Lighting Updates:");
        add("Structure Info Saving:");
        add("Sending up to 10 chunks per packet");
        add("Max Entity Collisions: 8");
        add("Custom Map Seeds:");
        add("Max TNT Explosions: 100");
        add("Tile Max Tick Time: 50ms Entity max Tick Time: 50ms");
        add("2016/11/");
        add("2016/12/");
        add("2017/1/");
        add("2017/2/");
        add("2017/3/");
        add("2017/4/");
        add("2017/5/");
        add("2017/6/");
        add("2017/7/");
        add("2017/8/");
        add("2017/9/");
        add("2017/12/");
        add("2017/01/");
        add("2017/02/");
        add("2017/03/");
        add("2017/04/");
        add("2017/05/");
        add("2017/06/");
        add("2017/07/");
        add("2017/08/");
        add("2017/09/");
    }};

    public static void setup() {
        for (String botName : BotManager.botUsers.keySet()) {
            messagesToHide.add(botName + " failed ");
            messagesToHide.add(botName + " Has been detected as hacking!");
        }
        ServerLink.plugin.getServer().getLogger().setFilter(new LoggerListener());

        ((Logger) LogManager.getRootLogger()).addFilter(new Filter() {
            @Override
            public Result getOnMismatch() {
                return null;
            }

            @Override
            public Result getOnMatch() {
                return null;
            }

            @Override
            public Result filter(Logger logger, Level level, Marker marker, String s, Object... objects) {
                for (String hiddenMessage : messagesToHide) {
                    if (s.contains(hiddenMessage)) {
                        return Filter.Result.DENY;
                    }
                }
                return null;
            }

            @Override
            public Result filter(Logger logger, Level level, Marker marker, Object o, Throwable throwable) {
                for (String hiddenMessage : messagesToHide) {
                    if (o.toString().contains(hiddenMessage)) {
                        return Filter.Result.DENY;
                    }
                }
                return null;
            }

            @Override
            public Result filter(Logger logger, Level level, Marker marker, Message message, Throwable throwable) {
                for (String hiddenMessage : messagesToHide) {
                    if (message.toString().contains(hiddenMessage)) {
                        return Filter.Result.DENY;
                    }
                }
                return null;
            }

            @Override
            public Result filter(LogEvent event) {
                for (String hiddenMessage : messagesToHide) {
                    if (event.getMessage().toString().contains(hiddenMessage)) {
                        return Filter.Result.DENY;
                    }
                }
                return null;
            }
        });
    }

    @Override
    public boolean isLoggable(LogRecord record) {
        for (String hiddenMessage : messagesToHide) {
            if (record.getMessage().contains(hiddenMessage)) {
                return false;
            }
        }
        return true;
    }
}

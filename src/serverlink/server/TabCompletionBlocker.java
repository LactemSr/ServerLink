package serverlink.server;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.reflect.FieldAccessException;
import serverlink.ServerLink;

public class TabCompletionBlocker {
    public static void setup() {
        // Blocks tab completing all commands (not player names) for non-admins
        ServerLink.protocolManager.addPacketListener(new PacketAdapter(ServerLink.plugin, PacketType.Play.Client.TAB_COMPLETE) {
            public void onPacketReceiving(PacketEvent event) {
                if (event.getPacketType() == PacketType.Play.Client.TAB_COMPLETE) {
                    try {
                        if (event.getPlayer().hasPermission("rank.9")) {
                            return;
                        }
                        PacketContainer packet = event.getPacket();
                        String message = packet.getSpecificModifier(String.class).read(0).toLowerCase();
                        if (message.startsWith("/") && !message.contains(" ")) {
                            event.setCancelled(true);
                        }
                    } catch (FieldAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }
}

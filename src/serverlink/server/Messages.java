package serverlink.server;

import mkremins.fanciful.FancyMessage;
import org.bukkit.ChatColor;

public class Messages {
    /*
    commands, player names, shop links: &6 gold
    info: &a green
    description/unimportant notificationa: &7 gray
    parameters/extra color: &e yellow
    errors: &f white
     */
    public final static String party_help = "\n&6/party &e<player>: &7Sends or accepts a party invite.\n" +
            "&6/party leave: &7Removes you from your party.\n" +
            "&6/party disband: &7Disbands your party.\n" +
            "&6/party kick &e<player>: &7Kicks someone from your party.\n" +
            "&6/party warp: &7Teleports your party members to your server.\n" +
            "&6/party list: &7Lists all the members in your party: \n" +
            "&aTalking in party chat:&7 Type @ before your message\n" +
            "&aMax players in a party (including leader):&7 10\n" +
            "&aPro tip:&7 You can always use &6/p &ainstead of &6/party.\n ";
    public final static String party_usage_join = "&7To join someone's party type &6/party&9|&6/p &e<someone's name>.";
    public final static String party_usage_invite = "&7To invite someone to your party type &6/party&9|&6/p &e<someone's name>.";
    public final static String party_usage_create = "&7To create a party type &6/party&9|&6/p &e<someone's name>.";
    public final static String party_usage_accept = "&7To accept an invite type &6/party&9|&6/p accept &e<party leader>";
    public final static String party_usage_kick = "&7To kick a party member type &6/party&9|&6/p kick &e<member>";
    public final static String party_success_join = "&aYou joined &6%leader%'s&a party.";
    public final static String party_success_invite = "&aYou sent an invitation to &6%player%.&a Your party will be created when someone accepts your invite.";
    public final static String party_success_leave = "&6You left &6%leader%'s&a party.";
    public final static String party_success_disband = "&aYour party was disbanded.";
    public final static String party_success_create = "&aYou created a party and added &6%player%&a to it.";
    public final static String party_update_left = "&6%player%&a left the party.";
    public final static String party_update_kicked = "&6%player%&a was kicked from the party.";
    public final static String party_update_joined = "&6%player%&a joined the party.";
    public final static String party_update_disbanded = "&6%leader%'s&a party has been disbanded.";
    public final static String party_update_disbandedOnlyPlayer = "&aYour party has been disbanded because you are the only player left.";
    public final static String party_notify_warpMember = "&aWarping to your party leader's server...";
    public final static String party_notify_warpLeader = "&aYour online party members have been warped to your server.";
    public final static String party_notify_tpWithParty = "&aChanging servers with your party...";
    public final static String party_notify_inviteSent = "&6%player%&a was invited to the party.";
    public final static String party_notify_receivedInviteExpired = "&7Your invitation to %leader%'s party has expired.";
    public final static String party_notify_sentInviteExpired = "&7Your party's invitation to %player% has expired.";
    public final static String party_notify_kicked = "&aYou have been kicked from &6%leader%'s&a party.";
    public final static String party_error_joinAlreadyInParty = "&rYou must leave your current party before you can join another.";
    public final static String party_error_inviteAlreadyInParty = "&rThat player is already in your party.";
    public final static String party_error_inviteAlreadyInOtherParty = "&rThat player is already in another party.";
    public final static String party_error_notLeader = "&rYou must be the party leader to do that.";
    public final static String party_error_playerNotFound = "&rThat player could not be found.";
    public final static String party_error_maxPlayersReached = "&rYou can only invite up to 10 players.";
    public final static String party_error_inviteSelf = "&rYou can't throw a party all by your lonesome...";
    public final static String party_error_kickSelf = "&rYou can't kick yourself.";
    public final static String party_error_inviteCooldown = "&rYou can only invite the same player once every five minutes.";
    public final static String party_error_notInvited = "&rYou don't have an active invitation from that party.";
    public final static String party_error_partyFull = "&rThat party has already reached its maximum of 10 players.";
    public final static String party_error_notAcceptingInvites = "&rThat player is not accepting party invitations.";
    public final static String party_error_notInParty = "&rYou are not in a party.";
    public final static String party_error_targetPlayerNotInParty = "&rThat player is not in your party.";
    public final static String party_error_targetPlayerNotOnline = "&rThat player is offline right now.";
    public final static String friends_help = "\n&6/friends: &7Lists friends in chat. Click on them to be teleported to their server.\n" +
            "&6/friend add &e<player>: &7Sends a friend request to another player or accepts a request they sent to you.\n" +
            "&6/friend remove &e<player>: &7Removes a friend from your friends list.\n" +
            "&6/friend accept &e<player>: &7Accepts a friend request from a player who has already sent you a friend request.\n" +
            "&6/friend deny &e<player>: &7Denies a friend request from a player.\n" +
            "&6/friend requests: &7Lists friend requests pending your approval. Click on them to accept or deny them.\n" +
            "&aPro tip:&7 You can always use &6/f &7instead of &6/friend.\n ";
    public final static String friends_sentInviteExpired = "&7Your friend request to %player% has expired.";
    public final static String friends_receivedInviteExpired = "&7Your friend request from %player% has expired.";
    public final static String friends_add = "&a%player% was added to your friends list.";
    public final static String friends_deny = "&7You denied %player%'s friend request.";
    public final static String friends_inviteSent = "&aA friend request was sent to %player%.";
    public final static String friends_remove = "&a%player% was removed from your friends list.";
    public final static String friends_error_inviteAlreadySent = "&rYou already sent a friend request to that player. Type &6/friend requests&f to view your currently active requests.";
    public final static String friends_error_maxFriendsReached = "&rYou can only have up to 50 friends.";
    public final static String friends_error_alreadyFriends = "&rYou are already friends with that player.";
    public final static String friends_error_invalidPlayer = "&rThat player could not be found.";
    public final static String friends_error_cantRemoveOrDeny = "&rYou can't unfriend/deny that player because they haven't invited you.";
    public final static String friends_error_notAcceptingRequests = "&rThat player has friend requests disabled.";
    public final static String friends_error_friendSelf = "&rYou can't be your own friend (or can you?).";

    public final static String refer_help = "\n&6/refer send &e<player>: &7Sends a request for another player to be referred by you.\n" +
            "&6/refer accept &e<player>: &7Accepts a referral from another player. This can only be done once.\n" +
            "&6/refer menu: &7Opens the the Referrals Menu.\n ";
    public final static String refer_error_referSelf = "You can't refer yourself...";
    public final static String refer_error_youAlreadyReferred = "You already accepted a referral from someone else.";
    public final static String refer_error_otherAlreadyReferred = "That player has already been referred.";
    public final static String refer_error_alreadySentRequest = "You already sent a referral request to that player.";
    public final static String refer_sent = "&aYou sent a referral request to %name%.";

    public final static String npc_help = "\n&6/npc add &e<number> <mobName> <x> <y> <z> <rotation>: &7adds an npc to the file and shows him for 5 seconds\n" +
            "&6/npc remove &e<number>: &7removes an npc from the file\n" +
            "&6/npc list: &7prints all npc numbers and types\n" +
            "&6/npc show &e<all> &7or &6/npc show &e<number>: &7shows one or all npc for 5 seconds\n ";

    public final static String border = "&7You reached the map's border and were teleported back to spawn.";

    public final static String afk_notify = "&6Afk detected! &cIf you don't move " +
            "around, you will be kicked in &4%x% &cseconds.";

    public final static String forums_usage = "\n&aTo link your Minecraft account to an email you will use on the forums: &6/forums link &e<email>\n" +
            "&aTo view the status of your forums account: &6/forums status\n ";
    public final static String forums_link_error_alreadyRegistered = "&fYou already linked your Minecraft account to %email%.";
    public final static String forums_link_error_invalidEmail = "&fThe email you entered could not be validated. Please choose another.";

    public final static String levelUp = "&6You just leveled up to level &c%level%!";

    public final static String messaging_help = "&aMessaging a player: &6/message &9| &6/msg &9| &6/tell &9| &6/t &e<player> <message>\n" +
            "&aReplying to the player you last messaged: &6/reply &9| &6/r &e<message>";
    public final static String messaging_noLastReply = "&fYou have to message someone before you can use the reply feature.";
    public final static String messaging_lastMessagedOffline = "&fThe player you last messaged is no longer online.";
    public final static String messaging_playerNotOnline = "&fThat player is not online.";
    public final static String messaging_cantMessageSelf = "&fYou can't message yourself...";
    public final static String messaging_notAcceptingPms = "&rThat player has private messaging disabled in their settings.";
    public final static FancyMessage messaging_cooldown = new FancyMessage("  ------  ------").color(ChatColor.RED)
            .style(ChatColor.BOLD).then("\nYou are only allowed to chat once every three seconds. You may purchase a " +
                    "rank at ").then("shop.alphacraft.us").color(ChatColor.GOLD).style(ChatColor.BOLD)
            .tooltip("Click to visit the shop").link("http://shop.alphacraft.us")
            .then(" if you wish to bypass the timer.").then("\n  ------  ------").color(ChatColor.RED).style(ChatColor.BOLD);

    public final static String ban_temporary = "&cYou have been banned for &d%duration% &cfor &d%reason%!";

    public final static String treasure_find_mini = "\n&9You just found a &e&lMINI CHEST! &r&9You have two weeks " +
            "to open it in hub.\n";
    public final static String treasure_find_epic = "\n&bYou just found an &e&lEPIC CHEST! &r&bYou have two weeks " +
            "to open it in hub.\n";
    public final static String treasure_find_legendary = "&dYou just found a &e&lLEGENDARY CHEST! &r&dYou have two weeks " +
            "to open it in hub.\n";
    public final static String treasure_find_announce_legendary = "\n%name% &r&djust found a &e&lLEGENDARY CHEST!\n";

    public final static String powerClass_lvlUp = "&6&lYou just leveled up %class%!&r&d You can " +
            "check out its new stats from the Classes Menu.";
}

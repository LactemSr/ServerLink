package serverlink.server;

import com.google.common.util.concurrent.AtomicDouble;
import serverlink.ServerLink;
import serverlink.customevent.CustomGametypePluginLoadEvent;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.network.JedisOne;
import serverlink.network.JedisPool;
import serverlink.util.CommandText;
import mkremins.fanciful.FancyMessage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.atomic.AtomicInteger;

public class GametypeBooster implements Listener {
    private static AtomicDouble currentBooster = new AtomicDouble(1);
    private static volatile String currentBuyer = "";
    private static AtomicInteger currentSecondsToExpire = new AtomicInteger(9999999);
    private static boolean choosingNewHandler = false;
    private static int consecutiveValidations = 0;
    private static int consecutiveInvalidations = 0;
    private static boolean runnableRunning = false;
    private static BukkitRunnable runnable = new BukkitRunnable() {
        @Override
        public void run() {
            // TODO REMOVE WHEN ADDING BOOSTERS
            if (true) return;
            Jedis jedis = JedisOne.getTwo();
            String handler = jedis.get(ServerLink.getServerType() + "BoosterHandler");
            String timeString = jedis.get(ServerLink.getServerType() + "BoosterHandlerTime");
            if (handler == null || timeString == null || handler.isEmpty() || timeString.isEmpty()) {
                // handler not set (new gametype or database failure), this server will take over
                jedis.set(ServerLink.getServerType() + "BoosterHandler", ServerLink.getServerName());
                handler = ServerLink.getServerName();
                timeString = ((Long) Instant.now().getEpochSecond()).toString();
                jedis.set(ServerLink.getServerType() + "BoosterMultiplier", currentBooster.toString());
                jedis.set(ServerLink.getServerType() + "BoosterBuyer", currentBuyer);
                jedis.set(ServerLink.getServerType() + "BoosterActive", "false");
            }
            if (handler.equals(ServerLink.getServerName())) {
                jedis.set(ServerLink.getServerType() + "BoosterHandler", ServerLink.getServerName());
                jedis.set(ServerLink.getServerType() + "BoosterHandlerTime", ((Long) Instant.now().getEpochSecond()).toString());
                String boosterActive = jedis.get(ServerLink.getServerType() + "BoosterActive");
                if (boosterActive != null && boosterActive.equals("true")) {
                    // a booster is active, set local variables
                    currentBooster.set(Double.parseDouble(jedis.get(ServerLink.getServerType() + "BoosterMultiplier")));
                    currentBuyer = jedis.get(ServerLink.getServerType() + "BoosterBuyer");
                    currentSecondsToExpire.set(jedis.ttl(ServerLink.getServerType() + "BoosterSecondsToExpire").intValue());
                    if (currentSecondsToExpire.get() <= 0) {
                        // announce that booster expired
                        FancyMessage announcement = new FancyMessage(currentBuyer + "'s").color(ChatColor.GOLD)
                                .tooltip(ChatColor.GRAY + "Click to view " + currentBuyer + ".").command("/player " + currentBuyer)
                                .then(" " + getBoosterMultiplierString() + "x").color(ChatColor.BLUE)
                                .then(ChatColor.stripColor(ServerLink.getFancyServerName())
                                        .replaceAll(">", "") + " booster just expired. Anyone can activate another in ")
                                .color(ChatColor.GREEN).then("My Profile -> Gametype Boosters ")
                                .tooltip("Click to open My Profile -> Gametype Boosters")
                                .command("/gui boosters").then("after purchasing one from the shop: ")
                                .color(ChatColor.GREEN).then("shop.alphacraft.us.").color(ChatColor.GOLD)
                                .style(ChatColor.BOLD).tooltip("Click to open the shop")
                                .link("http://shop.alphacraft.us");
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                for (Player p : Bukkit.getOnlinePlayers()) {
                                    p.sendMessage("");
                                    p.sendMessage("");
                                    CommandText.sendMessage(p, announcement.toJSONString());
                                    p.sendMessage("");
                                    p.sendMessage("");
                                }
                            }
                        }.runTask(ServerLink.plugin);
                        currentBooster.set(1);
                        currentBuyer = "";
                        currentSecondsToExpire.set(9999999);
                        jedis.set(ServerLink.getServerType() + "BoosterActive", "false");
                    }
                } else {
                    // no booster is active; check the queue
                    List<String> queue = jedis.lrange(ServerLink.getServerType() + "BoosterQueue", 0, -1);
                    if (queue != null && !queue.isEmpty()) {
                        // the queue has a booster in it; set the first element as the network's active booster
                        String[] booster = queue.get(0).split(":");
                        currentBooster.set(Double.parseDouble(booster[0]));
                        currentBuyer = booster[1];
                        currentSecondsToExpire.set(Integer.parseInt(booster[2]));
                        jedis.set(ServerLink.getServerType() + "BoosterMultiplier", currentBooster.toString());
                        jedis.set(ServerLink.getServerType() + "BoosterBuyer", currentBuyer);
                        jedis.set(ServerLink.getServerType() + "BoosterSecondsToExpire", "value doesn't matter");
                        jedis.expire(ServerLink.getServerName() + "BoosterSecondsToExpire", currentSecondsToExpire.get());
                        jedis.lpop(ServerLink.getServerType() + "BoosterQueue");
                        jedis.set(ServerLink.getServerType() + "BoosterActive", "true");
                        // announce that booster was activated
                        FancyMessage announcement = new FancyMessage(currentBuyer + "'s").color(ChatColor.GOLD)
                                .tooltip(ChatColor.GRAY + "Click to view " + currentBuyer + ".").command("/player " + currentBuyer)
                                .then(" " + getBoosterMultiplierString() + "x").color(ChatColor.BLUE)
                                .then(ChatColor.stripColor(ServerLink.getFancyServerName())
                                        .replaceAll(">", "") + " booster has just been activated for ").color(ChatColor.GREEN)
                                .then(getSecondsToExpire() / 60 + " minutes.").color(ChatColor.BLUE);
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                for (Player p : Bukkit.getOnlinePlayers()) {
                                    p.sendMessage("");
                                    CommandText.sendMessage(p, announcement.toJSONString());
                                    p.sendMessage("");
                                }
                            }
                        }.runTask(ServerLink.plugin);
                    }
                }
                JedisOne.closeTwo();
                return;
            }
            // this server is not the handler
            int time = Integer.parseInt(timeString);
            if (time + 15 >= Instant.now().getEpochSecond()) {
                // we're assuming another server is still handling the queue
                currentBooster.set(Double.parseDouble(jedis.get(ServerLink.getServerType() + "BoosterMultiplier")));
                currentBuyer = jedis.get(ServerLink.getServerType() + "BoosterBuyer");
                int expire = jedis.ttl(ServerLink.getServerType() + "BoosterSecondsToExpire").intValue();
                if (expire - currentSecondsToExpire.get() > 120) {
                    currentSecondsToExpire.set(expire);
                    // announce that booster was activated
                    FancyMessage announcement = new FancyMessage(currentBuyer + "'s").color(ChatColor.GOLD)
                            .tooltip(ChatColor.GRAY + "Click to view " + currentBuyer + ".").command("/player " + currentBuyer)
                            .then(" " + getBoosterMultiplierString() + "x").color(ChatColor.BLUE)
                            .then(ChatColor.stripColor(ServerLink.getFancyServerName())
                                    .replaceAll(">", "") + " booster has just been activated for ").color(ChatColor.GREEN)
                            .then(getSecondsToExpire() / 60 + " minutes.").color(ChatColor.BLUE);
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            for (Player p : Bukkit.getOnlinePlayers()) {
                                p.sendMessage("");
                                CommandText.sendMessage(p, announcement.toJSONString());
                                p.sendMessage("");
                            }
                        }
                    }.runTask(ServerLink.plugin);
                } else if (expire <= 0 && getSecondsToExpire() > 0) {
                    currentSecondsToExpire.set(expire);
                    // announce that booster expired
                    FancyMessage announcement = new FancyMessage(currentBuyer + "'s").color(ChatColor.GOLD)
                            .tooltip(ChatColor.GRAY + "Click to view " + currentBuyer + ".").command("/player " + currentBuyer)
                            .then(" " + getBoosterMultiplierString() + "x").color(ChatColor.BLUE)
                            .then(ChatColor.stripColor(ServerLink.getFancyServerName())
                                    .replaceAll(">", "") + " booster just expired. Anyone can activate another in ")
                            .color(ChatColor.GREEN).then("My Profile -> Gametype Boosters ")
                            .tooltip("Click to open My Profile -> Gametype Boosters")
                            .command("/gui boosters").then("after purchasing one from the shop: ")
                            .color(ChatColor.GREEN).then("shop.alphacraft.us.").color(ChatColor.GOLD)
                            .style(ChatColor.BOLD).tooltip("Click to visit the shop")
                            .link("http://shop.alphacraft.us");
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            for (Player p : Bukkit.getOnlinePlayers()) {
                                p.sendMessage("");
                                p.sendMessage("");
                                CommandText.sendMessage(p, announcement.toJSONString());
                                p.sendMessage("");
                                p.sendMessage("");
                            }
                        }
                    }.runTask(ServerLink.plugin);
                } else currentSecondsToExpire.set(expire);
                JedisOne.closeTwo();
                return;
            }
            JedisOne.closeTwo();
            // the main handler server went offline; all other servers of this gametype will now run the below
            // code to establish one of them as the new queue handler
            // cancel main task and set id to null so loading a module doesn't try to re-disable it
            runnable.cancel();
            // how this works: Every server immediately sets itself as the new handler (after checking again).
            // Only one server can be the last one to tell the database it is the new handler, so every server
            // checks again every 1.25 seconds to see who the handler is. If it sees itself twice in a row, it will
            // turn into the queue handler. If it sees another server twice in a row, it will assume that server is
            // the new handler.
            consecutiveValidations = 0;
            consecutiveInvalidations = 0;
            newHandlerRunnable.runTaskTimerAsynchronously(ServerLink.plugin, 0, 25);
        }
    };
    private static BukkitRunnable newHandlerRunnable = new BukkitRunnable() {
        @Override
        public void run() {
            Jedis jedis = JedisOne.getTwo();
            String handler2 = jedis.get(ServerLink.getServerType() + "BoosterHandler");
            int time2 = Integer.parseInt(jedis.get(ServerLink.getServerType() + "BoosterHandlerTime"));
            if (time2 + 7 >= Instant.now().getEpochSecond()) {
                if (handler2.equals(ServerLink.getServerName())) consecutiveValidations++;
                else {
                    consecutiveInvalidations++;
                    consecutiveValidations = 0;
                }
                if (consecutiveValidations == 2) {
                    choosingNewHandler = false;
                    JedisOne.closeTwo();
                    cancel();
                    runnable.runTaskTimerAsynchronously(ServerLink.plugin, 5, 20).getTaskId();
                    return;
                }
                if (consecutiveInvalidations == 2) {
                    choosingNewHandler = false;
                    JedisOne.closeTwo();
                    cancel();
                    runnable.runTaskTimerAsynchronously(ServerLink.plugin, 5, 20);
                    return;
                }
            } else {
                jedis.set(ServerLink.getServerType() + "BoosterHandler", ServerLink.getServerName());
                jedis.set(ServerLink.getServerType() + "BoosterHandlerTime", ((Long) Instant.now().getEpochSecond()).toString());
                JedisOne.closeTwo();
            }
        }
    };

    private static void schedule() {
        if (choosingNewHandler) return;
        if (ServerLink.getServerType() == ServerType.hub) return;
        // All servers of the same gametype need to communicate to have one server handle the queue.
        // The other servers of the gametype keep track of when the booster is set to expire.
        runnable.runTaskTimerAsynchronously(ServerLink.plugin, 20, 20);
        runnableRunning = true;
    }

    public static double getBoosterMultiplier() {
        return currentBooster.get();
    }

    public static String getBoosterMultiplierString() {
        return new BigDecimal(currentBooster.get()).setScale(2, BigDecimal.ROUND_DOWN).toString();
    }

    public static int getSecondsToExpire() {
        return currentSecondsToExpire.get();
    }

    public static String getBuyer() {
        return currentBuyer;
    }

    /**
     * called when a user buys a booster or boosters from the shop
     */
    public static void buyBooster(String buyerName, String buyerUUID, Double mult, Integer minutesToExpire, Integer amount, String gametype) {
        new BukkitRunnable() {
            @Override
            public void run() {
                String booster = mult.toString() + ":" + buyerName + ":" + minutesToExpire.toString() + ":" + gametype;
                Jedis jedis = JedisPool.getConn();
                for (int i = 0; i < amount; i++) jedis.lpush("gametypeBoosters:" + buyerUUID, booster);
                JedisPool.close(jedis);
                UUID uuid = UUID.fromString(buyerUUID);
                for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
                    if (aPlayer.getUuid() != uuid) continue;
                    aPlayer.getData("boostersGui").setValue(null);
                    return;
                }
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public static void useBooster(AlphaPlayer aPlayer, Double mult, Integer secondsToExpire, String gametype) {
        String booster = mult.toString() + ":" + aPlayer.getPlayer().getName() + ":" + secondsToExpire.toString() + ":" + gametype;
        Jedis j2 = JedisPool.getConn();
        j2.lrem("gametypeBoosters:" + aPlayer.getUuid().toString(), 1, booster);
        JedisPool.close(j2);
        Jedis j1 = JedisOne.getTwo();
        j1.lpush(gametype + "BoosterQueue", booster);
        JedisOne.closeTwo();
        aPlayer.getData("boostersGui").setValue(null);
        aPlayer.getPlayer().closeInventory();
    }

    @EventHandler
    public void onGametypeLoad(CustomGametypePluginLoadEvent e) {
        if (e.getServertype() == ServerType.hub) {
            if (runnableRunning) {
                runnable.cancel();
                runnableRunning = false;
            }
            if (choosingNewHandler) {
                newHandlerRunnable.cancel();
                choosingNewHandler = false;
            }
        } else if (!runnableRunning) schedule();
    }
}

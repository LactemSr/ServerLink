package serverlink.server;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.event.server.ServerCommandEvent;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.command.server.ServerCommand;
import serverlink.util.ConsoleOutput;

public class ShutdownInterceptor implements Listener {
    private static boolean shuttingDown = false;

    // Disable saving when shutting down (multicraft runs "save-all" before "stop")
    @EventHandler(priority = EventPriority.HIGHEST)
    private void onSaveWorlds(ServerCommandEvent e) {
        if (!e.getCommand().toLowerCase().equals("save-all")) return;
        e.setCancelled(true);
        new BukkitRunnable() {
            @Override
            public void run() {
                if (shuttingDown) return;
                Bukkit.getWorlds().forEach(World::save);
            }
        }.runTaskLater(ServerLink.plugin, 20);
    }
    @EventHandler(priority = EventPriority.LOWEST)
    private void onStop_Server(ServerCommandEvent e) {
        if (!e.getCommand().toLowerCase().startsWith("stop")) return;
        e.setCancelled(true);
        shuttingDown = true;
        new BukkitRunnable() {
            @Override
            public void run() {
                ServerLink.startShutdown();
            }
        }.runTask(ServerLink.plugin);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onStop_Player(PlayerCommandPreprocessEvent e) {
        if (!e.getMessage().toLowerCase().startsWith("/stop")) return;
        e.setCancelled(true);
        if (!e.getPlayer().hasPermission("rank.10")) {
            e.getPlayer().sendMessage(chat.color(chat.getServer() + chat.gcu()));
        } else {
            shuttingDown = true;
            new BukkitRunnable() {
                @Override
                public void run() {
                    ServerLink.startShutdown();
                }
            }.runTask(ServerLink.plugin);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onReload_Server(ServerCommandEvent e) {
        if (!e.getCommand().toLowerCase().startsWith("reload")) return;
        e.setCancelled(true);
        ConsoleOutput.printWarning("No Reloading!");
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onReload_Player(PlayerCommandPreprocessEvent e) {
        if (!e.getMessage().toLowerCase().startsWith("/reload")) return;
        e.setCancelled(true);
        e.getPlayer().sendMessage(chat.color(chat.getServer() + chat.gcu()));
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onRestart_Server(ServerCommandEvent e) {
        if (!e.getCommand().toLowerCase().startsWith("restart")) return;
        e.setCancelled(true);
        shuttingDown = true;
        new BukkitRunnable() {
            @Override
            public void run() {
                ServerLink.startShutdown();
            }
        }.runTask(ServerLink.plugin);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onRestart_Player(PlayerCommandPreprocessEvent e) {
        if (!e.getMessage().toLowerCase().startsWith("/restart")) return;
        e.setCancelled(true);
        if (!e.getPlayer().hasPermission("rank.10")) {
            e.getPlayer().sendMessage(chat.color(chat.getServer() + chat.gcu()));
        } else {
            shuttingDown = true;
            new BukkitRunnable() {
                @Override
                public void run() {
                    ServerLink.startShutdown();
                }
            }.runTask(ServerLink.plugin);
        }
    }
}

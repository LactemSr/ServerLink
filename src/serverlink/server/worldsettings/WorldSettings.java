package serverlink.server.worldsettings;


import org.bukkit.Bukkit;
import org.bukkit.entity.Animals;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Monster;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;

import java.util.ArrayList;

public class WorldSettings {

    static boolean fireBlockDestroy = false;
    static boolean explosionBlockDestroy = false;
    static boolean sheepGraze = false;
    static boolean endermanBlockGrief = false;
    static boolean zombieDoorBreak = false;
    static boolean iceMelt = false;
    static boolean snowMelt = false;
    static boolean leafDecay = false;

    static boolean vinegrowth = false;
    static boolean sugarcaneGrowth = false;
    static boolean mushroomGrowth = false;

    static boolean grassSpread = false;
    static boolean myceliumSpread = false;
    static boolean mushroomSpread = false;
    static boolean fireSpread = false;
    static boolean iceForm = false;

    static boolean villagerInfect = false;
    static boolean creatureExpDrop = false;
    static boolean blockBreakExp = false;
    static boolean weather = false;

    static ArrayList<EntityType> enabledEntities = new ArrayList<>();
    private static ArrayList<EntityType> monsters = new ArrayList<>();
    private static ArrayList<EntityType> animals = new ArrayList<>();

    public static void setup() {
        enabledEntities.add(EntityType.PAINTING);
        enabledEntities.add(EntityType.ARMOR_STAND);
        enabledEntities.add(EntityType.ARROW);
        enabledEntities.add(EntityType.BOAT);
        enabledEntities.add(EntityType.DROPPED_ITEM);
        enabledEntities.add(EntityType.FISHING_HOOK);
        enabledEntities.add(EntityType.FIREBALL);
        enabledEntities.add(EntityType.FIREWORK);
        enabledEntities.add(EntityType.EXPERIENCE_ORB);
        enabledEntities.add(EntityType.ITEM_FRAME);
        enabledEntities.add(EntityType.LEASH_HITCH);
        enabledEntities.add(EntityType.THROWN_EXP_BOTTLE);
        enabledEntities.add(EntityType.MINECART);
        enabledEntities.add(EntityType.MINECART_CHEST);
        enabledEntities.add(EntityType.MINECART_COMMAND);
        enabledEntities.add(EntityType.MINECART_FURNACE);
        enabledEntities.add(EntityType.MINECART_HOPPER);
        enabledEntities.add(EntityType.MINECART_MOB_SPAWNER);
        enabledEntities.add(EntityType.MINECART_TNT);
        enabledEntities.add(EntityType.LIGHTNING);
        enabledEntities.add(EntityType.PRIMED_TNT);
        enabledEntities.add(EntityType.SMALL_FIREBALL);
        enabledEntities.add(EntityType.SNOWBALL);
        enabledEntities.add(EntityType.SPLASH_POTION);
        new BukkitRunnable() {
            @Override
            public void run() {
                for (EntityType type
                        : EntityType.values()) {
                    try {
                        Entity entity = Bukkit.getWorlds().get(0).spawnEntity(Bukkit.getWorlds().get(0).getSpawnLocation(), type);
                        if (entity instanceof Monster) {
                            monsters.add(type);
                        } else if (entity instanceof Animals) {
                            animals.add(type);
                        }
                        entity.remove();
                    } catch (Exception ignored) {
                    }
                }
            }
        }.runTaskLaterAsynchronously(ServerLink.plugin, 40);
    }

    public static void setVineGrowth(Boolean bool) {
        vinegrowth = bool;
    }

    public static void setSugarcaneGrowth(Boolean bool) {
        sugarcaneGrowth = bool;
    }

    public static void enableSpawnOf(EntityType entity) {
        if (!enabledEntities.contains(entity)) enabledEntities.add(entity);
    }

    public static void disableSpawnOf(EntityType entity) {
        if (enabledEntities.contains(entity)) enabledEntities.remove(entity);
    }

    public static void enableSpawnOfMobs() {
        for (EntityType entity : EntityType.values())
            if (monsters.contains(entity)) if (!enabledEntities.contains(entity)) enabledEntities.add(entity);
    }

    public static void disableSpawnOfMobs() {
        for (EntityType entity : EntityType.values())
            if (monsters.contains(entity)) if (enabledEntities.contains(entity)) enabledEntities.remove(entity);
    }

    // villagers, snowmen, and iron golems are not animals
    public static void enableSpawnOfAnimals() {
        for (EntityType entity : EntityType.values())
            if (animals.contains(entity)) if (!enabledEntities.contains(entity)) enabledEntities.add(entity);
    }

    public static void disableSpawnOfAnimals() {
        for (EntityType entity : EntityType.values())
            if (!monsters.contains(entity)) if (enabledEntities.contains(entity)) enabledEntities.remove(entity);
    }

    public static boolean isFireBlockDestroy() {
        return fireBlockDestroy;
    }

    public static void setFireBlockDestroy(Boolean bool) {
        fireBlockDestroy = bool;
    }

    public static boolean isExplosionBlockDestroy() {
        return explosionBlockDestroy;
    }

    public static void setExplosionBlockDestroy(Boolean bool) {
        explosionBlockDestroy = bool;
    }

    public static boolean isSheepGraze() {
        return sheepGraze;
    }

    public static void setSheepGraze(Boolean bool) {
        sheepGraze = bool;
    }

    public static boolean isEndermanBlockGrief() {
        return endermanBlockGrief;
    }

    public static void setEndermanBlockGrief(Boolean bool) {
        endermanBlockGrief = bool;
    }

    public static boolean isZombieDoorBreak() {
        return zombieDoorBreak;
    }

    public static void setZombieDoorBreak(Boolean bool) {
        zombieDoorBreak = bool;
    }

    public static boolean isIceMelt() {
        return iceMelt;
    }

    public static void setIceMelt(Boolean bool) {
        iceMelt = bool;
    }

    public static boolean isSnowMelt() {
        return snowMelt;
    }

    public static void setSnowMelt(Boolean bool) {
        snowMelt = bool;
    }

    public static boolean isLeafDecay() {
        return leafDecay;
    }

    public static void setLeafDecay(Boolean bool) {
        leafDecay = bool;
    }

    public static boolean isVinegrowth() {
        return vinegrowth;
    }

    public static boolean isSugarcanegrowth() {
        return sugarcaneGrowth;
    }

    public static boolean isMushroomGrowth() {
        return mushroomGrowth;
    }

    public static void setMushroomGrowth(Boolean bool) {
        mushroomGrowth = bool;
    }

    public static boolean isGrassSpread() {
        return grassSpread;
    }

    public static void setGrassSpread(Boolean bool) {
        grassSpread = bool;
    }

    public static boolean isMyceliumSpread() {
        return myceliumSpread;
    }

    public static void setMyceliumSpread(Boolean bool) {
        myceliumSpread = bool;
    }

    public static boolean isMushroomSpread() {
        return mushroomSpread;
    }

    public static void setMushroomSpread(Boolean bool) {
        mushroomSpread = bool;
    }

    public static boolean isFirespread() {
        return fireSpread;
    }

    public static void setFirespread(Boolean bool) {
        fireSpread = bool;
    }

    public static boolean isIceForm() {
        return iceForm;
    }

    public static void setIceForm(Boolean bool) {
        iceForm = bool;
    }

    public static boolean isVillagerInfect() {
        return villagerInfect;
    }

    public static void setVillagerInfect(Boolean bool) {
        villagerInfect = bool;
    }

    public static boolean isCreatureExpDrop() {
        return creatureExpDrop;
    }

    public static void setCreatureExpDrop(Boolean bool) {
        creatureExpDrop = bool;
    }

    public static boolean isBlockBreakExp() {
        return blockBreakExp;
    }

    public static void setBlockBreakExp(Boolean bool) {
        blockBreakExp = bool;
    }

    public static boolean isWeather() {
        return weather;
    }

    public static void setWeather(Boolean bool) {
        weather = bool;
    }
}

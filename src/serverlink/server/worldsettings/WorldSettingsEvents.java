package serverlink.server.worldsettings;

import org.bukkit.Material;
import org.bukkit.entity.ArmorStand;
import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.*;
import org.bukkit.event.entity.*;
import org.bukkit.event.weather.WeatherChangeEvent;
import serverlink.npc.NpcManager;
import serverlink.player.interaction.PlayerInteractionEvents;

public class WorldSettingsEvents implements Listener {

    @EventHandler(priority = EventPriority.LOWEST)
    void fireBlockDestroy(BlockBurnEvent e) {
        if (!WorldSettings.fireBlockDestroy) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void explosionBlockDestroy(BlockExplodeEvent e) {
        if (!WorldSettings.explosionBlockDestroy) e.blockList().clear();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void explosionBlockDestroy2(EntityExplodeEvent e) {
        if (!WorldSettings.explosionBlockDestroy) e.blockList().clear();
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void protectPortals(BlockPhysicsEvent e) {
        if (e.getChangedType() == Material.PORTAL || e.getBlock().getType() == Material.PORTAL) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void sheepGrazeAndEndermanGrief(EntityChangeBlockEvent e) {
        if (e.getEntityType() == EntityType.ENDER_DRAGON) e.setCancelled(true);
        else if (e.getEntityType() == EntityType.WITHER_SKULL) e.setCancelled(true);
        else if (e.getEntityType() == EntityType.WITHER) e.setCancelled(true);
        else if (e.getEntityType() == EntityType.SHEEP) e.setCancelled(!WorldSettings.sheepGraze);
        else if (e.getEntityType() == EntityType.ENDERMAN) e.setCancelled(!WorldSettings.endermanBlockGrief);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void zombieDoorBreak(EntityBreakDoorEvent e) {
        if (!WorldSettings.zombieDoorBreak) e.setCancelled(true);
    }

    // only stops melting from light sources, doesn't stop ice from turning to water when broken
    @EventHandler(priority = EventPriority.LOWEST)
    void iceMeltAndSnowMelt(BlockFadeEvent e) {
        if (e.getBlock().getType() == Material.ICE || e.getNewState().getType() == Material.ICE)
            e.setCancelled(!WorldSettings.iceMelt);
        if (e.getBlock().getType() == Material.SNOW || e.getBlock().getType() == Material.SNOW_BLOCK || e.getNewState().getType() == Material.SNOW || e.getNewState().getType() == Material.SNOW_BLOCK)
            e.setCancelled(!WorldSettings.snowMelt);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void leafDecay(LeavesDecayEvent e) {
        e.setCancelled(!WorldSettings.leafDecay);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void growth(BlockGrowEvent e) {
        if (e.getBlock().getType() == Material.SUGAR_CANE_BLOCK || e.getNewState().getType() == Material.SUGAR_CANE_BLOCK)
            e.setCancelled(!WorldSettings.sugarcaneGrowth);
        else if (e.getBlock().getType() == Material.RED_MUSHROOM || e.getBlock().getType() == Material.BROWN_MUSHROOM || e.getNewState().getType() == Material.BROWN_MUSHROOM || e.getNewState().getType() == Material.RED_MUSHROOM)
            e.setCancelled(!WorldSettings.mushroomGrowth);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void spread(BlockSpreadEvent e) {
        if (e.getBlock().getType() == Material.VINE || e.getNewState().getType() == Material.VINE)
            e.setCancelled(!WorldSettings.vinegrowth);
        else if (e.getBlock().getType() == Material.GRASS || e.getNewState().getType() == Material.GRASS)
            e.setCancelled(!WorldSettings.grassSpread);
        else if (e.getBlock().getType() == Material.MYCEL || e.getNewState().getType() == Material.MYCEL)
            e.setCancelled(!WorldSettings.myceliumSpread);
        else if (e.getBlock().getType() == Material.RED_MUSHROOM || e.getBlock().getType() == Material.BROWN_MUSHROOM || e.getNewState().getType() == Material.BROWN_MUSHROOM || e.getNewState().getType() == Material.RED_MUSHROOM)
            e.setCancelled(!WorldSettings.mushroomGrowth);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void fireSpread(BlockIgniteEvent e) {
        if (e.getCause() == BlockIgniteEvent.IgniteCause.SPREAD)
            e.setCancelled(!WorldSettings.fireSpread);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void iceForm(BlockFormEvent e) {
        e.setCancelled(!WorldSettings.iceForm);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void creatureExpDrop(EntityDeathEvent e) {
        if (!WorldSettings.creatureExpDrop) e.setDroppedExp(0);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void blockBreakExpDrop(BlockBreakEvent e) {
        if (!WorldSettings.blockBreakExp) e.setExpToDrop(0);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void weather(WeatherChangeEvent e) {
        if (e.toWeatherState() && !WorldSettings.weather) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void enabledEntitiesAndVillagerInfect(CreatureSpawnEvent e) {
        // return if its an npc's armor stand
        if (e.getEntityType() == EntityType.ARMOR_STAND) {
            ArmorStand stand = (ArmorStand) e.getEntity();
            if (!stand.isVisible()) return;
        }
        if (e.getSpawnReason() == CreatureSpawnEvent.SpawnReason.CUSTOM) return;
        if (e.getSpawnReason() == CreatureSpawnEvent.SpawnReason.CHUNK_GEN) return;
        if (e.getSpawnReason() == CreatureSpawnEvent.SpawnReason.DEFAULT) return;
        if (NpcManager.getNpcs().contains(e.getEntity().getEntityId())) return;
        //villager infection
        if (e.getSpawnReason() == CreatureSpawnEvent.SpawnReason.INFECTION) if (!WorldSettings.villagerInfect) {
            e.setCancelled(true);
            return;
        }
        if (!WorldSettings.enabledEntities.contains(e.getEntityType())) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGH)
    void protectNpcs(EntityDamageEvent e) {
        if (NpcManager.getNpcs().contains(e.getEntity().getEntityId())) e.setCancelled(true);
    }
}

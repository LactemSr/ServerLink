package serverlink.server;

import net.citizensnpcs.api.CitizensAPI;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.bot.BotBaseTrait;
import serverlink.network.*;

public class Reset {
    private static final Object lock = new Object();
    volatile public static long ttl = 0;

    //check every 5 minutes if the database tells us to sync our count
    //we also make the sync method public so that the join event can call it if it catches the sync before this task
    public static void setup() {
        new BukkitRunnable() {
            @Override
            public void run() {
                synchronized (lock) {
                    synchronized (ncount.countLock) {
                        Jedis jedis = JedisOne.getOne();
                        long currentttl = jedis.ttl("reset");
                        JedisOne.closeOne();
                        if (currentttl > ttl) {
                            int playersOnline = 0;
                            for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
                                if (aPlayer.getPlayer() == null) {
                                    PlayerManager.onlineAlphaPlayers.remove(aPlayer);
                                    PlayerManager.alphaPlayers.remove(aPlayer.getUuid());
                                    continue;
                                } else if (!aPlayer.isFullyLoggedIn()) continue;
                                if (aPlayer.getPlayer().isOnline() || (aPlayer.getPlayer().hasMetadata("NPC") && CitizensAPI.getNPCRegistry().getNPC(aPlayer.getPlayer()).getTrait(BotBaseTrait.class).ready))
                                    playersOnline++;
                                else {
                                    PlayerManager.onlineAlphaPlayers.remove(aPlayer);
                                    PlayerManager.alphaPlayers.remove(aPlayer.getUuid());
                                }
                            }
                            ncount.setCount(playersOnline);
                            sync(currentttl, playersOnline, ncount.getStartPlayers(), ncount.getMaxPlayers(), nmap.getMap());
                        }
                    }
                }
            }
            //start the task immediately so that it won't inflate the count if the server starts after a reset (it will add 0 players)
        }.runTaskTimerAsynchronously(ServerLink.plugin, 2, 20 * 60 * 2);
    }

    public static void sync(long currentttl, int playersOnline, int startPlayers, int maxPlayers, String map) {
        synchronized (lock) {
            System.out.println("\n\nSyncing after net reset...\n\n");
            ttl = currentttl;
            Jedis jedis = JedisOne.getOne();
            jedis.incrBy("globalCount", playersOnline);
            jedis.incrBy(ServerLink.getServerType().toString() + "Count", playersOnline);
            jedis.hset("count", ServerLink.getServerName(), String.valueOf(playersOnline));
            jedis.hset("startPlayers", ServerLink.getServerName(), String.valueOf(startPlayers));
            jedis.hset("maxPlayers", ServerLink.getServerName(), String.valueOf(maxPlayers));
            jedis.hset("map", ServerLink.getServerName(), map);
            JedisOne.closeOne();
            nstatus.sync();
            navail.sync();
            resetTtl();
        }
    }

    private static void resetTtl() {
        new BukkitRunnable() {
            @Override
            public void run() {
                synchronized (lock) {
                    Jedis jedis = JedisOne.getOne();
                    Long ttl = jedis.ttl("reset");
                    JedisOne.closeOne();
                    if (ttl <= 0) Reset.ttl = 0;
                    else resetTtl();
                }
            }
        }.runTaskLaterAsynchronously(ServerLink.plugin, (20 * 300) + 40);
    }
}
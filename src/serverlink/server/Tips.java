package serverlink.server;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.network.JedisOne;
import serverlink.util.TimeUtils;

import java.util.List;

public class Tips {
    private static final int tipInterval = 60 * 3;
    private static int currentTipIndex = 0;

    public static void setup() {
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisOne.getTwo();
                String tipIndex = jedis.get(ServerLink.getServerType() + "TipIndex");
                if (tipIndex != null && !tipIndex.isEmpty()) currentTipIndex = Integer.parseInt(tipIndex);
                JedisOne.closeTwo();
            }
        }.runTaskLaterAsynchronously(ServerLink.plugin, 20);
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisOne.getTwo();
                String tipTime = jedis.get(ServerLink.getServerType() + "TipTime");
                if (tipTime == null || tipTime.isEmpty()) {
                    tipTime = String.valueOf(TimeUtils.getCurrent());
                    jedis.set(ServerLink.getServerType() + "TipTime", tipTime);
                }
                JedisOne.closeTwo();
                int startDelay = TimeUtils.getCurrent() - Integer.parseInt(tipTime);
                startDelay = tipInterval - ((startDelay + tipInterval) % tipInterval);

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        Jedis jedis = JedisOne.getTwo();
                        jedis.set(ServerLink.getServerType() + "TipTime", String.valueOf(TimeUtils.getCurrent()));
                        List<String> tips = jedis.lrange(ServerLink.getServerType() + "Tips", 0, -1);
                        if (tips == null || tips.isEmpty()) {
                            JedisOne.closeTwo();
                            return;
                        }
                        if (currentTipIndex >= tips.size() - 1) {
                            currentTipIndex = 0;
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    Bukkit.getServer().broadcastMessage(chat.color("&6&lTip&c> " + tips.get(currentTipIndex)));
                                }
                            }.runTask(ServerLink.plugin);
                            jedis.set(ServerLink.getServerType() + "TipIndex", "0");
                        } else {
                            currentTipIndex++;
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    Bukkit.getServer().broadcastMessage(chat.color("&6&lTip&c> " + tips.get(currentTipIndex)));
                                }
                            }.runTask(ServerLink.plugin);
                            jedis.set(ServerLink.getServerType() + "TipIndex", String.valueOf(currentTipIndex));
                        }
                        JedisOne.closeTwo();
                    }
                }.runTaskTimerAsynchronously(ServerLink.plugin, 20 * startDelay, 20 * tipInterval);
            }
        }.runTaskLaterAsynchronously(ServerLink.plugin, 20);
    }
}

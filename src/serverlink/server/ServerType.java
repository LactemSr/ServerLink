package serverlink.server;

import serverlink.ServerLink;

public enum ServerType {
    hub,
    kitpvp("KitPvP Token", "KitPvP Tokens"),
    skybattle("Sky Gem", "Sky Gems"),
    skyblock("Skystar", "Skystars"),
    other;

    private String currencySingular;
    private String currencyPlural;

    ServerType(String currencySingular, String currencyPlural) {
        this.currencySingular = currencySingular;
        this.currencyPlural = currencyPlural;
    }

    ServerType() {
        currencySingular = "point";
        currencyPlural = "points";
    }

    public static ServerType getType(String typeName) {
        typeName = typeName.toLowerCase().replaceAll(" ", "").replaceAll("_", "");
        for (ServerType serverType1 : values()) {
            if (serverType1.name().toLowerCase().replaceAll("_", "").equals(typeName))
                return serverType1;
        }
        return other;
    }

    public static String getThisCurrency() {
        return ServerLink.getServerType().getCurrency();
    }

    public static String getThisCurrency_Singular() {
        return ServerLink.getServerType().getCurrency_Singular();
    }

    public String getCurrency_Singular() {
        return currencySingular;
    }

    public String getCurrency() {
        return currencyPlural;
    }
}

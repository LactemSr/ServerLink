package serverlink.npc;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.EnumWrappers;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.plugin.PluginManager;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.customevent.CustomNpcClickEvent;

import java.util.HashMap;

public class NpcEventListener implements Listener {
    public static final HashMap<Player, Long> cooldowns = new HashMap<>();
    private static final HashMap<Player, CustomNpcClickEvent> queue = new HashMap<>();
    private static final PluginManager pluginManager = Bukkit.getPluginManager();

    public static void setup() {
        // This setup isn't optimal, but packets are received async and I think this is
        // the best way to handle the situation.
        new BukkitRunnable() {
            @Override
            public void run() {
                synchronized (queue) {
                    for (Player p : queue.keySet()) pluginManager.callEvent(queue.get(p));
                    queue.clear();
                }
            }
        }.runTaskTimer(ServerLink.plugin, 3, 3);

        ServerLink.protocolManager.addPacketListener(
                new PacketAdapter(ServerLink.plugin, ListenerPriority.NORMAL,
                        PacketType.Play.Client.USE_ENTITY) {
                    @Override
                    public void onPacketReceiving(PacketEvent event) {
                        if (event.getPacketType() != PacketType.Play.Client.USE_ENTITY) return;
                        PacketContainer packet = event.getPacket();
                        int targetId = packet.getIntegers().read(0);
                        EnumWrappers.EntityUseAction click = packet.getEntityUseActions().read(0);
                        ClickType clickType = click == EnumWrappers.EntityUseAction.ATTACK ? ClickType.LEFT : ClickType.RIGHT;
                        synchronized (queue) {
                            if (NpcManager.getNpcs().contains(targetId)) {
                                if (cooldowns.containsKey(event.getPlayer()) && cooldowns.get(event.getPlayer()) > System.currentTimeMillis())
                                    return;
                                queue.put(event.getPlayer(), new CustomNpcClickEvent(event.getPlayer(), targetId, clickType));
                                cooldowns.put(event.getPlayer(), System.currentTimeMillis() + 333);
                            }
                        }
                    }
                });
    }
}

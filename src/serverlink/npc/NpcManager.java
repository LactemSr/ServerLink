package serverlink.npc;

import net.minecraft.server.v1_8_R3.EntityPlayer;
import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.npc.customentity.*;

import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class NpcManager {
    public static ArrayList<Location> protectedNpcs = new ArrayList<>();
    private static HashMap<Integer, CustomEntity> npcs = new HashMap<>();

    public static Set<Integer> getNpcs() {
        return npcs.keySet();
    }

    public static void removeNpc(CustomEntity customEntity) {
        npcs.remove(customEntity.getEntity().getId());
        for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
            customEntity.removeForPlayer(aPlayer.getPlayer());
        }
    }

    /**
     * @param playerDisplayName   is only for player npcs
     * @param playerSkinOwnerName is only for player npcs
     */
    public static CustomEntity createNpc(CustomEntityType type,
                                         Location loc,
                                         @Nullable String playerSkinOwnerName,
                                         @Nullable String playerDisplayName,
                                         @Nullable ItemStack weapon,
                                         @Nullable ItemStack helmet,
                                         @Nullable ItemStack chestplate,
                                         @Nullable ItemStack leggings,
                                         @Nullable ItemStack boots) {
        CustomEntity customEntity = null;
        switch (type) {
            case BAT:
                customEntity = new CustomBat(loc);
                break;
            case BLAZE:
                customEntity = new CustomBlaze(loc);
                break;
            case CAVE_SPIDER:
                customEntity = new CustomCaveSpider(loc);
                break;
            case CHICKEN:
                customEntity = new CustomChicken(loc);
                break;
            case COW:
                customEntity = new CustomCow(loc);
                break;
            case CREEPER:
                customEntity = new CustomCreeper(loc);
                break;
            case ENDERDRAGON:
                customEntity = new CustomEnderdragon(loc);
                break;
            case ENDERMAN:
                customEntity = new CustomEnderman(loc, weapon);
                break;
            case ENDERMITE:
                customEntity = new CustomEndermite(loc);
                break;
            case GHAST:
                customEntity = new CustomGhast(loc);
                break;
            case GIANT:
                customEntity = new CustomGiant(loc);
                break;
            case GUARDIAN:
                customEntity = new CustomGuardian(loc);
                break;
            case HORSE:
                customEntity = new CustomHorse(loc);
                break;
            case IRON_GOLEM:
                customEntity = new CustomIronGolem(loc);
                break;
            case MAGMA_CUBE:
                customEntity = new CustomIronGolem(loc);
                break;
            case MUSHROOM_COW:
                customEntity = new CustomMushroomCow(loc);
                break;
            case OCELOT:
                customEntity = new CustomOcelot(loc);
                break;
            case PIG:
                customEntity = new CustomPig(loc);
                break;
            case PLAYER:
                if (playerDisplayName == null) playerDisplayName = "NPC";
                playerDisplayName = chat.color(playerDisplayName);
                String prefix = "";
                String suffix = "";
                if (playerDisplayName.length() > 16) {
                    if (playerDisplayName.length() > 31) {
                        prefix = playerDisplayName.substring(0, 16);
                        suffix = playerDisplayName.substring(17, playerDisplayName.length());
                        playerDisplayName = playerDisplayName.substring(16, 17);
                    } else {
                        prefix = playerDisplayName.substring(0, playerDisplayName.length() - 16);
                        playerDisplayName = playerDisplayName.substring(playerDisplayName.length() - 16, playerDisplayName.length());
                    }
                }
                customEntity = new CustomPlayer(loc, playerSkinOwnerName, prefix, playerDisplayName, suffix, weapon, helmet, chestplate, leggings, boots);
                break;
            case RABBIT:
                customEntity = new CustomRabbit(loc);
                break;
            case SHEEP:
                customEntity = new CustomSheep(loc);
                break;
            case SILVERFISH:
                customEntity = new CustomSilverfish(loc);
                break;
            case SKELETON:
                customEntity = new CustomSkeleton(loc, weapon, helmet, chestplate, leggings, boots);
                break;
            case SLIME:
                customEntity = new CustomSlime(loc);
                break;
            case SNOWMAN:
                customEntity = new CustomSnowman(loc);
                break;
            case SPIDER:
                customEntity = new CustomSpider(loc);
                break;
            case SQUID:
                customEntity = new CustomSquid(loc);
                break;
            case VILLAGER:
                customEntity = new CustomVillager(loc);
                break;
            case WITCH:
                customEntity = new CustomWitch(loc);
                break;
            case WITHER:
                customEntity = new CustomWither(loc);
                break;
            case WITHER_SKELETON:
                customEntity = new CustomWitherSkeleton(loc, weapon, helmet, chestplate, leggings, boots);
                break;
            case WOLF:
                customEntity = new CustomWolf(loc);
                break;
            case ZOMBIE:
                customEntity = new CustomZombie(loc, weapon, helmet, chestplate, leggings, boots);
                break;
            case ZOMBIE_PIGMAN:
                customEntity = new CustomZombiePigman(loc, weapon, helmet, chestplate, leggings, boots);
                break;
        }
        npcs.put(customEntity.getEntity().getId(), customEntity);
        return customEntity;
    }

    public static double getHologramHeight(CustomEntity customEntity) {
        if (customEntity.getEntity() instanceof EntityPlayer) return 3;
        String type = customEntity.getEntity().getName().toLowerCase().replace("entity", "");
        int y = 3;
        switch (type) {
            case "zombie":
                y = 3;
                break;
            case "villager":
                y = 3;
                break;
            case "zombiepigman":
            case "pigzombie":
            case "zombie_pigman":
                y = 3;
                break;
            case "sheep":
                y = 2;
                break;
            case "pig":
                y = 2;
                break;
            case "cow":
                y = 2;
                break;
            case "mushroomcow":
            case "mooshroom":
                y = 2;
                break;
            case "horse":
                y = 3;
                break;
            case "chicken":
                y = 1;
                break;
            case "blaze":
                y = 3;
                break;
            case "cavespider":
            case "cave_spider":
                y = 2;
                break;
            case "spider":
                y = 2;
                break;
            case "creeper":
                y = 2;
                break;
            case "enderman":
                y = 4;
                break;
            case "guardian":
                y = 2;
                break;
            case "irongolem":
            case "iron_golem":
                y = 3;
                break;
            case "rabbit":
                y = 1;
                break;
            case "snowman":
                y = 3;
                break;
            case "bat":
                y = 0;
                break;
            case "ghast":
                y = 3;
                break;
            case "magmacube":
            case "magma_cube":
                y = 2;
                break;
            case "slime":
                y = 2;
                break;
            case "skeleton":
                y = 3;
                break;
            case "witherskull":
            case "wither_skull":
            case "witherskeleton":
            case "wither_skeleton":
                y = 3;
                break;
            case "squid":
                y = 2;
                break;
            case "witch":
                y = 3;
                break;
            case "wolf":
            case "dog":
                y = 2;
                break;
        }
        return y;
    }
}

package serverlink.npc.customentity;

import com.mojang.authlib.GameProfile;
import com.mojang.authlib.properties.Property;
import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.chat.nametag.FakeTeam;
import serverlink.chat.nametag.Nametag;
import serverlink.util.profilefetcher.ProfileFetchResult;
import serverlink.util.profilefetcher.ProfileFetcher;

import java.util.UUID;

public class CustomPlayer extends CustomEntity {
    private Packet addPlayerPacket;
    private Packet spawnPacket;
    private Packet teleportPacket;
    private Packet lookPacket;
    private Packet weaponPacket = null;
    private Packet helmetPacket = null;
    private Packet chestplatePacket = null;
    private Packet leggingsPacket = null;
    private Packet bootsPacket = null;
    private Packet removePacket;
    private Packet destroyPacket;
    private boolean skinLoaded = false;
    private boolean skinLoadedImmediately = false;
    private boolean attemptingProfileFetch = false;

    public CustomPlayer(Location loc, String skinOwner, String prefix, String npcName,
                        String suffix, ItemStack weapon, ItemStack helmet,
                        ItemStack chestplate, ItemStack leggings, ItemStack boots) {
        super();
        nmsEntity = new EntityPlayer(((CraftServer) Bukkit.getServer()).getServer(), ((CraftWorld) loc.getWorld()).getHandle(), makeProfile(npcName, skinOwner), new PlayerInteractManager(((CraftWorld) loc.getWorld()).getHandle()));
        nmsEntity.setPositionRotation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
        ((EntityPlayer) nmsEntity).playerInteractManager.setGameMode(WorldSettings.EnumGamemode.CREATIVE);
        addPlayerPacket = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.ADD_PLAYER, (EntityPlayer) nmsEntity);
        spawnPacket = new PacketPlayOutNamedEntitySpawn((EntityPlayer) nmsEntity);
        teleportPacket = new PacketPlayOutEntityTeleport(nmsEntity);
        lookPacket = new PacketPlayOutEntityHeadRotation(nmsEntity, (byte) (nmsEntity.yaw * 256 / 360));
        if (prefix == null) prefix = "";
        if (suffix == null) suffix = "";
        if (!prefix.isEmpty() || !suffix.isEmpty()) {
            FakeTeam fakeTeam = Nametag.getFakeTeam(prefix, suffix);
            Nametag.addPlayerToTeam(npcName, fakeTeam);
        }
        if (weapon != null) {
            ((EntityPlayer) nmsEntity).getBukkitEntity().getEquipment().setItemInHand(weapon);
            weaponPacket = new PacketPlayOutEntityEquipment(nmsEntity.getId(), 0, CraftItemStack.asNMSCopy(((EntityPlayer) nmsEntity).getBukkitEntity().getEquipment().getItemInHand()));
        }
        if (helmet != null) {
            ((EntityPlayer) nmsEntity).getBukkitEntity().getEquipment().setHelmet(helmet);
            helmetPacket = new PacketPlayOutEntityEquipment(nmsEntity.getId(), 4, CraftItemStack.asNMSCopy(((EntityPlayer) nmsEntity).getBukkitEntity().getEquipment().getHelmet()));
        }
        if (chestplate != null) {
            ((EntityPlayer) nmsEntity).getBukkitEntity().getEquipment().setChestplate(chestplate);
            chestplatePacket = new PacketPlayOutEntityEquipment(nmsEntity.getId(), 3, CraftItemStack.asNMSCopy(((EntityPlayer) nmsEntity).getBukkitEntity().getEquipment().getChestplate()));
        }
        if (leggings != null) {
            ((EntityPlayer) nmsEntity).getBukkitEntity().getEquipment().setLeggings(leggings);
            leggingsPacket = new PacketPlayOutEntityEquipment(nmsEntity.getId(), 2, CraftItemStack.asNMSCopy(((EntityPlayer) nmsEntity).getBukkitEntity().getEquipment().getLeggings()));
        }
        if (boots != null) {
            ((EntityPlayer) nmsEntity).getBukkitEntity().getEquipment().setBoots(boots);
            bootsPacket = new PacketPlayOutEntityEquipment(nmsEntity.getId(), 1, CraftItemStack.asNMSCopy(((EntityPlayer) nmsEntity).getBukkitEntity().getEquipment().getBoots()));
        }
        removePacket = new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, (EntityPlayer) nmsEntity);
        destroyPacket = new PacketPlayOutEntityDestroy(nmsEntity.getId());
    }

    @Override
    public void spawnForPlayer(Player player) {
        if (skinLoaded) skinLoadedImmediately = true;
        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
        connection.sendPacket(addPlayerPacket);
        connection.sendPacket(spawnPacket);
        connection.sendPacket(lookPacket);
        if (weaponPacket != null) connection.sendPacket(weaponPacket);
        if (helmetPacket != null) connection.sendPacket(helmetPacket);
        if (chestplatePacket != null) connection.sendPacket(chestplatePacket);
        if (leggingsPacket != null) connection.sendPacket(leggingsPacket);
        if (bootsPacket != null) connection.sendPacket(bootsPacket);
        new BukkitRunnable() {
            @Override
            public void run() {
                if (!getPlayersInView().contains(player)) {
                    cancel();
                    return;
                }
                if (!skinLoaded) return;
                if (!skinLoadedImmediately) {
                    connection.sendPacket(removePacket);
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
                            connection.sendPacket(addPlayerPacket);
                            connection.sendPacket(spawnPacket);
                            connection.sendPacket(lookPacket);
                            if (weaponPacket != null) connection.sendPacket(weaponPacket);
                            if (helmetPacket != null) connection.sendPacket(helmetPacket);
                            if (chestplatePacket != null) connection.sendPacket(chestplatePacket);
                            if (leggingsPacket != null) connection.sendPacket(leggingsPacket);
                            if (bootsPacket != null) connection.sendPacket(bootsPacket);
                        }
                    }.runTaskLater(ServerLink.plugin, 10);
                }
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        connection.sendPacket(removePacket);
                    }
                }.runTaskLater(ServerLink.plugin, 60);
                cancel();
            }
        }.runTaskTimer(ServerLink.plugin, 5, 20);
    }

    @Override
    public void teleportServerSide(Location loc) {
        nmsEntity.setPositionRotation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
        lookPacket = new PacketPlayOutEntityHeadRotation(nmsEntity, (byte) (nmsEntity.yaw * 256 / 360));
        teleportPacket = new PacketPlayOutEntityTeleport(nmsEntity);
    }

    @Override
    public void updateLocForPlayer(Player player, Location loc) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(teleportPacket);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(lookPacket);
    }

    @Override
    public void removeForPlayer(Player player) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(destroyPacket);
    }

    private GameProfile makeProfile(String name, String skinOwner) {
        GameProfile profile = new GameProfile(UUID.randomUUID(), name);
        new BukkitRunnable() {
            @Override
            public void run() {
                if (skinLoaded) {
                    cancel();
                    return;
                }
                if (!attemptingProfileFetch) {
                    attemptingProfileFetch = true;
                    ProfileFetcher.fetch(name, skinOwner, request -> {
                        if (request.getResult() == ProfileFetchResult.PENDING) return;
                        attemptingProfileFetch = false;
                        //noinspection ConstantConditions
                        if (request.getResult() == ProfileFetchResult.SUCCESS) {
                            Property textures = request.getProfile().getProperties().get("textures").iterator().next();
                            profile.getProperties().put("textures", textures);
                            skinLoaded = true;
                        }
                    });
                }
            }
        }.runTaskTimer(ServerLink.plugin, 0, 40);
        return profile;
    }
}

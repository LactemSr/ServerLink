package serverlink.npc.customentity;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class CustomWitherSkeleton extends CustomEntity {
    private Packet spawnPacket;
    private Packet teleportPacket;
    private Packet lookPacket;
    private Packet weaponPacket = null;
    private Packet helmetPacket = null;
    private Packet chestplatePacket = null;
    private Packet leggingsPacket = null;
    private Packet bootsPacket = null;
    private Packet removePacket;

    public CustomWitherSkeleton(Location loc, ItemStack weapon, ItemStack helmet, ItemStack chestplate, ItemStack leggings, ItemStack boots) {
        super();
        nmsEntity = new EntitySkeleton(((CraftWorld) loc.getWorld()).getHandle());
        ((EntitySkeleton) nmsEntity).setSkeletonType(1);
        nmsEntity.setPositionRotation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
        spawnPacket = new PacketPlayOutSpawnEntityLiving(nmsEntity);
        teleportPacket = new PacketPlayOutEntityTeleport(nmsEntity);
        lookPacket = new PacketPlayOutEntityHeadRotation(nmsEntity, (byte) (nmsEntity.yaw * 256 / 360));
        if (weapon != null) {
            nmsEntity.setEquipment(0, CraftItemStack.asNMSCopy(weapon));
            weaponPacket = new PacketPlayOutEntityEquipment(nmsEntity.getId(), 0, nmsEntity.getEquipment(0));
        }
        if (helmet != null) {
            nmsEntity.setEquipment(1, CraftItemStack.asNMSCopy(helmet));
            helmetPacket = new PacketPlayOutEntityEquipment(nmsEntity.getId(), 1, nmsEntity.getEquipment(1));
        }
        if (chestplate != null) {
            nmsEntity.setEquipment(2, CraftItemStack.asNMSCopy(chestplate));
            chestplatePacket = new PacketPlayOutEntityEquipment(nmsEntity.getId(), 2, nmsEntity.getEquipment(2));
        }
        if (leggings != null) {
            nmsEntity.setEquipment(3, CraftItemStack.asNMSCopy(leggings));
            leggingsPacket = new PacketPlayOutEntityEquipment(nmsEntity.getId(), 3, nmsEntity.getEquipment(3));
        }
        if (boots != null) {
            nmsEntity.setEquipment(4, CraftItemStack.asNMSCopy(boots));
            bootsPacket = new PacketPlayOutEntityEquipment(nmsEntity.getId(), 4, nmsEntity.getEquipment(4));
        }
        removePacket = new PacketPlayOutEntityDestroy(nmsEntity.getId());
    }

    @Override
    public void spawnForPlayer(Player player) {
        PlayerConnection connection = ((CraftPlayer) player).getHandle().playerConnection;
        connection.sendPacket(spawnPacket);
        connection.sendPacket(lookPacket);
        if (weaponPacket != null) connection.sendPacket(weaponPacket);
        if (helmetPacket != null) connection.sendPacket(helmetPacket);
        if (chestplatePacket != null) connection.sendPacket(chestplatePacket);
        if (leggingsPacket != null) connection.sendPacket(leggingsPacket);
        if (bootsPacket != null) connection.sendPacket(bootsPacket);
    }

    @Override
    public void teleportServerSide(Location loc) {
        nmsEntity.setPositionRotation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
        lookPacket = new PacketPlayOutEntityHeadRotation(nmsEntity, (byte) (nmsEntity.yaw * 256 / 360));
        teleportPacket = new PacketPlayOutEntityTeleport(nmsEntity);
    }

    @Override
    public void updateLocForPlayer(Player player, Location loc) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(teleportPacket);
    }

    @Override
    public void removeForPlayer(Player player) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(removePacket);
    }
}

package serverlink.npc.customentity;

import net.minecraft.server.v1_8_R3.*;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.CraftWorld;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;

public class CustomSheep extends CustomEntity {
    private Packet spawnPacket;
    private Packet teleportPacket;
    private Packet lookPacket;
    private Packet removePacket;

    public CustomSheep(Location loc) {
        super();
        nmsEntity = new EntitySheep(((CraftWorld) loc.getWorld()).getHandle());
        nmsEntity.setPositionRotation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
        spawnPacket = new PacketPlayOutSpawnEntityLiving(nmsEntity);
        teleportPacket = new PacketPlayOutEntityTeleport(nmsEntity);
        lookPacket = new PacketPlayOutEntityHeadRotation(nmsEntity, (byte) (nmsEntity.yaw * 256 / 360));
        removePacket = new PacketPlayOutEntityDestroy(nmsEntity.getId());
    }

    @Override
    public void spawnForPlayer(Player player) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(spawnPacket);
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(lookPacket);
    }

    @Override
    public void teleportServerSide(Location loc) {
        nmsEntity.setPositionRotation(loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
        lookPacket = new PacketPlayOutEntityHeadRotation(nmsEntity, (byte) (nmsEntity.yaw * 256 / 360));
        teleportPacket = new PacketPlayOutEntityTeleport(nmsEntity);
    }

    @Override
    public void updateLocForPlayer(Player player, Location loc) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(teleportPacket);
    }

    @Override
    public void removeForPlayer(Player player) {
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(removePacket);
    }
}

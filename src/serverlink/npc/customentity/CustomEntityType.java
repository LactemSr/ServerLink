package serverlink.npc.customentity;

import org.bukkit.craftbukkit.libs.jline.internal.Nullable;

import java.util.HashMap;

public enum CustomEntityType {
    BAT,
    BLAZE,
    CAVE_SPIDER,
    CHICKEN,
    COW,
    CREEPER,
    ENDERDRAGON("dragon"),
    ENDERMAN,
    ENDERMITE,
    GHAST,
    GIANT("giantzombie"),
    GUARDIAN,
    HORSE,
    IRON_GOLEM,
    MAGMA_CUBE,
    MUSHROOM_COW("mooshroom", "mooshroomcow"),
    OCELOT("cat"),
    PIG,
    PLAYER("human", "npc"),
    RABBIT,
    SHEEP,
    SILVERFISH,
    SKELETON,
    SLIME,
    SNOWMAN,
    SPIDER,
    SQUID,
    VILLAGER,
    WITCH,
    WITHER,
    WITHER_SKELETON("witherskull"),
    WOLF("dog"),
    ZOMBIE,
    ZOMBIE_PIGMAN("pigzombie");


    private static HashMap<String, CustomEntityType> nameMap = new HashMap<>();


    static {
        for (CustomEntityType type : values()) {
            nameMap.put(type.name().toLowerCase().replaceAll("_", ""), type);
            if (type.aliases != null) {
                for (String alias : type.aliases) {
                    nameMap.put(alias, type);
                }
            }
        }
    }

    private String[] aliases;


    private CustomEntityType(String... aliases) {
        this.aliases = aliases;
    }

    public static
    @Nullable
    CustomEntityType fromName(String name) {
        return nameMap.get(name.toLowerCase().replace("entity", "").replaceAll("_", ""));
    }
}

package serverlink.npc.customentity;

import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.EntityLiving;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.npc.NpcManager;

import java.util.ArrayList;
import java.util.List;

public abstract class CustomEntity {
    EntityLiving nmsEntity;
    private List<Player> playersInView = new ArrayList<>();

    CustomEntity() {
        // Create a runnable to show the npc to nearby players
        new BukkitRunnable() {
            @Override
            public void run() {
                if (!NpcManager.getNpcs().contains(nmsEntity.getId())) {
                    cancel();
                    return;
                }
                Location loc = new Location(nmsEntity.getWorld().getWorld()
                        , nmsEntity.locX, nmsEntity.locY, nmsEntity.locZ);
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (!loc.getWorld().getName().equals(player.getWorld().getName())
                            || loc.distanceSquared(player.getLocation()) > 70 * 70) {
                        // Send player remove packets if the previously in-range npc is now out of range
                        if (playersInView.contains(player)) {
                            removeForPlayer(player);
                            playersInView.remove(player);
                        }
                    } else {
                        // Send player spawn packets if the npc is in range
                        if (!playersInView.contains(player)) {
                            spawnForPlayer(player);
                            playersInView.add(player);
                        }
                    }
                }
            }
        }.runTaskTimer(ServerLink.plugin, 5, 20);
    }

    /**
     * @return A list of players who have received packets to view the npc because they are near it
     */
    public List<Player> getPlayersInView() {
        return playersInView;
    }

    public Entity getEntity() {
        return nmsEntity;
    }

    public void teleportServerSide(Location loc) {
    }

    public void updateLocForPlayer(Player player, Location loc) {
    }

    public void spawnForPlayer(Player player) {
    }

    public void removeForPlayer(Player player) {
    }
}

package serverlink;

import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.ProtocolManager;
import com.sun.istack.internal.NotNull;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.alphaplayer.*;
import serverlink.bot.BotListener;
import serverlink.bot.BotManager;
import serverlink.bot.movement.SkybattleBotMovement;
import serverlink.chat.*;
import serverlink.classes.ClassEvents;
import serverlink.classes.ClassManager;
import serverlink.classes.ClassSelectionEvents;
import serverlink.classes.CooldownRunnable;
import serverlink.command.*;
import serverlink.command.admin.*;
import serverlink.command.punishment.BanCommand;
import serverlink.command.punishment.MuteCommand;
import serverlink.command.punishment.PardonCommand;
import serverlink.command.punishment.UnmuteCommand;
import serverlink.command.server.*;
import serverlink.inventory.Event;
import serverlink.inventory.InventoryManager;
import serverlink.inventory.gui.profile.BoostersGui;
import serverlink.inventory.gui.settings.*;
import serverlink.map.BorderEvents;
import serverlink.map.Map;
import serverlink.map.MapManager;
import serverlink.network.*;
import serverlink.npc.NpcEventListener;
import serverlink.player.*;
import serverlink.player.damage.PlayerDamage;
import serverlink.player.damage.PlayerDamageEvents;
import serverlink.player.hacking.Hacking;
import serverlink.player.interaction.PlayerInteraction;
import serverlink.player.interaction.PlayerInteractionEvents;
import serverlink.player.party.PartyManager;
import serverlink.player.party.PartyNetworkSync;
import serverlink.player.stats.StatsManager;
import serverlink.round.RoundManager;
import serverlink.server.*;
import serverlink.server.worldsettings.WorldSettings;
import serverlink.server.worldsettings.WorldSettingsEvents;
import serverlink.util.*;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class ServerLink extends JavaPlugin {
    public static final Thread mainThread = Thread.currentThread();
    private static final List<Method> preJoinMethods = new ArrayList<>();
    public static Plugin plugin;
    public static ServerLink main;
    public static ProtocolManager protocolManager;
    private static CommandManager commandManager;
    // TODO add /help admin
    // TODO test what happens if I party chat while one party member is offline (how will this affect bungee messaging?)
    // TODO sync serverName with bungee server name
    private static String serverName = "";
    private static String fancyServerName = "Server";
    private static ServerType serverType = ServerType.other;

    /**
     * Must only be called from the main thread
     */
    public static boolean isLoggedIn(Player player) {
        return PlayerManager.get(player).isFullyLoggedIn();
    }

    public static void startShutdown() {
        Permissions.shutDown();
        nmessage.shutdown();
        try {
            BotManager.disable();
        } catch (Throwable ignored) {
        }
        Jedis jedis = JedisPool.getConn();
        for (Player player : Bukkit.getOnlinePlayers()) {
            if (getServerType() != ServerType.hub) player.performCommand("hub");
            else {
                jedis.hset(player.getUniqueId().toString(), "online", "false");
                player.kickPlayer("Game temporarily shutting down for maintenance.");
                ncount.decrCount();
            }
        }
        JedisPool.close(jedis);
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Player player : Bukkit.getOnlinePlayers()) {
                    player.kickPlayer("Game temporarily shutting down for maintenance.");
                }
                // Run this after all players leave because you cannot unload a world with players in it
                for (Map map : MapManager.getGameMaps()) {
                    MapManager.unloadWorld(map.getWorld());
                }
                // Let the async threads finish their operations before shutting down the backend
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException ignored) {
                }
                Bukkit.shutdown();
            }
        }.runTaskLater(plugin, 10);
    }

    private static void shutdownBackend() {
        // Run this after all the players leave so their leave updates don't mess with server availability
        // Run this after depending plugins have shutdown in case they need a jedis connection
        navail.shutdown();
        JedisOne.shutdown();
        JedisPool.shutdown();
    }

    public static String getServerName() {
        return serverName;
    }

    public static void setServerName(@NotNull String name) {
        serverName = name;
    }

    public static String getFancyServerName() {
        return fancyServerName;
    }

    public static void setFancyServerName(@NotNull String serverName) {
        fancyServerName = serverName;
    }

    public static ServerType getServerType() {
        return serverType;
    }

    public static void setServerType(@NotNull ServerType type) {
        serverType = type;
    }

    public static void addPreJoinMethod(Method method) {
        if (!preJoinMethods.contains(method)) preJoinMethods.add(method);
    }

    public static List<Method> getPreJoinMethods() {
        return preJoinMethods;
    }

    // Only set the boolean to true if uuid is needed to fulfill a purchase
    // and there is a chance the player has never joined AlphaCraft.
    //
    // Call this asynchronously.
    public static UUID getUUIDFromName(String name, boolean useUUIDFetcherIfNecessary) {
        name = name.toLowerCase();
        UUID uuid = null;
        Player player = Bukkit.getPlayerExact(name);
        if (player != null && player.isOnline()) uuid = player.getUniqueId();
        else {
            Jedis jedis = JedisPool.getConn();
            String uuidFromRedis = jedis.get(name);
            JedisPool.close(jedis);
            if (uuidFromRedis != null && !uuidFromRedis.isEmpty()) uuid = UUID.fromString(uuidFromRedis);
        }
        if (useUUIDFetcherIfNecessary && uuid == null) {
            try {
                uuid = UUIDFetcher.getOfflineUUID(name);
            } catch (Exception ignored) {
            }
        }
        return uuid;
    }

    /**
     * You can call this from the main thread. The very tiny amount of blocking it does if the username is not
     * cached is inevitable and is the best way since every call after it will get it from the cache instead of blocking.
     * <p>
     * Certain functionalities may require getting the username of an offline player to
     * display to an AlphaPlayer on a regular basis.
     * This method stores them in reference to an AlphaPlayer. If there is no username stored,
     * it will look it up in the database.
     * Not to be used for players that have possibly never joined the network or on possibly invalid players.
     * This is basically a replacement for OfflinePlayer.getName().
     */

    public static String getNameFromAlphaPlayer(AlphaPlayer aPlayer, UUID getNameOf) {
        String name = (String) aPlayer.getData("nameOfOfflinePlayer:" + getNameOf.toString()).getValue();
        if (name != null && !name.isEmpty()) return name;
        Jedis jedis = JedisPool.getConn();
        name = jedis.hget(getNameOf.toString(), "username");
        JedisPool.close(jedis);
        String finalName = name;
        if (Thread.currentThread() == mainThread)
            aPlayer.getData("nameOfOfflinePlayer:" + getNameOf.toString()).setValue(finalName);
        else new BukkitRunnable() {
            @Override
            public void run() {
                aPlayer.getData("nameOfOfflinePlayer:" + getNameOf.toString()).setValue(finalName);
            }
        }.runTask(plugin);
        return name;
    }

    public static CommandManager getCommandManager() {
        return commandManager;
    }

    @Override
    public void onEnable() {
        plugin = this;
        main = this;
        setup();
    }

    @Override
    public void onDisable() {
        shutdownBackend();
    }

    public void setup() {
        protocolManager = ProtocolLibrary.getProtocolManager();
        if (!getDataFolder().exists()) //noinspection ResultOfMethodCallIgnored
            getDataFolder().mkdir();
        File configFile = new File(getDataFolder(), "universal.yml");
        YamlConfiguration config = YamlConfiguration.loadConfiguration(configFile);

        loadRedisInfo(configFile, config);
        setupNetwork(configFile, config);

        new HelpUtils();
        chat.setCommandUnavailable(ChatColor.RED + "Command Unavailable");
        MapManager.setLobbyMap(new Map(Bukkit.getWorlds().get(0), 300));
        LoggerListener.setup();
        WorldSettings.setup();

        InventoryManager.setup();
        Item.setup();
        Event.setup();
        ColorPickerGui.setup();
        SettingsGui.setup();

        LoginQueue.setup();
        LogoutQueue.setup();
        CooldownRunnable.setup();
        TrigCache.setup();

        PlayerDamage.setup();
        PlayerInteraction.setup();
        Coins.setup();
        Friends.setup();
        Forums.setup();
        PartyManager.setup();
        NpcEventListener.setup();
        TabCompletionBlocker.setup();

        Tips.setup();
        BotManager.setup();
        ChatFilter.setup();
        StatsManager.setup();

        registerCommandListeners();
        registerEventListeners();
        registerRoundListeners();
        registerBungeeListeners();
        registerBotTabCompletion();

        runTasksAfterServerSetup();
    }

    private void loadRedisInfo(File configFile, YamlConfiguration config) {
        // node1 and node2 need to be different databases (they can be on the same machine, though)
        if (!config.contains("database.node1.ip") && !config.contains("database.node1.port")) {
            config.set("database.node1.ip", "node1.alphacraft.us");
            config.set("database.node1.port", 6379);
        }
        if (!config.contains("database.node2.ip") && !config.contains("database.node2.port")) {
            config.set("database.node2.ip", "node2.alphacraft.us");
            config.set("database.node2.port", 6380);
        }
        // node 1
        String node1 = config.getString("database.node1.ip");
        if (node1.matches(".*[a-zA-Z]+.*")) {
            try {
                InetAddress address = InetAddress.getByName(node1);
                JedisOne.setIP(address.getHostAddress());
            } catch (UnknownHostException e) {
                getServer().getConsoleSender().sendMessage(ChatColor.RED + "COULD NOT RESOLVE NODE1 HOSTNAME: " + node1);
            }
        } else JedisOne.setIP(config.getString("database.node1.ip"));
        JedisOne.setPort(config.getInt("database.node1.port"));
        // node 2
        String node2 = config.getString("database.node2.ip");
        if (node2.matches(".*[a-zA-Z]+.*")) {
            try {
                InetAddress address = InetAddress.getByName(node2);
                JedisPool.setIP(address.getHostAddress());
            } catch (UnknownHostException e) {
                ConsoleOutput.printError("COULD NOT RESOLVE NODE2 HOSTNAME: " + node2);
            }
        } else JedisPool.setIP(config.getString("database.node2.ip"));
        JedisPool.setPort(config.getInt("database.node2.port"));

        if (config.contains("serverName")) {
            serverName = config.getString("serverName").toLowerCase();
        } else {
            config.set("serverName", "SET THIS NOW!!");
        }
        if (config.contains("gametype"))
            setServerType(ServerType.getType(config.getString("gametype")));
        else if (serverName.contains("-"))
            setServerType(ServerType.getType(serverName.split("-")[0]));
        try {
            config.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void setupNetwork(File configFile, YamlConfiguration config) {
        JedisOne.setup();
        JedisPool.setup();
        NetworkCache.setup();
        nmessage.setup();
        nstatus.setup();
        ncount.setCount(0);
        navail.setup();
        Permissions.setupGroups();
        Reset.setup();
        Slack.setup(configFile, config);
        ncount.setStartPlayers(ncount.getStartPlayers());
        ncount.setMaxPlayers(ncount.getMaxPlayers());
        nmap.setMap("loading...");
    }

    private void registerCommandListeners() {
        commandManager = new CommandManager();
        commandManager.registerCommands(main, new PermCommand());
        commandManager.registerCommands(main, new PointsCommand());
        commandManager.registerCommands(main, new CoinsCommand());
        commandManager.registerCommands(main, new ServerCommand());
        commandManager.registerCommands(main, new HubCommand());
        commandManager.registerCommands(main, new KitpvpCommand());
        commandManager.registerCommands(main, new SpleefCommand());
        commandManager.registerCommands(main, new SkywarsCommand());
        commandManager.registerCommands(main, new SkybattleCommand());
        commandManager.registerCommands(main, new SkyblockCommand());
        commandManager.registerCommands(main, new DsCommand());
        commandManager.registerCommands(main, new ListCommand());
        commandManager.registerCommands(main, new GlistCommand());
        commandManager.registerCommands(main, new HelpCommand());
        commandManager.registerCommands(main, new BanCommand());
        commandManager.registerCommands(main, new MuteCommand());
        commandManager.registerCommands(main, new PardonCommand());
        commandManager.registerCommands(main, new UnmuteCommand());
        commandManager.registerCommands(main, new StatusCommand());
        commandManager.registerCommands(main, new ResetCommand());
        commandManager.registerCommands(main, new ResetnetCommand());
        commandManager.registerCommands(main, new TpallCommand());
        commandManager.registerCommands(main, new NpcCommand());
        commandManager.registerCommands(main, new PartyCommand());
        commandManager.registerCommands(main, new WhereamiCommand());
        commandManager.registerCommands(main, new TellCommand());
        commandManager.registerCommands(main, new ReplyCommand());
        commandManager.registerCommands(main, new ForumsCommand());
        commandManager.registerCommands(main, new GuiCommand());
        commandManager.registerCommands(main, new DebugCommand());
        commandManager.registerCommands(main, new FriendsCommand());
        commandManager.registerCommands(main, new ReferCommand());
        commandManager.registerCommands(main, new JoinmessagesCommand());
        commandManager.registerCommands(main, new LeaveMessagesCommand());
        commandManager.registerCommands(main, new DeathmessagesCommand());
        commandManager.registerCommands(main, new PlayerCommand());
        commandManager.registerCommands(main, new RanktimeCommand());
        commandManager.registerCommands(main, new SetrankCommand());
        commandManager.registerCommands(main, new AddboosterCommand());
        commandManager.registerCommands(main, new ChestCommands());
        commandManager.registerCommands(main, new GrouppermsCommand());
        commandManager.registerCommands(main, new ClearchatCommand());
        commandManager.registerCommands(main, new TipCommand());
        commandManager.registerCommands(main, new BuyCommand());
        commandManager.registerCommands(main, new FailHackCommand());
        commandManager.registerCommands(main, new StatsCommand());
        commandManager.registerCommands(main, new BotsCommand());
        commandManager.registerCommands(main, new BotListener());
    }

    private void registerEventListeners() {
        Bukkit.getPluginManager().registerEvents(new Afk(), main);
        Bukkit.getPluginManager().registerEvents(new BorderEvents(), main);
        Bukkit.getPluginManager().registerEvents(new BoostersGui(), main);
        Bukkit.getPluginManager().registerEvents(new BotListener(), main);
        Bukkit.getPluginManager().registerEvents(new Coins(), main);
        Bukkit.getPluginManager().registerEvents(new ChatEvent(), main);
        Bukkit.getPluginManager().registerEvents(new ChatSettingsGui(), main);
        Bukkit.getPluginManager().registerEvents(new ClassSelectionEvents(), main);
        Bukkit.getPluginManager().registerEvents(new ColorPickerGui(), main);
        Bukkit.getPluginManager().registerEvents(new DisguiseListener(), main);
        Bukkit.getPluginManager().registerEvents(new DeathEvent(), main);
        Bukkit.getPluginManager().registerEvents(new Event(), main);
        Bukkit.getPluginManager().registerEvents(new Friends(), main);
        Bukkit.getPluginManager().registerEvents(new Points(), main);
        Bukkit.getPluginManager().registerEvents(new GametypeBooster(), main);
        Bukkit.getPluginManager().registerEvents(new Hacking(), main);
        Bukkit.getPluginManager().registerEvents(new JoinAndLeave(), main);
        Bukkit.getPluginManager().registerEvents(new MapManager(), main);
        Bukkit.getPluginManager().registerEvents(new Permissions(), main);
        Bukkit.getPluginManager().registerEvents(new PartyNetworkSync(), main);
        Bukkit.getPluginManager().registerEvents(new PlayersShownGui(), main);
        Bukkit.getPluginManager().registerEvents(new PlayerDamageEvents(), main);
        Bukkit.getPluginManager().registerEvents(new PlayerInteractionEvents(), main);
        Bukkit.getPluginManager().registerEvents(new NpcEventListener(), main);
        Bukkit.getPluginManager().registerEvents(new Punishments(), main);
        Bukkit.getPluginManager().registerEvents(new Referrals(), main);
        Bukkit.getPluginManager().registerEvents(new SettingsGui(), main);
        Bukkit.getPluginManager().registerEvents(new ShutdownInterceptor(), main);
        Bukkit.getPluginManager().registerEvents(new SpeedSettingsGui(), main);
        Bukkit.getPluginManager().registerEvents(new TreasureManager(), main);
        Bukkit.getPluginManager().registerEvents(new WorldSettingsEvents(), main);
        Bukkit.getPluginManager().registerEvents(new ClassEvents(), main);
        Bukkit.getPluginManager().registerEvents(commandManager, main);
        Bukkit.getPluginManager().registerEvents(new FlyAndDoubleJump(), main);
        Bukkit.getPluginManager().registerEvents(new ClassManager(), main);
        Bukkit.getPluginManager().registerEvents(new SkybattleBotMovement(), main);
    }

    private void registerRoundListeners() {
        RoundManager.registerListener(new ClassEvents());
        RoundManager.registerListener(new ClassManager());
        RoundManager.registerListener(new chat());
        RoundManager.registerListener(new FlyAndDoubleJump());
        RoundManager.registerListener(new SkybattleBotMovement());
    }

    private void registerBungeeListeners() {
        getServer().getMessenger().registerOutgoingPluginChannel(this, "BungeeCord");
        getServer().getMessenger().registerIncomingPluginChannel(this, "BungeeCord", new ChatPluginMessageListener());
    }

    private void registerBotTabCompletion() {
        // Register tab completion for bot usernames
        BotListener botListener = new BotListener();
        getCommand("tell").setTabCompleter(botListener);
        getCommand("reply").setTabCompleter(botListener);
        getCommand("botlink").setTabCompleter(botListener);
        getCommand("botsay").setTabCompleter(botListener);
        getCommand("refer").setTabCompleter(botListener);
        getCommand("player").setTabCompleter(botListener);
        getCommand("party").setTabCompleter(botListener);
    }

    private void runTasksAfterServerSetup() {
        new BukkitRunnable() {
            @Override
            public void run() {
                // For some reason the command "difficulty" isn't recognized until all plugins are loaded.
                // The Bukkit Scheduler only runs after all plugins are loaded.
                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "difficulty 1");
            }
        }.runTask(ServerLink.plugin);

        new BukkitRunnable() {
            @Override
            public void run() {
                // Reload holographic displays because some may have failed to load in unloaded worlds that another plugin just setup.
                Bukkit.getServer().dispatchCommand(Bukkit.getConsoleSender(), "hd reload");
            }
        }.runTaskLater(ServerLink.plugin, 60);
    }
}

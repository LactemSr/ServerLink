package serverlink.customevent;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import serverlink.bot.Bot;

public class CustomPlayerMessageBotEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private Bot bot;
    private String message;

    public CustomPlayerMessageBotEvent(Player player, Bot bot, String message) {
        this.player = player;
        this.bot = bot;
        this.message = message;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public Player getPlayer() {
        return player;
    }

    public Bot getBot() {
        return bot;
    }

    public String getMessage() {
        return message;
    }
}

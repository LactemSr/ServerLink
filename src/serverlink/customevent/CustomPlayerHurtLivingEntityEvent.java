package serverlink.customevent;

import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import serverlink.classes.Class;

public class CustomPlayerHurtLivingEntityEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled = false;
    private LivingEntity entity;
    private Player damager;
    private double damage;
    private Class damagerClass;

    public CustomPlayerHurtLivingEntityEvent(LivingEntity entity, Player damager, double damage, @Nullable Class damagerClass) {
        this.entity = entity;
        this.damager = damager;
        this.damage = damage;
        this.damagerClass = damagerClass;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public LivingEntity getEntity() {
        return entity;
    }

    public Player getDamager() {
        return damager;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double newDamage) {
        damage = newDamage;
    }

    public Class getDamagerClass() {
        return damagerClass;
    }
}

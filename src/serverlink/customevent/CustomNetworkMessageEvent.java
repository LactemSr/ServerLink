package serverlink.customevent;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CustomNetworkMessageEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private String message;

    public CustomNetworkMessageEvent(String message) {
        super(true);
        this.message = message;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public String getMessage() {
        return message;
    }
}

package serverlink.customevent;

import serverlink.classes.Class;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CustomChangeClassEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private Class oldClass;

    public CustomChangeClassEvent(Player player, Class oldClass) {
        this.player = player;
        this.oldClass = oldClass;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public Player getPlayer() {
        return player;
    }

    public Class getOldClass() {
        return oldClass;
    }
}

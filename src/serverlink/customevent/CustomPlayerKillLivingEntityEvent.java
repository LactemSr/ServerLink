package serverlink.customevent;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CustomPlayerKillLivingEntityEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private LivingEntity entity;


    public CustomPlayerKillLivingEntityEvent(Player player, LivingEntity entity) {
        this.player = player;
        this.entity = entity;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public Player getPlayer() {
        return player;
    }

    public LivingEntity getEntity() {
        return entity;
    }
}

package serverlink.customevent;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CustomPlayerDeathEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private String killer;

    public CustomPlayerDeathEvent(Player player, String killer) {
        this.player = player;
        this.killer = killer;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public String getKiller() {
        return killer;
    }

    public Player getPlayer() {
        return player;
    }
}

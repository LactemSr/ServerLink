package serverlink.customevent;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import serverlink.server.ServerType;

public class CustomGametypePluginLoadEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private ServerType servertype;

    public CustomGametypePluginLoadEvent(ServerType serverType) {
        this.servertype = serverType;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public ServerType getServertype() {
        return servertype;
    }
}

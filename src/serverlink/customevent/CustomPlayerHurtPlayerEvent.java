package serverlink.customevent;

import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import serverlink.classes.Class;

public class CustomPlayerHurtPlayerEvent extends Event implements Cancellable {
    private static final HandlerList handlers = new HandlerList();
    private boolean cancelled = false;
    private Player player;
    private Player damager;
    private double damage;
    private Class playerClass;
    private Class damagerClass;

    public CustomPlayerHurtPlayerEvent(Player player, Player damager, double damage, @Nullable Class playerClass, @Nullable Class damagerClass) {
        this.player = player;
        this.damager = damager;
        this.damage = damage;
        this.playerClass = playerClass;
        this.damagerClass = damagerClass;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    @Override
    public boolean isCancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Player getPlayer() {
        return player;
    }

    public Player getDamager() {
        return damager;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double newDamage) {
        this.damage = newDamage;
    }

    public Class getPlayerClass() {
        return playerClass;
    }

    public Class getDamagerClass() {
        return damagerClass;
    }
}

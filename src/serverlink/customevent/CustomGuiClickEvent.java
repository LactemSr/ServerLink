package serverlink.customevent;

import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class CustomGuiClickEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private String leftOrRight;
    private ItemStack item;
    private InventoryGui gui;
    private GuiPage page;
    private GuiRow row;
    private int slot;

    public CustomGuiClickEvent(Player player, String leftOrRight, ItemStack item, InventoryGui gui,
                               GuiPage page, GuiRow row, int slot) {
        this.player = player;
        this.leftOrRight = leftOrRight;
        this.item = item;
        this.gui = gui;
        this.page = page;
        this.row = row;
        this.slot = slot;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public int getSlot() {
        return slot;
    }

    public Player getPlayer() {
        return player;
    }

    public String getLeftOrRight() {
        return leftOrRight;
    }

    public ItemStack getItem() {
        return item;
    }

    public InventoryGui getGui() {
        return gui;
    }

    public GuiPage getPage() {
        return page;
    }

    public GuiRow getRow() {
        return row;
    }
}

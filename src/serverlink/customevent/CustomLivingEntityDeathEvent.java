package serverlink.customevent;

import org.bukkit.entity.LivingEntity;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

public class CustomLivingEntityDeathEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    LivingEntity entity;
    private String deathReason;
    private double health;

    public CustomLivingEntityDeathEvent(LivingEntity entity, String deathReason, double health) {
        this.entity = entity;
        this.deathReason = deathReason;
        this.health = health;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public LivingEntity getEntity() {
        return entity;
    }

    public String getDeathReason() {
        return deathReason;
    }

    public double getHealth() {
        return health;
    }
}

package serverlink.customevent;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.event.inventory.ClickType;

public class CustomNpcClickEvent extends Event {
    private static final HandlerList handlers = new HandlerList();
    private Player player;
    private int entityId;
    private ClickType clickType;

    public CustomNpcClickEvent(Player player, int entityId, ClickType clickType) {
        this.player = player;
        this.entityId = entityId;
        this.clickType = clickType;
    }

    public static HandlerList getHandlerList() {
        return handlers;
    }

    @Override
    public HandlerList getHandlers() {
        return handlers;
    }

    public Player getPlayer() {
        return player;
    }

    public int getEntityId() {
        return entityId;
    }

    public ClickType getClickType() {
        return clickType;
    }
}

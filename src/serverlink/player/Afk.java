package serverlink.player;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;
import org.bukkit.util.Vector;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.customevent.CustomGametypePluginLoadEvent;
import serverlink.server.Messages;
import serverlink.server.ServerType;

import java.util.HashMap;

public class Afk implements Listener {
    // there's no such thing as afk on AlphaCraft: you're either in hub (afk or not), or you're in-game
    // so kick to hub if afk is detected; don't make isAfk() method
    private static HashMap<Player, Location> locs = new HashMap<>();
    private static HashMap<Player, Integer> triggers = new HashMap<>();
    private static HashMap<Player, Integer> secondsUntilKick = new HashMap<>();
    private static HashMap<Player, Integer> ids = new HashMap<>();
    private static Integer taskId;

    private static void setup() {
        if (taskId != null) return;
        taskId = new BukkitRunnable() {
            @Override
            public void run() {
                for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
                    if (aPlayer.getRank() >= 9) continue;
                    Player p = aPlayer.getPlayer();
                    if (p.hasMetadata("NPC")) continue;
                    if (ids.containsKey(p)) continue;
                    // check location
                    int suspiciousness = 0;
                    if (Math.abs(p.getLocation().getX() - locs.get(p).getX()) < 2.5) {
                        if (Math.abs(p.getLocation().getY() - locs.get(p).getY()) < 1.5) {
                            if (Math.abs(p.getLocation().getZ() - locs.get(p).getZ()) < 2.5) {
                                suspiciousness += 3;
                            }
                        }
                    }
                    // check direction magnitudes
                    Vector dir1 = p.getLocation().getDirection();
                    Vector dir2 = locs.get(p).getDirection();
                    if (Math.abs(dir1.distance(dir2)) < 0.5) {
                        suspiciousness += 2;
                    }
                    // check direction angles
                    if (Math.abs(Math.toDegrees(dir1.angle(dir2))) < 20) {
                        suspiciousness += 1;
                    }
                    if (suspiciousness == 0) uncheck(p);
                    else checkPlayer(p, suspiciousness);
                    locs.put(p, p.getLocation());
                }
            }
        }.runTaskTimer(ServerLink.plugin, 20, 25 * 20).getTaskId();
    }

    private static void cancelTask() {
        if (taskId != null) Bukkit.getScheduler().cancelTask(taskId);
        taskId = null;
    }

    private static void checkPlayer(Player p, int suspiciousness) {
        if (ids.containsKey(p)) return;
        if (!triggers.containsKey(p)) triggers.put(p, 0);
        triggers.put(p, triggers.get(p) + suspiciousness);
        if (triggers.get(p) < 40) return;
        secondsUntilKick.put(p, 15);
        BukkitTask task = new BukkitRunnable() {
            @Override
            public void run() {
                if (!secondsUntilKick.containsKey(p)) {
                    cancel();
                    ids.remove(p);
                    return;
                }
                int suspicion = 0;
                if (Math.abs(p.getLocation().getX() - locs.get(p).getX()) < 0.8) {
                    if (Math.abs(p.getLocation().getY() - locs.get(p).getY()) < 0.2) {
                        if (Math.abs(p.getLocation().getZ() - locs.get(p).getZ()) < 0.8) {
                            suspicion += 3;
                        }
                    }
                }
                // check direction magnitudes
                Vector dir1 = p.getLocation().getDirection();
                Vector dir2 = locs.get(p).getDirection();
                if (Math.abs(dir1.distance(dir2)) < 0.5) {
                    suspicion += 2;
                }
                // check direction angles
                if (Math.abs(Math.toDegrees(dir1.angle(dir2))) < 10) {
                    suspicion += 1;
                }
                if (suspicion == 0) {
                    cancel();
                    triggers.remove(p);
                    secondsUntilKick.remove(p);
                    ids.remove(p);
                    return;
                }
                secondsUntilKick.put(p, secondsUntilKick.get(p) - 1);
                if (secondsUntilKick.get(p) == 0) {
                    p.performCommand("hub");
                    cancel();
                    triggers.remove(p);
                    secondsUntilKick.remove(p);
                    ids.remove(p);
                    return;
                }
                p.playSound(p.getLocation(), Sound.LAVA_POP, 1, 5);
                p.sendMessage(chat.color(chat.getServer() + Messages.afk_notify.replaceAll("%x%", secondsUntilKick.get(p).toString())));
            }
        }.runTaskTimer(ServerLink.plugin, 5, 20);
        ids.put(p, task.getTaskId());
    }

    private static void uncheck(Player p) {
        if (!triggers.containsKey(p)) triggers.put(p, 0);
        if (triggers.get(p) < 3) return;
        triggers.put(p, triggers.get(p) - 3);
    }

    public static void join(Player player) {
        locs.put(player, player.getLocation());
    }

    public static void leave(Player player) {
        locs.remove(player);
        triggers.remove(player);
        secondsUntilKick.remove(player);
        ids.remove(player);
    }

    @EventHandler
    void onTeleport(PlayerTeleportEvent e) {
        locs.put(e.getPlayer(), e.getTo());
    }

    @EventHandler
    private void onGametypeLoad(CustomGametypePluginLoadEvent e) {
        if (e.getServertype() == ServerType.hub) cancelTask();
        else setup();
    }
}
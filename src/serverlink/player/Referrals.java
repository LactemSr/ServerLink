package serverlink.player;

import mkremins.fanciful.FancyMessage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.customevent.CustomNetworkMessageEvent;
import serverlink.network.JedisPool;
import serverlink.network.nmessage;
import serverlink.player.stats.StatsManager;
import serverlink.server.Messages;
import serverlink.util.CommandText;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Referrals implements Listener {
    private final static HashMap<Integer, ArrayList<String>> rewardNames = new HashMap<Integer, ArrayList<String>>() {{
        put(1, new ArrayList<String>() {{
            add("10,000 KitPvP Tokens");
        }});
        put(3, new ArrayList<String>() {{
            add("30,000 COINS");
        }});
        put(3, new ArrayList<String>() {{
            add("30,000 Sky Gems");
        }});
        put(10, new ArrayList<String>() {{
            add("50,000 COINS");
        }});
        put(10, new ArrayList<String>() {{
            add("50,000 KitPvP Tokens");
        }});
        put(15, new ArrayList<String>() {{
            add("100,000 COINS");
        }});
        put(15, new ArrayList<String>() {{
            add("100,000 Sky Gems");
        }});
        put(15, new ArrayList<String>() {{
            add("5 LEGENDARY CHESTS");
        }});
    }};

    public static void actionSendReferral(AlphaPlayer aPlayer, UUID receiver) {
        aPlayer.getPlayer().sendMessage(chat.color(Messages.refer_sent.replace("%name%",
                ServerLink.getNameFromAlphaPlayer(aPlayer, receiver))));
        getSentReferrals(aPlayer).add(receiver);
        Jedis jedis = JedisPool.getConn();
        jedis.lpush("sentReferrals:" + aPlayer.getUuid().toString(), receiver.toString());
        jedis.lpush("receivedReferrals:" + receiver.toString(), aPlayer.getUuid().toString());
        if (jedis.hget(receiver.toString(), "online").equals("true")) {
            nmessage.sendMessage(jedis.hget(receiver.toString(), "server"), Settings.listToString("referrals",
                    "receiveReferral", receiver.toString(), aPlayer.getUuid().toString()), jedis);
        }
        JedisPool.close(jedis);
        clearCachedGuis(aPlayer);
    }

    public static void actionAcceptReferral(AlphaPlayer aPlayer, UUID sender) {
        getReceivedReferrals(aPlayer).clear();
        getSentReferrals(aPlayer).remove(sender);
        Jedis jedis = JedisPool.getConn();
        jedis.del("receivedReferrals:" + aPlayer.getUuid().toString());
        jedis.lrem("sentReferrals:" + sender.toString(), 0, aPlayer.getUuid().toString());
        jedis.lpush("acceptedReferrals:" + sender.toString(), aPlayer.getUuid().toString());
        aPlayer.setSetting("referrer", sender.toString());
        aPlayer.getData("referrer").setValue(sender.toString());
        aPlayer.getData("isReferred").setValue(true);
        if (jedis.hget(sender.toString(), "online").equals("true")) {
            nmessage.sendMessage(jedis.hget(sender.toString(), "server"), Settings.listToString("referrals",
                    "haveReferralAccepted", sender.toString(), aPlayer.getUuid().toString()), jedis);
        }
        // give prizes to each player
        giveRewards(sender, jedis.lrange("acceptedReferrals:" + sender.toString(), 0, -1).size(), false);
        JedisPool.close(jedis);
        giveRewards(aPlayer.getUuid(), getAcceptedReferrals(aPlayer).size(), true);
        sendRewardMessage(aPlayer);
        clearCachedGuis(aPlayer);
    }

    public static boolean isReferred(AlphaPlayer aPlayer) {
        return (boolean) aPlayer.getData("isReferred").getValue();
    }

    public static List<UUID> getAcceptedReferrals(AlphaPlayer aPlayer) {
        return (List<UUID>) aPlayer.getData("acceptedReferrals").getValue();
    }

    public static List<UUID> getSentReferrals(AlphaPlayer aPlayer) {
        return (List<UUID>) aPlayer.getData("sentReferrals").getValue();
    }

    public static List<UUID> getReceivedReferrals(AlphaPlayer aPlayer) {
        return (List<UUID>) aPlayer.getData("receivedReferrals").getValue();
    }

    public static boolean contains(List<UUID> list, UUID uuid) {
        for (UUID uuid1 : list) if (uuid1.equals(uuid)) return true;
        return false;
    }

    private static void receiveReferral(AlphaPlayer aPlayer, UUID sender) {
        getReceivedReferrals(aPlayer).add(sender);
        String senderName = ServerLink.getNameFromAlphaPlayer(aPlayer, sender);
        FancyMessage message = new FancyMessage("Click").color(ChatColor.GREEN).then(" here ").color(ChatColor.GOLD)
                .style(ChatColor.BOLD).tooltip("Click to accept " + senderName + "'s referral request.")
                .command("/refer accept " + senderName).then("to accept ").color(ChatColor.GREEN)
                .then(senderName + "'s").color(ChatColor.DARK_GREEN).then(" referral request. This will give you " +
                        "both a reward, but it can only be done once.");
        CommandText.sendMessage(aPlayer.getPlayer(), message.toJSONString());
        clearCachedGuis(aPlayer);
    }

    private static void haveReferralAccepted(AlphaPlayer aPlayer, UUID acceptor) {
        StatsManager.incrIncreasingStat(aPlayer.getUuid(), "referrals", 1);
        getReceivedReferrals(aPlayer).remove(acceptor);
        getSentReferrals(aPlayer).remove(acceptor);
        getAcceptedReferrals(aPlayer).add(acceptor);
        sendRewardMessage(aPlayer);
        clearCachedGuis(aPlayer);
    }

    private static void clearCachedGuis(AlphaPlayer aPlayer) {
        aPlayer.getData("referMenuGui").setValue(null);
        aPlayer.getData("referReceivedGui").setValue(null);
        aPlayer.getData("referRewardsGui").setValue(null);
    }

    private static void sendRewardMessage(AlphaPlayer aPlayer) {
        int count = getAcceptedReferrals(aPlayer).size();
        if (count == 0) count = 1;
        int tier = 1;
        if (count >= 15) tier = 15;
        else if (count >= 10) tier = 10;
        else if (count >= 3) tier = 3;
        Player p = aPlayer.getPlayer();
        p.sendMessage(chat.color("&a  ----- &6&lReferral Rewards&r&a -----"));
        for (String reward : rewardNames.get(tier)) p.sendMessage(chat.color("- &c" + reward));
        if (count == 3) p.sendMessage(chat.color("- &cDOG PET"));
        else if (count == 10) p.sendMessage(chat.color("- " + Permissions.getRankName(2) + "&c (3 months)!!!"));
        p.sendMessage("");
    }

    private static void giveRewards(UUID player, int acceptedReferrals, boolean playerOnline) {
        if (acceptedReferrals == 0) acceptedReferrals = 1;
        int tier = 1;
        if (acceptedReferrals >= 15) tier = 15;
        else if (acceptedReferrals >= 10) tier = 10;
        else if (acceptedReferrals >= 3) tier = 3;
        for (String reward : rewardNames.get(tier)) {
            reward = reward.toLowerCase().replaceAll(",", "");
            if (reward.contains("kitpvp token")) {
                int amount = Integer.parseInt(reward.replace("kitpvp tokens", "").replace("kitpvp token", "").replaceAll(" ", ""));
                if (playerOnline) PlayerManager.get(player).addPoints(amount);
                else Points.addPoints(player.toString(), "kitpvp", amount);
            }
            if (reward.contains("sky gem")) {
                int amount = Integer.parseInt(reward.replace("sky gems", "").replace("sky gem", "").replaceAll(" ", ""));
                if (playerOnline) PlayerManager.get(player).addPoints(amount);
                else Points.addPoints(player.toString(), "skyblock", amount);
            } else if (reward.contains("coin")) {
                int amount = Integer.parseInt(reward.replace("coins", "").replace("coin", "").replaceAll(" ", ""));
                if (playerOnline) PlayerManager.get(player).addCoins(amount);
                else Coins.addCoins(player.toString(), amount);
            } else if (reward.contains("legendary")) {
                TreasureManager.addLegendary(player.toString(), 5, 0);
                if (playerOnline)
                    Bukkit.getPlayer(player).sendMessage(chat.color(chat.getServer() + "&aYou received " +
                            "5 &e&lLEGENDARY CHESTS &r&abecause a referral request was accepted."));
                else {
                    FancyMessage notification = new FancyMessage("You received 5 ").color(ChatColor.GREEN)
                            .then("LEGENDARY CHESTS ").color(ChatColor.YELLOW).style(ChatColor.BOLD)
                            .then("because a referral request was accepted.").color(ChatColor.GREEN);
                    Notifications.sendFancyNotification(player.toString(), notification, 60 * 60 * 24 * 30 * 6);
                }
            }
        }
        if (acceptedReferrals == 3) {
            if (playerOnline) Permissions.addPerm(PlayerManager.get(player).getPlayer().getName(), "cosmetics.pet.dog");
            else
                Permissions.addPerm(ServerLink.getNameFromAlphaPlayer((AlphaPlayer) PlayerManager.getOnlineAlphaPlayers()
                        .toArray()[0], player), "cosmetics.pet.dog");
        } else if (acceptedReferrals == 10) {
            if (playerOnline) {
                RankExpiry.addTime(PlayerManager.get(player).getUuid().toString(), 2, 90);
            } else {
                String playerName = ServerLink.getNameFromAlphaPlayer((AlphaPlayer) PlayerManager.getOnlineAlphaPlayers()
                        .toArray()[0], player);
                RankExpiry.addTime(ServerLink.getUUIDFromName(playerName, false).toString(), 2, 90);
            }
        }
    }

    @EventHandler
    private void onNetworkMessage(CustomNetworkMessageEvent e) {
        List<String> l = Settings.stringToList(e.getMessage());
        if (!l.get(0).equals("referrals")) return;
        Player p = Bukkit.getPlayer(UUID.fromString(l.get(2)));
        if (p == null || !p.isOnline()) {
            Jedis jedis = JedisPool.getConn();
            if (jedis.hget(l.get(2), "online").equals("false")) {
                JedisPool.close(jedis);
                return;
            }
            String newServer = jedis.hget(l.get(2), "server");
            nmessage.sendMessage(newServer, Settings.listToString(l), jedis);
            JedisPool.close(jedis);
            return;
        }
        String message = l.get(1);
        new BukkitRunnable() {
            @Override
            public void run() {
                switch (message) {
                    case "receiveReferral":
                        receiveReferral(PlayerManager.get(p), UUID.fromString(l.get(3)));
                        return;
                    case "haveReferralAccepted":
                        haveReferralAccepted(PlayerManager.get(p), UUID.fromString(l.get(3)));
                        return;
                }
            }
        }.runTask(ServerLink.plugin);
    }
}

package serverlink.player;

import serverlink.ServerLink;
import serverlink.customevent.CustomFullyJoinEvent;
import serverlink.customevent.CustomGametypePluginLoadEvent;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.round.Round;
import me.libraryaddict.disguise.events.UndisguiseEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleFlightEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;

public class FlyAndDoubleJump implements Listener, Round {
    private static ArrayList pluginsWithFlight = new ArrayList<String>() {{
        add("spleef");
        add("skywars");
        add("skybattle");
        add("ds");
        add("hub");
    }};
    private static List<Player> cooldown = new ArrayList<>();
    private static boolean jumpAndFlyEnabled = false;

    @EventHandler
    void onDoubleSpacebar(PlayerToggleFlightEvent e) {
        if (!jumpAndFlyEnabled) return;
        // return if player is stopping flight
        if (!e.isFlying()) return;
        if (PlayerManager.get(e.getPlayer().getUniqueId()).getRank() >= 4) return;
        e.setCancelled(true);
        if (cooldown.contains(e.getPlayer())) return;
        e.getPlayer().setVelocity(e.getPlayer().getLocation().getDirection().clone()
                .setY(0).multiply(1.7).add(new Vector(0.0, 0.45, 0.0)));
        cooldown.add(e.getPlayer());
        new BukkitRunnable() {
            @Override
            public void run() {
                cooldown.remove(e.getPlayer());
            }
        }.runTaskLater(ServerLink.plugin, 20);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void unDisguise(UndisguiseEvent e) {
        if (e.isCancelled()) return;
        if (!(e.getEntity() instanceof Player)) return;
        ((Player) e.getEntity()).setAllowFlight(jumpAndFlyEnabled);
    }

    @Override
    public void onLobbyCountdownStart() {

    }

    @Override
    public void onLobbyCountdownCancel() {

    }

    @Override
    public void onMapCountdownStart() {
        jumpAndFlyEnabled = false;
        for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
            aPlayer.getPlayer().setAllowFlight(false);
        }
    }

    @Override
    public void onMapCountdownCancel() {
        jumpAndFlyEnabled = true;
        for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
            aPlayer.getPlayer().setAllowFlight(true);
        }
    }

    @Override
    public void onGameStart() {

    }

    @Override
    public void onEndCountdownStart() {

    }

    @Override
    public void onRoundEnd() {
        jumpAndFlyEnabled = true;
        for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
            aPlayer.getPlayer().setAllowFlight(true);
        }
    }

    @EventHandler
    private void onGametypeLoad(CustomGametypePluginLoadEvent e) {
        if (pluginsWithFlight.contains(e.getServertype())) {
            jumpAndFlyEnabled = true;
            for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
                aPlayer.getPlayer().setAllowFlight(true);
            }
        }
    }

    @EventHandler
    private void onCustomJoin(CustomFullyJoinEvent e) {
        e.getPlayer().getPlayer().setAllowFlight(jumpAndFlyEnabled);
    }
}

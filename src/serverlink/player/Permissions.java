package serverlink.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.customevent.CustomNetworkMessageEvent;
import serverlink.network.JedisPool;
import serverlink.network.nmessage;
import serverlink.util.UUIDFetcher;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class Permissions implements Listener {
    /*
     * group 1: defaultgroup 2: first rankgroup 3: second rankgroup 4: third
	 * rankgroup 5: fourth rankgroup 6: youtubergroup group 7: trial
	 * moderatorgroup 8: moderatorgroup 9: administratorgroup 10: owner
	 */

    public static HashMap<Integer, List<String>> permissionsGroups = new HashMap<>();
    private static HashMap<Integer, String> rankNames = new HashMap<>();

    @SuppressWarnings("deprecation")
    public static void setupGroups() {
        rankNames.put(1, "&7Default");
        rankNames.put(2, "&a&lBOSS");
        rankNames.put(3, "&d&lEPIC");
        rankNames.put(4, "&9&lELITE");
        rankNames.put(5, "&c&lSAINT");
        rankNames.put(6, "&c&lY&f&lT");
        rankNames.put(7, "&b&lTRIAL MOD");
        rankNames.put(8, "&b&lMODERATOR");
        rankNames.put(9, "&4&lADMIN");
        rankNames.put(10, "&6&lOWNER");
        // Load each group's permissions every ten minutes
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                for (Integer i = 1; i <= 10; i++) {
                    permissionsGroups.put(i, jedis.lrange("group:" + i, 0, -1));
                }
                JedisPool.close(jedis);
            }
        }.runTaskTimerAsynchronously(ServerLink.plugin, 0, 120000);
    }

    public static void shutDown() {
        for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers())
            aPlayer.getAttachment().remove();
    }

    public static List<String> getOfflinePermissions(UUID uuid) {
        Jedis jedis = JedisPool.getConn();
        List<String> perms = jedis.lrange("perms:" + uuid.toString(), 0, -1);
        JedisPool.close(jedis);
        return perms;
    }

    public static String addPerm(String playerName, String perm) {
        String uuid;
        String name;
        Player player = Bukkit.getPlayerExact(playerName);
        if (player != null && player.isOnline()) {
            uuid = player.getUniqueId().toString();
            name = player.getName();
        } else {
            Jedis jedis = JedisPool.getConn();
            uuid = jedis.get(playerName.toLowerCase());
            if (uuid != null && uuid.length() > 8) {
                name = jedis.hget(uuid, "username");
                JedisPool.close(jedis);
            } else {
                JedisPool.close(jedis);
                try {
                    List<String> uuidAndName = UUIDFetcher.getOfflineUUIDAndName(playerName.toLowerCase());
                    if (uuidAndName != null) {
                        uuid = uuidAndName.get(0);
                        name = uuidAndName.get(1);
                    } else {
                        return "That player does not exist!";
                    }
                } catch (Exception e) {
                    return "That player does not exist!";
                }
            }
        }
        Jedis jedis = JedisPool.getConn();
        List<String> currentPerms = jedis.lrange("perms:" + uuid, 0, -1);
        if (currentPerms.contains(perm)) {
            JedisPool.close(jedis);
            return ("That player already has this permission.");
        }
        if (perm.contains("rank.")) return "Use the /ranktime command instead.";
        jedis.lpush("perms:" + uuid, perm);
        JedisPool.close(jedis);
        if (player != null && player.isOnline()) {
            currentPerms.add(perm);
            PlayerManager.get(player).getAttachment().unsetPermission(perm);
            if (perm.startsWith("-"))
                PlayerManager.get(player).getAttachment().setPermission(perm, false);
            else PlayerManager.get(player).getAttachment().setPermission(perm, true);
        } else {
            jedis = JedisPool.getConn();
            nmessage.sendMessage(jedis.hget(uuid, "server"), "reloadPermissions:" + uuid, jedis);
            JedisPool.close(jedis);
        }
        return ("Permission \"" + perm + "\" added for " + name + ". This will take effect immediately.");
    }

    public static String removePerm(String playerName, String perm) {
        if (perm.startsWith("rank.")) return "Use the /ranktime command instead.";
        String uuid;
        String name;
        Player player = Bukkit.getPlayerExact(playerName);
        Jedis jedis = JedisPool.getConn();
        if (player != null && player.isOnline()) {
            uuid = player.getUniqueId().toString();
            name = player.getName();
        } else {
            uuid = jedis.get(playerName.toLowerCase());
            if (uuid != null && uuid.length() > 8) {
                name = jedis.hget(uuid, "username");
                JedisPool.close(jedis);
            } else {
                try {
                    List<String> uuidAndName = UUIDFetcher.getOfflineUUIDAndName(playerName.toLowerCase());
                    if (uuidAndName != null) {
                        uuid = uuidAndName.get(0);
                        name = uuidAndName.get(1);
                    } else {
                        JedisPool.close(jedis);
                        return "That player does not exist!";
                    }
                } catch (Exception e) {
                    JedisPool.close(jedis);
                    return "That player does not exist!";
                }
            }
        }
        List<String> currentPerms = jedis.lrange("perms:" + uuid, 0, -1);
        if (!currentPerms.contains(perm)) {
            JedisPool.close(jedis);
            return ("You can't remove that permission because the player doesn't have it. If you want to negate a permission, type /perm add <player> -<perm>");
        }
        jedis.lrem("perms:" + uuid, 0, perm);
        JedisPool.close(jedis);
        if (player != null && player.isOnline()) {
            currentPerms.remove(perm);
            PlayerManager.get(player).getAttachment().unsetPermission(perm);
        } else {
            jedis = JedisPool.getConn();
            nmessage.sendMessage(jedis.hget(uuid, "server"), "reloadPermissions:" + uuid, jedis);
            JedisPool.close(jedis);
        }
        return ("Permission \"" + perm + "\" removed for " + name + ". This will take immediately.");
    }


    public static String getRankName(int rank) {
        return rankNames.get(rank);
    }

    public static void reloadPermissions(Player player) {
        Jedis jedis = JedisPool.getConn();
        List<String> perms = jedis.lrange("perms:" + player.getUniqueId().toString(), 0, -1);
        String chatFormatString = jedis.hget("settings:" + player.getUniqueId().toString(), "chatFormat");
        String nameFormatString = jedis.hget("settings:" + player.getUniqueId().toString(), "nameFormat");
        JedisPool.close(jedis);
        int rank = 0;
        ArrayList<String> allowedPerms = new ArrayList<>();
        ArrayList<String> deniedPerms = new ArrayList<>();
        for (int i = 10; i > 0; i--) {
            if (!perms.contains("rank." + i)) if (i > 1) continue;
            if (rank < i) rank = i;
            for (int j = 1; j <= i; j++) {
                allowedPerms.add("rank." + j);
                for (String perm : Permissions.permissionsGroups.get(j)) {
                    if (perm.startsWith("-")) deniedPerms.add(perm.substring(1));
                    else allowedPerms.add(perm);
                }
            }
            break;
        }
        for (String perm : perms) {
            if (perm.startsWith("-")) deniedPerms.add(perm.substring(1));
            else allowedPerms.add(perm);
        }
        AlphaPlayer aPlayer = PlayerManager.get(player);
        aPlayer.getAttachment().remove();
        PermissionAttachment attachment = player.addAttachment(ServerLink.plugin);
        for (String perm : allowedPerms) attachment.setPermission(perm, true);
        for (String perm : deniedPerms) attachment.setPermission(perm, false);
        aPlayer.updatePermissions(attachment, allowedPerms, deniedPerms);
        aPlayer.setRank(rank);
        if (chatFormatString == null) chatFormatString = "";
        if (nameFormatString == null) nameFormatString = "";
        aPlayer.setSetting("chatFormat", chatFormatString);
        aPlayer.setSetting("nameFormat", nameFormatString);
        chat.calculateChatString(aPlayer);
        aPlayer.updateNameFormatting();
        aPlayer.getData("colorPickerGui").setValue("");
    }

    @EventHandler
    public void onNetworkMessage(CustomNetworkMessageEvent e) {
        String message = e.getMessage();
        if (message.startsWith("reloadPermissions:")) {
            UUID uuid = UUID.fromString(message.replace("reloadPermissions:", ""));
            Player player = Bukkit.getPlayer(uuid);
            if (player != null && player.isOnline()) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        reloadPermissions(player);
                    }
                }.runTask(ServerLink.plugin);
            } else {
                Jedis jedis = JedisPool.getConn();
                if (jedis.hget(uuid.toString(), "online").equals("true"))
                    nmessage.sendMessage(jedis.hget(uuid.toString(), "server"), "reloadPermissions:"
                            + uuid.toString(), jedis);
                JedisPool.close(jedis);
            }
        }
    }
}

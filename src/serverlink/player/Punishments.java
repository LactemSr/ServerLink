package serverlink.player;


import serverlink.chat.chat;
import serverlink.network.JedisPool;
import serverlink.util.CachedDecimalFormat;
import serverlink.util.TimeUtils;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent.Result;
import redis.clients.jedis.Jedis;

public class Punishments implements Listener {

    public static String format(Integer secs) {
        String s;
        if (secs < 60)
            s = secs.toString() + " seconds";
        else if (secs < 3600)
            s = ((Double) (secs / 60.0)).intValue() + " minutes";
        else if (secs < 86400)
            s = CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(secs / 3600.0) + " hours";
        else
            s = CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(secs / 86400.0) + " days";
        return s;
    }

    @EventHandler(priority = EventPriority.NORMAL)
    void onPreJoin(AsyncPlayerPreLoginEvent e) {
        Jedis jedis = JedisPool.getConn();
        String ban = jedis.hget(e.getUniqueId().toString(), "ban");
        JedisPool.close(jedis);
        if (ban == null || TimeUtils.getCurrent() > Integer.parseInt(ban.split(":")[1])) return;
        e.setLoginResult(Result.KICK_BANNED);
        e.setKickMessage(chat.color("&fYou are banned for &a"
                + Punishments.format(Integer.parseInt(ban.split(":")[1]) - TimeUtils.getCurrent())
                + " for &c" + ban.split(":")[0] + "."));
    }
}

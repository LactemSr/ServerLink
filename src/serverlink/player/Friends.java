package serverlink.player;

import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.customevent.CustomNetworkMessageEvent;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.network.JedisPool;
import serverlink.network.nmessage;
import serverlink.server.Messages;
import serverlink.util.CommandText;
import mkremins.fanciful.FancyMessage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class Friends implements Listener {
    public static void setup() {
        for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
            for (UUID uuid : getReceivedRequestsObject(aPlayer).keySet()) {
                int time = getReceivedRequestsObject(aPlayer).get(uuid);
                if (time <= 0) {
                    getReceivedRequestsObject(aPlayer).remove(uuid);
                    expireReceivedFriendRequest(aPlayer.getPlayer(), uuid);
                    Jedis jedis = JedisPool.getConn();
                    jedis.hdel("receivedFriendRequests:" + aPlayer.getPlayer().getUniqueId().toString(),
                            uuid.toString());
                    JedisPool.close(jedis);
                } else getReceivedRequestsObject(aPlayer).put(uuid, time - 1);
            }
            for (UUID uuid : getSentRequestsObject(aPlayer).keySet()) {
                int time = getSentRequestsObject(aPlayer).get(uuid);
                if (time <= 0) {
                    getSentRequestsObject(aPlayer).remove(uuid);
                    expireSentFriendRequest(aPlayer.getPlayer(), uuid);
                    Jedis jedis = JedisPool.getConn();
                    jedis.hdel("sentFriendRequests:" + aPlayer.getPlayer().getUniqueId().toString(),
                            uuid.toString());
                    JedisPool.close(jedis);
                } else getSentRequestsObject(aPlayer).put(uuid, time - 1);
            }
        }
    }

    public static void displayFriends(Player p, int page) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        if ((boolean) aPlayer.getData("displayFriendsProcessing", false).getValue()) return;
        aPlayer.getData("displayFriendsProcessing").setValue(true);
        Jedis jedis = JedisPool.getConn();
        List<FancyMessage> messages = new ArrayList<>();
        int index = (page * 10) - 10;
        List<UUID> friends = getFriendsObject(aPlayer);
        int stop = index + 9;
        // the '.0' in 10 is necessary
        int maxPages = (int) Math.ceil(friends.size() / 10.0);
        // top line with back/forward buttons
        messages.add(new FancyMessage(""));
        FancyMessage line1 = new FancyMessage("  ---- ").color(ChatColor.BLUE).style(ChatColor.BOLD);
        if (page > 1)
            line1.then(" << ").color(ChatColor.GRAY).tooltip("Previous Page").command("/friends list " + (page - 1));
        else line1.then(" ");
        line1.then("Friends (page " + page + ")").color(ChatColor.LIGHT_PURPLE).style(ChatColor.BOLD);
        if (page != maxPages)
            line1.then(" >> ").color(ChatColor.GRAY).tooltip("Next Page").command("/friends list " + (page + 1));
        else line1.then(" ");
        line1.then(" ----").color(ChatColor.BLUE).style(ChatColor.BOLD);
        messages.add(line1);
        while (index < stop) {
            // show two friends on this line
            if (friends.size() >= index + 1 + 2) {
                String p1Name = ServerLink.getNameFromAlphaPlayer(aPlayer, friends.get(index));
                ChatColor p1Color = ChatColor.RED;
                String p1Hover = "Offline";
                ChatColor p1HoverColor = ChatColor.GRAY;
                boolean p1Command = false;
                if (jedis.hget(friends.get(index).toString(), "online").equals("true")) {
                    p1Color = ChatColor.GREEN;
                    p1Hover = "Connected to your server.";
                    if (!jedis.hget(friends.get(index).toString(), "server").equals(ServerLink.getServerName())) {
                        p1HoverColor = ChatColor.GREEN;
                        p1Hover = "Click to teleport to your friend's server.";
                        p1Command = true;
                    }
                }
                String p2Name = ServerLink.getNameFromAlphaPlayer(aPlayer, friends.get(index + 1));
                ChatColor p2Color = ChatColor.RED;
                String p2Hover = "Offline";
                ChatColor p2HoverColor = ChatColor.GRAY;
                boolean p2Command = false;
                if (jedis.hget(friends.get(index).toString(), "online").equals("true")) {
                    p2Color = ChatColor.GREEN;
                    p2Hover = "Connected to your server.";
                    if (!jedis.hget(friends.get(index).toString(), "server").equals(ServerLink.getServerName())) {
                        p2HoverColor = ChatColor.GREEN;
                        p2Hover = "Click to teleport to your friend's server.";
                        p2Command = true;
                    }
                }
                FancyMessage fancy = new FancyMessage(" ").color(ChatColor.GOLD);
                fancy.then(p1Name).style(ChatColor.BOLD).color(p1Color);
                fancy.tooltip(p1HoverColor + p1Hover);
                if (p1Command) fancy.command("/server " + jedis.hget(friends.get(index).toString(), "server"));
                fancy.then(fill(19 - (p1Name.length() + 1))).color(ChatColor.GOLD);
                messages.add(fancy);
                fancy = new FancyMessage("  ").color(ChatColor.GOLD);
                fancy.then(p2Name).style(ChatColor.BOLD).color(p2Color);
                fancy.tooltip(p2HoverColor + p2Hover);
                if (p2Command) fancy.command("/server " + jedis.hget(friends.get(index).toString(), "server"));
                fancy.then(fill(36 - (p1Name.length() + 3 + p2Name.length()))).color(ChatColor.GOLD);
                messages.add(fancy);
                index += 2;
            }
            // show one friend on this line
            else if (friends.size() == index + 1) {
                String p1Name = ServerLink.getNameFromAlphaPlayer(aPlayer, friends.get(index));
                ChatColor p1Color = ChatColor.RED;
                String p1Hover = "Offline";
                ChatColor p1HoverColor = ChatColor.GRAY;
                boolean p1Command = false;
                if (jedis.hget(friends.get(index).toString(), "online").equals("true")) {
                    p1Color = ChatColor.GREEN;
                    p1Hover = "Connected to your server.";
                    if (!jedis.hget(friends.get(index).toString(), "server").equals(ServerLink.getServerName())) {
                        p1HoverColor = ChatColor.GREEN;
                        p1Hover = "Click to teleport to your friend's server.";
                        p1Command = true;
                    }
                }
                FancyMessage fancy = new FancyMessage(" ").color(ChatColor.GOLD);
                fancy.then(p1Name).style(ChatColor.BOLD).color(p1Color);
                fancy.tooltip(p1HoverColor + p1Hover);
                if (p1Command) fancy.command("/server " + jedis.hget(friends.get(index).toString(), "server"));
                fancy.then(fill(36 - (p1Name.length() + 1))).color(ChatColor.GOLD);
                messages.add(fancy);
                index++;
            } else break;
        }
        JedisPool.close(jedis);
        if (messages.size() == 2)
            messages.add(new FancyMessage("No friends to show on this page.").color(ChatColor.GRAY));
        messages.add(new FancyMessage("    ------- ------ ------ ------").color(ChatColor.BLUE).style(ChatColor.BOLD));
        messages.add(new FancyMessage(""));
        for (FancyMessage message : messages) CommandText.sendMessage(p, message.toJSONString());
        new BukkitRunnable() {
            @Override
            public void run() {
                aPlayer.getData("displayFriendsProcessing").setValue(false);
            }
        }.runTaskLater(ServerLink.plugin, 30);
    }

    private static String fill(int spaces) {
        String s = "";
        for (int i = 0; i < spaces; i++) {
            s = s + " ";
        }
        return s;
    }

    public static void displayRequests(Player p) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        if (getReceivedRequestsObject(aPlayer).isEmpty()) {
            p.sendMessage("");
            p.sendMessage(chat.color("&9&l  ---- &d&lFriend Requests&9&l ----"));
            p.sendMessage(chat.color("&7No friend requests to show."));
            p.sendMessage(chat.color("&9&l    ------- ------ ------ ------"));
            p.sendMessage("");
            return;
        }
        List<FancyMessage> messages = new ArrayList<>();
        FancyMessage fancy = new FancyMessage("  ---- ").color(ChatColor.BLUE).style(ChatColor.BOLD);
        fancy.then("Friend Requests").color(ChatColor.LIGHT_PURPLE).style(ChatColor.BOLD);
        fancy.then(" ----").color(ChatColor.BLUE).style(ChatColor.BOLD);
        messages.add(fancy);
        List<UUID> uuids = new ArrayList<>();
        uuids.addAll(getReceivedRequestsObject(aPlayer).keySet());
        for (int index = 1; index <= 7; index++) {
            if (index == 7 && getReceivedRequestsObject(aPlayer).size() > 7) {
                messages.add(new FancyMessage("And " + (getReceivedRequestsObject(aPlayer).size() - 6) + " more...").color(ChatColor.GOLD));
                break;
            }
            String friendName = ServerLink.getNameFromAlphaPlayer(aPlayer, uuids.get(index - 1));
            fancy = new FancyMessage(friendName).color(ChatColor.GOLD);
            // accept
            fancy.then("[").color(ChatColor.GRAY).then("ACCEPT").color(ChatColor.DARK_GREEN).style(ChatColor.BOLD);
            fancy.command("/friends accept " + friendName).then("] ").color(ChatColor.GRAY);
            fancy.tooltip(ChatColor.GRAY + "Click to Accept");
            // deny
            fancy.then("[").color(ChatColor.GRAY).then("DENY").color(ChatColor.DARK_RED).style(ChatColor.BOLD);
            fancy.command("/friends deny " + friendName).then("]").color(ChatColor.GRAY);
            fancy.tooltip(ChatColor.GRAY + "Click to Decline");
            CommandText.sendMessage(p, fancy.toJSONString());
            messages.add(fancy);
        }
        messages.add(new FancyMessage("    ------- ------ ------ ------").color(ChatColor.BLUE).style(ChatColor.BOLD));
        for (FancyMessage message : messages) CommandText.sendMessage(p, message.toJSONString());
    }

    public static void actionInviteFriend(Player p, UUID friend) {
        inviteFriend(p, friend);
        String now = Long.toString(Instant.now().getEpochSecond());
        Jedis jedis = JedisPool.getConn();
        jedis.hset("sentFriendRequests:" + p.getUniqueId().toString(), friend.toString(), now);
        jedis.hset("receivedFriendRequests:" + friend.toString(), p.getUniqueId().toString(), now);
        if (jedis.hget(friend.toString(), "online").equals("true")) {
            nmessage.sendMessage(jedis.hget(friend.toString(), "server"), Settings.listToString("friends",
                    "receiveInvite", friend.toString(), p.getUniqueId().toString()), jedis);
        } else {
            FancyMessage fancy = new FancyMessage("Friend request from ").color(ChatColor.GREEN);
            fancy.then(p.getName()).color(ChatColor.GOLD).then(" ").then("[ACCEPT]").color(ChatColor.DARK_GREEN);
            fancy.style(ChatColor.BOLD).command("/friends accept " + p.getName());
            fancy.tooltip(ChatColor.GRAY + "Click to Accept").then(" ").then("[DENY]").color(ChatColor.DARK_RED);
            fancy.style(ChatColor.BOLD).command("/friends deny " + p.getName());
            fancy.tooltip(ChatColor.GRAY + "Click to Decline");
            Notifications.sendFancyNotification(friend.toString(), fancy, 60 * 5);
        }
        JedisPool.close(jedis);
    }

    public static void actionAcceptFriend(Player p, UUID friend) {
        addFriend(p, friend);
        Jedis jedis = JedisPool.getConn();
        jedis.lpush("friends:" + p.getUniqueId().toString(), friend.toString());
        jedis.hdel("receivedFriendRequests:" + p.getUniqueId().toString(), friend.toString());
        jedis.lpush("friends:" + friend.toString(), p.getUniqueId().toString());
        jedis.hdel("sentFriendRequests:" + p.getUniqueId().toString(), friend.toString());
        if (jedis.hget(friend.toString(), "online").equals("true")) {
            nmessage.sendMessage(jedis.hget(friend.toString(), "server"), Settings.listToString("friends",
                    "add", friend.toString(), p.getUniqueId().toString()), jedis);
        } else {
            Notifications.sendNotification(friend.toString(), "&6" + p.getName() + "&a accepted your friend request.", 60 * 60 * 24 * 365);
        }
        JedisPool.close(jedis);
    }

    public static void actionRemoveFriend(Player p, UUID friend) {
        removeFriend(p, friend);
        Jedis jedis = JedisPool.getConn();
        jedis.lrem("friends:" + p.getUniqueId().toString(), 0, friend.toString());
        if (jedis.hget(friend.toString(), "online").equals("true")) {
            nmessage.sendMessage(jedis.hget(friend.toString(), "server"), Settings.listToString("friends",
                    "remove", friend.toString(), p.getUniqueId().toString()), jedis);
        } else {
            Notifications.sendNotification(friend.toString(), "&6" + p.getName() + "&a is no longer your friend.", 60 * 60 * 24 * 365);
        }
        JedisPool.close(jedis);
    }

    public static void actionDenyFriendRequest(Player p, UUID friend) {
        denyFriendRequest(p, friend);
        Jedis jedis = JedisPool.getConn();
        jedis.lrem("receivedFriendRequests:" + p.getUniqueId().toString(), 0, friend.toString());
        jedis.lrem("sentFriendRequests:" + friend.toString(), 0, p.getUniqueId().toString());
        if (jedis.hget(friend.toString(), "online").equals("true")) {
            nmessage.sendMessage(jedis.hget(friend.toString(), "server"), Settings.listToString("friends",
                    "haveFriendRequestDenied", friend.toString(), p.getUniqueId().toString()), jedis);
        } else {
            Notifications.sendNotification(friend.toString(), "&6" + p.getName() + "&a denied your friend request.", 60 * 60 * 24 * 365);
        }
        JedisPool.close(jedis);
    }

    public static List<UUID> getFriendsObject(AlphaPlayer aPlayer) {
        return (List) aPlayer.getData("friends", new ArrayList<>()).getValue();
    }

    public static ConcurrentHashMap<UUID, Integer> getSentRequestsObject(AlphaPlayer aPlayer) {
        return (ConcurrentHashMap) aPlayer.getData("sentFriendRequests", new ConcurrentHashMap<>()).getValue();
    }

    public static ConcurrentHashMap<UUID, Integer> getReceivedRequestsObject(AlphaPlayer aPlayer) {
        return (ConcurrentHashMap) aPlayer.getData("receivedFriendRequests", new ConcurrentHashMap<>()).getValue();
    }


    /*











     */


    private static void addFriend(Player p, UUID friend) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        getFriendsObject(aPlayer).add(friend);
        getSentRequestsObject(aPlayer).remove(friend);
        getReceivedRequestsObject(aPlayer).remove(friend);
        p.sendMessage(chat.color(chat.getServer() + Messages.friends_add.replace("%player%",
                ServerLink.getNameFromAlphaPlayer(aPlayer, friend))));
    }

    private static void removeFriend(Player p, UUID friend) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        getFriendsObject(aPlayer).remove(friend);
        p.sendMessage(chat.color(chat.getServer() + Messages.friends_remove.replace("%player%",
                ServerLink.getNameFromAlphaPlayer(aPlayer, friend))));
    }

    private static void expireSentFriendRequest(Player p, UUID friend) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        getSentRequestsObject(aPlayer).remove(friend);
        p.sendMessage(chat.color(chat.getServer() + Messages.friends_sentInviteExpired.replace(
                "%player%", ServerLink.getNameFromAlphaPlayer(aPlayer, friend))));
    }

    private static void expireReceivedFriendRequest(Player p, UUID friend) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        getReceivedRequestsObject(aPlayer).remove(friend);
        p.sendMessage(chat.color(chat.getServer() + Messages.friends_receivedInviteExpired.replace(
                "%player%", ServerLink.getNameFromAlphaPlayer(aPlayer, friend))));
    }

    private static void inviteFriend(Player p, UUID friend) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        getSentRequestsObject(aPlayer).put(friend, 60 * 5);
        p.sendMessage(chat.color(chat.getServer() + Messages.friends_inviteSent.replace(
                "%player%", ServerLink.getNameFromAlphaPlayer(aPlayer, friend))));
    }

    private static void denyFriendRequest(Player p, UUID friend) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        getReceivedRequestsObject(aPlayer).remove(friend);
        p.sendMessage(chat.color(chat.getServer() + Messages.friends_deny.replace(
                "%player%", ServerLink.getNameFromAlphaPlayer(aPlayer, friend))));
    }

    private static void haveFriendRequestDenied(Player p, UUID friend) {
        getSentRequestsObject(PlayerManager.get(p)).remove(friend);
        // don't send a message saying they got denied, they might re-invite
    }

    private static void receiveFriendInvite(Player p, UUID friend) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        getReceivedRequestsObject(aPlayer).put(friend, 60 * 5);
        String friendName = ServerLink.getNameFromAlphaPlayer(aPlayer, friend);
        FancyMessage fancy = new FancyMessage(friendName).color(ChatColor.GOLD);
        fancy.then(" just sent you a friend request. ").color(ChatColor.GREEN);
        fancy.then("\nClick to: ");
        // accept
        fancy.then("[ACCEPT]").color(ChatColor.DARK_GREEN).style(ChatColor.BOLD);
        fancy.command("/friends accept " + friendName);
        fancy.tooltip(ChatColor.GRAY + "Click to Accept").then("  or  ").color(ChatColor.GRAY);
        // deny
        fancy.then("[DENY]").color(ChatColor.DARK_RED).style(ChatColor.BOLD);
        fancy.command("/friends deny " + friendName);
        fancy.tooltip(ChatColor.GRAY + "Click to Decline");
        CommandText.sendMessage(p, fancy.toJSONString());
    }

    /*















     */

    @EventHandler
    private void onNetworkMessage(CustomNetworkMessageEvent e) {
        List<String> l = Settings.stringToList(e.getMessage());
        if (!l.get(0).equals("friends")) return;
        Player p = Bukkit.getPlayer(UUID.fromString(l.get(2)));
        if (p == null || !p.isOnline()) {
            Jedis jedis = JedisPool.getConn();
            if (jedis.hget(l.get(2), "online").equals("false")) {
                JedisPool.close(jedis);
                return;
            }
            String newServer = jedis.hget(l.get(2), "server");
            nmessage.sendMessage(newServer, Settings.listToString(l), jedis);
            JedisPool.close(jedis);
            return;
        }
        String message = l.get(1);
        new BukkitRunnable() {
            @Override
            public void run() {
                switch (message) {
                    case "add":
                        addFriend(p, UUID.fromString(l.get(3)));
                        return;
                    case "receiveInvite":
                        receiveFriendInvite(p, UUID.fromString(l.get(3)));
                        return;
                    case "remove":
                        removeFriend(p, UUID.fromString(l.get(3)));
                        return;
                    case "haveFriendRequestDenied":
                        haveFriendRequestDenied(p, UUID.fromString(l.get(3)));
                        return;
                }
            }
        }.runTask(ServerLink.plugin);
    }
}

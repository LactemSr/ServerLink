package serverlink.player.party;

import serverlink.ServerLink;
import serverlink.customevent.CustomNetworkMessageEvent;
import serverlink.network.JedisPool;
import serverlink.network.nmessage;
import serverlink.player.Settings;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;

import java.util.List;
import java.util.UUID;

public class PartyNetworkSync implements Listener {

    @EventHandler
    private void onNetworkMessage(CustomNetworkMessageEvent e) {
        List<String> l = Settings.stringToList(e.getMessage());
        if (!l.get(0).equals("party")) return;
        Player p = Bukkit.getPlayer(UUID.fromString(l.get(2)));
        if (p == null || !p.isOnline()) {
            Jedis jedis = JedisPool.getConn();
            if (jedis.hget(l.get(2), "online").equals("false")) {
                JedisPool.close(jedis);
                return;
            }
            String newServer = jedis.hget(l.get(2), "server");
            nmessage.sendMessage(newServer, Settings.listToString(l), jedis);
            JedisPool.close(jedis);
            return;
        }
        String message = l.get(1);
        new BukkitRunnable() {
            @Override
            public void run() {
                switch (message) {
                    case "notifyThatUserWasInvitedToParty":
                        PartyManager.notifyThatUserWasInvitedToParty(p, UUID.fromString(l.get(3)));
                        return;
                    case "notifyThatPartyWasDisbandedOnlyPlayer":
                        PartyManager.notifyThatPartyWasDisbandedOnlyPlayer(p);
                        return;
                    case "notifyThatUserWasKickedFromParty":
                        PartyManager.notifyThatUserWasKickedFromParty(p, UUID.fromString(l.get(3)));
                        return;
                    case "warp":
                        PartyManager.warp(p, l.get(3));
                        return;
                    case "notifyThatPartyWasDisbanded":
                        PartyManager.notifyThatPartyWasDisbanded(p);
                        return;
                    case "notifyThatUserLeftParty":
                        PartyManager.notifyThatUserLeftParty(p, UUID.fromString(l.get(3)));
                        return;
                    case "notifyThatUserJoinedParty":
                        PartyManager.notifyThatUserJoinedParty(p, UUID.fromString(l.get(3)));
                        return;
                    case "createParty":
                        PartyManager.createParty(p, UUID.fromString(l.get(3)));
                        return;
                    case "receiveInvite":
                        PartyManager.receiveInvite(p, UUID.fromString(l.get(3)));
                        return;
                    case "notifyThatPartySentInviteExpired":
                        PartyManager.notifyThatPartySentInviteExpired(p, UUID.fromString(l.get(3)));
                        return;
                    case "notifyKicked":
                        PartyManager.notifyKicked(p);
                        return;
                    default:
                        Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "UNKNOWN MESSAGE SENT TO PartyNetworkSync: " + message);
                }
            }
        }.runTask(ServerLink.plugin);
    }
}

package serverlink.player.party;

import mkremins.fanciful.FancyMessage;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.network.JedisOne;
import serverlink.network.JedisPool;
import serverlink.network.NetworkCache;
import serverlink.network.nmessage;
import serverlink.player.Settings;
import serverlink.server.Messages;
import serverlink.server.PluginMessaging;
import serverlink.util.CommandText;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class PartyManager {
    public static void setup() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
                    for (UUID uuid : getReceivedInvitesObject(aPlayer).keySet()) {
                        int time = getReceivedInvitesObject(aPlayer).get(uuid);
                        if (time <= 0) {
                            getReceivedInvitesObject(aPlayer).remove(uuid);
                            expireReceivedInvite(aPlayer.getPlayer(), uuid);
                            Jedis jedis = JedisPool.getConn();
                            jedis.hdel("receivedPartyInvites:" + aPlayer.getPlayer().getUniqueId().toString(),
                                    uuid.toString());
                            JedisPool.close(jedis);
                        } else getReceivedInvitesObject(aPlayer).put(uuid, time - 1);
                    }
                    for (UUID uuid : getSentInvitesObject(aPlayer).keySet()) {
                        int time = getSentInvitesObject(aPlayer).get(uuid);
                        if (time <= 0) {
                            getSentInvitesObject(aPlayer).remove(uuid);
                            expireSentInvite(aPlayer.getPlayer(), uuid);
                            Jedis jedis = JedisPool.getConn();
                            jedis.hdel("sentPartyInvites:" + aPlayer.getPlayer().getUniqueId().toString(),
                                    uuid.toString());
                            for (String member : jedis.lrange("partyMembers:" + aPlayer.getUuid().toString(), 0, -1)) {
                                jedis.hdel(member, "party");
                                if (jedis.hget(member, "online").equals("true")) {
                                    nmessage.sendMessage(jedis.hget(member, "server"), Settings.listToString("party",
                                            "notifyThatPartySentInviteExpired", member, uuid.toString()), jedis);
                                }
                            }
                            JedisPool.close(jedis);
                        } else getSentInvitesObject(aPlayer).put(uuid, time - 1);
                    }
                }
            }
        }.runTaskTimer(ServerLink.plugin, 20, 20);
    }

    // Parties are stored in db as lists --> list title: "partyMembers:leader's_uuid"   -->  the leader is not included in the list since he is the key

    public static void listPartyMembers(Player p) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        UUID leader = getPartyObject(aPlayer).get(0);
        Jedis jedis = JedisPool.getConn();
        String message = "&aLeader: &6" + ServerLink.getNameFromAlphaPlayer(aPlayer, leader);
        if (jedis.hget(leader.toString(), "online").equals("false")) message = message.concat(" &7(&coffline&7)");
        else if (!jedis.hget(leader.toString(), "server").equals(ServerLink.getServerName()))
            message = message.concat(" &7(&eplaying different server&7)");
        else message = message.concat(" &7(&aconnected to your server&7)");
        boolean skip = true;
        for (UUID member : getPartyObject(aPlayer)) {
            if (skip) {
                skip = false;
                continue;
            }
            message = message.concat("\n&a+ &6" + ServerLink.getNameFromAlphaPlayer(aPlayer, member));
            if (jedis.hget(member.toString(), "online").equals("false")) message = message.concat(" &7(&coffline&7)");
            else if (!jedis.hget(member.toString(), "server").equals(ServerLink.getServerName()))
                message = message.concat(" &7(&eplaying different server&7)");
            else message = message.concat(" &7(&aconnected to your server&7)");
        }
        JedisPool.close(jedis);
        p.sendMessage("");
        p.sendMessage(chat.color("&9&l  -------- &d&lParty Members&9&l --------"));
        p.sendMessage(chat.color(message));
        p.sendMessage(chat.color("&9&l    ------- ------ ------ ------"));
        p.sendMessage("");
    }

    public static void actionJoin(Player p, UUID leader) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        safelyAddPartyMember(aPlayer, leader);
        safelyAddPartyMember(aPlayer, p.getUniqueId());
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                for (String s : jedis.lrange("partyMembers:" + leader.toString(), 0, -1)) {
                    safelyAddPartyMember(aPlayer, UUID.fromString(s));
                }
                getSentInvitesObject(aPlayer).remove(leader);
                getReceivedInvitesObject(aPlayer).remove(leader);
                p.sendMessage(chat.color(chat.getServer() + Messages.party_success_join.replace("%leader%",
                        ServerLink.getNameFromAlphaPlayer(aPlayer, leader))));
                jedis.hset(p.getUniqueId().toString(), "party", leader.toString());
                jedis.lpush("partyMembers:" + leader.toString(), p.getUniqueId().toString());
                jedis.hdel("receivedPartyInvites:" + p.getUniqueId().toString(), leader.toString());
                jedis.hdel("sentPartyInvites:" + leader.toString(), p.getUniqueId().toString());
                List<String> members = jedis.lrange("partyMembers:" + leader.toString(), 0, -1);
                // the party is being created because this is the first member to join the leader's party
                if (members.size() == 1) {
                    jedis.hset(leader.toString(), "party", leader.toString());
                    if (jedis.hget(leader.toString(), "online").equals("true")) {
                        nmessage.sendMessage(jedis.hget(leader.toString(), "server"), Settings.listToString("party", "createParty",
                                leader.toString(), p.getUniqueId().toString()), jedis);
                    }
                } else {
                    for (String s : members) {
                        UUID member = UUID.fromString(s);
                        if (member.equals(p.getUniqueId())) continue;
                        if (jedis.hget(member.toString(), "online").equals("true")) {
                            nmessage.sendMessage(jedis.hget(member.toString(), "server"), Settings.listToString("party", "notifyThatUserJoinedParty",
                                    member.toString(), p.getUniqueId().toString()), jedis);
                        }
                    }
                }
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public static void actionLeave(Player p, UUID leader) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        getPartyObject(aPlayer).clear();
        new BukkitRunnable() {
            @Override
            public void run() {
                p.sendMessage(chat.color(chat.getServer() + Messages.party_success_leave.replace("%leader%",
                        ServerLink.getNameFromAlphaPlayer(aPlayer, leader))));
                Jedis jedis = JedisPool.getConn();
                jedis.hdel(p.getUniqueId().toString(), "party");
                jedis.lrem("partyMembers:" + leader.toString(), 0, p.getUniqueId().toString());
                List<String> partyMembers = jedis.lrange("partyMembers:" + leader.toString(), 0, -1);
                // tell other party members that the player left
                for (String member : partyMembers) {
                    if (member.equals(p.getUniqueId().toString())) continue;
                    if (jedis.hget(member, "online").equals("true")) {
                        nmessage.sendMessage(jedis.hget(member, "server"), Settings.listToString("party",
                                "notifyThatUserLeftParty", member, p.getUniqueId().toString()), jedis);
                    }
                }
                // disband party if the leader is the only one left
                if (partyMembers.isEmpty()) {
                    jedis.hdel(leader.toString(), "party");
                    jedis.del("partyMembers:" + leader.toString());
                    if (jedis.hget(leader.toString(), "online").equals("true")) {
                        nmessage.sendMessage(jedis.hget(leader.toString(), "server"), Settings.listToString("party",
                                "notifyThatPartyWasDisbandedOnlyPlayer", leader.toString()), jedis);
                    }
                }
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public static void actionDisband(Player p) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        getPartyObject(aPlayer).clear();
        getSentInvitesObject(aPlayer).clear();
        new BukkitRunnable() {
            @Override
            public void run() {
                p.sendMessage(chat.color(chat.getServer() + Messages.party_success_disband));
                Jedis jedis = JedisPool.getConn();
                jedis.hdel(p.getUniqueId().toString(), "party");
                for (String member : jedis.lrange("partyMembers:" + p.getUniqueId().toString(), 0, -1)) {
                    jedis.hdel(member, "party");
                    if (jedis.hget(member, "online").equals("true")) {
                        nmessage.sendMessage(jedis.hget(member, "server"), Settings.listToString("party",
                                "notifyThatPartyWasDisbanded", member), jedis);
                    }
                }
                jedis.del("partyMembers:" + p.getUniqueId().toString());
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public static void actionWarp(Player p) {
        p.sendMessage(chat.color(chat.getServer() + Messages.party_notify_warpLeader));
        Jedis jedis = JedisPool.getConn();
        for (UUID member : getPartyObject(PlayerManager.get(p))) {
            if (member.equals(p.getUniqueId())) continue;
            if (jedis.hget(member.toString(), "online").equals("false")) continue;
            nmessage.sendMessage(jedis.hget(member.toString(), "server"), Settings.listToString("party",
                    "warp", member.toString(), ServerLink.getServerName()), jedis);
        }
        JedisPool.close(jedis);
    }

    public static void actionKick(Player p, UUID playerToBeKicked) {
        notifyThatUserWasKickedFromParty(p, playerToBeKicked);
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                jedis.hdel(p.getUniqueId().toString(), "party");
                jedis.lrem("partyMembers:" + p.getUniqueId().toString(), 0, playerToBeKicked.toString());
                List<String> partyMembers = jedis.lrange("partyMembers:" + p.getUniqueId().toString(), 0, -1);
                safelyRemovePartyMember(PlayerManager.get(p), playerToBeKicked);
                // tell other party members that the player was kicked
                for (String member : partyMembers) {
                    if (member.equals(p.getUniqueId().toString())) continue;
                    if (member.equals(playerToBeKicked.toString())) continue;
                    if (jedis.hget(member, "online").equals("true")) {
                        nmessage.sendMessage(jedis.hget(member, "server"), Settings.listToString("party",
                                "notifyThatUserWasKickedFromParty", member, playerToBeKicked.toString()), jedis);
                    }
                }
                // disband party if the leader is the only one left
                if (partyMembers.size() == 1) {
                    jedis.hdel(p.getUniqueId().toString(), "party");
                    jedis.del("partyMembers:" + p.getUniqueId().toString());
                    getPartyObject(PlayerManager.get(p)).clear();
                    p.sendMessage(chat.color(chat.getServer() + Messages.party_update_disbandedOnlyPlayer));
                }
                // tell the player that he was kicked
                nmessage.sendMessage(jedis.hget(playerToBeKicked.toString(), "server"), Settings.listToString("party",
                        "notifyKicked", playerToBeKicked.toString()), jedis);
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public static void actionInvite(Player p, UUID invitee) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        getSentInvitesObject(aPlayer).put(invitee, 60 * 5);
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                String now = Long.toString(Instant.now().getEpochSecond());
                jedis.hset("sentPartyInvites:" + p.getUniqueId().toString(), invitee.toString(), now);
                jedis.hset("receivedPartyInvites:" + invitee.toString(), p.getUniqueId().toString(), now);
                if (jedis.hget(invitee.toString(), "online").equals("true")) {
                    nmessage.sendMessage(jedis.hget(invitee.toString(), "server"), Settings.listToString("party",
                            "receiveInvite", invitee.toString(), p.getUniqueId().toString()), jedis);
                }
                if (getPartyObject(aPlayer).isEmpty()) {
                    p.sendMessage(chat.color(chat.getServer() + Messages.party_success_invite.replace("%player%",
                            ServerLink.getNameFromAlphaPlayer(aPlayer, invitee))));
                } else {
                    p.sendMessage(chat.color(chat.getServer() + Messages.party_notify_inviteSent.replace("%player%",
                            ServerLink.getNameFromAlphaPlayer(aPlayer, invitee))));
                    // tell other party members that the player was invited
                    List<String> partyMembers = jedis.lrange("partyMembers:" + p.getUniqueId().toString(), 0, -1);
                    for (String member : partyMembers) {
                        if (member.equals(p.getUniqueId().toString())) continue;
                        if (jedis.hget(member, "online").equals("true")) {
                            nmessage.sendMessage(jedis.hget(member, "server"), Settings.listToString("party",
                                    "notifyThatUserWasInvitedToParty", member, invitee.toString()), jedis);
                        }
                    }
                }
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public static List<UUID> getPartyObject(AlphaPlayer aPlayer) {
        return (List<UUID>) aPlayer.getData("party", new ArrayList<>()).getValue();
    }

    public static void safelyAddPartyMember(AlphaPlayer aPlayer, UUID member) {
        List<UUID> members = new ArrayList<>(getPartyObject(aPlayer));
        members.add(member);
        aPlayer.getData("party").setValue(members);
    }

    public static void safelyRemovePartyMember(AlphaPlayer aPlayer, UUID member) {
        List<UUID> members = new ArrayList<>(getPartyObject(aPlayer));
        members.remove(member);
        aPlayer.getData("party").setValue(members);
    }

    public static ConcurrentHashMap<UUID, Integer> getSentInvitesObject(AlphaPlayer aPlayer) {
        return (ConcurrentHashMap<UUID, Integer>) aPlayer.getData("sentPartyInvites", new ConcurrentHashMap<>()).getValue();
    }

    public static ConcurrentHashMap<UUID, Integer> getReceivedInvitesObject(AlphaPlayer aPlayer) {
        return (ConcurrentHashMap<UUID, Integer>) aPlayer.getData("receivedPartyInvites", new ConcurrentHashMap<>()).getValue();
    }

    /*
    public methods ⤴








    private and package-local methods ⤵
     */

    private static void expireSentInvite(Player p, UUID invitee) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        getSentInvitesObject(aPlayer).remove(invitee);
    }

    private static void expireReceivedInvite(Player p, UUID inviter) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        getReceivedInvitesObject(aPlayer).remove(inviter);
        p.sendMessage(chat.color(chat.getServer() + Messages.party_notify_receivedInviteExpired.replace("%player%",
                ServerLink.getNameFromAlphaPlayer(aPlayer, inviter))));
    }

    static void warp(Player p, String server) {
        p.sendMessage(chat.color(chat.getServer() + Messages.party_notify_warpMember));
        String availability = NetworkCache.getAvailability(server);
        if (availability == null) {
            p.sendMessage(chat.color(chat.getServer() + "&cYou can't be warped to your party leader's " +
                    "server because it went offline."));
            return;
        } else if (availability.equals("closed")) {
            p.sendMessage(chat.color(chat.getServer() + "&cYou can't be warped to your party leader's " +
                    "server because it is full."));
            return;
        } else if (availability.equals("offline")) {
            p.sendMessage(chat.color(chat.getServer() + "&cYou can't be warped to your party leader's " +
                    "server because it went offline."));
            return;
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis j1 = JedisOne.getTwo();

                if (j1.hget("availability", server).equals("open"))
                    PluginMessaging.TP(p, server);
                else
                    p.sendMessage(chat.color(chat.getServer() + "&cYou can't be warped to your party leader's " +
                            "server because it is full."));

                JedisOne.closeTwo();
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    static void createParty(Player p, UUID firstPlayer) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        safelyAddPartyMember(aPlayer, p.getUniqueId());
        safelyAddPartyMember(aPlayer, firstPlayer);
        getSentInvitesObject(aPlayer).remove(firstPlayer);
        getReceivedInvitesObject(aPlayer).remove(firstPlayer);
        p.sendMessage(chat.color(chat.getServer() + Messages.party_success_create.replace("%player%",
                ServerLink.getNameFromAlphaPlayer(aPlayer, firstPlayer))));
    }

    static void notifyThatPartyWasDisbanded(Player p) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        UUID leader = getPartyObject(aPlayer).get(0);
        getPartyObject(aPlayer).clear();
        p.sendMessage(chat.color(chat.getServer() + Messages.party_update_disbanded.replace("%leader%",
                ServerLink.getNameFromAlphaPlayer(aPlayer, leader))));
    }

    static void notifyThatUserLeftParty(Player p, UUID leaver) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        safelyRemovePartyMember(aPlayer, leaver);
        p.sendMessage(chat.color(chat.getServer() + Messages.party_update_left.replace("%player%",
                ServerLink.getNameFromAlphaPlayer(aPlayer, leaver))));
    }

    static void notifyThatUserJoinedParty(Player p, UUID joiner) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        safelyAddPartyMember(aPlayer, joiner);
        getSentInvitesObject(aPlayer).remove(joiner);
        p.sendMessage(chat.color(chat.getServer() + Messages.party_update_joined.replace("%player%",
                ServerLink.getNameFromAlphaPlayer(aPlayer, joiner))));
    }

    static void notifyThatPartyWasDisbandedOnlyPlayer(Player p) {
        getPartyObject(PlayerManager.get(p)).clear();
        p.sendMessage(chat.color(chat.getServer() + Messages.party_update_disbandedOnlyPlayer));
    }

    static void receiveInvite(Player p, UUID inviter) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        getReceivedInvitesObject(aPlayer).put(inviter, 60 * 5);
        String inviterName = ServerLink.getNameFromAlphaPlayer(aPlayer, inviter);
        String unclickable1 = chat.color(chat.getServer() + "\nYou have been invited to %leader%'s party.\n");
        unclickable1 = unclickable1.replaceAll("%leader%", inviterName);
        String clickable = "Click Here to join.";
        String hover = "Click to join " + inviterName + "'s party";
        String command = "/party accept " + inviterName;

        FancyMessage fancy = new FancyMessage(unclickable1).color(ChatColor.BLUE);
        fancy.then(clickable).color(ChatColor.GOLD).style(ChatColor.BOLD);
        fancy.tooltip(hover);
        fancy.command(command);

        CommandText.sendMessage(p, fancy.toJSONString());
    }

    static void notifyThatPartySentInviteExpired(Player p, UUID invitee) {
        p.sendMessage(chat.color(chat.getServer() + Messages.party_notify_sentInviteExpired.replace("%player%",
                ServerLink.getNameFromAlphaPlayer(PlayerManager.get(p), invitee))));
    }

    static void notifyThatUserWasKickedFromParty(Player p, UUID kickedPlayer) {
        safelyRemovePartyMember(PlayerManager.get(p), kickedPlayer);
        p.sendMessage(chat.color(chat.getServer() + Messages.party_update_kicked.replace("%player%",
                ServerLink.getNameFromAlphaPlayer(PlayerManager.get(p), kickedPlayer))));
    }

    static void notifyThatUserWasInvitedToParty(Player p, UUID invitee) {
        p.sendMessage(chat.color(chat.getServer() + Messages.party_notify_inviteSent.replace("%player%",
                ServerLink.getNameFromAlphaPlayer(PlayerManager.get(p), invitee))));
    }

    static void notifyKicked(Player p) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        UUID leader = getPartyObject(aPlayer).get(0);
        getPartyObject(aPlayer).clear();
        p.sendMessage(chat.color(chat.getServer() + Messages.party_notify_kicked.replace("%leader%",
                ServerLink.getNameFromAlphaPlayer(PlayerManager.get(p), leader))));
    }
}
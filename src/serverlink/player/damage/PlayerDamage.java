package serverlink.player.damage;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;

import java.util.HashMap;

public class PlayerDamage {

    private static HashMap<Player, Boolean> takeMobDamage = new HashMap<>();
    private static HashMap<Player, Boolean> takePlayerDamage = new HashMap<>();
    private static HashMap<Player, Boolean> takeCactusDamage = new HashMap<>();
    private static HashMap<Player, Boolean> takeFireDamage = new HashMap<>();
    private static HashMap<Player, Boolean> takeLavaDamage = new HashMap<>();
    private static HashMap<Player, Boolean> takeDrownDamage = new HashMap<>();
    private static HashMap<Player, Boolean> takeExplosionDamage = new HashMap<>();
    private static HashMap<Player, Boolean> takeFallDamage = new HashMap<>();
    private static HashMap<Player, Boolean> takeOtherDamage = new HashMap<>();
    private static HashMap<Player, Boolean> loseHunger = new HashMap<>();
    private static HashMap<Player, Boolean> respawnOnVoid = new HashMap<>();
    private static HashMap<Player, Boolean> damageItems = new HashMap<>();
    private static HashMap<Player, Boolean> hurtMobs = new HashMap<>();
    private static HashMap<Player, Boolean> hurtPlayers = new HashMap<>();

    public static void setup() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Player p : new HashMap<>(PlayerDamageEvents.lastDamagerTimeout).keySet()) {
                    if (!PlayerDamageEvents.lastDamager.containsKey(p)) PlayerDamageEvents.lastDamagerTimeout.remove(p);
                    else if (PlayerDamageEvents.lastDamagerTimeout.get(p) <= 0.6) {
                        PlayerDamageEvents.lastDamager.remove(p);
                        PlayerDamageEvents.lastDamagerTimeout.remove(p);
                        PlayerDamageEvents.lastAssisters.get(p).clear();
                    } else
                        PlayerDamageEvents.lastDamagerTimeout.put(p, PlayerDamageEvents.lastDamagerTimeout.get(p) - 0.5);
                }
                for (Player p : new HashMap<>(PlayerDamageEvents.attackedRecently).keySet()) {
                    if (PlayerDamageEvents.attackedRecently.get(p) <= 0.6) {
                        PlayerDamageEvents.attackedRecently.remove(p);
                    } else PlayerDamageEvents.attackedRecently.put(p, PlayerDamageEvents.attackedRecently.get(p) - 0.5);
                }
                for (Player victim : new HashMap<>(PlayerDamageEvents.lastAssisters).keySet()) {
                    for (Player p : new HashMap<>(PlayerDamageEvents.lastAssisters.get(victim)).keySet()) {
                        if (PlayerDamageEvents.lastAssisters.get(victim).get(p) <= 0.6) {
                            PlayerDamageEvents.lastAssisters.get(victim).remove(p);
                        } else
                            PlayerDamageEvents.lastAssisters.get(victim).put(p, PlayerDamageEvents.lastAssisters.get(victim).get(p) - 0.5);

                    }
                }
            }
        }.runTaskTimer(ServerLink.plugin, 10, 10);
    }

    public static void leave(Player player) {
        takeMobDamage.remove(player);
        takePlayerDamage.remove(player);
        takeCactusDamage.remove(player);
        takeFireDamage.remove(player);
        takeLavaDamage.remove(player);
        takeDrownDamage.remove(player);
        takeExplosionDamage.remove(player);
        takeFallDamage.remove(player);
        takeOtherDamage.remove(player);
        loseHunger.remove(player);
        respawnOnVoid.remove(player);
        damageItems.remove(player);
        hurtMobs.remove(player);
        hurtPlayers.remove(player);
        //do this later so games can count leaving as a kill
        new BukkitRunnable() {
            @Override
            public void run() {
                PlayerDamageEvents.lastDamager.remove(player);
                PlayerDamageEvents.lastDamagerTimeout.remove(player);
            }
        }.runTaskLater(ServerLink.plugin, 5);
    }

    public static void setTakeMobDamage(Player player, Boolean bool) {
        takeMobDamage.put(player, bool);
    }

    public static void setTakePlayerDamage(Player player, Boolean bool) {
        takePlayerDamage.put(player, bool);
    }

    public static void setTakeCactusDamage(Player player, Boolean bool) {
        takeCactusDamage.put(player, bool);
    }

    public static void setTakeFireDamage(Player player, Boolean bool) {
        takeFireDamage.put(player, bool);
    }

    public static void setTakeLavaDamage(Player player, Boolean bool) {
        takeLavaDamage.put(player, bool);
    }

    public static void setTakeDrownDamage(Player player, Boolean bool) {
        takeDrownDamage.put(player, bool);
    }

    public static void setTakeExplosionDamage(Player player, Boolean bool) {
        takeExplosionDamage.put(player, bool);
    }

    public static void setTakeFallDamage(Player player, Boolean bool) {
        takeFallDamage.put(player, bool);
    }

    public static void setTakeOtherDamage(Player player, Boolean bool) {
        takeOtherDamage.put(player, bool);
    }

    public static void setLoseHunger(Player player, Boolean bool) {
        loseHunger.put(player, bool);
    }

    public static void setRespawnOnVoid(Player player, Boolean bool) {
        respawnOnVoid.put(player, bool);
    }

    public static void setItemsDamageable(Player player, Boolean bool) {
        damageItems.put(player, bool);
    }

    public static void setHurtMobs(Player player, Boolean bool) {
        hurtMobs.put(player, bool);
    }

    public static void setHurtPlayers(Player player, Boolean bool) {
        hurtPlayers.put(player, bool);
    }

    public static void invincible(Player player, Boolean bool) {
        takeMobDamage.put(player, !bool);
        takePlayerDamage.put(player, !bool);
        takeCactusDamage.put(player, !bool);
        takeFireDamage.put(player, !bool);
        takeLavaDamage.put(player, !bool);
        takeDrownDamage.put(player, !bool);
        takeExplosionDamage.put(player, !bool);
        takeFallDamage.put(player, !bool);
        takeOtherDamage.put(player, !bool);
        loseHunger.put(player, !bool);
        respawnOnVoid.put(player, bool);
    }

    public static boolean canTakeMobDamage(Player player) {
        if (!takeMobDamage.containsKey(player)) return true;
        return takeMobDamage.get(player);
    }


    public static boolean canTakePlayerDamage(Player player) {
        if (!takePlayerDamage.containsKey(player)) return true;
        return takePlayerDamage.get(player);
    }

    public static boolean canTakeCactusDamage(Player player) {
        if (!takeCactusDamage.containsKey(player)) return true;
        return takeCactusDamage.get(player);
    }

    public static boolean canTakeFireDamage(Player player) {
        if (!takeFireDamage.containsKey(player)) return true;
        return takeFireDamage.get(player);
    }

    public static boolean canTakeLavaDamage(Player player) {
        if (!takeLavaDamage.containsKey(player)) return true;
        return takeLavaDamage.get(player);
    }

    public static boolean canTakeDrownDamage(Player player) {
        if (!takeDrownDamage.containsKey(player)) return true;
        return takeDrownDamage.get(player);
    }

    public static boolean canTakeExplosionDamage(Player player) {
        if (!takeExplosionDamage.containsKey(player)) return true;
        return takeExplosionDamage.get(player);
    }

    public static boolean canTakeFallDamage(Player player) {
        if (!takeFallDamage.containsKey(player)) return true;
        return takeFallDamage.get(player);
    }

    public static boolean canTakeOtherDamage(Player player) {
        if (!takeOtherDamage.containsKey(player)) return true;
        return takeOtherDamage.get(player);
    }

    public static boolean doesLoseHunger(Player player) {
        if (!loseHunger.containsKey(player)) return true;
        return loseHunger.get(player);
    }

    public static boolean doesRespawnOnVoid(Player player) {
        if (!respawnOnVoid.containsKey(player)) return false;
        return respawnOnVoid.get(player);
    }

    public static boolean itemsDoTakeDamage(Player player) {
        if (!damageItems.containsKey(player)) return true;
        return damageItems.get(player);
    }

    public static boolean canHurtMobs(Player player) {
        if (!hurtMobs.containsKey(player)) return true;
        return hurtMobs.get(player);
    }

    public static boolean canHurtPlayers(Player player) {
        if (!hurtPlayers.containsKey(player)) return true;
        return hurtPlayers.get(player);
    }
}

package serverlink.player.damage;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.event.NPCCombustEvent;
import net.citizensnpcs.api.event.NPCDamageByEntityEvent;
import net.citizensnpcs.api.event.NPCDamageEvent;
import org.bukkit.Bukkit;
import org.bukkit.entity.*;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.*;
import org.bukkit.event.player.PlayerItemDamageEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.inventory.ItemStack;
import serverlink.ServerLink;
import serverlink.alphaplayer.PlayerManager;
import serverlink.bot.BotBaseTrait;
import serverlink.customevent.CustomLivingEntityDeathEvent;
import serverlink.customevent.CustomPlayerDeathEvent;
import serverlink.customevent.CustomPlayerKillLivingEntityEvent;
import serverlink.npc.NpcManager;
import serverlink.player.interaction.PlayerInteraction;
import serverlink.player.team.TeamManager;

import java.util.HashMap;

public class PlayerDamageEvents implements Listener {
    public static HashMap<Player, LivingEntity> lastDamager = new HashMap<>();
    // this removes a players damager after 7 seconds so that the server doesn't think
    // the player was killed by something that attacked it a while ago
    public static HashMap<Player, Double> lastDamagerTimeout = new HashMap<>();
    public static HashMap<Player, HashMap<Player, Double>> lastAssisters = new HashMap<>();
    public static HashMap<Player, Double> attackedRecently = new HashMap<>();

    @EventHandler
    private void onJoin(PlayerLoginEvent e) {
        lastAssisters.put(e.getPlayer(), new HashMap<>());
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onItemDamage(PlayerItemDamageEvent e) {
        if (!PlayerDamage.itemsDoTakeDamage(e.getPlayer())) {
            e.setCancelled(true);
            e.getPlayer().updateInventory();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void fallToVoid(PlayerMoveEvent e) {
        Player player = e.getPlayer();
        if (!PlayerManager.get(player).isFullyLoggedIn()) return;
        if (player.getLocation().getBlockY() < -2) {
            if (PlayerDamage.doesRespawnOnVoid(player)) {
                //we need nodamageticks because players will still take fall damage when respawning (even if nofalldamage is disabled)
                player.setNoDamageTicks(10);
                player.teleport(player.getWorld().getSpawnLocation().setDirection(player.getLocation().getDirection()));
            } else {
                if (lastDamager.containsKey(player)) {
                    Entity killer = lastDamager.get(player);
                    if (killer instanceof Player && (((Player) killer).isOnline() || (killer.hasMetadata("NPC") && CitizensAPI.getNPCRegistry().getNPC(player).getTrait(BotBaseTrait.class).ready)))
                        Bukkit.getPluginManager().callEvent(new CustomPlayerKillLivingEntityEvent((Player) killer, player));
                    else
                        Bukkit.getPluginManager().callEvent(new CustomPlayerDeathEvent(player, killer.getName()));
                    lastDamager.remove(player);
                    lastDamagerTimeout.remove(player);
                    lastAssisters.get(player).clear();
                } else {
                    Bukkit.getPluginManager().callEvent(new CustomPlayerDeathEvent(player, "suicide"));
                }
                // remove victim so he is no longer "in combat" if he attacked someone before dying
                attackedRecently.remove(player);
            }
        }
    }

    // only called by AntiAura or a new plugin that doesn't use ClassManager
    @EventHandler(priority = EventPriority.HIGHEST)
    private void playerDeath(PlayerDeathEvent e) {
        e.setDeathMessage(null);
        Player player = e.getEntity();
        if (!PlayerInteraction.canDeathItemDrop(player)) e.getDrops().clear();
        if (!PlayerInteraction.canDeathExpDrop(player)) e.setDroppedExp(0);
        if (lastDamager.containsKey(player)) {
            Entity killer = lastDamager.get(player);
            if (killer instanceof Player && ((Player) killer).isOnline())
                Bukkit.getPluginManager().callEvent(new CustomPlayerKillLivingEntityEvent((Player) killer, player));
            else
                Bukkit.getPluginManager().callEvent(new CustomPlayerDeathEvent(player, killer.getName()));
            lastDamager.remove(player);
            lastDamagerTimeout.remove(player);
            lastAssisters.get(player).clear();
        } else {
            Bukkit.getPluginManager().callEvent(new CustomPlayerDeathEvent(player, "suicide"));
        }
        // remove victim so he is no longer "in combat" if he attacked someone before dying
        attackedRecently.remove(player);
    }

    // only called by a new plugin that damages directly instead of using ClassManager
    @EventHandler(priority = EventPriority.HIGHEST)
    private void playerDeath(EntityDamageEvent e) {
        if (e.isCancelled()) return;
        if (!(e.getEntity() instanceof Player)) return;
        Player player = (Player) e.getEntity();
        if (!ServerLink.isLoggedIn(player)) return;
        if (e.getFinalDamage() < player.getHealth()) return;
        if (PlayerInteraction.canDeathItemDrop(player)) {
            for (ItemStack item : player.getInventory().getContents()) {
                if (item == null) continue;
                player.getWorld().dropItem(player.getLocation(), item);
            }
        }
        if (PlayerInteraction.canDeathExpDrop(player)) {
            ExperienceOrb exp = player.getWorld().spawn(player.getLocation(), ExperienceOrb.class);
            exp.setExperience(((Float) player.getExp()).intValue());
        }
        if (lastDamager.containsKey(player)) {
            Entity killer = lastDamager.get(player);
            if (killer instanceof Player && ((Player) killer).isOnline())
                Bukkit.getPluginManager().callEvent(new CustomPlayerKillLivingEntityEvent((Player) killer, player));
            else
                Bukkit.getPluginManager().callEvent(new CustomPlayerDeathEvent(player, killer.getName()));
            lastDamager.remove(player);
            lastDamagerTimeout.remove(player);
            lastAssisters.get(player).clear();
        } else Bukkit.getPluginManager().callEvent(new CustomPlayerDeathEvent(player, "suicide"));
        // remove victim so he is no longer "in combat" if he attacked someone before dying
        attackedRecently.remove(player);
        e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void livingEntityDeath(EntityDamageEvent e) {
        if (e.isCancelled()) return;
        if (!(e.getEntity() instanceof LivingEntity)) return;
        if (e.getEntity() instanceof Player) return;
        LivingEntity entity = (LivingEntity) e.getEntity();
        if (e.getFinalDamage() < entity.getHealth()) return;
        String deathReason = "died";
        switch (e.getCause()) {
            case BLOCK_EXPLOSION:
            case ENTITY_EXPLOSION:
                deathReason = "blew up";
                break;
            case DROWNING:
                deathReason = "drowned";
                break;
            case FALL:
                deathReason = "fell to death";
                break;
            case FALLING_BLOCK:
                deathReason = "was squished to death";
                break;
            case SUFFOCATION:
                deathReason = "suffocated to death";
                break;
            case FIRE:
                deathReason = "was burnt to death";
                break;
            case FIRE_TICK:
                deathReason = "was burnt to death";
                break;
            case LIGHTNING:
                deathReason = "was electrocuted";
                break;
            case MAGIC:
                deathReason = "was killed by a potion";
                break;
            case MELTING:
                deathReason = "melted to death";
                break;
            case PROJECTILE:
                deathReason = "was shot to death";
                break;
            case POISON:
                deathReason = "was died of poison";
                break;
            case WITHER:
                deathReason = "was killed by the wither effect";
                break;
        }
        Bukkit.getPluginManager().callEvent(new CustomLivingEntityDeathEvent(entity, deathReason, entity.getHealth()));
        entity.remove();
        e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void botDeath(NPCDamageEvent e) {
        if (e.isCancelled()) return;
        Player bot = (Player) e.getNPC().getEntity();
        if (e.getDamage() < bot.getHealth()) return;
        String deathReason = "died";
        switch (e.getCause()) {
            case BLOCK_EXPLOSION:
            case ENTITY_EXPLOSION:
                deathReason = "blew up";
                break;
            case DROWNING:
                deathReason = "drowned";
                break;
            case FALL:
                deathReason = "fell to death";
                break;
            case FALLING_BLOCK:
                deathReason = "was squished to death";
                break;
            case SUFFOCATION:
                deathReason = "suffocated to death";
                break;
            case FIRE:
                deathReason = "was burnt to death";
                break;
            case FIRE_TICK:
                deathReason = "was burnt to death";
                break;
            case LIGHTNING:
                deathReason = "was electrocuted";
                break;
            case MAGIC:
                deathReason = "was killed by a potion";
                break;
            case MELTING:
                deathReason = "melted to death";
                break;
            case PROJECTILE:
                deathReason = "was shot to death";
                break;
            case POISON:
                deathReason = "was died of poison";
                break;
            case WITHER:
                deathReason = "was killed by the wither effect";
                break;
        }
        Bukkit.getPluginManager().callEvent(new CustomLivingEntityDeathEvent(bot, deathReason, bot.getHealth()));
        bot.remove();
        e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void enterCombat(EntityDamageByEntityEvent e) {
        if (e.isCancelled()) return;
        if (e.getDamage() == 0 || e.getFinalDamage() == 0) return;
        Entity damager = e.getDamager();
        Entity entity = e.getEntity();
        if (e.getDamager() instanceof ThrownPotion) {
            ThrownPotion potion = (ThrownPotion) e.getDamager();
            //return if potion was shot from a dispenser
            if (!(potion.getShooter() instanceof Entity)) return;
            damager = (Entity) potion.getShooter();
        }
        if ((e.getDamager() instanceof Projectile)) {
            Projectile projectile = (Projectile) e.getDamager();
            //return if projectile was shot from a dispenser
            if (!(projectile.getShooter() instanceof Entity)) return;
            damager = (Entity) projectile.getShooter();
        }
        //return on mob attacking mob
        if (entity.getType() != EntityType.PLAYER && damager.getType() != EntityType.PLAYER) {
            return;
        }
        //player attacking player
        if (entity.getType() == EntityType.PLAYER && damager.getType() == EntityType.PLAYER) {
            Player attacker = (Player) damager;
            Player victim = (Player) entity;
            // Stop enderpearl damage from marking the player as fighting himself
            if (attacker == victim) return;
            // Don't register friendly fire
            if (TeamManager.getTeam(attacker) != null
                    && TeamManager.getTeam(attacker) == TeamManager.getTeam(victim))
                return;
            lastDamager.put(victim, attacker);
            lastDamagerTimeout.put(victim, 7.0);
            attackedRecently.put(attacker, 7.0);
            if (e.getDamage() >= victim.getHealth()) {
                e.setCancelled(true);
                Bukkit.getPluginManager().callEvent(new CustomPlayerKillLivingEntityEvent(attacker, victim));
                lastDamager.remove(victim);
                lastDamagerTimeout.remove(victim);
                lastAssisters.get(victim).clear();
                // remove victim so he is no longer "in combat" if he attacked someone before dying
                attackedRecently.remove(victim);
            }
        }
        //mob attacking player
        else if (damager instanceof LivingEntity && entity.getType() == EntityType.PLAYER) {
            Player victim = (Player) entity;
            lastDamager.put(victim, (LivingEntity) damager);
            lastDamagerTimeout.put(victim, 7.0);
            if (e.getDamage() >= victim.getHealth()) {
                e.setCancelled(true);
                if (damager.getCustomName().equals(damager.getName()))
                    Bukkit.getPluginManager().callEvent(new CustomPlayerDeathEvent(victim, "a " + damager.getName()));
                else
                    Bukkit.getPluginManager().callEvent(new CustomPlayerDeathEvent(victim, damager.getCustomName()));
                lastDamager.remove(victim);
                lastDamagerTimeout.remove(victim);
                lastAssisters.get(victim).clear();
                // remove victim so he is no longer "in combat" if he attacked someone before dying
                attackedRecently.remove(victim);
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void enterCombat_bot(NPCDamageByEntityEvent e) {
        if (e.isCancelled()) return;
        if (e.getDamage() == 0) return;
        Entity damager = e.getDamager();
        Entity entity = e.getNPC().getEntity();
        if (e.getDamager() instanceof ThrownPotion) {
            ThrownPotion potion = (ThrownPotion) e.getDamager();
            //return if potion was shot from a dispenser
            if (!(potion.getShooter() instanceof Entity)) return;
            damager = (Entity) potion.getShooter();
        }
        if ((e.getDamager() instanceof Projectile)) {
            Projectile projectile = (Projectile) e.getDamager();
            //return if projectile was shot from a dispenser
            if (!(projectile.getShooter() instanceof Entity)) return;
            damager = (Entity) projectile.getShooter();
        }
        //player attacking player
        if (damager.getType() == EntityType.PLAYER) {
            Player attacker = (Player) damager;
            Player victim = (Player) entity;
            // Stop enderpearl damage from marking the player as fighting himself
            if (attacker == victim) return;
            // Don't register friendly fire
            if (TeamManager.getTeam(attacker) != null
                    && TeamManager.getTeam(attacker) == TeamManager.getTeam(victim))
                return;
            lastDamager.put(victim, attacker);
            lastDamagerTimeout.put(victim, 7.0);
            attackedRecently.put(attacker, 7.0);
            if (e.getDamage() >= victim.getHealth()) {
                e.setCancelled(true);
                Bukkit.getPluginManager().callEvent(new CustomPlayerKillLivingEntityEvent(attacker, victim));
                lastDamager.remove(victim);
                lastDamagerTimeout.remove(victim);
                lastAssisters.get(victim).clear();
                // remove victim so he is no longer "in combat" if he attacked someone before dying
                attackedRecently.remove(victim);
            }
        }
        //mob attacking player
        else if (e.getDamager() instanceof LivingEntity) {
            Player victim = (Player) entity;
            lastDamager.put(victim, (LivingEntity) damager);
            lastDamagerTimeout.put(victim, 7.0);
            if (e.getDamage() >= victim.getHealth()) {
                e.setCancelled(true);
                if (damager.getCustomName().equalsIgnoreCase(damager.getType().name().toLowerCase().replaceAll("_", "").replace("entity", "")))
                    Bukkit.getPluginManager().callEvent(new CustomPlayerDeathEvent(victim, "a " + damager.getName()));
                else
                    Bukkit.getPluginManager().callEvent(new CustomPlayerDeathEvent(victim, damager.getCustomName()));
                lastDamager.remove(victim);
                lastDamagerTimeout.remove(victim);
                lastAssisters.get(victim).clear();
                // remove victim so he is no longer "in combat" if he attacked someone before dying
                attackedRecently.remove(victim);
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void playerTakingDamage(EntityDamageEvent e) {
        if (!(e.getEntity() instanceof Player)) return;
        Player player = (Player) e.getEntity();
        if (e.getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK) return;
        if (e.getCause() == EntityDamageEvent.DamageCause.CONTACT) {
            if (!PlayerDamage.canTakeCactusDamage(player)) e.setCancelled(true);
        } else if (e.getCause() == EntityDamageEvent.DamageCause.FIRE || e.getCause() == EntityDamageEvent.DamageCause.FIRE_TICK) {
            if (!PlayerDamage.canTakeFireDamage(player)) e.setCancelled(true);
        } else if (e.getCause() == EntityDamageEvent.DamageCause.LAVA) {
            if (!PlayerDamage.canTakeLavaDamage(player)) e.setCancelled(true);
        } else if (e.getCause() == EntityDamageEvent.DamageCause.FALL) {
            if (!PlayerDamage.canTakeFallDamage(player)) e.setCancelled(true);
        } else if (e.getCause() == EntityDamageEvent.DamageCause.DROWNING) {
            if (!PlayerDamage.canTakeDrownDamage(player)) e.setCancelled(true);
        } else if (e.getCause() == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION || e.getCause() == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION) {
            if (!PlayerDamage.canTakeExplosionDamage(player)) e.setCancelled(true);
        } else {
            if (!PlayerDamage.canTakeOtherDamage((player))) e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void botTakingDamage(NPCDamageEvent e) {
        if (e.getNPC().getEntity().getType() != EntityType.PLAYER) return;
        Player bot = (Player) e.getNPC().getEntity();
        if (e.getCause() == EntityDamageEvent.DamageCause.ENTITY_ATTACK) return;
        if (e.getCause() == EntityDamageEvent.DamageCause.CONTACT) {
            e.setCancelled(!PlayerDamage.canTakeCactusDamage(bot));
        } else if (e.getCause() == EntityDamageEvent.DamageCause.FIRE || e.getCause() == EntityDamageEvent.DamageCause.FIRE_TICK) {
            e.setCancelled(!PlayerDamage.canTakeFireDamage(bot));
        } else if (e.getCause() == EntityDamageEvent.DamageCause.LAVA) {
            e.setCancelled(!PlayerDamage.canTakeLavaDamage(bot));
        } else if (e.getCause() == EntityDamageEvent.DamageCause.FALL) {
            e.setCancelled(!PlayerDamage.canTakeFallDamage(bot));
        } else if (e.getCause() == EntityDamageEvent.DamageCause.DROWNING) {
            e.setCancelled(!PlayerDamage.canTakeDrownDamage(bot));
        } else if (e.getCause() == EntityDamageEvent.DamageCause.BLOCK_EXPLOSION || e.getCause() == EntityDamageEvent.DamageCause.ENTITY_EXPLOSION) {
            e.setCancelled(!PlayerDamage.canTakeExplosionDamage(bot));
        } else {
            e.setCancelled(!PlayerDamage.canTakeOtherDamage((bot)));
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void damage(EntityDamageByEntityEvent e) {
        Entity damager = e.getDamager();
        Entity entity = e.getEntity();
        if (e.getDamager() instanceof ThrownPotion) {
            ThrownPotion potion = (ThrownPotion) e.getDamager();
            //return if potion was shot from a dispenser
            if (!(potion.getShooter() instanceof Entity)) return;
            damager = (Entity) potion.getShooter();
        }
        if ((e.getDamager() instanceof Projectile)) {
            Projectile projectile = (Projectile) e.getDamager();
            //return if projectile was shot from a dispenser
            if (!(projectile.getShooter() instanceof Entity)) return;
            damager = (Entity) projectile.getShooter();
        }
        //return on mob attacking mob
        if (!(entity instanceof Player) && !(damager instanceof Player)) {
            return;
        }
        //player attacking player
        if ((entity.getType() == EntityType.PLAYER) && (damager.getType() == EntityType.PLAYER)) {
            Player attacker = (Player) damager;
            Player victim = (Player) entity;
            if (!PlayerDamage.canHurtPlayers(attacker)) e.setCancelled(true);
            else if (!PlayerDamage.canTakePlayerDamage(victim)) e.setCancelled(true);
        }
        //mob attacking player
        else if (damager instanceof LivingEntity && entity.getType() == EntityType.PLAYER) {
            Player victim = (Player) entity;
            if (!PlayerDamage.canTakeMobDamage(victim)) e.setCancelled(true);
        }
        //player attacking mob
        else {
            Player attacker = (Player) damager;
            if (!PlayerDamage.canHurtMobs(attacker)) e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void damage_bot(NPCDamageByEntityEvent e) {
        Entity damager = e.getDamager();
        Entity entity = e.getNPC().getEntity();
        if (e.getDamager() instanceof ThrownPotion) {
            ThrownPotion potion = (ThrownPotion) e.getDamager();
            //return if potion was shot from a dispenser
            if (!(potion.getShooter() instanceof Entity)) return;
            damager = (Entity) potion.getShooter();
        }
        if ((e.getDamager() instanceof Projectile)) {
            Projectile projectile = (Projectile) e.getDamager();
            //return if projectile was shot from a dispenser
            if (!(projectile.getShooter() instanceof Entity)) return;
            damager = (Entity) projectile.getShooter();
        }
        //return on mob attacking mob
        if (!(entity instanceof Player) && !(damager instanceof Player)) {
            return;
        }
        //player attacking player
        if ((entity.getType() == EntityType.PLAYER) && (damager.getType() == EntityType.PLAYER)) {
            e.setCancelled(!PlayerDamage.canHurtPlayers((Player) damager) || !PlayerDamage.canTakePlayerDamage((Player) entity));
        }
        //mob attacking player
        else if (damager instanceof LivingEntity && entity.getType() == EntityType.PLAYER) {
            e.setCancelled(!PlayerDamage.canTakeMobDamage((Player) entity));
        }
        //player attacking mob
        else {
            e.setCancelled(!PlayerDamage.canHurtMobs((Player) damager));
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void combust(EntityCombustEvent e) {
        Entity en = e.getEntity();
        if (NpcManager.getNpcs().contains(en.getEntityId())) {
            e.setCancelled(true);
            return;
        }
        if (en instanceof Player) {
            Player player = (Player) en;
            if (PlayerDamage.canTakeFireDamage(player)) return;
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void combust(NPCCombustEvent e) {
        Entity en = e.getNPC().getEntity();
        if (NpcManager.getNpcs().contains(en.getEntityId())) {
            e.setCancelled(true);
            return;
        }
        if (en instanceof Player) {
            Player player = (Player) en;
            if (PlayerDamage.canTakeFireDamage(player)) return;
            e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void loseHunger(FoodLevelChangeEvent e) {
        if (!(e.getEntity() instanceof Player)) return;
        Player player = (Player) e.getEntity();
        if (!PlayerDamage.doesLoseHunger(player)) {
            e.setCancelled(true);
        }
    }

    @EventHandler
    private void onCustomPlayerDeath(CustomPlayerDeathEvent e) {
        Player player = e.getPlayer();
        if (PlayerInteraction.canDeathItemDrop(player)) {
            for (ItemStack item : player.getInventory().getContents()) {
                if (item == null) continue;
                player.getWorld().dropItem(player.getLocation(), item);
            }
        }
        if (PlayerInteraction.canDeathExpDrop(player)) {
            ExperienceOrb exp = player.getWorld().spawn(player.getLocation(), ExperienceOrb.class);
            exp.setExperience(((Float) player.getExp()).intValue());
        }
    }

    @EventHandler
    private void onCustomPlayerKillEntity(CustomPlayerKillLivingEntityEvent e) {
        if (!(e.getEntity() instanceof Player)) return;
        Player player = (Player) e.getEntity();
        if (PlayerInteraction.canDeathItemDrop(player)) {
            for (ItemStack item : player.getInventory().getContents()) {
                if (item == null) continue;
                player.getWorld().dropItem(player.getLocation(), item);
            }
        }
        if (PlayerInteraction.canDeathExpDrop(player)) {
            ExperienceOrb exp = player.getWorld().spawn(player.getLocation(), ExperienceOrb.class);
            exp.setExperience(((Float) player.getExp()).intValue());
        }
    }
}

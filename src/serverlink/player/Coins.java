package serverlink.player;


import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.PlayerManager;
import serverlink.customevent.CustomNetworkMessageEvent;
import serverlink.network.JedisPool;
import serverlink.network.nmessage;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.UUID;

public class Coins implements Listener {
    // DecimalFormat is *probably* thread-safe when you only call .format()
    private static DecimalFormat thousand = new DecimalFormat("###.##");
    private static DecimalFormat million = new DecimalFormat("0.###");

    public static void setup() {
        thousand.setRoundingMode(RoundingMode.HALF_DOWN);
        million.setRoundingMode(RoundingMode.HALF_DOWN);
    }

    public static String formatCoins(int coins) {
        if (coins < 1000) return "&e" + ((Integer) coins).toString();
        else if (coins < 1000000) return "&e" + thousand.format((double) coins / 1000) + "&d&ok";
        else return "&e" + million.format((double) coins / 1000000) + "&c&omil";
    }

    public static String getOfflineCoinsString(String uuid) {
        Jedis jedis = JedisPool.getConn();
        String coins = jedis.hget(uuid, "coins");
        JedisPool.close(jedis);
        Integer iCoins;
        try {
            iCoins = Integer.parseInt(coins);
        } catch (NumberFormatException e1) {
            iCoins = 0;
        }
        if (iCoins < 0) return "&e0";
        else if (iCoins < 1000) return "&e" + iCoins.toString();
        else if (iCoins < 1000000) return "&e" + thousand.format(iCoins / 1000) + "&d&ok";
        else return "&e" + million.format(iCoins / 1000000) + "&c&omil";
    }

    public static int getOfflineCoinsInt(String uuid) {
        Jedis jedis = JedisPool.getConn();
        String coins = jedis.hget(uuid, "coins");
        JedisPool.close(jedis);
        int iCoins;
        try {
            iCoins = Integer.parseInt(coins);
        } catch (NumberFormatException e1) {
            iCoins = 0;
        }
        return iCoins;
    }

    public static void addCoins(String uuid, int coinsToAdd) {
        Player player = Bukkit.getPlayer(UUID.fromString(uuid));
        if (player != null && player.isOnline()) {
            PlayerManager.get(player.getUniqueId()).addCoins(coinsToAdd);
            return;
        }
        Jedis jedis = JedisPool.getConn();
        String currentCoins = jedis.hget(uuid, "coins");
        if (currentCoins == null)
            jedis.hset(uuid, "coins", ((Integer) coinsToAdd).toString());
        else
            jedis.hset(uuid, "coins",
                    ((Integer) (Integer.parseInt(currentCoins) + coinsToAdd)).toString());
        String playerOnNetwork = jedis.hget(uuid, "online");
        if (playerOnNetwork != null && playerOnNetwork.equals("true")) {
            nmessage.sendMessage(jedis.hget(uuid, "server"), "reloadCoins:" + uuid, jedis);
        }
        JedisPool.close(jedis);
    }

    public static void takeCoins(String uuid, int coinsToTake) {
        Player player = Bukkit.getPlayer(UUID.fromString(uuid));
        if (player != null && player.isOnline()) {
            PlayerManager.get(player.getUniqueId()).takeCoins(coinsToTake);
            return;
        }
        Jedis jedis = JedisPool.getConn();
        String currentCoins = jedis.hget(uuid, "coins");
        if (currentCoins == null || Integer.parseInt(currentCoins) - coinsToTake < 0)
            jedis.hset(uuid, "coins", "0");
        else
            jedis.hset(uuid, "coins",
                    ((Integer) (Integer.parseInt(currentCoins) - coinsToTake)).toString());
        JedisPool.close(jedis);
    }

    @EventHandler
    private void onNetworkMessage(CustomNetworkMessageEvent e) {
        String message = e.getMessage();
        if (!message.startsWith("reloadCoins")) return;
        String uuid = message.split(":")[1];
        Player player = Bukkit.getPlayer(UUID.fromString(uuid));
        if (player == null || !player.isOnline()) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    Jedis jedis = JedisPool.getConn();
                    if (jedis.hget(uuid, "online").equals("true")) {
                        nmessage.sendMessage(jedis.hget(uuid, "server"), "reloadCoins:" + uuid, jedis);
                    }
                    JedisPool.close(jedis);
                }
            }.runTaskAsynchronously(ServerLink.plugin);
            return;
        }
        PlayerManager.get(player).reloadCoins();
    }
}

package serverlink.player;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.network.JedisPool;

import java.util.*;

public class Settings {

    public static
    @NotNull
    String getStringSetting(String uuid, String setting) {
        AlphaPlayer aPlayer = PlayerManager.get(UUID.fromString(uuid));
        if (aPlayer != null && aPlayer.isFullyLoggedIn()) return aPlayer.getStringSetting(setting);
        Jedis jedis = JedisPool.getConn();
        String value = jedis.hget("settings:" + uuid, setting);
        JedisPool.close(jedis);
        if (value == null) return "";
        return value;
    }

    public static
    @Nullable
    Boolean getBooleanSetting(String uuid, String setting) {
        AlphaPlayer aPlayer = PlayerManager.get(UUID.fromString(uuid));
        if (aPlayer != null && aPlayer.isFullyLoggedIn()) return aPlayer.getBooleanSetting(setting);
        Jedis jedis = JedisPool.getConn();
        String value = jedis.hget("settings:" + uuid, setting);
        JedisPool.close(jedis);
        if (value == null) return null;
        if (value.equals("true")) return true;
        else if (value.equals("false")) return false;
        else return null;
    }

    public static
    @Nullable
    Integer getIntegerSetting(String uuid, String setting) {
        AlphaPlayer aPlayer = PlayerManager.get(UUID.fromString(uuid));
        if (aPlayer != null && aPlayer.isFullyLoggedIn()) return aPlayer.getIntegerSetting(setting);
        Jedis jedis = JedisPool.getConn();
        String value = jedis.hget("settings:" + uuid, setting);
        JedisPool.close(jedis);
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public static
    @NotNull
    List<String> getListSetting(String uuid, String setting) {
        AlphaPlayer aPlayer = PlayerManager.get(UUID.fromString(uuid));
        if (aPlayer != null && aPlayer.isFullyLoggedIn()) return aPlayer.getListSetting(setting);
        Jedis jedis = JedisPool.getConn();
        String value = jedis.hget("settings:" + uuid, setting);
        JedisPool.close(jedis);
        if (value == null) return new LinkedList<>();
        return stringToList(value);
    }

    public static void setSetting(String uuid, String setting, String value) {
        AlphaPlayer aPlayer = PlayerManager.get(UUID.fromString(uuid));
        if (aPlayer != null && aPlayer.isFullyLoggedIn()) {
            aPlayer.setSetting(setting, value);
            return;
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                if (value == null)
                    jedis.hdel("settings:" + uuid, setting);
                else
                    jedis.hset("settings:" + uuid, setting, value);
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public static
    @NotNull
    List<String> stringToList(String string) {
        if (string == null) return new LinkedList<>();
        List<String> list = new LinkedList<>(Arrays.asList(string.split("`;")));
        List<String> list2 = new ArrayList<>(list);
        list2.stream().filter(s -> s.trim().equals("")).forEach(list::remove);
        return list;
    }

    // we use StringBuilder instead of + so that we don't create tons of String objects
    public static
    @NotNull
    String listToString(List<String> list) {
        StringBuilder builder = new StringBuilder("");
        for (String t : list) {
            if (t.trim().equals("")) continue;
            builder.append("`;").append(t);
        }
        return builder.toString().replaceFirst("`;", "");
    }

    // we use StringBuilder instead of + so that we don't create tons of String objects
    public static
    @NotNull
    String listToString(String... array) {
        StringBuilder builder = new StringBuilder("");
        for (String t : array) {
            if (t.trim().equals("")) continue;
            builder.append("`;").append(t);
        }
        return builder.toString().replaceFirst("`;", "");
    }
}

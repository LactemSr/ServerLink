package serverlink.player;


import mkremins.fanciful.FancyMessage;
import redis.clients.jedis.Jedis;
import serverlink.network.JedisPool;

import java.time.Instant;
import java.util.ArrayList;
import java.util.List;

// Notifications are messages that are sent to players when they're offline. They are queued up and displayed when the player joins.

public class Notifications {
    public static void sendNotification(String uuid, String message, int secondsToExpire) {
        Jedis jedis = JedisPool.getConn();
        jedis.lpush("notifications:" + uuid, message + "`;" + (secondsToExpire + Instant.now().getEpochSecond()));
        JedisPool.close(jedis);
    }

    public static void sendNotification(String uuid, String message, Jedis jedis, int secondsToExpire) {
        jedis.lpush("notifications:" + uuid, message + "`;" + (secondsToExpire + Instant.now().getEpochSecond()));
    }

    /**
     * returns true if first time player because there were no messages to view
     */
    public static boolean viewedNotificationsOnLastLogin(String uuid, Jedis jedis) {
        String viewedNotificationsOnLastLogin = jedis.hget(uuid, "viewedNotificationsOnLastLogin");
        if (viewedNotificationsOnLastLogin == null) viewedNotificationsOnLastLogin = "true";
        return Boolean.getBoolean(viewedNotificationsOnLastLogin);
    }

    public static void sendFancyNotification(String uuid, FancyMessage message, int secondsToExpire) {
        Jedis jedis = JedisPool.getConn();
        jedis.lpush("notifications:" + uuid, "F@nS^~" + message.toJSONString() + "`;" + (secondsToExpire + Instant.now().getEpochSecond()));
        JedisPool.close(jedis);
    }

    public static void sendFancyNotification(String uuid, FancyMessage message, Jedis jedis, int secondsToExpire) {
        jedis.lpush("notifications:" + uuid, "F@nS^~" + message.toJSONString() + "`;" + (secondsToExpire + Instant.now().getEpochSecond()));
    }

    public static List<String> getNotifications(String uuid) {
        Jedis jedis = JedisPool.getConn();
        List<String> l = jedis.lrange("notifications:" + uuid, 0, -1);
        List<String> trimmed = new ArrayList<>();
        for (String s : l) {
            String[] split = s.split("`;");
            long now = Instant.now().getEpochSecond();
            int expire = Integer.parseInt(split[1]);
            if (expire - now >= 2)
                trimmed.add(split[0]);
            else jedis.lrem("notifications:" + uuid, 1, s);
        }
        JedisPool.close(jedis);
        return trimmed;
    }

    public static List<String> getNotifications(String uuid, Jedis jedis) {
        List<String> l = jedis.lrange("notifications:" + uuid, 0, -1);
        List<String> trimmed = new ArrayList<>();
        for (String s : l) {
            String[] split = s.split("`;");
            long now = Instant.now().getEpochSecond();
            int expire = Integer.parseInt(split[1]);
            if (expire - now >= 2)
                trimmed.add(split[0]);
            else jedis.lrem("notifications:" + uuid, 1, s);
        }
        return trimmed;
    }
}

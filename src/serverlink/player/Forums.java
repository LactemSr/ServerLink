package serverlink.player;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.network.JedisPool;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;

public class Forums {
    private static final HashMap<Integer, String> uuids = new HashMap<>();
    private static final HashMap<Integer, String> names = new HashMap<>();
    private static final HashMap<Integer, Integer> ranks = new HashMap<>();

    public static void setup() {
        new BukkitRunnable() {
            @Override
            public void run() {
                synchronized (uuids) {
                    final HashMap<Integer, String> copy = new HashMap<>(uuids);
                    for (int index = 0; index < copy.size(); index++) {
                        String uuid = uuids.get(index);
                        Jedis jedis = JedisPool.getConn();
                        String isLinked = jedis.hget(uuid, "hasLinkedForumsAccount");
                        if (isLinked == null || !isLinked.equals("true")) {
                            JedisPool.close(jedis);
                            continue;
                        }
                        Integer rank = ranks.get(index);
                        String username = names.get(index);
                        String existingUsername = jedis.hget(uuid, "usernameOnForums");
                        String existingRank = jedis.hget(uuid, "rankOnForums");
                        String id = jedis.hget(uuid, "forumsId");
                        // user is joining AlphaCraft for the first time after linking
                        if (existingUsername == null || existingUsername.isEmpty()) {
                            existingUsername = "this value will be updated";
                            existingRank = "0";
                        }
                        boolean error = false;
                        if (!username.equals(existingUsername)) {
                            // update mysql db with new ign so that it will display on the forums
                            Connection con = null;
                            Statement st = null;
                            String url = "jdbc:mysql://158.69.205.52:3306/xenforo";
                            String user = "serverlink";
                            String password = "7uTeyA?ruS";
                            try {
                                con = DriverManager.getConnection(url, user, password);
                                st = con.createStatement();
                                st.execute("REPLACE INTO xf_user_field_value (user_id, field_id, field_value) VALUES (\"" + id + "\", \"ign\", \"" + username + "\");");
                            } catch (SQLException ex) {
                                Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "ForumsLink:59   (forumsId is probably null, this error will persist because player's name in redis is different than his actual name)" + ex);
                                error = true;
                            } finally {
                                try {
                                    if (st != null) st.close();
                                    if (con != null) con.close();
                                } catch (SQLException ex) {
                                    error = true;
                                    Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "ForumsLink:67   (forumsId is probably null, this error will persist because player's name in redis is different than his actual name)" + ex);
                                }
                            }
                            if (!error) jedis.hset(uuid, "usernameOnForums", username);
                        }
                        if (Integer.parseInt(existingRank) != rank) {
                            // update mysql db with new rank so that it will display on the forums
                            Connection con = null;
                            Statement st = null;
                            String url = "jdbc:mysql://158.69.205.52:3306/xenforo";
                            String user = "serverlink";
                            String password = "7uTeyA?ruS";
                            try {
                                con = DriverManager.getConnection(url, user, password);
                                st = con.createStatement();
                                st.execute("REPLACE INTO xf_user_field_value (user_id, field_id, field_value) VALUES (\"" + id + "\", \"rank\", \"" + rank + "\");");
                                int xenforoGroup = 2;
                                if (rank > 1) xenforoGroup = 3 + rank;
                                if (rank >= 7)
                                    st.execute("UPDATE xf_user SET user_group_id=" + xenforoGroup + ",display_style_group_id=" +
                                            xenforoGroup + ",is_staff=1 WHERE user_id=" + id + ";");
                                else
                                    st.execute("UPDATE xf_user SET user_group_id=" + xenforoGroup + ",display_style_group_id=" +
                                            xenforoGroup + ",is_staff=0 WHERE user_id=" + id + ";");
                            } catch (SQLException ex) {
                                error = true;
                                Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "ForumsLink:93   (forumsId is probably null, this error will persist because player's rank in redis is different than his current rank)" + ex);
                            } finally {
                                try {
                                    if (st != null) st.close();
                                    if (con != null) con.close();
                                } catch (SQLException ex) {
                                    error = true;
                                    Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "ForumsLink:100   (forumsId is probably null, this error will persist because player's rank in redis is different than his current rank)" + ex);
                                }
                            }
                            if (!error) jedis.hset(uuid, "rankOnForums", rank.toString());
                        }
                        JedisPool.close(jedis);
                    }
                    uuids.clear();
                    names.clear();
                    ranks.clear();
                }
            }
        }.runTaskTimerAsynchronously(ServerLink.plugin, 20, 20);
    }

    public static void join(AlphaPlayer aPlayer) {
        synchronized (uuids) {
            int index = uuids.size();
            uuids.put(index, aPlayer.getUuid().toString());
            names.put(index, aPlayer.getPlayer().getName());
            ranks.put(index, aPlayer.getRank());
        }
    }
}

package serverlink.player;

import org.bukkit.ChatColor;

public class Level {
    private static final double levelAt1kExp = 4;

    public static ChatColor getLevelColor(int level) {
        if (level == 100) return ChatColor.BLACK;
        else if (level >= 75) return ChatColor.GOLD;
        else if (level >= 60) return ChatColor.RED;
        else if (level >= 50) return ChatColor.LIGHT_PURPLE;
        else if (level >= 35) return ChatColor.AQUA;
        else if (level >= 25) return ChatColor.GREEN;
        else if (level >= 10) return ChatColor.YELLOW;
        else if (level >= 5) return ChatColor.WHITE;
        else return ChatColor.GRAY;
    }

    public static int calculateLevelFromExp(int exp) {
        // casting to int truncates the number, which is perfect for levels
        int level = (int) ((levelAt1kExp / 31.6227) * Math.sqrt(exp));
        return level == 0 ? 1 : (level > 100 ? 100 : level);
    }
}

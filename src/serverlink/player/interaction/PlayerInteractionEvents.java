package serverlink.player.interaction;


import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EnderPearl;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.entity.Snowball;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.hanging.HangingBreakByEntityEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.event.player.*;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.Potion;
import serverlink.alphaplayer.PlayerManager;
import serverlink.inventory.GuiPage;
import serverlink.inventory.InventoryGui;

import java.util.Set;

public class PlayerInteractionEvents implements Listener {
    @EventHandler(priority = EventPriority.LOWEST)
    void blockPlace(BlockPlaceEvent e) {
        if (!PlayerInteraction.canPlaceBlock(e.getPlayer(), e.getBlock().getType())) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void armorstandPlace(PlayerInteractEvent e) {
        if (!(e.getAction() == Action.RIGHT_CLICK_BLOCK)) return;
        if (!(e.getPlayer().getItemInHand().getType() == Material.ARMOR_STAND)) return;
        if (!PlayerInteraction.canPlaceBlock(e.getPlayer(), Material.ARMOR_STAND)) {
            e.setCancelled(true);
            e.getPlayer().getInventory().setItemInHand(e.getPlayer().getItemInHand());
            Bukkit.getPluginManager().callEvent(new PlayerInteractEvent(e.getPlayer(), Action.RIGHT_CLICK_AIR, e.getPlayer().getItemInHand(), null, null));
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void blockBreak(BlockBreakEvent e) {
        if (!PlayerInteraction.canBreakBlock(e.getPlayer(), e.getBlock().getType())) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void openInventory(InventoryOpenEvent e) {
        if (!(e.getPlayer() instanceof Player)) return;
        if (e.getInventory().getName().contains("Buy class ")) return;
        for (ItemStack item : PlayerManager.get((Player) e.getPlayer()).getHotbarItems()) {
            if (!item.hasItemMeta()) continue;
            String title = item.getItemMeta().getDisplayName();
            if (title.equalsIgnoreCase(e.getInventory().getName())) return;
        }
        // return if a plugin opens an inventory (meaning player is not targeting a chest or other InventoryHolder
        try {
            if (!(e.getPlayer().getTargetBlock((Set<Material>) null, 6) instanceof InventoryHolder)) return;
        } catch (IllegalStateException e1) {
            // this error is bukkit's fault; it randomly throws when calling getTargetBlock
            // do nothing
        }
        Player player = (Player) e.getPlayer();
        for (InventoryGui gui : PlayerManager.get(player).getGuis()) {
            for (GuiPage page : gui.getPages()) {
                if (page.getInventory() == e.getInventory()) return;
            }
        }
        InventoryType type = e.getInventory().getType();
        if (type == InventoryType.CHEST || type == InventoryType.ENDER_CHEST) {
            if (!PlayerInteraction.canOpenChest(player)) e.setCancelled(true);
            e.getPlayer().closeInventory();
        } else if (type == InventoryType.FURNACE) {
            if (!PlayerInteraction.canOpenFurnace(player)) e.setCancelled(true);
            e.getPlayer().closeInventory();
        } else if (type == InventoryType.CRAFTING) {
            if (!PlayerInteraction.canOpenCraftingTable(player)) e.setCancelled(true);
            e.getPlayer().closeInventory();
        } else if (type == InventoryType.ENCHANTING) {
            if (!PlayerInteraction.canOpenEnchantmentTable(player)) e.setCancelled(true);
            e.getPlayer().closeInventory();
        } else if (!PlayerInteraction.canOpenOtherInventories(player)) {
            e.setCancelled(true);
            e.getPlayer().closeInventory();
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void shootBow(EntityShootBowEvent e) {
        if (!(e.getEntity() instanceof Player)) return;
        if (!PlayerInteraction.canShootBow((Player) e.getEntity())) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void shooting(ProjectileLaunchEvent e) {
        if (!(e.getEntity().getShooter() instanceof Player)) return;
        Player player = (Player) e.getEntity().getShooter();
        if (e.getEntity() instanceof EnderPearl && !PlayerInteraction.canShootEnderpearl(player))
            e.setCancelled(true);
        else if (e.getEntity() instanceof Snowball && !PlayerInteraction.canShootSnowball(player))
            e.setCancelled(true);
        else if (e.getEntity() instanceof Potion && !PlayerInteraction.canThrowPotion(player))
            e.setCancelled(true);
    }

    // interacting with blocks means flipping levers, pushing buttons, and stepping on pressure plates
    @EventHandler(priority = EventPriority.LOWEST)
    void interactWithBlock(PlayerInteractEvent e) {
        if (e.getAction() == Action.PHYSICAL) {
            if (e.getClickedBlock().getType() == Material.SOIL) {
                if (!PlayerInteraction.canTrampleSoil(e.getPlayer())) e.setCancelled(true);
            } else if (!PlayerInteraction.canInteractWithBlocks(e.getPlayer())) e.setCancelled(true);
            return;
        }
        //don't cancel event if player is interacting with something besides a block
        if (e.getAction() == Action.RIGHT_CLICK_BLOCK) {
            if (e.getClickedBlock().getType() == Material.TNT)
                e.setCancelled(!PlayerInteraction.canLightTnt(e.getPlayer()));
            else if (e.getPlayer().getItemInHand().getType() == Material.FLINT_AND_STEEL)
                e.setCancelled(!PlayerInteraction.canLightFire(e.getPlayer()));
            else if (e.getPlayer().getItemInHand().getType() == Material.WATER_BUCKET)
                e.setCancelled(!PlayerInteraction.canPlaceBlock(e.getPlayer(), Material.WATER_BUCKET)
                        || !PlayerInteraction.canPlaceBlock(e.getPlayer(), Material.WATER));
            else if (e.getPlayer().getItemInHand().getType() == Material.LAVA_BUCKET)
                e.setCancelled(!PlayerInteraction.canPlaceBlock(e.getPlayer(), Material.LAVA_BUCKET)
                        || !PlayerInteraction.canPlaceBlock(e.getPlayer(), Material.LAVA));
            else if (!e.getClickedBlock().getType().name().toLowerCase().contains("chest"))
                e.setCancelled(!PlayerInteraction.canInteractWithBlocks(e.getPlayer()));
        } else if (e.getAction() == Action.LEFT_CLICK_BLOCK) {
            if ((e.getClickedBlock().getType() == Material.FIRE ||
                    e.getClickedBlock().getRelative(e.getBlockFace()).getType() == Material.FIRE)
                    && !PlayerInteraction.canBreakBlock(e.getPlayer(), Material.FIRE))
                e.setCancelled(true);
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void interactEntity(PlayerInteractEntityEvent e) {
        if (!PlayerInteraction.canInteractEntity(e.getPlayer())) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void interactArmorStand(PlayerArmorStandManipulateEvent e) {
        if (!PlayerInteraction.canInteractEntity(e.getPlayer())) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void destroyFrameAndPainting(HangingBreakByEntityEvent e) {
        if (!(e.getRemover() instanceof Player)) return;
        Player player = (Player) e.getRemover();
        if (e.getEntity().getType() == EntityType.ITEM_FRAME) {
            if (!PlayerInteraction.canDestroyFrame(player)) e.setCancelled(true);
        } else if (e.getEntity().getType() == EntityType.PAINTING)
            if (!PlayerInteraction.canDestroyPainting(player)) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void itemDrop(PlayerDropItemEvent e) {
        if (!PlayerInteraction.canItemDrop(e.getPlayer())) e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void itemPickup(PlayerPickupItemEvent e) {
        if (!PlayerInteraction.canItemPickup(e.getPlayer())) e.setCancelled(true);
    }
}

package serverlink.player.interaction;

import org.bukkit.Material;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;

@SuppressWarnings("WeakerAccess")
public class PlayerInteraction {
    private static HashMap<Player, ArrayList<Material>> enabledPlaceBlocks = new HashMap<>();
    private static HashMap<Player, ArrayList<Material>> enabledBreakBlocks = new HashMap<>();
    private static ArrayList<Material> allMaterials = new ArrayList<>();
    private static HashMap<Player, Boolean> openChest = new HashMap<>();
    private static HashMap<Player, Boolean> openFurnace = new HashMap<>();
    private static HashMap<Player, Boolean> openCraftingTable = new HashMap<>();
    private static HashMap<Player, Boolean> openEnchantmentTable = new HashMap<>();
    private static HashMap<Player, Boolean> openOtherInventories = new HashMap<>();
    private static HashMap<Player, Boolean> shootBow = new HashMap<>();
    private static HashMap<Player, Boolean> shootEnderpearl = new HashMap<>();
    private static HashMap<Player, Boolean> shootSnowball = new HashMap<>();
    private static HashMap<Player, Boolean> throwPotion = new HashMap<>();
    private static HashMap<Player, Boolean> trampleSoil = new HashMap<>();
    private static HashMap<Player, Boolean> interactWithBlocks = new HashMap<>();
    private static HashMap<Player, Boolean> lightFire = new HashMap<>();
    private static HashMap<Player, Boolean> lightTnt = new HashMap<>();
    private static HashMap<Player, Boolean> destroyFrame = new HashMap<>();
    private static HashMap<Player, Boolean> destroyPainting = new HashMap<>();
    private static HashMap<Player, Boolean> interactEntity = new HashMap<>();
    private static HashMap<Player, Boolean> itemDrop = new HashMap<>();
    private static HashMap<Player, Boolean> itemPickup = new HashMap<>();
    private static HashMap<Player, Boolean> deathExpDrop = new HashMap<>();
    private static HashMap<Player, Boolean> deathItemDrop = new HashMap<>();

    public static void setup() {
        Collections.addAll(allMaterials, Material.values());
    }

    public static void leave(Player player) {
        enabledPlaceBlocks.remove(player);
        enabledBreakBlocks.remove(player);
        openChest.remove(player);
        openFurnace.remove(player);
        openCraftingTable.remove(player);
        openEnchantmentTable.remove(player);
        openOtherInventories.remove(player);
        shootBow.remove(player);
        shootEnderpearl.remove(player);
        shootSnowball.remove(player);
        throwPotion.remove(player);
        trampleSoil.remove(player);
        interactWithBlocks.remove(player);
        lightFire.remove(player);
        lightTnt.remove(player);
        destroyFrame.remove(player);
        destroyPainting.remove(player);
        interactEntity.remove(player);
        itemDrop.remove(player);
        itemPickup.remove(player);
        deathExpDrop.remove(player);
        deathItemDrop.remove(player);
    }

    public static void enablePlacingOfBlock(Player player, Material material) {
        if (!enabledPlaceBlocks.containsKey(player)) enabledPlaceBlocks.put(player, new ArrayList<>());
        if (!enabledPlaceBlocks.get(player).contains(material)) {
            ArrayList<Material> list = enabledPlaceBlocks.get(player);
            list.add(material);
            enabledPlaceBlocks.put(player, list);
        }
    }

    public static void disablePlacingOfBlock(Player player, Material material) {
        if (!enabledPlaceBlocks.containsKey(player)) return;
        if (enabledPlaceBlocks.get(player).contains(material)) enabledPlaceBlocks.get(player).remove(material);
        if (material == Material.WATER || material == Material.WATER_BUCKET) {
            enabledPlaceBlocks.get(player).remove(Material.WATER);
            enabledPlaceBlocks.get(player).remove(Material.WATER_BUCKET);
        } else if (material == Material.LAVA || material == Material.LAVA_BUCKET) {
            enabledPlaceBlocks.get(player).remove(Material.LAVA);
            enabledPlaceBlocks.get(player).remove(Material.LAVA_BUCKET);
        }
    }

    public static void enableBreakingOfBlock(Player player, Material material) {
        if (!enabledBreakBlocks.containsKey(player)) enabledBreakBlocks.put(player, new ArrayList<>());
        if (!enabledBreakBlocks.get(player).contains(material)) {
            ArrayList<Material> list = enabledBreakBlocks.get(player);
            list.add(material);
            enabledBreakBlocks.put(player, list);
        }
    }

    public static void disableBreakingOfBlock(Player player, Material material) {
        if (enabledBreakBlocks.containsKey(player)) return;
        if (enabledBreakBlocks.get(player).contains(material)) enabledBreakBlocks.get(player).remove(material);
    }

    public static void enableAllPlacing(Player player) {
        enabledPlaceBlocks.put(player, allMaterials);
        setLightFire(player, true);
    }

    public static void disableAllPlacing(Player player) {
        enabledPlaceBlocks.remove(player);
        setLightFire(player, false);
    }

    public static void enableAllBreaking(Player player) {
        enabledBreakBlocks.put(player, allMaterials);
        setDestroyFrame(player, true);
        setDestroyPainting(player, true);
    }

    public static void disableAllBreaking(Player player) {
        enabledBreakBlocks.remove(player);
        setDestroyFrame(player, false);
        setDestroyPainting(player, false);
    }

    public static void setOpenChest(Player player, Boolean bool) {
        openChest.put(player, bool);
    }

    public static void setOpenFurnace(Player player, Boolean bool) {
        openFurnace.put(player, bool);
    }

    public static void setOpenCraftingTable(Player player, Boolean bool) {
        openCraftingTable.put(player, bool);
    }

    public static void setOpenEnchantmentTable(Player player, Boolean bool) {
        openEnchantmentTable.put(player, bool);
    }

    public static void setOpenOtherInventories(Player player, Boolean bool) {
        openOtherInventories.put(player, bool);
    }

    public static void setShootBow(Player player, Boolean bool) {
        shootBow.put(player, bool);
    }

    public static void setShootEnderpearl(Player player, Boolean bool) {
        shootEnderpearl.put(player, bool);
    }

    public static void setShootSnowball(Player player, Boolean bool) {
        shootSnowball.put(player, bool);
    }

    public static void setThrowPotion(Player player, Boolean bool) {
        throwPotion.put(player, bool);
    }

    public static void setTrampleSoil(Player player, Boolean bool) {
        trampleSoil.put(player, bool);
    }

    public static void setInteractWithBlocks(Player player, Boolean bool) {
        interactWithBlocks.put(player, bool);
    }

    public static void setLightFire(Player player, Boolean bool) {
        lightFire.put(player, bool);
    }

    public static void setLightTnt(Player player, Boolean bool) {
        lightTnt.put(player, bool);
    }

    public static void setDestroyFrame(Player player, Boolean bool) {
        destroyFrame.put(player, bool);
    }

    public static void setDestroyPainting(Player player, Boolean bool) {
        destroyPainting.put(player, bool);
    }

    public static void setInteractEntity(Player player, Boolean bool) {
        interactEntity.put(player, bool);
    }

    public static void setItemDrop(Player player, Boolean bool) {
        itemDrop.put(player, bool);
    }

    public static void setItemPickup(Player player, Boolean bool) {
        itemPickup.put(player, bool);
    }

    public static void setDeathExpDrop(Player player, Boolean bool) {
        deathExpDrop.put(player, bool);
    }

    public static void setDeathItemDrop(Player player, Boolean bool) {
        deathItemDrop.put(player, bool);
    }

    public static void disableAllInteraction(Player player) {
        disableAllPlacing(player);
        disableAllBreaking(player);
        setOpenChest(player, false);
        setOpenCraftingTable(player, false);
        setOpenEnchantmentTable(player, false);
        setOpenFurnace(player, false);
        setOpenOtherInventories(player, false);
        setShootBow(player, false);
        setShootSnowball(player, false);
        setShootBow(player, false);
        setShootEnderpearl(player, false);
        setThrowPotion(player, false);
        setTrampleSoil(player, false);
        setInteractWithBlocks(player, false);
        setLightFire(player, false);
        setLightTnt(player, false);
        setDestroyFrame(player, false);
        setDestroyPainting(player, false);
        setInteractEntity(player, false);
        setItemDrop(player, false);
        setItemPickup(player, false);
        setDeathExpDrop(player, false);
        setDeathItemDrop(player, false);
    }

    public static void enableAllInteraction(Player player) {
        enableAllPlacing(player);
        enableAllBreaking(player);
        setOpenChest(player, true);
        setOpenCraftingTable(player, true);
        setOpenEnchantmentTable(player, true);
        setOpenFurnace(player, true);
        setOpenOtherInventories(player, true);
        setShootBow(player, true);
        setShootSnowball(player, true);
        setShootBow(player, true);
        setShootEnderpearl(player, true);
        setThrowPotion(player, true);
        setTrampleSoil(player, false);
        setInteractWithBlocks(player, true);
        setLightFire(player, true);
        setLightTnt(player, true);
        setDestroyFrame(player, true);
        setDestroyPainting(player, true);
        setInteractEntity(player, true);
        setItemDrop(player, true);
        setItemPickup(player, true);
        setDeathExpDrop(player, true);
        setDeathItemDrop(player, true);
    }

    public static boolean canBreakBlock(Player player, Material material) {
        return enabledBreakBlocks.containsKey(player) && enabledBreakBlocks.get(player).contains(material);
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public static boolean canPlaceBlock(Player player, Material material) {
        return enabledPlaceBlocks.containsKey(player) && enabledPlaceBlocks.get(player).contains(material);
    }

    public static boolean canOpenChest(Player player) {
        if (!openChest.containsKey(player)) return false;
        return openChest.get(player);
    }

    public static boolean canOpenFurnace(Player player) {
        if (!openFurnace.containsKey(player)) return false;
        return openFurnace.get(player);
    }

    public static boolean canOpenCraftingTable(Player player) {
        if (!openCraftingTable.containsKey(player)) return false;
        return openCraftingTable.get(player);
    }

    public static boolean canOpenEnchantmentTable(Player player) {
        if (!openEnchantmentTable.containsKey(player)) return false;
        return openEnchantmentTable.get(player);
    }

    public static boolean canOpenOtherInventories(Player player) {
        if (!openOtherInventories.containsKey(player)) return false;
        return openOtherInventories.get(player);
    }

    public static boolean canShootEnderpearl(Player player) {
        if (!shootEnderpearl.containsKey(player)) return false;
        return shootEnderpearl.get(player);
    }

    public static boolean canShootSnowball(Player player) {
        if (!shootSnowball.containsKey(player)) return false;
        return shootSnowball.get(player);
    }

    public static boolean canShootBow(Player player) {
        if (!shootBow.containsKey(player)) return false;
        return shootBow.get(player);
    }

    public static boolean canThrowPotion(Player player) {
        if (!throwPotion.containsKey(player)) return false;
        return throwPotion.get(player);
    }

    public static boolean canTrampleSoil(Player player) {
        if (!trampleSoil.containsKey(player)) return false;
        return trampleSoil.get(player);
    }

    public static boolean canInteractWithBlocks(Player player) {
        if (!interactWithBlocks.containsKey(player)) return false;
        return interactWithBlocks.get(player);
    }

    public static boolean canLightFire(Player player) {
        if (!lightFire.containsKey(player)) return false;
        return lightFire.get(player);
    }

    public static boolean canLightTnt(Player player) {
        if (!lightTnt.containsKey(player)) return false;
        return lightTnt.get(player);
    }

    public static boolean canDestroyFrame(Player player) {
        if (!destroyFrame.containsKey(player)) return false;
        return destroyFrame.get(player);
    }

    public static boolean canDestroyPainting(Player player) {
        if (!destroyPainting.containsKey(player)) return false;
        return destroyPainting.get(player);
    }

    public static boolean canInteractEntity(Player player) {
        if (!interactEntity.containsKey(player)) return false;
        return interactEntity.get(player);
    }

    public static boolean canItemDrop(Player player) {
        if (!itemDrop.containsKey(player)) return false;
        return itemPickup.get(player);
    }

    public static boolean canItemPickup(Player player) {
        if (!itemPickup.containsKey(player)) return false;
        return itemPickup.get(player);
    }

    public static boolean canDeathExpDrop(Player player) {
        if (!deathExpDrop.containsKey(player)) return false;
        return deathExpDrop.get(player);
    }

    public static boolean canDeathItemDrop(Player player) {
        if (!deathItemDrop.containsKey(player)) return false;
        return deathItemDrop.get(player);
    }
}

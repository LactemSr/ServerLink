package serverlink.player.team;

import org.bukkit.entity.Player;
import serverlink.chat.chat;

import java.util.ArrayList;
import java.util.List;

public class Team {
    private String name;
    private TeamColor teamColor;
    private List<Player> players = new ArrayList<>();
    private int maxPlayers;

    Team(String name, int maxPlayers, TeamColor teamColor) {
        this.name = name;
        this.teamColor = teamColor;
        this.maxPlayers = maxPlayers;
    }

    public TeamColor getTeamColor() {
        return teamColor;
    }

    public int getMaxPlayers() {
        return maxPlayers;
    }

    public void setMaxPlayers(int maxPlayers) {
        this.maxPlayers = maxPlayers;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = chat.color(name);
    }

    public boolean isOnTeam(Player player) {
        return players.contains(player);
    }

    public boolean isFull() {
        return players.size() >= maxPlayers;
    }

    public void addPlayer(Player player) {
        players.add(player);
    }

    public void removePlayer(Player player) {
        players.remove(player);
    }

    public List<Player> getPlayers() {
        return players;
    }
}

package serverlink.player.team;

import com.google.common.collect.Lists;
import org.bukkit.entity.Player;
import serverlink.alphaplayer.PlayerManager;
import serverlink.player.Friends;
import serverlink.player.party.PartyManager;
import serverlink.round.Round;
import serverlink.util.Probability;

import java.util.*;
import java.util.stream.Collectors;

public class TeamManager implements Round {
    public static final Map<TeamColor, List<Player>> teamWaitLists = new HashMap<>();
    private static List<Team> teams = new ArrayList<>();
    private static List<TeamColor> activeTeamColors = new ArrayList<>();
    private static Map<Team, Integer> teamDeficits = new HashMap<>();
    private static int teamSizes = 5;
    private static boolean teamGame = false;

    /**
     * Mark this gametype as a team game
     */
    public static void setTeamGame(int teamSizes, TeamColor... teamColors) {
        teamGame = true;
        TeamManager.teamSizes = teamSizes;
        activeTeamColors.clear();
        Collections.addAll(activeTeamColors, teamColors);
        resetTeams();
    }

    public static boolean isTeamGame() {
        return teamGame;
    }

    /**
     * @return the number of players per team (limit not enforced)
     */
    public static int getTeamSizes() {
        return teamSizes;
    }

    /**
     * Active teams are the teams that are available for use in this gametype
     */
    public static List<Team> getActiveTeams() {
        return teams;
    }

    /**
     * Active teams are the teams that are available for use in this gametype
     */
    public static List<TeamColor> getActiveTeamColors() {
        return activeTeamColors;
    }

    /**
     * Only call this from the main thread.
     */
    public static Team getTeam(Player player) {
        for (Team team : teams) {
            if (team.isOnTeam(player)) return team;
        }
        return null;
    }

    /**
     * Only call this from the main thread.
     */
    public static Team getTeam(TeamColor teamColor) {
        for (Team team : teams) {
            if (team.getTeamColor() == teamColor) return team;
        }
        return null;
    }

    /**
     * A team's deficit is the number of players it has less than the most populated team.
     * Deficit can never be negative, but it can be zero if all teams are equally populated.
     */
    public static void updateTeamDeficits() {
        // Record how many more players other teams have than each team
        teamDeficits = new HashMap<>();
        for (Team team : teams) {
            teamDeficits.put(team, 0);
            for (Team comparison : new ArrayList<>(teams)) {
                if (team.getTeamColor() == comparison.getTeamColor()) continue;
                int deficit = comparison.getPlayers().size() - team.getPlayers().size();
                if (deficit > 0 && comparison.getPlayers().size() > 0)
                    teamDeficits.put(team, teamDeficits.get(team) + deficit);
            }
        }
    }

    /**
     * A team's deficit is the number of players it has less than the most populated team.
     * Deficit can never be negative, but it can be zero if all teams are equally populated.
     */
    public static int getTeamDeficit(TeamColor teamColor) {
        return teamDeficits.get(TeamManager.getTeam(teamColor));
    }

    /**
     * @return a list of players waiting to join the teamColor team
     */
    public static List<Player> getWaitList(TeamColor teamColor) {
        return teamWaitLists.get(teamColor);
    }

    public static void clearWaitLists() {
        teamWaitLists.values().forEach(List::clear);
    }

    /**
     * Assigns player a team in the most balanced way possible
     */
    public static void assignTeam(Player player) {
        updateTeamDeficits();
        if (getTeam(player) != null && getActiveTeams().contains(getTeam(player))) return;
        // Find the team with the largest gap in players compared to other teams (deficit)
        List<Team> highestDeficitTeams = Lists.newArrayList(teams.get(0));
        for (Team team : teams) {
            if (teamDeficits.get(team) >= teamDeficits.get(highestDeficitTeams.get(0))) {
                highestDeficitTeams.add(team);
                // Prune teams that have lower deficits (only store the teams that tie for #1 deficit)
                int highestDeficit = getTeamDeficit(team.getTeamColor());
                for (Team team1 : new ArrayList<>(highestDeficitTeams)) {
                    if (getTeamDeficit(team1.getTeamColor()) < highestDeficit)
                        highestDeficitTeams.remove(team1);
                }
            }
        }
        // From among the highest deficit teams, assign more priority to teams with emptier wait
        // lists (fewer people trying to get in) and with more people trying to get out
        Map<Team, Integer> teamPriorities = new HashMap<>();
        Team highestPriorityTeam = highestDeficitTeams.get(0);
        for (Team team : highestDeficitTeams) {
            int priority = 0;
            // Lower priority for players trying to get into this team
            priority -= teamWaitLists.get(team.getTeamColor()).size();
            // Raise priority for players trying to get out of this team
            for (TeamColor teamColor : activeTeamColors) {
                if (teamColor == team.getTeamColor()) continue;
                for (Player swapper : teamWaitLists.get(teamColor)) {
                    if (getTeam(swapper).getTeamColor() == team.getTeamColor()) priority++;
                }
            }
            // Raise priority to the highest if a party member or friend is on this team
            List<UUID> partyMembers = PartyManager.getPartyObject(PlayerManager.get(player));
            List<UUID> friends = Friends.getFriendsObject(PlayerManager.get(player));
            for (Player teamPlayer : team.getPlayers()) {
                if (partyMembers.contains(teamPlayer.getUniqueId())) priority += 1000;
                if (friends.contains(teamPlayer.getUniqueId())) priority += 100;
            }
            teamPriorities.put(team, priority);
            if (priority > teamPriorities.get(highestPriorityTeam))
                highestPriorityTeam = team;
            else if (priority == teamPriorities.get(highestPriorityTeam) && Probability.get(50))
                highestPriorityTeam = team;
        }
        // Put player on the chosen team
        PlayerManager.get(player).setTeam(highestPriorityTeam);
        PlayerManager.get(player).updateNameFormatting();
        // Now check the wait lists for everyone trying to get off highestPriorityTeam
        // and change their teams if highestPriorityTeam now has more players than the
        // waiting player's desired team
        updateTeamDeficits();
        List<TeamColor> teamsWithWaitLists = teamWaitLists.keySet().stream().filter(teamColor -> !teamWaitLists.get(teamColor).isEmpty()).collect(Collectors.toList());
        if (!teamsWithWaitLists.isEmpty()) {
            Collections.shuffle(teamsWithWaitLists);
            for (TeamColor teamColor : teamsWithWaitLists) {
                Player waiter = teamWaitLists.get(teamColor).get(0);
                if (getTeam(waiter) == highestPriorityTeam
                        && getTeamDeficit(teamColor) < getTeamDeficit(highestPriorityTeam.getTeamColor())) {
                    teamWaitLists.get(teamColor).remove(waiter);
                    PlayerManager.get(waiter).setTeam(getTeam(teamColor));
                    PlayerManager.get(waiter).updateNameFormatting();
                    return;
                }
            }
        }
    }

    private static void resetTeams() {
        teams.clear();
        for (TeamColor teamColor : activeTeamColors) {
            teams.add(new Team(teamColor.name().replace("_", ""), teamSizes, teamColor));
            teamWaitLists.put(teamColor, new ArrayList<>());
        }
        updateTeamDeficits();
    }

    @Override
    public void onLobbyCountdownStart() {

    }

    @Override
    public void onLobbyCountdownCancel() {

    }

    @Override
    public void onMapCountdownStart() {
        new HashSet<>(teamWaitLists.keySet()).forEach(teamWaitLists::remove);
    }

    @Override
    public void onMapCountdownCancel() {

    }

    @Override
    public void onGameStart() {

    }

    @Override
    public void onEndCountdownStart() {

    }

    @Override
    public void onRoundEnd() {
        resetTeams();
    }
}

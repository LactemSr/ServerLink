package serverlink.player.hacking;

public enum HackType {
    ACCURACY(75),
    ANGLE(25),
    BOW(75),
    FAST_BREAK(50),
    FAST_CLICK(75),
    FAST_PLACE(50),
    FLIGHT(25),
    HEADMOVE(25),
    HIT_THROUGH_INVENTORY(100),
    NO_FALL(75),
    NO_SWING(75),
    PHASE_OR_TELEPORT(50),
    REGEN(75),
    SPEED_OR_SPIDER_OR_STEP(25),
    TOO_MANY_PACKETS(50);

    private int points;

    HackType(int points) {
        this.points = points;
    }

    int getPoints() {
        return points;
    }
}

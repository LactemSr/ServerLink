package serverlink.player.hacking;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerKickEvent;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.player.Settings;
import serverlink.util.TimeUtils;

import java.util.ArrayList;
import java.util.List;

public class Hacking implements Listener {
    /**
     * Registers player as failing hackType.
     * <p>
     * Punishment occurs when a player's points from the last 5 minutes add up to 100.
     * <p>
     * Failing the same hack multiple times in 5 minutes does not increase a player's points.
     */
    public static void failHack(Player player, HackType hackType) {
        if (player.hasMetadata("NPC")) return;
        AlphaPlayer aPlayer = PlayerManager.get(player);
        // prune hacks that happened over 5 minutes ago
        List<String> recentlyFailedHacks = aPlayer.getListSetting("recentlyFailedHacks");
        for (String hackData : new ArrayList<>(recentlyFailedHacks)) {
            if (hackData.split("-")[0].equals(hackType.name())) {
                recentlyFailedHacks.remove(hackData);
            } else if (TimeUtils.minutesBetween(Integer.parseInt(hackData.split("-")[1]),
                    TimeUtils.getCurrent()) > 5) {
                recentlyFailedHacks.remove(hackData);
            }
        }
        recentlyFailedHacks.add(hackType.name() + "-" + TimeUtils.getCurrent());
        // add up points and check to punish()
        int points = 0;
        for (String hackData : recentlyFailedHacks) {
            points += HackType.valueOf(hackData.split("-")[0]).getPoints();
        }
        if (points >= 100) {
            recentlyFailedHacks.clear();
            aPlayer.setSetting_SyncOnLogout("recentlyFailedHacks", null);
            punish(player);
        } else {
            aPlayer.setSetting_SyncOnLogout("recentlyFailedHacks", Settings.listToString(recentlyFailedHacks));
        }
    }

    private static void punish(Player player) {
        AlphaPlayer aPlayer = PlayerManager.get(player);
        Integer strikes = aPlayer.getIntegerSetting("totalHackingStrikes");
        if (strikes == null) strikes = 0;
        strikes++;
        aPlayer.setSetting_SyncOnLogout("totalHackingStrikes", String.valueOf(strikes));
        String command;
        String announcement;
        if (strikes >= 3) {
            command = "7 d";
            announcement = "7 days";
        } else if (strikes == 2) {
            command = "24 h";
            announcement = "24 hours";
        } else {
            command = "1 h";
            announcement = "1 hour";
        }
        Bukkit.getServer().broadcastMessage(chat.color(player.getDisplayName() +
                " &c was banned for &e" + announcement + "&c for &dcheating!"));
        Bukkit.dispatchCommand(Bukkit.getConsoleSender(), "ban " + player.getName() + " " +
                command + " cheating");
    }

    @EventHandler
    public void onKickForTooManyPackets(PlayerKickEvent e) {
        if (e.getLeaveMessage().toLowerCase().contains("too many packets")
                || e.getReason().toLowerCase().contains("too many packets")) {
            failHack(e.getPlayer(), HackType.TOO_MANY_PACKETS);
        }
    }
}

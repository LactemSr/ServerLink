package serverlink.player;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.customevent.CustomGametypePluginLoadEvent;
import serverlink.network.JedisPool;
import serverlink.server.ServerType;
import serverlink.util.TimeUtils;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.UUID;

public class Points implements Listener {
    private static Integer idGainedPoints;
    // DecimalFormat is *probably* thread-safe when you only call .format()
    private static DecimalFormat thousand = new DecimalFormat("###.##");
    private static DecimalFormat million = new DecimalFormat("0.###");

    private static void setup() {
        thousand.setRoundingMode(RoundingMode.HALF_DOWN);
        million.setRoundingMode(RoundingMode.HALF_DOWN);
        if (ServerLink.getServerType() == ServerType.hub) return;
        if (idGainedPoints != null) return;
        else cancelTasks();
        idGainedPoints = new BukkitRunnable() {
            // calculate and give players exp every 5 minutes
            @Override
            public void run() {
                int now = TimeUtils.getCurrent();
                for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
                    int nextCheckTime = aPlayer.getNextExpCheckTime();
                    if (nextCheckTime == 0 || now >= nextCheckTime) {
                        aPlayer.setNextExpCheckTime(now + (60 * 5));
                        // calculate how much exp to give
                        int gainedPoints = aPlayer.getGainedPoints();
                        if (gainedPoints < 5) aPlayer.addExp(10);
                        else if (gainedPoints < 50) aPlayer.addExp(25);
                        else if (gainedPoints < 100) aPlayer.addExp(40);
                        else if (gainedPoints < 200) aPlayer.addExp(75);
                        else if (gainedPoints < 500) aPlayer.addExp(250);
                        else if (gainedPoints < 750) aPlayer.addExp(350);
                        else aPlayer.addExp(400);

                        // leave some gained points so players can continuously earn exp
                        if (aPlayer.getGainedPoints() >= 1500) aPlayer.setGainedPoints(100);
                        else if (aPlayer.getGainedPoints() >= 500) aPlayer.setGainedPoints(70);
                        else if (aPlayer.getGainedPoints() >= 100) aPlayer.setGainedPoints(20);
                        else aPlayer.setGainedPoints(((Double) (aPlayer.getGainedPoints() * .3)).intValue());
                    }
                }
            }
        }.runTaskTimerAsynchronously(ServerLink.plugin, 0, 20 * 20).getTaskId();
    }

    private static void cancelTasks() {
        if (idGainedPoints != null) Bukkit.getScheduler().cancelTask(idGainedPoints);
        idGainedPoints = null;
    }

    /*
     * events ^^^^^
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 * methods vvvvv
	 */

    public static String formatPoints(int points) {
        if (points < 1000) return "&e" + ((Integer) points).toString();
        else if (points < 1000000) return "&e" + thousand.format((double) points / 1000) + "&d&ok";
        else return "&e" + million.format((double) points / 1000000) + "&c&omil";
    }

    public static String getOfflineGainedPointsString(String uuid) {
        Jedis jedis = JedisPool.getConn();
        String points = jedis.hget(uuid, "gainedPoints");
        JedisPool.close(jedis);
        Integer iPoints;
        try {
            iPoints = Integer.parseInt(points);
        } catch (NumberFormatException e1) {
            iPoints = 0;
        }
        if (iPoints < 0) return "0";
        else if (iPoints < 1000) return "&e" + iPoints.toString();
        else if (iPoints < 1000000) return "&e" + thousand.format(iPoints / 1000) + "&d&ok";
        else return "&e" + million.format(iPoints / 1000000) + "&c&omil";
    }

    public static int getOfflineGainedPointsInt(String uuid) {
        Jedis jedis = JedisPool.getConn();
        String points = jedis.hget(uuid, "gainedPoints");
        int iPoints;
        try {
            iPoints = Integer.parseInt(points);
        } catch (NumberFormatException e1) {
            iPoints = 0;
        }
        JedisPool.close(jedis);
        return iPoints;
    }

    public static void setOfflineGainedPoints(String uuid, Integer points) {
        Jedis jedis = JedisPool.getConn();
        jedis.hset(uuid, "gainedPoints", points.toString());
        JedisPool.close(jedis);
    }

    public static String getOfflinePointsString(String uuid, String gameType) {
        Jedis jedis = JedisPool.getConn();
        String points = jedis.hget(uuid, gameType.toLowerCase() + "Points");
        JedisPool.close(jedis);
        Integer iPoints;
        try {
            iPoints = Integer.parseInt(points);
        } catch (NumberFormatException e1) {
            iPoints = 0;
        }
        if (iPoints < 0) return "&e0";
        if (iPoints < 1000) {
            return "&e" + iPoints.toString();
        } else if (iPoints < 1000000) {
            BigDecimal decimalPoints = new BigDecimal((double) iPoints / 1000).setScale(2, BigDecimal.ROUND_HALF_DOWN);
            return "&e" + decimalPoints.toString().concat("&d&ok");
        } else {
            BigDecimal decimalPoints = new BigDecimal((double) iPoints / 1000000).setScale(3, BigDecimal.ROUND_HALF_DOWN);
            return "&e" + decimalPoints.toString().concat("&c&omil");
        }
    }

    public static int getOfflinePointsInt(String uuid, String gameType) {
        Jedis jedis = JedisPool.getConn();
        String points = jedis.hget(uuid, gameType.toLowerCase() + "Points");
        int iPoints;
        try {
            iPoints = Integer.parseInt(points);
        } catch (NumberFormatException e1) {
            iPoints = 0;
        }
        JedisPool.close(jedis);
        return iPoints;
    }

    public static void addPoints(String uuid, String gameType, int pointsToAdd) {
        Player player = Bukkit.getPlayer(UUID.fromString(uuid));
        if (player != null && player.isOnline()) {
            PlayerManager.get(player.getUniqueId()).addPoints(pointsToAdd);
            return;
        }
        Jedis jedis = JedisPool.getConn();
        String currentPoints = jedis.hget(uuid, gameType.toLowerCase() + "Points");
        if (currentPoints == null)
            jedis.hset(uuid, gameType.toLowerCase() + "Points", ((Integer) pointsToAdd).toString());
        else
            jedis.hset(uuid, gameType.toLowerCase() + "Points",
                    ((Integer) (Integer.parseInt(currentPoints) + pointsToAdd)).toString());
        JedisPool.close(jedis);
    }

    public static void takePoints(String uuid, String gameType, int pointsToTake) {
        Player player = Bukkit.getPlayer(UUID.fromString(uuid));
        if (player != null && player.isOnline()) {
            PlayerManager.get(player.getUniqueId()).takePoints(pointsToTake);
            return;
        }
        Jedis jedis = JedisPool.getConn();
        String currentPoints = jedis.hget(uuid, gameType.toLowerCase() + "Points");
        if (currentPoints == null || Integer.parseInt(currentPoints) - pointsToTake < 0)
            jedis.hset(uuid, gameType.toLowerCase() + "Points", "0");
        else
            jedis.hset(uuid, gameType.toLowerCase() + "Points",
                    ((Integer) (Integer.parseInt(currentPoints) - pointsToTake)).toString());
        JedisPool.close(jedis);
    }

    @EventHandler
    private void onGametypeLoad(CustomGametypePluginLoadEvent e) {
        if (e.getServertype().equals("hub")) cancelTasks();
        else setup();
    }
}

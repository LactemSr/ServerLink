package serverlink.player;


import serverlink.ServerLink;
import serverlink.network.JedisPool;
import serverlink.network.nmessage;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;

import java.util.Date;
import java.util.UUID;

public class RankExpiry {
    // tiers are 1-4, corresponding to each of the buyable ranks

    /**
     * This will return null if the expiry date has passed or was never set
     * A player can only ever have an expiry date for one tier. Adding/Subtracting time from another tier will
     * affect the current tier (by a logically adjusted amount)
     */
    public static Date getExpiryDate(String uuid, int tier, Jedis jedis) {
        if (tier < 2 || tier > 5) return null;
        String timeString = jedis.hget(uuid, "rank" + tier + "Expiry");
        try {
            long time = Long.parseLong(timeString);
            if (time == 0) return null;
            return new Date(time * 1000);
        } catch (NumberFormatException e) {
            return null;
        }
    }


    /**
     * Adding points to tier higher than player already has: points from lower tiers are logically applied to higher tier
     * Adding points to tier lower than player already has: points from lower tier are logically added to the current, higher tier
     * This means that if a player wants to be demoted, he/she needs to wait until his/her rank expires.
     * But if he/she accidentally purchases a lower rank, it will just extend his/her current rank.
     */
    public static void addTime(String uuid, int tier, int days) {
        double now = System.currentTimeMillis() / 1000.0;
        Jedis jedis = JedisPool.getConn();
        double current5;
        try {
            current5 = Long.parseLong(jedis.hget(uuid, "rank5Expiry"));
        } catch (NumberFormatException e) {
            jedis.hset(uuid, "rank5Expiry", "0");
            current5 = 0;
        }
        double current4;
        try {
            current4 = Long.parseLong(jedis.hget(uuid, "rank4Expiry"));
        } catch (NumberFormatException e) {
            jedis.hset(uuid, "rank4Expiry", "0");
            current4 = 0;
        }
        double current3;
        try {
            current3 = Long.parseLong(jedis.hget(uuid, "rank3Expiry"));
        } catch (NumberFormatException e) {
            jedis.hset(uuid, "rank3Expiry", "0");
            current3 = 0;
        }
        double current2;
        try {
            current2 = Long.parseLong(jedis.hget(uuid, "rank2Expiry"));
        } catch (NumberFormatException e) {
            jedis.hset(uuid, "rank2Expiry", "0");
            current2 = 0;
        }
        jedis.hset(uuid, "rank1Expiry", "0");
        // If the player has no rank or a permanent rank (6-10), the math below will make set multiplierCurrent to 0
        // Setting currentTier to 1 doesn't mean it is definitely 1, but it will calculate so that no extra points
        // from another tier are added (multiplierCurrent will be 0).
        int currentTier = 1;
        if (current5 > 0) {
            currentTier = 5;
            current5 = current5 - now;
        } else if (current4 > 0) {
            currentTier = 4;
            current4 = current4 - now;
        } else if (current3 > 0) {
            currentTier = 3;
            current3 = current3 - now;
        } else if (current2 > 0) {
            currentTier = 2;
            current2 = current2 - now;
        }
        if (currentTier == 1) {
            jedis.hset(uuid, "rank" + tier + "Expiry", ((Integer) ((Double) ((days * 86400) + now)).intValue()).toString());
        } else {
            // the formulas for the multipliers give the player's current points a lighter or heavier weight
            // depending on the difference between the inputted rank and the player's current rank
            double multiplier2 = 1 - (0.25 * (tier - 2));
            double multiplier3 = 1 - (0.25 * (tier - 3));
            double multiplier4 = 1 - (0.25 * (tier - 4));
            double multiplier5 = 1 - (0.25 * (tier - 5));
            Double newSeconds = (days * 86400) + (multiplier2 * current2) + (multiplier3 * current3)
                    + (multiplier4 + current4) + (multiplier5 * current5);
            jedis.hset(uuid, "rank" + tier + "Expiry", ((Integer) ((Double) (newSeconds + now)).intValue()).toString());
        }
        if (tier != 5) jedis.hset(uuid, "rank5Expiry", "0");
        if (tier != 4) jedis.hset(uuid, "rank4Expiry", "0");
        if (tier != 3) jedis.hset(uuid, "rank3Expiry", "0");
        if (tier != 2) jedis.hset(uuid, "rank2Expiry", "0");

        // check to change rank and reload permissions
        if (tier == currentTier) return;
        // former rank could have been above 5, so currentTier is not necessarily formerRank
        int formerRank;
        try {
            formerRank = Integer.parseInt(jedis.hget(uuid, "rank"));
        } catch (NumberFormatException e) {
            // if the player has never joined before
            formerRank = 1;
        }
        if (formerRank == tier) {
            JedisPool.close(jedis);
            return;
        }
        jedis.lrem("perms:" + uuid, 0, "rank." + formerRank);
        jedis.hdel("settings:" + uuid, "chatFormat", "nameFormat");
        // not a typo. Setting this to 0 makes sure the Forums synchronization keeps trying to sync until it's successful
        jedis.hset(uuid, "rankOnForums", "0");
        jedis.hset(uuid, "rank", ((Integer) tier).toString());
        jedis.lpush("perms:" + uuid, "rank." + tier);
        Player player = Bukkit.getPlayer(UUID.fromString(uuid));
        if (player != null && player.isOnline()) new BukkitRunnable() {
            @Override
            public void run() {
                Permissions.reloadPermissions(player);
            }
        }.runTask(ServerLink.plugin);
        else nmessage.sendMessage(jedis.hget(uuid, "server"), "reloadPermissions:" + uuid, jedis);
        JedisPool.close(jedis);
    }

    /**
     * Since this function should only be used for refunding and the special case of a donor wanting to become
     * a lower donor, it simply subtracts the inputted number of days from the player's current rank
     * (it it's rank 2-5).
     */
    public static void subtractTime(String uuid, int tier, int currentExpiry, int days, Jedis jedis) {
        Integer newExpiry = currentExpiry - (days * 86400);
        jedis.hset(uuid, "rank" + tier + "Expiry", newExpiry.toString());
    }
}
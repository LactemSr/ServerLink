package serverlink.player;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import mkremins.fanciful.FancyMessage;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.customevent.CustomNetworkMessageEvent;
import serverlink.network.JedisPool;
import serverlink.network.nmessage;
import serverlink.server.Messages;
import serverlink.util.TimeUtils;

import java.util.UUID;

public class TreasureManager implements Listener {

    public static void findMini(AlphaPlayer aPlayer) {
        aPlayer.getPlayer().sendMessage(chat.color(Messages.treasure_find_mini));
        aPlayer.miniChests.add(TimeUtils.nDaysLater_String(14));
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                jedis.lpush("miniChests:" + aPlayer.getUuid().toString(),
                        TimeUtils.nDaysLater_String(14));
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public static void findEpic(AlphaPlayer aPlayer) {
        aPlayer.getPlayer().sendMessage(chat.color(Messages.treasure_find_epic));
        aPlayer.epicChests.add(TimeUtils.nDaysLater_String(14));
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                jedis.lpush("epicChests:" + aPlayer.getUuid().toString(),
                        TimeUtils.nDaysLater_String(14));
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public static void findLegendary(AlphaPlayer aPlayer) {
        aPlayer.getPlayer().sendMessage(chat.color(Messages.treasure_find_legendary));
        for (AlphaPlayer mP1 : PlayerManager.getOnlineAlphaPlayers()) {
            mP1.getPlayer().sendMessage(chat.color(Messages.treasure_find_announce_legendary.replace("%name%", aPlayer.getPlayer().getDisplayName())));
            mP1.getPlayer().playSound(mP1.getPlayer().getLocation(), Sound.IRONGOLEM_DEATH, 10, 1);
        }
        aPlayer.legendaryChests.add(TimeUtils.nDaysLater_String(14));
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                jedis.lpush("legendaryChests:" + aPlayer.getUuid().toString(),
                        TimeUtils.nDaysLater_String(14));
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public static void addMini(@NotNull String uuid, @NotNull Integer numberOfChests, @Nullable Integer daysToExpire) {
        Jedis jedis = JedisPool.getConn();
        String server = jedis.hget(uuid, "server");
        if (server != null && jedis.hget(uuid, "online").equals("true"))
            nmessage.sendMessage(server, "buyChests:" + uuid + ":mini:" + numberOfChests + ":" + daysToExpire, jedis);
        else
            addOfflineChests(uuid, "mini", numberOfChests, daysToExpire, jedis);
        JedisPool.close(jedis);
    }

    public static void addEpic(@NotNull String uuid, @NotNull Integer numberOfChests, @Nullable Integer daysToExpire) {
        Jedis jedis = JedisPool.getConn();
        String server = jedis.hget(uuid, "server");
        if (server != null && !server.isEmpty())
            nmessage.sendMessage(server, "buyChests:" + uuid + ":epic:" + numberOfChests + ":" + daysToExpire, jedis);
        else
            addOfflineChests(uuid, "epic", numberOfChests, daysToExpire, jedis);
        JedisPool.close(jedis);
    }

    public static void addLegendary(@NotNull String uuid, @NotNull Integer numberOfChests, @Nullable Integer daysToExpire) {
        Jedis jedis = JedisPool.getConn();
        String server = jedis.hget(uuid, "server");
        if (server != null && !server.isEmpty())
            nmessage.sendMessage(server, "buyChests:" + uuid + ":legendary:" + numberOfChests + ":" + daysToExpire, jedis);
        else
            addOfflineChests(uuid, "legendary", numberOfChests, daysToExpire, jedis);
        JedisPool.close(jedis);
    }

    private static void addChests(Player player, @NotNull String chestType, @NotNull Integer numberOfChests, @Nullable Integer daysToExpire) {
        chestType = chestType.toLowerCase();
        AlphaPlayer aPlayer = PlayerManager.get(player.getUniqueId());
        String uuid = aPlayer.getUuid().toString();
        Jedis jedis = JedisPool.getConn();
        if (daysToExpire == null || daysToExpire <= 0) daysToExpire = 1000;
        for (int i = 0; i < numberOfChests; i++) {
            String expireTime = TimeUtils.nDaysLater_String(daysToExpire);
            jedis.lpush(chestType + "Chests:" + uuid,
                    expireTime);
            switch (chestType) {
                case "mini":
                    aPlayer.miniChests.add(expireTime);
                    break;
                case "epic":
                    aPlayer.epicChests.add(expireTime);
                    break;
                case "legendary":
                    aPlayer.legendaryChests.add(expireTime);
                    break;
            }
        }
        JedisPool.close(jedis);
        if (numberOfChests == 1) {
            if (chestType.equals("epic")) {
                player.sendMessage(chat.color(chat.getServer() + "&aYou received an &e&l"
                        + chestType.toUpperCase() + " CHEST!"));
            } else {
                player.sendMessage(chat.color(chat.getServer() + "&aYou received a &e&l"
                        + chestType.toUpperCase() + " CHEST!"));
            }
        } else player.sendMessage(chat.color(chat.getServer() + "&aYou received " +
                numberOfChests + " &e&l" + chestType.toUpperCase() + " CHESTS!"));
    }

    private static void addOfflineChests(String uuid, @NotNull String chestType, @NotNull Integer numberOfChests, @Nullable Integer daysToExpire, Jedis jedis) {
        chestType = chestType.toLowerCase();
        if (daysToExpire == null || daysToExpire <= 0) daysToExpire = 21;
        for (int i = 0; i < numberOfChests; i++) {
            String expireTime = TimeUtils.nDaysLater_String(daysToExpire);
            jedis.lpush(chestType + "Chests:" + uuid, expireTime);
        }
        FancyMessage notification;
        if (numberOfChests == 1)
            if (chestType.equals("epic")) {
                notification = new FancyMessage("You received an ").color(ChatColor.GREEN)
                        .then(chestType.toUpperCase() + " CHEST!").color(ChatColor.YELLOW).style(ChatColor.BOLD);

            } else {
                notification = new FancyMessage("You received a ").color(ChatColor.GREEN)
                        .then(chestType.toUpperCase() + " CHEST!").color(ChatColor.YELLOW).style(ChatColor.BOLD);
            }
        else
            notification = new FancyMessage("You received " + numberOfChests + " ")
                    .color(ChatColor.GREEN).then(chestType.toUpperCase() + " CHESTS!")
                    .color(ChatColor.YELLOW).style(ChatColor.BOLD);
        Notifications.sendFancyNotification(uuid, notification, jedis, 60 * 60 * 24 * 30 * 6);
    }

    @EventHandler
    private void onNetworkMessage(CustomNetworkMessageEvent e) {
        String message = e.getMessage();
        if (!message.split(":")[0].equals("buyChests")) return;
        String playerUuid = message.split(":")[1];
        String chestType = message.split(":")[2];
        int numberOfChests = Integer.parseInt(message.split(":")[3]);
        int daysToExpire = Integer.parseInt(message.split(":")[4]);
        Player player = Bukkit.getPlayer(UUID.fromString(playerUuid));
        if (player == null || !player.isOnline()) {
            Jedis jedis = JedisPool.getConn();
            if (jedis.hget(playerUuid, "online").equals("true"))
                nmessage.sendMessage(jedis.hget(playerUuid, "server"), message, jedis);
            else
                addOfflineChests(playerUuid, chestType, numberOfChests, daysToExpire, jedis);
            JedisPool.close(jedis);
            return;
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                addChests(player, chestType, numberOfChests, daysToExpire);
            }
        }.runTask(ServerLink.plugin);
    }
}

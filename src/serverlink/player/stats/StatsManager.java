package serverlink.player.stats;

import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.bot.BotManager;
import serverlink.network.JedisPool;

import java.util.*;

public class StatsManager {
    private static final HashMap<String, Ranking> cachedTopAlltime = new HashMap<>();
    private static final HashMap<String, Ranking> cachedTopWeekly = new HashMap<>();
    private static final HashMap<String, Ranking> cachedTopDaily = new HashMap<>();
    private static Calendar cal = Calendar.getInstance();
    private static int tomorrowStart;
    private static int nextWeekStart;

    public static void setup() {
        cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("America/New_York"));
        cal.setFirstDayOfWeek(cal.get(Calendar.MONDAY));
        cal.add(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek() - cal.get(Calendar.DAY_OF_WEEK));
        cal.add(Calendar.DAY_OF_YEAR, 6);
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.getActualMaximum(Calendar.HOUR_OF_DAY), cal.getActualMaximum(Calendar.MINUTE), cal.getActualMaximum(Calendar.SECOND));
        nextWeekStart = ((Long) ((cal.getTimeInMillis() + 1000) / 1000)).intValue();
        cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("America/New_York"));
        cal.setFirstDayOfWeek(cal.get(Calendar.MONDAY));
        cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.getActualMaximum(Calendar.HOUR_OF_DAY), cal.getActualMaximum(Calendar.MINUTE), cal.getActualMaximum(Calendar.SECOND));
        tomorrowStart = ((Long) ((cal.getTimeInMillis() + 1000) / 1000)).intValue();
        new BukkitRunnable() {
            @Override
            public void run() {
                cal = Calendar.getInstance();
                cal.setTimeZone(TimeZone.getTimeZone("America/New_York"));
                cal.setFirstDayOfWeek(cal.get(Calendar.MONDAY));
                Jedis jedis = JedisPool.getConn();
                try {
                    synchronized (cachedTopDaily) {
                        cachedTopDaily.clear();
                        for (String key : jedis.keys("dailyStatRankings:*")) {
                            String stat = key.split(":")[1];
                            Set<String> top = jedis.zrevrange(key, 0, 9);
                            String[] topArray = top.toArray(new String[top.size()]);
                            StatEntry[] stats = new StatEntry[top.size()];
                            for (int i = 0; i < top.size(); i++) {
                                stats[i] = new StatEntry(jedis.hget(topArray[i], "username"),
                                        jedis.zscore(key, topArray[i]));
                                if (stats[i].getPlayerName() == null || stats[i].getPlayerName().trim().toLowerCase().equals("null")) {
                                    jedis.zrem(key, topArray[i]);
                                    stats[i] = null;
                                }
                                for (int j = 1; j < top.size(); j++) {
                                    if (stats[j - 1] == null && stats.length > j) {
                                        stats[j - 1] = stats[j];
                                        stats[j] = null;
                                    }
                                }
                            }
                            cachedTopDaily.put(stat, new Ranking(stats));
                        }
                    }
                    synchronized (cachedTopWeekly) {
                        cachedTopWeekly.clear();
                        for (String key : jedis.keys("weeklyStatRankings:*")) {
                            String stat = key.split(":")[1];
                            Set<String> top = jedis.zrevrange(key, 0, 9);
                            String[] topArray = top.toArray(new String[top.size()]);
                            StatEntry[] stats = new StatEntry[top.size()];
                            for (int i = 0; i < top.size(); i++) {
                                stats[i] = new StatEntry(jedis.hget(topArray[i], "username"),
                                        jedis.zscore(key, topArray[i]));
                                if (stats[i].getPlayerName() == null || stats[i].getPlayerName().trim().toLowerCase().equals("null")) {
                                    jedis.zrem(key, topArray[i]);
                                    stats[i] = null;
                                }
                                for (int j = 1; j < top.size(); j++) {
                                    if (stats[j - 1] == null && stats.length > j) {
                                        stats[j - 1] = stats[j];
                                        stats[j] = null;
                                    }
                                }
                            }
                            cachedTopWeekly.put(stat, new Ranking(stats));
                        }
                    }
                    synchronized (cachedTopAlltime) {
                        cachedTopAlltime.clear();
                        for (String key : jedis.keys("alltimeStatRankings:*")) {
                            String stat = key.split(":")[1];
                            Set<String> top = jedis.zrevrange(key, 0, 9);
                            String[] topArray = top.toArray(new String[top.size()]);
                            StatEntry[] stats = new StatEntry[top.size()];
                            for (int i = 0; i < top.size(); i++) {
                                stats[i] = new StatEntry(jedis.hget(topArray[i], "username"),
                                        jedis.zscore(key, topArray[i]));
                                if (stats[i].getPlayerName() == null || stats[i].getPlayerName().trim().toLowerCase().equals("null")) {
                                    jedis.zrem(key, topArray[i]);
                                    stats[i] = null;
                                }
                                for (int j = 1; j < top.size(); j++) {
                                    if (stats[j - 1] == null && stats.length > j) {
                                        stats[j - 1] = stats[j];
                                        stats[j] = null;
                                    }
                                }
                            }
                            cachedTopAlltime.put(stat, new Ranking(stats));
                        }
                    }
                } catch (NullPointerException e) {
                    cachedTopWeekly.clear();
                    cachedTopDaily.clear();
                }

                cal = Calendar.getInstance();
                cal.setTimeZone(TimeZone.getTimeZone("America/New_York"));
                cal.setFirstDayOfWeek(cal.get(Calendar.MONDAY));
                if (cal.getTimeInMillis() / 1000 < tomorrowStart) {
                    JedisPool.close(jedis);
                    return;
                }
                // the day just ended
                if (jedis.ttl("dailyStatsResetHandled") < 0) {
                    // this server is handling the daily reset
                    jedis.set("dailyStatsResetHandled", "true");
                    jedis.expire("dailyStatsResetHandled", 125);
                    for (String stat : cachedTopDaily.keySet()) {
                        jedis.del("dailyStatRankings:" + stat);
                    }
                }
                // clear cached player stats
                for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
                    for (String stat : cachedTopDaily.keySet()) {
                        aPlayer.getData("dailyStatRankings:" + stat).setValue(null);
                    }
                }
                // update local daily ranking cache again
                synchronized (cachedTopDaily) {
                    cachedTopDaily.clear();
                    for (String key : jedis.keys("dailyStatRankings:*")) {
                        String stat = key.split(":")[1];
                        Set<String> top = jedis.zrevrange(key, 0, 9);
                        String[] topArray = top.toArray(new String[top.size()]);
                        StatEntry[] stats = new StatEntry[top.size()];
                        for (int i = 0; i < top.size(); i++) {
                            stats[i] = new StatEntry(jedis.hget(topArray[i], "username"),
                                    jedis.zscore(key, topArray[i]));
                            if (stats[i].getPlayerName() == null || stats[i].getPlayerName().trim().toLowerCase().equals("null")) {
                                jedis.zrem(key, topArray[i]);
                                stats[i] = null;
                            }
                            for (int j = 1; j < top.size(); j++) {
                                if (stats[j - 1] == null && stats.length > j) {
                                    stats[j - 1] = stats[j];
                                    stats[j] = null;
                                }
                            }
                        }
                        cachedTopDaily.put(stat, new Ranking(stats));
                    }
                }
                // reset tomorrowStart
                cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.getActualMaximum(Calendar.HOUR_OF_DAY), cal.getActualMaximum(Calendar.MINUTE), cal.getActualMaximum(Calendar.SECOND));
                tomorrowStart = ((Long) ((cal.getTimeInMillis() + 1000) / 1000)).intValue();

                cal = Calendar.getInstance();
                cal.setTimeZone(TimeZone.getTimeZone("America/New_York"));
                cal.setFirstDayOfWeek(cal.get(Calendar.MONDAY));
                if (cal.getTimeInMillis() / 1000 < nextWeekStart) {
                    JedisPool.close(jedis);
                    return;
                }
                // the week just ended
                if (jedis.ttl("weeklyStatsResetHandled") < 0) {
                    // this server is handling the daily reset
                    jedis.set("weeklyStatsResetHandled", "true");
                    jedis.expire("weeklyStatsResetHandled", 125);
                    for (String stat : cachedTopWeekly.keySet()) {
                        jedis.del("weeklyStatRankings:" + stat);
                    }
                }
                // clear cached player stats
                for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
                    for (String stat : cachedTopWeekly.keySet()) {
                        aPlayer.getData("weeklyStatRankings:" + stat).setValue(null);
                    }
                }
                // update local weekly ranking cache again
                synchronized (cachedTopWeekly) {
                    cachedTopWeekly.clear();
                    for (String key : jedis.keys("weeklyStatRankings:*")) {
                        String stat = key.split(":")[1];
                        Set<String> top = jedis.zrevrange(key, 0, 9);
                        String[] topArray = top.toArray(new String[top.size()]);
                        StatEntry[] stats = new StatEntry[top.size()];
                        for (int i = 0; i < top.size(); i++) {
                            stats[i] = new StatEntry(jedis.hget(topArray[i], "username"),
                                    jedis.zscore(key, topArray[i]));
                            if (stats[i].getPlayerName() == null || stats[i].getPlayerName().trim().toLowerCase().equals("null")) {
                                jedis.zrem(key, topArray[i]);
                                stats[i] = null;
                            }
                            for (int j = 1; j < top.size(); j++) {
                                if (stats[j - 1] == null && stats.length > j) {
                                    stats[j - 1] = stats[j];
                                    stats[j] = null;
                                }
                            }
                        }
                        cachedTopWeekly.put(stat, new Ranking(stats));
                    }
                }
                JedisPool.close(jedis);
                // reset nextWeekStart
                cal.add(Calendar.DAY_OF_WEEK, cal.getFirstDayOfWeek() - cal.get(Calendar.DAY_OF_WEEK));
                cal.add(Calendar.DAY_OF_YEAR, 6);
                cal.set(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH), cal.getActualMaximum(Calendar.HOUR_OF_DAY), cal.getActualMaximum(Calendar.MINUTE), cal.getActualMaximum(Calendar.SECOND));
                nextWeekStart = ((Long) ((cal.getTimeInMillis() + 1000) / 1000)).intValue();
            }
        }.runTaskTimerAsynchronously(ServerLink.plugin, 0, 30 * 20);
    }

    /**
     * setFluctuatingStat() is used for things like kdr, where we keep track of a player's HIGHEST REACHED
     * kdr/killstreak/etc. today, this week, and all time. For keeping track of TOTAL GAINED
     * points/kills/etc., see incrIncreasingStat()
     */
    public static void setFluctuatingStat(UUID player, String stat, int val) {
        setFluctuatingStat(player, stat, (double) val);
    }

    /**
     * setFluctuatingStat() is used for things like kdr, where we keep track of a player's HIGHEST REACHED
     * kdr/killstreak/etc. today, this week, and all time. For keeping track of TOTAL GAINED
     * points/kills/etc., see incrIncreasingStat()
     */
    public static void setFluctuatingStat(UUID player, String stat, double val) {
        if (BotManager.usedUUIDS.contains(player.toString())) return;
        if (PlayerManager.get(player) != null) {
            // set stat in each time period if it's higher than is already there
            AlphaPlayer aPlayer = PlayerManager.get(player);
            if (getDailyStat_UnformattedDouble(aPlayer, stat) < val)
                aPlayer.getData("dailyStatRankings:" + stat).setValue(val);
            if (getWeeklyStat_UnformattedDouble(aPlayer, stat) < val)
                aPlayer.getData("weeklyStatRankings:" + stat).setValue(val);
            if (getAlltimeStat_UnformattedDouble(aPlayer, stat) < val)
                aPlayer.getData("alltimeStatRankings:" + stat).setValue(val);
        }
        if (Thread.currentThread() != ServerLink.mainThread) {
            // set stat in each time period if it's higher than is already there
            Jedis jedis = JedisPool.getConn();
            Double previousVal = jedis.zscore("dailyStatRankings:" + stat, player.toString());
            if (previousVal == null || previousVal < val)
                jedis.zadd("dailyStatRankings:" + stat, val, player.toString());
            previousVal = jedis.zscore("weeklyStatRankings:" + stat, player.toString());
            if (previousVal == null || previousVal < val)
                jedis.zadd("weeklyStatRankings:" + stat, val, player.toString());
            previousVal = jedis.zscore("alltimeStatRankings:" + stat, player.toString());
            if (previousVal == null || previousVal < val)
                jedis.zadd("alltimeStatRankings:" + stat, val, player.toString());
            JedisPool.close(jedis);
        } else new BukkitRunnable() {
            @Override
            public void run() {
                // set stat in each time period if it's higher than is already there
                Jedis jedis = JedisPool.getConn();
                Double previousVal = jedis.zscore("dailyStatRankings:" + stat, player.toString());
                if (previousVal == null || previousVal < val)
                    jedis.zadd("dailyStatRankings:" + stat, val, player.toString());
                previousVal = jedis.zscore("weeklyStatRankings:" + stat, player.toString());
                if (previousVal == null || previousVal < val)
                    jedis.zadd("weeklyStatRankings:" + stat, val, player.toString());
                previousVal = jedis.zscore("alltimeStatRankings:" + stat, player.toString());
                if (previousVal == null || previousVal < val)
                    jedis.zadd("alltimeStatRankings:" + stat, val, player.toString());
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    /**
     * incrIncreasingStat() is used for things like kills, where we keep track of a player's TOTAL GAINED
     * points/kills/etc. today, this week, and all time. For keeping track of HIGHEST REACHED
     * kdr/killstreak/etc., see setFluctuatingStat()
     */
    public static void incrIncreasingStat(UUID player, String stat, int incrVal) {
        incrIncreasingStat(player, stat, (double) incrVal);
    }

    /**
     * incrIncreasingStat() is used for things like kills, where we keep track of a player's TOTAL GAINED
     * points/kills/etc. today, this week, and all time. For keeping track of HIGHEST REACHED
     * kdr/killstreak/etc., see setFluctuatingStat()
     */
    public static void incrIncreasingStat(UUID player, String stat, double incrVal) {
        if (BotManager.usedUUIDS.contains(player.toString())) return;
        if (PlayerManager.get(player) != null) {
            // update local cache
            AlphaPlayer aPlayer = PlayerManager.get(player);
            aPlayer.getData("dailyStatRankings:" + stat).setValue(
                    getDailyStat_UnformattedDouble(aPlayer, stat) + incrVal);
            aPlayer.getData("weeklyStatRankings:" + stat).setValue(
                    getWeeklyStat_UnformattedDouble(aPlayer, stat) + incrVal);
            aPlayer.getData("alltimeStatRankings:" + stat).setValue(
                    getAlltimeStat_UnformattedDouble(aPlayer, stat) + incrVal);
        }
        // store info in database
        if (Thread.currentThread() != ServerLink.mainThread) {
            Jedis jedis = JedisPool.getConn();
            jedis.zincrby("dailyStatRankings:" + stat, incrVal, player.toString());
            jedis.zincrby("weeklyStatRankings:" + stat, incrVal, player.toString());
            jedis.zincrby("alltimeStatRankings:" + stat, incrVal, player.toString());
            JedisPool.close(jedis);
        } else new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                jedis.zincrby("dailyStatRankings:" + stat, incrVal, player.toString());
                jedis.zincrby("weeklyStatRankings:" + stat, incrVal, player.toString());
                jedis.zincrby("alltimeStatRankings:" + stat, incrVal, player.toString());
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public static Ranking getTopTenAlltime(String stat) {
        synchronized (cachedTopAlltime) {
            return cachedTopAlltime.get(stat) != null ? cachedTopAlltime.get(stat) : new Ranking();
        }
    }

    public static Ranking getTopTenWeekly(String stat) {
        synchronized (cachedTopWeekly) {
            return cachedTopWeekly.get(stat) != null ? cachedTopWeekly.get(stat) : new Ranking();
        }
    }

    public static Ranking getTopTenDaily(String stat) {
        synchronized (cachedTopDaily) {
            return cachedTopDaily.get(stat) != null ? cachedTopDaily.get(stat) : new Ranking();
        }
    }

    public static int getDailyStat_Integer(AlphaPlayer aPlayer, String stat) {
        return (int) getDailyStat_UnformattedDouble(aPlayer, stat);
    }

    public static double getDailyStat_UnformattedDouble(AlphaPlayer aPlayer, String stat) {
        if (aPlayer.getData("dailyStatRankings:" + stat).getValue() != null)
            return (double) aPlayer.getData("dailyStatRankings:" + stat).getValue();
        Jedis jedis = JedisPool.getConn();
        Double statVal = jedis.zscore("dailyStatRankings:" + stat, aPlayer.getUuid().toString());
        JedisPool.close(jedis);
        if (statVal == null) statVal = 0.0;
        aPlayer.getData("dailyStatRankings:" + stat).setValue(statVal);
        return statVal;
    }

    public static int getWeeklyStat_Integer(AlphaPlayer aPlayer, String stat) {
        return (int) getWeeklyStat_UnformattedDouble(aPlayer, stat);
    }

    public static double getWeeklyStat_UnformattedDouble(AlphaPlayer aPlayer, String stat) {
        if (aPlayer.getData("weeklyStatRankings:" + stat).getValue() != null)
            return (double) aPlayer.getData("weeklyStatRankings:" + stat).getValue();
        Jedis jedis = JedisPool.getConn();
        Double statVal = jedis.zscore("weeklyStatRankings:" + stat, aPlayer.getUuid().toString());
        JedisPool.close(jedis);
        if (statVal == null) statVal = 0.0;
        aPlayer.getData("weeklyStatRankings:" + stat).setValue(statVal);
        return statVal;
    }

    public static int getAlltimeStat_Integer(AlphaPlayer aPlayer, String stat) {
        return (int) getAlltimeStat_UnformattedDouble(aPlayer, stat);
    }

    public static double getAlltimeStat_UnformattedDouble(AlphaPlayer aPlayer, String stat) {
        if (aPlayer.getData("alltimeStatRankings:" + stat).getValue() != null)
            return (double) aPlayer.getData("alltimeStatRankings:" + stat).getValue();
        Jedis jedis = JedisPool.getConn();
        Double statVal = jedis.zscore("alltimeStatRankings:" + stat, aPlayer.getUuid().toString());
        JedisPool.close(jedis);
        if (statVal == null) statVal = 0.0;
        aPlayer.getData("alltimeStatRankings:" + stat).setValue(statVal);
        return statVal;
    }
}

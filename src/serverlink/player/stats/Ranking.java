package serverlink.player.stats;

public class Ranking {
    private StatEntry firstPlace = null;
    private StatEntry secondPlace = null;
    private StatEntry thirdPlace = null;
    private StatEntry fourthPlace = null;
    private StatEntry fifthPlace = null;
    private StatEntry sixthPlace = null;
    private StatEntry seventhPlace = null;
    private StatEntry eighthPlace = null;
    private StatEntry ninthPlace = null;
    private StatEntry tenthPlace = null;

    public Ranking(StatEntry... places) {
        if (places.length >= 1) firstPlace = places[0];
        if (places.length >= 2) secondPlace = places[1];
        if (places.length >= 3) thirdPlace = places[2];
        if (places.length >= 4) fourthPlace = places[3];
        if (places.length >= 5) fifthPlace = places[4];
        if (places.length >= 6) sixthPlace = places[5];
        if (places.length >= 7) seventhPlace = places[6];
        if (places.length >= 8) eighthPlace = places[7];
        if (places.length >= 9) ninthPlace = places[8];
        if (places.length >= 10) tenthPlace = places[9];
    }

    public StatEntry getFirstPlace() {
        return firstPlace;
    }

    public StatEntry getSecondPlace() {
        return secondPlace;
    }

    public StatEntry getThirdPlace() {
        return thirdPlace;
    }

    public StatEntry getFourthPlace() {
        return fourthPlace;
    }

    public StatEntry getFifthPlace() {
        return fifthPlace;
    }

    public StatEntry getSixthPlace() {
        return sixthPlace;
    }

    public StatEntry getSeventhPlace() {
        return seventhPlace;
    }

    public StatEntry getEighthPlace() {
        return eighthPlace;
    }

    public StatEntry getNinthPlace() {
        return ninthPlace;
    }

    public StatEntry getTenthPlace() {
        return tenthPlace;
    }
}

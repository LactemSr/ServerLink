package serverlink.player.stats;

public class StatEntry {
    private String playerName;
    private double statValue;

    public StatEntry(String playerName, double statValue) {
        this.playerName = playerName;
        this.statValue = statValue;
    }

    public String getPlayerName() {
        return playerName;
    }

    public double getStatValue() {
        return statValue;
    }
}

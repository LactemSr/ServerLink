package serverlink.inventory;

import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import org.bukkit.inventory.ItemStack;

import java.util.HashMap;

public class GuiRow {
    // slots are 1-9 like a human would think, not 0-8
    private HashMap<Integer, ItemStack> items;
    private HashMap<Integer, Runnable> leftClicks;
    private HashMap<Integer, Runnable> rightClicks;
    private GuiPage page;
    // 1-6
    private int row;

    public GuiRow(@Nullable HashMap<Integer, ItemStack> items, @NotNull GuiPage page, @NotNull int row) {
        this.page = page;
        if (items == null) items = new HashMap<>();
        this.items = items;
        for (int i = 1; i <= 9; i++) if (!items.containsKey(i)) items.put(i, page.getFillItem());
        this.row = row;
        leftClicks = new HashMap<>();
        rightClicks = new HashMap<>();
        if (page.getRows() == null) {
            GuiRow[] rows = new GuiRow[1];
            rows[0] = this;
            page.rows = rows;
        } else {
            GuiRow[] rows = new GuiRow[page.getRows().length + 1];
            System.arraycopy(page.getRows(), 0, rows, 0, page.getRows().length);
            rows[page.getRows().length] = this;
            page.setRows(rows);
        }
    }

    public int getRow() {
        return row;
    }

    public HashMap<Integer, ItemStack> getItems() {
        return items;
    }

    public ItemStack getItem(int slot) {
        ItemStack is = items.get(slot);
        ItemStack actualItem = page.getInventory().getItem((9 * (row - 1)) - 1 + slot);
        if (actualItem != null && actualItem != is) {
            items.put(slot, actualItem);
            return actualItem;
        }
        return is;
    }

    public Runnable getLeftClick(int slot) {
        return leftClicks.get(slot);
    }

    public Runnable getRightClick(int slot) {
        return rightClicks.get(slot);
    }

    public GuiPage getPage() {
        return page;
    }

    // slots are 1-9
    public void setItem(int slot, ItemStack item) {
        if (item == null) items.put(slot, page.getFillItem());
        else items.put(slot, item);
        page.getInventory().setItem((9 * (row - 1)) - 1 + slot, item);
    }

    public void setLeftClick(int slot, Runnable runnable) {
        leftClicks.put(slot, runnable);
    }

    public void setRightClick(int slot, Runnable runnable) {
        rightClicks.put(slot, runnable);
    }

    public void setClick(int slot, Runnable runnable) {
        leftClicks.put(slot, runnable);
        rightClicks.put(slot, runnable);
    }
}

package serverlink.inventory;

import com.sun.istack.internal.NotNull;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import org.bukkit.entity.Player;

public class InventoryGui {
    private Player player;
    private GuiPage[] pages;
    private int currentPage;
    private String id;

    public InventoryGui(@NotNull Player player, @NotNull String id, GuiPage... pages) {
        this.id = id;
        this.player = player;
        this.pages = pages;
        for (GuiPage page : pages) page.setRows(page.getRows());
        currentPage = 1;
        AlphaPlayer aPlayer = PlayerManager.get(player);
        InventoryGui guiToReplace = null;
        for (InventoryGui gui : aPlayer.getGuis()) {
            if (gui.getId().equals(id)) {
                guiToReplace = gui;
                InventoryManager.teamGuis.remove(guiToReplace);
                break;
            }
        }
        if (guiToReplace != null) aPlayer.removeGui(guiToReplace);
        aPlayer.addGui(this);
    }

    public String getId() {
        return id;
    }

    public Player getPlayer() {
        return player;
    }

    public GuiPage[] getPages() {
        return pages;
    }

    // starting with 1, not 0
    public int getCurrentPage() {
        return currentPage;
    }

    // starting with 1, not 0
    public void openPage(int page) {
        currentPage = page;
        player.openInventory(pages[page - 1].getInventory());
    }
}

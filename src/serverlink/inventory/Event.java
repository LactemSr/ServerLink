package serverlink.inventory;


import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.alphaplayer.PlayerManager;
import serverlink.customevent.CustomGuiClickEvent;
import serverlink.util.Item;

import java.util.HashMap;
import java.util.List;

public class Event implements Listener {
    // We are using hashmaps here even though the String is pointless, because it may be faster than using
    // lists. Also, I chose to put the player's name as the String to avoid having two keys point to the
    // same value, which would slow it down to the speed of a list.
    private static HashMap<Player, String> guiCooldown = new HashMap<>();
    private static HashMap<Player, String> hotbarCooldown = new HashMap<>();

    public static void setup() {
        new BukkitRunnable() {
            @Override
            public void run() {
                guiCooldown.clear();
            }
        }.runTaskTimer(ServerLink.plugin, 10, 10);
        new BukkitRunnable() {
            @Override
            public void run() {
                hotbarCooldown.clear();
            }
        }.runTaskTimer(ServerLink.plugin, 20, 20);
    }

    @EventHandler
    void useHotbarItems(PlayerInteractEvent e) {
        if (e.getPlayer().getItemInHand().getType() == Material.AIR) return;
        if (hotbarCooldown.get(e.getPlayer()) != null && e.getPlayer().getOpenInventory() == null) {
            e.setCancelled(true);
            return;
        }
        if (e.getAction() == Action.PHYSICAL) return;
        ItemStack item = e.getPlayer().getItemInHand();
        List<ItemStack> hotbarItems = PlayerManager.get(e.getPlayer()).getHotbarItems();
        for (ItemStack hotbarItem : hotbarItems) {
            if (!Item.isSimilar(hotbarItem, item)) continue;
            PlayerManager.get(e.getPlayer()).getHotbarItemAction(hotbarItem).run();
            e.setCancelled(true);
            hotbarCooldown.put(e.getPlayer(), e.getPlayer().getName());
            return;
        }
    }

    @EventHandler
    void useHotbarItems(EntityDamageByEntityEvent e) {
        if (e.getDamager().getType() != EntityType.PLAYER) return;
        Player player = (Player) e.getDamager();
        if (player.getItemInHand().getType() == Material.AIR) return;
        if (hotbarCooldown.get(player) != null && player.getOpenInventory() == null) return;
        ItemStack item = player.getItemInHand();
        List<ItemStack> hotbarItems = PlayerManager.get(player).getHotbarItems();
        for (ItemStack hotbarItem : hotbarItems) {
            if (!Item.isSimilar(hotbarItem, item)) continue;
            PlayerManager.get(player).getHotbarItemAction(hotbarItem).run();
            e.setCancelled(true);
            hotbarCooldown.put(player, player.getName());
            return;
        }
    }

    @EventHandler
    void invClick(InventoryClickEvent e) {
        if (e.getCurrentItem() == null || e.getCurrentItem().getType() == Material.AIR) return;
        if (!(e.getWhoClicked() instanceof Player)) return;
        Player player = (Player) e.getWhoClicked();
        if (guiCooldown.get(player) != null) {
            e.setCancelled(true);
            return;
        }
        InventoryGui gui = null;
        GuiPage page = null;
        GuiRow row = null;
        int slot = 0;
        boolean found = false;
        for (InventoryGui gui1 : PlayerManager.get(player).getGuis()) {
            for (GuiPage page1 : gui1.getPages()) {
                if (!page1.getInventory().getViewers().contains(player)) continue;
                gui = gui1;
                page = page1;
                found = true;
                break;
            }
            if (found) break;
        }
        if (!found) return;
        e.setCancelled(true);
        boolean stop = false;
        for (GuiRow r : page.getRows()) {
            HashMap<Integer, ItemStack> items = r.getItems();
            for (int i : items.keySet()) {
                if (Item.isSimilar(items.get(i), e.getCurrentItem())) {
                    row = r;
                    slot = i;
                    stop = true;
                    break;
                }
            }
            /*for (int i = 1; i <= 9; i++) {
                if (Item.isSimilar(r.getItem(i), e.getCurrentItem())) {
                    row = r;
                    slot = i;
                    stop = true;
                    break;
                }
            }*/
            if (stop) break;
        }
        if (row == null) return;
        guiCooldown.put(player, player.getName());
        ItemStack item = e.getCurrentItem();
        if (item.getType() == Material.STAINED_GLASS_PANE) {
            if (item.hasItemMeta() && item.getItemMeta().getDisplayName().contains("Page ")) {
                Integer toPage = Integer.parseInt(String.valueOf(item.getItemMeta().getDisplayName().charAt(5)));
                gui.openPage(toPage);
                return;
            }
        }
        ClickType click = e.getClick();
        if (click == ClickType.LEFT || click == ClickType.SHIFT_LEFT)
            if (row.getLeftClick(slot) != null) row.getLeftClick(slot).run();
            else
                Bukkit.getPluginManager().callEvent(new CustomGuiClickEvent(player, "left", item, gui, page, row, slot));
        else if (click == ClickType.RIGHT || click == ClickType.SHIFT_RIGHT || click == ClickType.DOUBLE_CLICK)
            if (row.getRightClick(slot) != null) row.getRightClick(slot).run();
            else
                Bukkit.getPluginManager().callEvent(new CustomGuiClickEvent(player, "right", item, gui, page, row, slot));
    }
}
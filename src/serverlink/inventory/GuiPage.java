package serverlink.inventory;

import serverlink.chat.chat;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class GuiPage {
    GuiRow[] rows;
    Inventory inventory;
    private String title;
    private ItemStack fillItem;

    public GuiPage(String title, GuiRow... rows) {
        this.title = chat.color(title);
        this.fillItem = new ItemStack(Material.AIR);
        this.rows = rows;
        if (rows == null || rows.length == 0) this.inventory = Bukkit.createInventory(null, 6 * 9, this.title);
        else this.inventory = Bukkit.createInventory(null, rows.length * 9, this.title);
    }

    public GuiPage(String title, ItemStack fillItem, GuiRow... rows) {
        this.title = chat.color(title);
        this.fillItem = fillItem;
        if (this.fillItem == null) this.fillItem = new ItemStack(Material.AIR);
        this.rows = rows;
        if (rows == null || rows.length == 0) this.inventory = Bukkit.createInventory(null, 6 * 9, this.title);
        else this.inventory = Bukkit.createInventory(null, rows.length * 9, this.title);
    }

    public String getTitle() {
        return title;
    }

    public Inventory getInventory() {
        return inventory;
    }

    public ItemStack getFillItem() {
        return fillItem;
    }

    public void setFillItem(ItemStack fillItem) {
        ItemStack old = this.fillItem;
        this.fillItem = fillItem;
        for (GuiRow row : rows) {
            for (int slot : row.getItems().keySet()) {
                if (row.getItem(slot) == old) {
                    row.setItem(slot, fillItem);
                }
            }
        }
    }

    public GuiRow[] getRows() {
        return rows;
    }

    public void setRows(GuiRow... rows) {
        this.rows = rows;
        int lastRow = 1;
        for (GuiRow row : rows) if (row.getRow() > lastRow) lastRow = row.getRow();
        this.inventory = Bukkit.createInventory(null, lastRow * 9, title);
        for (GuiRow row : rows) {
            for (int slot = 1; slot <= 9; slot++) {
                row.setItem(slot, row.getItem(slot));
            }
        }
    }
}

package serverlink.inventory.gui.profile;

import org.bukkit.inventory.ItemStack;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.chat.chat;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.player.Permissions;
import serverlink.player.Referrals;
import serverlink.util.Item;

import java.util.List;
import java.util.UUID;

public class ReferralsGui {
    public static InventoryGui createMainMenu(AlphaPlayer aPlayer) {
        if (aPlayer.getData("referMenuGui").getValue() != null)
            return (InventoryGui) aPlayer.getData("referMenuGui").getValue();
        GuiPage page = new GuiPage(chat.color("&2&lReferrals Menu"));
        InventoryGui gui = new InventoryGui(aPlayer.getPlayer(), "ReferralsGui", page);
        GuiRow row = new GuiRow(null, page, 1);

        ItemStack lilypad;
        if (Referrals.isReferred(aPlayer))
            lilypad = Item.easyCreate("lilypad", "&aYou were referred by &6" + ServerLink.getNameFromAlphaPlayer(aPlayer, UUID.fromString((String) aPlayer.getData("referrer").getValue())),
                    "", "&7You can only be referred once.");
        else
            lilypad = Item.easyCreate("lilypad", "&aType &6/refer confirm &e<player>", "&aand earn 10k KitPvP Tokens.");
        row.setItem(2, lilypad);

        List<UUID> sent = Referrals.getSentReferrals(aPlayer);
        List<UUID> accepted = Referrals.getAcceptedReferrals(aPlayer);
        ItemStack vines = Item.easyCreate("vines", "&a&lSent Referral Requests", "&e(&c" + accepted.size() + " &eaccepted," +
                " &c" + sent.size() + " &epending)", "", "&7Type &6/refer send &e<player>", "&7to send someone a request.");
        row.setItem(5, vines);

        ItemStack tree = Item.easyCreate("junglesapling", "&a&lRewards Tree", "", "&7Earn greater rewards for",
                "&7referring more people. Click", "&7to display the Rewards Tree.");
        row.setItem(8, tree);
        row.setClick(8, () -> createRewardTree(aPlayer).openPage(1));

        GuiRow row2 = new GuiRow(null, page, 2);
        row2.setItem(5, Item.easyCreate("arrow", ProfileGui.backToMainMenu));
        row2.setClick(5, () -> ProfileGui.openGui(aPlayer));

        page.setRows(row, row2);
        aPlayer.getData("referMenuGui").setValue(gui);
        return gui;
    }

    private static InventoryGui createRewardTree(AlphaPlayer aPlayer) {
        if (aPlayer.getData("referRewardsGui").getValue() != null)
            return (InventoryGui) aPlayer.getData("referRewardsGui").getValue();
        GuiPage page = new GuiPage(chat.color("&2&lRewards Tree"));
        GuiRow row1 = new GuiRow(null, page, 1);
        GuiRow row2 = new GuiRow(null, page, 2);
        GuiRow row3 = new GuiRow(null, page, 3);
        GuiRow row4 = new GuiRow(null, page, 4);
        int playersReferred = Referrals.getAcceptedReferrals(aPlayer).size();
        // first row
        if (playersReferred >= 1)
            row1.setItem(5, Item.easyCreate("emeraldblock", "&b10,000 KITPVP TOKENS &7(1st & 2nd)", "", "&7Receive 10k KitPvP Tokens",
                    "&7when you refer someone AND", "&7when you accept a referral."));
        else
            row1.setItem(5, Item.easyCreate("redstoneblock", "&b10,000 KITPVP TOKENS", "", "&7Receive 10k KitPvP Tokens",
                    "&7when you refer someone AND", "&7when you accept a referral."));
        // second row
        if (playersReferred >= 3) {
            row2.setItem(4, Item.easyCreate("emeraldblock", 3, "&b30,000 COINS &7(3rd - 9th)", "", "&7Receive 30k coins for",
                    "&7every player you refer."));
            row2.setItem(5, Item.easyCreate("emeraldblock", 3, "&bDOG PET &7(3rd)", "", "&7Unlock the exclusive",
                    "&7dog pet when you refer", "&7your third player."));
            row2.setItem(6, Item.easyCreate("emeraldblock", 3, "&b30,000 SKY GEMS &7(3rd - 9th)", "", "&7Receive 30k Sky Gems for",
                    "&7every player you refer."));
        } else {
            row2.setItem(4, Item.easyCreate("redstoneblock", 3, "&b30,000 COINS &7(3rd - 9th)", "", "&7Receive 30k coins for",
                    "&7every player you refer."));
            row2.setItem(5, Item.easyCreate("redstoneblock", 3, "&bDOG PET &7(3rd)", "", "&7Unlock the exclusive",
                    "&7dog pet when you refer", "&7your third player."));
            row2.setItem(6, Item.easyCreate("redstoneblock", 3, "&b30,000 SKY GEMS &7(3rd - 9th)", "", "&7Receive 30k Sky Gems for",
                    "&7every player you refer."));
        }
        // third row
        if (playersReferred >= 10) {
            row3.setItem(3, Item.easyCreate("emeraldblock", 10, "&b50,000 COINS &7(10th - 14th)", "", "&7Receive 50k coins for " +
                    "every player you refer."));
            row3.setItem(5, Item.easyCreate("emeraldblock", 10, "&b" + Permissions.getRankName(2) + " &7(10th)", "", "&7Unlock " + Permissions.getRankName(2) +
                    " &7when you refer your", "&7tenth player."));
            row3.setItem(7, Item.easyCreate("emeraldblock", 10, "&b50,000 KITPVP TOKENS &7(10th - 14th)", "", "&7Receive 50k KitPvP Tokens for " +
                    "every player you refer."));
        } else {
            row3.setItem(3, Item.easyCreate("redstoneblock", 10, "&b50,000 COINS &7(10th - 14th)", "", "&7Receive 50k coins for " +
                    "every player you refer."));
            row3.setItem(5, Item.easyCreate("redstoneblock", 10, "&b" + Permissions.getRankName(2) + " &7(10th)", "", "&7Unlock " + Permissions.getRankName(2) +
                    " &7when you refer your", "&7tenth player."));
            row3.setItem(7, Item.easyCreate("redstoneblock", 10, "&b50,000 KITPVP TOKENS &7(10th - 14th)", "", "&7Receive 50k KitPvP Tokens for " +
                    "every player you refer."));
        }
        // fourth row
        if (playersReferred >= 15) {
            row4.setItem(2, Item.easyCreate("emeraldblock", 15, "&b100,000 COINS &7(15th+)", "", "&7Receive 100k coins for " +
                    "every player you refer."));
            row4.setItem(5, Item.easyCreate("emeraldblock", 15, "&b5 EPIC CHESTS &7(15th+)", "", "&7Receive 5 epic chests",
                    "&7for every player you refer."));
            row4.setItem(8, Item.easyCreate("emeraldblock", 15, "&b100,000 SKY GEMS &7(15th+)", "", "&7Receive 100k Sky Gems for " +
                    "&7every player you refer."));
        } else {
            row4.setItem(2, Item.easyCreate("redstoneblock", 15, "&b100,000 COINS &7(15th+)", "", "&7Receive 100k coins for " +
                    "every player you refer."));
            row4.setItem(5, Item.easyCreate("redstoneblock", 15, "&b5 EPIC CHESTS &7(15th+)", "", "&7Receive 5 epic chests",
                    "&7for every player you refer."));
            row4.setItem(8, Item.easyCreate("redstoneblock", 15, "&b100,000 SKY GEMS &7(15th+)", "", "&7Receive 100k Sky Gems for " +
                    "&7every player you refer."));
        }
        GuiRow row6 = new GuiRow(null, page, 6);
        row6.setItem(5, Item.easyCreate("arrow", "Back to &a&lReferrals Menu"));
        row6.setClick(5, () -> ReferralsGui.createMainMenu(aPlayer).openPage(1));
        page.setRows(row1, row2, row3, row4, new GuiRow(null, page, 5), row6);
        InventoryGui gui = new InventoryGui(aPlayer.getPlayer(), "ReferralsRewardTree", page);
        aPlayer.getData("referRewardsGui").setValue(gui);
        return gui;
    }
}

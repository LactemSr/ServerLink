package serverlink.inventory.gui.profile;

import serverlink.chat.chat;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.util.Item;
import org.bukkit.inventory.ItemStack;

class LevelPerksGui {
    private static String title = chat.color("&2&lLevel Perks");
    private static ItemStack white0 = Item.easyCreate("quartzblock", "&6Level 0");
    private static ItemStack white25 = Item.easyCreate("quartzblock", "&6Level 25");
    private static ItemStack white50 = Item.easyCreate("quartzblock", "&6Level 50");
    private static ItemStack white75 = Item.easyCreate("quartzblock", "&6Level 75");
    private static ItemStack red25 = Item.easyCreate("redstoneblock", "&6Level 25", "&aYou will earn 2x points in all",
            "&agames after reaching level 25.");
    private static ItemStack red50 = Item.easyCreate("redstoneblock", "&6Level 50", "&aYou will earn 3x points in all",
            "&agames after reaching level 50.");
    private static ItemStack red75 = Item.easyCreate("redstoneblock", "&6Level 75", "&aYou will earn 4x points in all",
            "&agames after reaching level 75.");
    private static ItemStack red100 = Item.easyCreate("redstoneblock", "&6Level 100", "&aYou will earn 5x points in all",
            "&agames after reaching level 100.");
    private static ItemStack green0 = Item.easyCreate("emeraldblock", "&6Level 0", "&9You earn 1x points in all games.");
    private static ItemStack green25 = Item.easyCreate("emeraldblock", "&6Level 25", "&9You earn 2x points in all games.");
    private static ItemStack green50 = Item.easyCreate("emeraldblock", "&6Level 50", "&9You earn 3x points in all games.");
    private static ItemStack green75 = Item.easyCreate("emeraldblock", "&6Level 75", "&9You earn 4x points in all games.");
    private static ItemStack green100 = Item.easyCreate("emeraldblock", "&6Level 100", "&9You earn 5x points in all games.");

    public static InventoryGui createGui(AlphaPlayer aPlayer) {
        GuiPage page = new GuiPage(title);
        InventoryGui gui = new InventoryGui(aPlayer.getPlayer(), "LevelPerks", page);
        GuiRow row1 = new GuiRow(null, page, 1);
        int level = aPlayer.getLevel();
        if (level == 100) {
            row1.setItem(1, white0.clone());
            row1.setItem(3, white25.clone());
            row1.setItem(5, white50.clone());
            row1.setItem(7, white75.clone());
            row1.setItem(9, green100.clone());
        } else if (level >= 75) {
            row1.setItem(1, white0.clone());
            row1.setItem(3, white25.clone());
            row1.setItem(5, white50.clone());
            row1.setItem(7, green75.clone());
            row1.setItem(9, red100.clone());
        } else if (level >= 50) {
            row1.setItem(1, white0.clone());
            row1.setItem(3, white25.clone());
            row1.setItem(5, green50.clone());
            row1.setItem(7, red75.clone());
            row1.setItem(9, red100.clone());
        } else if (level >= 25) {
            row1.setItem(1, white0.clone());
            row1.setItem(3, green25.clone());
            row1.setItem(5, red50.clone());
            row1.setItem(7, red75.clone());
            row1.setItem(9, red100.clone());
        } else {
            row1.setItem(1, green0.clone());
            row1.setItem(3, red25.clone());
            row1.setItem(5, red50.clone());
            row1.setItem(7, red75.clone());
            row1.setItem(9, red100.clone());
        }
        GuiRow row2 = new GuiRow(null, page, 2);
        row2.setItem(5, Item.easyCreate("arrow", ProfileGui.backToMainMenu));
        row2.setClick(5, () -> ProfileGui.openGui(aPlayer));
        page.setRows(row1, row2);
        return gui;
    }
}

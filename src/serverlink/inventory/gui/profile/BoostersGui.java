package serverlink.inventory.gui.profile;

import serverlink.chat.chat;
import serverlink.customevent.CustomGuiClickEvent;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.network.JedisOne;
import serverlink.network.JedisPool;
import serverlink.server.GametypeBooster;
import serverlink.util.CommandText;
import serverlink.util.Item;
import mkremins.fanciful.FancyMessage;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class BoostersGui implements Listener {
    // if someone buys more than 36 boosters, only 35 will show (we're not doing multiple pages)
    // make the first line a chest in the middle that opens the shop
    // the other lines list boosters in order of when they were bought
    // don't allow players to add more than one booster to the queue of the same game
    private static String title = chat.color("&2&lMy Gametype Boosters");
    private static ItemStack chest = Item.easyCreate("chest", "&6Buy More Boosters", "", "&7Click to buy more boosters.");

    public static InventoryGui createGui(AlphaPlayer aPlayer) {
        if (aPlayer.getData("boostersGui").getValue() != null)
            return (InventoryGui) aPlayer.getData("boostersGui").getValue();
        Jedis jedis = JedisPool.getConn();
        List<String> available = jedis.lrange("gametypeBoosters:" + aPlayer.getUuid().toString(), 0, -1);
        JedisPool.close(jedis);
        GuiPage page = new GuiPage(title);
        List<GuiRow> rows = new ArrayList<>();
        GuiRow row1 = new GuiRow(null, page, 1);
        row1.setItem(5, chest.clone());
        rows.add(row1);
        int rowNumber = 2;
        int slot = 1;
        int index = 0;
        GuiRow row = new GuiRow(null, page, rowNumber);
        for (String s : available) {
            if (index == available.size() - 1) break;
            if (slot == 8 && rowNumber == 5) {
                if (available.size() > 36) {
                    ItemStack andXMore = Item.easyCreate("glassbottle", available.size() - 35, "and " + (available.size() - 35) + " more...");
                    row.setItem(9, andXMore);
                    break;
                } else {
                    slot++;
                    index++;
                }
            } else if (slot == 9) {
                slot = 1;
                rowNumber++;
                index++;
                rows.add(row);
                row = new GuiRow(null, page, rowNumber);
            } else {
                slot++;
                index++;
            }
            String[] booster = s.split(":");
            String line1 = "&9" + booster[0] + "x &6" + booster[3] + " &abooster for &9" + booster[2] + " minutes";
            ItemStack potion = Item.easyCreate("potion:8195", chat.color(line1), chat.color("&7Click to Activate"));
            ItemMeta potionMeta = potion.getItemMeta();
            potionMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_POTION_EFFECTS);
            potion.setItemMeta(potionMeta);
            row.setItem(slot, potion);
        }
        rows.add(row);
        row = new GuiRow(null, page, rowNumber + 1);
        row.setItem(5, Item.easyCreate("arrow", ProfileGui.backToMainMenu));
        rows.add(row);
        page.setRows((GuiRow[]) rows.toArray(new GuiRow[rows.size()]));
        InventoryGui gui = new InventoryGui(aPlayer.getPlayer(), "BoostersGui", page);
        aPlayer.getData("boostersGui").setValue(gui);
        return gui;
    }

    @EventHandler
    public void onGuiClick(CustomGuiClickEvent e) {
        if (!e.getPage().getTitle().equals(title)) return;
        ItemStack item = e.getItem();
        if (item.getType() == Material.ARROW)
            ProfileGui.openGui(PlayerManager.get(e.getPlayer()));
        else if (item.getType() == Material.CHEST) {
            Player player = e.getPlayer();
            player.closeInventory();
            FancyMessage link = new FancyMessage("You can buy more gametype boosters at our shop:\n   ").color(ChatColor.GREEN)
                    .then("shop.alphacraft.us ").color(ChatColor.GOLD).style(ChatColor.BOLD).tooltip("Click to visit the shop")
                    .link("http://shop.alphacraft.us");
            player.sendMessage("");
            CommandText.sendMessage(player, link.toJSONString());
            player.sendMessage("");
        } else if (item.getType() == Material.POTION) {
            Player player = e.getPlayer();
            String displayName = ChatColor.stripColor(item.getItemMeta().getDisplayName()).replace(" booster for ", ":")
                    .replace(" minutes", "").replace("x ", ":");
            String[] split = displayName.split(":");
            Jedis jedis = JedisOne.getTwo();
            List<String> queue = jedis.lrange(split[1] + "BoosterQueue", 0, 1);
            JedisOne.closeTwo();
            for (String b : queue) {
                if (b.contains(":" + player.getName() + ":")) {
                    player.closeInventory();
                    player.sendMessage("");
                    player.sendMessage(chat.color("&cYou already have a booster in the " + split[1] + "queue."));
                    player.sendMessage("");
                    return;
                }
            }
            GametypeBooster.useBooster(PlayerManager.get(player), Double.parseDouble(split[0]), Integer.parseInt(split[2]), split[1]);
        }
    }
}

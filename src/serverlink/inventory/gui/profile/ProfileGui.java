package serverlink.inventory.gui.profile;

import mkremins.fanciful.FancyMessage;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.chat.chat;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.inventory.gui.stats.MainStatsGui;
import serverlink.network.JedisPool;
import serverlink.player.Level;
import serverlink.player.Permissions;
import serverlink.player.Points;
import serverlink.player.RankExpiry;
import serverlink.util.CommandText;
import serverlink.util.Item;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

public class ProfileGui {
    static String backToMainMenu = chat.color("Back to &a&lMy Profile");
    private static String title = chat.color("&a&lMy Profile");

    private static FancyMessage shopLink = new FancyMessage("You can visit our shop by clicking the link:\n   ")
            .color(ChatColor.GREEN).then("shop.alphacraft.us ").color(ChatColor.GOLD).style(ChatColor.BOLD)
            .tooltip("Click to visit the shop").link("http://shop.alphacraft.us");

    private static Date fiveYearsFromNow;

    static {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 5);
        fiveYearsFromNow = cal.getTime();
    }

    public static ItemStack createHotBarItem(Player player) {
        ItemStack item = Item.easyCreate("skullitem", title);
        SkullMeta meta = (SkullMeta) item.getItemMeta();
        meta.setOwner(player.getName());
        item.setItemMeta(meta);
        new BukkitRunnable() {
            @Override
            public void run() {
                player.updateInventory();
            }
        }.runTaskLater(ServerLink.plugin, 2);
        new BukkitRunnable() {
            @Override
            public void run() {
                player.updateInventory();
            }
        }.runTaskLater(ServerLink.plugin, 4);
        new BukkitRunnable() {
            @Override
            public void run() {
                player.updateInventory();
            }
        }.runTaskLater(ServerLink.plugin, 7);
        return item;
    }

    public static void openGui(AlphaPlayer aPlayer) {
        Object cachedGui = aPlayer.getData("profileGuiGui").getValue();
        if (cachedGui != null) {
            ((InventoryGui) cachedGui).openPage(1);
            return;
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                GuiPage page = new GuiPage("&2&lMy Profile");
                GuiRow row = new GuiRow(null, page, 1);

                // MY INFO
                String rank = "&c&lRANK: ";
                if (aPlayer.getRank() == 1) rank = rank + "&7none - Click to Purchase a Rank";
                else if (aPlayer.getRank() > 5) rank = rank + Permissions.getRankName(aPlayer.getRank());
                else {
                    Jedis jedis = JedisPool.getConn();
                    Date expiryDate = RankExpiry.getExpiryDate(aPlayer.getUuid().toString(), aPlayer.getRank(), jedis);
                    JedisPool.close(jedis);
                    if (expiryDate == null) {
                        rank = rank + Permissions.getRankName(aPlayer.getRank());
                    } else {
                        DateFormat df = new SimpleDateFormat("MMMM d, yyyy");
                        df.setTimeZone(TimeZone.getTimeZone("America/New_York"));
                        if (expiryDate.after(fiveYearsFromNow))
                            rank = rank + " &elifetime" + Permissions.getRankName(aPlayer.getRank());
                        else
                            rank = rank + Permissions.getRankName(aPlayer.getRank()) + " &7- expires on &e" + df.format(expiryDate);
                    }
                }
                String level = "&c&lLEVEL: " + Level.getLevelColor(aPlayer.getLevel()) + aPlayer.getLevel();
                String multiplier = "&c&lMULTIPLIER: &f" + aPlayer.getMultiplierString() + "x &7(level " + aPlayer.getLevel() + ")";
                String kitpvpPoints = "&c&lKITPVP TOKENS: " + Points.getOfflinePointsString(aPlayer.getUuid().toString(), "kitpvp");
                String skyblockPoints = "&c&lSKY GEMS: " + Points.getOfflinePointsString(aPlayer.getUuid().toString(), "skyblock");
                String coins = "&c&lCOINS: " + aPlayer.getCoinsString();
                String forums = "&c&lFORUMS: ";
                Jedis jedis = JedisPool.getConn();
                String isLinked = jedis.hget(aPlayer.getUuid().toString(), "hasLinkedForumsAccount");
                String email = jedis.hget(aPlayer.getUuid().toString(), "forumsEmail");
                if (isLinked == null && email != null) {
                    forums = forums + "&9Ready to register with email: &6" + email + "&9 and IGN: &6" + aPlayer.getPlayer().getName();
                } else if (isLinked != null && isLinked.equals("true")) {
                    forums = forums + "&9Linked to &6" + email;
                } else {
                    forums = forums + "&9Unlinked &7- type &6/forums link &e<email>";
                }
                ItemStack myinfo = Item.easyCreate("skullitem", "&a&lMy Info", "", rank, level, multiplier, kitpvpPoints, skyblockPoints, coins, forums);
                SkullMeta meta = (SkullMeta) myinfo.getItemMeta();
                meta.setOwner(aPlayer.getPlayer().getName());
                myinfo.setItemMeta(meta);
                row.setItem(1, myinfo);
                row.setClick(1, () -> {
                    aPlayer.getPlayer().sendMessage("");
                    CommandText.sendMessage(aPlayer.getPlayer(), shopLink.toJSONString());
                    aPlayer.getPlayer().sendMessage("");
                    aPlayer.getPlayer().closeInventory();
                });
                JedisPool.close(jedis);
                // GAMETYPE BOOSTERS
                ItemStack boostersItem = Item.easyCreate("potion:8197", "&a&lMy Gametype Boosters", "&9Coming Soon!");
                ItemMeta boostersItemMeta = boostersItem.getItemMeta();
                boostersItemMeta.addItemFlags(ItemFlag.HIDE_POTION_EFFECTS, ItemFlag.HIDE_ATTRIBUTES);
                boostersItem.setItemMeta(boostersItemMeta);
                row.setItem(3, boostersItem);
                // TODO reopen gametype boosters after testing and planning
                /*
                ItemStack gametypeBoosters;
                jedis = JedisPool.getConn();
                List<String> boosters = jedis.lrange("gametypeBoosters:" + maxPlayer.getUuid().toString(), 0, -1);
                JedisPool.close(jedis);
                if (boosters.isEmpty()) {
                    gametypeBoosters = Item.easyCreate("potion:8197", "&a&lMy Gametype Boosters", "", "&9You don't have any " +
                            "unused", "&9Gametype Boosters.", "&7Click to open the shop and purchase", "&7some boosters.");
                    row.setItem(3, gametypeBoosters);
                    row.setClick(3, () -> {
                        maxPlayer.getEntity().sendMessage("");
                        CommandText.sendMessage(maxPlayer.getEntity(), shopLink.toJSONString());
                        maxPlayer.getEntity().sendMessage("");
                        maxPlayer.getEntity().closeInventory();
                    });
                } else if (boosters.size() > 1) {
                    gametypeBoosters = Item.easyCreate("potion:8197", "&a&lMy Gametype Boosters", "", "&9You have &c" +
                            boosters.size() + " &9unused Gametype Boosters.", "&7Click to view or activate a booster.");
                    row.setItem(3, gametypeBoosters);
                    row.setClick(3, () -> BoostersGui.createGui(maxPlayer).openPage(1));
                } else {
                    gametypeBoosters = Item.easyCreate("potion:8197", "&a&lMy Gametype Boosters", "", "&9You have &c1" +
                            " &9unused Gametype Booster.", "&7Click to view or activate a booster.");
                    row.setItem(3, gametypeBoosters);
                    row.setClick(3, () -> BoostersGui.createGui(maxPlayer).openPage(1));
                }
                */

                // LEVEL PERKS
                ItemStack levelPerks = Item.easyCreate("expbottle", "&aClick to View Rewards for Each Level");
                row.setItem(4, levelPerks);
                row.setClick(4, () -> LevelPerksGui.createGui(aPlayer).openPage(1));

                // REFERRALS
                ItemStack referrals = Item.easyCreate("dropper", "&aClick to Open &lMy Referrals");
                row.setItem(6, referrals);
                row.setClick(6, () -> ReferralsGui.createMainMenu(aPlayer).openPage(1));

                // GAME STATS
                ItemStack gameStats = Item.easyCreate("map", "&aClick to open &lGame Stats");
                row.setItem(7, gameStats);
                row.setClick(7, () -> MainStatsGui.openMainStatsGui(aPlayer));

                // RANK STATUS
                ItemStack rankStatus = Item.easyCreate("ladder", "&a&lRank Status", "", rank, "", "&7Click to visit " +
                        "the shop, where you", "&7can buy ranks, cosmetics, and classes.");
                row.setItem(9, rankStatus);
                row.setClick(9, () -> {
                    aPlayer.getPlayer().sendMessage("");
                    CommandText.sendMessage(aPlayer.getPlayer(), shopLink.toJSONString());
                    aPlayer.getPlayer().sendMessage("");
                    aPlayer.getPlayer().closeInventory();
                });
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        page.setRows(row);
                        InventoryGui gui = new InventoryGui(aPlayer.getPlayer(), "ProfileGui", page);
                        aPlayer.getData("profileGuiGui").setValue(gui);
                        gui.openPage(1);
                    }
                }.runTask(ServerLink.plugin);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        aPlayer.getData("profileGuiGui").setValue(null);
                    }
                }.runTaskLater(ServerLink.plugin, 20 * 90);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        aPlayer.getPlayer().updateInventory();
                    }
                }.runTaskLater(ServerLink.plugin, 7);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }
}

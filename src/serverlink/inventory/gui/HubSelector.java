package serverlink.inventory.gui;


import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.network.NetworkCache;
import serverlink.util.Item;

import java.util.ArrayList;

public class HubSelector {
    public static ItemStack item = Item.easyCreate("endportalframe", "&d&lHub Selector");
    private static GuiPage page = new GuiPage(chat.color("&d&lHub Selector"));
    private static GuiRow row1 = new GuiRow(null, page, 1);
    private static GuiRow row2 = new GuiRow(null, page, 2);
    private static GuiRow row3 = new GuiRow(null, page, 3);
    private static GuiRow row4 = new GuiRow(null, page, 4);
    private static GuiRow row5 = new GuiRow(null, page, 5);

    public static void setup() {
        page.setRows(row1, row2, row3, row4, row5);
        new BukkitRunnable() {
            @Override
            public void run() {
                int number = 0;
                for (String serv : NetworkCache.getAllCounts().keySet()) {
                    if (!serv.contains("hub")) continue;
                    number++;
                    String name = chat.color(serv.replace("hub-", "&d&lHub "));
                    ArrayList<String> lore = new ArrayList<>();
                    ItemStack i;
                    if (NetworkCache.getAvailability(serv).equals("open")) {
                        // lime stained glass pane
                        i = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 5);
                    } else {
                        // red stained glass pane
                        i = new ItemStack(Material.STAINED_GLASS_PANE, 1, (short) 4);
                    }
                    lore.add(" ");
                    lore.add(chat.color("&f" + NetworkCache.getCount(serv) + "/" + NetworkCache.getMaxPlayers(serv) + " &7players"));
                    ItemMeta iMeta = i.getItemMeta();
                    iMeta.setDisplayName(name);
                    iMeta.setLore(lore);
                    i.setItemMeta(iMeta);
                    if (serv.equals(ServerLink.getServerName())) i = Item.addGlow(i);
                    if (number > 36) {
                        row5.setItem(number - 36, i);
                    } else if (number > 27) {
                        row4.setItem(number - 27, i);
                    } else if (number > 18) {
                        row3.setItem(number - 18, i);
                    } else if (number > 9) {
                        row2.setItem(number - 9, i);
                    } else {
                        row1.setItem(number, i);
                    }
                }
            }
        }.runTaskTimer(ServerLink.plugin, 20, 30);
    }

    public static InventoryGui createGui(Player player) {
        return new InventoryGui(player, "HubSelector", page);
    }


    public static void inv(Player player, ItemStack item) {
        if (item == null) return;
        if (item.getType() == null) return;
        if (item.getType() == Material.AIR) return;
        ItemMeta meta = item.getItemMeta();
        String server = meta.getDisplayName().toLowerCase().replace(' ', '-');
        if (server.equals(ServerLink.getServerName())) return;
        player.performCommand("server " + server);
    }
}

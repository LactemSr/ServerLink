package serverlink.inventory.gui;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.command.GlistCommand;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.util.Item;

import java.util.ArrayList;
import java.util.List;

public class GameSelector {
    public static ItemStack item = Item.easyCreate("compass", "&d&lGame Selector");
    private static GuiPage page = new GuiPage(chat.color("&d&lGame Selector"));
    private static GuiRow row1 = new GuiRow(null, page, 1);
    private static ItemStack kitpvp = new ItemStack(Material.DIAMOND_CHESTPLATE);
    private static ItemMeta kitpvpMeta = kitpvp.getItemMeta();
    private static ItemStack skyblock = new ItemStack(Material.GRASS);
    private static ItemMeta skyblockMeta = skyblock.getItemMeta();
    private static ItemStack skybattle = Item.easyCreate("spawnvillager", "");
    private static ItemMeta skybattleMeta = skybattle.getItemMeta();

    public static void setup() {

        kitpvpMeta.setDisplayName(chat.color("&6&lKitPvP"));
        List<String> kitpvpLore = new ArrayList<>();
        kitpvpLore.add("");
        kitpvpLore.add(chat.color("&eCompletely custom kitpvp"));
        kitpvpLore.add(chat.color("&ewith epic abilities, superb"));
        kitpvpLore.add(chat.color("&eanti-cheat, balanced classes,"));
        kitpvpLore.add(chat.color("&epowerups, and more!"));
        kitpvpLore.add("");
        kitpvpLore.add(ChatColor.BLUE + "" + GlistCommand.getKitpvpCount() + ChatColor.GRAY + " players connected");
        kitpvpMeta.setLore(kitpvpLore);
        kitpvp.setItemMeta(kitpvpMeta);
        row1.setItem(3, kitpvp);

        skyblockMeta.setDisplayName(chat.color("&6&lSkyblock"));
        skyblockMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_POTION_EFFECTS, ItemFlag.HIDE_ENCHANTS);
        List<String> skyblockLore = new ArrayList<>();
        skyblockLore.add("");
        skyblockLore.add(chat.color("&eComing Soon!"));
        /*skyblockLore.add(chat.color("&eThe most feature-packed yet"));
        skyblockLore.add(chat.color("&eeasy-to-use skyblock ever"));
        skyblockLore.add(chat.color("&emade! You can do anything you"));
        skyblockLore.add(chat.color("&ewant from the /is menu: trade,"));
        skyblockLore.add(chat.color("&evisit shops and arenas"));
        skyblockLore.add(chat.color("&etravel to islands on the"));
        skyblockLore.add(chat.color("&eleaderboard, configure your"));
        skyblockLore.add(chat.color("&eisland's time/biome/size,"));
        skyblockLore.add(chat.color("&eand manage builders on"));
        skyblockLore.add(chat.color("&eyour island."));*/
        skyblockLore.add("");
        skyblockLore.add(ChatColor.BLUE + "" + GlistCommand.getSkyblockCount() + ChatColor.GRAY + " players connected");
        skyblockMeta.setLore(skyblockLore);
        skyblock.setItemMeta(skyblockMeta);
        row1.setItem(5, skyblock);

        skybattleMeta.setDisplayName(chat.color("&6&lSky Battle"));
        skybattleMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES, ItemFlag.HIDE_POTION_EFFECTS, ItemFlag.HIDE_ENCHANTS);
        List<String> skybattleLore = new ArrayList<>();
        skybattleLore.add("");
        skybattleLore.add(chat.color("&eProtect your team's villager,"));
        skybattleLore.add(chat.color("&euse him to buy weapons and"));
        skybattleLore.add(chat.color("&eupgrades, and take out other"));
        skybattleLore.add(chat.color("&eteam villagers. Once your"));
        skybattleLore.add(chat.color("&evillager dies, you and your"));
        skybattleLore.add(chat.color("&eteammates cannot respawn."));
        skybattleLore.add("");
        skybattleLore.add(ChatColor.BLUE + "" + GlistCommand.getSkybattleCount() + ChatColor.GRAY + " players connected");
        skybattleMeta.setLore(skybattleLore);
        skybattle.setItemMeta(skybattleMeta);
        row1.setItem(7, skybattle);

        page.setRows(row1);

        new BukkitRunnable() {
            @Override
            public void run() {
                kitpvpLore.set(kitpvpLore.size() - 1, ChatColor.BLUE + "" +
                        GlistCommand.getKitpvpCount() + ChatColor.GRAY + " players connected");
                kitpvpMeta.setLore(kitpvpLore);
                kitpvp.setItemMeta(kitpvpMeta);
                row1.setItem(3, kitpvp);

                skyblockLore.set(skyblockLore.size() - 1, ChatColor.BLUE + "" +
                        GlistCommand.getSkyblockCount() + ChatColor.GRAY + " players connected");
                skyblockMeta.setLore(skyblockLore);
                skyblock.setItemMeta(skyblockMeta);
                row1.setItem(5, skyblock);

                skybattleLore.set(skybattleLore.size() - 1, ChatColor.BLUE + "" +
                        GlistCommand.getSkybattleCount() + ChatColor.GRAY + " players connected");
                skybattleMeta.setLore(skybattleLore);
                skybattle.setItemMeta(skybattleMeta);
                row1.setItem(7, skybattle);
            }
        }.runTaskTimer(ServerLink.plugin, 20, 20);
    }

    public static InventoryGui createGui(Player player) {
        return new InventoryGui(player, "GameSelector", page);
    }

    public static void inv(Player player, ItemStack item) {
        if (item == null) return;
        if (item.getType() == null) return;
        if (item.getType() == Material.AIR) return;
        String name = "kitpvp";
        if (Item.isSimilar(item, kitpvp)) name = "kitpvp";
        else if (Item.isSimilar(item, skyblock)) name = "skyblock";
        else if (Item.isSimilar(item, skybattle)) name = "skybattle";
        player.performCommand(name);
    }
}

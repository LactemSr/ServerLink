package serverlink.inventory.gui.stats;

import serverlink.alphaplayer.AlphaPlayer;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.util.Item;

public class MainStatsGui {
    public static void openMainStatsGui(AlphaPlayer aPlayer) {
        GuiPage page = new GuiPage("&a&lStats");
        GuiRow row1 = new GuiRow(null, page, 1);
        row1.setItem(2, Item.easyCreate("endportalframe", "&a&lGlobal Stats", "", "&eClick to view &a&lGlobal Stats"));
        row1.setClick(2, () -> GlobalStatsGui.openGlobalStatsGui(aPlayer, () -> openMainStatsGui(aPlayer)));
        row1.setItem(4, Item.easyCreate("diamondchestplate", "&a&lKit PvP Stats", "", "&eClick to view &a&lKitPvP Stats"));
        row1.setClick(4, () -> KitpvpStatsGui.openKitpvpStatsGui(aPlayer, () -> openMainStatsGui(aPlayer)));
        row1.setItem(6, Item.easyCreate("wool:yellow", "&a&lSky Battle Stats", "", "&eClick to view &a&lSkyBattle Stats"));
        row1.setClick(6, () -> SkybattleStatsGui.openSkybattleStatsGui(aPlayer, () -> openMainStatsGui(aPlayer)));
        row1.setItem(8, Item.easyCreate("grassblock", "&a&lSkyblock Stats", "", "&eComing Soon"));
        page.setRows(row1);
        new InventoryGui(aPlayer.getPlayer(), "MainStatsGui", page).openPage(1);
    }
}

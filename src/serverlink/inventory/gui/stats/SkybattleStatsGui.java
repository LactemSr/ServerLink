package serverlink.inventory.gui.stats;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.chat.chat;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.player.Points;
import serverlink.player.stats.Ranking;
import serverlink.player.stats.StatsManager;
import serverlink.util.CachedDecimalFormat;
import serverlink.util.Item;

import java.util.ArrayList;
import java.util.List;

public class SkybattleStatsGui {
    public static void openSkybattleStatsGui(AlphaPlayer aPlayer, Runnable backButton) {
        GuiPage page = new GuiPage("&a&lSkyBattle Stats");
        GuiRow row1 = new GuiRow(null, page, 1);
        GuiRow row2 = new GuiRow(null, page, 2);
        GuiRow row3 = new GuiRow(null, page, 3);
        GuiRow row4 = new GuiRow(null, page, 4);
        GuiRow row5 = new GuiRow(null, page, 5);
        row1.setItem(5, Item.easyCreate("arrow", "&r&fBack"));
        row1.setClick(5, backButton);

        // PLAY TIME
        Ranking ranking = StatsManager.getTopTenAlltime("skybattlePlayTime");
        ItemStack skybattlePlayTime = Item.easyCreate("clock", "&a&lMost Hours Played &7(&a&lAll Time&7)");
        ItemMeta meta = skybattlePlayTime.getItemMeta();
        List<String> lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(StatsManager.getAlltimeStat_UnformattedDouble(aPlayer, "skybattlePlayTime")) + " hours"));
        meta.setLore(lore);
        skybattlePlayTime.setItemMeta(meta);
        row2.setItem(1, skybattlePlayTime);
        // play time weekly
        ranking = StatsManager.getTopTenWeekly("skybattlePlayTime");
        skybattlePlayTime = Item.easyCreate("clock", "&a&lMost Hours Played &7(&a&lThis Week&7)");
        meta = skybattlePlayTime.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(StatsManager.getWeeklyStat_UnformattedDouble(aPlayer, "skybattlePlayTime")) + " hours"));
        meta.setLore(lore);
        skybattlePlayTime.setItemMeta(meta);
        row2.setItem(2, skybattlePlayTime);
        // play time daily
        ranking = StatsManager.getTopTenDaily("skybattlePlayTime");
        skybattlePlayTime = Item.easyCreate("clock", "&a&lMost Hours Played &7(&a&lToday&7)");
        meta = skybattlePlayTime.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(StatsManager.getDailyStat_UnformattedDouble(aPlayer, "skybattlePlayTime")) + " hours"));
        meta.setLore(lore);
        skybattlePlayTime.setItemMeta(meta);
        row2.setItem(3, skybattlePlayTime);

        // SKY GEMS GAINED
        ranking = StatsManager.getTopTenAlltime("skybattlePoints");
        ItemStack skybattlePoints = Item.easyCreate("goldingot", "&a&lMost Sky Gems Gained &7(&a&lAll Time&7)");
        meta = skybattlePoints.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFirstPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSecondPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getThirdPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFourthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFifthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSixthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSeventhPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getEighthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getNinthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getTenthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + Points.formatPoints(StatsManager.getAlltimeStat_Integer(aPlayer, "skybattlePoints")) + "&r&e Sky Gems"));
        meta.setLore(lore);
        skybattlePoints.setItemMeta(meta);
        row2.setItem(7, skybattlePoints);
        // Sky Gems gained weekly
        ranking = StatsManager.getTopTenWeekly("skybattlePoints");
        skybattlePoints = Item.easyCreate("goldingot", "&a&lMost Sky Gems Gained &7(&a&lThis Week&7)");
        meta = skybattlePoints.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFirstPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSecondPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getThirdPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFourthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFifthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSixthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSeventhPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getEighthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getNinthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getTenthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + Points.formatPoints(StatsManager.getWeeklyStat_Integer(aPlayer, "skybattlePoints")) + "&r&e Sky Gems"));
        meta.setLore(lore);
        skybattlePoints.setItemMeta(meta);
        row2.setItem(8, skybattlePoints);
        // Sky Gems gained daily
        ranking = StatsManager.getTopTenDaily("skybattlePoints");
        skybattlePoints = Item.easyCreate("goldingot", "&a&lMost Sky Gems Gained &7(&a&lToday&7)");
        meta = skybattlePoints.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFirstPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSecondPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getThirdPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFourthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFifthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSixthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSeventhPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getEighthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getNinthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getTenthPlace().getStatValue()) + "&r&e Sky Gems"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + Points.formatPoints(StatsManager.getDailyStat_Integer(aPlayer, "skybattlePoints")) + "&r&e Sky Gems"));
        meta.setLore(lore);
        skybattlePoints.setItemMeta(meta);
        row2.setItem(9, skybattlePoints);

        // MOST BLOCKS PLACED
        ranking = StatsManager.getTopTenAlltime("skybattleBlocksPlaced");
        ItemStack skybattleBlocksPlaced = Item.easyCreate("wool:red", "&a&lMost Blocks Placed &7(&a&lAll Time&7)");
        meta = skybattleBlocksPlaced.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getAlltimeStat_Integer(aPlayer, "skybattleBlocksPlaced") + " blocks placed"));
        meta.setLore(lore);
        skybattleBlocksPlaced.setItemMeta(meta);
        row3.setItem(4, skybattleBlocksPlaced);
        // most blocks placed weekly
        ranking = StatsManager.getTopTenWeekly("skybattleBlocksPlaced");
        skybattleBlocksPlaced = Item.easyCreate("wool:yellow", "&a&lMost Blocks Placed &7(&a&lThis Week&7)");
        meta = skybattleBlocksPlaced.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getWeeklyStat_Integer(aPlayer, "skybattleBlocksPlaced") + " blocks placed"));
        meta.setLore(lore);
        skybattleBlocksPlaced.setItemMeta(meta);
        row3.setItem(5, skybattleBlocksPlaced);
        // most blocks placed daily
        ranking = StatsManager.getTopTenDaily("skybattleBlocksPlaced");
        skybattleBlocksPlaced = Item.easyCreate("wool:green", "&a&lMost Blocks Placed &7(&a&lToday&7)");
        meta = skybattleBlocksPlaced.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " blocks placed"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getDailyStat_Integer(aPlayer, "skybattleBlocksPlaced") + " blocks placed"));
        meta.setLore(lore);
        skybattleBlocksPlaced.setItemMeta(meta);
        row3.setItem(6, skybattleBlocksPlaced);

        // KILLS MADE
        ranking = StatsManager.getTopTenAlltime("skybattleKills");
        ItemStack kills = Item.easyCreate("redstone", "&a&lMost Kills Made &7(&a&lAll Time&7)");
        meta = kills.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getAlltimeStat_Integer(aPlayer, "skybattleKills") + " kills"));
        meta.setLore(lore);
        kills.setItemMeta(meta);
        row5.setItem(1, kills);
        // kills made weekly
        ranking = StatsManager.getTopTenWeekly("skybattleKills");
        kills = Item.easyCreate("redstone", "&a&lMost Kills Made &7(&a&lThis Week&7)");
        meta = kills.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getWeeklyStat_Integer(aPlayer, "skybattleKills") + " kills"));
        meta.setLore(lore);
        kills.setItemMeta(meta);
        row5.setItem(2, kills);
        // kills made daily
        ranking = StatsManager.getTopTenDaily("skybattleKills");
        kills = Item.easyCreate("redstone", "&a&lMost Kills Made &7(&a&lToday&7)");
        meta = kills.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getDailyStat_Integer(aPlayer, "skybattleKills") + " kills"));
        meta.setLore(lore);
        kills.setItemMeta(meta);
        row5.setItem(3, kills);

        // MOST VILLAGERS KILLED
        ranking = StatsManager.getTopTenAlltime("skybattleVillagersKilled");
        ItemStack skybattleVillagersKilled = Item.easyCreate("redstonecomparator", "&a&lMost Villagers Slain &7(&a&lAll Time&7)");
        meta = skybattleVillagersKilled.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(StatsManager.getAlltimeStat_UnformattedDouble(aPlayer, "skybattleVillagersKilled")) + " villagers"));
        meta.setLore(lore);
        skybattleVillagersKilled.setItemMeta(meta);
        row5.setItem(7, skybattleVillagersKilled);
        // most villagers killed weekly
        ranking = StatsManager.getTopTenWeekly("skybattleVillagersKilled");
        skybattleVillagersKilled = Item.easyCreate("redstonecomparator", "&a&lMost Villagers Slain &7(&a&lThis Week&7)");
        meta = skybattleVillagersKilled.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(StatsManager.getWeeklyStat_UnformattedDouble(aPlayer, "skybattleVillagersKilled")) + " villagers"));
        meta.setLore(lore);
        skybattleVillagersKilled.setItemMeta(meta);
        row5.setItem(8, skybattleVillagersKilled);
        // most villagers killed daily
        ranking = StatsManager.getTopTenDaily("skybattleVillagersKilled");
        skybattleVillagersKilled = Item.easyCreate("redstonecomparator", "&a&lMost Villagers Slain &7(&a&lToday&7)");
        meta = skybattleVillagersKilled.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " villagers"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(StatsManager.getDailyStat_UnformattedDouble(aPlayer, "skybattleVillagersKilled")) + " villagers"));
        meta.setLore(lore);
        skybattleVillagersKilled.setItemMeta(meta);
        row5.setItem(9, skybattleVillagersKilled);

        page.setRows(row1, row2, row3, row4, row5);
        new InventoryGui(aPlayer.getPlayer(), "SkybattleStatsGui", page).openPage(1);
    }
}

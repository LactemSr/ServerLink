package serverlink.inventory.gui.stats;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.chat.chat;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.player.Coins;
import serverlink.player.stats.Ranking;
import serverlink.player.stats.StatsManager;
import serverlink.util.CachedDecimalFormat;
import serverlink.util.Item;

import java.util.ArrayList;
import java.util.List;

public class GlobalStatsGui {
    public static void openGlobalStatsGui(AlphaPlayer aPlayer, Runnable backButton) {
        GuiPage page = new GuiPage("&a&lGlobal Stats");
        GuiRow row1 = new GuiRow(null, page, 1);
        GuiRow row2 = new GuiRow(null, page, 2);
        GuiRow row3 = new GuiRow(null, page, 3);
        row1.setItem(5, Item.easyCreate("arrow", "&r&fBack"));
        row1.setClick(5, backButton);

        // PLAY TIME
        Ranking ranking = StatsManager.getTopTenAlltime("playTime");
        ItemStack playTime = Item.easyCreate("clock", "&a&lMost Hours Played &7(&a&lAll Time&7)");
        ItemMeta meta = playTime.getItemMeta();
        List<String> lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(StatsManager.getAlltimeStat_UnformattedDouble(aPlayer, "playTime")) + " hours"));
        meta.setLore(lore);
        playTime.setItemMeta(meta);
        row2.setItem(2, playTime);
        // play time weekly
        ranking = StatsManager.getTopTenWeekly("playTime");
        playTime = Item.easyCreate("clock", "&a&lMost Hours Played &7(&a&lThis Week&7)");
        meta = playTime.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(StatsManager.getWeeklyStat_UnformattedDouble(aPlayer, "playTime")) + " hours"));
        meta.setLore(lore);
        playTime.setItemMeta(meta);
        row2.setItem(3, playTime);
        // play time daily
        ranking = StatsManager.getTopTenDaily("playTime");
        playTime = Item.easyCreate("clock", "&a&lMost Hours Played &7(&a&lToday&7)");
        meta = playTime.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(StatsManager.getDailyStat_UnformattedDouble(aPlayer, "playTime")) + " hours"));
        meta.setLore(lore);
        playTime.setItemMeta(meta);
        row2.setItem(4, playTime);

        // FRIENDS REFERRED
        ranking = StatsManager.getTopTenAlltime("referrals");
        ItemStack referrals = Item.easyCreate("vines", "&a&lMost Friends Referred &7(&a&lAll Time&7)");
        meta = referrals.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getAlltimeStat_Integer(aPlayer, "referrals") + " referrals"));
        meta.setLore(lore);
        referrals.setItemMeta(meta);
        row2.setItem(6, referrals);
        // friends referred weekly
        ranking = StatsManager.getTopTenWeekly("referrals");
        referrals = Item.easyCreate("vines", "&a&lMost Friends Referred &7(&a&lThis Week&7)");
        meta = referrals.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getWeeklyStat_Integer(aPlayer, "referrals") + " referrals"));
        meta.setLore(lore);
        referrals.setItemMeta(meta);
        row2.setItem(7, referrals);
        // friends referred daily
        ranking = StatsManager.getTopTenDaily("referrals");
        referrals = Item.easyCreate("vines", "&a&lMost Friends Referred &7(&a&lToday&7)");
        meta = referrals.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " referrals"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getDailyStat_Integer(aPlayer, "referrals") + " referrals"));
        meta.setLore(lore);
        referrals.setItemMeta(meta);
        row2.setItem(8, referrals);

        // EXP GAINED
        ranking = StatsManager.getTopTenAlltime("exp");
        ItemStack exp = Item.easyCreate("bottleoenchanting", "&a&lMost Exp Earned &7(&a&lAll Time&7)");
        meta = exp.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getFirstPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getSecondPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getThirdPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getFourthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getFifthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getSixthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getSeventhPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getEighthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getNinthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getTenthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.ONES_COMMAS.format(StatsManager.getAlltimeStat_Integer(aPlayer, "exp")) + " exp"));
        meta.setLore(lore);
        exp.setItemMeta(meta);
        row3.setItem(1, exp);
        // exp gained weekly
        ranking = StatsManager.getTopTenWeekly("exp");
        exp = Item.easyCreate("bottleoenchanting", "&a&lMost Exp Earned &7(&a&lThis Week&7)");
        meta = exp.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getFirstPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getSecondPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getThirdPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getFourthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getFifthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getSixthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getSeventhPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getEighthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getNinthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getTenthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.ONES_COMMAS.format(StatsManager.getWeeklyStat_Integer(aPlayer, "exp")) + " exp"));
        meta.setLore(lore);
        exp.setItemMeta(meta);
        row3.setItem(2, exp);
        // exp gained daily
        ranking = StatsManager.getTopTenDaily("exp");
        exp = Item.easyCreate("bottleoenchanting", "&a&lMost Exp Earned &7(&a&lToday&7)");
        meta = exp.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getFirstPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getSecondPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getThirdPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getFourthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getFifthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getSixthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getSeventhPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getEighthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getNinthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.ONES_COMMAS.format(ranking.getTenthPlace().getStatValue()) + " exp"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.ONES_COMMAS.format(StatsManager.getDailyStat_Integer(aPlayer, "exp")) + " exp"));
        meta.setLore(lore);
        exp.setItemMeta(meta);
        row3.setItem(3, exp);

        // COINS GAINED
        ranking = StatsManager.getTopTenAlltime("coins");
        ItemStack coins = Item.easyCreate("sunflower", "&a&lMost Coins Gained &7(&a&lAll Time&7)");
        meta = coins.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getFirstPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getSecondPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getThirdPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getFourthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getFifthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getSixthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getSeventhPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getEighthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getNinthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getTenthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + Coins.formatCoins(StatsManager.getAlltimeStat_Integer(aPlayer, "coins")) + "&r&e coins"));
        meta.setLore(lore);
        coins.setItemMeta(meta);
        row3.setItem(7, coins);
        // coins gained weekly
        ranking = StatsManager.getTopTenWeekly("coins");
        coins = Item.easyCreate("sunflower", "&a&lMost Coins Gained &7(&a&lThis Week&7)");
        meta = coins.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getFirstPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getSecondPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getThirdPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getFourthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getFifthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getSixthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getSeventhPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getEighthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getNinthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getTenthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + Coins.formatCoins(StatsManager.getWeeklyStat_Integer(aPlayer, "coins")) + "&r&e coins"));
        meta.setLore(lore);
        coins.setItemMeta(meta);
        row3.setItem(8, coins);
        // coins gained daily
        ranking = StatsManager.getTopTenDaily("coins");
        coins = Item.easyCreate("sunflower", "&a&lMost Coins Gained &7(&a&lToday&7)");
        meta = coins.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getFirstPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getSecondPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getThirdPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getFourthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getFifthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getSixthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getSeventhPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getEighthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getNinthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + Coins.formatCoins((int) ranking.getTenthPlace().getStatValue()) + "&r&e coins"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + Coins.formatCoins(StatsManager.getDailyStat_Integer(aPlayer, "coins")) + "&r&e coins"));
        meta.setLore(lore);
        coins.setItemMeta(meta);
        row3.setItem(9, coins);

        page.setRows(row1, row2, row3);
        new InventoryGui(aPlayer.getPlayer(), "GlobalStatsGui", page).openPage(1);
    }
}

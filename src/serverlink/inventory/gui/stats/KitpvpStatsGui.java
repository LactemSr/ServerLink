package serverlink.inventory.gui.stats;

import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.chat.chat;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.player.Points;
import serverlink.player.stats.Ranking;
import serverlink.player.stats.StatsManager;
import serverlink.util.CachedDecimalFormat;
import serverlink.util.Item;

import java.util.ArrayList;
import java.util.List;

public class KitpvpStatsGui {
    public static void openKitpvpStatsGui(AlphaPlayer aPlayer, Runnable backButton) {
        GuiPage page = new GuiPage("&a&lKitPvP Stats");
        GuiRow row1 = new GuiRow(null, page, 1);
        GuiRow row2 = new GuiRow(null, page, 2);
        GuiRow row3 = new GuiRow(null, page, 3);
        GuiRow row4 = new GuiRow(null, page, 4);
        GuiRow row5 = new GuiRow(null, page, 5);
        GuiRow row6 = new GuiRow(null, page, 6);
        row1.setItem(5, Item.easyCreate("arrow", "&r&fBack"));
        row1.setClick(5, backButton);

        // PLAY TIME
        Ranking ranking = StatsManager.getTopTenAlltime("kitpvpPlayTime");
        ItemStack kitpvpPlayTime = Item.easyCreate("clock", "&a&lMost Hours Played &7(&a&lAll Time&7)");
        ItemMeta meta = kitpvpPlayTime.getItemMeta();
        List<String> lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(StatsManager.getAlltimeStat_UnformattedDouble(aPlayer, "kitpvpPlayTime")) + " hours"));
        meta.setLore(lore);
        kitpvpPlayTime.setItemMeta(meta);
        row2.setItem(1, kitpvpPlayTime);
        // play time weekly
        ranking = StatsManager.getTopTenWeekly("kitpvpPlayTime");
        kitpvpPlayTime = Item.easyCreate("clock", "&a&lMost Hours Played &7(&a&lThis Week&7)");
        meta = kitpvpPlayTime.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(StatsManager.getWeeklyStat_UnformattedDouble(aPlayer, "kitpvpPlayTime")) + " hours"));
        meta.setLore(lore);
        kitpvpPlayTime.setItemMeta(meta);
        row2.setItem(2, kitpvpPlayTime);
        // play time daily
        ranking = StatsManager.getTopTenDaily("kitpvpPlayTime");
        kitpvpPlayTime = Item.easyCreate("clock", "&a&lMost Hours Played &7(&a&lToday&7)");
        meta = kitpvpPlayTime.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " hours"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.TENTHS_NO_TRAILING_ZEROS.format(StatsManager.getDailyStat_UnformattedDouble(aPlayer, "kitpvpPlayTime")) + " hours"));
        meta.setLore(lore);
        kitpvpPlayTime.setItemMeta(meta);
        row2.setItem(3, kitpvpPlayTime);

        // TOKENS GAINED
        ranking = StatsManager.getTopTenAlltime("kitpvpPoints");
        ItemStack kitpvpPoints = Item.easyCreate("goldingot", "&a&lMost Tokens Gained &7(&a&lAll Time&7)");
        meta = kitpvpPoints.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFirstPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSecondPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getThirdPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFourthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFifthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSixthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSeventhPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getEighthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getNinthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getTenthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + Points.formatPoints(StatsManager.getAlltimeStat_Integer(aPlayer, "kitpvpPoints")) + "&r&e tokens"));
        meta.setLore(lore);
        kitpvpPoints.setItemMeta(meta);
        row2.setItem(7, kitpvpPoints);
        // tokens gained weekly
        ranking = StatsManager.getTopTenWeekly("kitpvpPoints");
        kitpvpPoints = Item.easyCreate("goldingot", "&a&lMost Tokens Gained &7(&a&lThis Week&7)");
        meta = kitpvpPoints.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFirstPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSecondPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getThirdPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFourthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFifthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSixthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSeventhPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getEighthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getNinthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getTenthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + Points.formatPoints(StatsManager.getWeeklyStat_Integer(aPlayer, "kitpvpPoints")) + "&r&e tokens"));
        meta.setLore(lore);
        kitpvpPoints.setItemMeta(meta);
        row2.setItem(8, kitpvpPoints);
        // tokens gained daily
        ranking = StatsManager.getTopTenDaily("kitpvpPoints");
        kitpvpPoints = Item.easyCreate("goldingot", "&a&lMost Tokens Gained &7(&a&lToday&7)");
        meta = kitpvpPoints.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFirstPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSecondPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getThirdPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFourthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getFifthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSixthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getSeventhPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getEighthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getNinthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + Points.formatPoints((int) ranking.getTenthPlace().getStatValue()) + "&r&e tokens"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + Points.formatPoints(StatsManager.getDailyStat_Integer(aPlayer, "kitpvpPoints")) + "&r&e tokens"));
        meta.setLore(lore);
        kitpvpPoints.setItemMeta(meta);
        row2.setItem(9, kitpvpPoints);

        // KILLS MADE
        ranking = StatsManager.getTopTenAlltime("kitpvpKills");
        ItemStack kills = Item.easyCreate("redstone", "&a&lMost Kills Made &7(&a&lAll Time&7)");
        meta = kills.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getAlltimeStat_Integer(aPlayer, "kitpvpKills") + " kills"));
        meta.setLore(lore);
        kills.setItemMeta(meta);
        row4.setItem(2, kills);
        // kills made weekly
        ranking = StatsManager.getTopTenWeekly("kitpvpKills");
        kills = Item.easyCreate("redstone", "&a&lMost Kills Made &7(&a&lThis Week&7)");
        meta = kills.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getWeeklyStat_Integer(aPlayer, "kitpvpKills") + " kills"));
        meta.setLore(lore);
        kills.setItemMeta(meta);
        row4.setItem(3, kills);
        // kills made daily
        ranking = StatsManager.getTopTenDaily("kitpvpKills");
        kills = Item.easyCreate("redstone", "&a&lMost Kills Made &7(&a&lToday&7)");
        meta = kills.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " kills"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getDailyStat_Integer(aPlayer, "kitpvpKills") + " kills"));
        meta.setLore(lore);
        kills.setItemMeta(meta);
        row4.setItem(4, kills);

        // HIGHEST KILLSTREAK ATTAINED
        ranking = StatsManager.getTopTenAlltime("kitpvpKillstreak");
        ItemStack kitpvpKillstreak = Item.easyCreate("diamondsword", "&a&lHighest Killstreak Attained &7(&a&lAll Time&7)");
        meta = kitpvpKillstreak.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getAlltimeStat_Integer(aPlayer, "kitpvpKillstreak") + " kills in a row"));
        meta.setLore(lore);
        kitpvpKillstreak.setItemMeta(meta);
        row4.setItem(6, kitpvpKillstreak);
        // highest killstreak attained weekly
        ranking = StatsManager.getTopTenWeekly("kitpvpKillstreak");
        kitpvpKillstreak = Item.easyCreate("diamondsword", "&a&lHighest Killstreak Attained &7(&a&lThis Week&7)");
        meta = kitpvpKillstreak.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getWeeklyStat_Integer(aPlayer, "kitpvpKillstreak") + " kills in a row"));
        meta.setLore(lore);
        kitpvpKillstreak.setItemMeta(meta);
        row4.setItem(7, kitpvpKillstreak);
        // highest killstreak attained daily
        ranking = StatsManager.getTopTenDaily("kitpvpKillstreak");
        kitpvpKillstreak = Item.easyCreate("diamondsword", "&a&lHighest Killstreak Attained &7(&a&lToday&7)");
        meta = kitpvpKillstreak.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " kills in a row"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getDailyStat_Integer(aPlayer, "kitpvpKillstreak") + " kills in a row"));
        meta.setLore(lore);
        kitpvpKillstreak.setItemMeta(meta);
        row4.setItem(8, kitpvpKillstreak);

        // HIGHEST KDR ATTAINED
        ranking = StatsManager.getTopTenAlltime("kitpvpKdr");
        ItemStack kitpvpKdr = Item.easyCreate("redstonecomparator", "&a&lHighest KDR Attained &7(&a&lAll Time&7)");
        meta = kitpvpKdr.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(StatsManager.getAlltimeStat_UnformattedDouble(aPlayer, "kitpvpKdr")) + " kills per death"));
        meta.setLore(lore);
        kitpvpKdr.setItemMeta(meta);
        row6.setItem(1, kitpvpKdr);
        // highest kdr attained weekly
        ranking = StatsManager.getTopTenWeekly("kitpvpKdr");
        kitpvpKdr = Item.easyCreate("redstonecomparator", "&a&lHighest KDR Attained &7(&a&lThis Week&7)");
        meta = kitpvpKdr.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(StatsManager.getWeeklyStat_UnformattedDouble(aPlayer, "kitpvpKdr")) + " kills per death"));
        meta.setLore(lore);
        kitpvpKdr.setItemMeta(meta);
        row6.setItem(2, kitpvpKdr);
        // highest kdr attained daily
        ranking = StatsManager.getTopTenDaily("kitpvpKdr");
        kitpvpKdr = Item.easyCreate("redstonecomparator", "&a&lHighest KDR Attained &7(&a&lToday&7)");
        meta = kitpvpKdr.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFirstPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSecondPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getThirdPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFourthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getFifthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSixthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getSeventhPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getEighthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getNinthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(ranking.getTenthPlace().getStatValue()) + " kills per death"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + CachedDecimalFormat.HUNDREDTHS_NO_TRAILING_ZEROS.format(StatsManager.getDailyStat_UnformattedDouble(aPlayer, "kitpvpKdr")) + " kills per death"));
        meta.setLore(lore);
        kitpvpKdr.setItemMeta(meta);
        row6.setItem(3, kitpvpKdr);

        // MOST POWERUPS COLLECTED
        ranking = StatsManager.getTopTenAlltime("kitpvpPowerups");
        ItemStack kitpvpPowerups = Item.easyCreate("redmushroom", "&a&lMost Powerups Collected &7(&a&lAll Time&7)");
        meta = kitpvpPowerups.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getAlltimeStat_Integer(aPlayer, "kitpvpPowerups") + " powerups"));
        meta.setLore(lore);
        kitpvpPowerups.setItemMeta(meta);
        row6.setItem(7, kitpvpPowerups);
        // most powerups collected weekly
        ranking = StatsManager.getTopTenWeekly("kitpvpPowerups");
        kitpvpPowerups = Item.easyCreate("redmushroom", "&a&lMost Powerups Collected &7(&a&lThis Week&7)");
        meta = kitpvpPowerups.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getWeeklyStat_Integer(aPlayer, "kitpvpPowerups") + " powerups"));
        meta.setLore(lore);
        kitpvpPowerups.setItemMeta(meta);
        row6.setItem(8, kitpvpPowerups);
        // most powerups collected daily
        ranking = StatsManager.getTopTenDaily("kitpvpPowerups");
        kitpvpPowerups = Item.easyCreate("redmushroom", "&a&lMost Powerups Collected &7(&a&lToday&7)");
        meta = kitpvpPowerups.getItemMeta();
        lore = new ArrayList<>();
        lore.add(chat.color(""));
        if (ranking.getFirstPlace() != null)
            lore.add(chat.color("&c&l1ˢᵗ: &a" + ranking.getFirstPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFirstPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l1ˢᵗ: &7---"));
        if (ranking.getSecondPlace() != null)
            lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &a" + ranking.getSecondPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSecondPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l2&r&cⁿ&c&lᵈ: &7---"));
        if (ranking.getThirdPlace() != null)
            lore.add(chat.color("&c&l3ʳᵈ: &a" + ranking.getThirdPlace().getPlayerName() + "&7 - &e" + (int) ranking.getThirdPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l3ʳᵈ: &7---"));
        if (ranking.getFourthPlace() != null)
            lore.add(chat.color("&c&l4ᵗʰ: &a" + ranking.getFourthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFourthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l4ᵗʰ: &7---"));
        if (ranking.getFifthPlace() != null)
            lore.add(chat.color("&c&l5ᵗʰ: &a" + ranking.getFifthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getFifthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l5ᵗʰ: &7---"));
        if (ranking.getSixthPlace() != null)
            lore.add(chat.color("&c&l6ᵗʰ: &a" + ranking.getSixthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSixthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l6ᵗʰ: &7---"));
        if (ranking.getSeventhPlace() != null)
            lore.add(chat.color("&c&l7ᵗʰ: &a" + ranking.getSeventhPlace().getPlayerName() + "&7 - &e" + (int) ranking.getSeventhPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l7ᵗʰ: &7---"));
        if (ranking.getEighthPlace() != null)
            lore.add(chat.color("&c&l8ᵗʰ: &a" + ranking.getEighthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getEighthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l8ᵗʰ: &7---"));
        if (ranking.getNinthPlace() != null)
            lore.add(chat.color("&c&l9ᵗʰ: &a" + ranking.getNinthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getNinthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l9ᵗʰ: &7---"));
        if (ranking.getTenthPlace() != null)
            lore.add(chat.color("&c&l10ᵗʰ: &a" + ranking.getTenthPlace().getPlayerName() + "&7 - &e" + (int) ranking.getTenthPlace().getStatValue() + " powerups"));
        else lore.add(chat.color("&c&l10ᵗʰ: &7---"));
        lore.add(chat.color(" "));
        lore.add(chat.color("&c&lYou: &e" + StatsManager.getDailyStat_Integer(aPlayer, "kitpvpPowerups") + " powerups"));
        meta.setLore(lore);
        kitpvpPowerups.setItemMeta(meta);
        row6.setItem(9, kitpvpPowerups);

        page.setRows(row1, row2, row3, row4, row5, row6);
        new InventoryGui(aPlayer.getPlayer(), "KitpvpStatsGui", page).openPage(1);
    }
}

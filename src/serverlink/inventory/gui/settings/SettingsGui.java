package serverlink.inventory.gui.settings;

import serverlink.chat.chat;
import serverlink.util.Item;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

public class SettingsGui implements Listener {
    public static String title = chat.color("&b&lMy Settings");
    public static Inventory inv = Bukkit.createInventory(null, 9, chat.color("&3&lMy Settings"));
    public static ItemStack settings = Item.easyCreate("daylightsensor", title);
    static String backToMainMenu = chat.color("Back to &b&lMy Settings");
    private static ItemStack colorPicker = Item.easyCreate("goldenapple:1", "&bColor Picker", "",
            "&7Change your name and",
            "&7chat styles.");
    private static ItemStack playersShown = Item.easyCreate("paper", "&bPlayers Visible in Hub", "",
            "&7Choose how many players",
            "&7to show in hubs.");
    private static ItemStack chatSettings = Item.easyCreate("eyeofender", "&bChat Settings", "",
            "&7Hide or show private",
            "&7messages, chat messages,",
            "&7friend requests, and party",
            "&7requests. Enable or disable",
            "&7chat mention sound alerts.");
    private static ItemStack speedSettings = Item.easyCreate("diamondboots", "&bSpeed Settings", "",
            "&7Change your speed in Hubs.");


    public static void setup() {
        inv.setItem(1, chatSettings);
        inv.setItem(3, colorPicker);
        inv.setItem(5, playersShown);
        inv.setItem(7, speedSettings);
    }

    @EventHandler
    private void onInvClick(InventoryClickEvent e) {
        if (!inv.getViewers().contains(e.getWhoClicked())) return;
        Player player = (Player) e.getWhoClicked();
        e.setCancelled(true);
        ItemStack is = e.getCurrentItem();
        if (Item.isSimilar(is, colorPicker)) ColorPickerGui.createGui(player).openPage(1);
        else if (Item.isSimilar(is, playersShown)) PlayersShownGui.createGui(player).openPage(1);
        else if (Item.isSimilar(is, chatSettings)) ChatSettingsGui.createGui(player).openPage(1);
        else if (Item.isSimilar(is, speedSettings)) SpeedSettingsGui.createGui(player).openPage(1);
    }
}
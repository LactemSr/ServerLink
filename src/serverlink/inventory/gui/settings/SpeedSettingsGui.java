package serverlink.inventory.gui.settings;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.customevent.CustomGuiClickEvent;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.server.ServerType;
import serverlink.util.Item;

public class SpeedSettingsGui implements Listener {
    private static String title = chat.color("&3&lSpeed Settings");
    private static ItemStack noSpeed = Item.easyCreate("milk", "&9No Speed");
    private static ItemStack speedOne = Item.easyCreate("waterbucket", "&9Speed I");
    private static ItemStack speedTwo = Item.easyCreate("lavabucket", "&9Speed II");

    static InventoryGui createGui(Player player) {
        AlphaPlayer aPlayer = PlayerManager.get(player);
        Object o = aPlayer.getData("speedSettingsGui").getValue();
        if (o instanceof InventoryGui) return (InventoryGui) o;
        GuiPage page = new GuiPage(title, null);
        GuiRow row = new GuiRow(null, page, 1);
        int setting = aPlayer.getIntegerSetting("speedSetting");

        if (setting == 1) row.setItem(2, Item.addGlow(noSpeed));
        else row.setItem(2, noSpeed);

        if (setting == 2) row.setItem(5, Item.addGlow(speedOne));
        else row.setItem(5, speedOne);

        if (setting == 3) row.setItem(8, Item.addGlow(speedTwo));
        else row.setItem(8, speedTwo);

        GuiRow row2 = new GuiRow(null, page, 2);
        row2.setItem(5, Item.easyCreate("arrow", SettingsGui.backToMainMenu));
        page.setRows(row, row2);
        InventoryGui gui = new InventoryGui(player, "SpeedSettings", page);
        aPlayer.getData("speedSettingsGui").setValue(gui);
        return gui;
    }

    @EventHandler
    private void onGuiClick(CustomGuiClickEvent e) {
        if (!e.getPage().getTitle().equals(title)) return;
        ItemStack is = e.getItem();
        ItemMeta meta = is.getItemMeta();
        String displayName = meta.getDisplayName();
        if (displayName.equals(SettingsGui.backToMainMenu)) {
            e.getPlayer().openInventory(SettingsGui.inv);
            return;
        }
        AlphaPlayer aPlayer = PlayerManager.get(e.getPlayer());
        int setting;
        GuiRow row = e.getRow();
        if (displayName.contains("II")) {
            setting = 3;
            row.setItem(2, noSpeed.clone());
            row.setItem(5, speedOne.clone());
            row.setItem(8, Item.addGlow(speedTwo.clone()));
            if (ServerLink.getServerType() == ServerType.hub)
                aPlayer.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 999999, 1, false, false), true);
        } else if (displayName.contains("I")) {
            setting = 2;
            row.setItem(2, noSpeed.clone());
            row.setItem(5, Item.addGlow(speedOne.clone()));
            row.setItem(8, speedTwo.clone());
            if (ServerLink.getServerType() == ServerType.hub)
                aPlayer.getPlayer().addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 999999, 0, false, false), true);
        } else if (displayName.contains("No")) {
            setting = 1;
            row.setItem(2, Item.addGlow(noSpeed.clone()));
            row.setItem(5, speedOne.clone());
            row.setItem(8, speedTwo.clone());
            if (ServerLink.getServerType() == ServerType.hub)
                aPlayer.getPlayer().removePotionEffect(PotionEffectType.SPEED);
        } else return;
        aPlayer.setSetting_SyncOnLogout("speedSetting", String.valueOf(setting));
    }
}

package serverlink.inventory.gui.settings;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.ChatFormat;
import serverlink.chat.NameFormat;
import serverlink.chat.chat;
import serverlink.customevent.CustomGuiClickEvent;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.player.Permissions;
import serverlink.player.Settings;
import serverlink.util.Item;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class ColorPickerGui implements Listener {
    private static String title = chat.color("&3&lName and Chat Settings");
    private static HashMap<NameFormat, Integer> nameRequiredRank = new HashMap<>();
    private static HashMap<NameFormat, String> nameItemTitle = new HashMap<>();
    private static HashMap<NameFormat, Material> nameMaterial = new HashMap<>();
    private static HashMap<NameFormat, Color> nameColor = new HashMap<>();
    private static HashMap<ChatFormat, Integer> chatRequiredRank = new HashMap<>();
    private static HashMap<ChatFormat, String> chatItemTitle = new HashMap<>();
    private static HashMap<ChatFormat, Material> chatMaterial = new HashMap<>();
    private static HashMap<ChatFormat, Color> chatColor = new HashMap<>();

    public static void setup() {
        // required ranks for name formats
        nameRequiredRank.put(NameFormat.WHITE, 1);
        nameRequiredRank.put(NameFormat.GREEN, 2);
        nameRequiredRank.put(NameFormat.LIGHT_PURPLE, 3);
        nameRequiredRank.put(NameFormat.BLUE, 4);
        nameRequiredRank.put(NameFormat.RED, 5);
        nameRequiredRank.put(NameFormat.RAINBOW, 5);
        nameRequiredRank.put(NameFormat.BOLD, 5);
        nameRequiredRank.put(NameFormat.ITALIC, 5);
        nameRequiredRank.put(NameFormat.DARK_GREEN, 6);
        nameRequiredRank.put(NameFormat.DARK_RED, 7);
        nameRequiredRank.put(NameFormat.AQUA, 7);
        nameRequiredRank.put(NameFormat.DARK_AQUA, 7);
        nameRequiredRank.put(NameFormat.DARK_PURPLE, 7);
        nameRequiredRank.put(NameFormat.YELLOW, 7);
        nameRequiredRank.put(NameFormat.GOLD, 10);
        // required ranks for chat formats
        chatRequiredRank.put(ChatFormat.GRAY, 1);
        chatRequiredRank.put(ChatFormat.WHITE, 2);
        chatRequiredRank.put(ChatFormat.AQUA, 3);
        chatRequiredRank.put(ChatFormat.BLUE, 4);
        chatRequiredRank.put(ChatFormat.RED, 5);
        chatRequiredRank.put(ChatFormat.UNICODE, 5);
        chatRequiredRank.put(ChatFormat.GREEN, 6);
        chatRequiredRank.put(ChatFormat.LIGHT_PURPLE, 9);
        chatRequiredRank.put(ChatFormat.DARK_GREEN, 9);
        chatRequiredRank.put(ChatFormat.DARK_RED, 9);
        chatRequiredRank.put(ChatFormat.DARK_AQUA, 9);
        chatRequiredRank.put(ChatFormat.BOLD, 9);
        chatRequiredRank.put(ChatFormat.ITALIC, 9);
        chatRequiredRank.put(ChatFormat.YELLOW, 9);
        chatRequiredRank.put(ChatFormat.GOLD, 10);
        // item display names for name formats
        nameItemTitle.put(NameFormat.BOLD, "&lBold Name");
        nameItemTitle.put(NameFormat.ITALIC, "&oItalic Name");
        nameItemTitle.put(NameFormat.RAINBOW, "&9R&aa&bi&cn&db&1o&3w &eN&6a&fm&5e");
        nameItemTitle.put(NameFormat.BLUE, "&9Blue Name");
        nameItemTitle.put(NameFormat.GREEN, "&aGreen Name");
        nameItemTitle.put(NameFormat.DARK_GREEN, "&2Dark Green Name");
        nameItemTitle.put(NameFormat.RED, "&cRed Name");
        nameItemTitle.put(NameFormat.DARK_RED, "&4Dark Red Name");
        nameItemTitle.put(NameFormat.AQUA, "&bAqua Name");
        nameItemTitle.put(NameFormat.DARK_AQUA, "&3Dark Aqua Name");
        nameItemTitle.put(NameFormat.LIGHT_PURPLE, "&dLight Purple Name");
        nameItemTitle.put(NameFormat.DARK_PURPLE, "&5Dark Purple Name");
        nameItemTitle.put(NameFormat.YELLOW, "&eYellow Name");
        nameItemTitle.put(NameFormat.WHITE, "&fWhite Name");
        nameItemTitle.put(NameFormat.GOLD, "&6Gold Name");
        // item display names for chat formats
        chatItemTitle.put(ChatFormat.BOLD, "&lBold Chat");
        chatItemTitle.put(ChatFormat.ITALIC, "&oItalic Chat");
        chatItemTitle.put(ChatFormat.UNICODE, chat.unicode("Fancy Chat"));
        chatItemTitle.put(ChatFormat.BLUE, "&9Blue Chat");
        chatItemTitle.put(ChatFormat.GREEN, "&aGreen Chat");
        chatItemTitle.put(ChatFormat.DARK_GREEN, "&2Dark Green Chat");
        chatItemTitle.put(ChatFormat.RED, "&cRed Chat");
        chatItemTitle.put(ChatFormat.DARK_RED, "&4Dark Red Chat");
        chatItemTitle.put(ChatFormat.AQUA, "&bAqua Chat");
        chatItemTitle.put(ChatFormat.DARK_AQUA, "&3Dark Aqua Chat");
        chatItemTitle.put(ChatFormat.LIGHT_PURPLE, "&dLight Purple Chat");
        chatItemTitle.put(ChatFormat.YELLOW, "&eYellow Chat");
        chatItemTitle.put(ChatFormat.WHITE, "&fWhite Chat");
        chatItemTitle.put(ChatFormat.GRAY, "&7Gray Chat");
        chatItemTitle.put(ChatFormat.GOLD, "&6Gold Chat");
        // material of item for name formats
        nameMaterial.put(NameFormat.BOLD, Material.DIAMOND);
        nameMaterial.put(NameFormat.ITALIC, Material.GOLD_NUGGET);
        nameMaterial.put(NameFormat.RAINBOW, Material.LEATHER_CHESTPLATE);
        nameMaterial.put(NameFormat.BLUE, Material.LEATHER_CHESTPLATE);
        nameMaterial.put(NameFormat.GREEN, Material.LEATHER_CHESTPLATE);
        nameMaterial.put(NameFormat.DARK_GREEN, Material.LEATHER_CHESTPLATE);
        nameMaterial.put(NameFormat.RED, Material.LEATHER_CHESTPLATE);
        nameMaterial.put(NameFormat.DARK_RED, Material.LEATHER_CHESTPLATE);
        nameMaterial.put(NameFormat.AQUA, Material.LEATHER_CHESTPLATE);
        nameMaterial.put(NameFormat.DARK_AQUA, Material.LEATHER_CHESTPLATE);
        nameMaterial.put(NameFormat.LIGHT_PURPLE, Material.LEATHER_CHESTPLATE);
        nameMaterial.put(NameFormat.DARK_PURPLE, Material.LEATHER_CHESTPLATE);
        nameMaterial.put(NameFormat.YELLOW, Material.LEATHER_CHESTPLATE);
        nameMaterial.put(NameFormat.WHITE, Material.LEATHER_CHESTPLATE);
        nameMaterial.put(NameFormat.GOLD, Material.LEATHER_CHESTPLATE);
        // material of item for chat formats
        chatMaterial.put(ChatFormat.BOLD, Material.DIAMOND);
        chatMaterial.put(ChatFormat.ITALIC, Material.GOLD_NUGGET);
        chatMaterial.put(ChatFormat.UNICODE, Material.ENCHANTMENT_TABLE);
        chatMaterial.put(ChatFormat.BLUE, Material.LEATHER_CHESTPLATE);
        chatMaterial.put(ChatFormat.GREEN, Material.LEATHER_CHESTPLATE);
        chatMaterial.put(ChatFormat.DARK_GREEN, Material.LEATHER_CHESTPLATE);
        chatMaterial.put(ChatFormat.RED, Material.LEATHER_CHESTPLATE);
        chatMaterial.put(ChatFormat.DARK_RED, Material.LEATHER_CHESTPLATE);
        chatMaterial.put(ChatFormat.AQUA, Material.LEATHER_CHESTPLATE);
        chatMaterial.put(ChatFormat.DARK_AQUA, Material.LEATHER_CHESTPLATE);
        chatMaterial.put(ChatFormat.LIGHT_PURPLE, Material.LEATHER_CHESTPLATE);
        chatMaterial.put(ChatFormat.YELLOW, Material.LEATHER_CHESTPLATE);
        chatMaterial.put(ChatFormat.WHITE, Material.LEATHER_CHESTPLATE);
        chatMaterial.put(ChatFormat.GRAY, Material.LEATHER_CHESTPLATE);
        chatMaterial.put(ChatFormat.GOLD, Material.LEATHER_CHESTPLATE);
        // leather chestplate colors for name formats
        nameColor.put(NameFormat.RAINBOW, Color.AQUA);
        nameColor.put(NameFormat.BLUE, Color.fromRGB(85, 85, 255));
        nameColor.put(NameFormat.GREEN, Color.fromRGB(85, 255, 85));
        nameColor.put(NameFormat.DARK_GREEN, Color.fromRGB(0, 170, 0));
        nameColor.put(NameFormat.RED, Color.fromRGB(255, 85, 85));
        nameColor.put(NameFormat.DARK_RED, Color.fromRGB(170, 0, 0));
        nameColor.put(NameFormat.AQUA, Color.fromRGB(85, 255, 255));
        nameColor.put(NameFormat.DARK_AQUA, Color.fromRGB(0, 170, 170));
        nameColor.put(NameFormat.LIGHT_PURPLE, Color.fromRGB(255, 85, 255));
        nameColor.put(NameFormat.DARK_PURPLE, Color.fromRGB(170, 0, 170));
        nameColor.put(NameFormat.YELLOW, Color.fromRGB(255, 255, 85));
        nameColor.put(NameFormat.WHITE, Color.fromRGB(255, 255, 255));
        nameColor.put(NameFormat.GOLD, Color.fromRGB(255, 170, 0));
        // leather chestplate colors for chat formats
        chatColor.put(ChatFormat.BLUE, Color.fromRGB(85, 85, 255));
        chatColor.put(ChatFormat.GREEN, Color.fromRGB(85, 255, 85));
        chatColor.put(ChatFormat.DARK_GREEN, Color.fromRGB(0, 170, 0));
        chatColor.put(ChatFormat.RED, Color.fromRGB(255, 85, 85));
        chatColor.put(ChatFormat.DARK_RED, Color.fromRGB(170, 0, 0));
        chatColor.put(ChatFormat.AQUA, Color.fromRGB(85, 255, 255));
        chatColor.put(ChatFormat.DARK_AQUA, Color.fromRGB(0, 170, 170));
        chatColor.put(ChatFormat.LIGHT_PURPLE, Color.fromRGB(255, 85, 255));
        chatColor.put(ChatFormat.YELLOW, Color.fromRGB(255, 255, 85));
        chatColor.put(ChatFormat.WHITE, Color.fromRGB(255, 255, 255));
        chatColor.put(ChatFormat.GRAY, Color.fromRGB(170, 170, 170));
        chatColor.put(ChatFormat.GOLD, Color.fromRGB(255, 170, 0));
    }

    public static InventoryGui createGui(Player player) {
        AlphaPlayer aPlayer = PlayerManager.get(player);
        Object o = aPlayer.getData("colorPickerGui").getValue();
        if (o instanceof InventoryGui) {
            BukkitRunnable runnable = new RainbowItemRunnable(player, ((InventoryGui) o).getPages()[0]);
            aPlayer.getData("colorPickerGuiRunnable").setValue(runnable);
            runnable.runTaskTimer(ServerLink.plugin, 0, 3);
            return (InventoryGui) o;
        }
        final GuiPage page = new GuiPage(title, null);
        ArrayList<GuiRow> rows = new ArrayList<>();
        int rank = PlayerManager.get(player).getRank();
        // name formatting
        List<String> stringFormats = aPlayer.getListSetting("nameFormat");
        ArrayList<NameFormat> nameFormats = stringFormats.stream().map(NameFormat::valueOf).collect(Collectors.toCollection(ArrayList::new));
        int rowNumber = 1;
        GuiRow row = new GuiRow(null, page, rowNumber);
        NameFormat[] nameValues = NameFormat.values();
        int slotNumber = 0;
        for (int i = 1; i <= 15; i++) {
            NameFormat f = nameValues[i - 1];
            slotNumber++;
            if (slotNumber == 10) {
                rowNumber++;
                rows.add(row);
                row = new GuiRow(null, page, rowNumber);
                slotNumber = 1;
            }
            ItemStack is = new ItemStack(nameMaterial.get(f));
            String line = chat.color("&aClick to Enable");
            if (nameRequiredRank.get(f) > rank)
                line = chat.color("&cRequires " + Permissions.getRankName(nameRequiredRank.get(f)));
            if (nameFormats.contains(f))
                line = chat.color("&aEnabled");
            List<String> lore = new ArrayList<>();
            lore.add(line);
            if (is.getType() == Material.LEATHER_CHESTPLATE) {
                LeatherArmorMeta lMeta = (LeatherArmorMeta) is.getItemMeta();
                lMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                lMeta.setColor(nameColor.get(f));
                lMeta.setDisplayName(chat.color(nameItemTitle.get(f)));
                lMeta.setLore(lore);
                is.setItemMeta(lMeta);
            } else {
                ItemMeta meta = is.getItemMeta();
                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                meta.setDisplayName(chat.color(nameItemTitle.get(f)));
                meta.setLore(lore);
                is.setItemMeta(meta);
            }
            if (nameFormats.contains(f)) is = Item.addGlow(is);
            row.setItem(slotNumber, is);
        }
        rows.add(row);
        rows.add(new GuiRow(null, page, 3));
        rowNumber = 4;
        row = new GuiRow(null, page, rowNumber);
        // chat formatting
        stringFormats = aPlayer.getListSetting("chatFormat");
        ArrayList<ChatFormat> chatFormats = stringFormats.stream().map(ChatFormat::valueOf).collect(Collectors.toCollection(ArrayList::new));
        ChatFormat[] chatValues = ChatFormat.values();
        slotNumber = 0;
        for (int i = 1; i <= 15; i++) {
            ChatFormat c = chatValues[i - 1];
            slotNumber++;
            if (slotNumber == 10) {
                rowNumber++;
                rows.add(row);
                row = new GuiRow(null, page, rowNumber);
                slotNumber = 1;
            }
            ItemStack is = new ItemStack(chatMaterial.get(c));
            String line = chat.color("&aClick to Enable");
            if (chatRequiredRank.get(c) > rank)
                line = chat.color("&cRequires " + Permissions.getRankName(chatRequiredRank.get(c)));
            if (chatFormats.contains(c))
                line = chat.color("&aEnabled");
            List<String> lore = new ArrayList<>();
            lore.add(line);
            if (is.getType() == Material.LEATHER_CHESTPLATE) {
                LeatherArmorMeta lMeta = (LeatherArmorMeta) is.getItemMeta();
                lMeta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                lMeta.setColor(chatColor.get(c));
                lMeta.setDisplayName(chat.color(chatItemTitle.get(c)));
                lMeta.setLore(lore);
                is.setItemMeta(lMeta);
            } else {
                ItemMeta meta = is.getItemMeta();
                meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
                meta.setDisplayName(chat.color(chatItemTitle.get(c)));
                meta.setLore(lore);
                is.setItemMeta(meta);
            }
            if (chatFormats.contains(c)) is = Item.addGlow(is);
            row.setItem(slotNumber, is);
        }
        rows.add(row);
        row = new GuiRow(null, page, 6);
        row.setItem(5, Item.easyCreate("arrow", SettingsGui.backToMainMenu));
        rows.add(row);
        page.setRows(rows.toArray(new GuiRow[0]));
        final InventoryGui gui = new InventoryGui(player, "ColorPicker", page);
        BukkitRunnable runnable = new RainbowItemRunnable(player, page);
        aPlayer.getData("colorPickerGuiRunnable").setValue(runnable);
        runnable.runTaskTimer(ServerLink.plugin, 0, 3);
        aPlayer.getData("colorPickerGui").setValue(gui);
        return gui;
    }


    @EventHandler
    private void onGuiClick(CustomGuiClickEvent e) {
        Inventory inv = e.getGui().getPages()[0].getInventory();
        if (!inv.getTitle().equals(chat.color("&3&lName and Chat Settings"))) return;
        ItemStack is = e.getItem();
        if (!is.hasItemMeta()) return;
        ItemMeta meta = is.getItemMeta();
        if (meta.getDisplayName().equals(SettingsGui.backToMainMenu)) {
            e.getPlayer().openInventory(SettingsGui.inv);
            return;
        }
        if (!meta.hasLore()) return;
        if (meta.getLore().get(0).replaceAll("§", "&").startsWith("&c")) return;
        Player player = e.getPlayer();
        AlphaPlayer aPlayer = PlayerManager.get(player);
        if (aPlayer.getRank() > 1 && meta.getDisplayName().contains("Gray")) return;
        // if a format style (bold or italic or fancy) is already enabled and it is clicked again, then it is disabled
        if (meta.getLore().get(0).replaceAll("§", "&").startsWith("&aEnabled")) {
            String ends = "e";
            if (meta.getDisplayName().contains("Chat")) ends = "t";
            if (!meta.getDisplayName().contains("Bold") && !meta.getDisplayName().contains("Italic") &&
                    !meta.getDisplayName().contains(chat.unicode("Fancy"))) return;
            List<String> lore = new ArrayList<>();
            lore.add(chat.color("&aClick to Enable"));
            meta.setLore(lore);
            is.setItemMeta(meta);
            if (meta.getDisplayName().contains("Italic")) {
                if (ends.equals("e")) {
                    List<String> list = aPlayer.getListSetting("nameFormat");
                    list.remove(NameFormat.ITALIC.name());
                    aPlayer.setSetting_SyncOnLogout("nameFormat", Settings.listToString(list));
                    aPlayer.updateNameFormatting();
                } else {
                    List<String> list = aPlayer.getListSetting("chatFormat");
                    list.remove(ChatFormat.ITALIC.name());
                    aPlayer.setSetting_SyncOnLogout("chatFormat", Settings.listToString(list));
                    chat.calculateChatString(aPlayer);
                }
            } else if (meta.getDisplayName().contains("Bold")) {
                if (ends.equals("e")) {
                    List<String> list = aPlayer.getListSetting("nameFormat");
                    list.remove(NameFormat.BOLD.name());
                    aPlayer.setSetting_SyncOnLogout("nameFormat", Settings.listToString(list));
                    aPlayer.updateNameFormatting();
                } else {
                    List<String> list = aPlayer.getListSetting("chatFormat");
                    list.remove(ChatFormat.BOLD.name());
                    aPlayer.setSetting_SyncOnLogout("chatFormat", Settings.listToString(list));
                    chat.calculateChatString(aPlayer);
                }
            } else {
                List<String> list = aPlayer.getListSetting("chatFormat");
                list.remove(ChatFormat.UNICODE.name());
                aPlayer.setSetting_SyncOnLogout("chatFormat", Settings.listToString(list));
                chat.calculateChatString(aPlayer);
            }
            e.getRow().setItem(e.getSlot(), Item.removeGlow(is));
            return;
        }

        if (meta.getDisplayName().contains(chat.unicode("Fancy"))) {
            List<String> lore = new ArrayList<>();
            lore.add(chat.color("&aEnabled"));
            meta.setLore(lore);
            is.setItemMeta(meta);
            e.getRow().setItem(e.getSlot(), Item.addGlow(is));
            List<String> list = aPlayer.getListSetting("chatFormat");
            list.add(ChatFormat.UNICODE.name());
            aPlayer.setSetting_SyncOnLogout("chatFormat", Settings.listToString(list));
            chat.calculateChatString(aPlayer);
            return;
        }

        // player is enabling one style and disabling another
        String ends = "e";
        if (meta.getDisplayName().contains("Chat")) ends = "t";

        // update the other items
        for (GuiRow scanRow : e.getPage().getRows()) {
            for (int i : scanRow.getItems().keySet()) {
                ItemStack stack = scanRow.getItem(i);
                if (stack == null || stack.getType() == Material.AIR) continue;
                if (!Item.isGlowing(stack)) continue;
                ItemMeta m = stack.getItemMeta();
                String stackDisplayName = m.getDisplayName();
                if (!m.getDisplayName().endsWith(ends)) continue;
                // you can have fancy + bold + italic + a color. The exception is rainbow name, which cannot be bolded or italicized
                if (stackDisplayName.contains(chat.unicode("Fancy"))) continue;
                if (stackDisplayName.contains("Bold")) {
                    if (ChatColor.stripColor(meta.getDisplayName()).toLowerCase().contains("rainbow")) {
                        List<String> list = aPlayer.getListSetting("nameFormat");
                        list.remove(NameFormat.BOLD.name());
                        aPlayer.setSetting_SyncOnLogout("nameFormat", Settings.listToString(list));
                        List<String> lore = new ArrayList<>();
                        lore.add(chat.color("&aClick to Enable"));
                        m.setLore(lore);
                        stack.setItemMeta(m);
                        scanRow.setItem(i, Item.removeGlow(stack));
                    }
                    continue;
                }
                if (stackDisplayName.contains("Italic")) {
                    if (ChatColor.stripColor(meta.getDisplayName()).toLowerCase().contains("rainbow")) {
                        List<String> list = aPlayer.getListSetting("nameFormat");
                        list.remove(NameFormat.ITALIC.name());
                        aPlayer.setSetting_SyncOnLogout("nameFormat", Settings.listToString(list));
                        List<String> lore = new ArrayList<>();
                        lore.add(chat.color("&aClick to Enable"));
                        m.setLore(lore);
                        stack.setItemMeta(m);
                        scanRow.setItem(i, Item.removeGlow(stack));
                    }
                    continue;
                }
                if (ends.equals("e")) {
                    NameFormat f = NameFormat.getFromString(ChatColor.stripColor(m.getDisplayName()).toLowerCase().replace(" name", "").replace(" ", "_"));
                    List<String> list = aPlayer.getListSetting("nameFormat");
                    if (meta.getDisplayName().contains("Bold") || meta.getDisplayName().contains("Italic")) {
                        if (f == NameFormat.RAINBOW) {
                            list.remove(f.name());
                            list.add(NameFormat.WHITE.name());
                            ItemStack whiteItem = e.getRow().getItem(1);
                            ItemMeta whiteItemMeta = whiteItem.getItemMeta();
                            List<String> lore = new ArrayList<>();
                            lore.clear();
                            lore.add(chat.color("&aEnabled"));
                            whiteItemMeta.setLore(lore);
                            whiteItem.setItemMeta(whiteItemMeta);
                            e.getRow().setItem(1, Item.addGlow(whiteItem));
                            aPlayer.setSetting_SyncOnLogout("nameFormat", Settings.listToString(list));
                            lore.clear();
                            lore.add(chat.color("&aClick to Enable"));
                            m.setLore(lore);
                            stack.setItemMeta(m);
                            scanRow.setItem(i, Item.removeGlow(stack));
                            break;
                        }
                        continue;
                    }
                    list.remove(f.name());
                    aPlayer.setSetting_SyncOnLogout("nameFormat", Settings.listToString(list));
                } else {
                    if (meta.getDisplayName().contains("Bold") || meta.getDisplayName().contains("Italic")) continue;
                    ChatFormat c = ChatFormat.getFromString(ChatColor.stripColor(m.getDisplayName()).toLowerCase().replace(" chat", "").replace(" ", "_"));
                    List<String> list = aPlayer.getListSetting("chatFormat");
                    list.remove(c.name());
                    aPlayer.setSetting_SyncOnLogout("chatFormat", Settings.listToString(list));
                }
                List<String> lore = new ArrayList<>();
                lore.add(chat.color("&aClick to Enable"));
                m.setLore(lore);
                stack.setItemMeta(m);
                scanRow.setItem(i, Item.removeGlow(stack));
            }
        }
        // update the clicked item
        List<String> lore = new ArrayList<>();
        lore.add(chat.color("&aEnabled"));
        meta.setLore(lore);
        is.setItemMeta(meta);
        e.getRow().setItem(e.getSlot(), Item.addGlow(is));
        if (ends.equals("e")) {
            NameFormat f = NameFormat.getFromString(ChatColor.stripColor(meta.getDisplayName()).toLowerCase().replace(" name", "").replace(" ", "_"));
            List<String> list = aPlayer.getListSetting("nameFormat");
            list.add(f.name());
            aPlayer.setSetting_SyncOnLogout("nameFormat", Settings.listToString(list));
            aPlayer.updateNameFormatting();
        } else {
            ChatFormat c = ChatFormat.getFromString(ChatColor.stripColor(meta.getDisplayName().toLowerCase()).replace(" chat", "").replace(" ", "_"));
            if (meta.getDisplayName().contains(chat.unicode("Fancy"))) c = ChatFormat.UNICODE;
            List<String> list = aPlayer.getListSetting("chatFormat");
            list.add(c.name());
            aPlayer.setSetting_SyncOnLogout("chatFormat", Settings.listToString(list));
            chat.calculateChatString(aPlayer);
        }
    }

    // cancels rainbow item runnable when a player closes inventory
    @EventHandler
    private void inventoryClose(InventoryCloseEvent e) {
        if (!e.getInventory().getTitle().equals(title)) return;
        ((BukkitRunnable) PlayerManager.get((Player) e.getPlayer()).getData("colorPickerGuiRunnable").getValue()).cancel();
    }
}
package serverlink.inventory.gui.settings;

import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.customevent.CustomGuiClickEvent;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.server.ServerType;
import serverlink.util.Item;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.HashMap;

public class PlayersShownGui implements Listener {
    private static String title = "&3&lPlayers Shown in Hubs";

    static InventoryGui createGui(Player player) {
        AlphaPlayer aPlayer = PlayerManager.get(player);
        Object o = aPlayer.getData("playersShownInHubGui").getValue();
        if (o instanceof InventoryGui) return (InventoryGui) o;
        GuiPage page = new GuiPage(title, null);
        GuiRow row = new GuiRow(null, page, 1);
        row.setItem(2, Item.easyCreate("paper", 1, chat.color("&aClick to Show &e0&a Players in Hubs")));
        row.setItem(4, Item.easyCreate("paper", 25, chat.color("&aClick to Show &e25&a Players in Hubs")));
        row.setItem(6, Item.easyCreate("paper", 50, chat.color("&aClick to Show &e50&a Players in Hubs")));
        row.setItem(8, Item.easyCreate("paper", 64, chat.color("&aClick to Show &eAll&a Players in Hubs")));
        Integer current = aPlayer.getIntegerSetting("playersShownInHub");
        if (current == null) {
            current = 300;
            aPlayer.setSetting_SyncOnLogout("playersShownInHub", ((Integer) 300).toString());
        }
        if (current == 0) {
            row.setItem(2, Item.addGlow(Item.easyCreate("paper", 1,
                    chat.color("&aCurrently Showing &e0&a Players"))));
        } else if (current == 25) {
            row.setItem(4, Item.addGlow(Item.easyCreate("paper", 25,
                    chat.color("&aCurrently Showing &e25&a Players"))));
        } else if (current == 50) {
            row.setItem(6, Item.addGlow(Item.easyCreate("paper", 50,
                    chat.color("&aCurrently Showing &e50&a Players"))));
        } else if (current == 300) {
            row.setItem(8, Item.addGlow(Item.easyCreate("paper", 64,
                    chat.color("&aCurrently Showing &eAll&a Players"))));
        }
        GuiRow row2 = new GuiRow(null, page, 2);
        row2.setItem(5, Item.easyCreate("arrow", SettingsGui.backToMainMenu));
        page.setRows(row, row2);
        InventoryGui gui = new InventoryGui(player, "PlayersShown", page);
        aPlayer.getData("playersShownInHubGui").setValue(gui);
        return gui;
    }

    @EventHandler
    private void onGuiClick(CustomGuiClickEvent e) {
        Inventory inv = e.getGui().getPages()[0].getInventory();
        if (!inv.getTitle().contains(chat.color(title))) return;
        ItemStack is = e.getItem();
        if (!is.hasItemMeta()) return;
        ItemMeta meta = is.getItemMeta();
        String displayName = meta.getDisplayName();
        if (displayName.equals(SettingsGui.backToMainMenu)) {
            e.getPlayer().openInventory(SettingsGui.inv);
            return;
        }
        if (displayName.contains("Currently")) return;
        AlphaPlayer aPlayer = PlayerManager.get(e.getPlayer());
        int players;
        try {
            players = Integer.parseInt(ChatColor.stripColor(displayName).toLowerCase().replace("click to show ", "").replace(" players in hubs", ""));
        } catch (NumberFormatException ignored) {
            players = 300;
        }
        HashMap<Integer, ItemStack> items = e.getRow().getItems();
        for (int i = 1; i <= 9; i++) {
            ItemStack stack = items.get(i);
            if (stack == null) continue;
            if (stack.getType() == Material.AIR) continue;
            ItemMeta stackMeta = stack.getItemMeta();
            // update old item
            if (Item.isGlowing(stack)) {
                stackMeta.setDisplayName(stackMeta.getDisplayName().replace("Currently Showing", "Click to Show") + " in Hubs");
                stack.setItemMeta(stackMeta);
                e.getRow().setItem(i, Item.removeGlow(stack));
                continue;
            }
            String playersString = ChatColor.stripColor(stackMeta.getDisplayName().toLowerCase())
                    .replace("click to show", "").replace("players in hubs", "").trim();
            // update new item
            if (!(players == 300 && playersString.equals("all")))
                if (!playersString.equals(((Integer) players).toString())) continue;
            stackMeta.setDisplayName(stackMeta.getDisplayName().replace("Click to Show", "Currently Showing").replace(" in Hubs", ""));
            stack.setItemMeta(stackMeta);
            e.getRow().setItem(i, Item.addGlow(stack));
        }
        aPlayer.getData("playersShownTemp").setValue(players);
        if ((Boolean) aPlayer.getData("playersShownCooldown", false).getValue()) return;
        new BukkitRunnable() {
            @Override
            public void run() {
                if (!e.getPlayer().isOnline()) return;
                Integer maxPlayers = (int) aPlayer.getData("playersShownTemp").getValue();
                aPlayer.setSetting_SyncOnLogout("playersShownInHub", maxPlayers.toString());
                if (ServerLink.getServerType() == ServerType.hub) {
                    int playersShown = 0;
                    for (AlphaPlayer mP2 : PlayerManager.getOnlineAlphaPlayers()) {
                        if (mP2 == aPlayer) continue;
                        if (playersShown >= maxPlayers) {
                            aPlayer.getPlayer().hidePlayer(mP2.getPlayer());
                            mP2.hideRankTagFrom(aPlayer.getPlayer());
                        } else {
                            aPlayer.getPlayer().showPlayer(mP2.getPlayer());
                            mP2.showRankTagTo(aPlayer.getPlayer());
                            playersShown++;
                        }
                    }
                    aPlayer.getData("playersCurrentlyShown").setValue(playersShown);
                }
                aPlayer.getData("playersShownCooldown").setValue(false);
            }
        }.runTaskLater(ServerLink.plugin, 40);
    }
}

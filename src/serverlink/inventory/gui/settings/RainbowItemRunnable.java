package serverlink.inventory.gui.settings;

import serverlink.inventory.GuiPage;
import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.scheduler.BukkitRunnable;

public class RainbowItemRunnable extends BukkitRunnable {
    private Player player;
    private int time = 10;
    private GuiPage page;
    private int id;

    public RainbowItemRunnable(Player player, GuiPage page) {
        this.player = player;
        this.page = page;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override

    public void run() {
        if (!player.isOnline()) {
            try {
                Bukkit.getScheduler().cancelTask(id);
            } catch (Exception e) {
                System.out.println("ColorPickerGui:33");
            }
            return;
        }
        Color color = Color.AQUA;
        if (time == 1) time = 10;
        else time--;
        switch (time) {
            case 10:
                color = Color.AQUA;
                break;
            case 9:
                color = Color.BLACK;
                break;
            case 8:
                color = Color.BLUE;
                break;
            case 7:
                color = Color.FUCHSIA;
                break;
            case 6:
                color = Color.GRAY;
                break;
            case 5:
                color = Color.GREEN;
                break;
            case 4:
                color = Color.NAVY;
                break;
            case 3:
                color = Color.ORANGE;
                break;
            case 2:
                color = Color.PURPLE;
                break;
            case 1:
                color = Color.TEAL;
                break;
        }
        ItemStack is = page.getRows()[0].getItem(6);
        LeatherArmorMeta meta = (LeatherArmorMeta) is.getItemMeta();
        meta.setColor(color);
        is.setItemMeta(meta);
        page.getRows()[0].setItem(6, is);
        player.updateInventory();
    }
}

package serverlink.inventory.gui.settings;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.customevent.CustomGuiClickEvent;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.util.Item;

public class ChatSettingsGui implements Listener {
    private static String title = chat.color("&3&lChat Settings");

    public static InventoryGui createGui(Player player) {
        AlphaPlayer aPlayer = PlayerManager.get(player);
        Object o = aPlayer.getData("chatSettingsGui").getValue();
        if (o instanceof InventoryGui) return (InventoryGui) o;
        GuiPage page = new GuiPage(title, null);
        GuiRow row = new GuiRow(null, page, 1);
        String enabled;

        enabled = aPlayer.getBooleanSetting("chatMessagesEnabled") ? "&aEnabled" : "&cDisabled";
        row.setItem(1, Item.easyCreate("eyeofender", 1, "&9Chat Messages: " + enabled,
                "&7Toggles public chat,",
                "&7Server messages will",
                "&7still be displayed."));

        enabled = aPlayer.getBooleanSetting("privateMessagesEnabled") ? "&aEnabled" : "&cDisabled";
        row.setItem(3, Item.easyCreate("enderpearl", 1, "&9Private Messages: " + enabled,
                "&7Allows other players to",
                "&7send you private messages."));

        enabled = aPlayer.getBooleanSetting("chatMentionsEnabled") ? "&aEnabled" : "&cDisabled";
        row.setItem(5, Item.easyCreate("noteblock", 1, "&9Chat Mention Alert: " + enabled,
                "&7Plays a sound when your",
                "&7name is mentioned in chat",
                "&7or when you receive a",
                "&7private message from someone."));

        enabled = aPlayer.getBooleanSetting("partyInvitesEnabled") ? "&aEnabled" : "&cDisabled";
        row.setItem(7, Item.easyCreate("cake", 1, "&9Party Invites: " + enabled,
                "&7Allows other players to",
                "&7send you party requests."));

        enabled = aPlayer.getBooleanSetting("friendRequestsEnabled") ? "&aEnabled" : "&cDisabled";
        row.setItem(9, Item.easyCreate("head:3", 1, "&9Friend Requests: " + enabled,
                "&7Allows other players to",
                "&7send you friend requests."));
        GuiRow row2 = new GuiRow(null, page, 2);
        row2.setItem(5, Item.easyCreate("arrow", SettingsGui.backToMainMenu));
        InventoryGui gui = new InventoryGui(player, "ChatSettings", page);
        aPlayer.getData("chatSettingsGui").setValue(gui);
        return gui;
    }

    @EventHandler
    private void onGuiClick(CustomGuiClickEvent e) {
        Inventory inv = e.getGui().getPages()[0].getInventory();
        if (!inv.getTitle().equals(title)) return;
        ItemStack is = e.getItem();
        if (!is.hasItemMeta()) return;
        ItemMeta meta = is.getItemMeta();
        String displayName = meta.getDisplayName();
        if (displayName.equals(SettingsGui.backToMainMenu)) {
            e.getPlayer().openInventory(SettingsGui.inv);
            return;
        }
        String setting = "chatMessagesEnabled";
        if (displayName.contains("Private Messages")) setting = "privateMessagesEnabled";
        else if (displayName.contains("Chat Mention")) setting = "chatMentionsEnabled";
        else if (displayName.contains("Party Invites")) setting = "partyInvitesEnabled";
        else if (displayName.contains("Friend Requests")) setting = "friendRequestsEnabled";

        AlphaPlayer aPlayer = PlayerManager.get(e.getPlayer());

        final boolean enabled = ChatColor.stripColor(displayName).endsWith(": Enabled");
        meta.setDisplayName(enabled ? displayName.replace(chat.color("&aEnabled"), chat.color("&cDisabled")) :
                displayName.replace(chat.color("&cDisabled"), chat.color("&aEnabled")));
        is.setItemMeta(meta);
        e.getRow().setItem(e.getSlot(), is);
        aPlayer.setSetting_SyncOnLogout(setting, String.valueOf(!enabled));
    }
}

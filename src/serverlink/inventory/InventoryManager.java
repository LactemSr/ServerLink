package serverlink.inventory;

import org.bukkit.ChatColor;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.inventory.gui.GameSelector;
import serverlink.inventory.gui.HubSelector;
import serverlink.player.team.Team;
import serverlink.util.Item;

import java.util.*;

public class InventoryManager {
    private static final Map<UUID, ItemStack> cachedSkulls = new HashMap<>();
    static List<InventoryGui> teamGuis = new ArrayList<>();

    public static void setup() {
        GameSelector.setup();
        HubSelector.setup();
        new BukkitRunnable() {
            @Override
            public void run() {
                cachedSkulls.clear();
            }
        }.runTaskTimer(ServerLink.plugin, 20 * 60 * 30, 20 * 60 * 30);
    }

    public static void clearInventory(Player player) {
        player.getInventory().clear();
        player.getInventory().setHelmet(null);
        player.getInventory().setChestplate(null);
        player.getInventory().setLeggings(null);
        player.getInventory().setBoots(null);
    }

    public static void leave(Player player) {
        boolean nextGui = false;
        for (InventoryGui gui : teamGuis) {
            if (nextGui) {
                nextGui = false;
                continue;
            }
            for (GuiPage page : gui.getPages()) {
                if (nextGui) break;
                for (GuiRow row : page.getRows()) {
                    if (nextGui) break;
                    for (int slot : row.getItems().keySet()) {
                        ItemStack is = row.getItem(slot);
                        if (is.getType() == Material.SKULL_ITEM) {
                            if (!is.hasItemMeta()) continue;
                            String owner = ((SkullMeta) is.getItemMeta()).getOwner();
                            if (owner == null) continue;
                            if (ChatColor.stripColor(owner).equals(player.getName()) || ChatColor.stripColor(owner).equals(player.getDisplayName())) {
                                row.setItem(slot, Item.removeGlow(is));
                                nextGui = true;
                                break;
                            }
                        }
                    }
                }
            }
        }
    }

    public static InventoryGui createListGui(Player player, String title, ItemStack fillItem, ArrayList<ItemStack> listItems) {
        if (fillItem == null) fillItem = new ItemStack(Material.AIR);
        ArrayList<GuiPage> pages = new ArrayList<>();
        int pageNumber = 1;
        int totalItemsRemaining = listItems.size();
        int currentItemIndex = 0;
        while (true) {
            boolean lastPage = totalItemsRemaining <= 45;
            GuiPage page = new GuiPage(title, fillItem);
            ArrayList<GuiRow> rows = new ArrayList<>();
            for (int rowNumber = 1; rowNumber <= 6; rowNumber++) {
                GuiRow row = new GuiRow(null, page, rowNumber);
                if ((rowNumber == 6 || totalItemsRemaining == 0) && !(pageNumber == 1 && totalItemsRemaining <= 9)) {
                    if (lastPage) {
                        if (pageNumber == 1) {
                            /*// there's only one page
                            ItemStack first = Item.easyCreate("stainedglasspane:14", "First Page");
                            row.setItem(1, first);
                            ItemStack last = Item.easyCreate("stainedglasspane:14", "Last Page");
                            row.setItem(9, last);*/
                        } else {
                            ItemStack back = Item.easyCreate("stainedglasspane", "Page " + (pageNumber - 1));
                            row.setItem(1, back);
                            ItemStack last = Item.easyCreate("stainedglasspane:14", "Last Page");
                            row.setItem(9, last);
                        }
                    } else if (pageNumber == 1) {
                        ItemStack first = Item.easyCreate("stainedglasspane:14", "First Page");
                        row.setItem(1, first);
                        ItemStack forward = Item.easyCreate("stainedglasspane", "Page " + (pageNumber + 1));
                        row.setItem(9, forward);
                    } else {
                        ItemStack back = Item.easyCreate("stainedglasspane", "Page " + (pageNumber - 1));
                        row.setItem(1, back);
                        ItemStack forward = Item.easyCreate("stainedglasspane:14", "Page " + (pageNumber + 1));
                        row.setItem(9, forward);
                    }
                    rows.add(row);
                    break;
                }
                for (int slot = 1; slot <= 9; slot++) {
                    if (totalItemsRemaining == 0) continue;
                    row.setItem(slot, listItems.get(currentItemIndex).clone());
                    totalItemsRemaining--;
                    currentItemIndex++;
                }
                rows.add(row);
                if (pageNumber == 1 && totalItemsRemaining == 0) break;
            }
            page.setRows(rows.toArray(new GuiRow[rows.size()]));
            pages.add(page);
            pageNumber++;
            if (lastPage) break;
        }
        return new InventoryGui(player, chat.color(title), (GuiPage[]) pages.toArray(new GuiPage[pages.size()]));
    }

    public static InventoryGui createTeamGui(Player player, String title, ItemStack fillItem, List<Team> teamsAlive, Map<Team, List<Player>> playersAliveOnEachTeam) {
        ArrayList<GuiPage> pages = new ArrayList<>();
        GuiPage page = new GuiPage(title, fillItem, null);
        ArrayList<GuiRow> rows = new ArrayList<>();
        GuiRow row = null;
        int rowNumber = 0;
        int slotNumber;
        // teams left excluding current one
        int teamsLeft = teamsAlive.size();
        for (Team team : teamsAlive) {
            teamsLeft--;
            rowNumber++;
            if (rowNumber != 1) rows.add(row);
            row = new GuiRow(null, page, rowNumber);
            if (rowNumber == 6) {
                // leave 6th row for back/forward buttons
                if (pages.size() == 0) {
                    // back button
                    ItemStack first = Item.easyCreate("stainedglasspane:14", "First Page");
                    row.setItem(1, first);
                } else {
                    // back button
                    ItemStack back = Item.easyCreate("stainedglasspane", "Page " + (pages.size()));
                    row.setItem(1, back);
                }
                if (teamsLeft == 0) {
                    // forward button
                    ItemStack forward = Item.easyCreate("stainedglasspane:14", "Last Page");
                    row.setItem(9, forward);
                } else {
                    // forward button
                    ItemStack forward = Item.easyCreate("stainedglasspane", "Page " + (pages.size() + 2));
                    row.setItem(9, forward);
                }
                // make new page
                rows.add(row);
                page.setRows(rows.toArray(new GuiRow[rows.size()]));
                pages.add(page);
                rows.clear();
                page = new GuiPage(title, fillItem, null);
                rowNumber = 1;
                row = new GuiRow(null, page, rowNumber);
            }
            // --Make team-colored chestplate--
            ChatColor color = ChatColor.valueOf(team.getName());
            ItemStack teamItem = new ItemStack(Material.LEATHER_CHESTPLATE);
            LeatherArmorMeta meta = (LeatherArmorMeta) teamItem.getItemMeta();
            meta.setDisplayName(color + team.getName() + " Team");
            switch (color) {
                case BLACK:
                    meta.setColor(Color.fromRGB(0, 0, 0));
                    break;
                case DARK_BLUE:
                    meta.setColor(Color.fromRGB(0, 0, 170));
                    break;
                case DARK_GREEN:
                    meta.setColor(Color.fromRGB(0, 170, 0));
                    break;
                case DARK_AQUA:
                    meta.setColor(Color.fromRGB(0, 170, 170));
                    break;
                case DARK_RED:
                    meta.setColor(Color.fromRGB(170, 0, 0));
                    break;
                case DARK_PURPLE:
                    meta.setColor(Color.fromRGB(170, 0, 170));
                    break;
                case GOLD:
                    meta.setColor(Color.fromRGB(250, 170, 0));
                    break;
                case GRAY:
                    meta.setColor(Color.fromRGB(170, 170, 170));
                    break;
                case DARK_GRAY:
                    meta.setColor(Color.fromRGB(85, 85, 85));
                    break;
                case BLUE:
                    meta.setColor(Color.fromRGB(85, 85, 255));
                    break;
                case GREEN:
                    meta.setColor(Color.fromRGB(85, 255, 85));
                    break;
                case AQUA:
                    meta.setColor(Color.fromRGB(85, 255, 255));
                    break;
                case RED:
                    meta.setColor(Color.fromRGB(255, 85, 85));
                    break;
                case LIGHT_PURPLE:
                    meta.setColor(Color.fromRGB(255, 85, 255));
                    break;
                case YELLOW:
                    meta.setColor(Color.fromRGB(255, 255, 85));
                    break;
                case WHITE:
                    meta.setColor(Color.fromRGB(255, 255, 255));
                    break;
            }
            teamItem.setItemMeta(meta);
            row.setItem(1, teamItem);

            // names left excluding current one
            int namesLeft = playersAliveOnEachTeam.get(team).size();
            slotNumber = 1;
            for (Player teamPlayer : playersAliveOnEachTeam.get(team)) {
                // --Move to then next slot/row--
                if (slotNumber == 9) {
                    slotNumber = 1;
                    rowNumber++;
                    if (rowNumber != 1) rows.add(row);
                    row = new GuiRow(null, page, rowNumber);
                } else slotNumber++;
                // leave 6th row for back/forward buttons
                if (rowNumber == 6) {
                    if (pages.size() == 0) {
                        // back button
                        ItemStack first = Item.easyCreate("stainedglasspane:14", "First Page");
                        row.setItem(1, first);
                    } else {
                        // back button
                        ItemStack back = Item.easyCreate("stainedglasspane", "Page " + (pages.size()));
                        row.setItem(1, back);
                    }
                    if (namesLeft != 1 && teamsLeft != 0) {
                        // forward button
                        ItemStack forward = Item.easyCreate("stainedglasspane", "Page " + (pages.size() + 2));
                        row.setItem(9, forward);
                    }
                    // make new page
                    rows.add(row);
                    page.setRows(rows.toArray(new GuiRow[rows.size()]));
                    pages.add(page);
                    page = new GuiPage(title, fillItem, null);
                    rowNumber = 1;
                    row = new GuiRow(null, page, rowNumber);
                    rows.clear();
                }
                // --Make head item--
                ItemStack head = createOrGetSkull(teamPlayer);
                row.setItem(slotNumber, head);
                row.setClick(slotNumber, () -> {
                    if ((teamPlayer.isOnline() || teamPlayer.hasMetadata("NPC")) && !teamPlayer.getAllowFlight()) {
                        player.teleport(teamPlayer);
                    } else {
                        player.closeInventory();
                        player.sendMessage(chat.color(chat.getServer() + teamPlayer.getDisplayName() +
                                " &cis out of the round."));
                    }
                });
                namesLeft--;
                // --Check if this is the last head to display--
                if (namesLeft == 0 && teamsLeft == 0) {
                    rows.add(row);
                    if (pages.size() > 1) {
                        rowNumber++;
                        while (rowNumber < 6) {
                            rows.add(new GuiRow(null, page, rowNumber));
                            rowNumber++;
                        }
                        row = new GuiRow(null, page, 6);
                        // back button
                        ItemStack back = Item.easyCreate("stainedglasspane", "Page " + (pages.size()));
                        row.setItem(1, back);
                        // forward button
                        ItemStack forward = Item.easyCreate("stainedglasspane:14", "Last Page");
                        row.setItem(9, forward);
                        rows.add(row);
                    }
                    page.setRows(rows.toArray(new GuiRow[rows.size()]));
                    pages.add(page);
                }
            }
        }
        InventoryGui gui = new InventoryGui(player, chat.color(title), (GuiPage[]) pages.toArray(new GuiPage[pages.size()]));
        teamGuis.add(gui);
        return gui;
    }

    /**
     * @return the player's skull with skin already loaded if this was previously called
     */
    public static ItemStack createOrGetSkull(Player player) {
        for (UUID uuid : cachedSkulls.keySet()) {
            if (uuid.equals(player.getUniqueId())) return cachedSkulls.get(uuid);
        }
        ItemStack skull = new ItemStack(Material.SKULL_ITEM, 1, (byte) SkullType.PLAYER.ordinal());
        SkullMeta skullMeta = (SkullMeta) skull.getItemMeta();
        skullMeta.setOwner(player.getName());
        skullMeta.setDisplayName(player.getDisplayName());
        skull.setItemMeta(skullMeta);
        cachedSkulls.put(player.getUniqueId(), skull);
        return skull;
    }
}

package serverlink.util;


import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketContainer;
import com.comphenix.protocol.events.PacketEvent;
import net.minecraft.server.v1_8_R3.EntityPlayer;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import net.minecraft.server.v1_8_R3.NBTTagList;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.SkullType;
import org.bukkit.craftbukkit.v1_8_R3.inventory.CraftItemStack;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;
import org.bukkit.inventory.meta.SkullMeta;
import org.bukkit.material.MaterialData;
import serverlink.ServerLink;
import serverlink.chat.chat;

import java.lang.reflect.Field;
import java.util.*;

public class Item {
    private static Field cC;

    public static void setup() {
        try {
            cC = EntityPlayer.class.getDeclaredField("containerCounter");
            cC.setAccessible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }

        Set<PacketType> packets = new HashSet<>();
        packets.add(PacketType.Play.Server.SET_SLOT);
        packets.add(PacketType.Play.Server.WINDOW_ITEMS);
        packets.add(PacketType.Play.Server.CUSTOM_PAYLOAD);

        ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(ServerLink.plugin, packets) {
            @Override
            public void onPacketSending(PacketEvent event) {
                PacketContainer packet = event.getPacket();
                PacketType type = packet.getType();
                if (type == PacketType.Play.Server.WINDOW_ITEMS) {
                    try {
                        ItemStack[] read = packet.getItemArrayModifier().read(0);
                        for (int i = 0; i < read.length; i++) {
                            read[i] = removeAttributes(read[i]);
                        }
                        packet.getItemArrayModifier().write(0, read);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (type == PacketType.Play.Server.CUSTOM_PAYLOAD) {
                    if (!packet.getStrings().read(0).equalsIgnoreCase("MC|TrList")) {
                        return;
                    }
                    /*
                    try {
                        EntityPlayer p = ((CraftPlayer) event.getPlayer()).getHandle();
                        ContainerMerchant cM = ((ContainerMerchant) p.activeContainer);
                        Field fieldMerchant = cM.getClass().getDeclaredField("merchant");
                        fieldMerchant.setAccessible(true);
                        IMerchant imerchant = (IMerchant) fieldMerchant.get(cM);

                        MerchantRecipeList merchantrecipelist = imerchant.getOffers(p);
                        @SuppressWarnings("MismatchedQueryAndUpdateOfCollection") MerchantRecipeList nlist = new MerchantRecipeList();
                        for (Object orecipe : merchantrecipelist) {
                            MerchantRecipe recipe = (MerchantRecipe) orecipe;
                            // "uses"
                            int uses = recipe.e();
                            // "maxUses"
                            int maxUses = recipe.f();
                            MerchantRecipe nrecipe = new MerchantRecipe(removeAttributes(recipe.getBuyItem1()), removeAttributes(recipe.getBuyItem2()), removeAttributes(recipe.getBuyItem3()));
                            nrecipe.a(maxUses - 7);
                            for (int i = 0; i < uses; i++) {
                                nrecipe.f();
                            }
                            nlist.add(nrecipe);
                        }

                        PacketDataSerializer packetdataserializer = new PacketDataSerializer(Unpooled.buffer());
                        packetdataserializer.writeInt(cC.getInt(p));
                        nlist.a(packetdataserializer);
                        byte[] b = packetdataserializer.array();
                        packet.getByteArrays().write(0, b);
                        packet.getIntegers().write(0, b.length);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    */
                } else {
                    try {
                        packet.getItemModifier().write(0, removeAttributes(packet.getItemModifier().read(0)));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    private static net.minecraft.server.v1_8_R3.ItemStack removeAttributes(net.minecraft.server.v1_8_R3.ItemStack i) {
        if (i == null) return null;
        if (net.minecraft.server.v1_8_R3.Item.getId(i.getItem()) == 386) return i;
        net.minecraft.server.v1_8_R3.ItemStack item = i.cloneItemStack();
        NBTTagCompound tag;
        if (!item.hasTag()) {
            tag = new NBTTagCompound();
            item.setTag(tag);
        } else {
            tag = item.getTag();
        }
        NBTTagList am = new NBTTagList();
        tag.set("AttributeModifiers", am);
        item.setTag(tag);
        return item;
    }

    private static ItemStack removeAttributes(ItemStack i) {
        if (i == null) return null;
        if (i.getType() == org.bukkit.Material.BOOK_AND_QUILL) return i;
        ItemStack item = i.clone();
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tag;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        } else {
            tag = nmsStack.getTag();
        }
        NBTTagList am = new NBTTagList();
        tag.set("AttributeModifiers", am);
        nmsStack.setTag(tag);
        return CraftItemStack.asCraftMirror(nmsStack);
    }

    /**
     * Only to be called from the main thread
     * <p>
     * Doesn't accept null values (checking for them will affect performance since this is
     * called very frequently)
     * <p>
     * Doesn't check if you're comparing an air block to another air block because that would also
     * affect performance... so don't compare air to air
     */
    public static boolean isSimilar(ItemStack item1, ItemStack item2) {
        if (item1.getType() == item2.getType()) {
            if (item1.getData().getData() == item2.getData().getData() && item1.getAmount() == item2.getAmount()) {
                if (item1.getEnchantments().toString().equals(item2.getEnchantments().toString())) {
                    if (item1.hasItemMeta() == item1.hasItemMeta()) {
                        return !item1.hasItemMeta() || getItemString(item1).equals(getItemString(item2));
                    }
                }
            }
        }
        return false;
    }

    /**
     * same as isSimilar, but doesn't check enchantments
     */
    public static boolean isMostlySimilar(ItemStack item1, ItemStack item2) {
        if (item1.getType() == item2.getType()) {
            if (item1.getData().getData() == item2.getData().getData() && item1.getAmount() == item2.getAmount()) {
                if (item1.hasItemMeta() == item1.hasItemMeta()) {
                    return !item1.hasItemMeta() || getItemString(item1).equals(getItemString(item2));
                }
            }
        }
        return false;
    }

    private static String getItemString(ItemStack is) {
        ItemMeta meta = is.getItemMeta();
        if (meta instanceof SkullMeta && ((SkullMeta) meta).getOwner() != null) {
            if (meta.hasLore())
                return meta.getDisplayName() + Arrays.toString(meta.getLore().toArray()) + ((SkullMeta) meta).getOwner().toLowerCase();
            return meta.getDisplayName() + ((SkullMeta) meta).getOwner().toLowerCase();
        }
        if (meta instanceof LeatherArmorMeta) {
            if (meta.hasLore())
                return meta.getDisplayName() + Arrays.toString(meta.getLore().toArray()) + ((LeatherArmorMeta) meta).getColor();
            return meta.getDisplayName() + ((LeatherArmorMeta) meta).getColor();
        }
        if (meta.hasLore())
            return meta.getDisplayName() + Arrays.toString(meta.getLore().toArray());
        return meta.getDisplayName() == null ? "" : meta.getDisplayName();
    }

    /**
     * Adds item enchantment glow without adding any enchantments
     */
    public static ItemStack addGlow(ItemStack item) {
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) tag = nmsStack.getTag();
        NBTTagList ench = new NBTTagList();
        tag.set("ench", ench);
        nmsStack.setTag(tag);
        item = CraftItemStack.asCraftMirror(nmsStack);
        return item;
    }

    /**
     * Does NOT remove enchantments, only the itemstack's glow attribute
     */
    public static ItemStack removeGlow(ItemStack item) {
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        NBTTagCompound tag = null;
        if (!nmsStack.hasTag()) {
            tag = new NBTTagCompound();
            nmsStack.setTag(tag);
        }
        if (tag == null) tag = nmsStack.getTag();
        tag.remove("ench");
        nmsStack.setTag(tag);
        item = CraftItemStack.asCraftMirror(nmsStack);
        return item;
    }

    public static boolean isGlowing(ItemStack item) {
        if (item == null || item.getType() == Material.AIR) return false;
        net.minecraft.server.v1_8_R3.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
        if (!nmsStack.hasTag()) return false;
        NBTTagCompound tag = nmsStack.getTag();
        return tag.get("ench") != null;
    }

    public static ItemStack create(org.bukkit.Material material, byte data, String displayName, String... lore) {
        ItemStack itemStack = new MaterialData(material, data).toItemStack(1);
        ItemMeta itemMeta = itemStack.getItemMeta();
        itemMeta.setDisplayName(chat.color(displayName));
        if (lore != null) {
            List<String> finalLore = new ArrayList<>();
            for (String s : lore)
                if (s != null)
                    finalLore.add(chat.color(s));
            itemMeta.setLore(finalLore);
        }
        itemStack.setItemMeta(itemMeta);
        return itemStack;
    }

    public static ItemStack create(org.bukkit.Material material, byte data, String displayName) {
        //noinspection NullArgumentToVariableArgMethod
        return create(material, data, chat.color(displayName), null);
    }

    /**
     * @param id can be in any valid format (name:0, name, 21:2, 21, etc.)
     */
    public static ItemStack easyCreate(String id, int amount, String displayName, String... lore) {
        MaterialData materialData = getDataFromCommonName(id);
        ItemStack is = materialData.toItemStack();
        if (is.getType() == Material.SKULL_ITEM)
            is = new ItemStack(Material.SKULL_ITEM, amount, (byte) SkullType.PLAYER.ordinal());
        is.setAmount(amount);
        ItemMeta meta = is.getItemMeta();
        if (displayName != null) meta.setDisplayName(chat.color(displayName));
        for (int i = 0; i < lore.length; i++) lore[i] = chat.color(lore[i]);
        meta.setLore(Arrays.asList(lore));
        is.setItemMeta(meta);
        return is;
    }

    /**
     * @param id can be in any valid format (name:0, name, 21:2, 21, etc.)
     */
    public static ItemStack easyCreate(String id, String displayName, String... lore) {
        return easyCreate(id, 1, displayName, lore);
    }

    /**
     * accepts any valid input - 101:0, 101, ice_block, iceblock, blockofice
     */
    public static MaterialData getDataFromCommonName(String name) {
        name = name.toLowerCase().replaceAll("_", "").replaceAll(" ", "");
        MaterialData special = ItemIds.getSpecial(name);
        if (special != null) return special;
        Integer id = getIdFromItemName(name.replaceAll(":", ""), false);
        String afterColon = "0";
        if (id != null) return new MaterialData(id);
        try {
            id = Integer.parseInt(name.split(":")[0]);
        } catch (NumberFormatException e) {
            String beforeColon = name.split(":")[0];
            id = getIdFromItemName(beforeColon, true);
        }
        if (name.contains(":")) afterColon = name.substring(name.indexOf(":") + 1, name.length());
        name = id.toString() + ":" + afterColon;
        if (name.split(":").length == 2)
            return new MaterialData(Integer.parseInt(name.split(":")[0]), (byte) Integer.parseInt(name.split(":")[1]));
        else return new MaterialData(id);
    }

    private static Integer getIdFromItemName(String name, boolean printWarningMessage) {
        for (ItemIds i : ItemIds.values()) {
            if (i.getName().equals(name)) return i.getId();
            for (String s : i.getAliases()) {
                if (s.equals(name)) return i.getId();
            }
        }
        if (printWarningMessage)
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.YELLOW + "ITEM " + name + " NOT RECOGNIZED");
        return null;
    }
}

package serverlink.util;

import org.bukkit.Material;
import org.bukkit.material.MaterialData;

enum ItemIds {
    AIR(0, "air"),
    STONE(1, "stone"),
    GRASS(2, "grassblock"),
    DIRT(3, "dirt", "dirtblock"),
    COBBLESTONE(4, "cobblestone", "cobble"),
    WOOD(5, "wood", "woodplank", "plank", "woodplanks", "planks", "woodenplank", "woodenplanks", "oakplank", "oakwoodplank"),
    SAPLING(6, "sapling", "sappling", "seedling", "oaksapling", "oaksappling"),
    BEDROCK(7, "bedrock"),
    WATER(8, "watermoving", "movingwater", "flowingwater", "waterflowing", "water1"),
    STATIONARY_WATER(9, "water", "waterstationary", "stationarywater", "stillwater"),
    LAVA(10, "lavamoving", "movinglava", "flowinglava", "lavaflowing", "lava1"),
    STATIONARY_LAVA(11, "lava", "lavastationary", "stationarylava", "stilllava"),
    SAND(12, "sand"),
    GRAVEL(13, "gravel"),
    GOLD_ORE(14, "goldore"),
    IRON_ORE(15, "ironore"),
    COAL_ORE(16, "coalore"),
    LOG(17, "log", "woodlog", "woodblock", "woodenblock", "oaklog", "oakwoodlog", "oakwood"),
    LEAVES(18, "leaves", "leaf"),
    SPONGE(19, "sponge"),
    GLASS(20, "glass"),
    LAPIS_LAZULI_ORE(21, "lapislazuliore", "lapisore"),
    LAPIS_LAZULI_BLOCK(22, "lapislazuli", "lapislazuliblock"),
    DISPENSER(23, "dispenser"),
    SANDSTONE(24, "sandstone"),
    NOTE_BLOCK(25, "musicblock", "noteblock", "note", "music", "instrument"),
    BED(26, "bed"),
    POWERED_RAIL(27, "poweredrail", "boosterrail", "poweredtrack", "boostertrack"),
    DETECTOR_RAIL(28, "detectorrail", "detector"),
    PISTON_STICKY_BASE(29, "stickypiston"),
    WEB(30, "web", "spiderweb", "cobweb"),
    DEAD_SHRUB(31, "deadshrub", "shrub"),
    DEAD_BUSH(32, "deadbush", "bush", "tumbleweed"),
    PISTON(33, "piston"),
    PISTON_HEAD(34, "pistonextension", "pistonhead"),
    CLOTH(35, "cloth", "wool"),
    PISTON_MOVING_PIECE(36, "movingpiston"),
    YELLOW_FLOWER(37, "yellowflower", "flower"),
    RED_FLOWER(38, "redflower", "redrose", "rose"),
    BROWN_MUSHROOM(39, "brownmushroom", "mushroom"),
    RED_MUSHROOM(40, "redmushroom"),
    GOLD_BLOCK(41, "gold", "goldblock"),
    IRON_BLOCK(42, "iron", "ironblock"),
    DOUBLE_STEP(43, "doubleslab", "doublestoneslab", "doublestep"),
    STEP(44, "slab", "stoneslab", "step", "halfstep"),
    BRICK(45, "brickblock", "blockofbrick"),
    TNT(46, "tnt"),
    BOOKCASE(47, "bookshelf", "bookshelves", "bookcase", "bookcases"),
    MOSSY_COBBLESTONE(48, "mossycobblestone", "mosscobble", "mossycobble"),
    OBSIDIAN(49, "obsidian"),
    TORCH(50, "torch"),
    FIRE(51, "fire"),
    MOB_SPAWNER(52, "mobspawner", "spawner"),
    WOODEN_STAIRS(53, "woodstair", "woodstairs", "woodenstair", "woodenstairs"),
    CHEST(54, "chest"),
    DIAMOND_ORE(56, "diamondore"),
    DIAMOND_BLOCK(57, "diamondblock", "blockofdiamond"),
    WORKBENCH(58, "workbench", "table", "craftingtable", "crafting"),
    CROPS(59, "crops", "crop", "wheatcrop", "wheatplant", "cropwheat"),
    SOIL(60, "soil", "farmland"),
    FURNACE(61, "furnace"),
    BURNING_FURNACE(62, "burningfurnace", "litfurnace", "furnace1"),
    LADDER(65, "ladder"),
    MINECART_TRACKS(66, "track", "tracks", "minecrattrack", "minecarttracks", "rails", "rail"),
    COBBLESTONE_STAIRS(67, "cobblestonestair", "cobblestonestairs", "cobblestair", "cobblestairs"),
    LEVER(69, "lever", "switch", "stonelever", "stoneswitch"),
    STONE_PRESSURE_PLATE(70, "stonepressureplate", "stoneplate"),
    WOODEN_PRESSURE_PLATE(72, "woodpressureplate", "woodplate", "woodenpressureplate", "woodenplate", "plate", "pressureplate"),
    REDSTONE_ORE(73, "redstoneore"),
    GLOWING_REDSTONE_ORE(74, "glowingredstoneore", "litredstoneore", "redstoneoreglowing", "redstoneorelit"),
    REDSTONE_TORCH_OFF(75, "redstonetorchoff", "offredstonetorch"),
    REDSTONE_TORCH_ON(76, "redstonetorch", "redstonetorchon", "litredstonetorch", "redstonetorchlit"),
    STONE_BUTTON(77, "stonebutton", "button"),
    SNOW(78, "snow"),
    ICE(79, "ice"),
    SNOW_BLOCK(80, "snowblock"),
    CACTUS(81, "cactus", "cacti"),
    CLAY(82, "clay"),
    JUKEBOX(84, "jukebox"),
    FENCE(85, "fence", "oakfence", "woodfence"),
    PUMPKIN(86, "pumpkin", "pumpkinblock"),
    NETHERRACK(87, "netherrack"),
    SOUL_SAND(88, "soulsand"),
    GLOWSTONE(89, "glowstone"),
    PORTAL(90, "portal", "netherportal", "portalnether"),
    JACK_O_LANTERN(91, "jackolantern"),
    CAKE(92, "cakeblock"),
    REDSTONE_REPEATER_OFF(93, "redstonerepeater", "redstonerepeateroff", "repeateroff", "repeater"),
    REDSTONE_REPEATER_ON(94, "redstonerepeateron", "repeateron"),
    LOCKED_CHEST(95, "unobtainable"),
    STAINED_GLASS(95, "stainedglass", "stainedglass:white"),
    TRAP_DOOR(96, "trapdoor"),
    MONSTER_EGGS(97, "monsteregg", "monstereggs", "stonemonstereggs"),
    STONE_BRICK(98, "stonebrick", "stonebricks"),
    RED_MUSHROOM_BLOCK(100, "giantmushroomred", "redgiantmushroom"),
    BROWN_MUSHROOM_BLOCK(99, "giantmushroombrown", "browngiantmushoom"),
    IRON_BARS(101, "ironbars", "ironfence", "bars", "bar"),
    GLASS_PANE(102, "window", "glasspane", "glasswindow"),
    MELON_BLOCK(103, "melonblock"),
    PUMPKIN_STEM(104, "pumpkinstem"),
    MELON_STEM(105, "melonstem"),
    VINE(106, "vine", "vines"),
    FENCE_GATE(107, "fencegate", "gate", "oakfencegate", "oakgate", "gateoak", "fencegateoak"),
    BRICK_STAIRS(108, "brickstairs", "brickstair", "stairsbrick", "stairbrick"),
    STONE_BRICK_STAIRS(109, "stonebrickstairs", "stonebrickstairs", "stairsstonebrick", "stairstonebrick"),
    MYCELIUM(110, "mycelium"),
    LILY_PAD(111, "lilypad"),
    NETHER_BRICK(112, "netherbrick"),
    NETHER_BRICK_FENCE(113, "netherbrickfence", "netherfence"),
    NETHER_BRICK_STAIRS(114, "netherbrickstairs", "netherbrickstair", "netherstairs", "netherstair", "stairnetherbrick", "stairsnetherbrick", "stairnether", "stairsnether"),
    NETHER_WART(115, "netherwart", "netherwarts"),
    ENCHANTMENT_TABLE(116, "enchantmenttable", "enchanttable"),
    BREWING_STAND(117, "brewingstand"),
    CAULDRON(118, "cauldron"),
    END_PORTAL(119, "endportal", "portalend"),
    END_PORTAL_FRAME(120, "endportalframe", "endframe"),
    END_STONE(121, "endstone", "enderstone"),
    DRAGON_EGG(122, "dragonegg", "enderdragonegg"),
    REDSTONE_LAMP_OFF(123, "redstonelamp", "lamp", "redstonelampoff"),
    REDSTONE_LAMP_ON(124, "redstonelampon", "litredstonelamp", "litlamp", "lampon"),
    DOUBLE_WOODEN_STEP(125, "doublewoodslab", "doublewoodenslab", "doublewoodslabs", "doublewoodenslabs"),
    WOOD_SLAB(126, "woodenslab", "woodslab"),
    COCOA_PLANT(127, "cocoplant", "cocoaplant", "growncoco", "growncocoa", "ripecoco", "ripecocoa", "cocobean1", "cocoabean1"),
    SANDSTONE_STAIRS(128, "sandstonestairs", "sandstonestair", "stairssandstone", "stairsandstone"),
    EMERALD_ORE(129, "emeraldore"),
    ENDER_CHEST(130, "enderchest"),
    TRIPWIRE_HOOK(131, "tripwirehook"),
    TRIPWIRE(132, "tripwire"),
    EMERALD_BLOCK(133, "emeraldblock"),
    SPRUCE_WOOD_STAIRS(134, "sprucestairs", "sprucewoodstairs", "sprucestair", "sprucestairs", "stairsspruce", "stairspruce"),
    BIRCH_WOOD_STAIRS(135, "birchstairs", "birchwoodstairs", "birchstair", "birchstairs", "stairsbirch", "stairbirch"),
    JUNGLE_WOOD_STAIRS(136, "junglestairs", "junglewoodstairs", "junglestair", "junglestairs", "stairsjungle", "stairjungle"),
    COMMAND_BLOCK(137, "commandblock"),
    BEACON(138, "beacon"),
    COBBLESTONE_WALL(139, "cobblestonewall", "cobblewall", "wall"),
    FLOWER_POT_BLOCK(140, "flowerpotblock", "potblock"),
    CARROTS(141, "carrotplant", "carrotcrop", "groundcarrots", "groundcarrot"),
    POTATO_PLANT(142, "potatoplant", "potatocrop", "groundpotatoes", "groundpotato"),
    WOODEN_BUTTON(143, "woodbutton", "woodenbutton", "buttonwood"),
    MOB_HEAD(143, "mobhead"),
    ANVIL(145, "anvil"),
    TRAPPED_CHEST(146, "trappedchest", "redstonechest"),
    PRESSURE_PLATE_LIGHT(147, "lightpressureplate", "goldpressureplate", "pressureplatelight", "weightedpressureplate", "lightweightedpressureplate", "weightedpressureplatelight"),
    PRESSURE_PLATE_HEAVY(148, "heavypressureplate", "ironpressureplate", "pressureplateheavy", "heavyweightedpressureplate", "weightedpressureplateheavy"),
    DAYLIGHT_SENSOR(151, "daylightsensor", "lightsensor", "daylightdetector", "lightdetector"),
    REDSTONE_BLOCK(152, "redstoneblock", "blockofredstone"),
    QUARTZ_ORE(153, "quartzore", "netherquartzore"),
    HOPPER(154, "hopper"),
    QUARTZ_BLOCK(155, "quartzblock", "quartz"),
    QUARTZ_STAIRS(156, "quartzstairs", "quartzstair", "stairsquartz", "stairquartz"),
    ACTIVATOR_RAIL(157, "activatorrail", "tntrail", "activatortrack"),
    DROPPER(158, "dropper"),
    STAINED_CLAY(159, "stainedclay", "stainedhardenedclay"),
    STAINED_GLASS_PANE(160, "stainedglasspane", "whitestainedglasspane", "stainedglasspanewhite"),
    LEAVES2(161, "leaves2", "leaf2", "acacialeaves", "acacialeaf"),
    LOG2(162, "log2", "acacia", "wood4", "log4", "acaciablock", "acacialog"),
    ACACIA_STAIRS(163, "acaciastairs", "acaciawoodstairs", "acaciastair", "acaciastairs", "stairsacacia", "stairacacia"),
    DARK_OAK_STAIRS(164, "darkoakstairs", "darkoakwoodstairs", "darkoakstair", "darkoakstairs", "stairsdarkoak", "stairdarkoak"),
    SLIME_BLOCK(165, "slimeblock", "blockofslime"),
    BARRIER(166, "barrier"),
    SEA_LANTERN(169, "sealantern"),
    HAY_BLOCK(170, "hayblock", "haybale", "wheatbale"),
    CARPET(171, "carpet"),
    HARDENED_CLAY(172, "hardenedclay", "hardclay"),
    COAL_BLOCK(173, "coalblock", "blockofcoal"),
    PACKED_ICE(174, "packedice", "hardice"),
    DOUBLE_PLANT(175, "largeflowers", "doubleflowers", "sunflower", "doubleplant", "largeflower"),
    INVERTED_DAYLIGHT_SENSER(178, "inverteddaylightsensor", "daylightsensor1"),
    RED_SANDSTONE(179, "redsandstone", "sandstonered"),
    RED_SANDSTONE_STAIRS(180, "redsandstonestairs", "redsandstonestair"),
    RED_SANDSTONE_DOUBLE_SLAB(181, "redsandstonedoubleslab"),
    RED_SANDSTONE_SLAB(182, "redsandstoneslab"),
    SPRUCE_FENCE_GATE(183, "sprucefencegate", "sprucegate", "gatespruce", "fencegatespruce"),
    BIRCH_FENCE_GATE(184, "birchfencegate", "birchgate", "gatebirch", "fencegatebirch"),
    JUNGLE_FENCE_GATE(185, "junglefencegate", "junglegate", "gatejungle", "fencegatejungle"),
    DARK_OAK_FENCE_GATE(186, "darkoakfencegate", "darkoakgate", "gatedarkoak", "fencegatedarkoak"),
    ACACIA_FENCE_GATE(187, "acaciafencegate", "acaciagate", "gateacacia", "fencegateacacia"),
    SPRUCE_FENCE(188, "sprucefence", "fencespruce"),
    BIRCH_FENCE(189, "birchfence", "fencebirch"),
    JUNGLE_FENCE(190, "junglefence", "fencejungle"),
    DARK_OAK_FENCE(191, "darkoakfence", "fencedarkoak"),
    ACACIA_FENCE(192, "acaciafence", "fenceacacia"),
    END_ROD(198, "endrod"),
    CHORUS_PLANT(199, "chorusplant", "plantchorus"),
    CHORUS_FLOWER(200, "chorusflower", "flowerchorus"),
    PURPUR_BLOCK(201, "purpurblock", ""),
    PURPUR_PILLAR(202, "purpurpillar"),
    PURPUR_STAIRS(203, "purpurstairs", "purpurstair"),
    PURPUR_DOUBLE_SLAB(204, "pururdoubleslab", "purpurdoubleslabs", "doubleslabpurpur"),
    PURPUR_SLAB(205, "purpurslab", "purpurslabs"),
    END_STONE_BRICKS(206, "endstonebricks", "endbricks", "endstonebrick", "endbrick"),
    BEETROOT_BLOCK(207, "beetrootblock", "beetrootcrop", "beetrootplant", "cropbeetroot"),
    GRASS_PATH(208, "grasspath", "yellowgrassblock"),
    END_GATEWAY(209, "endgateway", "endportal1"),
    REPEATING_COMMAND_BLOCK(210, "repeatingcommandblock"),
    CHAIN_COMMAND_BLOCK(211, "chaincommandblock"),
    FROSTED_ICE(212, "frostedice"),
    MAGMA_BLOCK(213, "magmablock", "magma"),
    NETHER_WART_BLOCK(214, "netherwartblock", "netherwartblock"),
    RED_NETHER_BRICK(215, "rednetherbrick"),
    BONE_BLOCK(216, "boneblock"),
    STRUCTURE_VOID(217, "stucturevoid"),
    OBSERVER(218, "observer", "observerblock"),
    WHITE_SKULKER_BOX(219, "whiteshulkerbox", "shulkerboxwhite", "whiteshulker", "shulkerwhite"),
    ORANGE_SKULKER_BOX(220, "orangeshulkerbox", "shulkerboxorange", "orangeshulker", "shulkerorange"),
    MAGENTA_SKULKER_BOX(221, "magentashulkerbox", "shulkerboxmagenta", "magentashulker",
            "shulkermagenta", "lightpurpleshulkerbox", "shulkerboxlightpurple", "lightpurpleshulker",
            "shulkerlightpurple"),
    LIGHT_BLUE_SKULKER_BOX(222, "lightblueshulkerbox", "shulkerboxlightblue", "lightblueshulker",
            "shulkerlightblue", "aquashulkerbox", "shulkerboxaqua", "aquashulker",
            "shulkeraqua"),
    YELLOW_SKULKER_BOX(223, "yellowshulkerbox", "shulkerboxyellow", "yellowshulker", "shulkeryellow"),
    LIME_SKULKER_BOX(224, "limeshulkerbox", "shulkerboxlime", "limeshulker", "shulkerlime",
            "lightgreenshulkerbox", "shulkerboxlightgreen", "lightgreenshulker", "shulkerlightgreen"),
    PINK_SKULKER_BOX(225, "pinkshulkerbox", "shulkerboxpink", "pinkshulker", "shulkerpink"),
    GRAY_SKULKER_BOX(226, "grayshulkerbox", "shulkerboxgray", "grayshulker", "shulkergray",
            "greyshulkerbox", "shulkerboxgrey", "greyshulker", "shulkergrey"),
    LIGHT_GRAY_SKULKER_BOX(227, "lightgrayshulkerbox", "shulkerboxlightgray", "lightgrayshulker",
            "shulkerlightgray", "lightgreyshulkerbox", "shulkerboxlightgrey", "lightgreyshulker",
            "shulkerlightgrey"),
    CYAN_SKULKER_BOX(228, "cyanshulkerbox", "shulkerboxcyan", "cyanshulker", "shulkercyan"),
    PURPLE_SKULKER_BOX(229, "purpleshulkerbox", "shulkerboxpurple", "purpleshulker", "shulkerpurple",
            "darkpurpleshulkerbox", "shulkerboxdarkpurple", "darkpurpleshulker", "shulkerdarkpurple"),
    BLUE_SKULKER_BOX(230, "blueshulkerbox", "shulkerboxblue", "blueshulker", "shulkerblue",
            "darkblueshulkerbox", "shulkerboxdarkblue", "darkblueshulker", "shulkerdarkblue"),
    BROWN_SKULKER_BOX(231, "brownshulkerbox", "shulkerboxbrown", "brownshulker", "shulkerbrown"),
    GREEN_SKULKER_BOX(232, "greenshulkerbox", "shulkerboxgreen", "greenshulker", "shulkergreen",
            "darkgreenshulkerbox", "shulkerboxdarkgreen", "darkgreenshulker", "shulkerdarkgreen"),
    RED_SKULKER_BOX(233, "redshulkerbox", "shulkerboxred", "redshulker", "shulkerred"),
    BLACK_SKULKER_BOX(234, "blackshulkerbox", "shulkerboxblack", "blackshulker", "shulkerblack"),
    STRUCTURE_BLOCK(255, "structureblock", "structure"),
    IRON_SHOVEL(256, "ironshovel"),
    IRON_PICK(257, "ironpick", "ironpickaxe"),
    IRON_AXE(258, "ironaxe"),
    FLINT_AND_STEEL(259, "lighter", "flintandsteel", "flintsteel", "flintandiron", "flintnsteel"),
    RED_APPLE(260, "redapple", "apple"),
    BOW(261, "bow"),
    ARROW(262, "arrow"),
    COAL(263, "coal"),
    DIAMOND(264, "diamond", "diamondingot"),
    IRON_BAR(265, "ironbar", "iron", "ironbullion", "ironingot"),
    GOLD_BAR(266, "goldbar", "gold", "goldbullion", "goldingot", "goldeningot"),
    IRON_SWORD(267, "ironsword"),
    WOOD_SWORD(268, "woodsword"),
    WOOD_SHOVEL(269, "woodshovel"),
    WOOD_PICKAXE(270, "woodpick", "woodpickaxe", "woodenpick", "woodenpickaxe"),
    WOOD_AXE(271, "woodaxe", "woodenaxe"),
    STONE_SWORD(272, "stonesword"),
    STONE_SHOVEL(273, "stoneshovel"),
    STONE_PICKAXE(274, "stonepick", "stonepickaxe"),
    STONE_AXE(275, "stoneaxe"),
    DIAMOND_SWORD(276, "diamondsword"),
    DIAMOND_SHOVEL(277, "diamondshovel"),
    DIAMOND_PICKAXE(278, "diamondpick", "diamondpickaxe"),
    DIAMOND_AXE(279, "diamondaxe"),
    STICK(280, "stick"),
    BOWL(281, "bowl"),
    MUSHROOM_SOUP(282, "mushroomsoup", "soup", "brbsoup"),
    GOLD_SWORD(283, "goldsword"),
    GOLD_SHOVEL(284, "goldshovel"),
    GOLD_PICKAXE(285, "goldpick", "goldpickaxe"),
    GOLD_AXE(286, "goldaxe"),
    STRING(287, "string"),
    FEATHER(288, "feather"),
    SULPHUR(289, "sulphur", "sulfur", "gunpowder"),
    WOOD_HOE(290, "woodhoe", "woodenhoe"),
    STONE_HOE(291, "stonehoe"),
    IRON_HOE(292, "ironhoe"),
    DIAMOND_HOE(293, "diamondhoe"),
    GOLD_HOE(294, "goldhoe"),
    SEEDS(295, "seeds", "seed"),
    WHEAT(296, "wheat"),
    BREAD(297, "bread"),
    LEATHER_HELMET(298, "leatherhelmet", "leatherhat", "cap", "leathercap"),
    LEATHER_CHEST(299, "leatherchest", "leatherchestplate", "leathervest", "leatherbreastplate", "leatherplate", "leathercplate", "leatherbody"),
    LEATHER_PANTS(300, "leatherpants", "leathergreaves", "leatherlegs", "leatherleggings", "leatherstockings", "leatherbreeches"),
    LEATHER_BOOTS(301, "leatherboots", "leathershoes", "leatherfoot", "leatherfeet"),
    CHAINMAIL_HELMET(302, "chainmailhelmet", "chainhelmet"),
    CHAINMAIL_CHEST(303, "chainmailchest", "chainmailchestplate", "chainchestplate", "chainchest"),
    CHAINMAIL_PANTS(304, "chainmailpants", "chainmailleggings", "chainleggings"),
    CHAINMAIL_BOOTS(305, "chainmailboots", "chainboots"),
    IRON_HELMET(306, "ironhelmet"),
    IRON_CHEST(307, "ironchest", "ironchestplate", "ironvest", "ironbreastplate", "ironplate", "ironcplate", "ironbody"),
    IRON_PANTS(308, "ironpants", "irongreaves", "ironlegs", "ironleggings", "ironstockings", "ironbreeches"),
    IRON_BOOTS(309, "ironboots", "ironshoes", "ironfoot", "ironfeet"),
    DIAMOND_HELMET(310, "diamondhelmet", "diamondhat"),
    DIAMOND_CHEST(311, "diamondchest", "diamondchestplate", "diamondvest", "diamondbreastplate", "diamondplate", "diamondcplate", "diamondbody"),
    DIAMOND_PANTS(312, "diamondpants", "diamondgreaves", "diamondlegs", "diamondleggings", "diamondstockings", "diamondbreeches"),
    DIAMOND_BOOTS(313, "diamondboots", "diamondshoes", "diamondfoot", "diamondfeet"),
    GOLD_HELMET(314, "goldhelmet"),
    GOLD_CHEST(315, "goldchest", "goldchestplate"),
    GOLD_PANTS(316, "goldpants", "goldleggings"),
    GOLD_BOOTS(317, "goldboots"),
    FLINT(318, "flint"),
    RAW_PORKCHOP(319, "rawpork", "rawporkchop", "rawmeat"),
    COOKED_PORKCHOP(320, "pork", "cookedpork", "cookedporkchop", "meat", "cookedmeat"),
    PAINTING(321, "painting"),
    GOLD_APPLE(322, "goldapple", "goldenapple"),
    SIGN(323, "sign"),
    WOODEN_DOOR_ITEM(324, "wooddoor", "door", "oakdoor", "woodendoor", "oakdooritem", "wooddooritem", "woodendooritem"),
    BUCKET(325, "bucket", "emptybucket"),
    WATER_BUCKET(326, "waterbucket"),
    LAVA_BUCKET(327, "lavabucket"),
    MINECART(328, "minecart", "cart"),
    SADDLE(329, "saddle"),
    IRON_DOOR_ITEM(330, "irondoor", "irondooritem"),
    REDSTONE_DUST(331, "redstonedust", "reddust", "redstone", "dust", "redstonewire", "wire"),
    SNOWBALL(332, "snowball"),
    WOOD_BOAT(333, "woodboat", "woodenboat", "boat"),
    LEATHER(334, "leather"),
    MILK_BUCKET(335, "milkbucket", "milk"),
    BRICK_BAR(336, "brickbar", "bricks", "brick"),
    CLAY_BALL(337, "clay"),
    SUGAR_CANE_ITEM(338, "sugarcane", "reed", "reeds", "sugarcane"),
    PAPER(339, "paper"),
    BOOK(340, "book"),
    SLIME_BALL(341, "slimeball", "slime"),
    STORAGE_MINECART(342, "storageminecart", "storagecart", "minecartwithchest", "minecartchest", "chestminecart"),
    POWERED_MINECART(343, "poweredminecart", "poweredcart", "minecartwithfurnace", "minecartfurnace", "furnaceminecart"),
    EGG(344, "egg"),
    COMPASS(345, "compass"),
    FISHING_ROD(346, "fishingrod", "fishingpole"),
    WATCH(347, "watch", "clock", "timer"),
    LIGHTSTONE_DUST(348, "lightstonedust", "glowstonedone", "brightstonedust", "brittlegolddust", "brimstonedust"),
    RAW_FISH(349, "rawfish", "fish"),
    COOKED_FISH(350, "cookedfish"),
    INK_SACK(351, "inksac", "ink", "dye", "inksack"),
    BONE(352, "bone"),
    SUGAR(353, "sugar"),
    CAKE_ITEM(354, "cake"),
    BED_ITEM(355, "bed"),
    REDSTONE_REPEATER(356, "redstonerepeater", "diode", "delayer", "repeater"),
    COOKIE(357, "cookie"),
    MAP(358, "map", "filledmap", "explorationmap"),
    SHEARS(359, "shears"),
    MELON(360, "melon", "melonslice"),
    PUMPKIN_SEEDS(361, "pumpkinseed", "pumpkinseeds"),
    MELON_SEEDS(362, "melonseed", "melonseeds"),
    RAW_BEEF(363, "rawbeef", "rawcow", "beef"),
    COOKED_BEEF(364, "steak", "cookedbeef", "cookedcow", "beefcooked"),
    RAW_CHICKEN(365, "rawchicken", "chickenraw"),
    COOKED_CHICKEN(366, "cookedchicken", "chicken", "grilledchicken", "chickencooked"),
    ROTTEN_FLESH(367, "rottenflesh", "flesh"),
    ENDER_PEARL(368, "pearl", "enderpearl"),
    BLAZE_ROD(369, "blazerod"),
    GHAST_TEAR(370, "ghasttear"),
    GOLD_NUGGET(371, "goldnugget"),
    NETHER_WART_ITEM(372, "netherwart", "netherwartseed"),
    POTION(373, "potion", "waterbottle"),
    GLASS_BOTTLE(374, "glassbottle"),
    SPIDER_EYE(375, "spidereye"),
    FERMENTED_SPIDER_EYE(376, "fermentedspidereye", "fermentedeye"),
    BLAZE_POWDER(377, "blazepowder"),
    MAGMA_CREAM(378, "magmacream"),
    BREWING_STAND_ITEM(379, "brewingstand"),
    CAULDRON_ITEM(380, "cauldron"),
    EYE_OF_ENDER(381, "eyeofender", "endereye"),
    GLISTERING_MELON(382, "glisteringmelon", "goldmelon", "glisteningmelon"),
    SPAWN_EGG(383, "spawnegg", "spawn", "mobspawnegg"),
    BOTTLE_O_ENCHANTING(384, "expbottle", "bottleoenchanting", "experiencebottle", "exppotion", "experiencepotion"),
    FIRE_CHARGE(385, "firecharge", "firestarter", "charge"),
    BOOK_AND_QUILL(386, "bookandquill", "quill", "writingbook"),
    WRITTEN_BOOK(387, "writtenbook"),
    EMERALD(388, "emeraldingot", "emerald"),
    ITEM_FRAME(389, "itemframe", "frame"),
    FLOWER_POT_ITEM(390, "flowerpot", "pot"),
    CARROT(391, "carrot"),
    POTATO(392, "potato"),
    BAKED_POTATO(393, "bakedpotato", "potatobaked"),
    POISONOUS_POTATO(394, "poisonpotato", "poisonouspotato"),
    BLANK_MAP(395, "blankmap", "emptymap"),
    GOLDEN_CARROT(396, "goldencarrot", "goldcarrot"),
    HEAD(397, "skull", "head", "headmount", "mount"),
    CARROT_ON_A_STICK(398, "carrotonastick", "carrotonstick", "stickcarrot", "carrotstick"),
    NETHER_STAR(399, "netherstar", "starnether"),
    PUMPKIN_PIE(400, "pumpkinpie"),
    FIREWORK_ROCKET(401, "firework", "rocket", "fireworkrocket", "fireworksrocket"),
    FIREWORK_STAR(402, "fireworkstar", "fireworkcharge"),
    ENCHANTED_BOOK(403, "enchantedbook"),
    COMPARATOR(404, "comparator", "redstonecomparator", "capacitor"),
    NETHER_BRICK_ITEM(405, "Nether Brick (item)", "netherbrickitem"),
    NETHER_QUARTZ(406, "netherquartz", "quartz"),
    TNT_MINECART(407, "minecraftwithtnt", "tntminecart", "minecarttnt"),
    HOPPER_MINECART(408, "minecraftwithhopper", "hopperminecart", "minecarthopper"),
    PRISMARINE_SHARD(409, "prismarineshard"),
    PRISMARINE_CRYSTALS(410, "prismarinecrystal", "prismarinecrystals"),
    RABBIT_STEW(413, "rabbitstew", "muttonstew"),
    ARMOR_STAND(416, "armorstand"),
    HORSE_ARMOR_IRON(417, "ironhorsearmor", "ironbarding"),
    HORSE_ARMOR_GOLD(418, "goldhorsearmor", "goldbarding"),
    HORSE_ARMOR_DIAMOND(419, "diamondhorsearmor", "diamondbarding"),
    LEAD(420, "lead", "leash"),
    NAME_TAG(421, "nametag"),
    COMMAND_BLOCK_MINECART(422, "commandblockminecart"),
    MUTTON(423, "mutton", "rawmutton"),
    COOKED_MUTTON(424, "cookedmutton"),
    BANNER(425, "banner"),
    SPRUCE_DOOR_ITEM(427, "sprucedoor", "sprucedooritem", "wooddoor1", "woodendoor1"),
    BIRCH_DOOR_ITEM(428, "birchdoor", "birchdooritem", "wooddoor2", "woodendoor2"),
    JUNGLE_DOOR_ITEM(429, "jungledoor", "jungledooritem", "wooddoor3", "woodendoor3"),
    ACACIA_DOOR_ITEM(430, "acaciadoor", "acaciadooritem", "wooddoor4", "woodendoor4"),
    DARK_OAK_DOOR_ITEM(431, "darkoakdoor", "darkoakdooritem", "wooddoor5", "woodendoor5"),
    CHORUS_FRUIT(432, "chorusfruit"),
    POPPED_CHORUS_FRUIT(433, "poppedchorusfruit"),
    BEETROOT(434, "beetroot"),
    BEETROOT_SEEDS(435, "beetrootseed", "beetrootseeds"),
    BEETROOT_SOUP(436, "beetrootsoup", "beetrootstew"),
    DRAGONS_BREATH(437, "dragonbreath", "dragonsbreath", "dragonbreathpotion", "dragonsbreathpotion"),
    SPLASH_POTION(438, "splashpotion"),
    SPECTRAL_ARROW(439, "spectralarrow"),
    TIPPED_ARROW(440, "tippedarrow"),
    LINGERING_POTION(441, "lingeringpotion"),
    SHIELD(442, "shield"),
    ELYTRA(443, "elytra", "wings"),
    SPRUCE_BOAT(444, "spruceboat", "boatspruce"),
    BIRCH_BOAT(445, "birchboat", "boatbirch"),
    JUNGLE_BOAT(446, "jungleboat", "boatjungle"),
    ACACIA_BOAT(447, "acaciaboat", "boatacacia"),
    DARK_OAK_BOAT(448, "darkoakboat", "boatdarkoak"),
    TOTEM_OF_UNDYING(449, "totemofundying", "undyingtotem"),
    SHULKER_SHELL(450, "shulkershell"),
    DISC_13(2256, "disc13", "musicdisc13"),
    DISC_CAT(2257, "disccat", "musicdisccat"),
    DISC_BLOCKS(2258, "discblocks", "musicdiscblocks"),
    DISC_CHIRP(2259, "discchirp", "musicdiscchirp"),
    DISC_FAR(2260, "discfar", "musicdiscfar"),
    DISC_MALL(2261, "discmall", "musicdiscmall"),
    DISC_MELLOHI(2262, "discmellohi", "musicdiscmellohi"),
    DISC_STAL(2263, "discstal", "musicdiscstal"),
    DISC_STRAD(2264, "discstrad", "musicdiscstrad"),
    DISC_WARD(2265, "discward", "musicdiscward"),
    DISC_11(2266, "disc11", "musicdisc11"),
    DISC_WAIT(2267, "discwait", "musicdisc11"),;

    private final int id;
    private final String name;
    private final String[] aliases;

    ItemIds(int id, String name, String... aliases) {
        this.id = id;
        this.name = name;
        this.aliases = aliases;
    }

    // special example: diorite is stone:3; if "diorite" is inputted, this will return stone:3
    // does not support double slabs, just Item.easyCreate doubleslab:x or doublewoodslab:x
    @SuppressWarnings("deprecation")
    public static MaterialData getSpecial(String name) {
        name = name.replaceAll(":", "");
        switch (name) {
            case "granite":
                return new MaterialData(Material.STONE, (byte) 1);
            case "polishedgranite":
            case "granitepolished":
                return new MaterialData(Material.STONE, (byte) 2);
            case "diorite":
                return new MaterialData(Material.STONE, (byte) 3);
            case "polisheddiorite":
            case "polishediorite":
            case "dioritepolished":
                return new MaterialData(Material.STONE, (byte) 4);
            case "andesite":
                return new MaterialData(Material.STONE, (byte) 5);
            case "polishedandesite":
            case "andesitepolished":
                return new MaterialData(Material.STONE, (byte) 6);
            case "coarsedirt":
            case "dirtcoarse":
                return new MaterialData(Material.DIRT, (byte) 1);
            case "podzol":
                return new MaterialData(Material.DIRT, (byte) 2);
            case "redsand":
            case "sandred":
                return new MaterialData(Material.SAND, (byte) 1);
            case "sprucewoodplank":
            case "spruceplank":
                return new MaterialData(Material.WOOD, (byte) 1);
            case "birchwoodplank":
            case "birchplank":
                return new MaterialData(Material.WOOD, (byte) 2);
            case "junglewoodplank":
            case "jungleplank":
                return new MaterialData(Material.WOOD, (byte) 3);
            case "acaciawoodplank":
            case "acaciaplank":
                return new MaterialData(Material.WOOD, (byte) 4);
            case "darkoakwoodplank":
            case "darkoakplank":
                return new MaterialData(Material.WOOD, (byte) 5);
            case "sprucesapling":
            case "sprucesappling":
                return new MaterialData(Material.SAPLING, (byte) 1);
            case "birchsapling":
            case "birchsappling":
                return new MaterialData(Material.SAPLING, (byte) 2);
            case "junglesapling":
            case "junglesappling":
                return new MaterialData(Material.SAPLING, (byte) 3);
            case "acaciasapling":
            case "acaciasappling":
                return new MaterialData(Material.SAPLING, (byte) 4);
            case "darkoaksapling":
            case "darkoaksappling":
                return new MaterialData(Material.SAPLING, (byte) 5);
            case "sprucewood":
            case "sprucelog":
            case "sprucewoodlog":
            case "spruce":
                return new MaterialData(Material.LOG, (byte) 1);
            case "birchwood":
            case "birchlog":
            case "birchwoodlog":
            case "birch":
                return new MaterialData(Material.LOG, (byte) 2);
            case "junglewood":
            case "junglelog":
            case "junglewoodlog":
            case "jungle":
                return new MaterialData(Material.LOG, (byte) 3);
            case "darkoakwood":
            case "darkoaklog":
            case "darkoakwoodlog":
            case "darkoak":
                return new MaterialData(Material.LOG_2, (byte) 1);
            case "spruceleaves":
            case "spruceleaf":
                return new MaterialData(Material.LEAVES, (byte) 1);
            case "birchleaves":
            case "birchleaf":
                return new MaterialData(Material.LEAVES, (byte) 2);
            case "jungleleaves":
            case "jungleleaf":
                return new MaterialData(Material.LEAVES, (byte) 3);
            case "darkoakleaves":
            case "darkoakleaf":
                return new MaterialData(Material.LEAVES_2, (byte) 1);
            case "wetsponge":
                return new MaterialData(Material.SPONGE, (byte) 1);
            case "chiseledsandstone":
            case "sandstonechiseled":
                return new MaterialData(Material.SANDSTONE, (byte) 1);
            case "smoothsandstone":
            case "sandstonesmooth":
                return new MaterialData(Material.SANDSTONE, (byte) 2);
            case "grass":
                return new MaterialData(Material.LONG_GRASS, (byte) 1);
            case "fern":
                return new MaterialData(Material.LONG_GRASS, (byte) 2);
            case "orangewool":
            case "woolorange":
                return new MaterialData(Material.WOOL, (byte) 1);
            case "magentawool":
            case "woolmagenta":
            case "lightpurplewool":
            case "woollightpurple":
                return new MaterialData(Material.WOOL, (byte) 2);
            case "lightbluewool":
            case "woollightblue":
                return new MaterialData(Material.WOOL, (byte) 3);
            case "yellowwool":
            case "woolyellow":
                return new MaterialData(Material.WOOL, (byte) 4);
            case "limewool":
            case "woollime":
            case "lightgreenwool":
            case "woollightgreen":
                return new MaterialData(Material.WOOL, (byte) 5);
            case "pinkwool":
            case "woolpink":
                return new MaterialData(Material.WOOL, (byte) 6);
            case "graywool":
            case "woolgray":
            case "darkgraywool":
            case "wooldarkgray":
                return new MaterialData(Material.WOOL, (byte) 7);
            case "lightgraywool":
            case "woollightgray":
                return new MaterialData(Material.WOOL, (byte) 8);
            case "cyanwool":
            case "woolcyan":
            case "aquawool":
            case "woolaqua":
                return new MaterialData(Material.WOOL, (byte) 9);
            case "purplewool":
            case "woolpurple":
            case "darkpurplewool":
            case "wooldarkpurple":
                return new MaterialData(Material.WOOL, (byte) 10);
            case "bluewool":
            case "woolblue":
            case "darkbluewool":
            case "wooldarkblue":
                return new MaterialData(Material.WOOL, (byte) 11);
            case "brownwool":
            case "woolbrown":
                return new MaterialData(Material.WOOL, (byte) 12);
            case "greenwool":
            case "woolgreen":
            case "darkgreenwool":
            case "wooldarkgreen":
                return new MaterialData(Material.WOOL, (byte) 13);
            case "redwool":
            case "woolred":
            case "lightredwool":
            case "woollightred":
                return new MaterialData(Material.WOOL, (byte) 14);
            case "blackwool":
            case "woolblack":
                return new MaterialData(Material.WOOL, (byte) 15);
            case "blueorchid":
            case "orchidblue":
            case "orchid":
            case "orchidflower":
            case "blueorchidflower":
            case "flowerorchid":
            case "flowerblueorchid":
                return new MaterialData(Material.RED_ROSE, (byte) 1);
            case "allium":
                return new MaterialData(Material.RED_ROSE, (byte) 2);
            case "azurebluet":
            case "bluetazure":
            case "azure":
            case "azureflower":
            case "azurebluetflower":
            case "flowerazure":
            case "flowerazurebluet":
                return new MaterialData(Material.RED_ROSE, (byte) 3);
            case "redtulip":
            case "tulipred":
            case "redtulipflower":
            case "flowerredtulip":
            case "tulip":
            case "tulipflower":
            case "flowertulip":
                return new MaterialData(Material.RED_ROSE, (byte) 4);
            case "orangetulip":
            case "tuliporange":
            case "orangetulipflower":
            case "flowerorangetulip":
                return new MaterialData(Material.RED_ROSE, (byte) 5);
            case "whitetulip":
            case "tulipwhite":
            case "whitetulipflower":
            case "flowerwhitetulip":
                return new MaterialData(Material.RED_ROSE, (byte) 6);
            case "pinktulip":
            case "tulippink":
            case "pinktulipflower":
            case "flowerpinktulip":
                return new MaterialData(Material.RED_ROSE, (byte) 7);
            case "oxeyedaisy":
            case "daisy":
            case "daisyflower":
            case "flowerdaisy":
            case "floweroxeyedaisy":
            case "oxeyedaisyflower":
                return new MaterialData(Material.RED_ROSE, (byte) 8);
            case "sandstoneslab":
            case "slabsandstone":
                return new MaterialData(Material.STEP, (byte) 1);
            case "cobblestoneslab":
            case "slabcobblestone":
                return new MaterialData(Material.STEP, (byte) 3);
            case "brickslab":
            case "slabbrick":
                return new MaterialData(Material.STEP, (byte) 4);
            case "stonebrickslab":
            case "slabstonebrick":
                return new MaterialData(Material.STEP, (byte) 5);
            case "netherbrickslab":
            case "slabnetherbrick":
                return new MaterialData(Material.STEP, (byte) 6);
            case "quartzslab":
            case "slabquartz":
                return new MaterialData(Material.STEP, (byte) 7);
            case "spruceslab":
            case "slabspruce":
                return new MaterialData(Material.WOOD_STEP, (byte) 1);
            case "birchslab":
            case "slabbirch":
                return new MaterialData(Material.WOOD_STEP, (byte) 2);
            case "jungleslab":
            case "slabjungle":
                return new MaterialData(Material.WOOD_STEP, (byte) 3);
            case "acaciaslab":
            case "slabacacia":
                return new MaterialData(Material.WOOD_STEP, (byte) 4);
            case "darkoakslab":
            case "slabdarkoak":
                return new MaterialData(Material.WOOD_STEP, (byte) 5);
            case "stainedglasswhite":
            case "whitestainedglass":
                return new MaterialData(Material.STAINED_GLASS);
            case "orangestainedglass":
            case "stainedglassorange":
                return new MaterialData(Material.STAINED_GLASS, (byte) 1);
            case "magentaglass":
            case "glassmagenta":
            case "lightpurpleglass":
            case "glasslightpurple":
            case "magentastainedglass":
            case "stainedglassmagenta":
            case "lightpurplestainedglass":
            case "stainedglasslightpurple":
                return new MaterialData(Material.STAINED_GLASS, (byte) 2);
            case "lightblueglass":
            case "glasslightblue":
            case "lightbluestainedglass":
            case "stainedglasslightblue":
                return new MaterialData(Material.STAINED_GLASS, (byte) 3);
            case "yellowglass":
            case "glassyellow":
            case "yellowstainedglass":
            case "stainedglassyellow":
                return new MaterialData(Material.STAINED_GLASS, (byte) 4);
            case "limeglass":
            case "limestainedglass":
            case "glasslime":
            case "stainedglasslime":
            case "lightgreenglass":
            case "lightgreenstainedglass":
            case "glasslightgreen":
            case "stainedglasslightgreen":
                return new MaterialData(Material.STAINED_GLASS, (byte) 5);
            case "pinkglass":
            case "glasspink":
            case "pinkstainedglass":
            case "stainedglasspink":
                return new MaterialData(Material.STAINED_GLASS, (byte) 6);
            case "grayglass":
            case "glassgray":
            case "darkgrayglass":
            case "glassdarkgray":
            case "graystainedglass":
            case "stainedglassgray":
            case "darkgraystainedglass":
            case "stainedglassdarkgray":
                return new MaterialData(Material.STAINED_GLASS, (byte) 7);
            case "lightgrayglass":
            case "glasslightgray":
            case "lightgraystainedglass":
            case "stainedglasslightgray":
                return new MaterialData(Material.STAINED_GLASS, (byte) 8);
            case "cyanglass":
            case "glasscyan":
            case "aquaglass":
            case "glassaqua":
            case "cyanstainedglass":
            case "stainedglasscyan":
            case "aquastainedglass":
            case "stainedglassaqua":
                return new MaterialData(Material.STAINED_GLASS, (byte) 9);
            case "purpleglass":
            case "glasspurple":
            case "darkpurpleglass":
            case "glassdarkpurple":
            case "purplestainedglass":
            case "stainedglasspurple":
            case "darkpurplestainedglass":
            case "stainedglassdarkpurple":
                return new MaterialData(Material.STAINED_GLASS, (byte) 10);
            case "blueglass":
            case "glassblue":
            case "darkblueglass":
            case "glassdarkblue":
            case "bluestainedglass":
            case "stainedglassblue":
            case "darkbluestainedglass":
            case "stainedglassdarkblue":
                return new MaterialData(Material.STAINED_GLASS, (byte) 11);
            case "brownglass":
            case "glassbrown":
            case "brownstainedglass":
            case "stainedglassbrown":
                return new MaterialData(Material.STAINED_GLASS, (byte) 12);
            case "greenglass":
            case "glassgreen":
            case "darkgreenglass":
            case "glassdarkgreen":
            case "greenstainedglass":
            case "stainedglassgreen":
            case "darkgreenstainedglass":
            case "stainedglassdarkgreen":
                return new MaterialData(Material.STAINED_GLASS, (byte) 13);
            case "redglass":
            case "glassred":
            case "lightredglass":
            case "glasslightred":
            case "redstainedglass":
            case "stainedglassred":
            case "lightredstainedglass":
            case "stainedglasslightred":
                return new MaterialData(Material.STAINED_GLASS, (byte) 14);
            case "blackglass":
            case "glassblack":
            case "blackstainedglass":
            case "stainedglass:black":
            case "stainedglassblack":
                return new MaterialData(Material.STAINED_GLASS, (byte) 15);
            case "cobblestonemonsteregg":
            case "monstereggcobblestone":
            case "cobblemonsteregg":
            case "monstereggcobble":
            case "cobblestonemonstereggs":
            case "monstereggscobblestone":
            case "cobblemonstereggs":
            case "monstereggscobble":
                return new MaterialData(Material.MONSTER_EGG, (byte) 1);
            case "stonebrickmonsteregg":
            case "monstereggstonebrick":
            case "stonebrickmonstereggs":
            case "monstereggsstonebrick":
                return new MaterialData(Material.MONSTER_EGG, (byte) 2);
            case "mossystonebrickmonsteregg":
            case "monstereggmossystonebrick":
            case "mossystonebrickmonstereggs":
            case "monstereggsmossystonebrick":
                return new MaterialData(Material.MONSTER_EGG, (byte) 3);
            case "crackedstonebrickmonsteregg":
            case "monstereggscrackedstonebrick":
            case "crackedstonebrickmonstereggs":
            case "monstereggcrackedstonebrick":
                return new MaterialData(Material.MONSTER_EGG, (byte) 4);
            case "chiseledstonebrickmonsteregg":
            case "monstereggchiseledstonebrick":
            case "chiseledstonebrickmonstereggs":
            case "monstereggschiseledstonebrick":
                return new MaterialData(Material.MONSTER_EGG, (byte) 5);
            case "mossystonebricks":
            case "mossystonebrick":
                return new MaterialData(Material.SMOOTH_BRICK, (byte) 1);
            case "crackedstonebricks":
            case "crackedstonebrick":
                return new MaterialData(Material.SMOOTH_BRICK, (byte) 2);
            case "chiseledstonebricks":
            case "chiseledstonebrick":
                return new MaterialData(Material.SMOOTH_BRICK, (byte) 3);
            case "mossycobblestonewall":
            case "mossycobblewall":
                return new MaterialData(Material.COBBLE_WALL, (byte) 1);
            case "chiseledquartzblock":
            case "quartzblockchiseled":
                return new MaterialData(Material.QUARTZ_BLOCK, (byte) 1);
            case "pillarquartzblock":
            case "quartzblockpillar":
                return new MaterialData(Material.QUARTZ_BLOCK, (byte) 2);
            case "orangeclay":
            case "clayorange":
                return new MaterialData(Material.STAINED_CLAY, (byte) 1);
            case "magentaclay":
            case "claymagenta":
            case "lightpurpleclay":
            case "claylightpurple":
                return new MaterialData(Material.STAINED_CLAY, (byte) 2);
            case "lightblueclay":
            case "claylightblue":
                return new MaterialData(Material.STAINED_CLAY, (byte) 3);
            case "yellowclay":
            case "clayyellow":
                return new MaterialData(Material.STAINED_CLAY, (byte) 4);
            case "limeclay":
            case "claylime":
            case "lightgreenclay":
            case "claylightgreen":
                return new MaterialData(Material.STAINED_CLAY, (byte) 5);
            case "pinkclay":
            case "claypink":
                return new MaterialData(Material.STAINED_CLAY, (byte) 6);
            case "grayclay":
            case "claygray":
            case "darkgrayclay":
            case "claydarkgray":
                return new MaterialData(Material.STAINED_CLAY, (byte) 7);
            case "lightgrayclay":
            case "claylightgray":
                return new MaterialData(Material.STAINED_CLAY, (byte) 8);
            case "cyanclay":
            case "claycyan":
            case "aquaclay":
            case "clayaqua":
                return new MaterialData(Material.STAINED_CLAY, (byte) 9);
            case "purpleclay":
            case "claypurple":
            case "darkpurpleclay":
            case "claydarkpurple":
                return new MaterialData(Material.STAINED_CLAY, (byte) 10);
            case "blueclay":
            case "clayblue":
            case "darkblueclay":
            case "claydarkblue":
                return new MaterialData(Material.STAINED_CLAY, (byte) 11);
            case "brownclay":
            case "claybrown":
                return new MaterialData(Material.STAINED_CLAY, (byte) 12);
            case "greenclay":
            case "claygreen":
            case "darkgreenclay":
            case "claydarkgreen":
                return new MaterialData(Material.STAINED_CLAY, (byte) 13);
            case "redclay":
            case "clayred":
            case "lightredclay":
            case "claylightred":
                return new MaterialData(Material.STAINED_CLAY, (byte) 14);
            case "blackclay":
            case "clayblack":
                return new MaterialData(Material.STAINED_CLAY, (byte) 15);
            case "orangestainedglasspane":
            case "stainedglasspaneorange":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 1);
            case "magentastainedglasspane":
            case "stainedglasspanemagenta":
            case "lightpurplestainedglasspane":
            case "stainedglasspanelightpurple":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 2);
            case "lightbluestainedglasspane":
            case "stainedglasspanelightblue":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 3);
            case "yellowstainedglasspane":
            case "stainedglasspaneyellow":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 4);
            case "limestainedglasspane":
            case "stainedglasspanelime":
            case "lightgreenstainedglasspane":
            case "stainedglasspanelightgreen":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 5);
            case "pinkstainedglasspane":
            case "stainedglasspanepink":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 6);
            case "graystainedglasspane":
            case "stainedglasspanegray":
            case "darkgraystainedglasspane":
            case "stainedglasspanedarkgray":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 7);
            case "lightgraystainedglasspane":
            case "stainedglasspanelightgray":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 8);
            case "cyanstainedglasspane":
            case "stainedglasspanecyan":
            case "aquastainedglasspane":
            case "stainedglasspaneaqua":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 9);
            case "purplestainedglasspane":
            case "stainedglasspanepurple":
            case "darkpurplestainedglasspane":
            case "stainedglasspanedarkpurple":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 10);
            case "bluestainedglasspane":
            case "stainedglasspaneblue":
            case "darkbluestainedglasspane":
            case "stainedglasspanedarkblue":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 11);
            case "brownstainedglasspane":
            case "stainedglasspanebrown":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 12);
            case "greenstainedglasspane":
            case "stainedglasspanegreen":
            case "darkgreenstainedglasspane":
            case "stainedglasspanedarkgreen":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 13);
            case "redstainedglasspane":
            case "stainedglasspanered":
            case "lightredstainedglasspane":
            case "stainedglasspanelightred":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 14);
            case "blackstainedglasspane":
            case "stainedglasspane:black":
            case "stainedglasspaneblack":
                return new MaterialData(Material.STAINED_GLASS_PANE, (byte) 15);
            case "prismarinebricks":
            case "prismarinebrick":
                return new MaterialData(Material.PRISMARINE, (byte) 1);
            case "darkprismarine":
            case "darkprismarineblock":
            case "prismarinedark":
                return new MaterialData(Material.PRISMARINE, (byte) 2);
            case "orangecarpet":
            case "carpetorange":
                return new MaterialData(Material.CARPET, (byte) 1);
            case "magentacarpet":
            case "carpetmagenta":
            case "lightpurplecarpet":
            case "carpetlightpurple":
                return new MaterialData(Material.CARPET, (byte) 2);
            case "lightbluecarpet":
            case "carpetlightblue":
                return new MaterialData(Material.CARPET, (byte) 3);
            case "yellowcarpet":
            case "carpetyellow":
                return new MaterialData(Material.CARPET, (byte) 4);
            case "limecarpet":
            case "carpetlime":
            case "lightgreencarpet":
            case "carpetlightgreen":
                return new MaterialData(Material.CARPET, (byte) 5);
            case "pinkcarpet":
            case "carpetpink":
                return new MaterialData(Material.CARPET, (byte) 6);
            case "graycarpet":
            case "carpetgray":
            case "darkgraycarpet":
            case "carpetdarkgray":
                return new MaterialData(Material.CARPET, (byte) 7);
            case "lightgraycarpet":
            case "carpetlightgray":
                return new MaterialData(Material.CARPET, (byte) 8);
            case "cyancarpet":
            case "carpetcyan":
            case "aquacarpet":
            case "carpetaqua":
                return new MaterialData(Material.CARPET, (byte) 9);
            case "purplecarpet":
            case "carpetpurple":
            case "darkpurplecarpet":
            case "carpetdarkpurple":
                return new MaterialData(Material.CARPET, (byte) 10);
            case "bluecarpet":
            case "carpetblue":
            case "darkbluecarpet":
            case "carpetdarkblue":
                return new MaterialData(Material.CARPET, (byte) 11);
            case "browncarpet":
            case "carpetbrown":
                return new MaterialData(Material.CARPET, (byte) 12);
            case "greencarpet":
            case "carpetgreen":
            case "darkgreencarpet":
            case "carpetdarkgreen":
                return new MaterialData(Material.CARPET, (byte) 13);
            case "redcarpet":
            case "carpetred":
            case "lightredcarpet":
            case "carpetlightred":
                return new MaterialData(Material.CARPET, (byte) 14);
            case "blackcarpet":
            case "carpetblack":
                return new MaterialData(Material.CARPET, (byte) 15);
            case "lilac":
            case "lilacflower":
            case "flowerlilac":
                return new MaterialData(Material.DOUBLE_PLANT, (byte) 1);
            case "tallgrass":
            case "grasstall":
                return new MaterialData(Material.DOUBLE_PLANT, (byte) 2);
            case "largefern":
            case "fernlarge":
            case "tallfern":
            case "ferntall":
                return new MaterialData(Material.DOUBLE_PLANT, (byte) 3);
            case "rosebush":
                return new MaterialData(Material.DOUBLE_PLANT, (byte) 4);
            case "peony":
            case "peonybush":
            case "peonyflower":
                return new MaterialData(Material.DOUBLE_PLANT, (byte) 5);
            case "chiseledredsandstone":
            case "redstandstonechiseled":
                return new MaterialData(Material.RED_SANDSTONE, (byte) 1);
            case "smoothredsandstone":
            case "redsandstonesmooth":
                return new MaterialData(Material.RED_SANDSTONE, (byte) 2);
            case "charcoal":
                return new MaterialData(Material.COAL, (byte) 1);
            case "enchantedgoldenapple":
            case "godapple":
            case "gapple":
            case "goldenappleenchanted":
                return new MaterialData(Material.GOLDEN_APPLE, (byte) 1);
            case "rawsalmon":
            case "salmonraw":
                return new MaterialData(Material.RAW_FISH, (byte) 1);
            case "clownfish":
                return new MaterialData(Material.RAW_FISH, (byte) 2);
            case "pufferfish":
                return new MaterialData(Material.RAW_FISH, (byte) 3);
            case "cookedsalmon":
            case "salmoncooked":
                return new MaterialData(Material.COOKED_FISH, (byte) 1);
            case "reddye":
            case "dyered":
            case "lightreddye":
            case "dyelightred":
            case "rosered":
                return new MaterialData(Material.INK_SACK, (byte) 1);
            case "greendye":
            case "dyegreen":
            case "darkgreendye":
            case "dyedarkgreen":
                return new MaterialData(Material.INK_SACK, (byte) 2);
            case "browndye":
            case "dyebrown":
            case "cocobeans":
            case "cocobean":
            case "cocoabeans":
            case "cocoabean":
            case "cacaobeans":
            case "cacaobean":
                return new MaterialData(Material.INK_SACK, (byte) 3);
            case "bluedye":
            case "dyeblue":
            case "darkbluedye":
            case "dyedarkblue":
            case "lapislazuli":
            case "lapis":
                return new MaterialData(Material.INK_SACK, (byte) 4);
            case "purpledye":
            case "dyepurple":
            case "darkpurpledye":
            case "dyedarkpurple":
                return new MaterialData(Material.INK_SACK, (byte) 5);
            case "cyandye":
            case "dyecyan":
            case "aquadye":
            case "dyeaqua":
                return new MaterialData(Material.INK_SACK, (byte) 6);
            case "lightgraydye":
            case "dyelightgray":
                return new MaterialData(Material.INK_SACK, (byte) 7);
            case "graydye":
            case "dyegray":
            case "darkgraydye":
            case "dyedarkgray":
                return new MaterialData(Material.INK_SACK, (byte) 8);
            case "pinkdye":
            case "dyepink":
                return new MaterialData(Material.INK_SACK, (byte) 9);
            case "limedye":
            case "dyelime":
            case "lightgreendye":
            case "dyelightgreen":
                return new MaterialData(Material.INK_SACK, (byte) 10);
            case "yellowdye":
            case "dyeyellow":
            case "dandelionyellow":
                return new MaterialData(Material.INK_SACK, (byte) 11);
            case "lightbluedye":
            case "dyelightblue":
                return new MaterialData(Material.INK_SACK, (byte) 12);
            case "magentadye":
            case "dyemagenta":
            case "lightpurpledye":
            case "dyelightpurple":
                return new MaterialData(Material.INK_SACK, (byte) 13);
            case "orangedye":
            case "dyeorange":
                return new MaterialData(Material.INK_SACK, (byte) 14);
            case "whitedye":
            case "dyewhite":
            case "bonemeal":
                return new MaterialData(Material.INK_SACK, (byte) 15);
            case "spawnelderguardian":
            case "spawnelderguardianegg":
            case "elderguardianegg":
            case "eggelderguardian":
            case "elderguardianmonsteregg":
            case "monstereggelderguardian":
            case "elderguardianspawnmonsteregg":
            case "spawnmonstereggelderguardian":
                return new MaterialData(Material.MONSTER_EGG, (byte) 4);
            case "spawnwitherskeleton":
            case "spawnwitherskeletonegg":
            case "witherskeletonegg":
            case "eggwitherskeleton":
            case "witherskeletonmonsteregg":
            case "monstereggwitherskeleton":
            case "witherskeletonspawnmonsteregg":
            case "spawnmonstereggwitherskeleton":
                return new MaterialData(Material.MONSTER_EGG, (byte) 5);
            case "spawnstray":
            case "spawnstrayegg":
            case "strayegg":
            case "eggstray":
            case "straymonsteregg":
            case "monstereggstray":
            case "strayspawnmonsteregg":
            case "spawnmonstereggstray":
                return new MaterialData(Material.MONSTER_EGG, (byte) 6);
            case "spawnhusk":
            case "spawnhuskegg":
            case "huskegg":
            case "egghusk":
            case "huskmonsteregg":
            case "monsteregghusk":
            case "huskspawnmonsteregg":
            case "spawnmonsteregghusk":
                return new MaterialData(Material.MONSTER_EGG, (byte) 23);
            case "spawnzombievillager":
            case "spawnzombievillageregg":
            case "zombievillageregg":
            case "eggzombievillager":
            case "zombievillagermonsteregg":
            case "monstereggzombievillager":
            case "zombievillagerspawnmonsteregg":
            case "spawnmonstereggzombievillager":
                return new MaterialData(Material.MONSTER_EGG, (byte) 27);
            case "spawnskeletonhorse":
            case "spawnskeletonhorseegg":
            case "skeletonhorseegg":
            case "eggskeletonhorse":
            case "skeletonhorsemonsteregg":
            case "monstereggskeletonhorse":
            case "skeletonhorsespawnmonsteregg":
            case "spawnmonstereggskeletonhorse":
                return new MaterialData(Material.MONSTER_EGG, (byte) 28);
            case "spawnzombiehorse":
            case "spawnzombiehorseegg":
            case "zombiehorseegg":
            case "eggzombiehorse":
            case "zombiehorsemonsteregg":
            case "monstereggzombiehorse":
            case "zombiehorsespawnmonsteregg":
            case "spawnmonstereggzombiehorse":
                return new MaterialData(Material.MONSTER_EGG, (byte) 29);
            case "spawndonkey":
            case "spawndonkeyegg":
            case "donkeyegg":
            case "eggdonkey":
            case "donkeymonsteregg":
            case "monstereggdonkey":
            case "donkeyspawnmonsteregg":
            case "spawnmonstereggdonkey":
                return new MaterialData(Material.MONSTER_EGG, (byte) 31);
            case "spawnmule":
            case "spawnmuleegg":
            case "muleegg":
            case "eggmule":
            case "mulemonsteregg":
            case "monstereggmule":
            case "mulespawnmonsteregg":
            case "spawnmonstereggmule":
                return new MaterialData(Material.MONSTER_EGG, (byte) 32);
            case "spawnevoker":
            case "spawnevokeregg":
            case "evokeregg":
            case "eggevoker":
            case "evokermonsteregg":
            case "monstereggevoker":
            case "evokerspawnmonsteregg":
            case "spawnmonstereggevoker":
                return new MaterialData(Material.MONSTER_EGG, (byte) 34);
            case "spawnvex":
            case "spawnvexegg":
            case "vexegg":
            case "eggvex":
            case "vexmonsteregg":
            case "monstereggvex":
            case "vexspawnmonsteregg":
            case "spawnmonstereggvex":
                return new MaterialData(Material.MONSTER_EGG, (byte) 35);
            case "spawnvindicator":
            case "spawnvindicatoregg":
            case "vindicatoregg":
            case "eggvindicator":
            case "vindicatormonsteregg":
            case "monstereggvindicator":
            case "vindicatorspawnmonsteregg":
            case "spawnmonstereggvindicator":
                return new MaterialData(Material.MONSTER_EGG, (byte) 36);
            case "spawncreeper":
            case "spawncreeperegg":
            case "creeperegg":
            case "eggcreeper":
            case "creepermonsteregg":
            case "monstereggcreeper":
            case "creeperspawnmonsteregg":
            case "spawnmonstereggcreeper":
                return new MaterialData(Material.MONSTER_EGG, (byte) 50);
            case "spawnskeleton":
            case "spawnskeletonegg":
            case "skeletonegg":
            case "eggskeleton":
            case "skeletonmonsteregg":
            case "monstereggskeleton":
            case "skeletonspawnmonsteregg":
            case "spawnmonstereggskeleton":
                return new MaterialData(Material.MONSTER_EGG, (byte) 51);
            case "spawnspider":
            case "spawnspideregg":
            case "spideregg":
            case "eggspider":
            case "spidermonsteregg":
            case "monstereggspider":
            case "spiderspawnmonsteregg":
            case "spawnmonstereggspider":
                return new MaterialData(Material.MONSTER_EGG, (byte) 52);
            case "spawnzombie":
            case "spawnzombieegg":
            case "zombieegg":
            case "eggzombie":
            case "zombiemonsteregg":
            case "monstereggzombie":
            case "zombiespawnmonsteregg":
            case "spawnmonstereggzombie":
                return new MaterialData(Material.MONSTER_EGG, (byte) 54);
            case "spawnslime":
            case "spawnslimeegg":
            case "slimeegg":
            case "eggslime":
            case "slimemonsteregg":
            case "monstereggslime":
            case "slimespawnmonsteregg":
            case "spawnmonstereggslime":
                return new MaterialData(Material.MONSTER_EGG, (byte) 55);
            case "spawnghast":
            case "spawnghastegg":
            case "ghastegg":
            case "eggghast":
            case "ghastmonsteregg":
            case "monstereggghast":
            case "ghastspawnmonsteregg":
            case "spawnmonstereggghast":
                return new MaterialData(Material.MONSTER_EGG, (byte) 56);
            case "spawnpigman":
            case "spawnpigmanegg":
            case "pigmanegg":
            case "eggpigman":
            case "pigmanmonsteregg":
            case "monstereggpigman":
            case "pigmanspawnmonsteregg":
            case "spawnmonstereggpigman":
            case "spawnzombiepigman":
            case "spawnzombiepigmanegg":
            case "zombiepigmanegg":
            case "eggzombiepigman":
            case "zombiepigmanmonsteregg":
            case "monstereggzombiepigman":
            case "zombiepigmanspawnmonsteregg":
            case "spawnmonstereggzombiepigman":
                return new MaterialData(Material.MONSTER_EGG, (byte) 57);
            case "spawnenderman":
            case "spawnendermanegg":
            case "endermanegg":
            case "eggenderman":
            case "endermanmonsteregg":
            case "monstereggenderman":
            case "endermanspawnmonsteregg":
            case "spawnmonstereggenderman":
                return new MaterialData(Material.MONSTER_EGG, (byte) 58);
            case "spawncavespider":
            case "spawncavespideregg":
            case "cavespideregg":
            case "eggcavespider":
            case "cavespidermonsteregg":
            case "monstereggcavespider":
            case "cavespiderspawnmonsteregg":
            case "spawnmonstereggcavespider":
                return new MaterialData(Material.MONSTER_EGG, (byte) 59);
            case "spawnsilverfish":
            case "spawnsilverfishegg":
            case "silverfishegg":
            case "eggsilverfish":
            case "silverfishmonsteregg":
            case "monstereggsilverfish":
            case "silverfishspawnmonsteregg":
            case "spawnmonstereggsilverfish":
                return new MaterialData(Material.MONSTER_EGG, (byte) 60);
            case "spawnblaze":
            case "spawnblazeegg":
            case "blazeegg":
            case "eggblaze":
            case "blazemonsteregg":
            case "monstereggblaze":
            case "blazespawnmonsteregg":
            case "spawnmonstereggblaze":
                return new MaterialData(Material.MONSTER_EGG, (byte) 61);
            case "spawnmagmacube":
            case "spawnmagmacubeegg":
            case "magmacubeegg":
            case "eggmagmacube":
            case "magmacubemonsteregg":
            case "monstereggmagmacube":
            case "magmacubespawnmonsteregg":
            case "spawnmonstereggmagmacube":
                return new MaterialData(Material.MONSTER_EGG, (byte) 62);
            case "spawnbat":
            case "spawnbategg":
            case "bategg":
            case "eggbat":
            case "batmonsteregg":
            case "monstereggbat":
            case "batspawnmonsteregg":
            case "spawnmonstereggbat":
                return new MaterialData(Material.MONSTER_EGG, (byte) 65);
            case "spawnwitch":
            case "spawnwitchegg":
            case "witchegg":
            case "eggwitch":
            case "witchmonsteregg":
            case "monstereggwitch":
            case "witchspawnmonsteregg":
            case "spawnmonstereggwitch":
                return new MaterialData(Material.MONSTER_EGG, (byte) 66);
            case "spawnendermite":
            case "spawnendermiteegg":
            case "endermiteegg":
            case "eggendermite":
            case "endermitemonsteregg":
            case "monstereggendermite":
            case "endermitespawnmonsteregg":
            case "spawnmonstereggendermite":
                return new MaterialData(Material.MONSTER_EGG, (byte) 67);
            case "spawnguardian":
            case "spawnguardianegg":
            case "guardianegg":
            case "eggguardian":
            case "guardianmonsteregg":
            case "monstereggguardian":
            case "guardianspawnmonsteregg":
            case "spawnmonstereggguardian":
                return new MaterialData(Material.MONSTER_EGG, (byte) 68);
            case "spawnshulker":
            case "spawnshulkeregg":
            case "shulkeregg":
            case "eggshulker":
            case "shulkermonsteregg":
            case "monstereggshulker":
            case "shulkerspawnmonsteregg":
            case "spawnmonstereggshulker":
                return new MaterialData(Material.MONSTER_EGG, (byte) 69);
            case "spawnpig":
            case "spawnpigegg":
            case "pigegg":
            case "eggpig":
            case "pigmonsteregg":
            case "monstereggpig":
            case "pigspawnmonsteregg":
            case "spawnmonstereggpig":
                return new MaterialData(Material.MONSTER_EGG, (byte) 90);
            case "spawnsheep":
            case "spawnsheepegg":
            case "sheepegg":
            case "eggsheep":
            case "sheepmonsteregg":
            case "monstereggsheep":
            case "sheepspawnmonsteregg":
            case "spawnmonstereggsheep":
                return new MaterialData(Material.MONSTER_EGG, (byte) 91);
            case "spawncow":
            case "spawncowegg":
            case "cowegg":
            case "eggcow":
            case "cowmonsteregg":
            case "monstereggcow":
            case "cowspawnmonsteregg":
            case "spawnmonstereggcow":
                return new MaterialData(Material.MONSTER_EGG, (byte) 92);
            case "spawnchicken":
            case "spawnchickenegg":
            case "chickenegg":
            case "eggchicken":
            case "chickenmonsteregg":
            case "monstereggchicken":
            case "chickenspawnmonsteregg":
            case "spawnmonstereggchicken":
                return new MaterialData(Material.MONSTER_EGG, (byte) 93);
            case "spawnsquid":
            case "spawnsquidegg":
            case "squidegg":
            case "eggsquid":
            case "squidmonsteregg":
            case "monstereggsquid":
            case "squidspawnmonsteregg":
            case "spawnmonstereggsquid":
                return new MaterialData(Material.MONSTER_EGG, (byte) 94);
            case "spawnwolf":
            case "spawnwolfegg":
            case "wolfegg":
            case "eggwolf":
            case "wolfmonsteregg":
            case "monstereggwolf":
            case "wolfspawnmonsteregg":
            case "spawnmonstereggwolf":
            case "spawndog":
            case "spawndogegg":
            case "dogegg":
            case "eggdog":
            case "dogmonsteregg":
            case "monstereggdog":
            case "dogspawnmonsteregg":
            case "spawnmonstereggdog":
                return new MaterialData(Material.MONSTER_EGG, (byte) 95);
            case "spawnmooshroom":
            case "spawnmooshroomegg":
            case "mooshroomegg":
            case "eggmooshroom":
            case "mooshroommonsteregg":
            case "monstereggmooshroom":
            case "mooshroomspawnmonsteregg":
            case "spawnmonstereggmooshroom":
            case "spawnmushroomcow":
            case "spawnmushroomcowegg":
            case "mushroomcowegg":
            case "eggmushroomcow":
            case "mushroomcowmonsteregg":
            case "monstereggmushroomcow":
            case "mushroomcowspawnmonsteregg":
            case "spawnmonstereggmushroomcow":
                return new MaterialData(Material.MONSTER_EGG, (byte) 96);
            case "spawnocelot":
            case "spawnocelotegg":
            case "ocelotegg":
            case "eggocelot":
            case "ocelotmonsteregg":
            case "monstereggocelot":
            case "ocelotspawnmonsteregg":
            case "spawnmonstereggocelot":
                return new MaterialData(Material.MONSTER_EGG, (byte) 98);
            case "spawnhorse":
            case "spawnhorseegg":
            case "horseegg":
            case "egghorse":
            case "horsemonsteregg":
            case "monsteregghorse":
            case "horsespawnmonsteregg":
            case "spawnmonsteregghorse":
                return new MaterialData(Material.MONSTER_EGG, (byte) 100);
            case "spawnrabbit":
            case "spawnrabbitegg":
            case "rabbitegg":
            case "eggrabbit":
            case "rabbitmonsteregg":
            case "monstereggrabbit":
            case "rabbitspawnmonsteregg":
            case "spawnmonstereggrabbit":
                return new MaterialData(Material.MONSTER_EGG, (byte) 101);
            case "spawnpolarbear":
            case "spawnpolarbearegg":
            case "polarbearegg":
            case "eggpolarbear":
            case "polarbearmonsteregg":
            case "monstereggpolarbear":
            case "polarbearspawnmonsteregg":
            case "spawnmonstereggpolarbear":
                return new MaterialData(Material.MONSTER_EGG, (byte) 102);
            case "spawnllama":
            case "spawnllamaegg":
            case "llamaegg":
            case "eggllama":
            case "llamamonsteregg":
            case "monstereggllama":
            case "llamaspawnmonsteregg":
            case "spawnmonstereggllama":
                return new MaterialData(Material.MONSTER_EGG, (byte) 103);
            case "spawnvillager":
            case "spawnvillageregg":
            case "villageregg":
            case "eggvillager":
            case "villagermonsteregg":
            case "monstereggvillager":
            case "villagerspawnmonsteregg":
            case "spawnmonstereggvillager":
                return new MaterialData(Material.MONSTER_EGG, (byte) 120);
            case "mobheadwither":
            case "mobheadwitherskeleton":
            case "skullwither":
            case "skullwitherskeleton":
            case "witherskull":
            case "witherskeletonskull":
            case "witherhead":
            case "witherskeletonhead":
                return new MaterialData(Material.SKULL, (byte) 1);
            case "mobheadzombie":
            case "skullzombie":
            case "zombieskull":
            case "zombiehead":
                return new MaterialData(Material.SKULL, (byte) 2);
            case "mobheadhuman":
            case "skullhuman":
            case "humanskull":
            case "humanhead":
            case "mobheadplayer":
            case "skullplayer":
            case "playerskull":
            case "playerhead":
            case "mobheadsteve":
            case "skullsteve":
            case "steveskull":
            case "stevehead":
            case "skull3":
            case "head3":
            case "mobhead3":
            case "skullitem":
            case "headitem":
            case "skullitem3":
            case "headitem3":
                return new MaterialData(Material.SKULL_ITEM);
            case "mobheadcreeper":
            case "skullcreeper":
            case "creeperskull":
            case "creeperhead":
                return new MaterialData(Material.SKULL, (byte) 4);
            case "mobheaddragon":
            case "skulldragon":
            case "dragonskull":
            case "dragonhead":
            case "mobheadenderdragon":
            case "skullenderdragon":
            case "enderdragonskull":
            case "enderdragonhead":
                return new MaterialData(Material.SKULL, (byte) 5);
        }
        return null;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String[] getAliases() {
        return aliases;
    }

}

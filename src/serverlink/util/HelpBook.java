package serverlink.util;

import mkremins.fanciful.FancyMessage;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HelpBook {
    private String name;
    private int pageCount;
    private List<String> pageText;

    public HelpBook(String name, List<String> pageText) {
        this.name = name;
        this.pageText = pageText;
        pageCount = pageText.size();
    }

    public String getName() {
        return name;
    }

    public int getPageCount() {
        return pageCount;
    }

    // pages start at 1, not 0
    public String getPageText(int page) {
        return pageText.get(page - 1);
    }

    public static class Builder {
        private String name;
        private List<FancyMessage> fancyMessages = new ArrayList<>();

        public Builder name(String name) {
            this.name = name;
            return this;
        }

        public Builder addPage(FancyMessage fancyMessage) {
            fancyMessages.add(fancyMessage == null ? new FancyMessage("") : fancyMessage);
            return this;
        }

        public HelpBook build() {
            List<String> pageText = fancyMessages.stream().map(FancyMessage::toJSONString).collect(Collectors.toList());
            return new HelpBook(name, pageText);
        }
    }
}

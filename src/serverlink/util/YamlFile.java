package serverlink.util;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

import java.io.*;

public class YamlFile {
    private Plugin plugin;

    private String fileName;
    private FileConfiguration file;
    private File fileFile;

    public YamlFile(Plugin plugin, String fileName) {
        this(plugin, fileName, fileName, plugin.getDataFolder());
    }

    public YamlFile(Plugin plugin, String fileName, String defaultFile, File parent) {
        this(plugin, fileName, defaultFile, parent, true);
    }

    public YamlFile(Plugin plugin, String fileName, File parent) {
        this(plugin, fileName, fileName, parent, true);
    }

    public YamlFile(Plugin plugin, String fileName, String defaultFile, File parent, boolean copy) {
        this.plugin = plugin;
        this.fileName = fileName;

        if (!parent.exists())
            parent.mkdir();

        fileFile = new File(parent, fileName);
        if (copy) {
            if (!fileFile.exists()) {
                copyFileFromJar(plugin, fileName, defaultFile, parent);
            }
        } else {
            fileFile.getParentFile().mkdirs();
        }

        file = YamlConfiguration.loadConfiguration(fileFile);
    }

    private static void copyFileFromJar(Plugin plugin, String writeTo, String copyFrom, File parent) {
        File file = new File(parent + File.separator + writeTo);
        if (!file.exists())
            file.getParentFile().mkdirs();
        InputStream fis;
        fis = plugin.getResource(copyFrom);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(file);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            byte[] buf = new byte[1024];
            int i = 0;
            while ((i = fis.read(buf)) != -1) {
                fos.write(buf, 0, i);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (fis != null) {
                try {
                    fis.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public String getFileName() {
        return fileName;
    }

    public FileConfiguration getFile() {
        return file;
    }

    public File getFileFile() {
        return fileFile;
    }

    public void reloadFile() {
        if (!fileFile.exists())
            plugin.saveResource(fileName, true);

        file = YamlConfiguration.loadConfiguration(fileFile);
    }

    public void saveFile() {
        try {
            file.save(fileFile);
        } catch (Exception e) {
            plugin.getLogger().severe(String.format("Couldn't save '%s', because: '%s'", fileName, e.getMessage()));
        }

        reloadFile();
    }

    public static class Builder {
        private Plugin plugin;
        private String fileName;
        private String defaultFile;
        private File parent;
        private boolean copy;

        public Builder plugin(Plugin plugin) {
            this.plugin = plugin;
            return this;
        }

        public Builder fileName(String fileName) {
            this.fileName = fileName;
            return this;
        }

        public Builder defaultFile(String defaultFile) {
            this.defaultFile = defaultFile;
            return this;
        }

        public Builder parent(File parent) {
            this.parent = parent;
            return this;
        }

        public Builder copy(boolean copy) {
            this.copy = copy;
            return this;
        }

        public YamlFile build() {
            return new YamlFile(plugin, fileName, defaultFile, parent, copy);
        }
    }
}

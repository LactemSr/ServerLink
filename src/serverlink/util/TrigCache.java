package serverlink.util;

import java.util.HashMap;

public class TrigCache {
    private static final HashMap<Integer, Double> sin = new HashMap<>();
    private static final HashMap<Integer, Double> cos = new HashMap<>();
    private static final HashMap<Integer, Double> tan = new HashMap<>();

    public static void setup() {
        for (int i = -360; i <= 360; i++) {
            sin.put(i, Math.sin(Math.toRadians(i)));
            cos.put(i, Math.cos(Math.toRadians(i)));
            tan.put(i, Math.tan(Math.toRadians(i)));
        }
    }

    public static double sin(int degree) {
        return sin.get(degree);
    }

    public static double cos(int degree) {
        return cos.get(degree);
    }

    public static double tan(int degree) {
        return tan.get(degree);
    }
}

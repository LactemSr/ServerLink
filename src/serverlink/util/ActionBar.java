package serverlink.util;

import net.minecraft.server.v1_8_R3.ChatComponentText;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import serverlink.chat.chat;

public class ActionBar {

    public static void show(Player player, String message) {
        if (player.hasMetadata("NPC")) return;
        ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutChat(new ChatComponentText(chat.color(message)), (byte) 2));
    }
}

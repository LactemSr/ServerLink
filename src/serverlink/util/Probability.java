package serverlink.util;

import java.util.SplittableRandom;

public class Probability {
    private static final SplittableRandom r = new SplittableRandom();

    /**
     * @param percent value from 0.00 to 99.99
     */
    public static boolean get(double percent) {
        // nextDouble returns 0.0 to 100.0
        return r.nextDouble() < (percent / 100);
    }

    /**
     * @return a double from 0 to 99.99
     */
    public static double selector() {
        return r.nextDouble() * 100;
    }
}

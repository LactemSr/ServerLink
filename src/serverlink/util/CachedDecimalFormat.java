package serverlink.util;

import java.text.DecimalFormat;

public enum CachedDecimalFormat {
    ONES(new DecimalFormat("#########")),
    ONES_COMMAS(new DecimalFormat("###,###,###")),

    TENTHS(new DecimalFormat("###,###,###.0")),
    TENTHS_COMMAS(new DecimalFormat("###,###,###.0")),
    TENTHS_COMMAS_NO_TRAILING_ZEROS(new DecimalFormat("###,###,###.#")),
    TENTHS_NO_TRAILING_ZEROS(new DecimalFormat("###,###,###.#")),

    HUNDREDTHS(new DecimalFormat("#########.00")),
    HUNDREDTHS_COMMAS(new DecimalFormat("###,###,###.00")),
    HUNDREDTHS_COMMAS_NO_TRAILING_ZEROS(new DecimalFormat("###,###,###.##")),
    HUNDREDTHS_NO_TRAILING_ZEROS(new DecimalFormat("#########.##")),

    THOUSANDTHS(new DecimalFormat("#########.000")),
    THOUSANDTHS_COMMAS(new DecimalFormat("###,###,###.000")),
    THOUSANDTHS_COMMAS_NO_TRAILING_ZEROS(new DecimalFormat("###,###,###.###")),
    THOUSANDTHS_NO_TRAILING_ZEROS(new DecimalFormat("############"));
    private final DecimalFormat decimalFormat;

    private CachedDecimalFormat(DecimalFormat format) {
        this.decimalFormat = format;
    }

    public DecimalFormat getDecimalFormat() {
        return decimalFormat;
    }

    public String format(double input) {
        return decimalFormat.format(input);
    }

    public String format(long input) {
        return decimalFormat.format(input);
    }

    public String format(int input) {
        return decimalFormat.format(input);
    }
}

package serverlink.util;

import serverlink.ServerLink;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.scheduler.BukkitRunnable;

import javax.net.ssl.HttpsURLConnection;
import java.io.DataOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.ArrayList;

public class Slack {
    private static ArrayList<String> errors = new ArrayList<>();
    private static ArrayList<String> punishments = new ArrayList<>();
    private static String url;

    @SuppressWarnings("deprecation")
    public static void setup(File configFile, YamlConfiguration config) {
        if (!config.contains("slack.url")) {
            config.set("slack.url", "put url here");
            try {
                config.save(configFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        url = config.getString("slack.url");
        if (url.equals("put url here")) return;
        new BukkitRunnable() {
            @Override
            public void run() {
                if (errors.size() > 0) {
                    Slack.run("#errors", errors.get(0));
                }
            }
        }.runTaskTimerAsynchronously(ServerLink.plugin, 0, 100);
        new BukkitRunnable() {
            @Override
            public void run() {
                if (punishments.size() > 0) {
                    Slack.run("#punishments", punishments.get(0));
                }
            }
        }.runTaskTimerAsynchronously(ServerLink.plugin, 40, 100);
    }

    public static void sendPost(String channel, String message) {
        if (url.equals("put url here")) return;
        if (channel.equals("#punishments")) {
            punishments.add(message);
        } else if (channel.equals("#errors")) {
            errors.add(message);
        }
    }

    private static void run(String channel, String message) {
        String botName = "bot";
        if (channel.equals("#punishments")) {
            botName = "Punish-Bot";
        } else if (channel.equals("#errors")) {
            botName = "Error-Bot";
        }
        URL obj = null;
        try {
            obj = new URL(url);
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        HttpsURLConnection con = null;
        try {
            assert obj != null;
            con = (HttpsURLConnection) obj.openConnection();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
        try {
            assert con != null;
            con.setRequestMethod("POST");
        } catch (ProtocolException e) {
            e.printStackTrace();
        }
        con.setRequestProperty("User-Agent", "Mozilla/5.0");
        con.setRequestProperty("Accept-Language", "en-US,en;q=0.5");
        String content = "payload={\"text\": " + message + ", \"username\": " + botName + "\"icon_emoji\": \":gem:\""
                + "}";
        con.setDoOutput(true);
        DataOutputStream wr = null;
        try {
            wr = new DataOutputStream(con.getOutputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        try {
            assert wr != null;
            wr.writeBytes(content);
            wr.flush();
            wr.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package serverlink.util.profilefetcher;

import com.google.common.base.Preconditions;
import com.mojang.authlib.Agent;
import com.mojang.authlib.GameProfile;
import com.mojang.authlib.GameProfileRepository;
import com.mojang.authlib.ProfileLookupCallback;
import com.sun.istack.internal.Nullable;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.CraftServer;
import org.bukkit.scheduler.BukkitTask;
import serverlink.ServerLink;

import java.util.Collection;

/**
 * Fetches game profiles that include skin data from Mojang servers.
 *
 * @see ProfileFetchThread
 */
public class ProfileFetcher {
    private static ProfileFetchThread PROFILE_THREAD;
    private static BukkitTask THREAD_TASK;

    /**
     * Fetch a profile.
     *
     * @param skinName The name of the player the profile belongs to.
     * @param handler  Optional handler to handle the result. Handler always invoked from the main thread.
     */
    public static void fetch(String player, String skinName, @Nullable ProfileFetchHandler handler) {
        Preconditions.checkNotNull(skinName);

        if (PROFILE_THREAD == null) {
            initThread();
        }
        PROFILE_THREAD.fetch(player, skinName, handler);
    }

    @Nullable
    private static ProfileRequest findRequest(String name, Collection<ProfileRequest> requests) {
        name = name.toLowerCase();

        for (ProfileRequest request : requests) {
            if (request.getPlayerToGetProfileOf().equals(name)) {
                return request;
            }
        }
        return null;
    }

    private static void initThread() {
        if (THREAD_TASK != null) {
            THREAD_TASK.cancel();
        }

        PROFILE_THREAD = new ProfileFetchThread();
        THREAD_TASK = Bukkit.getScheduler().runTaskTimerAsynchronously(ServerLink.plugin, PROFILE_THREAD, 21, 20);
    }

    private static boolean isProfileNotFound(Throwable throwable) {
        String message = throwable.getMessage();
        String cause = throwable.getCause() != null ? throwable.getCause().getMessage() : null;

        return (message != null && message.contains("did not find"))
                || (cause != null && cause.contains("did not find"));
    }

    /**
     * Fetch one or more profiles.
     *
     * @param requests The profile requests.
     */
    void fetchRequests(final Collection<ProfileRequest> requests) {
        Preconditions.checkNotNull(requests);

        final GameProfileRepository repo = ((CraftServer) Bukkit.getServer()).getServer().getGameProfileRepository();

        String[] playerNames = new String[requests.size()];

        int i = 0;
        for (ProfileRequest request : requests) {
            playerNames[i++] = request.getPlayerToGetProfileOf();
        }

        repo.findProfilesByNames(playerNames, Agent.MINECRAFT, new ProfileLookupCallback() {
            @Override
            public void onProfileLookupFailed(GameProfile profile, Exception e) {
                while (true) {
                    ProfileRequest request = findRequest(profile.getName(), requests);
                    if (request == null)
                        return;

                    requests.remove(request);

                    if (isProfileNotFound(e))
                        request.setResult(null, ProfileFetchResult.NOT_FOUND);
                    else {
                        request.setResult(profile, ProfileFetchResult.PENDING);
                        fetch(request.getPlayer(), request.getPlayerToGetProfileOf(), request.handler);
                    }
                    e.addSuppressed(e);
                }
            }

            @Override
            public void onProfileLookupSucceeded(final GameProfile profile) {
                while (true) {
                    ProfileRequest request = findRequest(profile.getName(), requests);
                    if (request == null)
                        return;

                    requests.remove(request);

                    try {
                        GameProfile loadedProfile = ((CraftServer) Bukkit.getServer()).getServer().aD().fillProfileProperties(profile, true);
                        if (loadedProfile == null || !loadedProfile.isComplete() || loadedProfile.getProperties().isEmpty()
                                || loadedProfile.getProperties().get("textures") == null
                                || !loadedProfile.getProperties().get("textures").iterator().hasNext())
                            request.setResult(null, ProfileFetchResult.TOO_MANY_REQUESTS);
                        else
                            request.setResult(loadedProfile, ProfileFetchResult.SUCCESS);
                    } catch (Throwable throwable) {
                        if (isProfileNotFound(throwable)) request.setResult(null, ProfileFetchResult.NOT_FOUND);
                        else request.setResult(null, ProfileFetchResult.TOO_MANY_REQUESTS);
                    }
                }
            }
        });
    }
}

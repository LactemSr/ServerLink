package serverlink.util.profilefetcher;

import com.google.common.base.Preconditions;
import com.mojang.authlib.GameProfile;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;

import javax.annotation.Nullable;

/**
 * Stores basic information about a single profile used to request profiles from the Mojang servers.
 * <p>
 * <p>
 * Also stores the result of the request.
 * </p>
 */
public class ProfileRequest {
    private final String player;
    private final String playerToGetProfileOf;
    ProfileFetchHandler handler;
    private GameProfile profile;
    private volatile ProfileFetchResult result = ProfileFetchResult.PENDING;

    /**
     * Constructor.
     *
     * @param player               The player that will be used in onResult
     * @param playerToGetProfileOf The player skin to be fetched.
     * @param handler              Optional handler to handle the result for the profile. Handler always invoked from the main thread.
     */
    ProfileRequest(String player, String playerToGetProfileOf, @Nullable ProfileFetchHandler handler) {
        this.player = player;
        this.playerToGetProfileOf = playerToGetProfileOf;

        if (handler != null) {
            setHandler(handler);
        }
    }

    public String getPlayer() {
        return player;
    }

    /**
     * Add one time result handler.
     * <p>
     * <p>
     * Handler is always invoked from the main thread.
     * </p>
     *
     * @param handler The result handler.
     */
    void setHandler(ProfileFetchHandler handler) {
        Preconditions.checkNotNull(handler);

        if (result != ProfileFetchResult.PENDING) {
            handler.onResult(this);
            return;
        }

        this.handler = handler;
    }

    /**
     * Get the name of the player the requested profile belongs to.
     */
    public String getPlayerToGetProfileOf() {
        return playerToGetProfileOf;
    }

    /**
     * Get the game profile that was requested.
     *
     * @return The game profile or null if the profile has not been retrieved yet or there was an error while retrieving
     * the profile.
     */
    @Nullable
    public GameProfile getProfile() {
        return profile;
    }

    /**
     * Get the result of the profile fetch.
     */
    public ProfileFetchResult getResult() {
        return result;
    }

    /**
     * Invoked to set the profile result.
     * <p>
     * <p>
     * Can be invoked from any thread, always executes on the main thread.
     * </p>
     *
     * @param profile The profile. Null if there was an error.
     * @param result  The result of the request.
     */
    void setResult(final @Nullable GameProfile profile, final ProfileFetchResult result) {
        new BukkitRunnable() {
            @Override
            public void run() {
                ProfileRequest.this.profile = profile;
                ProfileRequest.this.result = result;

                if (handler == null) return;
                handler.onResult(ProfileRequest.this);
                handler = null;
            }
        }.runTask(ServerLink.plugin);
    }
}

package serverlink.util.profilefetcher;

import com.google.common.base.Preconditions;
import org.bukkit.Bukkit;
import serverlink.ServerLink;

import javax.annotation.Nullable;
import java.util.*;

/**
 * Thread used to fetch profiles from the Mojang servers.
 * <p>
 * <p>
 * Maintains a cache of profiles so that no profile is ever requested more than once during a single server session.
 * </p>
 *
 * @see ProfileFetcher
 */
class ProfileFetchThread implements Runnable {
    private final ProfileFetcher profileFetcher = new ProfileFetcher();
    private final Deque<ProfileRequest> queue = new ArrayDeque<>();
    private final Map<String, ProfileRequest> requested = new HashMap<>(35);
    private final Object sync = new Object(); // sync for queue & requested fields

    ProfileFetchThread() {
    }

    private static void addHandler(final ProfileRequest request, final ProfileFetchHandler handler) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(ServerLink.plugin, () -> request.setHandler(handler), 1);
    }

    private static void sendResult(final ProfileFetchHandler handler, final ProfileRequest request) {
        Bukkit.getScheduler().scheduleSyncDelayedTask(ServerLink.plugin, () -> handler.onResult(request), 1);
    }

    /**
     * Fetch a profile.
     *
     * @param player               The player that will be used in onResult
     * @param playerToGetProfileOf The player skin to be fetched.
     * @param handler              Optional handler to be called when the result is fetched. Handler always invoked from the main thread.
     * @see ProfileFetcher#fetch
     */
    void fetch(String player, String playerToGetProfileOf, @Nullable ProfileFetchHandler handler) {
        Preconditions.checkNotNull(playerToGetProfileOf);

        playerToGetProfileOf = playerToGetProfileOf.toLowerCase();
        ProfileRequest request;

        synchronized (sync) {
            request = new ProfileRequest(player, playerToGetProfileOf, handler);
            queue.add(request);
        }

        if (handler != null) {
            if (request.getResult() == ProfileFetchResult.PENDING
                    || request.getResult() == ProfileFetchResult.TOO_MANY_REQUESTS) {
                addHandler(request, handler);
            } else {
                sendResult(handler, request);
            }
        }
    }

    @Override
    public void run() {
        List<ProfileRequest> requests;

        synchronized (sync) {
            if (queue.isEmpty())
                return;

            requests = new ArrayList<>(queue);
            queue.clear();
        }

        profileFetcher.fetchRequests(requests);
    }
}

package serverlink.util;

import serverlink.chat.chat;
import org.apache.commons.lang.WordUtils;

import java.util.Arrays;
import java.util.List;

public class LoreUtil {

    public static List<String> wrap(String text, int lineLength) {
        // undo any already-translated color codes
        text = text.replaceAll("§", "&");
        // split the String into lines
        text = WordUtils.wrap(text, lineLength, "\n", true);
        List<String> lore = Arrays.asList(text.split("\n"));

        // move color codes to their proper places (i.e. not at ends of lines)
        for (int i = 0; i < lore.size(); i++) {
            String s = lore.get(i).trim();
            if (s.endsWith("&")) {
                lore.set(i, s.substring(0, s.length() - 1));
                lore.set(i + 1, "&" + lore.get(i + 1));
            }
            if (s.isEmpty()) {
                lore.remove(i);
                i--;
                continue;
            }
            if (s.length() == 1) {
                if (s.equals("&")) {
                    lore.set(i + 1, "&" + lore.get(i + 1));
                    lore.remove(i);
                    i--;
                }
                continue;
            }
            while (s.substring(s.length() - 2, s.length() - 1).equals("&")) {
                String color = s.substring(s.length() - 2, s.length());
                // remove color from end of current line
                lore.set(i, s.substring(0, s.length() - 2));
                // add color to front of next line
                lore.set(i + 1, color + lore.get(i + 1));
                s = s.substring(0, s.length() - 2);
                if (s.isEmpty()) {
                    lore.remove(i);
                    i--;
                    break;
                }
                if (s.length() == 1) {
                    if (s.equals("&")) {
                        lore.set(i + 1, "&" + lore.get(i + 1));
                        lore.remove(i);
                        i--;
                    }
                    break;
                }
            }
        }
        // add color and keep it from line to line
        String lineStartColor = "&r&f";
        String lineEndColor = "";
        for (int i = 0; i < lore.size(); i++) {
            String l = lore.get(i);
            if (l.contains("&")) {
                int colorIndex = l.lastIndexOf("&");
                lineEndColor = l.substring(colorIndex, colorIndex + 2);
                if (colorIndex >= 2 && l.charAt(colorIndex - 2) == '&')
                    lineEndColor = l.substring(colorIndex - 2, colorIndex) + lineEndColor;
            }
            lore.set(i, chat.color(lineStartColor + l));
            if (!lineEndColor.isEmpty()) lineStartColor = lineEndColor;
        }
        return lore;
    }
}

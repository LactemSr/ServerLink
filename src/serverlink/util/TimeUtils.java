package serverlink.util;

public class TimeUtils {
    public static int getCurrent() {
        return ((Long) (System.currentTimeMillis() / 1000)).intValue();
    }

    public static int nSecondsLater(int seconds) {
        return getCurrent() + seconds;
    }

    public static int nMinutesLater(int minutes) {
        return getCurrent() + (minutes * 60);
    }

    public static int nHoursLater(int hours) {
        return getCurrent() + (hours * 3600);
    }

    public static int nDaysLater(int days) {
        return getCurrent() + (days * 86400);
    }

    public static String nSecondsLater_String(int seconds) {
        return ((Integer) nSecondsLater(seconds)).toString();
    }

    public static String nMinutesLater_String(int minutes) {
        return ((Integer) nMinutesLater(minutes)).toString();
    }

    public static String nHoursLater_String(int hours) {
        return ((Integer) nHoursLater(hours)).toString();
    }

    public static String nDaysLater_String(int days) {
        return ((Integer) nDaysLater(days)).toString();
    }

    public static int secondsUntil(int laterInstant) {
        return laterInstant - getCurrent();
    }

    public static double minutesUntil(int laterInstant) {
        return laterInstant - (getCurrent() / 60.0);
    }

    public static double hoursUntil(int laterInstant) {
        return laterInstant - (getCurrent() / 3600.0);
    }

    public static double daysUntil(int laterInstant) {
        return laterInstant - (getCurrent() / 86400.0);
    }

    public static int secondsBetween(int firstInstant, int secondInstant) {
        return secondInstant - firstInstant;
    }

    public static double minutesBetween(int firstInstant, int secondInstant) {
        return (secondInstant - firstInstant) / 60.0;
    }

    public static double hoursBetween(int firstInstant, int secondInstant) {
        return (secondInstant - firstInstant) / 3600.0;
    }

    public static double daysBetween(int firstInstant, int secondInstant) {
        return (secondInstant - firstInstant) / 86400.0;
    }
}

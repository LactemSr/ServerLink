package serverlink.util;

import com.google.common.collect.Lists;
import mkremins.fanciful.FancyMessage;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;

import java.util.List;

public class HelpUtils {
    private static HelpUtils instance;

    private List<HelpBook> books;

    public HelpUtils() {
        loadBooks();
        instance = this;
    }

    public static HelpUtils getInstance() {
        return instance;
    }

    // Use the help book called "help," starting at page 1
    public void help(Player player) {
        help(player, "help", 1);
    }

    // Send the contents of a help book at a specific page if it exists or suggest one with a similar name if it doesn't
    public void help(Player player, String book, int page) {
        book = book.toLowerCase();

        // Find the right HelpBook
        for (HelpBook testBook : getBooks()) {
            if (testBook != null && testBook.getName().equals(book)) {
                help(player, testBook, page);
                return;
            }
        }

        // If it reaches here, no book was found
        player.sendMessage("No help for topic: " + book);
    }

    // Send the contents of a help book at a specific page
    public void help(Player player, HelpBook book, int page) {
        // Make sure the page requested is positive
        if (page < 1) {
            player.sendMessage("Topics only have positive page numbers (not " + page + ")!");
            return;
        }

        // Make sure the book has a page with the given number
        if (page > book.getPageCount()) {
            player.sendMessage("This topic only has " + book.getPageCount() + " pages!");
            return;
        }
        player.sendMessage("");
        CommandText.sendMessage(player, book.getPageText(page));
        player.sendMessage("");
    }

    public List<HelpBook> getBooks() {
        return books;
    }

    public void addBook(HelpBook book) {
        books.add(book);
    }

    // All pages and line numbers start at 1 (not 0)
    // Color codes are automatically translated using the builder
    private void loadBooks() {
        books = Lists.newArrayList();

        // Main help page (book is called "help")
        addBook(new HelpBook.Builder()
                .name("help")
                // Page 1
                .addPage(new FancyMessage("-=-=-=-= ").color(ChatColor.GRAY)
                        .style(ChatColor.BOLD)
                        .then("Help ").color(ChatColor.GOLD).style(ChatColor.BOLD)
                        .then("(1) ").color(ChatColor.GRAY).then(">").color(ChatColor.AQUA)
                        .command("/help 2").tooltip("Next page").then(" =-=-=-=-")
                        .color(ChatColor.GRAY).style(ChatColor.BOLD)

                        .then("\nFriends").color(ChatColor.LIGHT_PURPLE).command("/f help").tooltip("Learn about friends on AlphaCraft")
                        .then("\nParties").color(ChatColor.LIGHT_PURPLE).command("/p").tooltip("Learn about parties on AlphaCraft")
                        .then("\nTreasure").color(ChatColor.LIGHT_PURPLE).command("/help treasure").tooltip("Learn how to get and use treasure chests to unlock features")
                        .then("\nHub Effects").color(ChatColor.LIGHT_PURPLE).command("/help hubeffects").tooltip("Learn about stylish Hub Effects")
                        .then("\nForums").color(ChatColor.LIGHT_PURPLE).command("/forums").tooltip("Learn about our engaging forums")
                        .then("\nReferrals").color(ChatColor.LIGHT_PURPLE).command("/refer").tooltip("Learn how to get rewards for referring friends")
                        .then("\nCoins").color(ChatColor.LIGHT_PURPLE).command("/help coins").tooltip("Learn what coins are used for and how to get them")
                        .then("\n✰✰✰ Effects and ❤❤❤ Effects").color(ChatColor.LIGHT_PURPLE).command("/help classeffects").tooltip("Learn about getting amazing effects for your classes")
                )

                // Page 2
                .addPage(new FancyMessage("-=-=-=-= ").color(ChatColor.GRAY)
                        .style(ChatColor.BOLD)
                        .then("< ").color(ChatColor.AQUA).command("/help")
                        .tooltip("Previous page")
                        .then("Help ").color(ChatColor.GOLD).style(ChatColor.BOLD)
                        .then("(2) ").color(ChatColor.GRAY).then("=-=-=-=-")
                        .color(ChatColor.GRAY).style(ChatColor.BOLD)

                        .then("\nKitPvP").color(ChatColor.LIGHT_PURPLE).command("/help kitpvp").tooltip("Learn about KitPvP on AlphaCraft")
                )
                .build());


        // treasure books
        FancyMessage treasure = new FancyMessage("Treasure: ").color(ChatColor.GREEN)
                .then("win hub effects and coins that can be used in hub and in minigames while waiting for the next round to start")
                .color(ChatColor.GRAY)
                .then("\nFinding Chests: ").color(ChatColor.GREEN)
                .then("randomly find treasure chests while playing any game on AlphaCraft").color(ChatColor.GRAY)
                .then("\nBuying Chests with Coins: ").color(ChatColor.GREEN)
                .then("click the chest in the upper right corner of the Treasure menu").color(ChatColor.GRAY)
                .then("\nChests Types: ").color(ChatColor.GREEN)
                .then("mini chests -> rare chests -> epic chests -> legendary chests").color(ChatColor.GRAY);
        addBook(new HelpBook.Builder()
                .name("treasure")
                .addPage(treasure)
                .build());
        addBook(new HelpBook.Builder()
                .name("treasurechest")
                .addPage(treasure)
                .build());
        addBook(new HelpBook.Builder()
                .name("treasurechests")
                .addPage(treasure)
                .build());
        addBook(new HelpBook.Builder()
                .name("minichest")
                .addPage(treasure)
                .build());
        addBook(new HelpBook.Builder()
                .name("minichests")
                .addPage(treasure)
                .build());
        addBook(new HelpBook.Builder()
                .name("epicchest")
                .addPage(treasure)
                .build());
        addBook(new HelpBook.Builder()
                .name("epicchests")
                .addPage(treasure)
                .build());
        addBook(new HelpBook.Builder()
                .name("epichest")
                .addPage(treasure)
                .build());
        addBook(new HelpBook.Builder()
                .name("epichests")
                .addPage(treasure)
                .build());
        addBook(new HelpBook.Builder()
                .name("legendarychest")
                .addPage(treasure)
                .build());
        addBook(new HelpBook.Builder()
                .name("legendarychests")
                .addPage(treasure)
                .build());


        // hub effects books
        FancyMessage hubEffects = new FancyMessage("Gadgets: ").color(ChatColor.GREEN)
                .then("can only be unlocked with ranks").color(ChatColor.GRAY)
                .then("\nHats: ").color(ChatColor.GREEN)
                .then("some are unlocked with ranks, others through events and Treasure Chests").color(ChatColor.GRAY)
                .then("\nParticle Effects: ").color(ChatColor.GREEN)
                .then("some are unlocked with ranks, others through events and Treasure Chests").color(ChatColor.GRAY)
                .then("\nMounts: ").color(ChatColor.GREEN)
                .then("some are unlocked with ranks, others through events and Treasure Chests").color(ChatColor.GRAY)
                .then("\nMorphs: ").color(ChatColor.GREEN)
                .then("some are unlocked with ranks, others through events and Treasure Chests").color(ChatColor.GRAY)
                .then("\nPets: ").color(ChatColor.GREEN)
                .then("some are unlocked with ranks, others through events and Treasure Chests").color(ChatColor.GRAY);
        addBook(new HelpBook.Builder()
                .name("hubeffects")
                .addPage(hubEffects)
                .build());
        addBook(new HelpBook.Builder()
                .name("cosmetics")
                .addPage(hubEffects)
                .build());


        // coins book
        addBook(new HelpBook.Builder()
                .name("coins")
                .addPage(new FancyMessage("Coins: ").color(ChatColor.GREEN)
                        .then("used to gain cosmetic advantages such as class combat effects and hub effects").color(ChatColor.GRAY)
                        .then("\nGetting Coins: ").color(ChatColor.GREEN)
                        .then("can be bought online, claimed daily and monthly from Jack Sparrow in hubs, and found in treasure chests").color(ChatColor.GRAY)
                )
                .build());


        // class and star effect books
        FancyMessage classEffects = new FancyMessage("✰✰✰ Effects: ").color(ChatColor.AQUA)
                .then("really good-looking particle effects that play when doing something combat-related like shooting an arrow or using a special ability").color(ChatColor.GRAY)
                .then("\n❤❤❤ Effects: ").color(ChatColor.AQUA)
                .then("cool particle effects (like flame halos!) that are always active while a class is equipped").color(ChatColor.GRAY)
                .then("\nEvery class in every game has its own ").color(ChatColor.GRAY)
                .then("❤❤❤ Effect ").color(ChatColor.AQUA).then("and ").color(ChatColor.GRAY)
                .then("✰✰✰ Effect ").color(ChatColor.AQUA)
                .then("that cost more or less coins to unlock, depending on how extra special they make the class look.").color(ChatColor.GRAY);
        addBook(new HelpBook.Builder()
                .name("classeffects")
                .addPage(classEffects)
                .build());
        addBook(new HelpBook.Builder()
                .name("combateffects")
                .addPage(classEffects)
                .build());
        addBook(new HelpBook.Builder()
                .name("stareffects")

                .addPage(new FancyMessage("✰✰✰ Effects: ").color(ChatColor.AQUA)
                        .then("really good-looking particle effects that play when doing something combat-related like shooting an arrow or using a special ability").color(ChatColor.GRAY)
                        .then("\nEvery class in every game has its own ").color(ChatColor.GRAY)
                        .then("❤❤❤ Effect ").color(ChatColor.AQUA).then("and ").color(ChatColor.GRAY)
                        .then("✰✰✰ Effect ").color(ChatColor.AQUA)
                        .then("that cost more or less coins to unlock, depending on how extra special they make the class look.").color(ChatColor.GRAY)
                )
                .build());
        addBook(new HelpBook.Builder()
                .name("hearteffects")
                .addPage(new FancyMessage("❤❤❤ Effects: ").color(ChatColor.AQUA)
                        .then("cool particle effects (like flame halos!) that are always active while a class is equipped").color(ChatColor.GRAY)
                        .then("\nEvery class in every game has its own ").color(ChatColor.GRAY)
                        .then("❤❤❤ Effect ").color(ChatColor.AQUA).then("and ").color(ChatColor.GRAY)
                        .then("✰✰✰ Effect ").color(ChatColor.AQUA)
                        .then("that cost more or less coins to unlock, depending on how extra special they make the class look.").color(ChatColor.GRAY)
                ).build());


        // kitpvp help book
        addBook(new HelpBook.Builder()
                .name("kitpvp")
                .addPage(new FancyMessage("Rewards: ").color(ChatColor.GREEN)
                        .then("Under the KitPvP Menu (beacon), click the paper to view all possible point rewards and classes you can unlock by getting kills and killstreaks.").color(ChatColor.GRAY)
                        .then("\nPowerups: ").color(ChatColor.GREEN)
                        .then("They spawn all over the map so you can collect as many as possible.").color(ChatColor.GRAY)
                        .then("\nUpgrade Signs: ").color(ChatColor.GREEN)
                        .then("Upgrade stats will never degrade your gear. You can only use each sign one time until you die or change classes.").color(ChatColor.GRAY)
                        .then("\nCombat Bar: ").color(ChatColor.GREEN)
                        .then("When you enter combat, a helpful bar will appear above your hotbar. You cannot log out while it's in the red or you will die in-game.").color(ChatColor.GRAY)
                        .then("\nKills and Assists: ").color(ChatColor.GREEN)
                        .then("A message will appear in the center of your screen for every kill and assist you get, telling you how many points you earned (earn more for higher killstreaks).").color(ChatColor.GRAY)
                )
                .build());
    }
}

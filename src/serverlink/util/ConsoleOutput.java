package serverlink.util;

import org.bukkit.Bukkit;

import java.util.logging.Level;

public class ConsoleOutput {
    public static void printError(String message) {
        System.out.print("\n\n\n\n");
        Bukkit.getLogger().log(Level.SEVERE, message);
        System.out.print("\n\n\n\n");
    }

    public static void printWarning(String message) {
        System.out.print("\n");
        Bukkit.getLogger().log(Level.WARNING, message);
        System.out.print("\n");
    }
}

package serverlink.util;

import org.bukkit.scheduler.BukkitRunnable;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import serverlink.ServerLink;

import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ThreadLocalRandom;

public class UUIDFetcher {
    private static final String PROFILE_URL = "https://api.mojang.com/profiles/minecraft";
    private static boolean coolingDown = false;

    public static synchronized UUID getOfflineUUID(String username) throws Exception {
        //noinspection StatementWithEmptyBody
        if (coolingDown) Thread.sleep(ThreadLocalRandom.current().nextInt(300) + 100);
        JSONParser jsonParser = new JSONParser();
        HttpURLConnection connection = createConnection();
        List<String> names = new ArrayList<>();
        names.add(username);
        String body = JSONArray.toJSONString(names);
        writeBody(connection, body);
        writeBody(connection, username);
        JSONArray array = (JSONArray) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
        connection.disconnect();
        coolingDown = true;
        new BukkitRunnable() {
            @Override
            public void run() {
                coolingDown = false;
            }
        }.runTaskLater(ServerLink.plugin, 25);
        if (array.isEmpty()) return null;
        return getUUID((String) ((JSONObject) array.get(0)).get("id"));
    }

    public static List<String> getOfflineUUIDAndName(String username) throws Exception {
        //noinspection StatementWithEmptyBody
        if (coolingDown) Thread.sleep(ThreadLocalRandom.current().nextInt(300) + 100);
        JSONParser jsonParser = new JSONParser();
        HttpURLConnection connection = createConnection();
        List<String> names = new ArrayList<>();
        names.add(username);
        String body = JSONArray.toJSONString(names);
        writeBody(connection, body);
        writeBody(connection, username);
        JSONArray array = (JSONArray) jsonParser.parse(new InputStreamReader(connection.getInputStream()));
        connection.disconnect();
        coolingDown = true;
        new BukkitRunnable() {
            @Override
            public void run() {
                coolingDown = false;
            }
        }.runTaskLater(ServerLink.plugin, 25);
        if (array.isEmpty()) return null;
        //noinspection LoopStatementThatDoesntLoop
        for (Object profile : array) {
            JSONObject jsonProfile = (JSONObject) profile;
            UUID uuid = getUUID((String) jsonProfile.get("id"));
            String name = (String) jsonProfile.get("name");
            ArrayList<String> list = new ArrayList<>();
            list.add(0, uuid.toString());
            list.add(1, name);
            return list;
        }
        return null;
    }

    private static void writeBody(HttpURLConnection connection, String body) throws Exception {
        OutputStream stream = connection.getOutputStream();
        stream.write(body.getBytes());
        stream.flush();
        stream.close();
    }

    private static HttpURLConnection createConnection() throws Exception {
        URL url = new URL(PROFILE_URL);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Content-Type", "application/json");
        connection.setUseCaches(false);
        connection.setDoInput(true);
        connection.setDoOutput(true);
        return connection;
    }

    private static UUID getUUID(String id) {
        String uuid = id.substring(0, 8) + "-" + id.substring(8, 12) + "-" + id.substring(12, 16) + "-"
                + id.substring(16, 20) + "-" + id.substring(20, 32);
        return UUID.fromString(uuid);
    }
}

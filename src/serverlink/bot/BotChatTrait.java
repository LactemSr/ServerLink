package serverlink.bot;

import com.google.code.chatterbotapi.ChatterBot;
import com.google.code.chatterbotapi.ChatterBotType;
import com.michaelwflaherty.cleverbotapi.CleverBotQuery;
import net.citizensnpcs.api.trait.Trait;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.bot.speech.*;
import serverlink.customevent.CustomFullyJoinEvent;
import serverlink.customevent.CustomPlayerKillLivingEntityEvent;
import serverlink.customevent.CustomPlayerMessageBotEvent;
import serverlink.util.Probability;
import serverlink.util.TimeUtils;

import javax.annotation.Nullable;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class BotChatTrait extends Trait {
    private static final List<String> ggForms = new ArrayList<String>() {{
        add("~gg");
        add("good game");
        add("gud game");
        add("=rekt");
        add("~rekt");
        add("=wrecked");
        add("~wrecked");
    }};
    private static final List<String> greetingForms = new ArrayList<String>() {{
        add("~hi");
        add("~hey");
        add("~hello");
        add("~whats");
        add("~what's");
        add("~wassup");
        add("~wazup");
        add("~wazzup");
        add("~oi");
        add("~hallo");
        add("~helo");
        add("~welcome");
        add("~hay");
        add("~hie");
        add("~heyder");
        add("~dude");
        add("~bro");
        add("~bruh");
        add("~yo");
    }};
    private static final List<String> hackusationForms = new ArrayList<String>() {{
        add("~hacks");
        add("~cheat");
        add("~cheated");
        add("~cheats");
        add("~hacked");
        add("~hacking");
        add("~hackz");
        add("~hacker");
        add("~hackzor");
        add("~cheater");
        add("~client");
        add("~hax");
        add("~haxor");
        add("~haxed");
        add("~haxing");
        add("~haking");
        add("~hakor");
        add("~forcefield");
        add("~aimbot");
        add("~reach");
        add("~aimassist");
        add("aim assist");
        add("~autoclick");
        add("~autoclicker");
        add("~aim bot");
        add("~knockback");
        add("anti-kb");
        add("anti kb");
        add("no-kb");
        add("no kb");
        add("anti-knockback");
        add("anti knockback");
        add("no-knockback");
        add("no knockback");
        add("~regen");
        add("~fastheal");
        add("~fastregen");
        add("fast regen");
        add("fast heal");
    }};
    private static final List<String> pvpChallengeForms = new ArrayList<String>() {{
        add("~1v1");
        add("1 v 1");
        add("1 vs 1");
        add("1 vs. 1");
        add("1 on 1");
        add("1 versus 1");
        add("one v one");
        add("one v. one");
        add("one vs. one");
        add("one versus one");
        add("one on one");
        add("~fite");
        add("~fight");
        add("~duel");
        add("come at m");
    }};
    private static final List<String> doIfNotBotForms = new ArrayList<String>() {{
        add("if you're not a bot");
        add("if you're not a ai");
        add("if you're not an ai");
        add("if you're not bot");
        add("if you're not ai");
        add("if youre not a bot");
        add("if youre not a ai");
        add("if youre not an ai");
        add("if youre not bot");
        add("if youre not ai");
        add("if your not a bot");
        add("if your not a ai");
        add("if your not an ai");
        add("if your not bot");
        add("if your not ai");
        add("if ur not a bot");
        add("if ur not a ai");
        add("if ur not an ai");
        add("if ur not bot");
        add("if ur not ai");
        add("if you are not a bot");
        add("if you are not a ai");
        add("if you are not an ai");
        add("if you are not bot");
        add("if you are not ai");
        add("if u are not a bot");
        add("if u are not a ai");
        add("if u are not an ai");
        add("if u are not bot");
        add("if u are not ai");
    }};
    private static final List<String> doIfAreBotForms = new ArrayList<String>() {{
        add("if you're a bot");
        add("if you're a ai");
        add("if you're an ai");
        add("if you're bot");
        add("if you're ai");
        add("if youre a bot");
        add("if youre a ai");
        add("if youre an ai");
        add("if youre bot");
        add("if youre ai");
        add("if your a bot");
        add("if your a ai");
        add("if your an ai");
        add("if your bot");
        add("if your ai");
        add("if ur a bot");
        add("if ur a ai");
        add("if ur an ai");
        add("if ur bot");
        add("if ur ai");
        add("if you are a bot");
        add("if you are a ai");
        add("if you are an ai");
        add("if you are bot");
        add("if you are ai");
        add("if u are a bot");
        add("if u are a ai");
        add("if u are an ai");
        add("if u are bot");
        add("if u are ai");
    }};
    private static final SplittableRandom r = new SplittableRandom();
    public SpeechStyle style;
    private ChatterBot cleverBot;
    private Bot bot;
    private Map<UUID, CleverBotQuery> sessions = new HashMap<>();
    private ConcurrentHashMap<UUID, Long> conversationEnd = new ConcurrentHashMap<>();
    private List<UUID> playersGreeted = new ArrayList<>();
    private String killerName = "";
    private List<String> recentVictims = new ArrayList<>();
    private boolean voluntarilySpeaks = false;
    private boolean saidGG = false;
    private int lastHackusationDefense = TimeUtils.getCurrent();
    private boolean typing = false;
    private int typingTimeTarget = 0;
    private int typingTimeProgress = 0;
    private String response = "";

    BotChatTrait(Bot bot) {
        super("botchat");
        this.bot = bot;
        if (Probability.get(50)) style = SpeechStyle.NO_CAP_NO_PUNC;
        else if (Probability.get(50)) style = SpeechStyle.YES_CAP_YES_PUNC;
        else if (Probability.get(50)) style = SpeechStyle.YES_CAP_NO_PUNC;
        else style = SpeechStyle.NO_CAP_YES_PUNC;
        voluntarilySpeaks = Probability.get(10);
        try {
            cleverBot = BotManager.chatBotFactory.create(ChatterBotType.CLEVERBOT);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (!voluntarilySpeaks) return;
    }

    /**
     * ~   checks if message contains a word: space before and after a form, first chars and space after a form, last chars and space before a form
     * =   checks if whole message equals a form
     * no ~ or = checks if message contains a form
     *
     * @param overrideWordWithContains ignores '~' and just checks if a form contains()
     */
    private static boolean matches(String message, List<String> forms, boolean overrideWordWithContains) {
        // perform check based on above comments
        if (overrideWordWithContains) {
            for (String s : forms) {
                if (s.startsWith("~")) {
                    if (message.contains(s.substring(1))) return true;
                } else if (s.startsWith("=")) {
                    if (message.equals(s.substring(1))) return true;
                } else if (message.contains(s)) return true;
            }
        } else {
            for (String s : forms) {
                if (s.startsWith("~")) {
                    if (message.startsWith(s.substring(1) + " ")) return true;
                    if (message.endsWith(" " + s.substring(1))) return true;
                    if (message.contains(" " + s.substring(1) + " ")) return true;
                } else if (s.startsWith("=")) {
                    if (message.equals(s.substring(1))) return true;
                } else if (message.contains(s)) return true;
            }
        }
        return false;
    }

    /**
     * @param message can end with "-" to mean punctuation not optional,
     *                or with no punctuation to mean punctuation will never be added
     */
    private void say(String message) {
        if (style == SpeechStyle.YES_CAP_NO_PUNC) {
            if (message.endsWith(".") || message.endsWith("!") || message.endsWith("?") || message.endsWith("-"))
                message = message.substring(0, 1).toUpperCase() + message.substring(1, message.length() - 1);
            else
                message = message.substring(0, 1).toUpperCase() + message.substring(1, message.length());
        } else if (style == SpeechStyle.YES_CAP_YES_PUNC) {
            if (message.endsWith("-"))
                message = message.substring(0, 1).toUpperCase() + message.substring(1, message.length() - 1);
            else
                message = message.substring(0, 1).toUpperCase() + message.substring(1, message.length());
        } else if (style == SpeechStyle.NO_CAP_NO_PUNC) {
            if (message.endsWith(".") || message.endsWith("!") || message.endsWith("?") || message.endsWith("-"))
                message = message.substring(0, message.length() - 1);
        } else if (style == SpeechStyle.NO_CAP_YES_PUNC) {
            if (message.endsWith("-"))
                message = message.substring(0, message.length() - 1);
        }
        String finalMessage = message;
        new BukkitRunnable() {
            @Override
            public void run() {
                if (npc.getEntity() == null || !npc.isSpawned() || npc.getEntity().isDead())
                    return;
                AsyncPlayerChatEvent event = new AsyncPlayerChatEvent(true, (Player) npc.getEntity(), finalMessage, new HashSet<>(Bukkit.getOnlinePlayers()));
                Bukkit.getPluginManager().callEvent(event);
                if (event.isCancelled()) return;
                for (Player p : Bukkit.getOnlinePlayers()) {
                    p.sendMessage(event.getFormat().replace("%2$s", event.getMessage()).replace("%1$s", ((Player) npc.getEntity()).getDisplayName()));
                }
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    /**
     * @param message can end with "-" to mean punctuation not optional,
     *                or with no punctuation to mean punctuation will never be added
     */
    private void sayTo(String message, Player player) {
        if (style == SpeechStyle.YES_CAP_NO_PUNC) {
            if (message.endsWith(".") || message.endsWith("!") || message.endsWith("?") || message.endsWith("-"))
                message = message.substring(0, 1).toUpperCase() + message.substring(1, message.length() - 1);
            else
                message = message.substring(0, 1).toUpperCase() + message.substring(1, message.length());
        } else if (style == SpeechStyle.YES_CAP_YES_PUNC) {
            if (message.endsWith("-"))
                message = message.substring(0, 1).toUpperCase() + message.substring(1, message.length() - 1);
            else
                message = message.substring(0, 1).toUpperCase() + message.substring(1, message.length());
        } else if (style == SpeechStyle.NO_CAP_NO_PUNC) {
            if (message.endsWith(".") || message.endsWith("!") || message.endsWith("?") || message.endsWith("-"))
                message = message.substring(0, message.length() - 1);
        } else if (style == SpeechStyle.NO_CAP_YES_PUNC) {
            if (message.endsWith("-"))
                message = message.substring(0, message.length() - 1);
        }
        if (npc.getEntity() == null || !npc.isSpawned() || npc.getEntity().isDead())
            return;
        ((Player) npc.getEntity()).performCommand("tell " + player.getName() + " " + message);
    }

    private void respondTo(Action action, @Nullable UUID enterConvoWith) {
        if (typing || bot.standingAfk) return;
        String response = "";
        if (action == Action.BEING_KILLED) response = ResponseToBeingKilled.getVariation();
        else if (action == Action.CALL) response = ResponseToCall.getVariation();
        else if (action == Action.GG) response = ResponseToGG.getVariation();
        else if (action == Action.GREETING) response = ResponseToGreeting.getVariation();
        else if (action == Action.HACKUSATION) response = ResponseToHackusation.getVariation();
        else if (action == Action.JOIN) response = ResponseToJoin.getVariation();
        else if (action == Action.KILL) response = ResponseToKill.getVariation();
        else if (action == Action.PVP_CHALLENGE) response = ResponseToPvpChallenge.getVariation();
        else if (action == Action.SAY_IF_NOT_BOT) response = ResponseToSayIfNotBot.getVariation();
        String finalResponse = response;
        typing = true;
        typingTimeProgress = 0;
        typingTimeTarget = 10 + ((int) (response.length() / 4.5 + r.nextDouble(response.length() / 15.0 + 1))) * 20;
        new BukkitRunnable() {
            @Override
            public void run() {
                if (npc == null || !npc.isSpawned() || npc.getEntity() == null || npc.getEntity().isDead()) {
                    typing = false;
                    typingTimeProgress = 0;
                    typingTimeTarget = 0;
                    cancel();
                    return;
                }
                npc.getNavigator().cancelNavigation();
                npc.getNavigator().setTarget(null);
                npc.getNavigator().cancelNavigation();
                npc.getNavigator().setPaused(true);
                typingTimeProgress += 1;
                if (typingTimeProgress < typingTimeTarget) return;
                cancel();
                say(finalResponse);
                typing = false;
                typingTimeProgress = 0;
                typingTimeTarget = 0;
                npc.getNavigator().setPaused(false);
                if (enterConvoWith != null) conversationEnd.put(enterConvoWith, System.currentTimeMillis() + 14000);
            }
        }.runTaskTimer(ServerLink.plugin, 1, 1);
    }

    private void respondDirectlyTo(Action action, Player player) {
        if (typing || bot.standingAfk) return;
        String response = "";
        if (action == Action.BEING_KILLED) response = ResponseToBeingKilled.getVariation();
        else if (action == Action.CALL) response = ResponseToCall.getVariation();
        else if (action == Action.GG) response = ResponseToGG.getVariation();
        else if (action == Action.GREETING) response = ResponseToGreeting.getVariation();
        else if (action == Action.HACKUSATION) response = ResponseToHackusation.getVariation();
        else if (action == Action.JOIN) response = ResponseToJoin.getVariation();
        else if (action == Action.KILL) response = ResponseToKill.getVariation();
        else if (action == Action.PVP_CHALLENGE) response = ResponseToPvpChallenge.getVariation();
        String finalResponse = response;
        typing = true;
        typingTimeProgress = 0;
        typingTimeTarget = 10 + ((int) (response.length() / 4.5 + r.nextDouble(response.length() / 15.0 + 1))) * 20;
        new BukkitRunnable() {
            @Override
            public void run() {
                if (npc == null || !npc.isSpawned() || npc.getEntity() == null || npc.getEntity().isDead()) {
                    typing = false;
                    typingTimeProgress = 0;
                    typingTimeTarget = 0;
                    cancel();
                    return;
                }
                npc.getNavigator().cancelNavigation();
                npc.getNavigator().setTarget(null);
                npc.getNavigator().cancelNavigation();
                npc.getNavigator().setPaused(true);
                typingTimeProgress += 1;
                if (typingTimeProgress < typingTimeTarget) return;
                cancel();
                if (player.isOnline()) sayTo(finalResponse, player);
                typing = false;
                typingTimeProgress = 0;
                typingTimeTarget = 0;
                npc.getNavigator().setPaused(false);
            }
        }.runTaskTimer(ServerLink.plugin, 1, 1);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    void onDeathByEnemy(CustomPlayerKillLivingEntityEvent e) {
        if (e.getEntity() == npc.getEntity()) {
            killerName = e.getPlayer().getName();
            new BukkitRunnable() {
                @Override
                public void run() {
                    killerName = "";
                }
            }.runTaskLater(ServerLink.plugin, 20 * 4);
            if (!voluntarilySpeaks || !Probability.get(20 - 0.5 * Math.min(38, PlayerManager.getOnlineAlphaPlayers().size())))
                return;
            respondTo(Action.BEING_KILLED, e.getPlayer().getUniqueId());
            saidGG = true;
            new BukkitRunnable() {
                @Override
                public void run() {
                    saidGG = false;
                }
            }.runTaskLater(ServerLink.plugin, 20 * 4);
        } else if (e.getPlayer() == npc.getEntity() && e.getEntity() instanceof Player) {
            if (!recentVictims.contains(e.getEntity().getName())) {
                recentVictims.add(e.getEntity().getName());
                if (voluntarilySpeaks && Probability.get(30 - 0.5 * Math.min(55, PlayerManager.getOnlineAlphaPlayers().size()))) {
                    respondTo(Action.KILL, e.getPlayer().getUniqueId());
                    recentVictims.remove(e.getEntity().getName());
                    return;
                }
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        recentVictims.remove(e.getEntity().getName());
                    }
                }.runTaskLater(ServerLink.plugin, 20 * 4);
            }
        }
    }

    @EventHandler
    void onJoin(CustomFullyJoinEvent e) {
        if (e.getPlayer().hasMetadata("NPC")) return;
        if (voluntarilySpeaks && Probability.get(100 * (4.0 / PlayerManager.getOnlineAlphaPlayers().size())))
            respondTo(Action.JOIN, null);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    void onChat(AsyncPlayerChatEvent e) {
        if (typing || bot.standingAfk || !npc.isSpawned() || e.isCancelled()) return;
        if (npc.getEntity() != null && e.getPlayer() == npc.getEntity()) return;
        boolean useCleverBot = false;
        String message = ChatColor.stripColor(e.getMessage().toLowerCase()).trim();
        boolean saidBotsName = false;
        for (String word : message.split(" ")) {
            word = word.replaceAll("/?", "").replaceAll("/.", "").replaceAll("/!", "");
            if (word.equals(npc.getEntity().getName().toLowerCase())) {
                saidBotsName = true;
                break;
            }
        }
        if (message.equals(npc.getEntity().getName().toLowerCase()) || message.equals(npc.getEntity().getName().toLowerCase() + "?") || message.equals(npc.getEntity().getName().toLowerCase() + "??")) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    respondTo(Action.CALL, e.getPlayer().getUniqueId());
                }
            }.runTask(ServerLink.plugin);
        } else if (((!saidGG && e.getPlayer().getName().equals(killerName)) || recentVictims.contains(e.getPlayer().getName())) && matches(message, ggForms, true)) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    recentVictims.remove(e.getPlayer().getName());
                    killerName = "";
                    respondTo(Action.GG, null);
                    saidGG = true;
                }
            }.runTask(ServerLink.plugin);
        } else if (lastHackusationDefense < TimeUtils.getCurrent() - 30 && (e.getPlayer().getName().equals(killerName) || recentVictims.contains(e.getPlayer().getName())) && matches(message, hackusationForms, true)) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    recentVictims.remove(e.getPlayer().getName());
                    killerName = "";
                    if (Probability.get(50)) {
                        respondTo(Action.HACKUSATION, e.getPlayer().getUniqueId());
                    }
                }
            }.runTask(ServerLink.plugin);
        } else if (saidBotsName) {
            if (matches(message, greetingForms, false) && !playersGreeted.contains(e.getPlayer().getUniqueId())) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        respondTo(Action.GREETING, e.getPlayer().getUniqueId());
                    }
                }.runTask(ServerLink.plugin);
                playersGreeted.add(e.getPlayer().getUniqueId());
            } else if (matches(message, hackusationForms, false)) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        respondTo(Action.HACKUSATION, e.getPlayer().getUniqueId());
                    }
                }.runTask(ServerLink.plugin);
            } else if (matches(message, pvpChallengeForms, false)) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        respondTo(Action.PVP_CHALLENGE, e.getPlayer().getUniqueId());
                    }
                }.runTask(ServerLink.plugin);
            } else if (message.contains("say ") || message.contains("type ") || message.contains("chat ") || message.contains("put ") || message.contains("write ")) {
                if (matches(message, doIfNotBotForms, false)) {
                    // player told bot to say something to prove he's not a bot
                    // determine what is to be repeated
                    String whatToSay = "";
                    boolean foundMessageStart = false;
                    for (String word : message.split(" ")) {
                        if (foundMessageStart) whatToSay = whatToSay + " " + word;
                        else if (word.equals("say") || word.equals("type") || word.equals("chat") || word.equals("put") || word.equals("write")) {
                            foundMessageStart = true;
                        }
                    }
                    for (String s : doIfNotBotForms) {
                        // remove the "if you're not a bot" part
                        whatToSay = whatToSay.replace(s, "");
                    }
                    whatToSay = whatToSay.trim().replaceAll("\"", "").replaceAll("\'", "");
                    if (whatToSay.contains(" i") || whatToSay.contains(" me") || whatToSay.contains(" my")) {
                        // player is trying to trick the bot by having it say something self-deprecating
                        useCleverBot = true;
                    } else {
                        response = whatToSay;
                        typing = true;
                        typingTimeProgress = 0;
                        typingTimeTarget = 10 + ((int) (response.length() / 4.5 + r.nextDouble(response.length() / 15.0 + 1))) * 20;
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                if (npc == null || !npc.isSpawned() || npc.getEntity() == null || npc.getEntity().isDead()) {
                                    typing = false;
                                    typingTimeProgress = 0;
                                    typingTimeTarget = 0;
                                    cancel();
                                    return;
                                }
                                npc.getNavigator().cancelNavigation();
                                npc.getNavigator().setTarget(null);
                                npc.getNavigator().cancelNavigation();
                                npc.getNavigator().setPaused(true);
                                typingTimeProgress += 1;
                                if (typingTimeProgress < typingTimeTarget) return;
                                cancel();
                                say(response);
                                typing = false;
                                typingTimeProgress = 0;
                                typingTimeTarget = 0;
                                npc.getNavigator().setPaused(false);
                                conversationEnd.put(e.getPlayer().getUniqueId(), System.currentTimeMillis() + 14000);
                            }
                        }.runTaskTimer(ServerLink.plugin, 1, 1);
                    }
                } else if (matches(message, doIfAreBotForms, false)) {
                    // player told bot: "say xxx if you're a bot"
                    respondTo(Action.SAY_IF_NOT_BOT, e.getPlayer().getUniqueId());
                } else useCleverBot = true;
            } else useCleverBot = true;
            if (!e.getPlayer().hasMetadata("NPC") && useCleverBot) {
                // End conversation in public chat if the player could be talking to someone else about the bot.
                boolean otherPlayerNameInMessage = false;
                for (String word : message.split(" ")) {
                    word = word.replaceAll("/?", "").replaceAll("/.", "").replaceAll("/!", "");
                    for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
                        if (word.equals(aPlayer.getPlayer().getName().toLowerCase()) && !word.equalsIgnoreCase(e.getPlayer().getName()) && !word.equalsIgnoreCase(npc.getEntity().getName()))
                            otherPlayerNameInMessage = true;
                    }
                }
                if (otherPlayerNameInMessage) {
                    if (conversationEnd.containsKey(e.getPlayer().getUniqueId()))
                        conversationEnd.remove(e.getPlayer().getUniqueId());
                    return;
                }
                // When someone uses the bot's name in an unknown sentence, respond with cleverbot.
                typing = true;
                typingTimeProgress = 0;
                typingTimeTarget = 100;
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                if (npc == null || !npc.isSpawned() || npc.getEntity() == null || npc.getEntity().isDead()) {
                                    typing = false;
                                    typingTimeProgress = 0;
                                    typingTimeTarget = 0;
                                    cancel();
                                    return;
                                }
                                npc.getNavigator().cancelNavigation();
                                npc.getNavigator().setTarget(null);
                                npc.getNavigator().cancelNavigation();
                                npc.getNavigator().setPaused(true);
                                typingTimeProgress += 1;
                                if (typingTimeProgress < typingTimeTarget) return;
                                cancel();
                                if (!BotChatTrait.this.response.isEmpty())
                                    say(BotChatTrait.this.response);
                                BotChatTrait.this.response = "";
                                typing = false;
                                typingTimeProgress = 0;
                                typingTimeTarget = 0;
                                npc.getNavigator().setPaused(false);
                                conversationEnd.put(e.getPlayer().getUniqueId(), System.currentTimeMillis() + 14000);
                            }
                        }.runTaskTimer(ServerLink.plugin, 1, 1);
                        String msg = message;
                        if (msg.contains(" you") || msg.contains("you ") | msg.contains("youre") || msg.contains("you're")) {
                            msg = msg.replaceAll(npc.getEntity().getName().toLowerCase() + " ", "").replaceAll(" " + npc.getEntity().getName().toLowerCase(), "");
                        } else {
                            msg = msg.replaceAll(npc.getEntity().getName().toLowerCase() + " ", "you ").replaceAll(" " + npc.getEntity().getName().toLowerCase(), " you");
                        }
                        msg = msg.replaceAll(" ikr", " I know, right?").replaceAll("ikr ", "I know, right? ");
                        msg = msg.replaceAll(" ik", " I know").replaceAll("ik ", "I know ");
                        msg = msg.replaceAll(" idk", " I don't know").replaceAll("idk ", "I don't know, ");
                        msg = msg.replaceAll(" pvp", " fighting").replaceAll("pvp ", "fighting ");
                        msg = msg.replaceAll("xd", "").replaceAll(":p", "").replaceAll("o_o", "").trim();
                        String response;
                        if (!sessions.containsKey(e.getPlayer().getUniqueId()))
                            sessions.put(e.getPlayer().getUniqueId(), new CleverBotQuery("2e1deb1e35bb9eae48317f51c23608cd", msg));
                        try {
                            sessions.get(e.getPlayer().getUniqueId()).setPhrase(msg);
                            sessions.get(e.getPlayer().getUniqueId()).sendRequest();
                            response = sessions.get(e.getPlayer().getUniqueId()).getResponse();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                            sessions.remove(e.getPlayer().getUniqueId());
                            typing = false;
                            return;
                        }
                        if (response.endsWith("!") || response.endsWith("?"))
                            response = response + "-";
                        response = response.substring(0, 1).toLowerCase() + response.substring(1, response.length());
                        BotChatTrait.this.response = response;
                        typingTimeTarget = 10 + ((int) (response.length() / 3.5 + r.nextDouble(response.length() / 15.0 + 1))) * 20;
                    }
                }.runTaskAsynchronously(ServerLink.plugin);
            }
        } else if (conversationEnd.containsKey(e.getPlayer().getUniqueId()) && !e.getPlayer().hasMetadata("NPC")) {
            if (conversationEnd.get(e.getPlayer().getUniqueId()) < System.currentTimeMillis()) {
                conversationEnd.remove(e.getPlayer().getUniqueId());
                return;
            }
            // End conversation in public chat if the player could be talking about the bot rather than to it.
            boolean otherPlayerNameInMessage = false;
            for (String word : message.split(" ")) {
                word = word.replaceAll("/?", "").replaceAll("/.", "").replaceAll("/!", "");
                for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
                    if (word.equals(aPlayer.getPlayer().getName().toLowerCase()) && !word.equalsIgnoreCase(e.getPlayer().getName()) && !word.equalsIgnoreCase(npc.getEntity().getName()))
                        otherPlayerNameInMessage = true;
                }
            }
            if (otherPlayerNameInMessage) {
                if (conversationEnd.containsKey(e.getPlayer().getUniqueId()))
                    conversationEnd.remove(e.getPlayer().getUniqueId());
                return;
            }
            // The player is in a conversation with the bot in public chat.
            if (message.contains("say ") || message.contains("type ") || message.contains("chat ") || message.contains("put ") || message.contains("write ")) {
                if (matches(message, doIfNotBotForms, false)) {
                    // player told bot to say something to prove he's not a bot
                    // determine what is to be repeated
                    String whatToSay = "";
                    boolean foundMessageStart = false;
                    for (String word : message.split(" ")) {
                        if (foundMessageStart) whatToSay = whatToSay + " " + word;
                        else if (word.equals("say") || word.equals("type") || word.equals("chat") || word.equals("put") || word.equals("write")) {
                            foundMessageStart = true;
                        }
                    }
                    for (String s : doIfNotBotForms) {
                        // remove the "if you're not a bot" part
                        whatToSay = whatToSay.replace(s, "");
                    }
                    whatToSay = whatToSay.trim().replaceAll("\"", "").replaceAll("\'", "");
                    if (whatToSay.contains(" i") || whatToSay.contains(" me") || whatToSay.contains(" my")) {
                        // player is trying to trick the bot by having it say something self-deprecating
                        // do nothing; the logic will take it cleverbot
                    } else {
                        response = whatToSay;
                        typing = true;
                        typingTimeProgress = 0;
                        typingTimeTarget = 10 + ((int) (response.length() / 4.5 + r.nextDouble(response.length() / 15.0 + 1))) * 20;
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                if (npc == null || !npc.isSpawned() || npc.getEntity() == null || npc.getEntity().isDead()) {
                                    typing = false;
                                    typingTimeProgress = 0;
                                    typingTimeTarget = 0;
                                    cancel();
                                    return;
                                }
                                npc.getNavigator().cancelNavigation();
                                npc.getNavigator().setTarget(null);
                                npc.getNavigator().cancelNavigation();
                                npc.getNavigator().setPaused(true);
                                typingTimeProgress += 1;
                                if (typingTimeProgress < typingTimeTarget) return;
                                cancel();
                                say(response);
                                typing = false;
                                typingTimeProgress = 0;
                                typingTimeTarget = 0;
                                npc.getNavigator().setPaused(false);
                                conversationEnd.put(e.getPlayer().getUniqueId(), System.currentTimeMillis() + 14000);
                            }
                        }.runTaskTimer(ServerLink.plugin, 1, 1);
                        return;
                    }
                } else if (matches(message, doIfAreBotForms, false)) {
                    // player told bot: "say xxx if you're a bot"
                    respondTo(Action.SAY_IF_NOT_BOT, e.getPlayer().getUniqueId());
                    return;
                }
            }
            if (message.equals("lol") || message.equals("xd") || message.equals(":p") ||
                    (message.contains("ha") && !message.contains(" "))) return;
            typing = true;
            typingTimeProgress = 0;
            typingTimeTarget = 100;
            new BukkitRunnable() {
                @Override
                public void run() {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            if (npc == null || !npc.isSpawned() || npc.getEntity() == null || npc.getEntity().isDead()) {
                                typing = false;
                                typingTimeProgress = 0;
                                typingTimeTarget = 0;
                                cancel();
                                return;
                            }
                            npc.getNavigator().cancelNavigation();
                            npc.getNavigator().setTarget(null);
                            npc.getNavigator().cancelNavigation();
                            npc.getNavigator().setPaused(true);
                            typingTimeProgress += 1;
                            if (typingTimeProgress < typingTimeTarget) return;
                            cancel();
                            if (!BotChatTrait.this.response.isEmpty())
                                say(BotChatTrait.this.response);
                            BotChatTrait.this.response = "";
                            typing = false;
                            typingTimeProgress = 0;
                            typingTimeTarget = 0;
                            npc.getNavigator().setPaused(false);
                            conversationEnd.put(e.getPlayer().getUniqueId(), System.currentTimeMillis() + 14000);
                        }
                    }.runTaskTimer(ServerLink.plugin, 1, 1);
                    String msg = message;
                    if (msg.contains(" you") || msg.contains("you ") | msg.contains("youre") || msg.contains("you're")) {
                        msg = msg.replaceAll(npc.getEntity().getName().toLowerCase() + " ", "").replaceAll(" " + npc.getEntity().getName().toLowerCase(), "");
                    } else {
                        msg = msg.replaceAll(npc.getEntity().getName().toLowerCase() + " ", "you ").replaceAll(" " + npc.getEntity().getName().toLowerCase(), " you");
                    }
                    msg = msg.replaceAll(" ikr", " I know, right?").replaceAll("ikr ", "I know, right? ");
                    msg = msg.replaceAll(" ik", " I know").replaceAll("ik ", "I know ");
                    msg = msg.replaceAll(" idk", " I don't know").replaceAll("idk ", "I don't know, ");
                    msg = msg.replaceAll(" pvp", " fighting").replaceAll("pvp ", "fighting ");
                    msg = msg.replaceAll("xd", "").replaceAll(":p", "").replaceAll("o_o", "").trim();
                    String response;
                    if (!sessions.containsKey(e.getPlayer().getUniqueId()))
                        sessions.put(e.getPlayer().getUniqueId(), new CleverBotQuery("2e1deb1e35bb9eae48317f51c23608cd", msg));
                    try {
                        sessions.get(e.getPlayer().getUniqueId()).setPhrase(msg);
                        sessions.get(e.getPlayer().getUniqueId()).sendRequest();
                        response = sessions.get(e.getPlayer().getUniqueId()).getResponse();
                    } catch (Exception e1) {
                        e1.printStackTrace();
                        sessions.remove(e.getPlayer().getUniqueId());
                        typing = false;
                        return;
                    }
                    if (response.endsWith("!") || response.endsWith("?"))
                        response = response + "-";
                    response = response.substring(0, 1).toLowerCase() + response.substring(1, response.length());
                    BotChatTrait.this.response = response;
                    typingTimeTarget = 10 + ((int) (response.length() / 3.5 + r.nextDouble(response.length() / 15.0 + 1))) * 20;
                }
            }.runTaskAsynchronously(ServerLink.plugin);
        }
    }

    // This event is always called asynchronously.
    // We don't need to wrap clever bot code in a runnable because the event's message is already sent before this event is called.
    @EventHandler
    void onPlayerMessageBot(CustomPlayerMessageBotEvent e) {
        if (typing || bot.standingAfk || !npc.isSpawned() || e.getBot() != bot) return;
        String message = ChatColor.stripColor(e.getMessage().toLowerCase());
        if (((!saidGG && e.getPlayer().getName().equals(killerName)) || recentVictims.contains(e.getPlayer().getName())) && matches(message, ggForms, true)) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    recentVictims.remove(e.getPlayer().getName());
                    killerName = "";
                    respondDirectlyTo(Action.GG, e.getPlayer());
                    saidGG = true;
                }
            }.runTask(ServerLink.plugin);
        } else if (lastHackusationDefense < TimeUtils.getCurrent() - 30 && (e.getPlayer().getName().equals(killerName) || recentVictims.contains(e.getPlayer().getName())) && matches(message, hackusationForms, true)) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    recentVictims.remove(e.getPlayer().getName());
                    killerName = "";
                    if (Probability.get(50)) respondDirectlyTo(Action.HACKUSATION, e.getPlayer());
                }
            }.runTask(ServerLink.plugin);
        } else {
            if (matches(message, greetingForms, false) && !playersGreeted.contains(e.getPlayer().getUniqueId())) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        respondDirectlyTo(Action.GREETING, e.getPlayer());
                    }
                }.runTask(ServerLink.plugin);
                playersGreeted.add(e.getPlayer().getUniqueId());
            } else if (matches(message, hackusationForms, false))
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        respondDirectlyTo(Action.HACKUSATION, e.getPlayer());
                    }
                }.runTask(ServerLink.plugin);
            else if (matches(message, pvpChallengeForms, false))
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        respondDirectlyTo(Action.PVP_CHALLENGE, e.getPlayer());
                    }
                }.runTask(ServerLink.plugin);
            else if (message.contains("say ") || message.contains("type ") || message.contains("chat ") || message.contains("put ") || message.contains("write ")) {
                if (matches(message, doIfNotBotForms, false)) {
                    // player told bot to say something to prove he's not a bot
                    // determine what is to be repeated
                    String whatToSay = "";
                    boolean foundMessageStart = false;
                    for (String word : message.split(" ")) {
                        if (foundMessageStart) whatToSay = whatToSay + " " + word;
                        else if (word.equals("say") || word.equals("type") || word.equals("chat") || word.equals("put") || word.equals("write")) {
                            foundMessageStart = true;
                        }
                    }
                    for (String s : doIfNotBotForms) {
                        // remove the "if you're not a bot" part
                        whatToSay = whatToSay.replace(s, "");
                    }
                    whatToSay = whatToSay.trim().replaceAll("\"", "").replaceAll("\'", "");
                    if (whatToSay.contains(" i") || whatToSay.contains(" me") || whatToSay.contains(" my")) {
                        // player is trying to trick the bot by having it say something self-deprecating
                        // do nothing; the logic will take it to cleverbot
                    } else {
                        String finalWhatToSay = whatToSay;
                        typing = true;
                        typingTimeProgress = 0;
                        typingTimeTarget = 10 + ((int) (response.length() / 4.5 + r.nextDouble(response.length() / 15.0 + 1))) * 20;
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                if (npc == null || !npc.isSpawned() || npc.getEntity() == null || npc.getEntity().isDead()) {
                                    typing = false;
                                    typingTimeProgress = 0;
                                    typingTimeTarget = 0;
                                    cancel();
                                    return;
                                }
                                npc.getNavigator().cancelNavigation();
                                npc.getNavigator().setTarget(null);
                                npc.getNavigator().cancelNavigation();
                                npc.getNavigator().setPaused(true);
                                typingTimeProgress += 1;
                                if (typingTimeProgress < typingTimeTarget) return;
                                cancel();
                                say(finalWhatToSay);
                                typing = false;
                                typingTimeProgress = 0;
                                typingTimeTarget = 0;
                                npc.getNavigator().setPaused(false);
                                conversationEnd.put(e.getPlayer().getUniqueId(), System.currentTimeMillis() + 14000);
                            }
                        }.runTaskTimer(ServerLink.plugin, 1, 1);
                    }
                } else if (matches(message, doIfAreBotForms, false)) {
                    // player told bot: "say xxx if you're a bot"
                    respondTo(Action.SAY_IF_NOT_BOT, e.getPlayer().getUniqueId());
                }
            } else {
                // When /tells the bot something unrecognized, respond with cleverbot.
                if (message.equals("lol") || message.equals("xd") || message.equals(":p") ||
                        (message.contains("ha") && !message.contains(" "))) return;
                typing = true;
                typingTimeProgress = 0;
                typingTimeTarget = 100;
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        if (npc == null || !npc.isSpawned() || npc.getEntity() == null || npc.getEntity().isDead()) {
                            typing = false;
                            typingTimeProgress = 0;
                            typingTimeTarget = 0;
                            cancel();
                            return;
                        }
                        npc.getNavigator().cancelNavigation();
                        npc.getNavigator().setTarget(null);
                        npc.getNavigator().cancelNavigation();
                        npc.getNavigator().setPaused(true);
                        typingTimeProgress += 1;
                        if (typingTimeProgress < typingTimeTarget) return;
                        cancel();
                        if (!BotChatTrait.this.response.isEmpty() && e.getPlayer().isOnline())
                            sayTo(BotChatTrait.this.response, e.getPlayer());
                        BotChatTrait.this.response = "";
                        typing = false;
                        typingTimeProgress = 0;
                        typingTimeTarget = 0;
                        npc.getNavigator().setPaused(false);
                    }
                }.runTaskTimer(ServerLink.plugin, 1, 1);
                if (message.contains(" you") || message.contains("you ") | message.contains("youre") || message.contains("you're")) {
                    message = message.replaceAll(npc.getEntity().getName().toLowerCase() + " ", "").replaceAll(" " + npc.getEntity().getName().toLowerCase(), "");
                } else {
                    message = message.replaceAll(npc.getEntity().getName().toLowerCase() + " ", "you ").replaceAll(" " + npc.getEntity().getName().toLowerCase(), " you");
                }
                message = message.replaceAll(" ikr", " I know, right?").replaceAll("ikr ", "I know, right? ");
                message = message.replaceAll(" ik", " I know").replaceAll("ik ", "I know ");
                message = message.replaceAll(" idk", " I don't know").replaceAll("idk ", "I don't know, ");
                message = message.replaceAll(" pvp", " fighting").replaceAll("pvp ", "fighting ");
                message = message.replaceAll("xd", "").replaceAll(":p", "").replaceAll("o_o", "").trim();
                String response;
                if (!sessions.containsKey(e.getPlayer().getUniqueId()))
                    sessions.put(e.getPlayer().getUniqueId(), new CleverBotQuery("2e1deb1e35bb9eae48317f51c23608cd", message));
                try {
                    sessions.get(e.getPlayer().getUniqueId()).setPhrase(message);
                    sessions.get(e.getPlayer().getUniqueId()).sendRequest();
                    response = sessions.get(e.getPlayer().getUniqueId()).getResponse();
                } catch (Exception e1) {
                    e1.printStackTrace();
                    sessions.remove(e.getPlayer().getUniqueId());
                    typing = false;
                    return;
                }
                if (response.endsWith("!") || response.endsWith("?"))
                    response = response + "-";
                response = response.substring(0, 1).toLowerCase() + response.substring(1, response.length());
                this.response = response;
                typingTimeTarget = 10 + ((int) (response.length() / 3.5 + r.nextDouble(response.length() / 15.0 + 1))) * 20;
            }
        }
    }
}

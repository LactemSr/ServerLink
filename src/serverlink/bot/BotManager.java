package serverlink.bot;

import com.google.code.chatterbotapi.ChatterBotFactory;
import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.bot.speech.conversation.ConversationManager;
import serverlink.map.MapManager;
import serverlink.network.nstatus;
import serverlink.util.Probability;

import java.util.*;

public class BotManager {
    public static final Map<String, Integer> botUsers = new HashMap<String, Integer>() {{
        // 8 SAINTs
        put("swordC", 5);
        put("Stanini", 5);
        put("Naosuki", 5);
        put("LanAlinor", 5);
        put("Mueylou", 5);
        put("BxbbelV2", 5);
        put("JaCzZ", 5);
        put("NeonWolf23", 5);
        // 12 ELITEs
        put("MrPiesek", 4);
        put("Luk_0815", 4);
        put("r3dm0h4wk1988", 4);
        put("verlohren", 4);
        put("preciousthorne", 4);
        put("xKonaah_", 4);
        put("Herliq", 4);
        put("Kjevin_", 4);
        put("denus", 4);
        put("JonathanEskil", 4);
        put("_TheDaviCp", 4);
        put("FrOsTig", 4);
        // 15 EPICs
        put("Nerevar", 3);
        put("AzlackBoti", 3);
        put("CamilaNeweray", 3);
        put("iLuvsSelina", 3);
        put("sandcat08", 3);
        put("Hai2joo", 3);
        put("iTzSprintG0D", 3);
        put("tvendeseter1", 3);
        put("Atikin", 3);
        put("Ender045", 3);
        put("MsMoltenMagma", 3);
        put("Hilfigerrr", 3);
        put("BloreX", 3);
        put("NichtReduziert", 3);
        put("LaGGeRFeeD", 3);
        // 20 BOSSes
        put("Skytrill", 2);
        put("RobbiePitts", 2);
        put("meres10", 2);
        put("AyeMB", 2);
        put("Karenwdk", 2);
        put("Mouser", 2);
        put("Autzen", 2);
        put("Liberators", 2);
        put("MrTobi", 2);
        put("mooduk", 2);
        put("Master_xD", 2);
        put("pepeologist", 2);
        put("ReymanHD", 2);
        put("keinVxrsaqer", 2);
        put("Andrii", 2);
        put("diamands", 2);
        put("stephyrawrz", 2);
        put("Dool", 2);
        put("Krava", 2);
        put("arborhawk", 2);
        // the rest have no rank
        put("veljko", 1);
        put("eriktron2001", 1);
        put("zomerchacha", 1);
        put("sonik", 1);
        put("ShadowFigure81", 1);
        put("iGamer_Plus", 1);
        put("rexkiller2000", 1);
        put("LoganPlay", 1);
        put("Hammertyme", 1);
        put("nessa94", 1);
        put("ShGabo", 1);
        put("Dqddydanieldig", 1);
        put("_Ender_Girl_", 1);
        put("mars222", 1);
        put("BabyHara", 1);
        put("Yasminyos", 1);
        put("MRzolotce", 1);
        put("minerID", 1);
        put("Bart_15", 1);
        put("Moonsided", 1);
        put("Pressinq", 1);
        put("Withering_Minerr", 1);
        put("CPacket", 1);
        put("kciredornov", 1);
        put("RaZeSemtex", 1);
        put("tomcio2001", 1);
        put("trylleklovn", 1);
        put("SussiJana", 1);
        put("HannahIsAMeme", 1);
        put("Devinzzz", 1);
        put("Vtownkiller", 1);
        put("nikita", 1);
        put("Drzzter", 1);
        put("Kezar2023", 1);
        put("darkbot30", 1);
        put("Lunacocca", 1);
        put("toxicsandwich", 1);
        put("Darrencool", 1);
        put("frodo1005", 1);
        put("Raimundo42", 1);
        put("zLeqitKokaina", 1);
        put("Ledinuke01", 1);
        put("xToqqler", 1);
        put("Snaudulys", 1);
        put("LukeysGurl", 1);
        put("CrazySaimon123", 1);
        put("Deivis_LT", 1);
        put("Dubeat", 1);
        put("panter123", 1);
        put("shiru116", 1);
        put("Dartanum", 1);
        put("Kristupoks", 1);
        put("xandervs99", 1);
        put("jonhavar", 1);
        put("karasandra", 1);
        put("Gartenarbeit", 1);
        put("flieger2000", 1);
        put("Cockico12", 1);
        put("Asttre", 1);
        put("ArnasLTUYT", 1);
        put("MrMelagis", 1);
        put("WarrioRas", 1);
        put("3blpro", 1);
        put("Tar4", 1);
        put("oo_wolf", 1);
        put("EmilyVRooney", 1);
        put("xEroZ_", 1);
        put("jnewcombe", 1);
        put("iAleexws_", 1);
        put("Durchfallnico", 1);
        put("jEnGa", 1);
        put("MadMax195", 1);
        put("Raima", 1);
        put("SomoxRO", 1);
        put("Sebi2231", 1);
        put("SniperYT", 1);
        put("MrGal", 1);
        put("iiJazela", 1);
        put("Alexaner", 1);
        put("XGamingRo", 1);
        put("Skylag69", 1);
        put("MineStarCZ", 1);
        put("MariaAlexandra", 1);
        put("_Moxito_", 1);
        put("Nastya_gon", 1);
        put("iTzNicolePvP", 1);
        put("natalia1399", 1);
        put("Coco911231", 1);
        put("_IulianSXS_", 1);
        put("LarryBoy123", 1);
        put("IaNniS", 1);
        put("hunter10", 1);
        put("valentin05", 1);
        put("BoBHD", 1);
        put("veronka248", 1);
        put("XeNoN", 1);
        put("kirby01", 1);
        put("Shaxious1", 1);
        put("DoctorGHG", 1);
        put("TheKillersYTB", 1);
        put("iGabrihell", 1);
        put("TheSteffan", 1);
        put("Emir34TR", 1);
        put("JLimons", 1);
        put("SalaminoPiccolo", 1);
        put("Woruxsz_", 1);
        put("NutzeLabyMod", 1);
        put("iMarkes", 1);
        put("zSaeyoung", 1);
        put("NorManLike", 1);
        put("Sempiternal", 1);
        put("ItsMeBumble", 1);
        put("LiebtThomi", 1);
        put("aupquit", 1);
        put("jakeys4", 1);
        put("erschoepfte", 1);
        put("AlleenPvP_YT_", 1);
        put("Anxiity", 1);
        put("ZzxDaylintjuhxzZ", 1);
        put("NichtMelina", 1);
        put("OwTh_", 1);
        put("ItzMehTank", 1);
        put("TheBry", 1);
        put("Wxnterz", 1);
        put("disconhaze", 1);
        put("EswarLard", 1);
        put("DragonBoolats", 1);
        put("CaedmonBeorn", 1);
        put("TuPapitoSasuke", 1);
        put("Zygina", 1);
        put("SnTos", 1);
        put("CanWinner_V1", 1);
        put("kruemelHD", 1);
        put("MrHassGefxhl", 1);
        put("Pqnsyy", 1);
        put("BoesesEileen", 1);
        put("ApoClapRicto", 1);
        put("Reesb_21", 1);
        put("CuzImUnlegite", 1);
        put("VquRsClient", 1);
        put("DoozLany05", 1);
        put("Leighxo", 1);
        put("CazHetEENDENVOER", 1);
        put("Licaneba", 1);
        put("Penguwaifu", 1);
        put("DjReks", 1);
        put("iiBearYT", 1);
        put("Cqry", 1);
        put("m00k1", 1);
        put("QuangHang", 1);
        put("NickGamer05", 1);
        put("Awhtistic", 1);
        put("coodexz", 1);
        put("EntenskillsFNA", 1);
        put("JannisReturns", 1);
        put("CorsairRazer", 1);
        put("zDragonPvP_FTW", 1);
        put("Txcuu", 1);
        put("SantaReacty", 1);
        put("NameWirGeladen", 1);
        put("emzie8", 1);
        put("StrafeAndComboZ", 1);
        put("Ozzy_Guzzi", 1);
        put("rhouni", 1);
        put("zuvielkarma", 1);
        put("XWhiteNemesissX", 1);
        put("umgeknickt", 1);
        put("Emilyolley", 1);
        put("OoyschesteroO", 1);
        put("Sarixed", 1);
        put("Nellie_Johnson", 1);
        put("Brandonna554", 1);
        put("Nalatte", 1);
        put("iiTzJollehMe", 1);
        put("smurndustin", 1);
        put("ankesuchtklaus", 1);
        put("iiSteffetjeh", 1);
        put("Melkehjerte", 1);
        put("ItzzzBlue", 1);
        put("oStrqfe", 1);
        put("LYSENK", 1);
        put("TheDonut7w7", 1);
        put("NeonPlayXD", 1);
        put("Aarinho", 1);
        put("xXPoPoKaKaXx", 1);
        put("Sekhment03", 1);
        put("RosiDaKittyGamer", 1);
        put("vhaen", 1);
        put("Malacka11", 1);
        put("zordayz", 1);
        put("FrenkiMC", 1);
        put("Shadyfolselover", 1);
        put("ZMCNikolai", 1);
        put("zRafaaael_", 1);
        put("Vinylux", 1);
        put("Priscillang", 1);
        put("spooGHG", 1);
        put("avrged", 1);
        put("Jeffthebeast13", 1);
        put("BielzinSkills", 1);
        put("Zarpao", 1);
        put("Fabian524", 1);
        put("Cherriuki", 1);
        put("Ztarck", 1);
        put("Psellismophobia", 1);
        put("_XLoyalLaurenX_", 1);
        put("Mannrrys", 1);
        put("NazmiHDV2", 1);
        put("JwieJ", 1);
        put("Scrow24k", 1);
        put("FSGCCR", 1);
        put("Levoamphetamine", 1);
        put("juninhoow_", 1);
        put("lilpato", 1);
        put("Sayonarachu", 1);
        put("Waterfang223", 1);
        put("kawaiinutella_", 1);
        put("TemmieTehPuntato", 1);
        put("randomlol", 1);
        put("_ItzJojoDodel_", 1);
        put("Levsito", 1);
        put("XxCuteSoulxX", 1);
        put("Woaendtie", 1);
        put("iiPumpkinBB", 1);
        put("sculpinofcourage", 1);
        put("SirCutieYuki", 1);
        put("Luxufy", 1);
        put("KatsukiYuri", 1);
        put("Juulhock_er_Bae", 1);
        put("Pepe_Lg07", 1);
        put("TacoBaeJr", 1);
        put("pinkcamo3", 1);
        put("SeyZ0", 1);
        put("mammo2002", 1);
        put("Saqra", 1);
        put("KhoyaSmaga", 1);
        put("LetzteSuppe", 1);
        put("amaury83000", 1);
        put("Warrior15_", 1);
        put("SayakaMiki_", 1);
        put("Pr7nc3", 1);
        put("MrPerson660", 1);
        put("Kreyk25", 1);
        put("PaigeMc2003", 1);
        put("ReeedGhost21", 1);
        put("Neville96", 1);
        put("Edyy_ProGamer", 1);
        put("FIFO255", 1);
        put("candlestickking", 1);
        put("elVacano7", 1);
        put("princesslulu07", 1);
        put("wolfclaw77", 1);
        put("Caylex77", 1);
        put("MulletJZ", 1);
        put("Grendal27", 1);
        put("Icktoplasm", 1);
        put("nina58781923", 1);
        put("tucker70031", 1);
        put("Destroyed789", 1);
        put("ItsGator199", 1);
        put("armard1987", 1);
        put("kecl3on", 1);
        put("Coolfire2006", 1);
    }};
    public static final Map<String, Integer> botExp = new HashMap<String, Integer>() {{
        // 8 SAINTs
        put("swordC", 554600);
        put("Stanini", 451800);
        put("Naosuki", 405540);
        put("LanAlinor", 432400);
        put("Mueylou", 160100);
        put("BxbbelV2", 551310);
        put("JaCzZ", 280680);
        put("NeonWolf23", 200270);
        // 12 ELITEs
        put("MrPiesek", 17011);
        put("Luk_0815", 71249);
        put("r3dm0h4wk1988", 17827);
        put("verlohren", 90813);
        put("preciousthorne", 85269);
        put("xKonaah_", 39218);
        put("Herliq", 13682);
        put("Kjevin_", 78109);
        put("denus", 76520);
        put("JonathanEskil", 96110);
        put("_TheDaviCp", 36812);
        put("FrOsTig", 46811);
        // 15 EPICs
        put("Nerevar", 17692);
        put("AzlackBoti", 44045);
        put("CamilaNeweray", 44458);
        put("iLuvsSelina", 41222);
        put("sandcat08", 12755);
        put("Hai2joo", 27249);
        put("iTzSprintG0D", 27650);
        put("tvendeseter1", 41249);
        put("Atikin", 47637);
        put("Ender045", 18903);
        put("MsMoltenMagma", 41821);
        put("Hilfigerrr", 218739);
        put("BloreX", 26427);
        put("NichtReduziert", 20004);
        put("LaGGeRFeeD", 17764);
        // 20 BOSSes
        put("Skytrill", 6307);
        put("RobbiePitts", 2048);
        put("meres10", 24049);
        put("AyeMB", 18867);
        put("Karenwdk", 8454);
        put("Mouser", 11767);
        put("Autzen", 26325);
        put("Liberators", 23497);
        put("MrTobi", 3547);
        put("mooduk", 22358);
        put("Master_xD", 30458);
        put("pepeologist", 11074);
        put("ReymanHD", 18743);
        put("keinVxrsaqer", 42558);
        put("Andrii", 58068);
        put("diamands", 18052);
        put("stephyrawrz", 8695);
        put("Dool", 18052);
        put("Krava", 39243);
        put("arborhawk", 6505);
        // the rest have no rank
        put("veljko", 13502);
        put("eriktron2001", 20002);
        put("zomerchacha", 15182);
        put("sonik", 3282);
        put("ShadowFigure81", 7752);
        put("iGamer_Plus", 8062);
        put("rexkiller2000", 4142);
        put("LoganPlay", 3902);
        put("Hammertyme", 2052);
        put("nessa94", 14502);
        put("ShGabo", 4352);
        put("Dqddydanieldig", 12982);
        put("_Ender_Girl_", 12062);
        put("mars222", 12242);
        put("BabyHara", 9572);
        put("Yasminyos", 5002);
        put("MRzolotce", 8512);
        put("minerID", 13502);
        put("Bart_15", 2252);
        put("Moonsided", 19442);
        put("Pressinq", 14042);
        put("Withering_Minerr", 15082);
        put("CPacket", 4002);
        put("kciredornov", 14252);
        put("RaZeSemtex", 8912);
        put("tomcio2001", 4802);
        put("trylleklovn", 21122);
        put("SussiJana", 902);
        put("HannahIsAMeme", 1142);
        put("Devinzzz", 6212);
        put("Vtownkiller", 10082);
        put("nikita", 11062);
        put("Drzzter", 12322);
        put("Kezar2023", 10232);
        put("darkbot30", 2382);
        put("Lunacocca", 6962);
        put("toxicsandwich", 13342);
        put("Darrencool", 15412);
        put("frodo1005", 2852);
        put("Raimundo42", 672);
        put("zLeqitKokaina", 6752);
        put("Ledinuke01", 14562);
        put("xToqqler", 402);
        put("Snaudulys", 752);
        put("LukeysGurl", 8062);
        put("CrazySaimon123", 442);
        put("Deivis_LT", 3842);
        put("Dubeat", 3572);
        put("panter123", 352);
        put("shiru116", 7802);
        put("Dartanum", 22142);
        put("Kristupoks", 1742);
        put("xandervs99", 9452);
        put("jonhavar", 1842);
        put("karasandra", 7082);
        put("Gartenarbeit", 4042);
        put("flieger2000", 2432);
        put("Cockico12", 4652);
        put("Asttre", 8282);
        put("ArnasLTUYT", 1752);
        put("MrMelagis", 642);
        put("WarrioRas", 1952);
        put("3blpro", 8162);
        put("Tar4", 12152);
        put("oo_wolf", 1602);
        put("EmilyVRooney", 1532);
        put("xEroZ_", 12652);
        put("jnewcombe", 1952);
        put("iAleexws_", 8912);
        put("Durchfallnico", 1502);
        put("jEnGa", 2282);
        put("MadMax195", 3782);
        put("Raima", 162);
        put("SomoxRO", 6212);
        put("Sebi2231", 1192);
        put("SniperYT", 2942);
        put("MrGal", 7902);
        put("iiJazela", 2802);
        put("Alexaner", 7382);
        put("XGamingRo", 6632);
        put("Skylag69", 352);
        put("MineStarCZ", 3602);
        put("MariaAlexandra", 992);
        put("_Moxito_", 14252);
        put("Nastya_gon", 602);
        put("iTzNicolePvP", 1562);
        put("natalia1399", 5402);
        put("Coco911231", 6382);
        put("_IulianSXS_", 9172);
        put("LarryBoy123", 9722);
        put("IaNniS", 7762);
        put("hunter10", 752);
        put("valentin05", 7932);
        put("BoBHD", 8202);
        put("veronka248", 3102);
        put("XeNoN", 9002);
        put("kirby01", 7442);
        put("Shaxious1", 122);
        put("DoctorGHG", 9602);
        put("TheKillersYTB", 9182);
        put("iGabrihell", 7982);
        put("TheSteffan", 6472);
        put("Emir34TR", 2902);
        put("JLimons", 14402);
        put("SalaminoPiccolo", 17222);
        put("Woruxsz_", 402);
        put("NutzeLabyMod", 5342);
        put("iMarkes", 3602);
        put("zSaeyoung", 9722);
        put("NorManLike", 2722);
        put("Sempiternal", 8282);
        put("ItsMeBumble", 3802);
        put("LiebtThomi", 5062);
        put("aupquit", 3752);
        put("jakeys4", 2252);
        put("erschoepfte", 3002);
        put("AlleenPvP_YT_", 17502);
        put("Anxiity", 5522);
        put("ZzxDaylintjuhxzZ", 13402);
        put("NichtMelina", 3992);
        put("OwTh_", 3242);
        put("ItzMehTank", 1002);
        put("TheBry", 6862);
        put("Wxnterz", 5882);
        put("disconhaze", 8702);
        put("EswarLard", 7402);
        put("DragonBoolats", 1202);
        put("CaedmonBeorn", 3332);
        put("TuPapitoSasuke", 17112);
        put("Zygina", 4242);
        put("SnTos", 5112);
        put("CanWinner_V1", 9662);
        put("kruemelHD", 9322);
        put("MrHassGefxhl", 452);
        put("Pqnsyy", 6562);
        put("BoesesEileen", 2222);
        put("ApoClapRicto", 7252);
        put("Reesb_21", 5602);
        put("CuzImUnlegite", 1682);
        put("VquRsClient", 4182);
        put("DoozLany05", 1442);
        put("Leighxo", 1362);
        put("CazHetEENDENVOER", 8202);
        put("Licaneba", 1222);
        put("Penguwaifu", 9802);
        put("DjReks", 1952);
        put("iiBearYT", 7542);
        put("Cqry", 1402);
        put("m00k1", 3202);
        put("QuangHang", 4932);
        put("NickGamer05", 4002);
        put("Awhtistic", 7682);
        put("coodexz", 2702);
        put("EntenskillsFNA", 222);
        put("JannisReturns", 7902);
        put("CorsairRazer", 1822);
        put("zDragonPvP_FTW", 292);
        put("Txcuu", 7802);
        put("SantaReacty", 4902);
        put("NameWirGeladen", 2752);
        put("emzie8", 8362);
        put("StrafeAndComboZ", 852);
        put("Ozzy_Guzzi", 2762);
        put("rhouni", 6722);
        put("zuvielkarma", 7682);
        put("XWhiteNemesissX", 6722);
        put("umgeknickt", 5822);
        put("Emilyolley", 1202);
        put("OoyschesteroO", 4832);
        put("Sarixed", 2522);
        put("Nellie_Johnson", 482);
        put("Brandonna554", 2602);
        put("Nalatte", 3332);
        put("iiTzJollehMe", 9722);
        put("smurndustin", 2242);
        put("ankesuchtklaus", 2722);
        put("iiSteffetjeh", 8062);
        put("Melkehjerte", 3842);
        put("ItzzzBlue", 5202);
        put("oStrqfe", 4642);
        put("LYSENK", 2062);
        put("TheDonut7w7", 4202);
        put("NeonPlayXD", 1902);
        put("Aarinho", 722);
        put("xXPoPoKaKaXx", 6202);
        put("Sekhment03", 14502);
        put("RosiDaKittyGamer", 1402);
        put("vhaen", 8602);
        put("Malacka11", 1392);
        put("zordayz", 2102);
        put("FrenkiMC", 1472);
        put("Shadyfolselover", 8052);
        put("ZMCNikolai", 2882);
        put("zRafaaael_", 6632);
        put("Vinylux", 5042);
        put("Priscillang", 4752);
        put("spooGHG", 4852);
        put("avrged", 9122);
        put("Jeffthebeast13", 1202);
        put("BielzinSkills", 7682);
        put("Zarpao", 5852);
        put("Fabian524", 3122);
        put("Cherriuki", 5772);
        put("Ztarck", 1342);
        put("Psellismophobia", 7682);
        put("_XLoyalLaurenX_", 2502);
        put("Mannrrys", 3782);
        put("NazmiHDV2", 4692);
        put("JwieJ", 1642);
        put("Scrow24k", 122);
        put("FSGCCR", 1532);
        put("Levoamphetamine", 1442);
        put("juninhoow_", 8582);
        put("lilpato", 2042);
        put("Sayonarachu", 1122);
        put("Waterfang223", 1042);
        put("kawaiinutella_", 2322);
        put("TemmieTehPuntato", 1402);
        put("randomlol", 3542);
        put("_ItzJojoDodel_", 4482);
        put("Levsito", 9202);
        put("XxCuteSoulxX", 4952);
        put("Woaendtie", 9752);
        put("iiPumpkinBB", 8522);
        put("sculpinofcourage", 1502);
        put("SirCutieYuki", 4042);
        put("Luxufy", 4402);
        put("KatsukiYuri", 9962);
        put("Juulhock_er_Bae", 4322);
        put("Pepe_Lg07", 3002);
        put("TacoBaeJr", 5482);
        put("pinkcamo3", 1922);
        put("SeyZ0", 2802);
        put("mammo2002", 5842);
        put("Saqra", 26682);
        put("KhoyaSmaga", 4962);
        put("LetzteSuppe", 2162);
        put("amaury83000", 2662);
        put("Warrior15_", 3802);
        put("SayakaMiki_", 4422);
        put("Pr7nc3", 1262);
        put("MrPerson660", 252);
        put("Kreyk25", 5322);
        put("PaigeMc2003", 1142);
        put("ReeedGhost21", 2362);
        put("Neville96", 1442);
        put("Edyy_ProGamer", 2162);
        put("FIFO255", 4202);
        put("candlestickking", 15542);
        put("elVacano7", 6452);
        put("princesslulu07", 5322);
        put("wolfclaw77", 6902);
        put("Caylex77", 6022);
        put("MulletJZ", 562);
        put("Grendal27", 2752);
        put("Icktoplasm", 3152);
        put("nina58781923", 3152);
        put("tucker70031", 8582);
        put("Destroyed789", 1562);
        put("ItsGator199", 4182);
        put("armard1987", 7702);
        put("kecl3on", 4412);
        put("Coolfire2006", 1162);
    }};
    public static final List<String> usedUUIDS = new ArrayList<>();
    static final ChatterBotFactory chatBotFactory = new ChatterBotFactory();
    private static final List<Bot> bots = new ArrayList<>();
    private static final List<String> disabledBotNames = new ArrayList<>();
    private static final SplittableRandom r = new SplittableRandom();
    public static boolean creatingOrDeletingBots = false;
    private static int nextNpcId = 700;
    private static int botsToManage = 0;
    // Dynamic bot creation means the server will create and destroy up to 30 bots to keep the total server population at a minimum of 15%.
    private static boolean dynamicBotCreation = false;
    private static Location botSpawnLoc = null;

    public static void setup() {
        // Randomly make bots join and leave like normal players would
        new BukkitRunnable() {
            @Override
            public void run() {
                if (!Probability.get(100 * ((Math.min(10, bots.size() * 0.5)) / (60.0 * 7.0))) || creatingOrDeletingBots)
                    return;
                Bot bot = null;
                for (int i = 0; i < 50; i++) {
                    bot = bots.get(r.nextInt(bots.size()));
                    if (bot.getNpc().isSpawned() && bot.getNpc().getTrait(BotBaseTrait.class).ready) break;
                }
                if (!bot.getNpc().isSpawned() || !bot.getNpc().getTrait(BotBaseTrait.class).ready) return;
                bot.destroy(true);
                creatingOrDeletingBots = true;
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        creatingOrDeletingBots = false;
                    }
                }.runTaskLater(ServerLink.plugin, 20);
            }
        }.runTaskTimer(ServerLink.plugin, 20, 20);
        new BukkitRunnable() {
            @Override
            public void run() {
                if (bots.size() >= botsToManage && !dynamicBotCreation) return;
                if (creatingOrDeletingBots) return;
                Location spawnLocation = BotManager.getBotSpawnLoc();
                if (spawnLocation == null) {
                    if (nstatus.isGame())
                        spawnLocation = MapManager.getCurrentGameMap().getWorld().getSpawnLocation();
                    else spawnLocation = MapManager.getLobbyMap().getWorld().getSpawnLocation();
                }
                String username = (String) botUsers.keySet().toArray()[r.nextInt(botUsers.keySet().size())];
                while (BotManager.isBotNameDisabled(username)) {
                    username = (String) botUsers.keySet().toArray()[r.nextInt(botUsers.keySet().size())];
                }
                // spawn a bot
                new Bot(username, spawnLocation, true);
                creatingOrDeletingBots = true;
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        creatingOrDeletingBots = false;
                    }
                }.runTaskLater(ServerLink.plugin, 20);
            }
        }.runTaskTimer(ServerLink.plugin, 20 * 10, 20 * 10);

        // Make bots start conversations with each other
        new BukkitRunnable() {
            @Override
            public void run() {
                if (bots.size() < 7) return;
                if (ConversationManager.isConversationActive()) {
                    if (ConversationManager.wasLastLineSaid())
                        ConversationManager.scheduleNextLine();
                } else if (Probability.get(3)) {
                    ConversationManager.startConversation();
                }
            }
        }.runTaskTimer(ServerLink.plugin, 1, 10);

        // Destroy bots that despawn and remove from count but don't remove from tab or bots list
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Bot bot : bots) {
                    if (bot.getNpc().isSpawned() && bot.getNpc().getEntity() != null) continue;
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            if ((!bot.getNpc().isSpawned() || bot.getNpc().getEntity() == null) && !bot.destroying) {
                                bot.destroy(true);
                            }
                        }
                    }.runTaskLater(ServerLink.plugin, 20 * 5);
                }
            }
        }.runTaskTimer(ServerLink.plugin, 20 * 60, 20 * 60);
    }

    public static List<Bot> getBots() {
        return bots;
    }

    public static int getNumOfBots() {
        return bots.size();
    }

    public static void setFireTicks(Player player, int fireTicks) {
        for (Bot bot : bots) {
            if (bot.getNpc().isSpawned() && bot.getNpc().getEntity() == player) {
                bot.getNpc().getTrait(BotBaseTrait.class).fireTicks = fireTicks;
                return;
            }
        }
    }

    public static int getBotsToManage() {
        return botsToManage;
    }

    public static void setBotsToManage(int botsToManage) {
        BotManager.botsToManage = botsToManage;
    }

    static void disableBotName(String name) {
        if (!disabledBotNames.contains(name)) disabledBotNames.add(name);
    }

    static void enableBotName(String name) {
        disabledBotNames.remove(name);
    }

    public static boolean isBotNameDisabled(String name) {
        return disabledBotNames.contains(name);
    }

    public static void disable() {
        for (Bot bot : new ArrayList<>(bots)) {
            try {
                bot.destroy(false);
            } catch (Exception ignored) {
            }
        }
        for (NPC npc : CitizensAPI.getNPCRegistry().sorted()) {
            try {
                npc.destroy();
            } catch (Exception ignored) {
            }
        }
    }

    public static Location getBotSpawnLoc() {
        return botSpawnLoc;
    }

    public static void setBotSpawnLoc(Location botSpawnLoc) {
        BotManager.botSpawnLoc = botSpawnLoc;
    }

    public static boolean isDynamicBotCreationEnabled() {
        return dynamicBotCreation;
    }

    public static void setDynamicBotCreationEnabled() {
        dynamicBotCreation = true;
        botsToManage = 0;
    }

    public static void setDynamicBotCreationDisabled() {
        dynamicBotCreation = false;
        botsToManage = bots.size();
    }

    static int nextNpcId() {
        nextNpcId++;
        return nextNpcId;
    }

    static UUID nextUUID() {
        String uuid = randomNumericalUUID();
        while (usedUUIDS.contains(uuid)) uuid = randomNumericalUUID();
        usedUUIDS.add(uuid);
        return UUID.fromString(uuid);
    }

    private static String randomNumericalUUID() {
        return ((Integer) r.nextInt(11111111, 99999999)).toString()
                + "-" + ((Integer) r.nextInt(1111, 9999)).toString()
                + "-" + ((Integer) r.nextInt(1111, 9999)).toString()
                + "-" + ((Integer) r.nextInt(1111, 9999)).toString()
                + "-" + ((Integer) r.nextInt(111111111, 999999999)).toString()
                + ((Integer) r.nextInt(111, 999)).toString();
    }
}

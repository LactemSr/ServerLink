package serverlink.bot;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityDestroy;
import net.minecraft.server.v1_8_R3.PacketPlayOutPlayerInfo;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.customevent.CustomLeaveEvent;
import serverlink.network.ncount;
import serverlink.npc.NpcEventListener;
import serverlink.player.Permissions;
import serverlink.player.damage.PlayerDamage;
import serverlink.player.interaction.PlayerInteraction;
import serverlink.player.team.TeamColor;
import serverlink.player.team.TeamManager;

import java.util.Collections;
import java.util.ConcurrentModificationException;
import java.util.List;
import java.util.stream.Collectors;

public class Bot {
    public boolean standingAfk = false;
    public boolean destroying = false;
    boolean announceJoin;
    Location spawnLoc;
    private NPC npc;
    private String username;


    public Bot(String username, Location loc, boolean announceJoin) {
        this.username = username;
        this.announceJoin = announceJoin;
        spawnLoc = loc;
        BotManager.disableBotName(username);
        npc = CitizensAPI.getNPCRegistry().createNPC(EntityType.PLAYER, BotManager.nextUUID(), BotManager.nextNpcId(), username);
        npc.addTrait(new BotBaseTrait(this));
        npc.addTrait(new BotChatTrait(this));
        npc.spawn(loc);
    }

    public String getUsername() {
        return username;
    }

    public NPC getNpc() {
        return npc;
    }

    public void destroy(boolean callLeaveEvent) {
        destroying = true;
        if (!callLeaveEvent) {
            try {
                npc.getTrait(BotBaseTrait.class).ready = false;
            } catch (Exception ignored) {
            }
            try {
                BotManager.enableBotName(username);
                BotManager.getBots().remove(Bot.this);
                AlphaPlayer aPlayer = PlayerManager.get(npc.getUniqueId());
                Player botPlayer = aPlayer.getPlayer();
                aPlayer.removeRankTag();
                PlayerInteraction.leave(botPlayer);
                if (aPlayer.getTeam() != null) {
                    TeamManager.teamWaitLists.get(aPlayer.getTeam().getTeamColor()).remove(botPlayer);
                    // Update deficits and try to allow someone into the leaving player's team
                    TeamManager.updateTeamDeficits();
                    List<TeamColor> teamsWithWaitLists = TeamManager.teamWaitLists.keySet().stream().filter(teamColor -> !TeamManager.teamWaitLists.get(teamColor).isEmpty()).collect(Collectors.toList());
                    if (!teamsWithWaitLists.isEmpty()) {
                        Collections.shuffle(teamsWithWaitLists);
                        for (TeamColor teamColor : teamsWithWaitLists) {
                            Player waiter = TeamManager.teamWaitLists.get(teamColor).get(0);
                            if (TeamManager.getTeamDeficit(TeamManager.getTeam(waiter).getTeamColor())
                                    >= TeamManager.getTeamDeficit(teamColor)) continue;
                            TeamManager.teamWaitLists.get(teamColor).remove(waiter);
                            PlayerManager.get(waiter).setTeam(TeamManager.getTeam(teamColor));
                            PlayerManager.get(waiter).updateNameFormatting();
                            break;
                        }
                    }
                }
                try {
                    PlayerDamage.leave(botPlayer);
                } catch (Exception ignored) {
                }
                NpcEventListener.cooldowns.remove(botPlayer);
            } catch (Exception e) {
            }
            try {
                PlayerManager.onlineAlphaPlayers.remove(PlayerManager.get(npc.getTrait(BotBaseTrait.class).player));
            } catch (Exception ignored) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        try {
                            PlayerManager.onlineAlphaPlayers.remove(PlayerManager.get(npc.getTrait(BotBaseTrait.class).player));
                        } catch (Exception ignored) {
                        }
                    }
                }.runTaskLater(ServerLink.plugin, 20);
            }
            try {
                PlayerManager.alphaPlayers.remove(npc.getTrait(BotBaseTrait.class).player.getUniqueId());
            } catch (Exception ignored) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        try {
                            PlayerManager.alphaPlayers.remove(npc.getTrait(BotBaseTrait.class).player.getUniqueId());
                        } catch (Exception ignored) {
                        }
                    }
                }.runTaskLater(ServerLink.plugin, 20);
            }
            try {
                if (npc.getTrait(BotBaseTrait.class).ready)
                    npc.despawn();
            } catch (Exception ignored) {
            }
            try {
                npc.destroy();
            } catch (Exception ignored) {
            }
            try {
                CitizensAPI.getNPCRegistry().deregister(npc);
            } catch (Exception ignored) {
            }
            ncount.decrCount();
            return;
        }
        if (!npc.isSpawned() || npc.getEntity() == null || !npc.getTrait(BotBaseTrait.class).ready) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (!npc.isSpawned() || npc.getEntity() == null || !npc.getTrait(BotBaseTrait.class).ready) {
                        System.out.println("Bot destroying before fully enabled.");
                    }
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            try {
                                if (npc == null || npc.getEntity() == null) return;
                                for (Player p : Bukkit.getOnlinePlayers()) {
                                    ((CraftPlayer) p).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityDestroy(npc.getEntity().getEntityId()));
                                    try {
                                        ((CraftPlayer) p).getHandle().playerConnection.sendPacket(new PacketPlayOutPlayerInfo(PacketPlayOutPlayerInfo.EnumPlayerInfoAction.REMOVE_PLAYER, ((CraftPlayer) npc.getEntity()).getHandle()));
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        }
                    }.runTaskLater(ServerLink.plugin, 20);
                    try {
                        BotManager.enableBotName(username);
                        BotManager.getBots().remove(Bot.this);
                        BotManager.getBots().remove(Bot.this);
                        AlphaPlayer aPlayer = PlayerManager.get(npc.getUniqueId());
                        Player botPlayer = aPlayer.getPlayer();
                        Bukkit.getPluginManager().callEvent(new CustomLeaveEvent(botPlayer));
                        String messages = chat.getJoinLeaveMessagesStatus();
                        if (!messages.equals("none")) {
                            if (messages.equals("all") || (messages.equals("donors") && aPlayer.getRank() > 1))
                                if (aPlayer.getRank() > 1)
                                    Bukkit.getServer().broadcastMessage(chat.color("&8<&c&lQuit&r&8> " +
                                            Permissions.getRankName(aPlayer.getRank()) + "&r " + botPlayer.getDisplayName()));
                                else
                                    Bukkit.getServer().broadcastMessage(chat.color("&8<&c&lQuit&r&8> &r" + botPlayer.getDisplayName()));
                        }
                        npc.getTrait(BotBaseTrait.class).ready = false;
                        aPlayer.removeRankTag();
                        PlayerManager.alphaPlayers.remove(npc.getUniqueId());
                        PlayerManager.onlineAlphaPlayers.remove(aPlayer);
                        PlayerManager.onlineAlphaPlayers.remove(aPlayer);
                        PlayerInteraction.leave(botPlayer);
                        if (aPlayer.getTeam() != null) {
                            TeamManager.teamWaitLists.get(aPlayer.getTeam().getTeamColor()).remove(botPlayer);
                            // Update deficits and try to allow someone into the leaving player's team
                            TeamManager.updateTeamDeficits();
                            List<TeamColor> teamsWithWaitLists = TeamManager.teamWaitLists.keySet().stream().filter(teamColor -> !TeamManager.teamWaitLists.get(teamColor).isEmpty()).collect(Collectors.toList());
                            if (!teamsWithWaitLists.isEmpty()) {
                                Collections.shuffle(teamsWithWaitLists);
                                for (TeamColor teamColor : teamsWithWaitLists) {
                                    Player waiter = TeamManager.teamWaitLists.get(teamColor).get(0);
                                    if (TeamManager.getTeamDeficit(TeamManager.getTeam(waiter).getTeamColor())
                                            >= TeamManager.getTeamDeficit(teamColor)) continue;
                                    TeamManager.teamWaitLists.get(teamColor).remove(waiter);
                                    PlayerManager.get(waiter).setTeam(TeamManager.getTeam(teamColor));
                                    PlayerManager.get(waiter).updateNameFormatting();
                                    break;
                                }
                            }
                        }

                        try {
                            PlayerDamage.leave(botPlayer);
                        } catch (Exception ignored) {
                        }
                        NpcEventListener.cooldowns.remove(botPlayer);
                        try {
                            npc.despawn();
                        } catch (ConcurrentModificationException ignored) {
                        }
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                npc.destroy();
                            }
                        }.runTaskLater(ServerLink.plugin, 10);
                        CitizensAPI.getNPCRegistry().deregister(npc);
                        ncount.decrCount();
                    } catch (Exception ignored) {
                    }
                }
            }.runTaskLater(ServerLink.plugin, 20);
            return;
        }
        BotManager.enableBotName(username);
        Player botPlayer = npc.getTrait(BotBaseTrait.class).player;
        AlphaPlayer aPlayer = PlayerManager.get(botPlayer.getUniqueId());
        Bukkit.getPluginManager().callEvent(new CustomLeaveEvent(botPlayer));
        String messages = chat.getJoinLeaveMessagesStatus();
        if (!messages.equals("none")) {
            if (messages.equals("all") || (messages.equals("donors") && aPlayer.getRank() > 1))
                if (aPlayer.getRank() > 1)
                    Bukkit.getServer().broadcastMessage(chat.color("&8<&c&lQuit&r&8> " +
                            Permissions.getRankName(aPlayer.getRank()) + "&r " + botPlayer.getDisplayName()));
                else
                    Bukkit.getServer().broadcastMessage(chat.color("&8<&c&lQuit&r&8> &r" + botPlayer.getDisplayName()));
        }
        npc.getTrait(BotBaseTrait.class).ready = false;
        BotManager.getBots().remove(this);
        BotManager.getBots().remove(this);
        aPlayer.removeRankTag();
        PlayerManager.alphaPlayers.remove(botPlayer.getUniqueId());
        PlayerManager.onlineAlphaPlayers.remove(aPlayer);
        PlayerManager.onlineAlphaPlayers.remove(aPlayer);
        PlayerInteraction.leave(botPlayer);
        if (aPlayer.getTeam() != null) {
            TeamManager.teamWaitLists.get(aPlayer.getTeam().getTeamColor()).remove(botPlayer);
            // Update deficits and try to allow someone into the leaving player's team
            TeamManager.updateTeamDeficits();
            List<TeamColor> teamsWithWaitLists = TeamManager.teamWaitLists.keySet().stream().filter(teamColor -> !TeamManager.teamWaitLists.get(teamColor).isEmpty()).collect(Collectors.toList());
            if (!teamsWithWaitLists.isEmpty()) {
                Collections.shuffle(teamsWithWaitLists);
                for (TeamColor teamColor : teamsWithWaitLists) {
                    Player waiter = TeamManager.teamWaitLists.get(teamColor).get(0);
                    if (TeamManager.getTeamDeficit(TeamManager.getTeam(waiter).getTeamColor())
                            >= TeamManager.getTeamDeficit(teamColor)) continue;
                    TeamManager.teamWaitLists.get(teamColor).remove(waiter);
                    PlayerManager.get(waiter).setTeam(TeamManager.getTeam(teamColor));
                    PlayerManager.get(waiter).updateNameFormatting();
                    break;
                }
            }
        }
        try {
            PlayerDamage.leave(botPlayer);
        } catch (Exception ignored) {
        }
        NpcEventListener.cooldowns.remove(botPlayer);
        try {
            npc.despawn();
        } catch (ConcurrentModificationException ignored) {
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                npc.destroy();
            }
        }.runTaskLater(ServerLink.plugin, 10);
        CitizensAPI.getNPCRegistry().deregister(npc);
        ncount.decrCount();
    }
}

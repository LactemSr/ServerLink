package serverlink.bot.movement;

import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.trait.trait.Inventory;
import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.customevent.CustomLeaveEvent;
import serverlink.customevent.CustomPlayerDeathEvent;
import serverlink.customevent.CustomPlayerKillLivingEntityEvent;
import serverlink.network.nstatus;
import serverlink.player.team.Team;
import serverlink.player.team.TeamColor;
import serverlink.round.Round;
import serverlink.util.Probability;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class SkybattleBotMovement implements Listener, Round {
    private static Map<Team, Player> teamBuilders = new HashMap<>();

    // Return true if this method handled the bot's movement
    static boolean walkAround(BotMovement movement) {
        if (!nstatus.isGame()) return false;
        Player player = movement.getPlayer();
        NPC npc = movement.getNpc();
        AlphaPlayer aPlayer = movement.getAlphaPlayer();
        // If the bot is spectating or waiting to respawn, make it not move
        if (player.getAllowFlight()) return true;
        // If the bot's team doesn't have a builder, make bot its team builder
        teamBuilders.putIfAbsent(aPlayer.getTeam(), player);
        // If the bot is not the team's builder, let it walk around normally or go to mid
        Location mid = (Location) aPlayer.getData("mapMid").getValue();
        if (teamBuilders.get(aPlayer.getTeam()) != player) {
            if (Probability.get(2)) {
                aPlayer.getData("walkingToMid").setValue(true);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        aPlayer.getData("walkingToMid").setValue(null);
                    }
                }.runTaskLater(ServerLink.plugin, 20 * 5);
            }
            if (aPlayer.getData("walkingToMid").getValue() != null) {
                npc.getNavigator().setTarget(mid);
                movement.setWalkGoal(mid);
            } else {
                return false;
            }
        }
        switchToWool(npc);
        // If the bot is already at mid, let it walk around normally
        if (player.getLocation().distanceSquared(mid) < 3 * 3) return false;
        Location pathLoc = player.getLocation();
        pathLoc.setY(mid.getY());
        pathLoc.add(0, -0.3, 0);
        pathLoc = nextPathLoc(pathLoc, mid);
        if (pathLoc.getBlock().getType() == Material.AIR && pathLoc.clone().add(0, -1, 0).getBlock().getType() == Material.AIR) {
            // If the block in between the bot and the middle is void, look down and place wool
            aPlayer.getData("walkToVoidBegun").setValue(null);
            npc.getNavigator().setTarget(player.getLocation());
            movement.setWalkGoal(player.getLocation());
            npc.faceLocation(pathLoc);
            DyeColor dye = DyeColor.RED;
            if (aPlayer.getTeam().getTeamColor() == TeamColor.YELLOW) dye = DyeColor.YELLOW;
            if (aPlayer.getTeam().getTeamColor() == TeamColor.GREEN) dye = DyeColor.GREEN;
            if (aPlayer.getTeam().getTeamColor() == TeamColor.BLUE) dye = DyeColor.BLUE;
            pathLoc.getBlock().setType(Material.WOOL);
            pathLoc.getBlock().setData(dye.getData());
            Bukkit.getPluginManager().callEvent(new BlockPlaceEvent(pathLoc.getBlock(), pathLoc.getBlock().getState(), pathLoc.getBlock(), player.getItemInHand(), player, true));
            return true;
        }
        // Find the next void block between the bot and the map's mid
        Location nextVoid = player.getLocation();
        nextVoid.setY(mid.getY());
        nextVoid.add(0, -0.3, 0);
        while (true) {
            nextVoid = nextPathLoc(nextVoid.clone(), mid);
            if (nextVoid.getBlock().getType() == Material.AIR && nextVoid.clone().add(0, -1, 0).getBlock().getType() == Material.AIR)
                break;
            // Let the bot walk around normally if all blocks have been built
            if (nextVoid.distanceSquared(mid) < 3 * 3) return false;
        }
        // If the bot has been "walking" to the next void block for too long, just place it
        if ((long) aPlayer.getData("walkToVoidBegun", System.currentTimeMillis()).getValue() < System.currentTimeMillis() - 6000) {
            aPlayer.getData("walkToVoidBegun").setValue(null);
            npc.getNavigator().setTarget(player.getLocation());
            movement.setWalkGoal(player.getLocation());
            npc.faceLocation(pathLoc);
            DyeColor dye = DyeColor.RED;
            if (aPlayer.getTeam().getTeamColor() == TeamColor.YELLOW) dye = DyeColor.YELLOW;
            if (aPlayer.getTeam().getTeamColor() == TeamColor.GREEN) dye = DyeColor.GREEN;
            if (aPlayer.getTeam().getTeamColor() == TeamColor.BLUE) dye = DyeColor.BLUE;
            nextVoid.getBlock().setType(Material.WOOL);
            nextVoid.getBlock().setData(dye.getData());
            Bukkit.getPluginManager().callEvent(new BlockPlaceEvent(pathLoc.getBlock(), pathLoc.getBlock().getState(), pathLoc.getBlock(), player.getItemInHand(), player, true));
            nextVoid.add(0, 1, 0);
            Bukkit.getPluginManager().callEvent(new PlayerTeleportEvent(player, player.getLocation(), nextVoid));
            player.teleport(nextVoid);
            return true;
        }
        // Otherwise, walk toward the next void block
        npc.getNavigator().setTarget(nextVoid);
        movement.setWalkGoal(nextVoid);
        if (aPlayer.getData("walkToVoidBegun").getValue() == null)
            aPlayer.getData("walkToVoidBegun").setValue(System.currentTimeMillis());
        return true;
    }

    private static Location nextPathLoc(Location pathLoc, Location destination) {
        // Move x coordinate up to one block closer to the destination
        if (destination.getX() > pathLoc.getX() + 1) {
            pathLoc.setX(((int) (pathLoc.getX() + 1)) - 0.5);
            pathLoc.add(1, 0, 0);
        } else if (destination.getX() < pathLoc.getX() - 1) {
            pathLoc.setX(((int) pathLoc.getX()) + 0.5);
            pathLoc.add(-1, 0, 0);
        } else {
            pathLoc.setX(destination.getX());
        }
        // Move z coordinate up to one block closer to the destination
        if (destination.getZ() > pathLoc.getZ() + 1) {
            pathLoc.setZ(((int) (pathLoc.getZ() + 1)) - 0.5);
            pathLoc.add(0, 0, 1);
        } else if (destination.getZ() < pathLoc.getZ() - 1) {
            pathLoc.setZ(((int) pathLoc.getZ()) + 0.5);
            pathLoc.add(0, 0, -1);
        } else {
            pathLoc.setZ(destination.getZ());
        }
        pathLoc.setY(destination.getY() - 0.3);
        return pathLoc;
    }

    private static void switchToWool(NPC npc) {
        int i = 0;
        Inventory inv = npc.getTrait(Inventory.class);
        ItemStack[] items = inv.getContents();
        ItemStack held = items[0] == null ? null : items[0].clone();
        if (held != null && held.getType() == Material.WOOL) return;
        while (i < items.length - 1) {
            i++;
            if (items[i] != null && items[i].getType() == Material.WOOL) {
                items[0] = items[i];
                items[i] = held == null ? new ItemStack(Material.AIR) : held;
                inv.setContents(items);
                return;
            }
        }
    }

    @Override
    public void onLobbyCountdownStart() {

    }

    @Override
    public void onLobbyCountdownCancel() {

    }

    @Override
    public void onMapCountdownStart() {

    }

    @Override
    public void onMapCountdownCancel() {

    }

    @Override
    public void onGameStart() {
        teamBuilders.clear();
    }

    @Override
    public void onEndCountdownStart() {

    }

    @Override
    public void onRoundEnd() {

    }

    // Get another builder when the current one dies or leaves

    @EventHandler
    private void onBuilderDeath(CustomPlayerDeathEvent e) {
        for (Team team : new HashSet<>(teamBuilders.keySet())) {
            if (teamBuilders.get(team) == e.getPlayer()) teamBuilders.remove(team);
        }
    }

    @EventHandler
    private void onBuilderDeath(CustomPlayerKillLivingEntityEvent e) {
        if (e.getEntity().getType() != EntityType.PLAYER) return;
        for (Team team : new HashSet<>(teamBuilders.keySet())) {
            if (teamBuilders.get(team) == e.getEntity()) teamBuilders.remove(team);
        }
    }

    @EventHandler
    private void onBuilderLeave(CustomLeaveEvent e) {
        for (Team team : new HashSet<>(teamBuilders.keySet())) {
            if (teamBuilders.get(team) == e.getPlayer()) teamBuilders.remove(team);
        }
    }
}

package serverlink.bot.movement;

import net.citizensnpcs.api.npc.NPC;
import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.bot.BotBaseTrait;
import serverlink.util.Probability;

class KitpvpBotMovement {
    // Return true if this method handled the bot's movement
    static boolean walkAround(BotMovement movement) {
        Player player = movement.getPlayer();
        BotBaseTrait base = movement.getBase();
        if (!((!base.pvpEnabledLastRun || System.currentTimeMillis() - base.timePvpEnabled < 2000) && player.getLocation().getY() > player.getWorld().getSpawnLocation().subtract(0, 3.5, 0).getY()))
            return false;
        NPC npc = movement.getNpc();
        AlphaPlayer aPlayer = movement.getAlphaPlayer();
        // If the bot is in spawn without a target exit location, make it walk and then boost it out of spawn
        if (aPlayer.getData("kitpvpSpawnExitLoc").getValue() != null) {
            Location target = (Location) aPlayer.getData("kitpvpSpawnExitLoc").getValue();
            if ((Math.abs(target.getX() - player.getLocation().getX()) > 4 || Math.abs(target.getZ() - player.getLocation().getZ()) > 4) && aPlayer.getData("exitingKitpvpSpawn").getValue() == null) {
                npc.getNavigator().setTarget(target);
            } else if (aPlayer.getData("exitingKitpvpSpawn").getValue() == null) {
                npc.getNavigator().cancelNavigation();
                aPlayer.getData("exitingKitpvpSpawn").setValue(true);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        player.setVelocity(player.getLocation().subtract(player.getWorld().getSpawnLocation()).toVector().normalize().multiply(0.3).setY(0.1));
                    }
                }.runTaskLater(ServerLink.plugin, 10);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        if (player != null && aPlayer != null)
                            aPlayer.getData("exitingKitpvpSpawn").setValue(null);
                    }
                }.runTaskLater(ServerLink.plugin, 20);
            }
            return true;
        }
        // If the bot is still walking along its path to the spawn exit location,
        // set its target again or change direction
        if (movement.WalkPositiveX()) {
            if (Probability.get(50) || (npc.getNavigator().getTargetAsLocation() != null && npc.getNavigator().getTargetAsLocation().distanceSquared(player.getWorld().getSpawnLocation().add(12.5, 0, 0)) < 4 * 4))
                npc.getNavigator().setTarget(player.getWorld().getSpawnLocation().add(11, -2, 0));
            else
                npc.getNavigator().setTarget(player.getWorld().getSpawnLocation().add(0, -2, 11));
        } else {
            if (Probability.get(50) || (npc.getNavigator().getTargetAsLocation() != null && npc.getNavigator().getTargetAsLocation().distanceSquared(player.getWorld().getSpawnLocation().add(-12.5, 0, 0)) < 4 * 4))
                npc.getNavigator().setTarget(player.getWorld().getSpawnLocation().add(-11, -2, 0));
            else
                npc.getNavigator().setTarget(player.getWorld().getSpawnLocation().add(0, -2, -11));
        }
        aPlayer.getData("kitpvpSpawnExitLoc").setValue(npc.getNavigator().getTargetAsLocation());
        new BukkitRunnable() {
            @Override
            public void run() {
                if (player != null && aPlayer != null)
                    aPlayer.getData("kitpvpSpawnExitLoc").setValue(null);
            }
        }.runTaskLater(ServerLink.plugin, 140);
        return true;
    }
}

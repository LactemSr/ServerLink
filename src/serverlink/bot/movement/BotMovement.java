package serverlink.bot.movement;

import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.npc.entity.EntityHumanNPC;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerTeleportEvent;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.bot.Bot;
import serverlink.bot.BotBaseTrait;
import serverlink.network.nstatus;
import serverlink.player.damage.PlayerDamage;
import serverlink.server.ServerType;
import serverlink.util.Probability;
import serverlink.util.RayTrace;

public class BotMovement {
    private Bot bot;
    private BotBaseTrait base;
    private Player player;
    private AlphaPlayer aPlayer;
    private NPC npc;
    private long runStarted = System.currentTimeMillis();
    private long lastJump = System.currentTimeMillis();
    private boolean correctingSpin = false;
    private Location pvp_goal = new Location(null, 0, 0, 0);
    private Location walk_goal = new Location(null, 0, 0, 0);
    private boolean walkPositiveX = true;
    private boolean walkPositiveZ = true;
    private boolean movingAfterWaterEnter = false;
    private boolean fleeingCombat = false;
    private boolean gettingAwayFromSpawn = false;


    public BotMovement(Bot bot, BotBaseTrait base, Player player) {
        this.bot = bot;
        this.base = base;
        this.player = player;
        this.aPlayer = PlayerManager.get(player);
        this.npc = base.getNPC();
        // Detect and stop spinning
        aPlayer.getData("botRotation").setValue(0.0);
        aPlayer.getData("botRotationTime").setValue(0);
        aPlayer.getData("spinDetectionDir").setValue(player.getLocation().getDirection());
        new BukkitRunnable() {
            @Override
            public void run() {
                if (!base.ready || !npc.isSpawned() || bot.destroying) {
                    cancel();
                    return;
                }
                if ((int) aPlayer.getData("botRotationTime").getValue() == 40) {
                    aPlayer.getData("botRotationTime").setValue(0);
                    aPlayer.getData("botRotation").setValue(0.0);
                }
                aPlayer.getData("botRotationTime").setValue((int) aPlayer.getData("botRotationTime").getValue() + 5);
                aPlayer.getData("botRotation").setValue((double) aPlayer.getData("botRotation").getValue() + Math.min(90, base.getCombat().horizontalAngle(player.getLocation().getDirection(), (Vector) aPlayer.getData("spinDetectionDir").getValue())));
                if ((double) aPlayer.getData("botRotation").getValue() >= 360.0) {
                    // spinning detected
                    if (npc.getNavigator().isNavigating())
                        npc.faceLocation(npc.getNavigator().getTargetAsLocation());
                    npc.getNavigator().cancelNavigation();
                    walk_goal = player.getLocation();
                    pvp_goal = player.getLocation();
                    aPlayer.getData("botRotationTime").setValue(0);
                    aPlayer.getData("botRotation").setValue(0.0);
                    correctingSpin = true;
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            correctingSpin = false;
                        }
                    }.runTaskLater(ServerLink.plugin, 11);
                }
                aPlayer.getData("spinDetectionDir").setValue(player.getLocation().getDirection());
            }
        }.runTaskTimer(ServerLink.plugin, 10, 5);
        // Trigger move/teleport events for the bot twice a second
        new BukkitRunnable() {
            @Override
            public void run() {
                if (!base.ready || !npc.isSpawned() || bot.destroying) {
                    cancel();
                    return;
                }
                try {
                    if (player.getLocation().distanceSquared(base.lastLoc) > 0.4) {
                        Bukkit.getPluginManager().callEvent(new PlayerTeleportEvent(player, base.lastLoc, player.getLocation()));
                    } else {
                        Bukkit.getPluginManager().callEvent(new PlayerMoveEvent(player, base.lastLoc, player.getLocation()));
                    }
                    player.getNearbyEntities(0.8, 0.8, 0.8).stream().filter(en -> en instanceof Item).forEach(en -> Bukkit.getPluginManager().callEvent(new PlayerPickupItemEvent(player, (Item) en, ((Item) en).getItemStack().getAmount())));
                    base.lastLoc = player.getLocation();
                } catch (Exception e) {
                    base.ready = false;
                    cancel();
                }
            }
        }.runTaskTimer(ServerLink.plugin, 0, 10);
        // Check to jump while sprinting
        try {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (!base.ready || !npc.isSpawned() || bot.destroying) {
                        cancel();
                        return;
                    }
                    if (player.isSprinting() && (!nstatus.isLobby() || Probability.get(1))) {
                        jump();
                    }
                }
            }.runTaskTimer(ServerLink.plugin, 2, 2);
        } catch (IllegalStateException e) {
            try {
                bot.destroy(true);
            } catch (Exception ignored) {
            }
        }
    }

    public boolean WalkPositiveX() {
        return walkPositiveX;
    }

    public Bot getBot() {
        return bot;
    }

    public BotBaseTrait getBase() {
        return base;
    }

    public AlphaPlayer getAlphaPlayer() {
        return aPlayer;
    }

    public NPC getNpc() {
        return npc;
    }

    public Player getPlayer() {
        return player;
    }

    public boolean isMovingAfterWaterEnter() {
        return movingAfterWaterEnter;
    }

    public void setMovingAfterWaterEnter(boolean movingAfterWaterEnter) {
        this.movingAfterWaterEnter = movingAfterWaterEnter;
    }

    public boolean isFleeingCombat() {
        return fleeingCombat;
    }

    public void setFleeingCombat(boolean fleeingCombat) {
        this.fleeingCombat = fleeingCombat;
    }

    public boolean isGettingAwayFromSpawn() {
        return gettingAwayFromSpawn;
    }

    public void setGettingAwayFromSpawn(boolean gettingAwayFromSpawn) {
        this.gettingAwayFromSpawn = gettingAwayFromSpawn;
    }

    public Location getPvpGoal() {
        return pvp_goal;
    }

    public void setPvpGoal(Location pvp_goal) {
        this.pvp_goal = pvp_goal;
    }

    public Location getWalkGoal() {
        return walk_goal;
    }

    public void setWalkGoal(Location walk_goal) {
        this.walk_goal = walk_goal;
    }

    public void changeWalkDirX() {
        walkPositiveX = !walkPositiveX;
    }

    public void changeWalkDirZ() {
        walkPositiveZ = !walkPositiveZ;
    }

    public boolean isCorrectingSpin() {
        return correctingSpin;
    }

    public void walkAround(boolean sprint) {
        if (Probability.get(20)) System.out.println("walk around");
        if (base.mvmtInLast2SecsSqrd < 1.2 * 1.2) runStarted = System.currentTimeMillis();
        if ((nstatus.isMapCountdown() || nstatus.isEndCountdown()) &&
                ServerLink.getServerType() == ServerType.skybattle) return;
        for (int i = 0; i <= 5; i++) {
            // If the bot is in the air, make it fall (they tend to float down)
            Location loc = player.getLocation().clone().add(0, 1, 0);
            loc.subtract(0, i, 0);
            if (loc.getBlock().getType() != null && loc.getBlock().getType() != Material.AIR) break;
            if (i == 5) {
                Vector velocity = player.getVelocity();
                velocity.setY(-2);
                player.setVelocity(velocity);
                npc.getNavigator().setTarget(player.getLocation());
                walk_goal = player.getLocation();
                pvp_goal = player.getLocation();
                return;
            }
        }
        if (npc.getNavigator().getTargetAsLocation() != null &&
                npc.getNavigator().getTargetAsLocation().distanceSquared(player.getLocation()) > 20 * 20) {
            // Stop navigating if the target location is too far away
            npc.getNavigator().setTarget(player.getLocation());
            walk_goal = player.getLocation();
            pvp_goal = player.getLocation();
        }

        // If the bot needs to do a special movement for a gametype, let it do that instead
        if (ServerLink.getServerType() == ServerType.kitpvp && KitpvpBotMovement.walkAround(this))
            return;
        if (ServerLink.getServerType() == ServerType.skybattle && !ServerLink.getServerName().startsWith("skybattlelobby-") && SkybattleBotMovement.walkAround(this))
            return;

        if (!gettingAwayFromSpawn && Probability.get(2 - (PlayerDamage.canTakePlayerDamage(player) ? 1.99 : 0)))
            walkPositiveX = !walkPositiveX;
        if (!gettingAwayFromSpawn && Probability.get(2 - (PlayerDamage.canTakePlayerDamage(player) ? 1.99 : 0)))
            walkPositiveZ = !walkPositiveZ;
        if (base.isInWater(player)) {
            if (movingAfterWaterEnter) return;
            Location nearestLand = nearestLandBlock();
            if (nearestLand != null) {
                movingAfterWaterEnter = true;
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        movingAfterWaterEnter = false;
                    }
                }.runTaskLater(ServerLink.plugin, 19);
                npc.faceLocation(nearestLand);
                npc.getNavigator().setTarget(nearestLand);
                player.setSprinting(true);
                return;
            }
        }
        Location walkTo = player.getLocation().add(BotBaseTrait.r.nextInt(PlayerDamage.canTakePlayerDamage(player) ? 2 : 3, (PlayerDamage.canTakePlayerDamage(player)) ? 15 : 4) * (walkPositiveX ? 1 : -1), 0, BotBaseTrait.r.nextInt(PlayerDamage.canTakePlayerDamage(player) ? 2 : 3, (PlayerDamage.canTakePlayerDamage(player)) ? 15 : 4) * (walkPositiveZ ? 1 : -1));
        // Move target location up/down to the ground/top of hill to get an accurate ray trace
        int blocksUp = 0;
        while (!isTraversable(walkTo.getBlock()) && blocksUp < 8) {
            walkTo.add(0, 1, 0);
            blocksUp++;
        }
        if (blocksUp == 8) walkTo.subtract(0, 8, 0);
        blocksUp = 0;
        while (isTraversable(walkTo.clone().subtract(0, 1, 0).getBlock()) && blocksUp > -8) {
            walkTo.subtract(0, 1, 0);
            blocksUp--;
        }
        if (blocksUp == -8) walkTo.add(0, 8, 0);
        boolean willFall = false;
        RayTrace rayTrace = new RayTrace(player.getLocation().toVector(), walkTo.toVector().subtract(player.getLocation().toVector()));
        int lastSolidBlockY = player.getLocation().getBlockY();
        for (Vector v : rayTrace.traverse(player.getLocation().distance(walkTo), 1)) {
            Block b = v.toLocation(player.getWorld()).getBlock();
            if ((b.getType() == Material.PORTAL || b.getType() == Material.BARRIER || b.getType() == Material.WEB) && b.getLocation().distanceSquared(player.getLocation()) > 2) {
                walkTo = b.getLocation();
                break;
            }
            while (b.getType() == Material.AIR) {
                b = b.getWorld().getBlockAt(b.getLocation().clone().subtract(0, 1, 0));
                if (lastSolidBlockY - b.getY() >= 4) {
                    willFall = true;
                    walkTo = b.getLocation();
                    break;
                }
            }
            if (willFall) break;
            lastSolidBlockY = b.getY();
        }
        int loops = 0;
        while (willFall || !isTraversable(walkTo.getBlock()) || walkTo.getBlock().getType().name().contains("WATER") || walkTo.getBlock().getType() == Material.PORTAL || walkTo.getBlock().getType() == Material.BARRIER || walkTo.getWorld().getBlockAt(walkTo.clone().subtract(0, 1, 0)).getType().name().contains("WATER") || base.getCombat().horizontalAngle(walkTo.clone().toVector().subtract(player.getLocation().toVector()), walk_goal.clone().toVector().subtract(player.getLocation().toVector())) > 80) {
            loops++;
            if (loops == 50) break;
            if (!gettingAwayFromSpawn && Probability.get(12 - (PlayerDamage.canTakePlayerDamage(player) ? 11.99 : 0)))
                walkPositiveX = !walkPositiveX;
            if (!gettingAwayFromSpawn && Probability.get(12 - (PlayerDamage.canTakePlayerDamage(player) ? 11.99 : 0)))
                walkPositiveZ = !walkPositiveZ;
            walkTo = player.getLocation().add(BotBaseTrait.r.nextInt(PlayerDamage.canTakePlayerDamage(player) ? 2 : 3, (PlayerDamage.canTakePlayerDamage(player)) ? 15 : 4) * (walkPositiveX ? 1 : -1), 0, BotBaseTrait.r.nextInt((PlayerDamage.canTakePlayerDamage(player)) ? 2 : 3, PlayerDamage.canTakePlayerDamage(player) ? 15 : 4) * (walkPositiveZ ? 1 : -1));
            if (base.getCombat().horizontalAngle(walkTo.clone().toVector().subtract(player.getLocation().toVector()), walk_goal.clone().toVector().subtract(player.getLocation().toVector())) > 80)
                continue;
            // Move target location up/down to the ground/top of hill to get an accurate ray trace
            blocksUp = 0;
            while (!isTraversable(walkTo.getBlock()) && blocksUp < 8) {
                walkTo.add(0, 1, 0);
                blocksUp++;
            }
            if (blocksUp == 8) walkTo.subtract(0, 8, 0);
            blocksUp = 0;
            while (isTraversable(walkTo.clone().subtract(0, 1, 0).getBlock()) && blocksUp > -8) {
                walkTo.subtract(0, 1, 0);
                blocksUp--;
            }
            if (blocksUp == -8) walkTo.add(0, 8, 0);
            // Make sure the bot won't fall. Otherwise, pathfinding will tp it.
            lastSolidBlockY = player.getLocation().getBlockY();
            rayTrace = new RayTrace(player.getLocation().toVector(), walkTo.toVector().subtract(player.getLocation().toVector()));
            willFall = false;
            for (Vector v : rayTrace.traverse(player.getLocation().distance(walkTo), 1)) {
                Block b = v.toLocation(player.getWorld()).getBlock();
                if ((b.getType() == Material.PORTAL || b.getType() == Material.BARRIER || b.getType() == Material.WEB) && b.getLocation().distanceSquared(player.getLocation()) > 2) {
                    walkTo = b.getLocation();
                    break;
                }
                while (b.getType() == Material.AIR) {
                    b = b.getWorld().getBlockAt(b.getLocation().clone().subtract(0, 1, 0));
                    if (lastSolidBlockY - b.getY() >= 4) {
                        willFall = true;
                        walkTo = b.getLocation();
                        break;
                    }
                }
                if (willFall) break;
                lastSolidBlockY = b.getY();
            }
        }
        if (willFall) {
            walkTo = player.getLocation().add(1.5 * (walkPositiveX ? 1 : -1), -4, 1.5 * (walkPositiveZ ? 1 : -1));
            // Move target location up/down to the ground/top of hill to get an accurate ray trace
            blocksUp = 0;
            while (isTraversable(walkTo.clone().subtract(0, 1, 0).getBlock()) && blocksUp > -8) {
                walkTo.subtract(0, 1, 0);
                blocksUp--;
            }
            if (blocksUp == -8) walkTo.add(0, 8, 0);
        }
        if (base.mvmtInLast2SecsSqrd < 1.2 * 1.2) runStarted = System.currentTimeMillis();
        npc.getNavigator().setTarget(walkTo);
        walk_goal = walkTo;
        if (sprint && !player.isSprinting()) player.setSprinting(true);
    }

    // Doesn't work with things like waterfalls and underwater steps
    private Location nearestLandBlock() {
        Location loc;
        boolean foundBlock;
        for (int r = 2; r < 7; r++) {
            int x = r;
            while (true) {
                for (int r2 = r; r2 < r + 3; r2++) {
                    int z = r2;
                    while (true) {
                        loc = player.getLocation().clone().add(x, 0, z);
                        foundBlock = true;
                        RayTrace rayTrace = new RayTrace(player.getLocation().clone().toVector(), loc.clone().toVector().subtract(player.getLocation().toVector()));
                        for (Vector v : rayTrace.traverse(player.getLocation().distance(loc), 0.5)) {
                            Location l = v.toLocation(player.getWorld());
                            if (!isTraversable(l.clone().add(0, 1, 0).getBlock())
                                    || !isTraversable(l.clone().add(0, 2, 0).getBlock())) {
                                foundBlock = false;
                                break;
                            }
                        }
                        if (foundBlock) {
                            if (!loc.getBlock().getType().name().contains("WATER")
                                    && isTraversable(loc.getBlock())
                                    && !isTraversable(loc.clone().add(0, 1, 0).getBlock())
                                    && !isTraversable(loc.clone().add(0, 2, 0).getBlock()))
                                return loc;
                        }
                        if (z > 0) z *= -1;
                        else break;
                    }
                }
                if (x > 0) x *= -1;
                else break;
            }
        }
        return null;
    }

    private boolean isTraversable(Block b) {
        return b == null || (!b.getType().isSolid());
    }

    private void jump() {
        if (player.isOnGround() && System.currentTimeMillis() - lastJump > 1600 && System.currentTimeMillis() - runStarted > 700 && Math.abs(Math.toDegrees(base.lastLoc.getDirection().angle(player.getLocation().getDirection()))) < 90 && Math.abs(Math.toDegrees(base.loc1SecAgo.getDirection().angle(player.getLocation().getDirection()))) < 90 && base.getCombat().timeSinceAttack > 40) {
            double headRot1 = Math.abs(Math.toDegrees(base.lastLoc.getDirection().angle(player.getLocation().getDirection())));
            double headRot2 = Math.abs(Math.toDegrees(base.loc1SecAgo.getDirection().angle(player.getLocation().getDirection())));
            if ((Double.isNaN(headRot1) || headRot1 < 90) && (Double.isNaN(headRot2) || headRot2 < 90)) {
                ((EntityHumanNPC.PlayerNPC) npc.getEntity()).getHandle().getControllerJump().b();
                ((EntityHumanNPC.PlayerNPC) npc.getEntity()).getHandle().getControllerJump().a();
            }
        }
    }
}

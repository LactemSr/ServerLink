package serverlink.bot;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerChatTabCompleteEvent;
import org.bukkit.event.world.ChunkUnloadEvent;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.alphaplayer.PlayerManager;
import serverlink.command.CommandManager;
import serverlink.customevent.CustomFullyJoinEvent;
import serverlink.customevent.CustomLeaveEvent;
import serverlink.map.MapManager;
import serverlink.network.ncount;
import serverlink.network.nstatus;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.SplittableRandom;


public class BotListener implements Listener, TabCompleter {
    private SplittableRandom r = new SplittableRandom();

    @EventHandler
    void onPlayerJoin(CustomFullyJoinEvent e) {
        if (BotManager.botUsers.keySet().contains(e.getPlayer().getName())) return;
        if (BotManager.creatingOrDeletingBots) return;
        // determine bots to spawn and destroy
        if (!BotManager.isDynamicBotCreationEnabled()) return;
        int botsToSpawn = 0;
        int botsToDestroy = 0;
        if (PlayerManager.getOnlineAlphaPlayers().size() < ncount.getMaxPlayers() * 0.15) {
            botsToSpawn = (int) Math.ceil((ncount.getMaxPlayers() * 0.15) - PlayerManager.getOnlineAlphaPlayers().size());
        } else if (Bukkit.getOnlinePlayers().size() / ncount.getMaxPlayers() >= 0.5) {
            botsToDestroy = BotManager.getNumOfBots();
        } else if (Bukkit.getOnlinePlayers().size() > ncount.getMaxPlayers() * 0.15 && Bukkit.getOnlinePlayers().size() / BotManager.getNumOfBots() < 2.0) {
            double botsToKeep = Bukkit.getOnlinePlayers().size() / 2.0;
            botsToDestroy = (int) Math.ceil(BotManager.getNumOfBots() - botsToKeep);
        }
        if (botsToSpawn + BotManager.getNumOfBots() > 30) botsToSpawn = 30 - BotManager.getNumOfBots();
        if (botsToDestroy > BotManager.getNumOfBots()) botsToDestroy = BotManager.getNumOfBots();
        Location spawnLocation = BotManager.getBotSpawnLoc();
        if (spawnLocation == null) {
            if (nstatus.isGame()) spawnLocation = MapManager.getCurrentGameMap().getWorld().getSpawnLocation();
            else spawnLocation = MapManager.getLobbyMap().getWorld().getSpawnLocation();
        }
        int delay = 5 + r.nextInt(5);

        // destroy bots
        for (int i = 0; i < botsToDestroy; i++) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    BotManager.getBots().get(r.nextInt(BotManager.getNumOfBots())).destroy(true);
                }
            }.runTaskLater(ServerLink.plugin, delay);
            delay += 21 + r.nextInt(1);
        }

        // spawn bots
        for (int i = 0; i < botsToSpawn; i++) {
            Location finalSpawnLocation = spawnLocation;
            new BukkitRunnable() {
                @Override
                public void run() {
                    // choose each bot's username
                    String username = (String) BotManager.botUsers.keySet().toArray()[r.nextInt(BotManager.botUsers.keySet().size())];
                    while (BotManager.isBotNameDisabled(username)) {
                        username = (String) BotManager.botUsers.keySet().toArray()[r.nextInt(BotManager.botUsers.keySet().size())];
                    }
                    // spawn each bot
                    new Bot(username, finalSpawnLocation, true);
                }
            }.runTaskLater(ServerLink.plugin, delay);
            delay += 21 + r.nextInt(1);
        }
        if (botsToSpawn != 0 || botsToDestroy != 0) {
            BotManager.creatingOrDeletingBots = true;
            new BukkitRunnable() {
                @Override
                public void run() {
                    BotManager.creatingOrDeletingBots = false;
                }
            }.runTaskLater(ServerLink.plugin, (botsToSpawn + botsToDestroy) * 25);
        }
    }

    @EventHandler
    void onLeave(CustomLeaveEvent e) {
        // enable bots to be created with the player's name
        for (String botName : BotManager.botUsers.keySet()) {
            if (botName.equalsIgnoreCase(e.getPlayer().getName())) {
                BotManager.enableBotName(botName);
                break;
            }
        }
        if (BotManager.botUsers.keySet().contains(e.getPlayer().getName())) return;
        // determine bots to spawn and destroy
        if (!BotManager.isDynamicBotCreationEnabled()) return;
        if (BotManager.creatingOrDeletingBots) return;
        int botsToSpawn = 0;
        int botsToDestroy = 0;
        if (PlayerManager.getOnlineAlphaPlayers().size() < ncount.getMaxPlayers() * 0.15) {
            botsToSpawn = (int) Math.ceil((ncount.getMaxPlayers() * 0.15) - PlayerManager.getOnlineAlphaPlayers().size());
        } else if (Bukkit.getOnlinePlayers().size() / ncount.getMaxPlayers() >= 0.5) {
            botsToDestroy = BotManager.getNumOfBots();
        } else if (Bukkit.getOnlinePlayers().size() > ncount.getMaxPlayers() * 0.15 && Bukkit.getOnlinePlayers().size() / BotManager.getNumOfBots() < 2.0) {
            double botsToKeep = Bukkit.getOnlinePlayers().size() / 2.0;
            botsToDestroy = (int) Math.ceil(BotManager.getNumOfBots() - botsToKeep);
        }
        if (botsToSpawn + BotManager.getNumOfBots() > 30) botsToSpawn = 30 - BotManager.getNumOfBots();
        if (botsToDestroy > BotManager.getNumOfBots()) botsToDestroy = BotManager.getNumOfBots();
        Location spawnLocation = BotManager.getBotSpawnLoc();
        if (spawnLocation == null) {
            if (nstatus.isGame()) spawnLocation = MapManager.getCurrentGameMap().getWorld().getSpawnLocation();
            else spawnLocation = MapManager.getLobbyMap().getWorld().getSpawnLocation();
        }
        int delay = 5 + r.nextInt(5);

        // destroy bots
        for (int i = 0; i < botsToDestroy; i++) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    BotManager.getBots().get(r.nextInt(BotManager.getNumOfBots())).destroy(true);
                }
            }.runTaskLater(ServerLink.plugin, delay);
            delay += 21 + r.nextInt(1);
        }

        // spawn bots
        for (int i = 0; i < botsToSpawn; i++) {
            Location finalSpawnLocation = spawnLocation;
            new BukkitRunnable() {
                @Override
                public void run() {
                    // choose each bot's username
                    String username = (String) BotManager.botUsers.keySet().toArray()[r.nextInt(BotManager.botUsers.keySet().size())];
                    while (BotManager.isBotNameDisabled(username)) {
                        username = (String) BotManager.botUsers.keySet().toArray()[r.nextInt(BotManager.botUsers.keySet().size())];
                    }
                    // spawn each bot
                    new Bot(username, finalSpawnLocation, true);
                }
            }.runTaskLater(ServerLink.plugin, delay);
            delay += 21 + r.nextInt(1);
        }
        if (botsToSpawn != 0 || botsToDestroy != 0) {
            BotManager.creatingOrDeletingBots = true;
            new BukkitRunnable() {
                @Override
                public void run() {
                    BotManager.creatingOrDeletingBots = false;
                }
            }.runTaskLater(ServerLink.plugin, (botsToSpawn + botsToDestroy) * 25);
        }
    }

    @EventHandler
    void onPreJoin(AsyncPlayerPreLoginEvent e) {
        // destroy and disable another creation of the bot that has this player's name
        for (Bot bot : new ArrayList<>(BotManager.getBots())) {
            if (bot.getUsername().equalsIgnoreCase(e.getName())) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        bot.destroy(true);
                        BotManager.disableBotName(bot.getUsername());
                    }
                }.runTask(ServerLink.plugin);
                return;
            }
        }
        // disable bot names so that bots are not created with this name of an online player
        for (String botName : BotManager.botUsers.keySet()) {
            if (botName.equalsIgnoreCase(e.getName())) {
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        BotManager.disableBotName(botName);
                    }
                }.runTask(ServerLink.plugin);
                return;
            }
        }
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onChunkUnload_LAST(ChunkUnloadEvent e) {
        for (Entity en : e.getChunk().getEntities()) {
            if (en.hasMetadata("NPC")) {
                e.setCancelled(true);
                return;
            }
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    private void onChunkUnload_FIRST(ChunkUnloadEvent e) {
        for (Entity en : e.getChunk().getEntities()) {
            if (en.hasMetadata("NPC")) {
                e.setCancelled(true);
                return;
            }
        }
    }

    @CommandManager.CommandHandler(name = "botlink", async = false, sendUsageMessageOnUnrecognizedSubCommand = false, permission = "rank.10")
    public void base(Player player, String[] args) {
        if (args.length == 0) {
            player.sendMessage("Usage: /botlink <bot_name>");
            return;
        }
        Bot bot = null;
        for (Bot b : BotManager.getBots()) {
            if (b.getNpc().isSpawned() && b.getNpc().getTrait(BotBaseTrait.class).ready && b.getNpc().getEntity().getName().equalsIgnoreCase(args[0])) {
                bot = b;
                break;
            }
        }
        if (bot == null) {
            player.sendMessage("Couldn't find a bot with that name.");
            return;
        }
        PlayerManager.get(player.getUniqueId()).getData("botlink").setValue(bot);
        player.sendMessage("Bot linked. Anything you type after /botsay will be said by the bot.");
    }

    @CommandManager.CommandHandler(name = "botsay", aliases = {"bsay", "bs",}, async = false, permission = "rank.10", sendUsageMessageOnUnrecognizedSubCommand = false)
    public void botSay(Player player, String[] args) {
        Object obj = PlayerManager.get(player).getData("botlink").getValue();
        if (obj != null && obj instanceof Bot) {
            Bot bot = (Bot) obj;
            if (!bot.getNpc().isSpawned()) {
                player.sendMessage("The bot you are linked to is no longer valid.");
                return;
            }
            String message = "";
            for (String arg : args) message = message + arg + " ";
            message = message.trim();
            String finalMessage = message;
            new BukkitRunnable() {
                @Override
                public void run() {
                    AsyncPlayerChatEvent event = new AsyncPlayerChatEvent(true, (Player) bot.getNpc().getEntity(), finalMessage, new HashSet<>(Bukkit.getOnlinePlayers()));
                    Bukkit.getPluginManager().callEvent(event);
                    if (event.isCancelled()) return;
                    for (Player p : Bukkit.getOnlinePlayers()) {
                        p.sendMessage(event.getFormat().replace("%2$s", event.getMessage()).replace("%1$s", ((Player) bot.getNpc().getEntity()).getDisplayName()));
                    }
                }
            }.runTaskAsynchronously(ServerLink.plugin);
        }
    }

    // for tab completing in a chat message
    @EventHandler
    void onTabCompletion(PlayerChatTabCompleteEvent e) {
        String word = e.getLastToken().toLowerCase();
        for (Bot bot : BotManager.getBots()) {
            if (bot.getNpc().isSpawned() && bot.getNpc().getEntity().getName() != null
                    && bot.getNpc().getEntity().getName().toLowerCase().startsWith(word.toLowerCase())) {
                e.getTabCompletions().add(bot.getNpc().getEntity().getName());
            }
        }
    }

    // for tab completing in certain commands
    @Override
    public List<String> onTabComplete(CommandSender sender, Command command, String alias, String[] args) {
        ArrayList<String> tabCompletions = new ArrayList<>();
        for (Bot bot : BotManager.getBots()) {
            if (bot.getNpc().isSpawned() && bot.getNpc().getEntity().getName() != null
                    && bot.getNpc().getEntity().getName().toLowerCase().startsWith(args[args.length - 1].toLowerCase())) {
                tabCompletions.add(bot.getNpc().getEntity().getName());
            }
        }
        for (Player p : Bukkit.getOnlinePlayers()) {
            if (p.getName().toLowerCase().startsWith(args[args.length - 1].toLowerCase())) {
                tabCompletions.add(p.getName());
            }
        }
        return tabCompletions;
    }
}

package serverlink.bot;


import net.citizensnpcs.api.trait.Trait;
import net.citizensnpcs.api.trait.trait.Inventory;
import net.minecraft.server.v1_8_R3.PacketPlayOutEntityMetadata;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.bot.combat.BotCombat;
import serverlink.bot.movement.BotMovement;
import serverlink.classes.Class;
import serverlink.classes.ClassManager;
import serverlink.customevent.CustomPlayerDeathEvent;
import serverlink.customevent.CustomPlayerKillLivingEntityEvent;
import serverlink.network.ncount;
import serverlink.network.nstatus;
import serverlink.player.damage.PlayerDamage;
import serverlink.server.ServerType;
import serverlink.util.Probability;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;
import java.util.stream.Collectors;

public class BotBaseTrait extends Trait {
    public static final SplittableRandom r = new SplittableRandom();
    // Do not change!
    private static final int updateRate = 5;
    private final int healRate = 80;
    private final long minAfkDuration = 20 * 15;
    private final long maxAfkDuration = 20 * 120;
    public Player player;
    public boolean ready = false;
    public double mvmtInLast2SecsSqrd = 0;
    public Location loc1SecAgo = new Location(Bukkit.getWorlds().get(0), 0, 0, 0);
    public Location loc2SecsAgo = new Location(Bukkit.getWorlds().get(0), 0, 0, 0);
    public int loc2SecsAgoTick = 0;
    public Location lastLoc = new Location(Bukkit.getWorlds().get(0), 0, 0, 0);
    public boolean pausingAfterRespawn = false;
    public boolean pvpEnabledLastRun = false;
    public long timePvpEnabled = 0;
    int fireTicks = 0;
    private Bot bot;
    private BotMovement movement;
    private BotCombat combat;
    private long timeSinceHeal = 0;
    private int runTick = 0;
    private int timeSinceFireDamage = 0;


    BotBaseTrait(Bot bot) {
        super("botbase");
        this.bot = bot;
    }

    public BotMovement getMovement() {
        return movement;
    }

    public BotCombat getCombat() {
        return combat;
    }

    public boolean isWeapon(Material mat) {
        switch (mat) {
            case DIAMOND_SWORD:
            case GOLD_SWORD:
            case IRON_SWORD:
            case STONE_SWORD:
            case WOOD_SWORD:
            case DIAMOND_PICKAXE:
            case GOLD_PICKAXE:
            case IRON_PICKAXE:
            case STONE_PICKAXE:
            case WOOD_PICKAXE:
            case DIAMOND_AXE:
            case GOLD_AXE:
            case IRON_AXE:
            case STONE_AXE:
            case WOOD_AXE:
            case DIAMOND_SPADE:
            case GOLD_SPADE:
            case IRON_SPADE:
            case STONE_SPADE:
            case WOOD_SPADE:
            case BLAZE_ROD:
                return true;
            default:
                return false;
        }
    }

    public void swapToMelee() {
        if (!npc.hasTrait(Inventory.class)) return;
        int i = 0;
        Inventory inv = npc.getTrait(Inventory.class);
        ItemStack[] items = inv.getContents();
        ItemStack held = items[0] == null ? null : items[0].clone();
        if (held != null && isWeapon(held.getType())) return;
        while (i < items.length - 1) {
            i++;
            if (items[i] != null && items[i].getType() != Material.AIR && isWeapon(items[i].getType())) {
                items[0] = items[i];
                items[i] = held == null ? new ItemStack(Material.AIR) : held;
                inv.setContents(items);
                return;
            }
        }
    }

    private void swapToRandomSlot() {
        if (!npc.hasTrait(Inventory.class)) return;
        int newSlot = r.nextInt(9);
        Inventory inv = npc.getTrait(Inventory.class);
        ItemStack[] items = inv.getContents();
        ItemStack held = items[0] == null ? new ItemStack(Material.AIR) : items[0].clone();
        items[0] = items[newSlot] == null ? new ItemStack(Material.AIR) : items[newSlot].clone();
        items[newSlot] = held;
        inv.setContents(items);
    }

    private void runUpdate() {
        if (!ready || player == null || player.isDead() || bot.destroying || PlayerManager.get(player) == null || !PlayerManager.get(player).isFullyLoggedIn()) {
            System.out.println("bot runUpdate() being called while bot is invalid: " + (!ready) + ", "
                    + (player == null) + ", " + (player == null || player.isDead()) + ", "
                    + (bot.destroying) + ", " + (PlayerManager.get(player) == null) + ", "
                    + (PlayerManager.get(player) == null || !PlayerManager.get(player).isFullyLoggedIn()));
            return;
        }
        if (Probability.get(1)) System.out.println("run update");
        if (player.getLocation().getWorld() != lastLoc.getWorld() || player.getLocation().distanceSquared(lastLoc) > 10) {
            lastLoc = player.getLocation();
            loc1SecAgo = player.getLocation();
            loc2SecsAgo = player.getLocation();
            movement.setPvpGoal(player.getLocation());
            movement.setWalkGoal(player.getLocation());
        }
        combat.currentTargetTime += updateRate;
        combat.timeSinceAttack += updateRate;
        timeSinceHeal += updateRate;
        loc2SecsAgoTick += updateRate;
        timeSinceFireDamage += updateRate;
        // Heal player
        if (healRate > 0 && timeSinceHeal > healRate && player.getHealth() < 20) {
            player.setHealth(Math.min(player.getHealth() + 1.0, 20));
            timeSinceHeal = 0;
        }
        if (player.getLocation().getY() < 20 && PlayerDamage.canTakePlayerDamage(player)) {
            Bukkit.getPluginManager().callEvent(new CustomPlayerDeathEvent(player, "suicide"));
            return;
        }
        // Send fire packet and damage player if on fire (the server doesn't handle this for bots)
        if (player.getLocation().getBlock().getType().name().contains("LAVA")
                || player.getLocation().getBlock().getType().name().contains("FIRE"))
            fireTicks = 80;
        if (!PlayerDamage.canTakeFireDamage(player)) fireTicks = 0;
        if (fireTicks > 0) {
            if (timeSinceFireDamage >= 20) {
                timeSinceFireDamage = 0;
                LivingEntity enemy = ClassManager.getCombatOpponent(player);
                if (enemy == null || enemy.isDead() || (enemy.getType() == EntityType.PLAYER && !((Player) enemy).isOnline())) {
                    player.setLastDamageCause(new EntityDamageEvent(player, EntityDamageEvent.DamageCause.FIRE_TICK, 2));
                    ClassManager.dealDamage(player, "fire", 2.0, EntityDamageEvent.DamageCause.FIRE_TICK, true);
                } else {
                    ClassManager.dealDamage_NoKb(player, enemy, 2.0, null, EntityDamageEvent.DamageCause.FIRE_TICK, true);
                }
                // The bot died from fire damage
                if (fireTicks == 0) return;
            }
            ((CraftPlayer) player).getHandle().fireTicks = fireTicks;
            ((CraftPlayer) player).getHandle().maxFireTicks = 200;
            ((CraftPlayer) player).getHandle().getDataWatcher().watch(0, (byte) 0x01);
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.getLocation().distanceSquared(this.player.getLocation()) > 32 * 32) return;
                ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(this.player.getEntityId(), ((CraftPlayer) player).getHandle().getDataWatcher(), false));
            }
            if (player.getLocation().getBlock().getType().name().contains("WATER")) {
                fireTicks = -6;
                ((CraftPlayer) player).getHandle().fireTicks = 0;
                ((CraftPlayer) player).getHandle().maxFireTicks = 200;
                ((CraftPlayer) player).getHandle().getDataWatcher().watch(9, (byte) 0);
                ((CraftPlayer) player).getHandle().getDataWatcher().watch(0, (byte) 0x08);
                for (Player player : Bukkit.getOnlinePlayers()) {
                    if (player.getLocation().distanceSquared(this.player.getLocation()) > 32 * 32) return;
                    ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(this.player.getEntityId(), ((CraftPlayer) player).getHandle().getDataWatcher(), true));
                }
            }
        } else if (fireTicks > -6) {
            ((CraftPlayer) player).getHandle().fireTicks = 0;
            ((CraftPlayer) player).getHandle().maxFireTicks = 200;
            ((CraftPlayer) player).getHandle().getDataWatcher().watch(9, (byte) 0);
            ((CraftPlayer) player).getHandle().getDataWatcher().watch(0, (byte) 0x08);
            for (Player player : Bukkit.getOnlinePlayers()) {
                if (player.getLocation().distanceSquared(this.player.getLocation()) > 32 * 32) return;
                ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(this.player.getEntityId(), ((CraftPlayer) player).getHandle().getDataWatcher(), true));
            }
            player.setSneaking(false);
        }
        if (Probability.get(1)) {
            // clear arrows from time to time
            /*
             * for 1.9+
             * ((CraftPlayer) player).getHandle().k(0);
             */
            ((CraftPlayer) player).getHandle().getDataWatcher().watch(9, (byte) 0);
            for (Player player : Bukkit.getOnlinePlayers()) {
                ((CraftPlayer) player).getHandle().playerConnection.sendPacket(new PacketPlayOutEntityMetadata(this.player.getEntityId(), ((CraftPlayer) player).getHandle().getDataWatcher(), true));
            }
        }
        if (ServerLink.getServerType() == ServerType.skybattle && (nstatus.isLobby() || nstatus.isLobbyCountdown())
                && (player.getLocation().getY() < bot.spawnLoc.getY() - 2 || player.getLocation().getY() > bot.spawnLoc.getY() + 2)) {
            Player closestPlayer = null;
            for (Player p : Bukkit.getOnlinePlayers()) {
                if (closestPlayer == null || player.getLocation().distanceSquared(p.getLocation()) < player.getLocation().distanceSquared(closestPlayer.getLocation())) {
                    closestPlayer = p;
                }
            }
            if (closestPlayer != null) {
                player.teleport(closestPlayer);
                movement.setWalkGoal(closestPlayer.getLocation());
                npc.getNavigator().setTarget(closestPlayer.getLocation());
                npc.getNavigator().cancelNavigation();
            }
            return;
        }
        if (loc2SecsAgoTick == 20) loc1SecAgo = player.getLocation();
        else if (loc2SecsAgoTick >= 40) {
            loc2SecsAgoTick = 0;
            mvmtInLast2SecsSqrd = player.getLocation().clone().distanceSquared(loc2SecsAgo.clone());
            loc2SecsAgo = player.getLocation();
            loc1SecAgo = player.getLocation();
        }
        if ((nstatus.isMapCountdown() || nstatus.isEndCountdown()) &&
                ServerLink.getServerType() == ServerType.skybattle) return;
        // Return if afk, or roll dice to make player afk
        if (bot.standingAfk) return;
        else if (Probability.get(0.02 + (nstatus.isLobby() ? 0.5 : 0))) {
            player.setSprinting(false);
            bot.standingAfk = true;
            new BukkitRunnable() {
                @Override
                public void run() {
                    bot.standingAfk = false;
                }
            }.runTaskLater(ServerLink.plugin, r.nextLong(minAfkDuration, maxAfkDuration));
            return;
        } else if (pausingAfterRespawn) return;
        // Chance to swap inventory to random slot
        if (combat.getEntityChasing() == null && Probability.get(1)) swapToRandomSlot();
        // Now handle movement and combat
        if (combat.timeSinceAttack > (20 * 120) + r.nextInt(20 * 120) && ServerLink.getServerType() != ServerType.skybattle) {
            // Teleport to spawn if the bot hasn't attacked anyone for over two minutes
            npc.getNavigator().cancelNavigation();
            npc.getNavigator().setTarget(null);
            player.teleport(player.getWorld().getSpawnLocation());
            combat.timeSinceAttack = 0;
            npc.getNavigator().cancelNavigation();
            npc.getNavigator().setTarget(null);
        } else if (movement.isMovingAfterWaterEnter()) return;
        else if (movement.isCorrectingSpin()) return;
        else if (!nstatus.isGame() || !PlayerDamage.canHurtPlayers(player)) {
            pvpEnabledLastRun = false;
            movement.walkAround(false);
        } else if (movement.isGettingAwayFromSpawn()) {
            movement.walkAround(true);
        } else if (!pvpEnabledLastRun && Probability.get(15 + Math.pow(getNearbyLivingEntities(player.getLocation(), 12).size(), 1.8))) {
            timePvpEnabled = System.currentTimeMillis();
            pvpEnabledLastRun = true;
            movement.setGettingAwayFromSpawn(true);
            movement.walkAround(true);
            new BukkitRunnable() {
                @Override
                public void run() {
                    movement.setGettingAwayFromSpawn(false);
                }
            }.runTaskLater(ServerLink.plugin, 20 * r.nextInt(5, 12));
        } else {
            if (!pvpEnabledLastRun) {
                pvpEnabledLastRun = true;
                timePvpEnabled = System.currentTimeMillis();
            }
            LivingEntity target = combat.findBestTarget();
            if (target != null && System.currentTimeMillis() - timePvpEnabled > 2000) {
                if (movement.isFleeingCombat()) {
                    movement.walkAround(true);
                } else if (Probability.get(combat.getChanceToFlee()) || (player.getHealth() < 5 && Probability.get(combat.getChanceToFleeWhenHealthLow()))) {
                    // Chance to flee
                    movement.setFleeingCombat(true);
                    movement.walkAround(true);
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            movement.setFleeingCombat(false);
                        }
                    }.runTaskLater(ServerLink.plugin, r.nextLong(20 * 3, 20 * 8));
                } else {
                    combat.setChasing(target);
                    combat.chaseTarget();
                }
            } else {
                combat.setChasing(null);
                movement.walkAround(true);
            }
        }
        if (player.getLocation().distanceSquared(loc1SecAgo) < 1 && player.getLocation().distanceSquared(loc2SecsAgo) < 1)
            player.setSprinting(false);
    }

    @Override
    public void run() {
        if (!npc.isSpawned() || !ready || bot.destroying) return;
        runTick++;
        if (runTick >= updateRate) {
            runTick = 0;
            runUpdate();
        }
    }

    @Override
    public void onSpawn() {
        boolean secondCall = player != null;
        player = (Player) npc.getEntity();
        setHealth(20);
        if (secondCall && !ready && !bot.destroying) {
            initialize();
        } else {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (!ready && npc.isSpawned() && player != null && !bot.destroying) initialize();
                }
            }.runTaskLater(ServerLink.plugin, 100);
        }
    }

    private void initialize() {
        if (ready) return;
        new BukkitRunnable() {
            @Override
            public void run() {
                if (ready) return;
                AlphaPlayer aPlayer = new AlphaPlayer(player.getUniqueId(), player, bot.announceJoin);
                BotManager.getBots().add(bot);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        ncount.incrCount();
                    }
                }.runTaskAsynchronously(ServerLink.plugin);
                changeToRandomClass(aPlayer);
                if (Probability.get(33) && nstatus.isGame() && ServerLink.getServerType() == ServerType.kitpvp)
                    ClassManager.equipClass(player, ((Class) aPlayer.getData("class").getValue()), null);
                movement = new BotMovement(bot, BotBaseTrait.this, player);
                combat = new BotCombat(bot, BotBaseTrait.this, player);
                if (Probability.get(50)) movement.changeWalkDirX();
                if (Probability.get(50)) movement.changeWalkDirZ();
                fireTicks = 0;
                ((CraftPlayer) player).getHandle().fireTicks = 0;
                ((CraftPlayer) player).getHandle().maxFireTicks = 200;
                lastLoc = player.getLocation();
                loc1SecAgo = player.getLocation();
                loc2SecsAgo = player.getLocation();
                movement.setPvpGoal(player.getLocation());
                movement.setWalkGoal(player.getLocation());
                ready = true;
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        if (!ready || !npc.isSpawned() || bot.destroying) {
                            cancel();
                            return;
                        }
                        fireTicks--;
                        if (fireTicks < -6) fireTicks = -6;
                    }
                }.runTaskTimer(ServerLink.plugin, 1, 1);
            }
        }.runTask(ServerLink.plugin);
    }

    private void setHealth(double health) {
        health = Math.min(20, health);
        if (npc.isSpawned()) {
            player.setMaxHealth(20);
            player.setHealth(health);
        }
    }

    private void changeToRandomClass(AlphaPlayer aPlayer) {
        if (ClassManager.getClasses().isEmpty()) {
            aPlayer.getData("class").setValue(ClassManager.getDefaultClass());
            return;
        }
        ClassManager.unEquipClass(player, (Class) aPlayer.getData("class").getValue());
        aPlayer.getData("class").setValue(ClassManager.getClasses().get(r.nextInt(ClassManager.getClasses().size())));
        while (true) {
            Class cl = (Class) aPlayer.getData("class").getValue();
            String className = cl.getName();
            if (cl instanceof ClassManager.BlankClass || className.contains("archer") || className.contains("marksman") || className.contains("horse") || className.contains("assassin") || className.contains("fisherman") || className.contains("rider"))
                aPlayer.getData("class").setValue(ClassManager.getClasses().get(r.nextInt(ClassManager.getClasses().size())));
            else break;
        }
    }

    public boolean isInWater(LivingEntity entity) {
        return (entity.getLocation().getBlock().getType().name().contains("WATER") ||
                entity.getLocation().getWorld().getBlockAt(entity.getLocation().subtract(0, 1, 0)).getType().name().contains("WATER"));
    }

    public List<LivingEntity> getNearbyLivingEntities(Location loc, double range) {
        return loc.getWorld().getNearbyEntities(loc, range, range, range).stream().filter(ent -> ent instanceof LivingEntity && !ent.isDead()).map(ent -> (LivingEntity) ent).collect(Collectors.toList());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onDeath(CustomPlayerKillLivingEntityEvent e) {
        if (!e.getEntity().getUniqueId().equals(player.getUniqueId())) return;
        death(player);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void onDeath(CustomPlayerDeathEvent e) {
        if (!e.getPlayer().getUniqueId().equals(player.getUniqueId())) return;
        death(e.getPlayer());
    }

    private void death(Player player) {
        fireTicks = 0;
        combat.timeSinceAttack = 0;
        combat.currentTargetTime = 0;
        combat.setChasing(null);
        movement.setFleeingCombat(false);
        movement.setGettingAwayFromSpawn(false);
        movement.setMovingAfterWaterEnter(false);
        movement.setPvpGoal(player.getWorld().getSpawnLocation());
        movement.setWalkGoal(player.getWorld().getSpawnLocation());
        pausingAfterRespawn = true;
        new BukkitRunnable() {
            @Override
            public void run() {
                pausingAfterRespawn = false;
            }
        }.runTaskLater(ServerLink.plugin, r.nextLong(20, 80));
        new BukkitRunnable() {
            @Override
            public void run() {
                if (!ready || !npc.isSpawned() || bot.destroying) return;
                // 15% chance to change classes
                if (Probability.get(15)) {
                    changeToRandomClass(PlayerManager.get(player.getUniqueId()));
                }
                // 33% chance to equip class
                if (Probability.get(33) && nstatus.isGame())
                    ClassManager.equipClass(player, ((Class) PlayerManager.get(player.getUniqueId()).getData("class").getValue()), null);
            }
        }.runTaskLater(ServerLink.plugin, 5);
    }

    @Override
    public void onDespawn() {
        if (bot.destroying) return;
        for (Bot bot : new ArrayList<>(BotManager.getBots())) {
            if ((bot.getNpc().getEntity() == null && bot.getNpc().getTrait(BotBaseTrait.class).ready) || (bot.getNpc().getEntity() != null && bot.getNpc().getEntity().getUniqueId().equals(player.getUniqueId()))) {
                System.out.println("Destroying bot onDespawn().");
                bot.destroy(true);
                return;
            }
        }
    }

    @Override
    public void onRemove() {
        if (bot.destroying) return;
        for (Bot bot : new ArrayList<>(BotManager.getBots())) {
            if (bot.getNpc().getEntity().getUniqueId().equals(player.getUniqueId())) {
                System.out.println("Destroying bot onRemove().");
                bot.destroy(true);
                return;
            }
        }
    }
}
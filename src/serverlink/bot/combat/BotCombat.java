package serverlink.bot.combat;

import net.citizensnpcs.api.npc.NPC;
import net.citizensnpcs.api.trait.trait.Inventory;
import net.citizensnpcs.util.PlayerAnimation;
import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.PlayerAnimationEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.util.Vector;
import serverlink.ServerLink;
import serverlink.alphaplayer.PlayerManager;
import serverlink.bot.Bot;
import serverlink.bot.BotBaseTrait;
import serverlink.classes.ClassManager;
import serverlink.player.damage.PlayerDamage;
import serverlink.player.team.TeamManager;
import serverlink.server.ServerType;
import serverlink.util.Probability;

import java.util.ArrayList;
import java.util.List;

public class BotCombat {
    // Range to look for enemies in (provided they are in the line-of-sight)
    public final double range = 40.0;
    public final double rangesquared = range * range;
    // Chance every second to flee combat (1 means bot will flee once every 100 seconds of pursuit and combat)
    public final double chanceToFlee = 1.2;
    public final double chanceToFleeWhenHealthLow = 4;
    // The maximum number of times the bot can attack each second
    public final int attackRate = BotBaseTrait.r.nextInt(6, 12);
    // Should the bot swing its weapon while in pursuit (like a player) or only hit when close to enemies (like a mob)?
    public boolean swingWhileChasing = true;
    public Bot bot;
    public BotBaseTrait base;
    public Player player;
    public NPC npc;
    public int currentTargetTime;
    public long timeSinceAttack = 0;
    private LivingEntity chasing = null;
    // True means move left, false means move right
    private boolean moveLeftAfterHit = true;
    private List<LivingEntity> entitiesIgnoring = new ArrayList<>();

    public BotCombat(Bot bot, BotBaseTrait base, Player player) {
        this.bot = bot;
        this.base = base;
        this.player = player;
        this.npc = base.getNPC();
    }

    public void setChasing(LivingEntity chasing) {
        this.chasing = chasing;
    }

    public double getChanceToFlee() {
        return chanceToFlee;
    }

    public double getChanceToFleeWhenHealthLow() {
        return chanceToFleeWhenHealthLow;
    }

    public void ignoreEntity(LivingEntity entity) {
        if (!entitiesIgnoring.contains(entity)) entitiesIgnoring.add(entity);
    }

    public LivingEntity findBestTarget() {
        for (LivingEntity ent : base.getNearbyLivingEntities(player.getLocation(), 3)) {
            // If there's an entity right in front of the bot, just hit it
            if (Math.abs(Math.toDegrees(player.getLocation().getDirection().angle(player.getEyeLocation().toVector().subtract(ent.getLocation().toVector())))) < 80 && shouldTarget(ent) && (ent.getType() != EntityType.PLAYER || PlayerDamage.canTakePlayerDamage((Player) ent))) {
                currentTargetTime = 0;
                return ent;
            }
        }
        // Check status of currentTarget
        LivingEntity currentTarget = chasing;
        if (currentTargetTime > 20 * 6) currentTarget = null;
        else if (currentTarget == null || currentTarget.isDead() || !shouldTarget(currentTarget) || !(currentTarget.getType() != EntityType.PLAYER || PlayerDamage.canTakePlayerDamage((Player) currentTarget))) {
            currentTarget = null;
        } else {
            double d = currentTarget.getLocation().distanceSquared(player.getLocation());
            if (d > rangesquared) {
                currentTarget = null;
            }
        }
        // Instantly return currentTarget if still alive, within range, and picked up less than two seconds ago
        if (currentTarget != null && currentTargetTime < 20 * 2) {
            return currentTarget;
        }
        // Instantly return currentTarget if still alive, within range, and last attacker
        if (currentTarget != null && (ClassManager.getCombatOpponent(player) == currentTarget || Probability.get(50))) {
            return currentTarget;
        }
        LivingEntity closest = null;
        double closestEnemy = rangesquared + 50;
        for (LivingEntity ent : base.getNearbyLivingEntities(player.getLocation(), range)) {
            double dist = ent.getLocation().distanceSquared(player.getLocation());
            if (ClassManager.getCombatOpponent(player) == ent && dist < 5 * 5)
                return ent;
            // Add weight based on who hit the bot last
            if (Probability.get(25) && ClassManager.getCombatOpponent(player) == ent)
                dist -= 5 * 5;
            else //noinspection SuspiciousMethodCalls
                if (Probability.get(25) && ClassManager.getAssisters(player).contains(ent))
                    dist -= 2 * 2;
            double horizontalAngle = horizontalAngle(player.getLocation().getDirection(), ent.getLocation().toVector().subtract(player.getLocation().clone().toVector()));
            if ((shouldTarget(ent) && canSee(ent)) && dist + Math.min(50, horizontalAngle) < closestEnemy && (ent.getType() != EntityType.PLAYER || PlayerDamage.canTakePlayerDamage((Player) ent)) && base.getNearbyLivingEntities(ent.getLocation(), 8 - BotBaseTrait.r.nextDouble(3.5)).size() < 4 + BotBaseTrait.r.nextInt(3)) {
                closestEnemy = dist + Math.min(50, horizontalAngle);
                closest = ent;
            }
        }
        currentTargetTime = 0;
        return closest;
    }

    private boolean isIgnored(LivingEntity entity) {
        if (entity.getUniqueId().equals(player.getUniqueId())) {
            return true;
        }
        if (entity.getType() == EntityType.PLAYER) {
            if (((Player) entity).getGameMode() == GameMode.CREATIVE || ((Player) entity).getGameMode() == GameMode.SPECTATOR) {
                return true;
            }
        }
        return entitiesIgnoring.contains(entity);
    }

    private double getDamageFromWeapon(ItemStack weapon) {
        if (weapon == null || weapon.getType() == Material.AIR) return 1;
        int damage;
        Material type = weapon.getType();
        switch (type) {
            case STONE_SPADE:
            case WOOD_PICKAXE:
            case GOLD_PICKAXE:
                damage = 2;
                break;
            case IRON_SPADE:
            case WOOD_AXE:
            case GOLD_AXE:
            case STONE_PICKAXE:
                damage = 3;
                break;
            case WOOD_SWORD:
            case GOLD_SWORD:
            case DIAMOND_SPADE:
            case STONE_AXE:
            case IRON_PICKAXE:
                damage = 4;
                break;
            case STONE_SWORD:
            case IRON_AXE:
            case DIAMOND_PICKAXE:
                damage = 5;
                break;
            case IRON_SWORD:
            case DIAMOND_AXE:
                damage = 6;
                break;
            case DIAMOND_SWORD:
                damage = 7;
                break;

            default:
                damage = 1;
                break;
        }
        if (weapon.containsEnchantment(Enchantment.DAMAGE_ALL))
            damage += 1.25 * weapon.getEnchantmentLevel(Enchantment.DAMAGE_ALL);
        return damage;
    }

    private boolean canSee(LivingEntity entity) {
        return (entity.getType() != EntityType.PLAYER || !((Player) entity).isSneaking()) && Math.abs(Math.toDegrees(player.getLocation().getDirection().angle(player.getEyeLocation().toVector().subtract(entity.getLocation().toVector())))) < 120;
    }

    private boolean shouldTarget(LivingEntity entity) {
        return entity.getType() != EntityType.ARMOR_STAND && !isIgnored(entity) && !base.isInWater(entity) &&
                (entity.getLocation().getY() < player.getLocation().getY() + 2 || entity.isOnGround()) &&
                (entity.getType() != EntityType.PLAYER || TeamManager.getTeam((Player) entity) == null || TeamManager.getTeam((Player) entity) != TeamManager.getTeam(player)) &&
                (ServerLink.getServerType() != ServerType.skybattle || entity.getType() != EntityType.VILLAGER || TeamManager.getTeam(player) == null || ((Location) PlayerManager.get(player).getData("teamSpawnLoc").getValue()).distanceSquared(entity.getLocation()) > 15 * 15);
    }

    private void swingWeapon() {
        PlayerAnimation.ARM_SWING.play(player, 32);
    }

    private void punch(LivingEntity entity) {
        npc.faceLocation(entity.getLocation());
        timeSinceAttack = 0;
        player.setSprinting(false);
        swingWeapon();
        Inventory inv = npc.getTrait(Inventory.class);
        ItemStack[] items = inv.getContents();
        ItemStack heldWeapon = items[0] == null ? new ItemStack(Material.AIR) : items[0].clone();
        Bukkit.getPluginManager().callEvent(new EntityDamageByEntityEvent(player, entity, EntityDamageEvent.DamageCause.ENTITY_ATTACK, getDamageFromWeapon(heldWeapon)));
        if (Probability.get(10)) {
            // 1 in 10 chance to move to the opponent's left/right slightly after hitting
            if (Probability.get(10)) moveLeftAfterHit = !moveLeftAfterHit;
            Location side = player.getLocation();
            Vector unitDir = player.getLocation().getDirection().normalize().divide(new Vector(3, 1, 3));
            if (Math.abs(unitDir.getX()) > Math.abs(unitDir.getZ())) {
                unitDir.setZ(unitDir.getX());
                unitDir.setX(0);
            } else {
                unitDir.setX(unitDir.getZ());
                unitDir.setZ(0);
            }
            if (moveLeftAfterHit)
                side.add(unitDir.getX(), 0, unitDir.getBlockZ());
            else
                side.subtract(unitDir.getX(), 0, unitDir.getBlockZ());
            npc.getNavigator().setTarget(side);
            player.setSprinting(true);
        }
    }

    public void chaseTarget() {
        if (chasing == null || !chasing.isValid()) return;
        LivingEntity entity = chasing;
        base.swapToMelee();
        tryAttack(entity);
        if (npc.getNavigator().getTargetAsLocation() != null
                && ((npc.getNavigator().getTargetAsLocation().getWorld().equals(entity.getWorld())
                && npc.getNavigator().getTargetAsLocation().distanceSquared(entity.getLocation()) < 0.5 * 0.5)
                || (npc.getNavigator().getTargetAsLocation().getWorld().equals(base.getMovement().getPvpGoal().getWorld())
                && npc.getNavigator().getTargetAsLocation().distanceSquared(base.getMovement().getPvpGoal()) < 0.5 * 0.5))
                && Probability.get(50) && timeSinceAttack > 15) {
            // this makes the bot not turn its head to the player at every step to not look like aimbot
            return;
        }
        npc.getNavigator().getDefaultParameters().stuckAction(null);
        npc.getNavigator().setTarget(chasing.getEyeLocation());
        base.getMovement().setPvpGoal(entity.getEyeLocation());
        player.setSprinting(chasing.getLocation().distanceSquared(player.getLocation()) > 2 * 2);
    }

    private void tryAttack(LivingEntity entity) {
        if (swingWhileChasing || chasing.getLocation().distanceSquared(player.getLocation()) < 9 * 9) {
            if (timeSinceAttack >= attackRate || (timeSinceAttack != 0 && chasing.getLocation().distanceSquared(player.getLocation()) < 2 * 2 && Probability.get(80))) {
                double distSquared = chasing.getLocation().distanceSquared(player.getLocation());
                if (distSquared < 6.2 + BotBaseTrait.r.nextDouble(5)) {
                    timeSinceAttack = distSquared < 3 ? attackRate / 2 : 0;
                    launchAttack(entity);
                } else if (horizontalAngle(player.getLocation().getDirection(), entity.getLocation().toVector().subtract(player.getLocation().clone().toVector())) < 30) {
                    swingWeapon();
                    npc.faceLocation(entity.getLocation());
                    Bukkit.getPluginManager().callEvent(new PlayerAnimationEvent(player));
                    timeSinceAttack = attackRate / 2;
                }
            }
            if (Probability.get(1)) swingWhileChasing = false;
        } else if (Probability.get(25)) {
            swingWhileChasing = true;
            if (timeSinceAttack >= attackRate || (timeSinceAttack != 0 && chasing.getLocation().distanceSquared(player.getLocation()) < 2 * 2 && Probability.get(80))) {
                double distSquared = chasing.getLocation().distanceSquared(player.getLocation());
                if (distSquared < 6.2 + BotBaseTrait.r.nextDouble(5)) {
                    timeSinceAttack = distSquared < 3 ? attackRate / 2 : 0;
                    launchAttack(entity);
                } else if (horizontalAngle(player.getLocation().getDirection(), entity.getLocation().toVector().subtract(player.getLocation().clone().toVector())) < 30) {
                    // Swinging the sword makes the bot turn weirdly.
                    swingWeapon();
                    npc.faceLocation(entity.getLocation());
                    Bukkit.getPluginManager().callEvent(new PlayerAnimationEvent(player));
                    timeSinceAttack = attackRate / 2;
                }
            }
        }
    }

    public LivingEntity getEntityChasing() {
        return chasing;
    }

    private void launchAttack(LivingEntity entity) {
        punch(entity);
        chaseTarget();
    }

    public double horizontalAngle(Vector vec1, Vector vec2) {
        return Math.abs(Math.toDegrees(Math.acos((vec1.getX() * vec2.getX() + vec1.getZ() * vec2.getZ()) / (Math.sqrt(vec1.getX() * vec1.getX() + vec1.getZ() * vec1.getZ()) * Math.sqrt(vec2.getX() * vec2.getX() + vec2.getZ() * vec2.getZ())))));
    }
}

package serverlink.bot.speech;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;

public class ResponseToPvpChallenge {
    private static final SplittableRandom r = new SplittableRandom();
    // Responses are lowercase first letter with punctuation except special cases.
    private static final List<String[]> responses = new ArrayList<String[]>() {{
        add(new String[]{"alright m8 i'll rek u.", "alright m8 i'll rek u gud.",
                "alright m8 ill rek u.", "alright m8 ill rek u gud.", "alright mate i'll rek u.",
                "alright mate i'll rek u gud.", "alright mate ill rek u.", "alright mate ill rek u gud."});
        add(new String[]{"m8 i'll rek u.", "m8 i'll rek u gud.", "m8 ill rek u.",
                "m8 ill rek u gud.", "mate i'll rek u.", "mate i'll rek u gud.",
                "mate ill rek u.", "mate ill rek u gud."});
        add(new String[]{"come at me.", "come at me bud."});
        add(new String[]{"better find your hiding place now.",
                "start hiding cause ur dead as soon as I touch u.",
                "better start looking for a hiding place m8.",
                "better start hiding bud."});
        add(new String[]{"fite me bud.", "fight me.", "fight me then.", "fite me then.",
                "come fite me bud.", "come fight me bud.", "come fite me then.", "come fight me then."});
        add(new String[]{"ok, just don't tase me bro!", "just don't tase me bro.",
                "DON'T TASE ME BRO!-"});
        add(new String[]{"rly you don't stand a chance.", "you don't really stand a chance tho."});
        add(new String[]{"nah you can't handle dis.", "nah you can't handle this.",
                "nah you can't handle my skills.", "nah you can't handle my skillz.",
                "you can't handle dis.", "you can't handle this.", "you can't handle my skills.",
                "you can't handle my skillz.", "you can't handle dis m8.",
                "you can't handle this m8.", "you can't handle my skills m8.",
                "you can't handle my skillz m8."});
        add(new String[]{"Haha xD, you couldn't kill me if you tried Lol.",
                "Haha xD, you couldn't kill me if you tried lol.",
                "you couldn't kill me if you tried lol.", "you couldn't kill me if you tried."});
        add(new String[]{"prepare to be demolished!-", "brace for recking!", "k. get ready to get rekt then"});
        add(new String[]{"kys or il do it 4 u", "u might as well jump in lava right now mate"});
    }};

    public static String getVariation() {
        String[] generalResponse = responses.get(r.nextInt(responses.size()));
        return generalResponse[r.nextInt(generalResponse.length)];
    }
}

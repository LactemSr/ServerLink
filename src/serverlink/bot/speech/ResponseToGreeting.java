package serverlink.bot.speech;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;

public class ResponseToGreeting {
    private static final SplittableRandom r = new SplittableRandom();
    // Responses are lowercase first letter with punctuation except special cases.
    private static final List<String[]> responses = new ArrayList<String[]>() {{
        add(new String[]{"hi."});
        add(new String[]{"hi."});
        add(new String[]{"hi."});
        add(new String[]{"hello."});
        add(new String[]{"hello."});
        add(new String[]{"hello."});
        add(new String[]{"hey."});
        add(new String[]{"hey."});
        add(new String[]{"hey."});
        add(new String[]{"what's up?-", "whats up?-", "wassup?-", "sup", "wazzup?-"});
        add(new String[]{"what's up?-", "whats up?-", "wassup?-", "sup", "wazzup?-"});
        add(new String[]{"what's up?-", "whats up?-", "wassup?-", "sup", "wazzup?-"});
        add(new String[]{"heyder", "hie"});
        add(new String[]{"who's this?", "who are you?-", "do I know you?"});
        add(new String[]{"can I help you?-", "you need help?-", "hello, can I help you?-",
                "hey, do you need something?-", "hello. do you need something?-"});
        add(new String[]{"hey how are doin?-", "hey how are doing?-", "hey how are ya?-",
                "how are you?-", "hello, how are you?-", "hey how're you doing?-"});
        add(new String[]{"hey how's it going?-", "hello, how are you?-", "hey how are you?-",
                "hey, how are u?-", "hey how r u?-", "hey hows it going?-", "hey, how it going?-"});
        add(new String[]{"hey bud", "hello mate", "hello m8", "hey m8 what's up?-",
                "hey m8 whats up?-"});
        add(new String[]{"oi wassup?-", "hey wassup?-", "ey wassup?-", "ey wazzup?-",
                "hey whats up?-", "hey what's up?-"});
        add(new String[]{"hi sup.", "hi, sup?", "hey sup.", "Hey 'sup."});
        add(new String[]{"wat", "wat?", "what?-", "wut?-", "wut"});
        add(new String[]{"what do you want?-"});
        add(new String[]{"Im busy.", "I'm busy.", "not now, busy.", "not now dude.",
                "not right now I'm busy.", "not now I'm busy.", "I'm busy ok?-"});
        add(new String[]{"do not disturb.", "nope."});
        add(new String[]{"dont feel like talking now.", "don't feel like talking now.",
                "I dont feel like talking now.", "I don't feel like talking right now.",
                "Im not up for a conversation man.", "I'm not up for a conversation atm, sry.",
                "I'm not up for a conversation atm, sry bro.", "I'm not up for a conversation atm, sry bud."});

    }};

    public static String getVariation() {
        String[] generalResponse = responses.get(r.nextInt(responses.size()));
        return generalResponse[r.nextInt(generalResponse.length)];
    }
}

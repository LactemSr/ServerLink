package serverlink.bot.speech;

import serverlink.util.Probability;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;

public class ResponseToGG {
    private static final SplittableRandom r = new SplittableRandom();
    // Responses are lowercase first letter with punctuation except special cases.
    private static final List<String[]> responses = new ArrayList<String[]>() {{
        add(new String[]{"gg.", "gg!", "GG.", "GG!"});
        add(new String[]{"you too.", "you too!", "same to you."});
        add(new String[]{"gg m8.", "gg m8!", "gg m9.", "gg m9!", "gg mate.", "gg mate!",});
        add(new String[]{"good game.", "good game m8.", "good game mate.", "good game bud."});
        add(new String[]{"gg no re", "GG no re", "GG no RE"});
        add(new String[]{"gg come again", "gg, let's go again.", "gg, again!-"});
        add(new String[]{"well played.", "gg, wp.", "gg, well played.", "gg wp",
                "well played!", "well played my friend.", "well played bud.",
                "well played m8.", "well played mate."});
        add(new String[]{"yeah gg.", "yeah, gg.", "yeah GG.", "yeah, GG."});
        add(new String[]{"yep gg mate.", "yep, gg mate."});
        add(new String[]{"yeah good game.", "yeah, good game."});
        add(new String[]{"gg i guess.", "gg I guess.", "I guess",
                "I guess you could call that gg.", "I guess that was gg."});
        add(new String[]{"not really.", "that wasnt really gg.", "that wasn't really gg",
                "not really gg", "not exactly gg", "wasnt exactly gg", "wasn't exactly gg",
                "that wasnt exactly gg", "that wasn't exactly gg"});
        add(new String[]{"if you wanna call that gg", "you can call it that if you want...-"});
        add(new String[]{"if you say so.", "if you say so...-", "if u say so.", "if u say so...-",
                "whatever you say...-", "whatever u say...-", "whatever u say."});
        add(new String[]{"thanks.", "thanks, you too.", "thanks, you too!-", "thanks man.",
                "thanks man, you 2."});
    }};

    public static String getVariation() {
        if (Probability.get(10)) return "gg";
        String[] generalResponse = responses.get(r.nextInt(responses.size()));
        return generalResponse[r.nextInt(generalResponse.length)];
    }
}

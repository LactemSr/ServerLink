package serverlink.bot.speech;

public enum SpeechStyle {
    NO_CAP_NO_PUNC,
    NO_CAP_YES_PUNC,
    YES_CAP_NO_PUNC,
    YES_CAP_YES_PUNC;
}

package serverlink.bot.speech;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;

public class ResponseToBeingKilled {
    private static final SplittableRandom r = new SplittableRandom();
    // Responses are lowercase first letter with punctuation except special cases.
    private static final List<String[]> responses = new ArrayList<String[]>() {{
        add(new String[]{"nice hax.", "nice hax bud.", "nice hax m8.", "nice hax mate.",
        });
        add(new String[]{"gg", "GG", "gg m8.", "GG mate.", "GG m8.", "gg no re", "GG no re"});
        add(new String[]{"fite me again.", "fite me again m8.", "fite me again mate.",
                "lets do a rematch.", "let's do a rematch.", "let's do it again."});
        add(new String[]{"close one!", "that was a close one!", "that was so close!",
        });
        add(new String[]{"i almost beat you.", "i almost beat u.", "i almost had it!",
                "i almost won!", "i almost won that!"});
        add(new String[]{"come at me ill rek u.", "come at me i'll rek u."});
        add(new String[]{"dude wtf i was typing.", "bro wtf i was typing.",
                "hey wtf i was typing.", "yo wtf i was typing.", "dude wtf i was typing!",
                "bro wtf i was typing!", "hey wtf i was typing!", "yo wtf i was typing!",
                "dude i was typing.", "bro i was typing.", "hey i was typing.",
                "yo i was typing.", "dude i was typing!", "bro i was typing!",
                "hey i was typing!", "yo i was typing!"});
        add(new String[]{"WOW nice hacks bro", "WOW nice hax bro.", "WOW nice hax bro!",
                "nice client man.", "nice hacks man.", "yo what client do you use?-",
        });
        add(new String[]{"wtf ill get my revenge.", "wtf im coming back.",
                "im coming back.", "im coming back again.",});
        add(new String[]{"good battle m8.", "good battle mate.", "good battle bud."});
        add(new String[]{"xD I got destroyed", "i got destroyed xD", "i got rekt lel",
                "i got rekt there xD", "i got rekt just now xD", "i got rekt there wow.",
                "i got rekt just now omg."});
    }};

    public static String getVariation() {
        String[] generalResponse = responses.get(r.nextInt(responses.size()));
        return generalResponse[r.nextInt(generalResponse.length)];
    }
}

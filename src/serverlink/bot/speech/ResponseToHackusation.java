package serverlink.bot.speech;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;

public class ResponseToHackusation {
    private static final SplittableRandom r = new SplittableRandom();
    // Responses are lowercase first letter with punctuation except special cases.
    private static final List<String[]> responses = new ArrayList<String[]>() {{
        add(new String[]{"whos hacking?-", "who's hacking?-", "hacker, where?-"});
        add(new String[]{"dude i don't hack u just suck.", "dude i don't hack you just suck."});
        add(new String[]{"yea rite m8 im just 2 good 4 u"});
        add(new String[]{"k.-", "K.-", "k.", "whatever u say bro."});
        add(new String[]{"if you say so.", "if u say so.", "if you say so m8.",
                "k mate if u say so.", "if you say so...-", "if u say so...-"});
        add(new String[]{"imeaniguess", "I mean i guess.", "I mean I guess...-",
                "imeaniguess...-"});
        add(new String[]{"no hax just skillz.", "skillz not hacks", "skillz not hackz",
                "these are all skills my dude.", "bro these are skills not hacks."});
        add(new String[]{"if anyone hacks it's you!"});
        add(new String[]{"no hacks here.", "hacks? where?", "hacks? I don't see any hacks."});
        add(new String[]{"nah I'm legit.", "nah I'm leqit.", "nah, I play legit.",
                "I play legit m8.", "bruh I'm for real."});
        add(new String[]{"bro I don't hack, get your stuff right.",
                "bro I don't hack, get ur stuff right."});
        add(new String[]{"nice try i just too good.", "nice try im just too good.",
                "nice try I'm just too good."});
        add(new String[]{"nah, I just don't suck at pvp like you.",
                "nah, I'm just a better pvper than you.", "nah, I'm just better at pvp than you.",
                "nah, I'm just better at pvp.", "I'm just better at pvp."});
        add(new String[]{"get outta here, I dont hax."});
        add(new String[]{"bruh, please."});
        add(new String[]{"ur just a sore loser.", "your just a sore loser.",
                "you're just a sore loser.", "sore loser."});
        add(new String[]{"loser.", "loser...-"});
        add(new String[]{"no, u hack more than hillary's emails.",
                "I don't hack, u hack harder than hillary's email server.",
                "you hack harder than hillary's email server.",
                "you hack more than hillary's email server.",
                "you hack more than hillary's emails.",});
    }};

    public static String getVariation() {
        String[] generalResponse = responses.get(r.nextInt(responses.size()));
        return generalResponse[r.nextInt(generalResponse.length)];
    }
}

package serverlink.bot.speech;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;

public class ResponseToCall {
    private static final SplittableRandom r = new SplittableRandom();
    // Responses are lowercase first letter with punctuation except special cases.
    private static final List<String[]> responses = new ArrayList<String[]>() {{
        add(new String[]{"what do u want?-"});
        add(new String[]{"yes??-", "yes?-"});
        add(new String[]{"yes??-", "yes?-"});
        add(new String[]{"wut m8", "wut?-", "what?-"});
        add(new String[]{"wtf u want?-", "wtf do you want?-"});
        add(new String[]{"go bother some1 else", "go bother someone else",
                "bother some1 else", "bother someone else"});
        add(new String[]{"what's up?-", "whats up?-", "wassup?-", "sup", "wazzup?-"});
        add(new String[]{"what's up?-", "whats up?-", "wassup?-", "sup", "wazzup?-"});
        add(new String[]{"what's up?-", "whats up?-", "wassup?-", "sup", "wazzup?-"});
        add(new String[]{"yeah?-", "ye?-", "yea?-"});
        add(new String[]{"what is it?-"});
        add(new String[]{"hey.", "hi.", "hello."});
        add(new String[]{"hey.", "hi.", "hello."});
        add(new String[]{"hey.", "hi.", "hello."});
        add(new String[]{"hey.", "hi.", "hello."});
        add(new String[]{"who's this?", "who are you?-", "do I know you?"});
        add(new String[]{"can I help you?-", "you need help?-", "hello, can I help you?-",
                "hey, do you need something?-", "do you need something?-"});
        add(new String[]{"Im busy.", "I'm busy.", "not now, busy.", "not now dude.",
                "not right now I'm busy.", "not now I'm busy.", "I'm busy ok?-"});
        add(new String[]{"do not disturb.", "nope."});
        add(new String[]{"dont feel like talking now.", "don't feel like talking now.",
                "I dont feel like talking now.", "I don't feel like talking right now.",
                "Im not up for a conversation man.", "I'm not up for a conversation atm, sry.",
                "I'm not up for a conversation atm, sry bro.", "I'm not up for a conversation atm, sry bud."});
    }};

    public static String getVariation() {
        String[] generalResponse = responses.get(r.nextInt(responses.size()));
        return generalResponse[r.nextInt(generalResponse.length)];
    }
}

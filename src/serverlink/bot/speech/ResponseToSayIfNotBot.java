package serverlink.bot.speech;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;

public class ResponseToSayIfNotBot {
    private static final SplittableRandom r = new SplittableRandom();
    // Responses are lowercase first letter with punctuation except special cases.
    private static final List<String[]> responses = new ArrayList<String[]>() {{
        add(new String[]{"cool.", "that's cool.", "cool man.", "cool...-", "coool"});
        add(new String[]{"fuck off.", "yo fuck off I'm not a bot.", "fuck off I'm not a bot.",
                "piss off.", "piss off, I'm not a bot.", "I'm not a bot...", "fuck off m8."});
        add(new String[]{"I'm not a bot.", "I'm not a bot...-", "bro I'm not a bot.",
                "dude I'm not a bot."});
        add(new String[]{"good for you.", "that's good for you bro.", "good for you bro."});
        add(new String[]{"wtf u want?-", "wtf do you want?-", "wtf u want dude?-",
                "wtf do u want dude?-", "wtf do you want dude?-"});
        add(new String[]{"go bother some1 else", "go bother someone else",
                "bother some1 else", "bother someone else"});
        add(new String[]{"what's wrong with you?-", "whats wrong with u?-",
                "what's wrong with u bro?-"});
        add(new String[]{"are you ok?-"});
        add(new String[]{"I'm not a bot."});
        add(new String[]{"Im busy.", "I'm busy.", "not now, busy.", "not now dude.",
                "not right now I'm busy.", "not now I'm busy.", "I'm busy ok?-"});
        add(new String[]{"nope.", "no.", ".....-", "I'm not saying that.", "I'm not going to say that."});
        add(new String[]{"nope.", "no.", ".....-", "I'm not saying that.", "I'm not going to say that."});
    }};

    public static String getVariation() {
        String[] generalResponse = responses.get(r.nextInt(responses.size()));
        return generalResponse[r.nextInt(generalResponse.length)];
    }
}

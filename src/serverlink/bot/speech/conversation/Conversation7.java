package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation7 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:anyone else like rock or heavy metal? ;-;",
                "anyone else like rock?-"});
        add(new String[]{"1:anyone who likes edm?-", "any edm fans on?-"});
        add(new String[]{"1:anyone sub for sub?-", "how about sub for sub?-"});
        add(new String[]{"1:any mods on?-", "are there any mods on?-"});
        add(new String[]{"1:anyone want to be friends?-", "who wants to be my friend?-",
                "anyone have a gaming pc?-", "say if u have a gaming pc?-"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:idk?-", "i dunno?-", "idk maybe?-"});
        add(new String[]{"2:maybe...-", "perhaps...-", "crap", "darn", "dang"});
        add(new String[]{"2:oh well.", "ah well."});
        add(new String[]{"2:ah", "sorry :-;"});
        add(new String[]{"2:oopsies :-)"});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:nah", "why do they always do this?-"});
        add(new String[]{"3:go ask someone else"});
        add(new String[]{"3:that went well...-", "that was ok...-"});
        add(new String[]{"3:ah well, next time then.", "next time then.," +
                "next time then i guess.", "next time i guess."});
        add(new String[]{"3:try again later.", "maybe try later.",
                "ask again later.", "ask again some other time."});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3);
    }
}

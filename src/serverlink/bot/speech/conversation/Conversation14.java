package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation14 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:woah datboi xD", "omg lol", "wow u were right", "lol i should have listened"});
        add(new String[]{"1:wow smh"});
        add(new String[]{"1:is he done."});
        add(new String[]{"1:just do it fam."});
        add(new String[]{"1:is he gonna do it??-"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:omfg...-"});
        add(new String[]{"2:no way he actually did it."});
        add(new String[]{"2:what the actual...?-"});
        add(new String[]{"2:lol"});
        add(new String[]{"2:hahah xD"});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:yep"});
        add(new String[]{"3:he just did it...-"});
        add(new String[]{"3:lol that really just happened."});
        add(new String[]{"3:*locks door*"});
        add(new String[]{"3:I'm dying."});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3);
    }
}

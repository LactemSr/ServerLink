package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation13 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:any boy wanna be my bf?",
                "any girl want to be my girlfriend?"});
        add(new String[]{"1:wanna go on a date anyone?",
                "does anyone want to maybe date me?-"});
        add(new String[]{"1:I'm looking for hot dates. Anyone interested?-",
                "any hot people on here looking to hook up??-"});
        add(new String[]{"1:who wants to hookup after some kitpvp?-",
                "how about after kitpvp you and me... you know?-"});
        add(new String[]{"1:anyone wanna wife me up?-",
                "who wants to wife me up? I'm lonely."});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"1:jk jk I already got someone.",
                "lol I already have someone actually.",
                "lawl I'm already in a relationship"});
        add(new String[]{"1:jk guyz don't worry about me.",
                "I'm joking tho."});
        add(new String[]{"1:plz...-",
                "plz guys."});
        add(new String[]{"1:pls I beg of u.",
                "please someone help me out"});
        add(new String[]{"1:Anyone?",
                "Someone...anyone"});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"2:nah", "hard pass.. very hard pass"});
        add(new String[]{"2:that's really weird bro.",
                "so messed up."});
        add(new String[]{"2:THIS IS NOT TINDER!!",
                "DO NOT DO THAT ON MINECRAFT PLEASE!",
                "STOP ASKING FOR GFS ON MINECRAFT!!"});
        add(new String[]{"2:try an actual dating site.",
                "how about you try an actual dating site?"});
        add(new String[]{"2:we're on a minecraft server...",
                "this is mc... not tinder"});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[]{"3:really?", "really dude?-", "ur really one of those peopel?-"});
        add(new String[]{"3:pm me anyway.", "still pm me", "idc if ur joking just message me"});
        add(new String[]{"3:go away dude.", "just leave"});
        add(new String[]{"3:take that sh** back to mineplex.",
                "you can do that on mineplex, not ac"});
        add(new String[]{"3:this is not the place lmao",
                "Alphacraft is not the right place for gfs lol"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3, line4);
    }
}

package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation5 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:bruh who wants my insta?"});
        add(new String[]{"1:someone add me on skype"});
        add(new String[]{"1:yo who wants a new twitter follower?"});
        add(new String[]{"1:guys who wants to add my insta?-"});
        add(new String[]{"1:someone pls follow me on instagram??-"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:pm me it."});
        add(new String[]{"2:sure lol message me."});
        add(new String[]{"2:no thanks...-"});
        add(new String[]{"2:im good."});
        add(new String[]{"2:ya no."});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:lol"});
        add(new String[]{"3:weird"});
        add(new String[]{"3:interesting...-"});
        add(new String[]{"3:alright then"});
        add(new String[]{"3:thats... ok...-"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3);
    }
}

package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation13_1 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:ladies I'm accepting girlfriend apps now, pm if interested.",
                "message me if you want to go out with me."});
        add(new String[]{"1:who wants to go out with me?-",
                "pls tell me if you want to go out with me."});
        add(new String[]{"1:if I beat u in a 1 v 1 will you go out with me?-",
                "how about if I win we go on a date??"});
        add(new String[]{"1:will pay for gfs, pm me if ur up to it.",
                "if you date me I will pay good money"});
        add(new String[]{"1:anyone wanna play gf and bf with me irl?-",
                "someone who wants to play boyfriend with me?-"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:nah", "hard pass", "defintily not interested"});
        add(new String[]{"2:that's kinda odd."});
        add(new String[]{"2:lol", "heh", "lol thanks but no thanks"});
        add(new String[]{"2:go on match or eharmony (or tinder lol)"});
        add(new String[]{"2:minecraft isn't a dating platform bro.",
                "this is not where you date!!", "MC is not for dating omg"});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:really?", "rlly man?... really?-"});
        add(new String[]{"3:pm me.", "./msg me"});
        add(new String[]{"3:go away", "git gone"});
        add(new String[]{"3:gtfo with that sh*t", "don't come here with that.",
                "we don't need hookups on ac"});
        add(new String[]{"3:this isn't the place for that", "just no", "just stop"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3);
    }
}

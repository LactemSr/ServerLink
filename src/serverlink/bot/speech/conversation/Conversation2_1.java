package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation2_1 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:who saw that one movie?-"});
        add(new String[]{"1:did anyone see yesterday's?-"});
        add(new String[]{"1:anyone saw the new one yet?-"});
        add(new String[]{"1:has anyone seen last week's episode?-"});
        add(new String[]{"1:who liked the show's latest episode?-"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:I did!-"});
        add(new String[]{"2:meee!!1"});
        add(new String[]{"2:yup.", "it was nice."});
        add(new String[]{"2:you know it m8.", "who hasn't??"});
        add(new String[]{"2:ofc I did.", "of course bro", "m8 ofc i did."});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:same.", "same man", "yeah same."});
        add(new String[]{"3:I didn't...-", "i still haven't."});
        add(new String[]{"3:sure did.", "yessir I saw it.", "you already know I did."});
        add(new String[]{"3:yepee."});
        add(new String[]{"3:I SAW IT!-"});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[]{"1:lol good to know.", "good to know I guess",
                "K.-"});
        add(new String[]{"1:haha me too.", "ha me too.", "me too man.", "me too lol"});
        add(new String[]{"1:wow.", "wowzers.", "omg so many ppl did."});
        add(new String[]{"1:i saw it but I actually didnt like it that much :o",
                "saw it... wasn't good", "it wasn't really good"});
        add(new String[]{"1:wish I had known what was coming...-"});
    }};
    private static List<String[]> line5 = new ArrayList<String[]>() {{
        add(new String[]{"1:yeah...-"});
        add(new String[]{"1:ikr", "i know right bro?-"});
        add(new String[]{"1:right?-"});
        add(new String[]{"1:yep", "exactly.", "same tbh"});
        add(new String[]{"1:ok it wasn't that bad imo."});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3, line4, line5);
    }
}

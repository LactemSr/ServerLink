package serverlink.bot.speech.conversation;

import net.citizensnpcs.api.npc.NPC;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.bot.Bot;
import serverlink.bot.BotBaseTrait;
import serverlink.bot.BotManager;
import serverlink.util.Probability;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;

public class ConversationManager {
    private static final SplittableRandom r = new SplittableRandom();
    private static int typingTimeProgress = 0;
    private static int typingTimeTarget = 0;
    private static Conversation activeConversation = null;
    private static boolean lastLineSaid = true;

    public static void startConversation() {
        // choose conversation
        Conversation convo = getRandomConversation();

        // choose part actors to participate in the conversation
        Bot[] partActors = new Bot[convo.getParts()];
        int partsAssigned = 0;
        int iterations = 0;
        while (partsAssigned < convo.getParts() - 1) {
            if (iterations > 200) break;
            for (Bot bot : BotManager.getBots()) {
                iterations++;
                if (iterations > 200) break;
                if (!bot.getNpc().getTrait(BotBaseTrait.class).ready || !bot.getNpc().isSpawned()
                        || bot.getNpc().getEntity() == null) continue;
                boolean botAlreadyAssigned = false;
                for (Bot actor : partActors) if (actor != null && actor == bot) botAlreadyAssigned = true;
                if (botAlreadyAssigned || Probability.get(50)) continue;
                partActors[partsAssigned] = bot;
                partsAssigned++;
                if (partsAssigned == convo.getParts()) break;
            }
        }
        convo.setPartActors(partActors);

        // start conversation
        activeConversation = convo;
        scheduleNextLine();
    }

    public static void scheduleNextLine() {
        lastLineSaid = false;
        Bot bot = activeConversation.getNextPartActor();
        if (bot == null || !BotManager.getBots().contains(bot)) {
            activeConversation = null;
            return;
        }
        String nextLine = activeConversation.getNextLine();
        NPC npc = bot.getNpc();
        typingTimeProgress = 0;
        typingTimeTarget = 10 + ((int) (nextLine.length() / 4.5 + r.nextDouble(nextLine.length() / 15.0 + 1))) * 20;
        new BukkitRunnable() {
            @Override
            public void run() {
                if (npc == null || !npc.isSpawned() || npc.getEntity() == null || npc.getEntity().isDead()) {
                    typingTimeProgress = 0;
                    typingTimeTarget = 0;
                    lastLineSaid = true;
                    activeConversation = null;
                    cancel();
                    return;
                }
                npc.getNavigator().cancelNavigation();
                npc.getNavigator().setTarget(null);
                npc.getNavigator().cancelNavigation();
                npc.getNavigator().setPaused(true);
                typingTimeProgress += 1;
                if (typingTimeProgress < typingTimeTarget) return;
                cancel();
                if (activeConversation.sayLine()) activeConversation = null;
                typingTimeProgress = 0;
                typingTimeTarget = 0;
                lastLineSaid = true;
                npc.getNavigator().setPaused(false);
            }
        }.runTaskTimer(ServerLink.plugin, 1, 1);
    }

    public static boolean isConversationActive() {
        return activeConversation != null;
    }

    public static boolean wasLastLineSaid() {
        return lastLineSaid;
    }

    private static Conversation getRandomConversation() {
        List<ConversationCreator> convos = new ArrayList<>();
        convos.add(new Conversation1());
        convos.add(new Conversation2());
        convos.add(new Conversation3());
        convos.add(new Conversation4());
        convos.add(new Conversation5());
        convos.add(new Conversation6());
        convos.add(new Conversation7());
        convos.add(new Conversation8());
        convos.add(new Conversation9());
        convos.add(new Conversation10());
        convos.add(new Conversation11());
        convos.add(new Conversation12());
        convos.add(new Conversation13());
        convos.add(new Conversation14());
        convos.add(new Conversation15());
        convos.add(new Conversation16());
        convos.add(new Conversation17());
        convos.add(new Conversation18());
        convos.add(new Conversation19());
        convos.add(new Conversation20());
        convos.add(new Conversation21());
        convos.add(new Conversation22());
        ConversationCreator convoCreator = convos.get(r.nextInt(convos.size()));
        if (convoCreator instanceof Conversation3 || convoCreator instanceof Conversation4
                || convoCreator instanceof Conversation5 || convoCreator instanceof Conversation8
                || convoCreator instanceof Conversation13 || convoCreator instanceof Conversation18
                || convoCreator instanceof Conversation20) {
            convos = new ArrayList<>();
            convos.add(new Conversation1());
            convos.add(new Conversation2());
            convos.add(new Conversation3());
            convos.add(new Conversation4());
            convos.add(new Conversation5());
            convos.add(new Conversation6());
            convos.add(new Conversation7());
            convos.add(new Conversation8());
            convos.add(new Conversation9());
            convos.add(new Conversation10());
            convos.add(new Conversation11());
            convos.add(new Conversation12());
            convos.add(new Conversation13());
            convos.add(new Conversation14());
            convos.add(new Conversation15());
            convos.add(new Conversation16());
            convos.add(new Conversation17());
            convos.add(new Conversation18());
            convos.add(new Conversation19());
            convos.add(new Conversation20());
            convos.add(new Conversation21());
            convos.add(new Conversation22());
            convoCreator = convos.get(r.nextInt(convos.size()));
        }
        List<ConversationCreator> subConvos = new ArrayList<>();
        subConvos.add(convoCreator);
        if (convoCreator instanceof Conversation2) {
            subConvos.add(new Conversation2_1());
            subConvos.add(new Conversation2_2());
            subConvos.add(new Conversation2_3());
        } else if (convoCreator instanceof Conversation5) {
            subConvos.add(new Conversation5_1());
        } else if (convoCreator instanceof Conversation13) {
            subConvos.add(new Conversation13_1());
        }
        return subConvos.get(r.nextInt(subConvos.size())).createConversation();
    }
}

package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation18 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:anyone else on fire rn?", "\"lit\"... literally",
                "can't believe u fell for that."});
        add(new String[]{"1:I just actually stole someone's girl.", "mmm dat kill was so good."});
        add(new String[]{"1:I been playing for so long I just had to pee in my pants..",
                "when you gotta go... you gotta go"});
        add(new String[]{"1:so… just got back from your mom's", "ur mom's house is pretty nice btw",
                "i really like ur mom's bed... so soft.."});
        add(new String[]{"1:guyz I'm on tv!", "i'm on channel 7 if u live in alaska.",
                "guys i'm on tv right now check it out!!"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:yeah, I realized it was a joke.", "i know ur jk",
                "is that supposed to be funny?-"});
        add(new String[]{"2:he's obv joking.", "not funny dude", "ur not funny.",
                "it's not that funny."});
        add(new String[]{"2:u better be jk.", "i really hope ur just kidding.",
                "that better be a joke.", "don't mess around like that."});
        add(new String[]{"2:can't tell if trolling or just an idiot.",
                "fork off bud."});
        add(new String[]{"2:cool bro.", "that's cool i guess", "good for u.",
                "ok good for you mate."});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:and since you were to lazy to type \"anyone\""});
        add(new String[]{"3:i was to lazy to type \"yes\""});
        add(new String[]{"3:yo yo yo"});
        add(new String[]{"3:*dabs*"});
        add(new String[]{"3:lit fam"});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[]{"4:What does that prove?"});
        add(new String[]{"4:how dare you xD"});
        add(new String[]{"4:cant tell if this is serious or not.", "are you serious or not?-"});
        add(new String[]{"4:...wait"});
        add(new String[]{"4:obviously…-"});
    }};

    public Conversation createConversation() {
        return new Conversation(line1, line2, line3, line4);
    }
}

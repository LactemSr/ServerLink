package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation2_2 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[] {"1:was the latest episode any good?-"});
        add(new String[] {"1:anyone think the new version was good?-"});
        add(new String[] {"1:was the new one good?-"});
        add(new String[] {"1:was it as good as the last one though?-"});
        add(new String[] {"1:who even liked this week's?-"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[] {"2:nah."});
        add(new String[] {"2:incredible."});
        add(new String[] {"2:wasn't the best but I enjoyed it."});
        add(new String[] {"2:it was pretty frickin good man."});
        add(new String[] {"2:can't lie it was simply spectacular."});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[] {"3:I disagree."});
        add(new String[] {"3:quite the opposite actually."});
        add(new String[] {"3:not exactly...-"});
        add(new String[] {"3:that's putting it lightly."});
        add(new String[] {"3:exactly."});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[] {"1:well don't spoil it for me, I didnt watch it yet."});
        add(new String[] {"1:still haven't seen it."});
        add(new String[] {"1:pls don't spoil it though."});
        add(new String[] {"1:don't give it away bc I didn't see it yet."});
        add(new String[] {"1:just don't spoil anything until I see it myself."});
    }};
    private static List<String[]> line5 = new ArrayList<String[]>() {{
        add(new String[] {"3:ok"});
        add(new String[] {"3:fiine"});
        add(new String[] {"3:sure...-"});
        add(new String[] {"3:alright"});
        add(new String[] {"3:cool"});
    }};
    private static List<String[]> line6 = new ArrayList<String[]>() {{
        add(new String[] {"1:i'm watching it tonight."});
        add(new String[] {"1:Im still gonna watch it."});
        add(new String[] {"1:well I still want to see it."});
        add(new String[] {"1:tonight is the night I see it!"});
        add(new String[] {"1:I'm going later today."});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3, line4, line5, line6);
    }
}

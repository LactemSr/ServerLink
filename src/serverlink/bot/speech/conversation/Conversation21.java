package serverlink.bot.speech.conversation;

import serverlink.util.Probability;

import java.util.ArrayList;
import java.util.List;

class Conversation21 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:is %offence% mutable?-"});
        add(new String[]{"1:can %offence% get me banned?-"});
        add(new String[]{"1:will owner ban me for %offence%?"});
        add(new String[]{"1:is %offence% allowed on here?-"});
        add(new String[]{"1:%offence% is allowed right?"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:no", "absolutely not.", "no bro", "pls no", "nah"});
        add(new String[]{"2:don't do it.", "just don't do it."});
        add(new String[]{"2:idc", "idgaf if u do.", "I don't care either way."});
        add(new String[]{"2:sure, it doesn't bother me.", "fine by me.", "it's fine with me if u do."});
        add(new String[]{"2:you do you bro.", "you do you.", "you just do you ok?-"});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:why not?", "i mean why not?-", "why wouldn't it?", "i mean i guess...-"});
        add(new String[]{"3:that's kinda a grey area.",
                "just don't get caught!", "as long as no mods notice…", "make sure the mods don't notice."});
        add(new String[]{"3:I wouldn't risk it if I were you.", "it's not worth it.",
                "not worth it.", "it's just not worth it in my opinion.", "I wouldn't risk it.", "wouldnt risk it."});
        add(new String[]{"3:ya no...-", "how about no", "yeah no", "just no."});
        add(new String[]{"3:are you crazy?", "you crazy bro?", "you outta yo mind boi?", "mate r u crazy?-"});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[]{"4:What does that prove?"});
        add(new String[]{"4:how dare you xD"});
        add(new String[]{"4:cant tell if this is serious or not."});
        add(new String[]{"4:...wait"});
        add(new String[]{"4:obviously…-"});
    }};

    public Conversation createConversation() {
        List<String[]> _line1 = line1;
        String offence = "swearing";
        double selector = Probability.selector();
        if (selector < 6) offence = "saying f u";
        else if (selector < 12) offence = "saying ass";
        else if (selector < 18) offence = "having health tags mod";
        else if (selector < 24) offence = "using reach";
        else if (selector < 30) offence = "having fullbright mod";
        else if (selector < 36) offence = "using the smart moving mod";
        else if (selector < 42) offence = "speaking Spanish";
        else if (selector < 48) offence = "talking in Russian";
        else if (selector < 54) offence = "calling someone dumb";
        else if (selector < 60) offence = "calling this guy an idiot";
        else if (selector < 66) offence = "using an xray texture pack";
        else if (selector < 72) offence = "using wurst for fullbright";
        else if (selector < 78) offence = "using weepcraft";
        else if (selector < 84) offence = "using nodus just for friends";
        for (int i = 0; i < _line1.size(); i++) {
            String[] variations = _line1.get(i);
            for (int j = 0; j < variations.length; j++)
                variations[j] = variations[j].replaceAll("%offence%", offence);
            _line1.set(i, variations);
        }
        return new Conversation(_line1, line2, line3, line4);
    }
}

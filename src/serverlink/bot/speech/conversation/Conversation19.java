package serverlink.bot.speech.conversation;

import serverlink.util.Probability;

import java.util.ArrayList;
import java.util.List;

class Conversation19 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:why do you have %x% emails?", "you don't need that many accounts.",
                "i can't stand when ppl make multiple accounts on the forums."});
        add(new String[]{"1:why so many gamer skins?", "all thse gamer skins",
                "does no one like animal skins?-", "why is everyone's skin a camo block?-",
                "ppl stop trying to camoflauge with your skin."});
        add(new String[]{"1:so many hackers omg", "omg so many hackers"});
        add(new String[]{"1:kys haxors", "i really hate hackers.", "stop hacking it's not cool."});
        add(new String[]{"1:why do people wear so much black these days?", "why is pink even a color?"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:bruh"});
        add(new String[]{"2:they're epic dude."});
        add(new String[]{"2:u should try it m8"});
        add(new String[]{"2:they hate us because they ain't us.", "you obviously do it too...",
                "try it urself youll see..."});
        add(new String[]{"2:if you tried to understand u would."});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"1:that's dumb."});
        add(new String[]{"1:lame.. LAME!"});
        add(new String[]{"1:lol rly?"});
        add(new String[]{"1:lmao so weird", "lol the cringe"});
        add(new String[]{"1:just wow"});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[]{"1:never got that myself", "no i would never do that."});
        add(new String[]{"1:idek anymore"});
        add(new String[]{"1:honestly", "srsly guys."});
        add(new String[]{"1:right bro, that's what I'm sayin."});
        add(new String[]{"1:yea", "yeah", "ya"});
    }};

    public Conversation createConversation() {
        List<String[]> _line1 = line1;
        for (int i = 0; i < _line1.size(); i++) {
            String[] variations = _line1.get(i);
            for (int j = 0; j < variations.length; j++)
                variations[j] = variations[j].replaceAll("%x%", String.valueOf(((Double) Probability.selector()).intValue()));
            _line1.set(i, variations);
        }
        return new Conversation(_line1, line2, line3, line4);
    }
}

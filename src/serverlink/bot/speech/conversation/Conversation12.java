package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation12 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:i kill u."});
        add(new String[]{"1:don't touch him."});
        add(new String[]{"1:stop being overprotective."});
        add(new String[]{"1:watch I'm going to get muted later."});
        add(new String[]{"1:just watch."});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:ok"});
        add(new String[]{"2:whatever you say...-"});
        add(new String[]{"2:duhn duhn duhn"});
        add(new String[]{"2:k den"});
        add(new String[]{"2:lol"});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:hahaha"});
        add(new String[]{"3:xD"});
        add(new String[]{"3:o_0"});
        add(new String[]{"3:YOLO!"});
        add(new String[]{"3:weeee!-"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3);
    }
}

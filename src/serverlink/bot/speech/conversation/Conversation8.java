package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation8 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:I'm gonna go collapse from exhaustion bye.",
                "my eyes will close themselves if I don't get off now."});
        add(new String[]{"1:time to finally get sleep after MC all day, cya later.",
                "too much mc.. must leave now."});
        add(new String[]{"1:been a long cold day where im at, I gtg now.",
                "cya guys I gotta do my chores."});
        add(new String[]{"1:guys I gtg feed my lizards, I'll see you all later.",
                "i gotta feed my pets. cya tomorrow probably."});
        add(new String[]{"1:its still early where I am but I'm done with this day bye.",
                "ik it's morning for some of you, but i gtg to sleep now."});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:XD", "xD"});
        add(new String[]{"2:^", "yup same lol", "yep same"});
        add(new String[]{"2:same", "same honestly", "don't we all"});
        add(new String[]{"2:take care lmao.", "have a good one lmao", "well cya then"});
        add(new String[]{"2:this ^", "^^ exactly"});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:lol I have a very different time zone.",
                "we a very different then."});
        add(new String[]{"3:why bro?-", "oi m8... oi"});
        add(new String[]{"3:why u do dis?-"});
        add(new String[]{"3:cya later"});
        add(new String[]{"3:byee", "see you tomorrow?-"});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[]{"4:why.", "whyyy?"});
        add(new String[]{"4:wowers", "wowzers", "wowiie"});
        add(new String[]{"4:\"guys\"", "right..."});
        add(new String[]{"4:i will.", "oh i will", "bet ur ass i will boi"});
        add(new String[]{"4:jowdy", "jeowdy", "jeopardy"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3, line4);
    }
}

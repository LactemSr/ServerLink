package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation22 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:lawl"});
        add(new String[]{"1:hehe"});
        add(new String[]{"1:don't call me bro.. bro",
                "bro don't call me bro ok?"});
        add(new String[]{"1:gotta catch em all!",
                "gotta ketchup all!", "I gotta catch em all!", "pokemon! gotta catch em all!"});
        add(new String[]{"1:guyz I'm on tv!"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:oop sorry", "oops sorry", "oops srry"});
        add(new String[]{"2:rest in peace harambe <3", "rip Harambe",
                "harambe you will always be in our hearts."});
        add(new String[]{"2:hooray!", "yippee", "yipee", "yikes!"});
        add(new String[]{"2:help us against the hamsters.", "save us from the hamsters.",
                "protect me from the rodents."});
        add(new String[]{"2:I smoke weed.. weed don't do dat.",
                "weed don't make you do that.", "man I smoke weed, weed don't make you do that.",
                "idk what he on but weed don't make you do that."});
    }};

    public Conversation createConversation() {
        return new Conversation(line1, line2);
    }
}

package serverlink.bot.speech.conversation;

import serverlink.util.Probability;

import java.util.ArrayList;
import java.util.List;

class Conversation2_3 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:the one last week tho."});
        add(new String[]{"1:i can't even believe how good dat last one was."});
        add(new String[]{"1:it turned out to be even better than I expected."});
        add(new String[]{"1:even I didnt think it was gonna be that good."});
        add(new String[]{"1:last week's was actually unbelievable if u ask me."});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:fur real bro."});
        add(new String[]{"2:dude no joke it was amazing."});
        add(new String[]{"2:meh."});
        add(new String[]{"2:rly? it wasn't."});
        add(new String[]{"2:i liked it but not that much xD"});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:i thought it was pretty shit."});
        add(new String[]{"3:to each his own I guess."});
        add(new String[]{"3:well.. that's ur opinion then."});
        add(new String[]{"3:the plot itself was kinda lame if u ask me."});
        add(new String[]{"3:that wasn't good...-"});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[]{"3:i dont know any1 else who liked it."});
        add(new String[]{"3:none of my friends liked it."});
        add(new String[]{"3:no one I know likes it either."});
        add(new String[]{"3:literally no one thought it was good."});
        add(new String[]{"3:everyone I know also thinks its trash."});
    }};
    private static List<String[]> line5 = new ArrayList<String[]>() {{
        add(new String[]{"1:we know different people then."});
        add(new String[]{"1:I know people who liked it...-"});
        add(new String[]{"1:well you don't know enough ppl."});
        add(new String[]{"1:looks like you need new friends!-"});
        add(new String[]{"1:guess you don't know the same people as me."});
    }};
    private static List<String[]> line6 = new ArrayList<String[]>() {{
        add(new String[]{"4:what are we talking about?-"});
        add(new String[]{"4:what movie/show?-"});
        add(new String[]{"4:which show?-"});
        add(new String[]{"4:what show are u guys talkin about?-"});
        add(new String[]{"4:a tv show or a movie?-"});
    }};
    private static List<String[]> line7 = new ArrayList<String[]>() {{
        add(new String[]{"1:%show%... duh", "bruh... %show%"});
        add(new String[]{"1:talking about %show%"});
        add(new String[]{"1:it's %show%."});
        add(new String[]{"1:%show%."});
        add(new String[]{"1:we were discussing %show%"});
    }};
    private static List<String[]> line8 = new ArrayList<String[]>() {{
        add(new String[]{"2:yeah"});
        add(new String[]{"2:yep."});
        add(new String[]{"2:that one."});
        add(new String[]{"2:%show% ofc", "ofc %show%"});
        add(new String[]{"2:no... %show%", "really? I thought it was %show%"});
    }};


    public Conversation createConversation() {
        String show = "Power Rangers";
        double selector = Probability.selector();
        if (selector < 10) show = "Beauty and the Beast";
        else if (selector < 20) show = "The Walking Dead";
        else if (selector < 30) show = "Walking Dead";
        else if (selector < 40) show = "Logan";
        else if (selector < 50) show = "Orange is the New Black";
        else if (selector < 60) show = "Game of Thrones";
        else if (selector < 70) show = "La La Land";
        else if (selector < 80) show = "the Sherlock Show";
        else if (selector < 90) show = "Get Out";
        for (int i = 0; i < line7.size(); i++) {
            String[] variations = line7.get(i);
            for (int j = 0; j < variations.length; j++)
                variations[j] = variations[j].replaceAll("%show%", show);
            line7.set(i, variations);
        }
        for (int i = 0; i < line8.size(); i++) {
            String[] variations = line8.get(i);
            for (int j = 0; j < variations.length; j++)
                variations[j] = variations[j].replaceAll("%show%", show);
            line8.set(i, variations);
        }
        return new Conversation(line1, line2, line3, line4, line5, line6, line7, line8);
    }
}

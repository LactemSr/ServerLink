package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation9 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:*dose a backflip*"});
        add(new String[]{"1:aww Alex gtg, talk on insta?-"});
        add(new String[]{"1:ok"});
        add(new String[]{"1:coo"});
        add(new String[]{"1:I WANT C"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:really"});
        add(new String[]{"2:kiss who"});
        add(new String[]{"2:the person behind you :P"});
        add(new String[]{"2:sup"});
        add(new String[]{"2:look over here."});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:this person right here."});
        add(new String[]{"3:-_--"});
        add(new String[]{"3:what do you mean?"});
        add(new String[]{"3:I dare you to."});
        add(new String[]{"3:so you know her irl?"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3);
    }
}

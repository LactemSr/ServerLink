package serverlink.bot.speech.conversation;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.bot.Bot;
import serverlink.bot.BotBaseTrait;
import serverlink.bot.BotChatTrait;
import serverlink.bot.speech.SpeechStyle;
import serverlink.util.Probability;

import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.SplittableRandom;

class Conversation {
    private static SplittableRandom r = new SplittableRandom();
    private int parts = 0;
    // part number -> bot
    private HashMap<Integer, Bot> partActors;
    private int lineIndex = 0;
    // line number -> line beginning with part number of bot to say it and a colon ("2:")
    private HashMap<Integer, String[]> lines1 = new HashMap<>();
    private HashMap<Integer, String[]> lines2 = new HashMap<>();
    private HashMap<Integer, String[]> lines3 = new HashMap<>();
    private HashMap<Integer, String[]> lines4 = new HashMap<>();
    private HashMap<Integer, String[]> lines5 = new HashMap<>();
    private String nextLine = "";
    private boolean nextLineSelected = false;

    @SafeVarargs
    Conversation(List<String[]>... lines) {
        for (int lineNumber = 0; lineNumber < lines.length; lineNumber++) {
            int partNumber = Integer.parseInt(lines[lineNumber].get(0)[0].substring(0, 1));
            if (partNumber > parts) parts = partNumber;
            partNumber = Integer.parseInt(lines[lineNumber].get(1)[0].substring(0, 1));
            if (partNumber > parts) parts = partNumber;
            partNumber = Integer.parseInt(lines[lineNumber].get(2)[0].substring(0, 1));
            if (partNumber > parts) parts = partNumber;
            partNumber = Integer.parseInt(lines[lineNumber].get(3)[0].substring(0, 1));
            if (partNumber > parts) parts = partNumber;
            partNumber = Integer.parseInt(lines[lineNumber].get(4)[0].substring(0, 1));
            if (partNumber > parts) parts = partNumber;
            this.lines1.put(lineNumber, lines[lineNumber].get(0));
            this.lines2.put(lineNumber, lines[lineNumber].get(1));
            this.lines3.put(lineNumber, lines[lineNumber].get(2));
            this.lines4.put(lineNumber, lines[lineNumber].get(3));
            this.lines5.put(lineNumber, lines[lineNumber].get(4));
        }
    }

    int getParts() {
        return parts;
    }

    void setPartActors(Bot... partActors) {
        this.partActors = new HashMap<>();
        int actorNumber = 1;
        for (Bot bot : partActors) {
            this.partActors.put(actorNumber, bot);
            actorNumber++;
        }
    }

    Bot getNextPartActor() {
        return partActors.get(Integer.parseInt(lines1.get(lineIndex)[0].substring(0, 1)));
    }

    String getNextLine() {
        if (nextLineSelected) return nextLine;
        HashMap<Integer, String[]> lines = lines1;
        if (Probability.get(20)) lines = lines2;
        else if (Probability.get(20)) lines = lines3;
        else if (Probability.get(20)) lines = lines4;
        else if (Probability.get(20)) lines = lines5;
        nextLine = lines.get(lineIndex)[r.nextInt(lines.get(lineIndex).length)].trim();
        if (nextLine.substring(1, 2).equals(":")) nextLine = nextLine.substring(2).trim();
        nextLineSelected = true;
        return nextLine;
    }

    // return true if conversation is now over (do not reuse the Conversation instance)
    boolean sayLine() {
        Bot bot = getNextPartActor();
        if (!bot.getNpc().getTrait(BotBaseTrait.class).ready || !bot.getNpc().isSpawned()
                || bot.getNpc().getEntity() == null) {
            return true;
        }
        SpeechStyle style = bot.getNpc().getTrait(BotChatTrait.class).style;
        String line = getNextLine();
        nextLineSelected = false;
        if (style == SpeechStyle.YES_CAP_NO_PUNC) {
            if (line.endsWith(".") || line.endsWith("!") || line.endsWith("?") || line.endsWith("-"))
                line = line.substring(0, 1).toUpperCase() + line.substring(1, line.length() - 1);
            else
                line = line.substring(0, 1).toUpperCase() + line.substring(1, line.length());
        } else if (style == SpeechStyle.YES_CAP_YES_PUNC) {
            if (line.endsWith("-"))
                line = line.substring(0, 1).toUpperCase() + line.substring(1, line.length() - 1);
            else
                line = line.substring(0, 1).toUpperCase() + line.substring(1, line.length());
        } else if (style == SpeechStyle.NO_CAP_NO_PUNC) {
            if (line.endsWith(".") || line.endsWith("!") || line.endsWith("?") || line.endsWith("-"))
                line = line.substring(0, line.length() - 1);
        } else if (style == SpeechStyle.NO_CAP_YES_PUNC) {
            if (line.endsWith("-"))
                line = line.substring(0, line.length() - 1);
        }
        String finalLine = line;
        new BukkitRunnable() {
            @Override
            public void run() {
                if (bot.getNpc().getEntity() == null || !bot.getNpc().isSpawned() || bot.getNpc().getEntity().isDead())
                    return;
                AsyncPlayerChatEvent event = new AsyncPlayerChatEvent(true, (Player) bot.getNpc().getEntity(), finalLine, new HashSet<>(Bukkit.getOnlinePlayers()));
                Bukkit.getPluginManager().callEvent(event);
                if (event.isCancelled()) return;
                for (Player p : Bukkit.getOnlinePlayers()) {
                    p.sendMessage(event.getFormat().replace("%2$s", event.getMessage()).replace("%1$s", ((Player) bot.getNpc().getEntity()).getDisplayName()));
                }
            }
        }.runTaskAsynchronously(ServerLink.plugin);
        lineIndex++;
        return lineIndex == lines1.size();
    }
}

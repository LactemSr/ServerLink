package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation1 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:hi guys", "hey guys", "hi guyz", "hey guyz", "hai guyz", "hai guyzz"});
        add(new String[]{"1:hello to everyone", "hello everyone", "hi everyone!"});
        add(new String[]{"1:hi all", "hey all", "hi all!", "hay everybody", "hey everybody"});
        add(new String[]{"1:hello?-", "hallo?-", "ello", "hey ppl wassup?-"});
        add(new String[]{"1:oi mates", "wazzup bois"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:hello", "hallo", "ello", "ey mate"});
        add(new String[]{"2:hai", "ahoy matey"});
        add(new String[]{"2:hey", "whats up yo?", "heyo"});
        add(new String[]{"2:wazzup", "wazzup bud", "what's up?", "what is up"});
        add(new String[]{"2:hey bud", "heyder bro", "hey dude"});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:hay", "haaay", "heeey", "HELLO!", "HI!"});
        add(new String[]{"3:hello!-", "hi guy", "wassup bro"});
        add(new String[]{"3:hey there.", "howdy", "io", "oye"});
        add(new String[]{"3:what is up my dude?-", "sup dude", "what's up ma dude?", "whats up my dude?"});
        add(new String[]{"3:hello to you!", "good day", "top o de morning friend", "top of the morning to you", "hey man"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3);
    }
}

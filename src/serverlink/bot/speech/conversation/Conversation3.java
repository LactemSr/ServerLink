package serverlink.bot.speech.conversation;

import serverlink.util.Probability;

import java.util.ArrayList;
import java.util.List;

class Conversation3 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:whos a fan of %youtuber%?-"});
        add(new String[]{"1:who likes %youtuber%?-"});
        add(new String[]{"1:anyone here like %youtuber%?-"});
        add(new String[]{"1:%youtuber% anyone?-"});
        add(new String[]{"1:anyone on here a fan of the firenation of TBNRFrags?-"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"1:say I if you are.", "say I if u agree.", "say \"I\" if u are."});
        add(new String[]{"1:anyone?-", "someone?-", "somebody?-", "anybody?-",
                "enyone?-", "pls some1."});
        add(new String[]{"1:say \"I\" if ur with me.", "say I if u love it."});
        add(new String[]{"1:anyone at least heard of him?-", "or just heard of him?",
                "who at least saw one video from him?-", "someone at least know who that is??"});
        add(new String[]{"1:if you are, say I.", "if u are, say \"I\"", "if u are, say \"bacon\""});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"2:nobody", "sorta not really.", "sorta", "nah", "kind of"});
        add(new String[]{"2:no one.", "no1", "hate that dude.", "hate that guy.",
                "can't stand him.", "can't stand those vids."});
        add(new String[]{"2:nope.", "nein", "definitely not.", "ya no...-", "defs no."});
        add(new String[]{"2:no thank you.", "gonna have to decline that one.",
                "gonna pass on that one...-", "never."});
        add(new String[]{"2:no thanks.", "pls no.", "pls not that guy."});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[]{"3:%youtuber%?-"});
        add(new String[]{"3:who is that?-", "who dat?-"});
        add(new String[]{"3:idek how ppl like him.", "every1 knows him but he sux.",
                "ofc everyone knows %youtuber%!"});
        add(new String[]{"3:screw that dude.", "the cringe of that dude.", "it's so cringe.",
                "the cringe makes me want to die.", "can't stand the cringe on there."});
        add(new String[]{"3:idk %youtuber%.", "idk who that is.", "don't know %youtuber%.",
                "don't know who that is.", "i dont know who that is.", "i saw only 2 vids of his."});
    }};
    private static List<String[]> line5 = new ArrayList<String[]>() {{
        add(new String[]{"1:they are all playing diff games and ya",
                "they keep it real with different types of content.",
                "he records random stuff sometimes.", "literally everything %youtuber% uploads is funny."});
        add(new String[]{"1:he's a really good mc youtuber.", "he's super good at editing vids.",
                "that's a funny guy on youtube.", "I like his channel soo much."});
        add(new String[]{"1:he does epic youtube videos.", "all his vids are epic.", "his videos are so epic."});
        add(new String[]{"1:%youtuber% is a great youtuber.",
                "%youtuber% is only one of the world's best people on yt.",
                "%youtuber% is my favorite channel on youtube rn.",
                "I just love %youtuber% so much i cant explain."});
        add(new String[]{"1:an amazing youtuber.", "the most savage person on yt, that's who.",
                "only the most savage youtuber ever.", "a super epic youtuber.", "the best yt channel this year."});
    }};
    private static List<String[]> line6 = new ArrayList<String[]>() {{
        add(new String[]{"1:%youtuber% and %youtuber2%"});
        add(new String[]{"1:%youtuber% or %youtuber2%"});
        add(new String[]{"1:%youtuber2% too", "also %youtuber2%."});
        add(new String[]{"1:%youtuber2% is good too", "%youtuber2% is also nice.",
                "%youtuber2% is also good.", "%youtuber2% is almost as epic.", "i recommend %youtuber2% as well."});
        add(new String[]{"1:anyone like %youtuber2% then?-", "how about %youtuber2% then?-",
                "well what about %youtuber2%?-"});
    }};
    private static List<String[]> line7 = new ArrayList<String[]>() {{
        add(new String[]{"2:the guy who sucks more than ur mum?-",
                "oh u mean the world's most famous noob?-", "that noob rlly?-"});
        add(new String[]{"2:you mean the clickbait moneywars guy...-",
                "The youtuber who posts clickbait thumbnails, right?-",
                "that's the one with the dishonest thumbnails."});
        add(new String[]{"2:that noob?-", "that noob>?-", "that guy's a real noob."});
        add(new String[]{"2:I hate clickbait youtubers.", "can't stand clickbait on youtube like that noob."});
        add(new String[]{"2:what a joke.", "that's a joke right?-", "are you even serious?-",
                "u being serious?", "for real dude?-"});
    }};
    private static List<String[]> line8 = new ArrayList<String[]>() {{
        add(new String[]{"1:wow", "just wow", "ok wow"});
        add(new String[]{"1:no...-", "no... %youtuber% is good."});
        add(new String[]{"1:wtf?-", "dafuq m8?-", "dafuq", "wtf...-"});
        add(new String[]{"1:%youtuber% is not like that.", "that's not %youtuber%.", "" +
                "not %youtuber% you noob."});
        add(new String[]{"1:obviously you haven't seen %youtuber%.",
                "i know you never even saw %youtuber%.", "u probably never even watched %youtuber%."});
    }};
    private static List<String[]> line9 = new ArrayList<String[]>() {{
        add(new String[]{"4:it's true.", "true facts", "facts bro.", "straight truth."});
        add(new String[]{"4:exposed omg", "wow rekt.", "that's gotta hurt"});
        add(new String[]{"4:can't stand clickbait either.", "i hate clickbait on yt too.",
                "%youtuber% is definitely clickbait.-", "screw clickbaiting youtubers."});
        add(new String[]{"4:i cant stand %youtuber% tbh.", "in all honesty, %youtuber% isn't even good.",
                "that guy's not even really funny.", "his videos are just plain boring."});
        add(new String[]{"4:dat guy doesn't even have a sense of humor.", "he's just too fake."});
    }};
    private static List<String[]> line10 = new ArrayList<String[]>() {{
        add(new String[]{"2:maybe that's %youtuber3% actually."});
        add(new String[]{"2:or am i thinking of %youtuber3%?-"});
        add(new String[]{"2:maybe I'm getting %youtuber% confused with %youtuber3%."});
        add(new String[]{"2:%youtuber3% is the worst though."});
        add(new String[]{"2:not as bad as %youtuber3% at least."});
    }};
    private static List<String[]> line11 = new ArrayList<String[]>() {{
        add(new String[]{"1:thats just mean hater. but ...-", "now that's just mean. hater...-",
                "omg why u be hating?-"});
        add(new String[]{"1:haters gonna hate idk what to say", "haters gonna hate i guess...-",
                "oh well, haters is always gonna hate.", "what can u do bro.. haters gonna hate."});
        add(new String[]{"1:why u be hatin on %youtuber3% too?-", "wow haters be hatin.",
                "haven't seen so much hate in months.", "waw u just turned up the hate to the max."});
        add(new String[]{"1:ur being a hater now dude.", "such hater.", "much hate. wow.-",
                "the hater lvl is over 9000 rn.", "hate is over 9k at that one."});
        add(new String[]{"1:stop being a hater.", "stop hating on good youtubers.",
                "just stop hating on youtubers ok?-"});
    }};
    private static List<String[]> line12 = new ArrayList<String[]>() {{
        add(new String[]{"2:hater is just a word for person with facts u dont like",
                "calling me a hater doesn't change facts.", "hater = person with facts you don't like."});
        add(new String[]{"2:just spouting facts, not hating.", "im not hating, im speaking truth."});
        add(new String[]{"2:that's what people say when they run out of good arguments...-",
                "looks like you ran out of good arguments so ur calling names.."});
        add(new String[]{"2:you can call truth hate if you want...-",
                "is that gonna change truth?-"});
        add(new String[]{"2:if you call me a hater it means you can't handle the truth.",
                "calling hate means you can't handle the truth.",
                "saying hater means you can't handle the truth."});
    }};
    private static List<String[]> line13 = new ArrayList<String[]>() {{
        add(new String[]{"1:true.. srry", "true facts yo.", "well that's true tbh."});
        add(new String[]{"1:i actually agree with that :(", "i agree."});
        add(new String[]{"1:you're right bro.", "u right", "u right man."});
        add(new String[]{"1:ok", "K.-", "k den."});
        add(new String[]{"1:whatever", "it's whatever.", "whatevs u say."});
    }};

    public Conversation createConversation() {
        List<String[]> _line1 = line1;
        List<String[]> _line4 = line4;
        List<String[]> _line5 = line5;
        List<String[]> _line6 = line6;
        List<String[]> _line8 = line8;
        List<String[]> _line9 = line9;
        List<String[]> _line10 = line10;
        List<String[]> _line11 = line11;
        String youtuber = "TBNRFrags";
        double selector = Probability.selector();
        if (selector < 10) youtuber = "Prestonplayz";
        else if (selector < 20) youtuber = "BajanCanadian";
        else if (selector < 30) youtuber = "Pewdiepie";
        else if (selector < 40) youtuber = "pewds";
        else if (selector < 50) youtuber = "SethBling";
        else if (selector < 60) youtuber = "markiplier";
        else if (selector < 70) youtuber = "cupquake";
        else if (selector < 80) youtuber = "CaptainSparklez";
        else if (selector < 90) youtuber = "Filthy Frank";
        String youtuber2 = "TBNRFrags";
        double selector2 = Probability.selector();
        if (selector2 < 10) youtuber2 = "Prestonplayz";
        else if (selector2 < 20) youtuber2 = "BajanCanadian";
        else if (selector2 < 30) youtuber2 = "Pewdiepie";
        else if (selector2 < 40) youtuber2 = "pewds";
        else if (selector2 < 50) youtuber2 = "SethBling";
        else if (selector2 < 60) youtuber2 = "markiplier";
        else if (selector2 < 70) youtuber2 = "cupquake";
        else if (selector2 < 80) youtuber2 = "CaptainSparklez";
        else if (selector2 < 90) youtuber2 = "Filthy Frank";
        String youtuber3 = "TBNRFrags";
        double selector3 = Probability.selector();
        if (selector3 < 10) youtuber3 = "Prestonplayz";
        else if (selector3 < 20) youtuber3 = "BajanCanadian";
        else if (selector3 < 30) youtuber3 = "Pewdiepie";
        else if (selector3 < 40) youtuber3 = "pewds";
        else if (selector3 < 50) youtuber3 = "SethBling";
        else if (selector3 < 60) youtuber3 = "markiplier";
        else if (selector3 < 70) youtuber3 = "cupquake";
        else if (selector3 < 80) youtuber3 = "CaptainSparklez";
        else if (selector3 < 90) youtuber3 = "Filthy Frank";
        for (int i = 0; i < _line1.size(); i++) {
            String[] variations = _line1.get(i);
            for (int j = 0; j < variations.length; j++)
                variations[j] = variations[j].replaceAll("%youtuber%", youtuber);
            _line1.set(i, variations);
        }
        for (int i = 0; i < _line4.size(); i++) {
            String[] variations = _line4.get(i);
            for (int j = 0; j < variations.length; j++)
                variations[j] = variations[j].replaceAll("%youtuber%", youtuber);
            _line4.set(i, variations);
        }
        for (int i = 0; i < _line5.size(); i++) {
            String[] variations = _line5.get(i);
            for (int j = 0; j < variations.length; j++)
                variations[j] = variations[j].replaceAll("%youtuber%", youtuber);
            _line5.set(i, variations);
        }
        for (int i = 0; i < _line6.size(); i++) {
            String[] variations = _line6.get(i);
            for (int j = 0; j < variations.length; j++)
                variations[j] = variations[j].replaceAll("%youtuber%", youtuber)
                        .replaceAll("%youtuber2%", youtuber2);
            _line6.set(i, variations);
        }
        for (int i = 0; i < _line8.size(); i++) {
            String[] variations = _line8.get(i);
            for (int j = 0; j < variations.length; j++)
                variations[j] = variations[j].replaceAll("%youtuber%", youtuber);
            _line8.set(i, variations);
        }
        for (int i = 0; i < _line9.size(); i++) {
            String[] variations = _line9.get(i);
            for (int j = 0; j < variations.length; j++)
                variations[j] = variations[j].replaceAll("%youtuber%", youtuber);
            _line9.set(i, variations);
        }
        for (int i = 0; i < _line10.size(); i++) {
            String[] variations = _line10.get(i);
            for (int j = 0; j < variations.length; j++)
                variations[j] = variations[j].replaceAll("%youtuber%", youtuber)
                        .replaceAll("%youtuber3%", youtuber3);
            _line10.set(i, variations);
        }
        for (int i = 0; i < _line11.size(); i++) {
            String[] variations = _line11.get(i);
            for (int j = 0; j < variations.length; j++)
                variations[j] = variations[j].replaceAll("%youtuber3%", youtuber3);
            _line11.set(i, variations);
        }
        return new Conversation(_line1, line2, line3, _line4, _line5, _line6, line7, _line8,
                _line9, _line10, _line11, line12, line13);
    }
}

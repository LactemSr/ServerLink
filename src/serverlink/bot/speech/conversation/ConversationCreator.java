package serverlink.bot.speech.conversation;

public interface ConversationCreator {
    Conversation createConversation();
}

package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation20 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:those are some sick arms…", "sick arms you have…",
                "sick arms mate.", "cool stuff", "i love ur style bud."});
        add(new String[]{"1:cool skin man.", "yo cool skin man.", "i really want that skin dude.",
                "lol nice skin."});
        add(new String[]{"1:nice choice of gear there bud.",
                "that's some nice gear you got on there.", "woah, nice gear dude."});
        add(new String[]{"1:wow, that's an epic skin", "that's such an epic skin",
                "omg that skin is so cool.", "omg that skin is so epic."});
        add(new String[]{"1:good outfit my man…", "cool outfit yo.",
                "cool outfit bro.", "what a sexy outfit."});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:me?-"});
        add(new String[]{"2:u talking about this here m8?-", "you talking about me?-"});
        add(new String[]{"2:mine?-"});
        add(new String[]{"2:who me?-"});
        add(new String[]{"2:which, mine?-"});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"1:yeah bro.", "ya bro", "ya", "yes u mate"});
        add(new String[]{"1:yeppers", "yep yep", "yeeeep", "yeppp"});
        add(new String[]{"1:yup.", "who else?-"});
        add(new String[]{"1:yeah u my man.", "you my man."});
        add(new String[]{"1:only you…-", "lol only you…-", "of course"});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[]{"1:well thanks.", "hey thanks man."});
        add(new String[]{"1:aww thanks.", "thanks <3", "awww thanks bro."});
        add(new String[]{"1:thank u very much sir.", "thank you good sir.",
                "thank you kindly sir."});
        add(new String[]{"1:thanks mate.", "thanks m8", "thanks man."});
        add(new String[]{"1:<3", "<333", ":3"});
    }};
    private static List<String[]> line5 = new ArrayList<String[]>() {{
        add(new String[]{"2:psyche!", "psyche you thought!", "psyche! lol", "lol psyche!",
                "NOT lol", "not...."});
        add(new String[]{"2:ha jk it sucks.", "ha jk it really sucks",
                "haha just kidding it's lame"});
        add(new String[]{"2:lolol just kidding.", "jk lol", "noob I was kidding.",
                "you thought I was serious?", "obviously i was joking.", "im messing with u obviously..."});
        add(new String[]{"2:nah I wasn't being serious, that's terrible.",
                "I was joking, it's awful.", "I wasn't being serious. it's honestly terrible.",
                "nah I was completely joking"});
        add(new String[]{"2:jk...-", "just kidding…."});
    }};
    private static List<String[]> line6 = new ArrayList<String[]>() {{
        add(new String[]{"1:dick", "you dick", "dick...-", "stupidhead", "that makes u a jerk.",
                "k so you're a jerk then.", "meanie"});
        add(new String[]{"1:...", "........", "................."});
        add(new String[]{"1:whatever", "k whatever", "ok whatever then"});
        add(new String[]{"1:screw u too hater", "buttface!", "go troll someone else pls.",
                "pls don't joke with me like that."});
        add(new String[]{"1:go bother someone else mate.", "piss off mate",
                "noob", "hater noob"});
    }};

    public Conversation createConversation() {
        return new Conversation(line1, line2, line3, line4, line5, line6);
    }
}

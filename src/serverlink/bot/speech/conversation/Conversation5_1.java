package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation5_1 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:who wants to connect on snapchat or instagram?",
                "meet me online?"});
        add(new String[]{"1:someone add my twitter", "anyone want my twitter?-"});
        add(new String[]{"1:yo who wants a new follower on SC?", "yo someone add my snapchat",
                "guys someone add me on snapchat."});
        add(new String[]{"1:guys who wants to friend me on any social media?-"});
        add(new String[]{"1:someone pls follow me on twitter-",
                "please someone follow ma twitter. it's lit"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:piss off mate.", "you go away, k?"});
        add(new String[]{"2:stranger danger!", "aaaah the cringe"});
        add(new String[]{"2:why?-", "why bother", "why would you even ask?"});
        add(new String[]{"2:how bout no.", "nah dude", "methinks not", "not today buddy"});
        add(new String[]{"2:please no", "fuuuuudge no", "erm... nope", "bruh.."});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"1:please.", "please someone.", "pls."});
        add(new String[]{"1:but why lol", "it won't hurt dude", "why not?-"});
        add(new String[]{"1:don't you want more friends?-", "you don't like friends?-",
                "are you antisocial??-", "are you antisocial or what?-"});
        add(new String[]{"1:i'm cool i swear.", "i am actually really nice."});
        add(new String[]{"1:just try it.", "just do it."});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[]{"3:lel", "haha", "hahah"});
        add(new String[]{"3:ok then...-", "alrighty mates."});
        add(new String[]{"3:you two are the weirdest ppl on right now",
                "so random.", "well that was interesting.", "how unusual."});
        add(new String[]{"3:can't stand these kids.", "skids on mc amirite?"});
        add(new String[]{"3:what a character", "just odd"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3, line4);
    }
}

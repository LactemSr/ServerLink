package serverlink.bot.speech.conversation;

import serverlink.util.CachedDecimalFormat;
import serverlink.util.Probability;

import java.util.ArrayList;
import java.util.List;

class Conversation17 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:ill be beack", "brb guys"});
        add(new String[]{"1:wow lol"});
        add(new String[]{"1:i just have to get some gas"});
        add(new String[]{"1:in" + CachedDecimalFormat.ONES.format(Probability.selector()) + " years"});
        add(new String[]{"1:five years later still no sign"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:gl with your heist."});
        add(new String[]{"2:and i want to smash"});
        add(new String[]{"2:Haaaa"});
        add(new String[]{"2:and can give them to me."});
        add(new String[]{"2:do you have something I can talk to you with?"});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:and since you were to lazy to type \"anyone\""});
        add(new String[]{"3:i was to lazy to type \"yes\""});
        add(new String[]{"3:yo yo yo"});
        add(new String[]{"3:*dabs*"});
        add(new String[]{"3:lit fam"});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[]{"1:don't ask us"});
        add(new String[]{"1:quite rude"});
        add(new String[]{"1:>.> cant find it"});
        add(new String[]{"1:that a creature?"});
        add(new String[]{"1:i think they got rid of it."});
    }};
    private static List<String[]> line5 = new ArrayList<String[]>() {{
        add(new String[]{"3:to everyone here: I hope you have a nice day."});
        add(new String[]{"3:i invited u again"});
        add(new String[]{"3:u should like play."});
        add(new String[]{"3:I know I can treat u better."});
        add(new String[]{"3:goku"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3, line4, line5);
    }
}

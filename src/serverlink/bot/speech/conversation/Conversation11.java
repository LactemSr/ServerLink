package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation11 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:she's too cringe for her."});
        add(new String[]{"1:i stuck."});
        add(new String[]{"1:ugh"});
        add(new String[]{"1:oh so it was you?-"});
        add(new String[]{"1:mkay"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:not sure...-"});
        add(new String[]{"2:it's fine with me."});
        add(new String[]{"2:yep"});
        add(new String[]{"2:now I feel like an idiot."});
        add(new String[]{"2:lol"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2);
    }
}

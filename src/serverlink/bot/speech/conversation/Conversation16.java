package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation16 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:accept"});
        add(new String[]{"1:i do that but he not locate.",
                ""});
        add(new String[]{"1:her?"});
        add(new String[]{"1:I said do."});
        add(new String[]{"1:no thx"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:not did", "dats done", "worth it"});
        add(new String[]{"2::/", "if only I could play with that fps.",
                "I'm stuck with this slow video card instead :/"});
        add(new String[]{"2:what is dat", "oooh what's that?-", "cool what is it?"});
        add(new String[]{"2:got a gtx only uses it for minecraft.",
                "got a gtx... lucky!", "woah I rlly want a gaming card too.",
                "if I had those graphics....", "you don't even know how much I want that gtx."});
        add(new String[]{"2:a video card", "that's a good video card.",
                "i heard it's one of the best cards out there.", "that's an epic card",
                "cool, my friend has that video card also."});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:how much fps u get now?-",
                "can you play overwatch now?-", "is it as good as they say?-",
                "what's ur fps?-", "what fps?-", "how many frames?-", "how many frames you getting now?-"});
        add(new String[]{"3:how fast is it?-",
                "what fps now?-", "it's smoother now?-", "congratz!",
                "nice dude!", "i always wanted one myself.", "i'm jealous lol"});
        add(new String[]{"3:is it even that much better?-",
                "is it actually better?-", "how good??-"});
        add(new String[]{"3:what kind of improvement do you see?-",
                "what u at now?-", "how much faster is it?-"});
        add(new String[]{"3:does it work?-",
                "how does it work?-", "is it working well?-", "how good is it running?"});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[]{"1:solid 60", "50 now", "an solid 80-100",
                "around 50", "it stays in the double digits now",
                "it's like 35", "it still jumps around a lot", "can't say yet"});
        add(new String[]{"1:im getting 8.", "hovers around 65",
                "i'm getting 47 now.", "now its 14 :(", "lol it's still 40 ish.",
                "like 80", "more than 70", "in the 120's!", "100 boi"});
        add(new String[]{"1:ffs xD", "oh well. I try another one later",
                "still not good enough", "same as last time.", "tbh about the same."});
        add(new String[]{"1:someone should upgrade me XD",
                "i need one too!", "bro get me one also.", "pls... I need one also."});
        add(new String[]{"1:eh", "well....", "hah lol", "come see for yourself.",
                "better than before at least.", "works super well!"});
    }};
    private static List<String[]> line5 = new ArrayList<String[]>() {{
        add(new String[]{"3:Ohhh.", "superb", "that's special", "ur special", "cute", "noice"});
        add(new String[]{"3:or sometimes lagplex.", "faster than lagplex",
                "at least it's not laggy like mineplex.", "not as slow as lagplex"});
        add(new String[]{"3:is your ign typed in correctly?", "did you put it in right?-",
                "are you sure you typed it exactly?"});
        add(new String[]{"3:they did.", "ya dun did it.", "i didn't.. they did"});
        add(new String[]{"3:ur my dad.", "ur my mom ok?-", "hi mommy!"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3, line4, line5);
    }
}

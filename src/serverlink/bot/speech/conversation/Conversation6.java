package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation6 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:i killed sooooo many chickens...-", "i drank soo much water...-",
                "too much soda...-"});
        add(new String[]{"1:very much tacos...-", "way too many o dat...-"});
        add(new String[]{"1:gooodddd", "grrreaat!"});
        add(new String[]{"1:lets be friends.", "let us friend."});
        add(new String[]{"1:lolololol", "lawl boi"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:this seems legit.", "so totally legit..."});
        add(new String[]{"2:that's right.", "right.._"});
        add(new String[]{"2:oops", "not really man....", "no... kid....", "no kidding..."});
        add(new String[]{"2:you're just jealous.", "i see ur jealous.", "i see ur jealous of me.",
                "i can smell the jealousy."});
        add(new String[]{"1:Get savin"});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:is it me or is friends working?-", "friends is working now right?-"});
        add(new String[]{"3:are the friends working now?-", "did they bring friends back?-"});
        add(new String[]{"3:pls stop", "staahp....", "staahp.", "just stop"});
        add(new String[]{"3:doesn't matter", "it really doesn't matter",
                "i had normal armour and i killed him.", "i still rekt him.",
                "i did it with a default class only."});
        add(new String[]{"3:lol ikr", "ya ikr", "i know right?"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3);
    }
}

package serverlink.bot.speech.conversation;

import serverlink.util.Probability;

import java.util.ArrayList;
import java.util.List;

class Conversation2 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:who likes %show%?-"});
        add(new String[]{"1:who liked %show%?-"});
        add(new String[]{"1:anyone here like %show%?-"});
        add(new String[]{"1:anyone seen %show%?-"});
        add(new String[]{"1:who likes %show%?-"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:looks good."});
        add(new String[]{"2:it looks good."});
        add(new String[]{"2:it looks like it's good."});
        add(new String[]{"2:I bet it's good."});
        add(new String[]{"2:it looks like a good movie."});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:story is crap."});
        add(new String[]{"3:the story is just too much."});
        add(new String[]{"3:I didn't like the story."});
        add(new String[]{"3:plot was not interesting."});
        add(new String[]{"3:the story is not good though."});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[]{"3:half the movie was fight scenes."});
        add(new String[]{"3:it was all drama."});
        add(new String[]{"3:too intense for me."});
        add(new String[]{"3:the fight scenes were good, but that was half the movie."});
        add(new String[]{"3:I wish they didn't make half the movie action."});
    }};
    private static List<String[]> line5 = new ArrayList<String[]>() {{
        add(new String[]{"4:well that's what they were focusing for."});
        add(new String[]{"4:well that's how they advertised it."});
        add(new String[]{"4:it was supposed to focus on the action parts."});
        add(new String[]{"4:that was the whole point of the movie!-"});
        add(new String[]{"4:%show% was made for the drama!-"});
    }};
    private static List<String[]> line6 = new ArrayList<String[]>() {{
        add(new String[]{"3:ok"});
        add(new String[]{"3:whatever"});
        add(new String[]{"3:well...-"});
        add(new String[]{"3:alright"});
        add(new String[]{"3:cool"});
    }};
    private static List<String[]> line7 = new ArrayList<String[]>() {{
        add(new String[]{"1:I still haven't seen it."});
        add(new String[]{"1:I'm still gonna watch it."});
        add(new String[]{"1:well I still want to see it."});
        add(new String[]{"1:haters gonna hate I guess."});
        add(new String[]{"1:I liked it...-"});
    }};


    public Conversation createConversation() {
        List<String[]> _line1 = line1;
        List<String[]> _line5 = line5;
        String show = "Rogue One";
        double selector = Probability.selector();
        if (selector < 10) show = "the new Star Wars";
        else if (selector < 20) show = "The Walking Dead";
        else if (selector < 30) show = "Walking Dead";
        else if (selector < 40) show = "the prison show";
        else if (selector < 50) show = "Orange is the New Black";
        else if (selector < 60) show = "Game of Thrones";
        else if (selector < 70) show = "Manchester By The Sea";
        else if (selector < 80) show = "the Sherlock Show";
        else if (selector < 90) show = "Doctor Strange";
        for (int i = 0; i < _line1.size(); i++) {
            String[] variations = _line1.get(i);
            for (int j = 0; j < variations.length; j++)
                variations[j] = variations[j].replaceAll("%show%", show);
            _line1.set(i, variations);
        }
        for (int i = 0; i < _line5.size(); i++) {
            String[] variations = _line5.get(i);
            for (int j = 0; j < variations.length; j++)
                variations[j] = variations[j].replaceAll("%show%", show);
            _line5.set(i, variations);
        }
        return new Conversation(_line1, line2, line3, line4, _line5, line6, line7);
    }
}

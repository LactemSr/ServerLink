package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation15 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[] {"1:it is working."});
        add(new String[] {"1:Ima go change my skin."});
        add(new String[] {"1:something but yourself."});
        add(new String[] {"1:I'm here."});
        add(new String[] {"1:i luv meh"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[] {"2:chuu"});
        add(new String[] {"2:back"});
        add(new String[] {"2:what's ur name?-"});
        add(new String[] {"2:I really like urs."});
        add(new String[] {"2:well done!"});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[] {"3:You missed one."});
        add(new String[] {"3:oh"});
        add(new String[] {"3::)"});
        add(new String[] {"3:suree!"});
        add(new String[] {"3:np"});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[] {"1:stop it."});
        add(new String[] {"1:finee"});
        add(new String[] {"1:ooo"});
        add(new String[] {"1:^-^"});
        add(new String[] {"1:yeah"});
    }};
    private static List<String[]> line5 = new ArrayList<String[]>() {{
        add(new String[] {"1:something else"});
        add(new String[] {"1:np"});
        add(new String[] {"1:hmm"});
        add(new String[] {"1:ikr"});
        add(new String[] {"1:ik same"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3, line4, line5);
    }
}

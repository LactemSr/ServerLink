package serverlink.bot.speech.conversation;

import java.util.ArrayList;
import java.util.List;

class Conversation10 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:idk"});
        add(new String[]{"1:guess what"});
        add(new String[]{"1:idk how to spell it but"});
        add(new String[]{"1:um...."});
        add(new String[]{"1:are you alive or nah?-"});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:he dead"});
        add(new String[]{"2:what do you want to do?"});
        add(new String[]{"2:TELL MEH"});
        add(new String[]{"2:no don't turn around!-"});
        add(new String[]{"2:awwww"});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:nvm"});
        add(new String[]{"3:nevermind."});
        add(new String[]{"3:...nevermind"});
        add(new String[]{"3:just forget about it."});
        add(new String[]{"3:forget I said anything...-"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3);
    }
}

package serverlink.bot.speech.conversation;

import serverlink.util.CachedDecimalFormat;
import serverlink.util.Probability;

import java.util.ArrayList;
import java.util.List;

class Conversation4 implements ConversationCreator {
    private static List<String[]> line1 = new ArrayList<String[]>() {{
        add(new String[]{"1:i was supposed to the war game but this player keeps reking me.",
                "i was supposed to game of wars but this player keeps reking me.",
                "i was supposed to this strategy game but this guy keeps reking me."});
        add(new String[]{"1:i keep dying to this hacker dude!", "this hacker keeps killing me!!"});
        add(new String[]{"1:I'm trying to play this game on my phone but i cant figure it out.",
                "i cant figure this new game out.", "this new iphone game is too complicated."});
        add(new String[]{"1:whenever I play this game one hacker always kicks my ass.",
                "i don't stand a chance against hackers online."});
        add(new String[]{"1:sucks that whenever I play this game hackers kill me.",
                "i cant even play this one game without hackers reking me."});
    }};
    private static List<String[]> line2 = new ArrayList<String[]>() {{
        add(new String[]{"2:i heard the rock could throw a boulder farther than a catapult.",
                "why do ppl say stupid stuff like that?-", "dumbass", "moron"});
        add(new String[]{"2:lel stupid commercials.", "lol the camel thing."});
        add(new String[]{"2:nu it's lies!", "no the lies!"});
        add(new String[]{"2:is it the legend" + CachedDecimalFormat.ONES.format(Probability.selector()) + "?-",
                "lol that's me.", "i think it's me lol.", "lolol im pretty sure it's me."});
        add(new String[]{"2:right click maybe.", "you have to click around more.", "just click faster."});
    }};
    private static List<String[]> line3 = new ArrayList<String[]>() {{
        add(new String[]{"3:maybe you shouldn't play it...-",
                "try not playing it...-"});
        add(new String[]{"3:delete ur account.",
                "delete your account?-"});
        add(new String[]{"3:just quit minecraft then."});
        add(new String[]{"3:at least you can figure out how to connect to the internet (i cant)."});
        add(new String[]{"3:that's me!"});
    }};
    private static List<String[]> line4 = new ArrayList<String[]>() {{
        add(new String[]{"4:LEL!-"});
        add(new String[]{"4:omg lol!"});
        add(new String[]{"4:LOLOL!-"});
        add(new String[]{"4:ha", "haha", "xD"});
        add(new String[]{"4:.....", "............"});
    }};


    public Conversation createConversation() {
        return new Conversation(line1, line2, line3, line4);
    }
}

package serverlink.bot.speech;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;

public class ResponseToKill {
    private static final SplittableRandom r = new SplittableRandom();
    // Responses are lowercase first letter with punctuation except special cases.
    private static final List<String[]> friendlyResponses = new ArrayList<String[]>() {{
        add(new String[]{"gg", "GG"});
        add(new String[]{"gg", "GG"});
        add(new String[]{"gg", "GG"});
        add(new String[]{"gg", "GG"});
        add(new String[]{"gg", "GG"});
        add(new String[]{"gg", "GG"});
        add(new String[]{"gg", "GG"});
        add(new String[]{"gg m8.", "GG mate.", "GG m8."});
        add(new String[]{"gg m8.", "GG mate.", "GG m8."});
        add(new String[]{"gg m8.", "GG mate.", "GG m8."});
        add(new String[]{"gg no re", "GG no re", "GG no RE"});
        add(new String[]{"gg come again", "gg, let's go again.", "gg, again!-"});
        add(new String[]{"fight me again.", "fight me again m8.", "fight me again mate.",
                "fight me again.", "fight me again m8.", "fight me again mate.",
                "come at me again.", "come at me again m8.", "come at me again mate."});
        add(new String[]{"lets do a rematch.", "let's do a rematch.", "rematch me m8."});
        add(new String[]{"let's do it again.", "one more time let's go.",
                "let's go again!", "come fight again!", "come fight again."});
        add(new String[]{"good battle m8.", "good battle mate.", "good battle bud."});
        add(new String[]{"well played.", "gg, wp.", "gg, well played.", "gg wp",
                "well played!", "well played my friend.", "well played bud.",
                "well played m8.", "well played mate."});
    }};
    private static final List<String[]> trashTalkResponses = new ArrayList<String[]>() {{
        add(new String[]{"gg rekt", "rekt m8", "wrecked."});
        add(new String[]{"GET REKT SCRUB!", "GET REKT SCRUB.", "get rekt scrub"});
        add(new String[]{"gtfo i rekt u"});
        add(new String[]{"lol u suck.", "lol u suck!", "omg u suck.", "omg u suck!",
                "lol you suck.", "lol you suck!", "omg you suck.", "omg you suck!",
                "lol you suck.", "lol you suck!", "omg you suck.", "omg you suck!",
                "lol you suck.", "lol you suck!", "omg you suck.", "omg you suck!"});
        add(new String[]{"come at me one more time m8.", "come at me one more time mate.",
                "come at me one more time.", "come at me another time mate.",
                "come at me another time m8.", "come at me another time.",
                "come at me again mate.", "come at me again m8.", "come at me again."});
        add(new String[]{"i have too much skills for you mate.", "i have too much skillz for you mate.",
                "i have too much skillz for you bud.", "i have too much skills for you bud.",
                "i got too many skillz for you.", "i got too many skills for you.",
                "i got too much skillz for you.", "i got too much skills for you.",
                "i got too many skillz 4 u", "i got too much skillz 4 u",
                "i got too much skills 4 u", "im 2 good for u m8", "im 2 good for u"});
        add(new String[]{"bro you really suk.", "bro you suck a lot.", "bro you really suck.",
                "bro you suk bad.", "bro u suck a lot.", "bro u really suk.", "bro u really suck.",
                "bro u suk bad.", "dude you suck a lot.", "dude you really suk.", "dude you really suck.",
                "dude you suk bad.", "dude u suck a lot.", "dude u really suk.",
                "dude u really suck.", "dude u suk bad."});
        add(new String[]{"lmfao u got rekt.", "lol you got rekt.", "lmfao you got rekt.",
                "lmao u got rekt.", "lmao you got rekt.", "lol haha rekt."});
        add(new String[]{"rekt over 9000.", "you just got rekt over 9000.",
                "you just got rekt over 9000 bud", "you just got rekt over 9000 m8",
                "rekt so hard.", "rekt so hard omg", "u got rekt so hard omg",
                "ultra rekt.", "that was, in a word... wrecked!"});
        add(new String[]{"omg how does it feel to get rekt that much?-",
                "omg how does it feel to get rekt so much?-",
                "omg how does it feel to get rekt that hard?-",
                "omg how did it feel to get rekt so much?-",
                "omg how did it feel to get rekt that much?-",
                "omg how did it feel to get rekt that hard?-"});
        add(new String[]{"get shrekt.", "get shrekt over 9000", "shrekt.", "shrekt m8",
                "u just got shrekt", "you just got shrekt.", "bud you got shrekt hard.",
                "bud you got shrekt there.", "you got shrekt there bud.", "you got shrekt m8."});
        add(new String[]{"get rekt LOL.", "get rekt scrub LOL.", "scrub get rekt LOL.",
                "lol get rekt scrub."});
        add(new String[]{"u suck more than TRUMP", "u suck more than Trump",
                "u suck more than Donald Trump", "u suck more than DONALD TRUMP",
                "you suck more than TRUMP!", "you suck more than Trump",
                "you suck more than Donald Trump", "you suck more than DONALD TRUMP",
                "youre worse than TRUMP!", "you're worse than Trump!",
                "you're worse than Donald Trump.", "you're worse than DONALD TRUMP!"});
        add(new String[]{"that was so easy.", "so ez.", "that was soo easy bro.", "E.Z.-"});
        add(new String[]{"easy as bacon.", "easy as butter.", "easy as 1 2 3.", "easy as 123.",
                "easy as toast.", "ez as bacon.", "ez as butter.", "ez as 1 2 3.", "ez as 123.",
                "ez as toast."});
        add(new String[]{"2 ez", "that was just too easy",
                "mate why you make getting kills so ez?-", "m8 why u make killing so ez?-",
                "mate why you make getting kills so easy?-", "m8 why u make killing so easy?-",
                "that was too easy."});
    }};

    public static String getVariation() {
        if (r.nextBoolean()) {
            String[] generalResponse = friendlyResponses.get(r.nextInt(friendlyResponses.size()));
            return generalResponse[r.nextInt(generalResponse.length)];
        }
        String[] generalResponse = trashTalkResponses.get(r.nextInt(trashTalkResponses.size()));
        return generalResponse[r.nextInt(generalResponse.length)];
    }
}

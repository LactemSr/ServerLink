package serverlink.bot.speech;

public enum Action {
    BEING_KILLED,
    CALL,
    GG,
    GREETING,
    HACKUSATION,
    JOIN,
    KILL,
    PVP_CHALLENGE,
    SAY_IF_NOT_BOT,
    UNKNOWN;
}

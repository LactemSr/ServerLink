package serverlink.bot.speech;

import java.util.ArrayList;
import java.util.List;
import java.util.SplittableRandom;

public class ResponseToJoin {
    private static final SplittableRandom r = new SplittableRandom();
    // Responses are lowercase first letter with punctuation except special cases.
    private static final List<String[]> responses = new ArrayList<String[]>() {{
        add(new String[]{"welcome!-"});
        add(new String[]{"welcome"});
        add(new String[]{"wELcome!", "wELcOme", "WElcome", "WELCome!-"});
        add(new String[]{"welcome bro.", "welcome mate.", "welcome m8.", "welcome bruh."});
        add(new String[]{"weLCOme m8.", "weLCoME mate.", "weLCOmE m9."});
        add(new String[]{"wELCome bruh.", "wELcome bruh.", "weLCoMe bruh."});
        add(new String[]{"wb", "wb bro", "wb mate", "wb to the server.", "welcome back.",
                "welcome back man.", "welcome back m8.", "welcome back mate.", "welcome back bro."});
        add(new String[]{"welcome to AC.", "welcome to AC!"});
        add(new String[]{"welcome to the server!", "welcome to the server."});
    }};

    public static String getVariation() {
        String[] generalResponse = responses.get(r.nextInt(responses.size()));
        return generalResponse[r.nextInt(generalResponse.length)];
    }
}

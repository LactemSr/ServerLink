package serverlink.network;

import java.util.ArrayList;
import java.util.stream.Collectors;

public class ServerSelection {
    //sorts least open slots to most open slots
    public static ArrayList<String> sortyByCount(ArrayList<String> list) {
        ArrayList<String> sorted = new ArrayList<>();
        ArrayList<String> zero = new ArrayList<>();
        ArrayList<String> one = new ArrayList<>();
        ArrayList<String> two = new ArrayList<>();
        ArrayList<String> three = new ArrayList<>();
        for (String serv : list) {
            int openSlots = NetworkCache.getMaxPlayers(serv) - NetworkCache.getCount(serv);
            if (openSlots < 5) {
                zero.add(serv);
            } else if (openSlots < 10) {
                one.add(serv);
            } else if (openSlots < 15) {
                two.add(serv);
            } else if (openSlots < 25) {
                three.add(serv);
            } else sorted.add(sorted.size(), serv);
        }
        sorted.addAll(zero.stream().collect(Collectors.toList()));
        sorted.addAll(one.stream().collect(Collectors.toList()));
        sorted.addAll(two.stream().collect(Collectors.toList()));
        sorted.addAll(three.stream().collect(Collectors.toList()));
        return sorted;
    }
}

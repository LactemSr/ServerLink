package serverlink.network;

import serverlink.ServerLink;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;

public class nstatus {
    private static String status = "lobby";

    /*
     * server statuses: lobby, lobbyCountdown, mapCountdown, game, endCountdown
     */
    public static void setup() {
        if (ServerLink.getServerName() == null) return;
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisOne.getThree();
                jedis.hset("status", ServerLink.getServerName(), "lobby");
                JedisOne.closeThree();
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public static void sync() {
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisOne.getThree();
                jedis.hset("status", ServerLink.getServerName(), status);
                JedisOne.closeThree();
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    synchronized public static void setLobby() {
        if (status.equals("lobby")) return;
        status = "lobby";
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisOne.getThree();
                jedis.hset("status", ServerLink.getServerName(), "lobby");
                JedisOne.closeThree();
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    synchronized public static void setLobbyCountdown() {
        if (status.equals("lobbyCountdown")) return;
        status = "lobbyCountdown";
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisOne.getThree();
                jedis.hset("status", ServerLink.getServerName(), "lobbyCountdown");
                JedisOne.closeThree();
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    synchronized public static void setMapCountdown() {
        if (status.equals("mapCountdown")) return;
        status = "mapCountdown";
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisOne.getThree();
                jedis.hset("status", ServerLink.getServerName(), "mapCountdown");
                JedisOne.closeThree();
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    synchronized public static void setGame() {
        if (status.equals("game")) return;
        status = "game";
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisOne.getThree();
                jedis.hset("status", ServerLink.getServerName(), "game");
                JedisOne.closeThree();
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    synchronized public static void setEndCountdown() {
        if (status.equals("endCountdown")) return;
        status = "endCountdown";
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisOne.getThree();
                jedis.hset("status", ServerLink.getServerName(), "endCountdown");
                JedisOne.closeThree();
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    synchronized public static boolean isLobby() {
        return status.equals("lobby");
    }

    synchronized public static boolean isLobbyCountdown() {
        return status.equals("lobbyCountdown");
    }

    synchronized public static boolean isMapCountdown() {
        return status.equals("mapCountdown");
    }

    synchronized public static boolean isGame() {
        return status.equals("game");
    }

    synchronized public static boolean isEndCountdown() {
        return status.equals("endCountdown");
    }
}

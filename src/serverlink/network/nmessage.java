package serverlink.network;

import serverlink.ServerLink;
import serverlink.customevent.CustomNetworkMessageEvent;
import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;

import java.util.List;

public class nmessage {

    public static void setup() {
        new BukkitRunnable() {
            @Override
            public void run() {
                checkForMessages();
            }
        }.runTaskTimerAsynchronously(ServerLink.plugin, 0, 7);
    }

    public static void shutdown() {
        Jedis jedis = JedisPool.getConn();
        jedis.del("messages:" + ServerLink.getServerName());
        JedisPool.close(jedis);
    }

    private static void checkForMessages() {
        Jedis jedis = JedisPool.getConn();
        List<String> list = jedis.lrange("messages:" + ServerLink.getServerName(), 0, -1);
        if (list.isEmpty()) {
            JedisPool.close(jedis);
            return;
        }
        for (String message : list) {
            jedis.lrem("messages:" + ServerLink.getServerName(), 1, message);
            Bukkit.getPluginManager().callEvent(new CustomNetworkMessageEvent(message));
        }
        JedisPool.close(jedis);
    }

    public static void sendMessage(String server, String message, Jedis jedis) {
        jedis.lpush("messages:" + server, message);
    }
}

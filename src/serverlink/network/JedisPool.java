package serverlink.network;

import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.PlayerManager;
import serverlink.util.ConsoleOutput;

import java.util.ArrayList;

public class JedisPool {
    // Node 2 stores permission group info and all player info
    private static String ip;
    private static int port;
    private static ArrayList<Jedis> free = new ArrayList<>();
    private static int inUse = 0;

    public static void setup() {
        new BukkitRunnable() {
            @Override
            public void run() {
                keepAlive();
            }
        }.runTaskTimerAsynchronously(ServerLink.plugin, 20 * 60, 20 * 60);
    }

    public static void shutdown() {
        free.forEach(Jedis::close);
    }

    public static void setIP(String dbIP) {
        ip = dbIP;
    }

    public static void setPort(int dbPort) {
        port = dbPort;
    }

    synchronized public static Jedis getConn() {
        if (!free.isEmpty()) {
            Jedis jedis = free.get(0);
            free.remove(jedis);
            inUse++;
            return jedis;
        }
        Jedis jedis = null;
        try {
            jedis = new Jedis(ip, port, 5000);
        } catch (Exception e) {
            ConsoleOutput.printError("COULDN'T OPEN A REDIS CONNECTION TO NODE 2!!!");
        }
        inUse++;
        return jedis;
    }

    synchronized public static void close(Jedis jedis) {
        if (!free.contains(jedis)) {
            free.add(jedis);
            inUse--;
        } else
            ConsoleOutput.printError("SOME CODE EITHER CLOSED A REDIS CONNECTION TWICE OR CLOSED A REDIS CONNECTION THAT WAS CREATED OUTSIDE OF JEDISPOOL!");
    }

    synchronized private static void keepAlive() {
        ArrayList<Jedis> copy = new ArrayList<>(free);
        for (Jedis jedis : free) {
            try {
                if (!jedis.ping().equals("PONG")) {
                    ConsoleOutput.printError("REDIS CONNECTION TIMED OUT SOMETIME AFTER OPENING!!! (node 2)");
                    jedis.close();
                    continue;
                }
            } catch (Exception e) {
                try {
                    jedis.close();
                } catch (Exception ignored) {
                }
                ConsoleOutput.printError(e.getMessage());
                ConsoleOutput.printError("REDIS CONNECTION TIMED OUT SOMETIME AFTER OPENING!!! (node 2)");
                continue;
            }
            // don't allow the number of free connections to exceed half the number of inUse connections
            if (copy.size() > inUse * 0.5) {
                jedis.close();
                copy.remove(jedis);
            }
        }
        free = copy;
        if (inUse + free.size() > (PlayerManager.getOnlineAlphaPlayers().size() * 1.5) + 2)
            ConsoleOutput.printWarning("Current node 2 connections: " + inUse + " in use, " + free.size() + " reserved");
    }
}

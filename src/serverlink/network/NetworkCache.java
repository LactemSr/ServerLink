package serverlink.network;

import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;

import java.util.HashMap;
import java.util.Map;

public class NetworkCache {
    private static final Object mapsLock = new Object();
    private static final Object statusesLock = new Object();
    private static final Object availabilitiesLock = new Object();
    private static final Object countsLock = new Object();
    private static final Object startPlayersLock = new Object();
    private static final Object maxPlayersLock = new Object();
    private static Map<String, String> maps = new HashMap<>();
    private static Map<String, String> statuses = new HashMap<>();
    private static Map<String, String> availabilities = new HashMap<>();
    private static Map<String, Integer> counts = new HashMap<>();
    private static Map<String, Integer> startPlayers = new HashMap<>();
    private static Map<String, Integer> maxPlayers = new HashMap<>();

    public static void setup() {
        new BukkitRunnable() {
            @Override
            public void run() {
                getServers();
            }
        }.runTaskTimerAsynchronously(ServerLink.plugin, 1, 19);
    }

    private static void getServers() {
        Jedis jedis = JedisOne.getThree();
        getMaps(jedis);
        getStatuses(jedis);
        getAvailabilities(jedis);
        getCounts(jedis);
        getStartPlayers(jedis);
        getMaxPlayers(jedis);
        JedisOne.closeThree();
    }

    private static void getMaps(Jedis jedis) {
        Map<String, String> tempMap = jedis.hgetAll("map");
        synchronized (mapsLock) {
            maps = tempMap;
        }
    }

    private static void getStatuses(Jedis jedis) {
        Map<String, String> tempMap = jedis.hgetAll("status");
        synchronized (statusesLock) {
            statuses = tempMap;
        }
    }

    private static void getAvailabilities(Jedis jedis) {
        Map<String, String> tempMap = jedis.hgetAll("availability");
        synchronized (availabilitiesLock) {
            availabilities = tempMap;
        }
    }

    private static void getCounts(Jedis jedis) {
        Map<String, Integer> tempMap = new HashMap<>();
        Map<String, String> jedisMap = jedis.hgetAll("count");
        for (String server : jedisMap.keySet()) {
            tempMap.put(server, Integer.parseInt(jedisMap.get(server)));
        }
        synchronized (countsLock) {
            counts.clear();
            counts.putAll(tempMap);
        }
    }

    private static void getStartPlayers(Jedis jedis) {
        Map<String, Integer> tempMap = new HashMap<>();
        Map<String, String> jedisMap = jedis.hgetAll("startPlayers");
        for (String server : jedisMap.keySet()) {
            tempMap.put(server, Integer.parseInt(jedisMap.get(server)));
        }
        synchronized (startPlayersLock) {
            startPlayers.clear();
            startPlayers.putAll(tempMap);
        }
    }

    private static void getMaxPlayers(Jedis jedis) {
        Map<String, Integer> tempMap = new HashMap<>();
        Map<String, String> jedisMap = jedis.hgetAll("maxPlayers");
        for (String server : jedisMap.keySet()) {
            tempMap.put(server, Integer.parseInt(jedisMap.get(server)));
        }
        synchronized (maxPlayersLock) {
            maxPlayers.clear();
            maxPlayers.putAll(tempMap);
        }
    }
    
    /*
    
    
    
    
    
    
    // public methods
    
    
    
    
    
     */

    public static String getMap(String server) {
        synchronized (mapsLock) {
            return maps.containsKey(server) ? maps.get(server) : "loading...";
        }
    }

    public static Map<String, String> getAllMaps() {
        synchronized (mapsLock) {
            return new HashMap<>(maps);
        }
    }

    public static String getStatus(String server) {
        synchronized (statusesLock) {
            return statuses.containsKey(server) ? statuses.get(server) : "lobby";
        }
    }

    public static Map<String, String> getAllStatuses() {
        synchronized (statusesLock) {
            return new HashMap<>(statuses);
        }
    }

    public static String getAvailability(String server) {
        synchronized (availabilitiesLock) {
            return availabilities.containsKey(server) ? availabilities.get(server) : "offline";
        }
    }

    public static Map<String, String> getAllAvailabilities() {
        synchronized (availabilitiesLock) {
            return new HashMap<>(availabilities);
        }
    }

    public static int getCount(String server) {
        synchronized (countsLock) {
            return counts.containsKey(server) ? counts.get(server) : 0;
        }
    }

    public static Map<String, Integer> getAllCounts() {
        synchronized (countsLock) {
            return new HashMap<>(counts);
        }
    }

    public static int getStartPlayers(String server) {
        synchronized (startPlayersLock) {
            return startPlayers.containsKey(server) ? startPlayers.get(server) : 0;
        }
    }

    public static Map<String, Integer> getAllStartPlayers() {
        synchronized (startPlayersLock) {
            return new HashMap<>(startPlayers);
        }
    }

    public static int getMaxPlayers(String server) {
        synchronized (maxPlayersLock) {
            return maxPlayers.containsKey(server) ? maxPlayers.get(server) : 0;
        }
    }

    public static Map<String, Integer> getAllMaxPlayers() {
        synchronized (maxPlayersLock) {
            return maxPlayers;
        }
    }
}
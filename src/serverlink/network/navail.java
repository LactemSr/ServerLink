package serverlink.network;

import serverlink.ServerLink;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;

public class navail {
    // everything in here runs async
    private static String availability = "open";

	/*
     * server availabilities: open, closed, offline
	 */

    public static void setup() {
        if (ServerLink.getServerName() == null) {
            Bukkit.getServer().getConsoleSender().sendMessage(ChatColor.RED + "SERVER NAME NOT SET!");
            return;
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                setOpen();
                Jedis jedis = JedisOne.getThree();
                jedis.hset("availability", ServerLink.getServerName(), "open");
                JedisOne.closeThree();
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public static void sync() {
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisOne.getThree();
                jedis.hset("availability", ServerLink.getServerName(), availability);
                JedisOne.closeThree();
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public static void shutdown() {
        Jedis jedis = JedisOne.getThree();
        jedis.hset("availability", ServerLink.getServerName(), "offline");
        JedisOne.closeThree();
    }

    synchronized public static void setOpen() {
        if (availability.equals("open")) {
            return;
        }
        availability = "open";
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisOne.getThree();
                jedis.hset("availability", ServerLink.getServerName(), "open");
                JedisOne.closeThree();
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    synchronized public static void setClosed() {
        if (availability.equals("closed")) {
            return;
        }
        availability = "closed";
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisOne.getThree();
                jedis.hset("availability", ServerLink.getServerName(), "closed");
                JedisOne.closeThree();
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    synchronized public static boolean isOpen() {
        return availability.equals("open");
    }

    synchronized public static boolean isClosed() {
        return !availability.equals("open");
    }
}

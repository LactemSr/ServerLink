package serverlink.network;


import net.citizensnpcs.api.CitizensAPI;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.bot.BotBaseTrait;
import serverlink.server.Reset;

public class ncount {
    public static final Object countLock = new Object();
    private static final Object startPlayersLock = new Object();
    private static final Object maxPlayersLock = new Object();
    private static Integer count = 0;
    private static Integer startPlayers = 100;
    private static Integer maxPlayers = 101;

    public static void incrCount() {
        synchronized (countLock) {
            Jedis jedis = JedisOne.getOne();
            long currentttl = jedis.ttl("reset");
            if (currentttl > Reset.ttl) {
                int playersOnline = 0;
                for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
                    if (aPlayer.getPlayer() == null) {
                        PlayerManager.onlineAlphaPlayers.remove(aPlayer);
                        PlayerManager.alphaPlayers.remove(aPlayer.getUuid());
                        continue;
                    }
                    if (aPlayer.getPlayer().isOnline() || (aPlayer.getPlayer().hasMetadata("NPC") && CitizensAPI.getNPCRegistry().getNPC(aPlayer.getPlayer()).getTrait(BotBaseTrait.class).ready))
                        playersOnline++;
                    else {
                        PlayerManager.onlineAlphaPlayers.remove(aPlayer);
                        PlayerManager.alphaPlayers.remove(aPlayer.getUuid());
                    }
                }
                count = playersOnline;
                jedis.hset("count", ServerLink.getServerName(), count.toString());
                Reset.sync(currentttl, playersOnline, getStartPlayers(), getMaxPlayers(), nmap.getMap());
            } else {
                Reset.ttl = currentttl;
                count++;
                jedis.hset("count", ServerLink.getServerName(), count.toString());
                jedis.incr("globalCount");
                jedis.incr(ServerLink.getServerType().toString().concat("Count"));
            }
            JedisOne.closeOne();
        }
    }

    public static void decrCount() {
        synchronized (countLock) {
            Jedis jedis = JedisOne.getOne();
            long currentttl = jedis.ttl("reset");
            if (currentttl > Reset.ttl) {
                int playersOnline = 0;
                for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
                    if (aPlayer.getPlayer() == null) {
                        PlayerManager.onlineAlphaPlayers.remove(aPlayer);
                        PlayerManager.alphaPlayers.remove(aPlayer.getUuid());
                        continue;
                    }
                    if (aPlayer.getPlayer().isOnline() || (aPlayer.getPlayer().hasMetadata("NPC") && CitizensAPI.getNPCRegistry().getNPC(aPlayer.getPlayer()).getTrait(BotBaseTrait.class).ready))
                        playersOnline++;
                    else {
                        PlayerManager.onlineAlphaPlayers.remove(aPlayer);
                        PlayerManager.alphaPlayers.remove(aPlayer.getUuid());
                    }
                }
                count = playersOnline;
                jedis.hset("count", ServerLink.getServerName(), count.toString());
                Reset.sync(currentttl, playersOnline, getStartPlayers(), getMaxPlayers(), nmap.getMap());
            } else {
                Reset.ttl = currentttl;
                if (count != 0) count--;
                jedis.hset("count", ServerLink.getServerName(), count.toString());
                if (jedis.decr(ServerLink.getServerType().toString().concat("Count")) < 0) {
                    jedis.set(ServerLink.getServerType().toString().concat("Count"), "0");
                }
                if (jedis.decr("globalCount") < 0) {
                    jedis.set("globalCount", "0");
                }
            }
            JedisOne.closeOne();
        }
    }

    public static int getCount() {
        synchronized (countLock) {
            return count;
        }
    }

    public static void setCount(Integer c) {
        synchronized (countLock) {
            count = c;
            Jedis jedis = JedisOne.getOne();
            jedis.hset("count", ServerLink.getServerName(), count.toString());
            JedisOne.closeOne();
        }
    }

    public static int getStartPlayers() {
        synchronized (startPlayersLock) {
            return startPlayers;
        }
    }

    public static void setStartPlayers(int i) {
        synchronized (startPlayersLock) {
            startPlayers = i;
            Jedis jedis = JedisOne.getOne();
            jedis.hset("startPlayers", ServerLink.getServerName(), startPlayers.toString());
            JedisOne.closeOne();
        }
    }

    public static int getMaxPlayers() {
        synchronized (maxPlayersLock) {
            return maxPlayers;
        }
    }

    public static void setMaxPlayers(int i) {
        synchronized (maxPlayersLock) {
            maxPlayers = i;
            Jedis jedis = JedisOne.getOne();
            jedis.hset("maxPlayers", ServerLink.getServerName(), maxPlayers.toString());
            JedisOne.closeOne();
        }
    }
}

package serverlink.network;

import serverlink.util.ConsoleOutput;
import redis.clients.jedis.Jedis;

import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class JedisOne {
    // Node 1 stores server info
    private static final Lock lock1 = new ReentrantLock();
    private static final Lock lock2 = new ReentrantLock();
    private static final Lock lock3 = new ReentrantLock();
    private static String ip;
    private static int port;
    private static Jedis jedis1;
    private static Jedis jedis2;
    private static Jedis jedis3;

    public static void setup() {
        try {
            jedis1 = new Jedis(ip, port, 5000);
        } catch (Exception e) {
            ConsoleOutput.printError(e.getMessage());
        }
        try {
            jedis2 = new Jedis(ip, port, 5000);
        } catch (Exception e) {
            ConsoleOutput.printError(e.getMessage());
        }
        try {
            jedis3 = new Jedis(ip, port, 5000);
        } catch (Exception e) {
            ConsoleOutput.printError(e.getMessage());
        }
    }

    public static void shutdown() {
        jedis1.close();
        jedis2.close();
        jedis3.close();
    }

    public static void setIP(String dbIP) {
        ip = dbIP;
    }

    public static void setPort(int dbPort) {
        port = dbPort;
    }

    public static Jedis getOne() {
        lock1.lock();
        return jedis1;
    }

    public static void closeOne() {
        lock1.unlock();
    }

    public static Jedis getTwo() {
        lock2.lock();
        return jedis2;
    }

    public static void closeTwo() {
        lock2.unlock();
    }

    public static Jedis getThree() {
        lock3.lock();
        return jedis3;
    }

    public static void closeThree() {
        lock3.unlock();
    }
}

package serverlink.network;

import serverlink.ServerLink;
import redis.clients.jedis.Jedis;

public class nmap {
    private static String map = "loading...";

    synchronized public static String getMap() {
        return map;
    }

    synchronized public static void setMap(String mapName) {
        map = mapName;
        Jedis jedis = JedisOne.getThree();
        jedis.hset("map", ServerLink.getServerName(), mapName);
        JedisOne.closeThree();
    }
}

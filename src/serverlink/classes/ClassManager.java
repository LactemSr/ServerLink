package serverlink.classes;

import com.avaje.ebean.validation.NotNull;
import net.citizensnpcs.util.PlayerAnimation;
import net.minecraft.server.v1_8_R3.Entity;
import net.minecraft.server.v1_8_R3.MathHelper;
import org.bukkit.*;
import org.bukkit.craftbukkit.libs.jline.internal.Nullable;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftLivingEntity;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.bot.BotManager;
import serverlink.chat.chat;
import serverlink.customevent.CustomPlayerDeathEvent;
import serverlink.customevent.CustomPlayerHurtLivingEntityEvent;
import serverlink.customevent.CustomPlayerHurtPlayerEvent;
import serverlink.customevent.CustomPlayerKillLivingEntityEvent;
import serverlink.inventory.InventoryManager;
import serverlink.player.damage.PlayerDamageEvents;
import serverlink.round.Round;
import serverlink.util.ConsoleOutput;
import serverlink.util.Item;
import serverlink.util.Probability;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

import static org.bukkit.event.entity.EntityDamageEvent.DamageCause.*;

public class ClassManager implements Round, Listener {

    private static ArrayList<Class> classes = new ArrayList<>();
    private static Class defaultClass = new BlankClass("default");
    private static HashMap<Entity, Long> setKbTimes = new HashMap<>();
    private static HashMap<Entity, Long> noKbTimes = new HashMap<>();

    public static boolean isInCombat(Player player) {
        return PlayerDamageEvents.attackedRecently.containsKey(player)
                || PlayerDamageEvents.lastDamager.containsKey(player);
    }

    /**
     * returns seconds (with half-second precision) until the player is out of combat
     */
    public static double getCombatTime(Player player) {
        if (PlayerDamageEvents.attackedRecently.containsKey(player)) {
            if (PlayerDamageEvents.lastDamagerTimeout.containsKey(player))
                return PlayerDamageEvents.lastDamagerTimeout.get(player) > PlayerDamageEvents.attackedRecently.get(player) ? PlayerDamageEvents.lastDamagerTimeout.get(player) : PlayerDamageEvents.attackedRecently.get(player);
            else return PlayerDamageEvents.attackedRecently.get(player);
        } else if (PlayerDamageEvents.lastDamagerTimeout.containsKey(player))
            return PlayerDamageEvents.lastDamagerTimeout.get(player);
        return 0;
    }

    public static LivingEntity getCombatOpponent(Player player) {
        if (PlayerDamageEvents.lastDamager.containsKey(player)) return PlayerDamageEvents.lastDamager.get(player);
        for (Player opponent : PlayerDamageEvents.lastDamager.keySet()) {
            if (PlayerDamageEvents.lastDamager.get(opponent) == player) return opponent;
        }
        return null;
    }

    /**
     * Assisters are players who damaged the player in the last 5 seconds.
     */
    public static
    @NotNull
    Set<Player> getAssisters(Player player) {
        return PlayerDamageEvents.lastAssisters.get(player).keySet();
    }

    // add class to list of available classes
    public static void addClass(Class cl) {
        if (classes.contains(cl)) return;
        if (cl.getFancyName() == null) cl.fancyName = "null";
        classes.add(cl);
        if (cl instanceof PowerClass) {
            try {
                ServerLink.addPreJoinMethod(ClassManager.class.getMethod("loadClassLvls", AlphaPlayer.class, Jedis.class));
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            }
        }
    }

    // must remain public for reflection to work
    public static void loadClassLvls(AlphaPlayer aPlayer, Jedis jedis) {
        for (Class cl : new ArrayList<>(classes)) {
            if (!(cl instanceof PowerClass)) continue;
            String expString = jedis.hget("settings:" + aPlayer.getUuid().toString(),
                    ServerLink.getServerType() + ":classExp" + cl.getName());
            double exp;
            if (expString == null) {
                exp = 0.0;
                expString = "0.0";
            } else exp = Double.parseDouble(expString);
            aPlayer.getData(ServerLink.getServerType() + ":classExp" + cl.getName()).setValue(exp);
            aPlayer.getData(ServerLink.getServerType() + ":classLevel" + cl.getName()).setValue(((PowerClass) cl).calculateLevelFromExp(exp));
            aPlayer.settings.put(ServerLink.getServerType() + ":classExp" + cl.getName(), expString);
            jedis.hset("settings:" + aPlayer.getUuid().toString(),
                    ServerLink.getServerType() + ":classExp" + cl.getName(), expString);
        }
    }

    /**
     * remove class from list of available classes
     */
    public static void removeClass(Class cl) {
        if (cl == defaultClass) {
            ConsoleOutput.printError("CANNOT REMOVE THE DEFAULT CLASS!!");
            return;
        }
        classes.remove(cl);
        for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
            aPlayer.getData("class").setValue(defaultClass);
            unEquipClass(aPlayer.getPlayer(), cl);
        }
    }

    public static Class getDefaultClass() {
        return defaultClass;
    }

    public static void setDefaultClass(Class defaultClass) {
        ClassManager.defaultClass = defaultClass;
    }

    public static Class getClassFromName(String name) {
        name = ChatColor.stripColor(name.trim());
        for (Class cl : classes) {
            if (cl.getName().equalsIgnoreCase(name)) return cl;
        }
        return null;
    }

    public static Class getClassFromFancyName(String name) {
        name = ChatColor.stripColor(chat.color(name));
        for (Class cl : classes) {
            if (ChatColor.stripColor(chat.color(cl.getFancyName())).equalsIgnoreCase(name)) return cl;
        }
        return null;
    }

    public static Class getClassFromWeaponType(WeaponType type) {
        for (Class cl : classes) for (WeaponType wT : cl.getWeaponTypes()) if (wT == type) return cl;
        return null;
    }

    public static ArrayList<Class> getClasses() {
        return classes;
    }

    /**
     * unequips last class, gives the player class items, and equips class
     */
    public static void equipClass(Player player, Class cl, @Nullable Class oldClass) {
        InventoryManager.clearInventory(player);
        if (oldClass != null) unEquipClass(player, oldClass);
        PlayerManager.get(player).clearWeapons();
        for (WeaponType weaponType : cl.getWeaponTypes()) {
            if (weaponType.getItem() == null || weaponType.getItem().getType() == Material.AIR) continue;
            Weapon weapon = new Weapon(weaponType, player);
            player.getInventory().setItem(weaponType.getItemSlot() - 1, weapon.getItem());
            PlayerManager.get(player).addWeapon(weapon);
        }
        player.getInventory().setHelmet(cl.getHelmet(player));
        player.getInventory().setChestplate(cl.getChestplate(player));
        player.getInventory().setLeggings(cl.getLeggings(player));
        player.getInventory().setBoots(cl.getBoots(player));
        if (!cl.alreadyEquipped.contains(player)) {
            cl.onEquip(player);
            cl.alreadyEquipped.add(player);
            cl.playerDefenses.put(player, cl.getBaseDefense());
            cl.playerKbTaken.put(player, cl.getBasePercentKnockbackTaken());
            cl.playerFistDamageDealt.put(player, cl.getBaseFistDamageDealt());
            cl.playerFistKbDealt.put(player, cl.getBasePercentFistKnockbackDealt());
        }
    }

    public static void unEquipClass(Player player, Class cl) {
        if (!cl.alreadyEquipped.contains(player)) return;
        cl.onUnequip(player);
        cl.alreadyEquipped.remove(player);
        cl.playerDefenses.remove(player);
        cl.playerKbTaken.remove(player);
        cl.playerFistDamageDealt.remove(player);
        cl.playerFistKbDealt.remove(player);
    }

    /**
     * gets the player's Weapon (not unassigned WeaponType) that can use Ability
     */
    public static Weapon getWeaponFromAbility(Player p, Ability ability) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        for (Weapon weapon : aPlayer.getWeapons()) {
            for (Ability abil : weapon.getType().getAbilities()) {
                if (abil == ability) return weapon;
            }
        }
        return null;
    }

    /**
     * If you shoot something with a delay (like an arrow barrage), you need to call
     * this in order to associate the projectile with the weapon it was fired from.
     * Otherwise the player could switch slots and have projectiles associated with
     * another weapon.
     */
    public static void tagProjectile(Projectile proj, Weapon weaponToAssociateWith) {
        ClassEvents.projectileLaunchers.put(proj, weaponToAssociateWith);
    }

    public static
    @Nullable
    Weapon getWeaponedTaggedToProjectile(Projectile proj) {
        return ClassEvents.projectileLaunchers.get(proj);
    }

    /**
     * Should only be needed in very special circumstances such as flagging players
     * hurting mobs and for very special class abilities.
     * If you only want to mark one player as in combat, input the player and null
     * in either order.
     */
    public static void markInCombat(LivingEntity attacker, LivingEntity victim) {
        if (attacker != null && attacker instanceof Player)
            PlayerDamageEvents.attackedRecently.put((Player) attacker, 7.0);
        if (victim instanceof Player) {
            if (attacker != null) {
                // make the last attacker an assister if it was a player (only players get assists)
                if (PlayerDamageEvents.lastDamager.containsKey(victim) && PlayerDamageEvents.lastDamager.get(victim) != attacker) {
                    if (PlayerDamageEvents.lastDamager.get(victim) instanceof Player)
                        PlayerDamageEvents.lastAssisters.get(victim).put((Player) PlayerDamageEvents.lastDamager.get(victim), PlayerDamageEvents.lastDamagerTimeout.get(victim));
                }
                // make this attacker the new lastDamager
                PlayerDamageEvents.lastAssisters.get(victim).remove(attacker);
                PlayerDamageEvents.lastDamager.put((Player) victim, attacker);
                // mark player as having taken damage
                PlayerDamageEvents.lastDamagerTimeout.put((Player) victim, 7.0);
            }
        }
    }

    /**
     * This is not to be called from within ServerLink!
     * You're doing something wrong if you use this for entity vs. entity.
     */
    public static void dealDamage(LivingEntity victim, LivingEntity attacker, Double damage, Weapon weapon, EntityDamageEvent.DamageCause cause) {
        if (victim instanceof Player) {
            if (attacker instanceof Player) {
                // player attacking player
                dealDamage_pvp((Player) victim, (Player) attacker, damage, weapon, cause, false, false);
            } else {
                // entity attacking player
                if (!attacker.getCustomName().equalsIgnoreCase(attacker.getType().name().toLowerCase().replaceAll("_", "").replace("entity", "")))
                    dealDamage((Player) victim, "by " + attacker.getCustomName(), damage, cause, true);
                else
                    dealDamage((Player) victim, "by a " + attacker.getName(), damage, cause, true);
            }
            return;
        }
        // player attacking entity
        dealDamage_pve(victim, (Player) attacker, damage, weapon, cause, false);
    }

    /**
     * Used for marking two players as in combat in a special ability
     * Checks to see if the damage would kill the player.
     * Accounts for defense.
     * Does not apply knockback or change player's velocity.
     */
    public static void dealDamage_NoKb(LivingEntity victim, LivingEntity attacker, Double damage, Weapon weapon, EntityDamageEvent.DamageCause cause, boolean playRedHurtAnimation) {
        if (victim instanceof Player) {
            if (attacker instanceof Player) {
                // player attacking player
                dealDamage_pvp((Player) victim, (Player) attacker, damage, weapon, cause, false, true);
            } else {
                // entity attacking player
                dealDamage((Player) victim, attacker.getName(), damage, cause, playRedHurtAnimation);
            }
            return;
        }
        // player attacking entity
        dealDamage_pve(victim, (Player) attacker, damage, weapon, cause, playRedHurtAnimation);
    }

    /**
     * Checks to see if the damage would kill the player.
     * Accounts for defense.
     * Does not apply knockback or change player's velocity.
     */
    public static void dealDamage(Player victim, String deathReason, Double damage, EntityDamageEvent.DamageCause cause, boolean playRedHurtAnimation) {
        Class victimClass = ((Class) PlayerManager.get(victim).getData("class").getValue());
        damage = getDamageFromArmor(victim.getInventory().getArmorContents(), victimClass.modifyDamageFromCause(Math.max(0, damage - victimClass.getDefense(victim)), cause), cause);
        if (damage < 0) damage = 0.0;
        if (damage >= victim.getHealth()) {
            victim.setVelocity(new Vector(0, 0, 0));
            if (playRedHurtAnimation) {
                victim.playEffect(EntityEffect.HURT);
                victim.playSound(victim.getLocation(), Sound.CAT_HISS, 10, 1);
                if (victim.hasMetadata("NPC")) PlayerAnimation.HURT.play(victim);
            }
            Bukkit.getPluginManager().callEvent(new CustomPlayerDeathEvent(victim, deathReason));
            PlayerDamageEvents.lastDamager.remove(victim);
            PlayerDamageEvents.lastAssisters.get(victim).clear();
            // remove victim so he is no longer "in combat" if he attacked someone before dying
            PlayerDamageEvents.attackedRecently.remove(victim);
        } else {
            if (playRedHurtAnimation) {
                victim.playEffect(EntityEffect.HURT);
                victim.playSound(victim.getLocation(), Sound.HURT_FLESH, 10, 1);
                if (victim.hasMetadata("NPC")) PlayerAnimation.HURT.play(victim);
            }
            victim.setHealth(victim.getHealth() - damage);
        }
    }

    /**
     * Checks to see if the damage would kill the player.
     * Accounts for defense.
     * Applies knockback based on weapon's knockback multiplier.
     */
    static boolean dealDamage_pvp(Player victim, Player attacker, Double initialDamage, Weapon weapon, EntityDamageEvent.DamageCause cause, boolean calledFromDamageEvent, boolean cancelKnockback) {
        Class attackerClass = (Class) PlayerManager.get(attacker).getData("class").getValue();
        Class victimClass = (Class) PlayerManager.get(victim).getData("class").getValue();
        double damage = getDamageFromArmor(victim.getInventory().getArmorContents(), victimClass.modifyDamageFromCause(Math.max(0, initialDamage - victimClass.getDefense(victim)), cause), cause);
        if (damage <= 0) {
            if (initialDamage > 0) damage = 0.5;
            else damage = 0.0;
        }
        CustomPlayerHurtPlayerEvent event = new CustomPlayerHurtPlayerEvent(victim, attacker, damage, victimClass, attackerClass);
        Bukkit.getPluginManager().callEvent(event);
        if (event.isCancelled()) return false;
        damage = event.getDamage();
        if (!cancelKnockback) {
            double knockbackMultiplier;
            if (weapon == null) knockbackMultiplier = attackerClass.getPercentFistKnockbackDealt(attacker);
            else {
                if (cause == PROJECTILE) {
                    if (attackerClass instanceof PowerClass && ClassStat.customStats.containsKey(attackerClass.getName() + "weaponProjKnockback" + weapon.getType().getItemSlot()))
                        knockbackMultiplier = ((PowerClass) attackerClass).getStat(ClassStat.weaponProjKnockback(attackerClass, weapon.getType().getItemSlot()),
                                weapon.getPercentProjKnockback(),
                                attacker);
                    else knockbackMultiplier = weapon.getPercentProjKnockback();
                } else {
                    if (attackerClass instanceof PowerClass && ClassStat.customStats.containsKey(attackerClass.getName() + "weaponKnockback" + weapon.getType().getItemSlot()))
                        knockbackMultiplier = ((PowerClass) attackerClass).getStat(ClassStat.weaponKnockback(attackerClass, weapon.getType().getItemSlot()),
                                weapon.getPercentKnockback(),
                                attacker);
                    else knockbackMultiplier = weapon.getPercentKnockback();
                }
            }
            double knockbackDefense = victimClass.getPercentKnockbackTaken(victim);
            double knockback = (knockbackDefense - 100.0) + knockbackMultiplier;
            if (knockback < 0) knockback = 0.0;
            if (calledFromDamageEvent && !attacker.hasMetadata("NPC")) ClassEvents.nextVelMult.put(victim, knockback);
            else victim.setVelocity(getKnockbackFromHit(victim, attacker, damage, knockbackMultiplier));
        }
        // add exp if PowerClass
        if (attackerClass instanceof PowerClass) {
            ((PowerClass) attackerClass).addExp(PlayerManager.get(attacker), damage);
        }
        // make current attacker victim's lastDamager, make last attacker an assister
        if (PlayerDamageEvents.lastDamager.containsKey(victim) && PlayerDamageEvents.lastDamager.get(victim) != attacker) {
            if (PlayerDamageEvents.lastDamager.get(victim) instanceof Player)
                PlayerDamageEvents.lastAssisters.get(victim).put((Player) PlayerDamageEvents.lastDamager.get(victim), PlayerDamageEvents.lastDamagerTimeout.get(victim));
        }
        PlayerDamageEvents.lastAssisters.get(victim).remove(attacker);
        PlayerDamageEvents.lastDamager.put(victim, attacker);
        PlayerDamageEvents.lastDamagerTimeout.put(victim, 7.0);
        PlayerDamageEvents.attackedRecently.put(attacker, 7.0);
        if (damage >= victim.getHealth()) {
            victim.setVelocity(new Vector(0, 0, 0));
            if (victim.hasMetadata("NPC"))
                PlayerAnimation.HURT.play(victim);
            victim.playEffect(EntityEffect.HURT);
            victim.playSound(victim.getLocation(), Sound.CAT_HISS, 10, 1);
            attacker.playSound(victim.getLocation(), Sound.LEVEL_UP, 10, 1);
            Bukkit.getPluginManager().callEvent(new CustomPlayerKillLivingEntityEvent(attacker, victim));
            if (attacker.isOnline())
                ((Class) PlayerManager.get(attacker).getData("class").getValue()).onKill(attacker, victim, weapon);
            PlayerDamageEvents.lastDamager.remove(victim);
            PlayerDamageEvents.lastAssisters.get(victim).clear();
            // remove victim so he is no longer "in combat" if he attacked someone before dying
            PlayerDamageEvents.attackedRecently.remove(victim);
        } else {
            victim.setHealth(victim.getHealth() - damage);
            if (victim.hasMetadata("NPC"))
                PlayerAnimation.HURT.play(victim);
            victim.playSound(victim.getLocation(), Sound.HURT_FLESH, 10, 1);
            attacker.playSound(victim.getLocation(), Sound.HURT_FLESH, 10, 1);
            if (calledFromDamageEvent && !victim.hasMetadata("NPC") && !attacker.hasMetadata("NPC")) return true;
            if (!victim.hasMetadata("NPC") && !calledFromDamageEvent) victim.playEffect(EntityEffect.HURT);
            // set victim on fire if weapon has fire aspect or flame
            if (weapon == null) return true;
            ItemStack weaponItem = weapon.getItem();
            for (ItemStack is : attacker.getInventory().getContents()) {
                if (is != null && Item.isMostlySimilar(is, weaponItem)) {
                    weaponItem = is;
                    break;
                }
            }
            if (weaponItem.getEnchantments().containsKey(Enchantment.FIRE_ASPECT)) {
                if (weaponItem.getEnchantmentLevel(Enchantment.FIRE_ASPECT) == 1)
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            victim.setFireTicks(100);
                            if (victim.hasMetadata("NPC")) BotManager.setFireTicks(victim, 100);
                        }
                    }.runTask(ServerLink.plugin);
                else if (weaponItem.getEnchantmentLevel(Enchantment.FIRE_ASPECT) == 2)
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            victim.setFireTicks(140);
                            if (victim.hasMetadata("NPC")) BotManager.setFireTicks(victim, 140);
                        }
                    }.runTask(ServerLink.plugin);
                else
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            victim.setFireTicks(200);
                            if (victim.hasMetadata("NPC")) BotManager.setFireTicks(victim, 200);
                        }
                    }.runTask(ServerLink.plugin);
            } else if (weaponItem.getEnchantments().containsKey(Enchantment.ARROW_FIRE)) {
                if (weaponItem.getEnchantmentLevel(Enchantment.ARROW_FIRE) == 1) {
                    victim.setFireTicks(100);
                    if (victim.hasMetadata("NPC")) BotManager.setFireTicks(victim, 100);
                } else if (weaponItem.getEnchantmentLevel(Enchantment.ARROW_FIRE) == 2) {
                    victim.setFireTicks(140);
                    if (victim.hasMetadata("NPC")) BotManager.setFireTicks(victim, 140);
                } else {
                    victim.setFireTicks(200);
                    if (victim.hasMetadata("NPC")) BotManager.setFireTicks(victim, 200);
                }
            }
        }
        return true;
    }

    /**
     * Checks to see if the damage would kill the entity.
     * Accounts for defense.
     * Applies knockback based on weapon's knockback multiplier.
     */
    static void dealDamage_pve(LivingEntity victim, Player attacker, Double initialDamage, Weapon weapon, EntityDamageEvent.DamageCause cause, boolean cancelKnockback) {
        if (victim.hasMetadata("owner")) {
            Object victimOwner = victim.getMetadata("owner").get(0).value();
            if (victimOwner == attacker) return;
        }
        Class attackerClass = (Class) PlayerManager.get(attacker).getData("class").getValue();
        double damage = getDamageFromArmor(victim.getEquipment().getArmorContents(), initialDamage, cause);
        if (damage <= 0) {
            if (initialDamage > 0) damage = 0.5;
            else damage = 0.0;
        }
        CustomPlayerHurtLivingEntityEvent event = new CustomPlayerHurtLivingEntityEvent(victim, attacker, damage, attackerClass);
        Bukkit.getPluginManager().callEvent(event);
        if (event.isCancelled()) return;
        damage = event.getDamage();
        if (!cancelKnockback) {
            double knockback;
            if (weapon == null) knockback = attackerClass.getPercentFistKnockbackDealt(attacker);
            else {
                if (cause == PROJECTILE) {
                    if (attackerClass instanceof PowerClass && ClassStat.customStats.containsKey(attackerClass.getName() + "weaponProjKnockback" + weapon.getType().getItemSlot()))
                        knockback = ((PowerClass) attackerClass).getStat(ClassStat.weaponProjKnockback(attackerClass, weapon.getType().getItemSlot()),
                                weapon.getPercentProjKnockback(),
                                attacker);
                    else knockback = weapon.getPercentProjKnockback();
                } else {
                    if (attackerClass instanceof PowerClass && ClassStat.customStats.containsKey(attackerClass.getName() + "weaponKnockback" + weapon.getType().getItemSlot()))
                        knockback = ((PowerClass) attackerClass).getStat(ClassStat.weaponKnockback(attackerClass, weapon.getType().getItemSlot()),
                                weapon.getPercentKnockback(),
                                attacker);
                    else knockback = weapon.getPercentKnockback();
                }
            }
            victim.setVelocity(getKnockbackFromHit(victim, attacker, damage, knockback));
        }
        // add exp if PowerClass
        if (attackerClass instanceof PowerClass) {
            ((PowerClass) attackerClass).addExp(PlayerManager.get(attacker), damage);
        }
        // set player(s) involved as in combat
        PlayerDamageEvents.attackedRecently.put(attacker, 7.0);
        if (victim.hasMetadata("owner")) {
            Player victimOwner = (Player) victim.getMetadata("owner").get(0).value();
            if (PlayerDamageEvents.lastDamager.containsKey(victimOwner) && PlayerDamageEvents.lastDamager.get(victimOwner) != attacker) {
                if (PlayerDamageEvents.lastDamager.get(victimOwner) instanceof Player)
                    PlayerDamageEvents.lastAssisters.get(victimOwner).put((Player) PlayerDamageEvents.lastDamager.get(victimOwner), PlayerDamageEvents.lastDamagerTimeout.get(victimOwner));
            }
            PlayerDamageEvents.lastAssisters.get(victimOwner).remove(attacker);
            PlayerDamageEvents.lastDamager.put(victimOwner, attacker);
            PlayerDamageEvents.lastDamagerTimeout.put(victimOwner, 7.0);
        }
        if (damage >= victim.getHealth()) {
            victim.setVelocity(new Vector(0, 0, 0));
            victim.playEffect(EntityEffect.HURT);
            attacker.playSound(victim.getLocation(), Sound.LEVEL_UP, 10, 1);
            Bukkit.getPluginManager().callEvent(new CustomPlayerKillLivingEntityEvent(attacker, victim));
            if (attacker.isOnline())
                ((Class) PlayerManager.get(attacker).getData("class").getValue()).onKill(attacker, victim, weapon);
            victim.remove();
        } else {
            victim.setHealth(victim.getHealth() - damage);
            victim.playEffect(EntityEffect.HURT);
            attacker.playSound(victim.getLocation(), Sound.HURT_FLESH, 10, 1);
            // set victim on fire if weapon has fire aspect or flame
            if (weapon == null) return;
            ItemStack weaponItem = weapon.getItem();
            for (ItemStack is : attacker.getInventory().getContents()) {
                if (is != null && Item.isMostlySimilar(is, weaponItem)) {
                    weaponItem = is;
                    break;
                }
            }
            if (weaponItem.getEnchantments().containsKey(Enchantment.FIRE_ASPECT)) {
                if (weaponItem.getEnchantmentLevel(Enchantment.FIRE_ASPECT) == 1)
                    victim.setFireTicks(100);
                else if (weaponItem.getEnchantmentLevel(Enchantment.FIRE_ASPECT) == 2)
                    victim.setFireTicks(140);
                else
                    victim.setFireTicks(200);
            } else if (weaponItem.getEnchantments().containsKey(Enchantment.ARROW_FIRE)) {
                if (weaponItem.getEnchantmentLevel(Enchantment.ARROW_FIRE) == 1)
                    victim.setFireTicks(100);
                else if (weaponItem.getEnchantmentLevel(Enchantment.ARROW_FIRE) == 2)
                    victim.setFireTicks(140);
                else
                    victim.setFireTicks(200);
            }
        }
    }

    /**
     * Returns a new double that is lower than the inputted damage according to
     * victim's armor and damage cause (takes into account enchantments, fire, etc.).
     * <p>
     * This is based off of Minecraft's damage formula without complicated
     * things like armor toughness and durability.
     * Defense points (armor type) are given more weight here than in Minecraft's
     * formula.
     */
    private static double getDamageFromArmor(ItemStack[] armorContents, double damage, EntityDamageEvent.DamageCause cause) {
        double defense = 0;
        double enchantmentDefense = 0;
        for (ItemStack i : armorContents) {
            if (i == null) continue;
            String name = i.getType().name().toLowerCase();
            if (name.contains("diamond")) {
                if (name.contains("boots") || name.equals("helmet")) defense += 3;
                else if (name.contains("leggings")) defense += 6;
                else if (name.contains("chestplate")) defense += 8;
            } else if (i.getType().name().toLowerCase().contains("iron")) {
                if (name.contains("boots") || name.equals("helmet")) defense += 2;
                else if (name.contains("leggings")) defense += 5;
                else if (name.contains("chestplate")) defense += 6;
            } else if (i.getType().name().toLowerCase().contains("chain")) {
                if (name.contains("boots")) defense += 1;
                else if (name.contains("leggings")) defense += 4;
                else if (name.contains("chestplate")) defense += 5;
                else if (name.contains("helmet")) defense += 2;
            } else if (i.getType().name().toLowerCase().contains("gold")) {
                if (name.contains("boots")) defense += 1;
                else if (name.contains("leggings")) defense += 5;
                else if (name.contains("chestplate")) defense += 6;
                else if (name.contains("helmet")) defense += 2;
            } else if (i.getType().name().toLowerCase().contains("leather")) {
                if (name.contains("boots") || name.equals("helmet")) defense += 1;
                else if (name.contains("leggings")) defense += 2;
                else if (name.contains("chestplate")) defense += 3;
            }
            // This is all based off of Minecraft's defaults
            for (Enchantment ench : i.getEnchantments().keySet()) {
                if (ench == Enchantment.PROTECTION_ENVIRONMENTAL)
                    enchantmentDefense += i.getEnchantments().get(ench);
                else if (ench == Enchantment.PROTECTION_EXPLOSIONS && (cause == BLOCK_EXPLOSION || cause == ENTITY_EXPLOSION))
                    enchantmentDefense += 2 * i.getEnchantments().get(ench);
                else if (ench == Enchantment.PROTECTION_FALL && cause == FALL)
                    enchantmentDefense += 3 * i.getEnchantments().get(ench);
                else if (ench == Enchantment.PROTECTION_PROJECTILE && cause == PROJECTILE)
                    enchantmentDefense += 3 * i.getEnchantments().get(ench);
                else if (ench == Enchantment.PROTECTION_FIRE && (cause == FIRE || cause == FIRE_TICK || cause == LAVA))
                    enchantmentDefense += 2 * i.getEnchantments().get(ench);
            }
        }
        // This is based off of Minecraft's damage formula without complicated
        // stuff like armor toughness and durability.
        // Defense points (armor) are given more weight than in Minecraft's formula.
        return damage * (1.0 - (Math.min(20.0, defense / 1.2) / 25.0)) * (1.0 - (Math.min(20.0, enchantmentDefense) / 25.0));
    }

    /**
     * Returns a vector to be used with Bukkit's setVelocity(). This is only
     * loosely based off of Minecraft's defaults, but through testing it has
     * been made to feel similar while allowing for modifying knockback dealt.
     * <p>
     * kbMultiplier is a percent out of 100.
     */
    public static Vector getKnockbackFromHit(LivingEntity entity, LivingEntity attacker, double damage, double kbMultiplier) {
        Entity nmsEntity = ((CraftLivingEntity) entity).getHandle();
        Entity nmsAttacker = ((CraftLivingEntity) attacker).getHandle();
        double distanceX = Math.min(5.0, nmsAttacker.locX - nmsEntity.locX);
        double distanceZ = Math.min(5.0, nmsAttacker.locZ - nmsEntity.locZ);
        float distance = MathHelper.sqrt(distanceX * distanceX + distanceZ * distanceZ);
        Vector vec = new Vector((distanceX / distance) * -0.62, 0, (distanceZ / distance) * -0.62);
        if (!setKbTimes.containsKey(nmsEntity)) setKbTimes.put(nmsEntity, System.currentTimeMillis() - 100);
        if (setKbTimes.get(nmsEntity) <= System.currentTimeMillis()) {
            vec.setY(0.364);
            setKbTimes.put(nmsEntity, System.currentTimeMillis() + 400);
        }
        if (Probability.get(33)) {
            vec.setX(vec.getX() * 1.18);
            vec.setY(vec.getY() * 1.08);
            vec.setZ(vec.getZ() * 1.18);
        }
        double damageMultiplier = Math.min(1.6, Math.max(1.0, 0.23 * damage));
        vec.setX(vec.getX() * damageMultiplier);
        vec.setZ(vec.getZ() * damageMultiplier);

        return vec.multiply(kbMultiplier / 100.0).setY(Math.max(0, vec.getY() - entity.getVelocity().getY()) + entity.getVelocity().getY());
    }

    @Override
    public void onLobbyCountdownStart() {
    }

    @Override
    public void onLobbyCountdownCancel() {
    }

    @Override
    public void onMapCountdownStart() {
    }

    @Override
    public void onMapCountdownCancel() {
        for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
            aPlayer.clearWeapons();
            unEquipClass(aPlayer.getPlayer(), (Class) aPlayer.getData("class", defaultClass).getValue());
        }
    }

    @Override
    public void onGameStart() {
    }

    @Override
    public void onEndCountdownStart() {
    }

    @Override
    public void onRoundEnd() {
        PlayerManager.getOnlineAlphaPlayers().forEach(AlphaPlayer::clearWeapons);
        for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
            aPlayer.clearWeapons();
            unEquipClass(aPlayer.getPlayer(), (Class) aPlayer.getData("class", defaultClass).getValue());
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    private void onCustomPlayerDeath(CustomPlayerDeathEvent e) {
        AlphaPlayer aPlayer = PlayerManager.get(e.getPlayer().getUniqueId());
        aPlayer.clearWeapons();
        unEquipClass(aPlayer.getPlayer(), (Class) aPlayer.getData("class", defaultClass).getValue());
    }

    @EventHandler(priority = EventPriority.LOW)
    private void onPlayerKillEntity(CustomPlayerKillLivingEntityEvent e) {
        if (e.getEntity().getType() != EntityType.PLAYER) return;
        AlphaPlayer aPlayer = PlayerManager.get((Player) e.getEntity());
        aPlayer.clearWeapons();
        unEquipClass(aPlayer.getPlayer(), (Class) aPlayer.getData("class", defaultClass).getValue());
    }

    public static class BlankClass extends Class {
        public BlankClass(String name) {
            super(name, name, name, 31, 0, new ItemStack(Material.AIR));
        }

        public BlankClass(String name, ItemStack item) {
            super(name, name, name, 31, 0, item);
        }

        @Override
        public void onEquip(Player player) {
        }

        @Override
        public void onUnequip(Player player) {
        }

        @Override
        public void onKill(Player player, LivingEntity victim, Weapon weaponUsed) {
        }

        @Override
        public double modifyDamageFromCause(double damage, EntityDamageEvent.DamageCause cause) {
            return damage;
        }
    }
}

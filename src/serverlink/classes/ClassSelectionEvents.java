package serverlink.classes;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.ItemStack;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.customevent.CustomChangeClassEvent;
import serverlink.customevent.CustomGuiClickEvent;
import serverlink.customevent.CustomNpcClickEvent;
import serverlink.player.Permissions;
import serverlink.server.ServerType;

public class ClassSelectionEvents implements Listener {

    @EventHandler
    public void onGuiClick(CustomGuiClickEvent e) {
        Player player = e.getPlayer();
        ItemStack item = e.getItem();
        if (!ChatColor.stripColor(player.getOpenInventory().getTitle()).equalsIgnoreCase("Classes Menu")) return;
        String itemName = ChatColor.stripColor(item.getItemMeta().getDisplayName());
        Class cl;
        if (itemName.contains("  lvl ")) cl = ClassManager.getClassFromFancyName(itemName.split("  lvl")[0]);
        else cl = ClassManager.getClassFromFancyName(itemName);
        if (cl instanceof PowerClass) ClassInventories.openPowerClassInventory(player, (PowerClass) cl);
        else ClassInventories.openClassInventory(player, cl);
    }

    @EventHandler
    private void onNpcClick(CustomNpcClickEvent e) {
        // TODO rework for levels, exp, and effects if possible; make call
        // TODO openClassMenu() or openPowerClassMenu()
        /*
        Player player = e.getPlayer();
        AlphaPlayer aPlayer = PlayerManager.get(player);
        if (!NpcManager.getClassNpcs().contains(e.getEntityId())) return;
        Class cl = NpcManager.classNpcs.get(e.getEntityId());
        // class already equipped
        Class currentClass = (Class) maxPlayer.getData("class").getValue();
        assert cl != null;
        if (cl == currentClass) {
            player.sendMessage(achat.color(achat.getServer() + "You're already " + cl.getFancyName() + "&f class."));
            return;
        }
        // if it's unlocked, equip it
        if (cl == ClassManager.getDefaultClass() ||
                player.hasPermission(ServerLink.getServerType() + "." + cl.getName())) {
            player.sendMessage(achat.color(achat.getServer() + "&aYou equipped "
                    + cl.getFancyName() + "."));
            aPlayer.getData("class").setValue(cl);
            Bukkit.getPluginManager().callEvent(new CustomChangeClassEvent(player, currentClass));
            player.closeInventory();
        } else if (cl.getPrice() > 0) {
            if (aPlayer.getPoints() < cl.getPrice()) {
                player.sendMessage(achat.color(achat.getServer() + "You need " +
                        (cl.getPrice() - maxPlayer.getPoints()) + " more points before you " +
                        "can buy " + cl.getFancyName() + "."));
                return;
            }
            openBuyClassInv(player, cl);
        } else {
            player.sendMessage(achat.color(achat.getServer() + "You haven't unlocked " +
                    cl.getFancyName() + "&f class."));
        }
        */
    }

    @EventHandler
    private void onChangeClass(CustomChangeClassEvent e) {
        PlayerManager.get(e.getPlayer()).getData("classesMenuGui").setValue(null);
    }

    @EventHandler
    private void confirmBuy(InventoryClickEvent e) {
        if (e.getInventory().getName().contains("Buy class ")) {
            e.setCancelled(true);
            ClickType click = e.getClick();
            if (!(e.getWhoClicked() instanceof Player)) return;
            if (!(click == ClickType.LEFT || click == ClickType.RIGHT || click == ClickType.SHIFT_LEFT
                    || click == ClickType.SHIFT_RIGHT)) {
                e.setCancelled(true);
                return;
            }
            if (e.getSlot() > 26) {
                e.setCancelled(true);
                return;
            }
            Player player = (Player) e.getWhoClicked();
            AlphaPlayer aPlayer = PlayerManager.get(player);
            Material currentType = e.getCurrentItem().getType();
            Material emeraldBlock = Material.EMERALD_BLOCK;
            Material redstoneBlock = Material.REDSTONE_BLOCK;
            if (currentType == emeraldBlock) {
                Class cl = ClassManager.getClassFromFancyName(e.getInventory().getName().substring(10, e.getInventory().getName().length() - 1));
                assert cl != null;
                int price = cl.getPrice();
                if (aPlayer.getPoints() < price) {
                    player.closeInventory();
                    player.sendMessage(chat.color(
                            ServerLink.getFancyServerName() + "You don't have enough " + ServerType.getThisCurrency() + "."));
                    e.setCancelled(true);
                    return;
                }
                aPlayer.takePoints(price);
                Class oldClass = (Class) aPlayer.getData("class").getValue();
                aPlayer.getData("class").setValue(cl);
                Bukkit.getPluginManager().callEvent(new CustomChangeClassEvent(player, oldClass));
                Permissions.addPerm(player.getName(), ServerLink.getServerType() + "." + cl.getName());
                e.setCancelled(true);
                player.closeInventory();
                player.sendMessage(chat.color(chat.getServer() + "&aYou bought and equipped " + cl.getFancyName() + "."));
                e.setCancelled(true);
                aPlayer.getData(cl.getName() + "ClassMenu").setValue(null);
            } else if (currentType == redstoneBlock) {
                player.closeInventory();
            }
        } else if (e.getInventory().getName().contains("Buy ❤❤❤ ")) {
            e.setCancelled(true);
            ClickType click = e.getClick();
            if (!(e.getWhoClicked() instanceof Player)) return;
            if (!(click == ClickType.LEFT || click == ClickType.RIGHT || click == ClickType.SHIFT_LEFT
                    || click == ClickType.SHIFT_RIGHT)) {
                return;
            }
            if (e.getSlot() > 26) {
                return;
            }
            Player player = (Player) e.getWhoClicked();
            AlphaPlayer aPlayer = PlayerManager.get(player);
            Material currentType = e.getCurrentItem().getType();
            Material emeraldBlock = Material.EMERALD_BLOCK;
            Material redstoneBlock = Material.REDSTONE_BLOCK;
            if (currentType == emeraldBlock) {
                Class cl = ClassManager.getClassFromFancyName(e.getInventory().getName().replace("Buy ❤❤❤ effect for ", "").replace("?", ""));
                assert cl != null;
                int price = cl.getHeartEffPrice();
                if (aPlayer.getCoins() < price) {
                    player.closeInventory();
                    player.sendMessage(chat.color(
                            ServerLink.getFancyServerName() + "You don't have enough AC Coins."));
                    return;
                }
                aPlayer.takeCoins(price);
                Permissions.addPerm(player.getName(), ServerLink.getServerType() + "." + cl.getName() + "HeartEffect");
                aPlayer.getData(cl.getName() + "ClassMenu").setValue(null);
                if (cl instanceof PowerClass)
                    ClassInventories.openPowerClassInventory(player, (PowerClass) cl);
                else
                    ClassInventories.openClassInventory(player, cl);
            } else if (currentType == redstoneBlock) {
                player.closeInventory();
            }
        } else if (e.getInventory().getName().contains("Buy ✰✰✰ ")) {
            e.setCancelled(true);
            ClickType click = e.getClick();
            if (!(e.getWhoClicked() instanceof Player)) return;
            if (!(click == ClickType.LEFT || click == ClickType.RIGHT || click == ClickType.SHIFT_LEFT
                    || click == ClickType.SHIFT_RIGHT)) {
                return;
            }
            if (e.getSlot() > 26) {
                e.setCancelled(true);
                return;
            }
            Player player = (Player) e.getWhoClicked();
            AlphaPlayer aPlayer = PlayerManager.get(player);
            Material currentType = e.getCurrentItem().getType();
            Material emeraldBlock = Material.EMERALD_BLOCK;
            Material redstoneBlock = Material.REDSTONE_BLOCK;
            if (currentType == emeraldBlock) {
                Class cl = ClassManager.getClassFromFancyName(e.getInventory().getName().replace("Buy ✰✰✰ effect for ", "").replace("?", ""));
                assert cl != null;
                int price = cl.getStarEffPrice();
                if (aPlayer.getCoins() < price) {
                    player.closeInventory();
                    player.sendMessage(chat.color(
                            ServerLink.getFancyServerName() + "You don't have enough AC Coins."));
                    return;
                }
                aPlayer.takeCoins(price);
                Permissions.addPerm(player.getName(), ServerLink.getServerType() + "." + cl.getName() + "StarEffect");
                e.setCancelled(true);
                aPlayer.getData(cl.getName() + "ClassMenu").setValue(null);
                if (cl instanceof PowerClass)
                    ClassInventories.openPowerClassInventory(player, (PowerClass) cl);
                else
                    ClassInventories.openClassInventory(player, cl);
            } else if (currentType == redstoneBlock) {
                player.closeInventory();
            }
        }
    }
}
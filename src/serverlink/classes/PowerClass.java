package serverlink.classes;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.server.Messages;

import java.util.HashMap;
import java.util.Set;

public abstract class PowerClass extends Class {
    private int levelAt10Kills;
    private HashMap<ClassStat, Double> statIncreases_EachLvl = new HashMap<>();
    private HashMap<ClassStat, Double> statIncreases_EvenLvls = new HashMap<>();
    private HashMap<ClassStat, Double> statIncreases_OddLvls = new HashMap<>();
    private HashMap<ClassStat, Double> statCaps = new HashMap<>();
    private HashMap<Integer, ItemStack> helmetAtLvl = new HashMap<>();
    private HashMap<Integer, ItemStack> chestplateAtLvl = new HashMap<>();
    private HashMap<Integer, ItemStack> leggingsAtLvl = new HashMap<>();
    private HashMap<Integer, ItemStack> bootsAtLvl = new HashMap<>();

    public PowerClass(String name, String fancyName, String description,
                      int loreLineLength, int price, ItemStack menuItem,
                      Set<WeaponType> weaponTypes, ItemStack helmet,
                      ItemStack chestplate, ItemStack leggings, ItemStack boots,
                      double damageTaken, double knockbackTaken, double fistDamageDealt,
                      double percentFistKnockbackDealt, String heartEffectDesc,
                      String starEffectDesc, int heartEffPrice, int starEffPrice,
                      String heartEffLockReason, String starEffLockReason,
                      int levelAt10Kills) {
        super(name, fancyName, description, loreLineLength, price, menuItem,
                weaponTypes, helmet, chestplate, leggings, boots, damageTaken,
                knockbackTaken, fistDamageDealt, percentFistKnockbackDealt, heartEffectDesc,
                starEffectDesc, heartEffPrice, starEffPrice, heartEffLockReason,
                starEffLockReason);
        this.levelAt10Kills = levelAt10Kills;
    }

    public PowerClass(String name, String fancyName, String description,
                      int loreLineLength, int price, ItemStack menuItem,
                      Set<WeaponType> weaponTypes, ItemStack helmet,
                      ItemStack chestplate, ItemStack leggings, ItemStack boots,
                      double defense, double percentKnockbackTaken,
                      double fistDamageDealt, double percentFistKnockbackDealt,
                      int levelAt10Kills) {
        super(name, fancyName, description, loreLineLength, price, menuItem,
                weaponTypes, helmet, chestplate, leggings, boots, defense,
                percentKnockbackTaken, fistDamageDealt, percentFistKnockbackDealt);
        this.levelAt10Kills = levelAt10Kills;
    }

    public int calculateLevelFromExp(double exp) {
        // casting to int truncates the number, which is perfect for levels
        int level = (int) ((levelAt10Kills / 14.1421) * Math.sqrt(exp));
        if (level <= 0) return 1;
        return level >= 50 ? 50 : level;
    }

    public double getExpForLvl(int lvl) {
        return lvl == 1 ? 0 : Math.pow(((double) lvl) / (levelAt10Kills / 14.1421), 2);
    }

    public void setIncreaseStatOnEvenLvlup(ClassStat stat, double increaseBy) {
        statIncreases_EvenLvls.put(stat, increaseBy);
        if (!statCaps.containsKey(stat)) statCaps.put(stat, (increaseBy * 110));
    }

    public void setIncreaseStatOnOddLvlup(ClassStat stat, double increaseBy) {
        statIncreases_OddLvls.put(stat, increaseBy);
        if (!statCaps.containsKey(stat)) statCaps.put(stat, (increaseBy * 110));
    }

    public void setIncreaseStatOnEachLvlup(ClassStat stat, double increaseBy) {
        statIncreases_EachLvl.put(stat, increaseBy);
        if (!statCaps.containsKey(stat)) statCaps.put(stat, (increaseBy * 110));
    }

    public void setStatCap(ClassStat stat, double cap) {
        statCaps.put(stat, cap);
    }

    public void setHelmetAtLvl(ItemStack helmet, int lvl) {
        helmetAtLvl.put(lvl, helmet);
    }

    public void setChestplateAtLvl(ItemStack chestplate, int lvl) {
        chestplateAtLvl.put(lvl, chestplate);
    }

    public void setLeggingsAtLvl(ItemStack leggings, int lvl) {
        leggingsAtLvl.put(lvl, leggings);
    }

    public void setBootsAtLvl(ItemStack boots, int lvl) {
        bootsAtLvl.put(lvl, boots);
    }

    public double getStat(ClassStat stat, double baseValue, Player p) {
        if (getStatCache(PlayerManager.get(p)).containsKey(stat)) return getStatCache(PlayerManager.get(p)).get(stat);
        int lvl = getLevel(PlayerManager.get(p.getUniqueId()));
        double totalIncrease = baseValue;
        double evenIncrease = 0;
        double oddIncrease = 0;
        double eachIncrease = 0;
        if (statIncreases_EvenLvls.containsKey(stat)) evenIncrease = statIncreases_EvenLvls.get(stat);
        if (statIncreases_OddLvls.containsKey(stat)) oddIncrease = statIncreases_OddLvls.get(stat);
        if (statIncreases_EachLvl.containsKey(stat)) eachIncrease = statIncreases_EachLvl.get(stat);
        double cap = 9999;
        if (statCaps.containsKey(stat)) cap = statCaps.get(stat);
        for (int i = 2; i <= lvl; i++) {
            if (i % 2 == 0) {
                if (totalIncrease + evenIncrease <= cap) totalIncrease = totalIncrease + evenIncrease;
                else totalIncrease = cap;
            } else {
                if (totalIncrease + oddIncrease <= cap) totalIncrease = totalIncrease + oddIncrease;
                else totalIncrease = cap;
            }
            if (totalIncrease + eachIncrease <= cap) totalIncrease = totalIncrease + eachIncrease;
        }
        getStatCache(PlayerManager.get(p)).put(stat, totalIncrease);
        return totalIncrease;
    }

    @Override
    public ItemStack getHelmet(Player p) {
        int lvl = getLevel(PlayerManager.get(p.getUniqueId()));
        ItemStack helmet = super.getHelmet(p);
        for (int i = 1; i <= lvl; i++) {
            if (helmetAtLvl.get(i) != null) helmet = helmetAtLvl.get(i);
        }
        return helmet;
    }

    @Override
    public ItemStack getChestplate(Player p) {
        int lvl = getLevel(PlayerManager.get(p.getUniqueId()));
        ItemStack chestplate = super.getChestplate(p);
        for (int i = 1; i <= lvl; i++) {
            if (chestplateAtLvl.get(i) != null) chestplate = chestplateAtLvl.get(i);
        }
        return chestplate;
    }

    @Override
    public ItemStack getLeggings(Player p) {
        int lvl = getLevel(PlayerManager.get(p.getUniqueId()));
        ItemStack leggings = super.getLeggings(p);
        for (int i = 1; i <= lvl; i++) {
            if (leggingsAtLvl.get(i) != null) leggings = leggingsAtLvl.get(i);
        }
        return leggings;
    }

    @Override
    public ItemStack getBoots(Player p) {
        int lvl = getLevel(PlayerManager.get(p.getUniqueId()));
        ItemStack boots = super.getBoots(p);
        for (int i = 1; i <= lvl; i++) {
            if (bootsAtLvl.get(i) != null) boots = bootsAtLvl.get(i);
        }
        return boots;
    }

    @Override
    public double getDefense(Player p) {
        return getStat(ClassStat.DEFENSE, super.getDefense(p), p);
    }

    public double getDefenseBeforeStatIncrease(Player p) {
        return super.getDefense(p);
    }

    @Override
    public double getPercentKnockbackTaken(Player p) {
        return getStat(ClassStat.PERCENT_KNOCKBACK_TAKEN, super.getPercentKnockbackTaken(p), p);
    }

    public double getPercentKnockbackTakenBeforeStatIncrease(Player p) {
        return super.getPercentKnockbackTaken(p);
    }

    @Override
    public double getPercentFistKnockbackDealt(Player p) {
        return getStat(ClassStat.PERCENT_FIST_KNOCKBACK_DEALT, super.getPercentFistKnockbackDealt(p), p);
    }

    public double getPercentFistKnockbackDealtBeforeStatIncrease(Player p) {
        return super.getPercentFistKnockbackDealt(p);
    }

    @Override
    public double getFistDamageDealt(Player p) {
        return getStat(ClassStat.FIST_DAMAGE_DEALT, super.getFistDamageDealt(p), p);
    }

    public double getFistDamageDealtBeforeStatIncrease(Player p) {
        return super.getFistDamageDealt(p);
    }

    public void addExp(AlphaPlayer aPlayer, double expToAdd) {
        double newExp = expToAdd + (double) aPlayer.getData(ServerLink.getServerType() + ":classExp" + getName()).getValue();
        final int oldLvl = (int) aPlayer.getData(ServerLink.getServerType() + ":classLevel" + getName()).getValue();
        final int newLvl = calculateLevelFromExp(newExp);
        for (int i = oldLvl + 1; i <= newLvl; i++) levelUp(aPlayer);
        aPlayer.setSetting_SyncOnLogout(ServerLink.getServerType() + ":classExp" + getName(), String.valueOf(newExp));
        aPlayer.getData(ServerLink.getServerType() + ":classExp" + getName()).setValue(newExp);
        aPlayer.getData(ServerLink.getServerType() + "ClassesMenu").setValue(null);
    }

    public void levelUp(AlphaPlayer aPlayer) {
        aPlayer.getData("statcache" + getName()).setValue(new HashMap<>());
        aPlayer.getData(ServerLink.getServerType() + ":classLevel" + getName())
                .setValue(((int) aPlayer.getData(ServerLink.getServerType() + ":classLevel"
                        + getName()).getValue()) + 1);
        aPlayer.getPlayer().sendMessage(chat.color(chat.getServer()
                + Messages.powerClass_lvlUp.replace("%class%", getFancyName())));
    }

    public int getLevel(AlphaPlayer aPlayer) {
        return (int) aPlayer.getData(ServerLink.getServerType() + ":classLevel" + getName()).getValue();
    }

    abstract public ItemStack getStatsItem(Player p);

    public void addToStatCache(AlphaPlayer aPlayer) {
        aPlayer.getData("statcache" + getName()).setValue(new HashMap<>());
    }

    private HashMap<ClassStat, Double> getStatCache(AlphaPlayer aPlayer) {
        return (HashMap<ClassStat, Double>) aPlayer.getData("statcache" + getName()).getValue();
    }
}

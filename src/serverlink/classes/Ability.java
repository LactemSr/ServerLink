package serverlink.classes;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import java.util.HashMap;
import java.util.Set;

public abstract class Ability {
    private Set<ActivationMethod> activationMethods;
    private double cooldown;
    private HashMap<Player, Double> cooldownLeft;

    public Ability(Set<ActivationMethod> activationMethods, double cooldown) {
        this.activationMethods = activationMethods;
        this.cooldown = cooldown;
        cooldownLeft = new HashMap<>();
    }

    public abstract void onUse(Player player, LivingEntity enemy);

    public abstract void onCooldownEnd(Player player);

    public Set<ActivationMethod> getActivationMethods() {
        return activationMethods;
    }

    public double getCooldown() {
        return cooldown;
    }

    public double getCooldownLeft(Player p) {
        Double time = cooldownLeft.get(p);
        if (time == null) return 0;
        time = (time - System.currentTimeMillis()) / 1000.0;
        return time > 0.0 ? time : 0;
    }

    public void setCooldownLeft(Player p, double cooldownLeft) {
        this.cooldownLeft.put(p, System.currentTimeMillis() + (cooldownLeft * 1000));
        CooldownRunnable.add(p, this);
    }

    public void setCooldownLeft(HashMap<Player, Double> cooldownLeft) {
        this.cooldownLeft = cooldownLeft;
    }
}

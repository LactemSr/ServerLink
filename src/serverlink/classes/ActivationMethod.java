package serverlink.classes;

public enum ActivationMethod {
    LEFT_CLICK_BLOCK,
    LEFT_CLICK_AIR,
    LEFT_CLICK_ENEMY,
    RIGHT_CLICK_BLOCK,
    RIGHT_CLICK_AIR,
    RIGHT_CLICK_ENEMY,
    PASSIVE
}

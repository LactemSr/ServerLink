package serverlink.classes;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.inventory.ItemStack;

import java.util.*;

abstract public class Class {
    // this variable makes sure that calling equip() twice without calling unequip() in between
    // will not cause problems
    List<Player> alreadyEquipped = new ArrayList<>();
    HashMap<Player, Double> playerDefenses = new HashMap<>();
    HashMap<Player, Double> playerKbTaken = new HashMap<>();
    HashMap<Player, Double> playerFistDamageDealt = new HashMap<>();
    HashMap<Player, Double> playerFistKbDealt = new HashMap<>();
    String fancyName;
    private String name;
    private String description;
    private int loreLineLength;
    private int price;
    private ItemStack menuItem;
    private Set<WeaponType> weaponTypes = new HashSet<>();
    private ItemStack helmet;
    private ItemStack chestplate;
    private ItemStack leggings;
    private ItemStack boots;
    private double defense;
    private double percentKnockbackTaken;
    private double fistDamageDealt;
    private double percentFistKnockbackDealt;
    private String heartEffectDesc;
    private String starEffectDesc;
    private int heartEffPrice;
    private int starEffPrice;
    private String heartEffLockReason;
    private String starEffLockReason;

    public Class(String name, String fancyName, String description, int loreLineLength,
                 int price, ItemStack menuItem) {
        this.name = name.toLowerCase().trim().replaceAll("-", "").replaceAll("_", "");
        this.fancyName = fancyName;
        this.description = description;
        this.loreLineLength = loreLineLength;
        this.price = price;
        this.menuItem = menuItem;
        this.weaponTypes = new HashSet<>();
        this.helmet = null;
        this.chestplate = null;
        this.leggings = null;
        this.boots = null;
        this.defense = 0;
        this.percentKnockbackTaken = 100;
        this.fistDamageDealt = 1;
        this.percentFistKnockbackDealt = 100;
        this.heartEffectDesc = null;
        this.starEffectDesc = null;
        this.heartEffPrice = 0;
        this.starEffPrice = 0;
        this.heartEffLockReason = null;
        this.starEffLockReason = null;
        if (!name.equals("default")) ClassManager.addClass(this);
    }

    public Class(String name, String fancyName, String description, int loreLineLength,
                 int price, ItemStack menuItem, Set<WeaponType> weaponTypes,
                 ItemStack helmet, ItemStack chestplate, ItemStack leggings,
                 ItemStack boots, double defense, double percentKnockbackTaken,
                 double fistDamageDealt, double percentFistKnockbackDealt) {
        this.name = name.toLowerCase().trim().replaceAll("-", "").replaceAll("_", "");
        this.fancyName = fancyName;
        this.description = description;
        this.loreLineLength = loreLineLength;
        this.price = price;
        this.menuItem = menuItem;
        this.weaponTypes = weaponTypes;
        this.helmet = helmet;
        this.chestplate = chestplate;
        this.leggings = leggings;
        this.boots = boots;
        this.defense = defense;
        this.percentKnockbackTaken = percentKnockbackTaken;
        this.fistDamageDealt = fistDamageDealt;
        this.percentFistKnockbackDealt = percentFistKnockbackDealt;
        this.heartEffectDesc = null;
        this.starEffectDesc = null;
        this.heartEffPrice = 0;
        this.starEffPrice = 0;
        this.heartEffLockReason = null;
        this.starEffLockReason = null;
        if (!name.equals("default")) ClassManager.addClass(this);
    }

    public Class(String name, String fancyName, String description, int loreLineLength,
                 int price, ItemStack menuItem, Set<WeaponType> weaponTypes,
                 ItemStack helmet, ItemStack chestplate, ItemStack leggings,
                 ItemStack boots, double defense, double percentKnockbackTaken,
                 double fistDamageDealt, double percentFistKnockbackDealt,
                 String heartEffectDesc, String starEffectDesc, int heartEffPrice,
                 int starEffPrice, String heartEffLockReason, String starEffLockReason) {
        this.name = name.toLowerCase().trim().replaceAll("-", "").replaceAll("_", "");
        this.fancyName = fancyName;
        this.description = description;
        this.loreLineLength = loreLineLength;
        this.price = price;
        this.menuItem = menuItem;
        this.weaponTypes = weaponTypes;
        this.helmet = helmet;
        this.chestplate = chestplate;
        this.leggings = leggings;
        this.boots = boots;
        this.defense = defense;
        this.percentKnockbackTaken = percentKnockbackTaken;
        this.fistDamageDealt = fistDamageDealt;
        this.percentFistKnockbackDealt = percentFistKnockbackDealt;
        this.heartEffectDesc = heartEffectDesc;
        this.starEffectDesc = starEffectDesc;
        this.heartEffPrice = heartEffPrice;
        this.starEffPrice = starEffPrice;
        this.heartEffLockReason = heartEffLockReason;
        this.starEffLockReason = starEffLockReason;
        if (!name.equals("default")) ClassManager.addClass(this);
    }

    public int getHeartEffPrice() {
        return heartEffPrice;
    }

    public int getStarEffPrice() {
        return starEffPrice;
    }

    public String getHeartEffLockReason() {
        return heartEffLockReason;
    }

    public String getStarEffLockReason() {
        return starEffLockReason;
    }

    abstract public void onEquip(Player player);

    abstract public void onUnequip(Player player);

    abstract public void onKill(Player player, LivingEntity victim, Weapon weaponUsed);

    /**
     * Example of use: Make players of a class have "poison resistance" by returning
     * half of @damage if @cause is POISON.
     * Another Example: Make players immune to thorns damage by returning 0 if @cause
     * is THORNS.
     * This is the not final damage that will be applied to the player; Armor defense
     * is factored in after, using this damage value.
     */
    abstract public double modifyDamageFromCause(double damage, EntityDamageEvent.DamageCause cause);

    public String getHeartEffectDesc() {
        return heartEffectDesc;
    }

    public String getStarEffectDesc() {
        return starEffectDesc;
    }

    public int getLoreLineLength() {
        return loreLineLength;
    }

    public double getBaseDefense() {
        return defense;
    }

    public double getDefense(Player p) {
        return defense;
    }

    public void setDefense(Player p, double newDefense) {
        playerDefenses.put(p, newDefense);
    }

    public Set<WeaponType> getWeaponTypes() {
        return weaponTypes;
    }

    public ItemStack getHelmet(Player p) {
        return helmet;
    }

    public ItemStack getChestplate(Player p) {
        return chestplate;
    }

    public ItemStack getLeggings(Player p) {
        return leggings;
    }

    public ItemStack getBoots(Player p) {
        return boots;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public ItemStack getMenuItem() {
        return menuItem;
    }

    public String getDescription() {
        return description;
    }

    public String getFancyName() {
        return fancyName;
    }

    public double getBasePercentKnockbackTaken() {
        return percentKnockbackTaken;
    }

    public double getPercentKnockbackTaken(Player p) {
        return percentKnockbackTaken;
    }

    public void setPercentKnockbackTaken(Player p, double newPercentKbTaken) {
        percentKnockbackTaken = newPercentKbTaken;
    }

    public double getBasePercentFistKnockbackDealt() {
        return percentFistKnockbackDealt;
    }

    public double getPercentFistKnockbackDealt(Player p) {
        return percentFistKnockbackDealt;
    }

    public void setPercentFistKnockbackDealt(Player p, double newFistKbDealt) {
        percentFistKnockbackDealt = newFistKbDealt;
    }

    public double getBaseFistDamageDealt() {
        return fistDamageDealt;
    }

    public double getFistDamageDealt(Player p) {
        return fistDamageDealt;
    }

    public void setFistDamageDealt(Player p, double newFistDamageDealt) {
        fistDamageDealt = newFistDamageDealt;
    }
}

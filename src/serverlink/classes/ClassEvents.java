package serverlink.classes;

import net.citizensnpcs.api.event.NPCDamageByEntityEvent;
import org.bukkit.Material;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.ProjectileHitEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerVelocityEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.network.nstatus;
import serverlink.player.team.TeamManager;
import serverlink.round.Round;
import serverlink.util.Item;

import java.util.HashMap;

public class ClassEvents implements Listener, Round {
    static HashMap<Projectile, Weapon> projectileLaunchers = new HashMap<>();
    static HashMap<Player, Double> nextVelMult = new HashMap<>();

    @EventHandler
    private void onInteract(PlayerInteractEvent e) {
        if (!nstatus.isGame()) return;
        // Bukkit cancels left click air and right click air by default, so we use this instead of e.isCancelled()
        if (e.useItemInHand() == org.bukkit.event.Event.Result.DENY) return;
        Player player = e.getPlayer();
        ActivationMethod method = ActivationMethod.LEFT_CLICK_AIR;
        if (e.getAction() == Action.LEFT_CLICK_AIR) method = ActivationMethod.LEFT_CLICK_AIR;
        else if (e.getAction() == Action.RIGHT_CLICK_AIR) method = ActivationMethod.RIGHT_CLICK_AIR;
        else if (e.getAction() == Action.LEFT_CLICK_BLOCK) method = ActivationMethod.LEFT_CLICK_BLOCK;
        else if (e.getAction() == Action.RIGHT_CLICK_BLOCK) method = ActivationMethod.RIGHT_CLICK_BLOCK;
        for (Weapon weapon : PlayerManager.get(player).getWeapons()) {
            if (!Item.isMostlySimilar(player.getItemInHand(), weapon.getItem())) continue;
            for (Ability ability : weapon.getType().getAbilities()) {
                if (ability.getCooldownLeft(player) != 0) continue;
                if (ability.getActivationMethods().contains(method)) {
                    ability.onUse(player, null);
                    ability.setCooldownLeft(player, ability.getCooldown());
                }
            }
            if (weapon.getItem().getType() != Material.BOW && weapon.getItem().getType() != Material.SNOW_BALL)
                e.setCancelled(true);
            return;
        }
    }

    @EventHandler
    private void onInteractEntity(PlayerInteractEntityEvent e) {
        if (!(e.getRightClicked() instanceof LivingEntity)) return;
        if (!nstatus.isGame()) return;
        if (e.isCancelled()) return;
        Player player = e.getPlayer();
        for (Weapon weapon : PlayerManager.get(player).getWeapons()) {
            if (!Item.isMostlySimilar(player.getItemInHand(), weapon.getItem())) continue;
            for (Ability ability : weapon.getType().getAbilities()) {
                if (ability.getCooldownLeft(player) != 0) continue;
                if (ability.getActivationMethods().contains(ActivationMethod.RIGHT_CLICK_ENEMY)) {
                    ability.onUse(player, (LivingEntity) e.getRightClicked());
                    ability.setCooldownLeft(player, ability.getCooldown());
                }
            }
            e.setCancelled(true);
            return;
        }
    }

    // No vanilla damage is done if an ability is used
    @EventHandler
    private void onHitEnemy(EntityDamageByEntityEvent e) {
        if (!nstatus.isGame()) return;
        if (e.isCancelled()) return;
        if (!(e.getDamager() instanceof Player)) return;
        if (!(e.getEntity() instanceof LivingEntity)) return;
        Player player = (Player) e.getDamager();
        if (e.getEntity() instanceof Player) {
            if (TeamManager.getTeam(player) != null &&
                    TeamManager.getTeam(player) == TeamManager.getTeam((Player) e.getEntity())) {
                e.setCancelled(true);
                return;
            }
        }
        if (e.getCause() == EntityDamageEvent.DamageCause.THORNS) {
            if (e.getEntity() instanceof Player) {
                if (!ClassManager.dealDamage_pvp((Player) e.getEntity(), player, e.getDamage(), null, e.getCause(), true, false))
                    e.setCancelled(true);
            } else {
                e.setCancelled(true);
                ClassManager.dealDamage_pve((LivingEntity) e.getEntity(), player, e.getDamage(), null, e.getCause(), false);
            }
            e.setDamage(0);
            return;
        }
        for (Weapon weapon : PlayerManager.get(player).getWeapons()) {
            if (!Item.isMostlySimilar(player.getItemInHand(), weapon.getItem())) continue;
            for (Ability ability : weapon.getType().getAbilities()) {
                if (ability.getActivationMethods().contains(ActivationMethod.LEFT_CLICK_ENEMY)) {
                    e.setCancelled(true);
                    if (ability.getCooldownLeft(player) != 0) return;
                    ability.onUse(player, (LivingEntity) e.getEntity());
                    ability.setCooldownLeft(player, ability.getCooldown());
                    return;
                }
            }
            // the following code runs if left click attacking something/someone with a weapon with no
            // left click ability
            Class cl = (Class) PlayerManager.get(player).getData("class").getValue();
            if (cl instanceof PowerClass && ClassStat.customStats.containsKey(cl.getName() + "weaponDamage" + weapon.getType().getItemSlot())) {
                if (e.getEntity() instanceof Player) {
                    if (!ClassManager.dealDamage_pvp((Player) e.getEntity(), player,
                            ((PowerClass) cl).getStat(ClassStat.weaponDamage(cl, weapon.getType().getItemSlot()),
                                    weapon.getDamageDealt(),
                                    player),
                            weapon, e.getCause(), true, false))
                        e.setCancelled(true);
                } else {
                    e.setCancelled(true);
                    ClassManager.dealDamage_pve((LivingEntity) e.getEntity(), player,
                            ((PowerClass) cl).getStat(ClassStat.weaponDamage(cl, weapon.getType().getItemSlot()),
                                    weapon.usingVanillaDamage() ? e.getDamage() : weapon.getDamageDealt(),
                                    player),
                            weapon, e.getCause(), false);
                }
            } else {
                if (e.getEntity() instanceof Player) {
                    if (!ClassManager.dealDamage_pvp((Player) e.getEntity(), player,
                            weapon.usingVanillaDamage() ? e.getDamage() : weapon.getDamageDealt(), weapon, e.getCause(), true, false))
                        e.setCancelled(true);
                } else {
                    e.setCancelled(true);
                    ClassManager.dealDamage_pve((LivingEntity) e.getEntity(), player,
                            weapon.usingVanillaDamage() ? e.getDamage() : weapon.getDamageDealt(), weapon, e.getCause(), false);
                }
            }
            e.setDamage(0);
            return;
        }
        // Player hit with his fist or an item that is not a registered weapon
        AlphaPlayer aPlayer = PlayerManager.get(player.getUniqueId());
        Class cl = (Class) aPlayer.getData("class").getValue();
        if (e.getEntity() instanceof Player) {
            if (!ClassManager.dealDamage_pvp((Player) e.getEntity(), player,
                    (cl instanceof ClassManager.BlankClass || (player.getItemInHand() != null && player.getItemInHand().getType() != Material.AIR)) ? e.getDamage() : cl.getFistDamageDealt(player),
                    null, e.getCause(), true, false))
                e.setCancelled(true);
        } else {
            e.setCancelled(true);
            ClassManager.dealDamage_pve((LivingEntity) e.getEntity(), player,
                    (cl instanceof ClassManager.BlankClass || (player.getItemInHand() != null && player.getItemInHand().getType() != Material.AIR)) ? e.getDamage() : cl.getFistDamageDealt(player),
                    null, e.getCause(), false);
        }
        e.setDamage(0);
    }

    // no vanilla damage is done if an ability is used
    @EventHandler(priority = EventPriority.LOWEST)
    private void onHitEnemy_bot(NPCDamageByEntityEvent e) {
        if (!nstatus.isGame()) return;
        if (!(e.getDamager() instanceof Player)) return;
        if (e.isCancelled()) return;
        Player player = (Player) e.getDamager();
        Player victim = (Player) e.getNPC().getEntity();
        if (PlayerManager.get(victim) == null || !PlayerManager.get(victim).isFullyLoggedIn()) {
            e.setCancelled(true);
            return;
        }
        if (TeamManager.getTeam(player) != null &&
                TeamManager.getTeam(player) == TeamManager.getTeam(victim)) {
            e.setCancelled(true);
            return;
        }
        if (e.getCause() == EntityDamageEvent.DamageCause.THORNS) {
            if (!ClassManager.dealDamage_pvp(victim, player, e.getDamage(), null, e.getCause(), true, false))
                e.setCancelled(true);
            e.setDamage(0);
            return;
        }
        for (Weapon weapon : PlayerManager.get(player).getWeapons()) {
            if (!Item.isMostlySimilar(player.getItemInHand(), weapon.getItem())) continue;
            for (Ability ability : weapon.getType().getAbilities()) {
                if (ability.getActivationMethods().contains(ActivationMethod.LEFT_CLICK_ENEMY)) {
                    e.setCancelled(true);
                    if (ability.getCooldownLeft(player) != 0) return;
                    ability.onUse(player, victim);
                    ability.setCooldownLeft(player, ability.getCooldown());
                    return;
                }
            }
            // the following code runs if left click attacking something/someone with a weapon with no
            // left click ability
            Class cl = (Class) PlayerManager.get(player).getData("class").getValue();
            if (cl instanceof PowerClass && ClassStat.customStats.containsKey(cl.getName() + "weaponDamage" + weapon.getType().getItemSlot())) {
                if (!ClassManager.dealDamage_pvp(victim, player,
                        ((PowerClass) cl).getStat(ClassStat.weaponDamage(cl, weapon.getType().getItemSlot()),
                                weapon.usingVanillaDamage() ? e.getDamage() : weapon.getDamageDealt(),
                                player),
                        weapon, e.getCause(), true, false))
                    e.setCancelled(true);
            } else {
                if (!ClassManager.dealDamage_pvp(victim, player,
                        weapon.usingVanillaDamage() ? e.getDamage() : weapon.getDamageDealt(), weapon, e.getCause(), true, false))
                    e.setCancelled(true);
            }
            e.setDamage(0);
            return;
        }
        // Player hit with his fist or an item that is not a registered weapon
        AlphaPlayer aPlayer = PlayerManager.get(player.getUniqueId());
        Class cl = (Class) aPlayer.getData("class").getValue();
        if (!ClassManager.dealDamage_pvp(victim, player,
                (cl instanceof ClassManager.BlankClass || (player.getItemInHand() != null && player.getItemInHand().getType() != Material.AIR)) ? e.getDamage() : cl.getFistDamageDealt(player),
                null, e.getCause(), true, false))
            e.setCancelled(true);
        e.setDamage(0);
    }

    /**
     * to change knockback dealt, set the weapon's percentKnockback
     * on projectileHit() and then change it back 2 ticks later
     * <p>
     * to change damage dealt, set the weapon's percentProjectileDamage
     * on projectileHit() and then change it back 2 ticks later
     * <p>
     * to cancel damage, call cancelNextProjectileDamage() on projectileHit()
     */
    @EventHandler
    private void onShootEnemy(EntityDamageByEntityEvent e) {
        if (!(e.getEntity() instanceof LivingEntity)) return;
        if (!nstatus.isGame()) return;
        if (e.isCancelled()) return;
        if (!(e.getDamager() instanceof Projectile)) return;
        if (!(((Projectile) e.getDamager()).getShooter() instanceof Player)) return;
        Player player = (Player) ((Projectile) e.getDamager()).getShooter();
        if (e.getEntity() instanceof Player && TeamManager.getTeam(player) != null &&
                TeamManager.getTeam(player) == TeamManager.getTeam((Player) e.getEntity())) {
            e.setCancelled(true);
            return;
        }
        Weapon weapon = projectileLaunchers.get(e.getDamager());
        if (!player.isOnline()) {
            projectileLaunchers.remove(e.getDamager());
            e.setCancelled(true);
            return;
        }
        if (weapon == null) {
            if (e.getEntity() instanceof Player) {
                if (!ClassManager.dealDamage_pvp((Player) e.getEntity(), player,
                        e.getDamage(), null, e.getCause(), true, false))
                    e.setCancelled(true);
            } else {
                e.setCancelled(true);
                ClassManager.dealDamage_pve((LivingEntity) e.getEntity(), player,
                        e.getDamage(), null, e.getCause(), false);
            }
            return;
        }
        weapon.getType().onProjectileHit(player, (LivingEntity) e.getEntity(), weapon);
        projectileLaunchers.remove(e.getDamager());
        if (weapon.getType().isNextProjectileDamageCancelled(weapon.getOwner())) {
            weapon.getType().removeNextProjectileDamagePlayer(weapon.getOwner());
            return;
        }
        Class cl = ClassManager.getClassFromWeaponType(weapon.getType());
        if (cl == null) {
            if (e.getEntity() instanceof Player) {
                if (!ClassManager.dealDamage_pvp((Player) e.getEntity(), player,
                        e.getDamage(), null, e.getCause(), true, false))
                    e.setCancelled(true);
            } else {
                e.setCancelled(true);
                ClassManager.dealDamage_pve((LivingEntity) e.getEntity(), player,
                        e.getDamage(), null, e.getCause(), false);
            }
            return;
        }
        if (cl instanceof PowerClass && ClassStat.customStats.containsKey(cl.getName() + "weaponProjDamage" + weapon.getType().getItemSlot())) {
            if (e.getEntity() instanceof Player) {
                if (!ClassManager.dealDamage_pvp((Player) e.getEntity(), player,
                        ((PowerClass) cl).getStat(ClassStat.weaponProjDamage(cl, weapon.getType().getItemSlot()),
                                weapon.getProjectileDamageDealt(),
                                player),
                        weapon, EntityDamageEvent.DamageCause.PROJECTILE, true, false))
                    e.setCancelled(true);
            } else {
                e.setCancelled(true);
                ClassManager.dealDamage_pve((LivingEntity) e.getEntity(), player,
                        ((PowerClass) cl).getStat(ClassStat.weaponProjDamage(cl, weapon.getType().getItemSlot()),
                                weapon.getProjectileDamageDealt(),
                                player),
                        weapon, EntityDamageEvent.DamageCause.PROJECTILE, false);
            }
        } else {
            if (e.getEntity() instanceof Player) {
                if (!ClassManager.dealDamage_pvp((Player) e.getEntity(), player,
                        weapon.getProjectileDamageDealt(), weapon,
                        EntityDamageEvent.DamageCause.PROJECTILE, true, false))
                    e.setCancelled(true);
            } else {
                e.setCancelled(true);
                ClassManager.dealDamage_pve((LivingEntity) e.getEntity(), player,
                        weapon.getProjectileDamageDealt(), weapon,
                        EntityDamageEvent.DamageCause.PROJECTILE, false);
            }
        }
        e.setDamage(0);
        e.getDamager().remove();
    }

    @EventHandler
    private void onProjectileLand(ProjectileHitEvent e) {
        if (!nstatus.isGame()) return;
        Projectile proj = e.getEntity();
        // put it on a delay so that we only call onLand OR onHit (method above this one), but not both
        new BukkitRunnable() {
            @Override
            public void run() {
                Weapon weapon = projectileLaunchers.get(proj);
                if (weapon == null) return;
                if (weapon.getOwner() != null && weapon.getOwner().isOnline())
                    weapon.getType().onProjectileLand(weapon.getOwner(), proj, weapon);
                projectileLaunchers.remove(proj);
                proj.remove();
            }
        }.runTaskLater(ServerLink.plugin, 2);
    }

    // adds projectile to hashmap so we know what weapon they were shot with
    @EventHandler
    private void tagProjectiles(ProjectileLaunchEvent e) {
        if (e.isCancelled()) return;
        if (!(e.getEntity().getShooter() instanceof Player)) return;
        Player p = (Player) e.getEntity().getShooter();
        ItemStack launcher = p.getItemInHand();
        if (launcher == null) return;
        Projectile proj = e.getEntity();
        for (Weapon weapon : PlayerManager.get(p).getWeapons()) {
            if (!(launcher.hasItemMeta() && weapon.getItem().hasItemMeta() &&
                    launcher.getItemMeta().getDisplayName() != null &&
                    launcher.getItemMeta().getDisplayName().equals(weapon.getItem()
                            .getItemMeta().getDisplayName())))
                continue;
            projectileLaunchers.put(proj, weapon);
            weapon.getType().onProjectileLaunch(p, weapon, proj);
            return;
        }
    }

    @EventHandler(priority = EventPriority.LOW)
    private void adjustKB(PlayerVelocityEvent e) {
        if (!nextVelMult.containsKey(e.getPlayer())) return;
        if (nextVelMult.get(e.getPlayer()) == 0) e.setCancelled(true);
        else e.setVelocity(e.getVelocity().multiply(nextVelMult.get(e.getPlayer()) / 100.0));
        nextVelMult.remove(e.getPlayer());
    }

    @Override
    public void onLobbyCountdownStart() {
    }

    @Override
    public void onLobbyCountdownCancel() {
    }

    @Override
    public void onMapCountdownStart() {
        PlayerManager.getOnlineAlphaPlayers().forEach(AlphaPlayer::updateRankTag);
    }

    @Override
    public void onMapCountdownCancel() {

    }

    @Override
    public void onGameStart() {
        // this first for loop accounts for a game that allows players to change
        // classes mid-game, by ensuring the hashmaps or currently unused classes
        // won't be empty when they are to be used
        for (Class cl : ClassManager.getClasses()) {
            for (WeaponType weaponType : cl.getWeaponTypes()) {
                for (Ability ability : weaponType.getAbilities()) {
                    ability.setCooldownLeft(new HashMap<>());
                }
            }
        }
        for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
            if (!aPlayer.isFullyLoggedIn()) continue;
            Class cl = (Class) aPlayer.getData("class").getValue();
            for (WeaponType weaponType : cl.getWeaponTypes()) {
                for (Ability ability : weaponType.getAbilities()) {
                    ability.setCooldownLeft(aPlayer.getPlayer(), ability.getCooldown());
                }
            }
        }
    }

    @Override
    public void onEndCountdownStart() {
    }

    @Override
    public void onRoundEnd() {
        PlayerManager.getOnlineAlphaPlayers().forEach(AlphaPlayer::updateRankTag);
    }
}


package serverlink.classes;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class Weapon {
    private ItemStack item;
    private WeaponType type;
    private Player owner;
    private double percentKnockback;
    private double percentProjKnockback;
    // health (1-20)
    private double damageDealt;
    // health (1-20)
    private Double projectileDamageDealt;
    private boolean useVanillaDamage = false;

    public Weapon(WeaponType type, Player owner) {
        this.type = type;
        // type.getItem() is already a clone, so there's no need to call .clone()
        item = type.getItem();
        percentKnockback = type.getPercentKnockback();
        percentProjKnockback = type.getPercentProjKnockback();
        damageDealt = type.getDamageDealt();
        projectileDamageDealt = type.getProjectileDamageDealt();
        this.owner = owner;
    }

    public double getPercentProjKnockback() {
        return percentProjKnockback;
    }

    public void setPercentProjKnockback(double percentProjKnockback) {
        this.percentProjKnockback = percentProjKnockback;
    }

    public Double getProjectileDamageDealt() {
        return projectileDamageDealt;
    }

    public void setProjectileDamageDealt(Double projectileDamageDealt) {
        this.projectileDamageDealt = projectileDamageDealt;
    }

    public double getPercentKnockback() {
        return percentKnockback;
    }

    public void setPercentKnockback(double percentKnockback) {
        this.percentKnockback = percentKnockback;
    }

    public double getDamageDealt() {
        return damageDealt;
    }

    public void setDamageDealt(double damageDealt) {
        this.damageDealt = damageDealt;
    }

    public ItemStack getItem() {
        return item;
    }

    public WeaponType getType() {
        return type;
    }

    public Player getOwner() {
        return owner;
    }

    public boolean usingVanillaDamage() {
        return useVanillaDamage;
    }

    public void useVanillaDamage() {
        useVanillaDamage = true;
    }

    public void useDamageDealt() {
        useVanillaDamage = false;
    }
}

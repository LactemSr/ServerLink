package serverlink.classes;

import java.util.HashMap;

public enum ClassStat {
    DEFENSE,
    PERCENT_KNOCKBACK_TAKEN,
    PERCENT_FIST_KNOCKBACK_DEALT,
    FIST_DAMAGE_DEALT,
    CUSTOM_1(),
    CUSTOM_2(),
    CUSTOM_3(),
    CUSTOM_4(),
    CUSTOM_5(),
    CUSTOM_6(),
    CUSTOM_7(),
    CUSTOM_8(),
    CUSTOM_9(),
    CUSTOM_10(),
    CUSTOM_11(),
    CUSTOM_12(),
    CUSTOM_13(),
    CUSTOM_14(),
    CUSTOM_15(),
    CUSTOM_16(),
    CUSTOM_17(),
    CUSTOM_18(),
    CUSTOM_19(),
    CUSTOM_20(),
    CUSTOM_21(),
    CUSTOM_22(),
    CUSTOM_23(),
    CUSTOM_24(),
    CUSTOM_25(),
    CUSTOM_26(),
    CUSTOM_27(),
    CUSTOM_28(),
    CUSTOM_29(),
    CUSTOM_30(),
    CUSTOM_31(),
    CUSTOM_32(),
    CUSTOM_33(),
    CUSTOM_34(),
    CUSTOM_35(),
    CUSTOM_36(),
    CUSTOM_37(),
    CUSTOM_38(),
    CUSTOM_39(),
    CUSTOM_40(),
    CUSTOM_41(),
    CUSTOM_42(),
    CUSTOM_43(),
    CUSTOM_44(),
    CUSTOM_45(),
    CUSTOM_46(),
    CUSTOM_47(),
    CUSTOM_48(),
    CUSTOM_49(),
    CUSTOM_50(),
    CUSTOM_51(),
    CUSTOM_52(),
    CUSTOM_53(),
    CUSTOM_54(),
    CUSTOM_55(),
    CUSTOM_56(),
    CUSTOM_57(),
    CUSTOM_58(),
    CUSTOM_59(),
    CUSTOM_60(),
    CUSTOM_61(),
    CUSTOM_62(),
    CUSTOM_63(),
    CUSTOM_64(),
    CUSTOM_65(),
    CUSTOM_66(),
    CUSTOM_67(),
    CUSTOM_68(),
    CUSTOM_69(),
    CUSTOM_70(),
    CUSTOM_71(),
    CUSTOM_72(),
    CUSTOM_73(),
    CUSTOM_74(),
    CUSTOM_75(),
    CUSTOM_76(),
    CUSTOM_77(),
    CUSTOM_78(),
    CUSTOM_79(),
    CUSTOM_80(),
    CUSTOM_81(),
    CUSTOM_82(),
    CUSTOM_83(),
    CUSTOM_84(),
    CUSTOM_85(),
    CUSTOM_86(),
    CUSTOM_87(),
    CUSTOM_88(),
    CUSTOM_89(),
    CUSTOM_90(),
    CUSTOM_91(),
    CUSTOM_92(),
    CUSTOM_93(),
    CUSTOM_94(),
    CUSTOM_95(),
    CUSTOM_96(),
    CUSTOM_97(),
    CUSTOM_98(),
    CUSTOM_99();
    static final HashMap<String, ClassStat> customStats = new HashMap<>();

    /**
     * Use for special ability stats (unique implementations)
     */
    public static ClassStat custom(Class cl, String statName) {
        String customId = cl.getName() + statName.trim().toLowerCase().replaceAll("-", "").replaceAll("_", "");
        if (customStats.containsKey(customId)) return customStats.get(customId);
        Integer nextCustomStat = customStats.size() + 1;
        ClassStat stat = ClassStat.valueOf("CUSTOM_" + nextCustomStat.toString());
        customStats.put(customId, stat);
        return stat;
    }

    /**
     * Don't waste time making custom ClassStats and applying them for
     * every class with upgradable weapon damage. Use this instead.
     */
    public static ClassStat weaponDamage(Class cl, int defaultWeaponSlot) {
        String customId = cl.getName() + "weaponDamage" + defaultWeaponSlot;
        if (customStats.containsKey(customId)) return customStats.get(customId);
        Integer nextCustomStat = customStats.size() + 1;
        ClassStat stat = ClassStat.valueOf("CUSTOM_" + nextCustomStat.toString());
        customStats.put(customId, stat);
        return stat;
    }

    /**
     * Don't waste time making custom ClassStats and applying them for
     * every class with upgradable weapon knockback. Use this instead.
     */
    public static ClassStat weaponKnockback(Class cl, int defaultWeaponSlot) {
        String customId = cl.getName() + "weaponKnockack" + defaultWeaponSlot;
        if (customStats.containsKey(customId)) return customStats.get(customId);
        Integer nextCustomStat = customStats.size() + 1;
        ClassStat stat = ClassStat.valueOf("CUSTOM_" + nextCustomStat.toString());
        customStats.put(customId, stat);
        return stat;
    }

    public static ClassStat weaponProjDamage(Class cl, int defaultWeaponSlot) {
        String customId = cl.getName() + "weaponProjDamage" + defaultWeaponSlot;
        if (customStats.containsKey(customId)) return customStats.get(customId);
        Integer nextCustomStat = customStats.size() + 1;
        ClassStat stat = ClassStat.valueOf("CUSTOM_" + nextCustomStat.toString());
        customStats.put(customId, stat);
        return stat;
    }

    /**
     * Don't waste time making custom ClassStats and applying them for
     * every class with projectiles. Use this instead.
     */
    public static ClassStat weaponProjKnockback(Class cl, int defaultWeaponSlot) {
        String customId = cl.getName() + "weaponProjKnockback" + defaultWeaponSlot;
        if (customStats.containsKey(customId)) return customStats.get(customId);
        Integer nextCustomStat = customStats.size() + 1;
        ClassStat stat = ClassStat.valueOf("CUSTOM_" + nextCustomStat.toString());
        customStats.put(customId, stat);
        return stat;
    }
}

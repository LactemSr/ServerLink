package serverlink.classes;

import serverlink.ServerLink;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CooldownRunnable {
    private static HashMap<Ability, List<Player>> coolingDown = new HashMap<>();

    public static void setup() {
        new BukkitRunnable() {
            @Override
            public void run() {
                for (Ability ability : new HashMap<>(coolingDown).keySet()) {
                    for (Player player : new ArrayList<>(coolingDown.get(ability))) {
                        if (ability.getCooldownLeft(player) > 0) return;
                        ability.onCooldownEnd(player);
                        coolingDown.get(ability).remove(player);
                        if (coolingDown.get(ability).isEmpty()) coolingDown.remove(ability);
                    }
                }
            }
        }.runTaskTimer(ServerLink.plugin, 1, 1);
    }

    public static void add(Player player, Ability ability) {
        if (coolingDown.containsKey(ability)) {
            if (!coolingDown.get(ability).contains(player)) coolingDown.get(ability).add(player);
            return;
        }
        coolingDown.put(ability, new ArrayList<Player>() {{
            add(player);
        }});
    }
}

package serverlink.classes;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.customevent.CustomChangeClassEvent;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.inventory.InventoryManager;
import serverlink.server.ServerType;
import serverlink.util.CachedDecimalFormat;
import serverlink.util.Item;
import serverlink.util.LoreUtil;

import java.util.ArrayList;
import java.util.List;

public class ClassInventories {
    public static InventoryGui createClassesMenu(Player player) {
        AlphaPlayer aPlayer = PlayerManager.get(player);
        ArrayList<ItemStack> items = new ArrayList<>();
        for (Class cl : ClassManager.getClasses()) {
            if (cl instanceof ClassManager.BlankClass) {
                items.add(cl.getMenuItem());
                continue;
            }
            ItemStack menuItem = cl.getMenuItem();
            String line1;
            if (cl == aPlayer.getData("class").getValue()) {
                menuItem = Item.addGlow(menuItem);
                line1 = "&d&lEquipped";
            } else if (cl == ClassManager.getDefaultClass())
                line1 = "&aUnlocked";
            else if (player.hasPermission(ServerLink.getServerType() + "." + cl.getName()))
                line1 = "&aUnlocked";
            else if (cl.getPrice() > 0)
                line1 = "&c&lCosts &r&e" + cl.getPrice() + "&c&l " + ServerType.getThisCurrency() + " to Unlock";
            else
                line1 = "&c&lClass Not Unlocked";
            ItemMeta meta = menuItem.getItemMeta();
            meta.setDisplayName(chat.color(cl.getFancyName()));
            List<String> lines = new ArrayList<>();
            lines.add(chat.color(line1));
            lines.add("   ");
            lines.addAll(LoreUtil.wrap(cl.getDescription(), cl.getLoreLineLength()));
            String heartEff = cl.getHeartEffectDesc();
            String starEff = cl.getStarEffectDesc();
            if ((heartEff != null && !heartEff.isEmpty()) ||
                    (starEff != null && !starEff.isEmpty())) {
                lines.add("      ");
                // heart effect
                if (heartEff != null && !heartEff.isEmpty()) {
                    String text;
                    if (player.hasPermission(ServerLink.getServerType() + "." +
                            cl.getName() + "HeartEffect"))
                        text = "&b❤❤❤ Effect&r &7(&aunlocked&7)&f: " + heartEff;
                    else text = "&b❤❤❤ Effect&r &7(&clocked&7)&f: " + heartEff;
                    lines.addAll(LoreUtil.wrap(text, cl.getLoreLineLength()));
                } else {
                    lines.add(chat.color("&b❤❤❤ Effect&r&f: &7none"));
                }
                // star effect
                if (starEff != null && !starEff.isEmpty()) {
                    String text;
                    if (player.hasPermission(ServerLink.getServerType() + "." +
                            cl.getName() + "StarEffect"))
                        text = "&b✰✰✰ Effect&r &7(&aunlocked&7)&f: " + starEff;
                    else text = "&b✰✰✰ Effect&r &7(&clocked&7)&f: " + starEff;
                    lines.addAll(LoreUtil.wrap(text, cl.getLoreLineLength()));
                } else {
                    lines.add(chat.color("&b✰✰✰ Effect&r&f: &7none"));
                }
            }


            meta.setLore(lines);
            menuItem.setItemMeta(meta);
            items.add(menuItem);
        }
        return InventoryManager.createListGui(player, chat.color("&c&lClasses Menu"), new ItemStack(Material.AIR), items);
    }

    public static void openClassesMenu(Player player) {
        createClassesMenu(player).openPage(1);
    }

    public static void openBuyClassInv(Player player, Class cl) {
        Inventory inv = Bukkit.createInventory(null, 27, chat.color("Buy class " + cl.getFancyName() + "?"));
        ItemStack confirm = Item.easyCreate("emeraldblock", "&r&fBuy " + cl.getFancyName() + "&r&f for &e" + cl.getPrice()
                + "&r&f " + ServerType.getThisCurrency() + ".");
        ItemStack cancel = Item.easyCreate("redstoneblock", "&c&lCancel");
        for (int i = 0; i < 27; i++) {
            if (i == 13) {
                inv.setItem(13, confirm);
            } else {
                inv.setItem(i, cancel);
            }
        }
        player.openInventory(inv);
    }

    public static void openBuyHeartEffInv(Player player, Class cl) {
        Inventory inv = Bukkit.createInventory(null, 27, chat.color("Buy ❤❤❤ effect for " + cl.getFancyName() + "?"));
        ItemStack confirm = Item.easyCreate("emeraldblock", "&r&fBuy ❤❤❤ effect for &e" + cl.getHeartEffPrice()
                + "&f AC Coins.");
        ItemStack cancel = Item.easyCreate("redstoneblock", "&c&lCancel");
        for (int i = 0; i < 27; i++) {
            if (i == 13) {
                inv.setItem(13, confirm);
            } else {
                inv.setItem(i, cancel);
            }
        }
        player.openInventory(inv);
    }

    public static void openBuyStarEffInv(Player player, Class cl) {
        Inventory inv = Bukkit.createInventory(null, 27, chat.color("Buy ✰✰✰ effect for " + cl.getFancyName() + "?"));
        ItemStack confirm = Item.easyCreate("emeraldblock", "&r&fBuy ✰✰✰ effect for &e" + cl.getStarEffPrice()
                + "&f AC Coins.");
        ItemStack cancel = Item.easyCreate("redstoneblock", "&c&lCancel");
        for (int i = 0; i < 27; i++) {
            if (i == 13) {
                inv.setItem(13, confirm);
            } else {
                inv.setItem(i, cancel);
            }
        }
        player.openInventory(inv);
    }

    public static void openPowerClassInventory(Player p, PowerClass cl) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        Object cachedGui = aPlayer.getData(cl.getName() + "ClassesMenu").getValue();
        if (cachedGui != null && cachedGui instanceof InventoryGui) {
            ((InventoryGui) cachedGui).openPage(1);
            return;
        }
        GuiPage page = new GuiPage(cl.getFancyName());
        GuiRow row1 = new GuiRow(null, page, 1);
        GuiRow row2 = new GuiRow(null, page, 2);
        GuiRow row3 = new GuiRow(null, page, 3);
        // background items
        ItemStack background = Item.easyCreate("stainedglasspane:black", " ");
        row1.setItem(2, background);
        row1.setItem(3, background);
        row1.setItem(4, background);
        row1.setItem(6, background);
        row1.setItem(7, background);
        row1.setItem(8, background);
        row2.setItem(1, background);
        row2.setItem(9, background);
        row3.setItem(1, background);
        row3.setItem(2, background);
        row3.setItem(3, background);
        row3.setItem(4, background);
        row3.setItem(5, background);
        row3.setItem(6, background);
        row3.setItem(7, background);
        row3.setItem(8, background);
        row3.setItem(9, background);
        // class item
        ItemStack classItem = cl.getMenuItem().clone();
        ItemMeta classMeta = classItem.getItemMeta();
        int lvl = (int) aPlayer.getData(ServerLink.getServerType() + ":classLevel" + cl.getName()).getValue();
        classMeta.setDisplayName(chat.color(cl.getFancyName() + "&r  lvl " + lvl));
        List<String> lines = new ArrayList<>();
        String expString = "||||||||||||||||||||||||||||||||||||||||";
        if (lvl < 50) {
            double currentExp = (double) aPlayer.getData(ServerLink.getServerType() + ":classExp" + cl.getName()).getValue();
            double nextLvlExp = cl.getExpForLvl(lvl + 1) - cl.getExpForLvl(lvl);
            currentExp = currentExp - cl.getExpForLvl(lvl);
            double percent = currentExp / nextLvlExp;
            int grayIndex = (int) (percent * 40.0);
            expString = expString.substring(0, grayIndex) + "&7" + expString.substring(grayIndex, expString.length());
            expString = "&bExp to level up: &a" + expString;
            expString = expString + "&r &8(&f" + CachedDecimalFormat.ONES.format(currentExp) + " &7/&f " + CachedDecimalFormat.ONES.format(nextLvlExp) + "&8)";
        } else expString = "&4&lMax Level Achieved!";
        expString = chat.color(expString);
        lines.add(chat.color(expString));
        lines.add("   ");
        lines.addAll(LoreUtil.wrap(cl.getDescription(), cl.getLoreLineLength()));
        String heartEff = cl.getHeartEffectDesc();
        String starEff = cl.getStarEffectDesc();
        lines.add("      ");
        if (heartEff != null && !heartEff.isEmpty()) {
            if (p.hasPermission(ServerLink.getServerType() + "." + cl.getName() + "HeartEffect"))
                lines.add(chat.color("&b❤❤❤ Effect: &aunlocked"));
            else lines.add(chat.color("&b❤❤❤ Effect: &cnot unlocked"));
        } else lines.add(chat.color("&b❤❤❤ Effect&f: &7none"));
        if (starEff != null && !starEff.isEmpty()) {
            if (p.hasPermission(ServerLink.getServerType() + "." + cl.getName() + "StarEffect"))
                lines.add(chat.color("&b✰✰✰ Effect: &aunlocked"));
            else lines.add(chat.color("&b✰✰✰ Effect: &cnot unlocked"));
        } else lines.add(chat.color("&b✰✰✰ Effect&f: &7none"));
        classMeta.setLore(lines);
        classItem.setItemMeta(classMeta);
        row1.setItem(5, classItem);
        lines.clear();
        // stats item
        row2.setItem(5, cl.getStatsItem(p));
        // heart effect item
        ItemStack heartItem = Item.easyCreate("goldenapple:1", "&b&lHeart Effect");
        ItemMeta heartMeta = heartItem.getItemMeta();
        if (heartEff != null && !heartEff.isEmpty()) {
            lines.add("  ");
            lines.addAll(LoreUtil.wrap("&b❤❤❤ Effect&f " + heartEff, cl.getLoreLineLength()));
            lines.add("     ");
            if (p.hasPermission(ServerLink.getServerType() + "." + cl.getName() + "HeartEffect")) {
                lines.add(chat.color("&aunlocked"));
            } else if (cl.getHeartEffPrice() <= 0) {
                lines.add(chat.color(cl.getHeartEffLockReason()));
            } else if (aPlayer.getCoins() >= cl.getHeartEffPrice() && (p.hasPermission(ServerLink.getServerType() + "." + cl.getName()) || cl == ClassManager.getDefaultClass())) {
                lines.add(chat.color("&cClick to unlock for &e" + cl.getHeartEffPrice() + " &cAC Coins"));
                row2.setClick(3, () -> openBuyHeartEffInv(p, cl));
            } else {
                lines.add(chat.color("&cCosts &e" + cl.getHeartEffPrice() + " &cAC Coins"));
                row2.setClick(3, () -> {
                    if (!p.hasPermission(ServerLink.getServerType() + "." + cl.getName()) && cl != ClassManager.getDefaultClass()) {
                        p.sendMessage(chat.color("&fYou haven't unlocked " + cl.getFancyName() + "&r!"));
                    } else if (aPlayer.getCoins() < cl.getHeartEffPrice()) {
                        p.sendMessage(chat.color("&fYou do not have enough AC Coins!"));
                    } else openBuyHeartEffInv(p, cl);
                });
            }
        } else heartMeta.setDisplayName(chat.color("&b&lHeart Effect:&r&7 none"));
        heartMeta.setLore(lines);
        heartItem.setItemMeta(heartMeta);
        row2.setItem(3, heartItem);
        lines.clear();
        // star effect item
        ItemStack starItem = Item.easyCreate("netherstar", "&b&lStar Effect");
        ItemMeta starMeta = starItem.getItemMeta();
        if (starEff != null && !starEff.isEmpty()) {
            lines.add("  ");
            lines.addAll(LoreUtil.wrap("&b✰✰✰ Effect&f " + starEff, cl.getLoreLineLength()));
            lines.add("     ");
            if (p.hasPermission(ServerLink.getServerType() + "." + cl.getName() + "StarEffect")) {
                lines.add(chat.color("&aunlocked"));
            } else if (cl.getStarEffPrice() <= 0) {
                lines.add(chat.color(cl.getStarEffLockReason()));
            } else if (aPlayer.getCoins() >= cl.getStarEffPrice() && (p.hasPermission(ServerLink.getServerType() + "." + cl.getName()) || cl == ClassManager.getDefaultClass())) {
                lines.add(chat.color("&cClick to unlock for &e" + cl.getStarEffPrice() + " &cAC Coins"));
                row2.setClick(7, () -> openBuyStarEffInv(p, cl));
            } else {
                lines.add(chat.color("&cCosts &e" + cl.getStarEffPrice() + " &cAC Coins"));
                row2.setClick(7, () -> {
                    if (!p.hasPermission(ServerLink.getServerType() + "." + cl.getName()) && cl != ClassManager.getDefaultClass()) {
                        p.sendMessage(chat.color("&fYou haven't unlocked " + cl.getFancyName() + "&r!"));
                    } else if (aPlayer.getCoins() < cl.getStarEffPrice()) {
                        p.sendMessage(chat.color("&fYou do not have enough AC Coins!"));
                    } else openBuyStarEffInv(p, cl);
                });
            }
        } else starMeta.setDisplayName(chat.color("&b&lStar Effect:&r&7 none"));
        starMeta.setLore(lines);
        starItem.setItemMeta(starMeta);
        row2.setItem(7, starItem);
        lines.clear();
        // back button item
        row1.setItem(1, Item.easyCreate("arrow", "Back to &c&lClasses Menu"));
        row1.setClick(1, () -> createClassesMenu(p));
        // equip/buy/can't_buy item
        ItemStack buyItem;
        if (cl == ClassManager.getDefaultClass() || p.hasPermission(ServerLink.getServerType() + "." + cl.getName())) {
            buyItem = Item.easyCreate("emeraldblock", "&a&lClick to Select", cl.getFancyName());
            row1.setClick(9, () -> {
                p.sendMessage(chat.color(chat.getServer() + "&aYou equipped "
                        + cl.getFancyName() + "."));
                Class oldClass = (Class) aPlayer.getData("class").getValue();
                aPlayer.getData("class").setValue(cl);
                Bukkit.getPluginManager().callEvent(new CustomChangeClassEvent(p, oldClass));
                p.closeInventory();
            });
        } else if (cl.getPrice() > 0) {
            if (aPlayer.getPoints() < cl.getPrice()) {
                buyItem = Item.easyCreate("redstoneblock", cl.getFancyName() + "&c&l costs", "&e" + cl.getPrice() + "&c&l " + ServerType.getThisCurrency());
                row1.setClick(9, () -> p.sendMessage(chat.color(chat.getServer() + "You need " + (cl.getPrice() - aPlayer.getPoints()) + " more " + ServerType.getThisCurrency() + " before you " + "can buy " + cl.getFancyName() + ".")));
            } else {
                buyItem = Item.easyCreate("goldblock", "&c&lClick to buy", "&c&lfor &e" + cl.getPrice() + "&c&l " + ServerType.getThisCurrency());
                row1.setClick(9, () -> openBuyClassInv(p, cl));
            }
        } else {
            buyItem = Item.easyCreate("redstoneblock", "&c&lClass Not Unlocked");
            row1.setClick(9, () -> p.sendMessage(chat.color(chat.getServer() + "You haven't unlocked " + cl.getFancyName() + "&f class.")));
        }
        row1.setItem(9, buyItem);
        page.setRows(row1, row2, row3);
        InventoryGui gui = new InventoryGui(p, cl.getName() + "ClassMenu", page);
        gui.openPage(1);
        aPlayer.getData(cl.getName() + "ClassMenu").setValue(gui);
        aPlayer.getData(cl.getName() + "ClassMenu").setValue(null);
    }

    public static void openClassInventory(Player p, Class cl) {
        AlphaPlayer aPlayer = PlayerManager.get(p);
        GuiPage page = new GuiPage(cl.getFancyName());
        GuiRow row1 = new GuiRow(null, page, 1);
        GuiRow row2 = new GuiRow(null, page, 2);
        GuiRow row3 = new GuiRow(null, page, 3);
        // background items
        ItemStack background = Item.easyCreate("stainedglasspane:black", " ");
        row1.setItem(2, background);
        row1.setItem(3, background);
        row1.setItem(4, background);
        row1.setItem(6, background);
        row1.setItem(7, background);
        row1.setItem(8, background);
        row2.setItem(1, background);
        row2.setItem(9, background);
        row3.setItem(1, background);
        row3.setItem(2, background);
        row3.setItem(3, background);
        row3.setItem(4, background);
        row3.setItem(5, background);
        row3.setItem(6, background);
        row3.setItem(7, background);
        row3.setItem(8, background);
        row3.setItem(9, background);
        // class item
        ItemStack classItem = cl.getMenuItem().clone();
        ItemMeta classMeta = classItem.getItemMeta();
        List<String> lines = new ArrayList<>();
        lines.add("   ");
        lines.addAll(LoreUtil.wrap(cl.getDescription(), cl.getLoreLineLength()));
        String heartEff = cl.getHeartEffectDesc();
        String starEff = cl.getStarEffectDesc();
        lines.add("      ");
        if (heartEff != null && !heartEff.isEmpty()) {
            if (p.hasPermission(ServerLink.getServerType() + "." + cl.getName() + "HeartEffect"))
                lines.add(chat.color("&b❤❤❤ Effect: &aunlocked"));
            else lines.add(chat.color("&b❤❤❤ Effect: &cnot unlocked"));
        } else lines.add(chat.color("&b❤❤❤ Effect&f: &7none"));
        if (starEff != null && !starEff.isEmpty()) {
            if (p.hasPermission(ServerLink.getServerType() + "." + cl.getName() + "StarEffect"))
                lines.add(chat.color("&b✰✰✰ Effect: &aunlocked"));
            else lines.add(chat.color("&b✰✰✰ Effect: &cnot unlocked"));
        } else lines.add(chat.color("&b✰✰✰ Effect&f: &7none"));
        classMeta.setLore(lines);
        classItem.setItemMeta(classMeta);
        row1.setItem(5, classItem);
        lines.clear();
        // heart effect item
        ItemStack heartItem = Item.easyCreate("goldenapple:1", "&b&lHeart Effect");
        ItemMeta heartMeta = heartItem.getItemMeta();
        if (heartEff != null && !heartEff.isEmpty()) {
            lines.add("  ");
            lines.addAll(LoreUtil.wrap("&b❤❤❤ Effect&f " + heartEff, cl.getLoreLineLength()));
            lines.add("     ");
            if (p.hasPermission(ServerLink.getServerType() + "." + cl.getName() + "HeartEffect")) {
                lines.add(chat.color("&aunlocked"));
            } else if (cl.getHeartEffPrice() <= 0) {
                lines.add(chat.color(cl.getHeartEffLockReason()));
            } else if (aPlayer.getCoins() >= cl.getHeartEffPrice() && (p.hasPermission(ServerLink.getServerType() + "." + cl.getName()) || cl == ClassManager.getDefaultClass())) {
                lines.add(chat.color("&cClick to unlock for &e" + cl.getHeartEffPrice() + " &cAC Coins"));
                row2.setClick(3, () -> openBuyHeartEffInv(p, cl));
            } else {
                lines.add(chat.color("&cCosts &e" + cl.getHeartEffPrice() + " &cAC Coins"));
                row2.setClick(3, () -> {
                    if (!p.hasPermission(ServerLink.getServerType() + "." + cl.getName()) && cl != ClassManager.getDefaultClass()) {
                        p.sendMessage(chat.color("&fYou haven't unlocked " + cl.getFancyName() + "&r!"));
                    } else if (aPlayer.getCoins() < cl.getHeartEffPrice()) {
                        p.sendMessage(chat.color("&fYou do not have enough AC Coins!"));
                    } else openBuyHeartEffInv(p, cl);
                });
            }
        } else heartMeta.setDisplayName(chat.color("&b&lHeart Effect:&r&7 none"));
        heartMeta.setLore(lines);
        heartItem.setItemMeta(heartMeta);
        row2.setItem(3, heartItem);
        lines.clear();
        // star effect item
        ItemStack starItem = Item.easyCreate("netherstar", "&b&lStar Effect");
        ItemMeta starMeta = starItem.getItemMeta();
        if (starEff != null && !starEff.isEmpty()) {
            lines.add("  ");
            lines.addAll(LoreUtil.wrap("&b✰✰✰ Effect&f " + starEff, cl.getLoreLineLength()));
            lines.add("     ");
            if (p.hasPermission(ServerLink.getServerType() + "." + cl.getName() + "StarEffect")) {
                lines.add(chat.color("&aunlocked"));
            } else if (cl.getStarEffPrice() <= 0) {
                lines.add(chat.color(cl.getStarEffLockReason()));
            } else if (aPlayer.getCoins() >= cl.getStarEffPrice() && (p.hasPermission(ServerLink.getServerType() + "." + cl.getName()) || cl == ClassManager.getDefaultClass())) {
                lines.add(chat.color("&cClick to unlock for &e" + cl.getStarEffPrice() + " &cAC Coins"));
                row2.setClick(7, () -> openBuyStarEffInv(p, cl));
            } else {
                lines.add(chat.color("&cCosts &e" + cl.getStarEffPrice() + " &cAC Coins"));
                row2.setClick(7, () -> {
                    if (!p.hasPermission(ServerLink.getServerType() + "." + cl.getName()) && cl != ClassManager.getDefaultClass()) {
                        p.sendMessage(chat.color("&fYou haven't unlocked " + cl.getFancyName() + "&r!"));
                    } else if (aPlayer.getCoins() < cl.getStarEffPrice()) {
                        p.sendMessage(chat.color("&fYou do not have enough AC Coins!"));
                    } else openBuyStarEffInv(p, cl);
                });
            }
        } else starMeta.setDisplayName(chat.color("&b&lStar Effect:&r&7 none"));
        starMeta.setLore(lines);
        starItem.setItemMeta(starMeta);
        row2.setItem(7, starItem);
        lines.clear();
        // back button item
        row1.setItem(1, Item.easyCreate("arrow", "Back to &c&lClasses Menu"));
        row1.setClick(1, () -> createClassesMenu(p));
        // equip/buy/can't_buy item
        ItemStack buyItem;
        if (cl == ClassManager.getDefaultClass() || p.hasPermission(ServerLink.getServerType() + "." + cl.getName())) {
            buyItem = Item.easyCreate("emeraldblock", "&a&lClick to Select", cl.getFancyName());
            row1.setClick(9, () -> {
                p.sendMessage(chat.color(chat.getServer() + "&aYou equipped "
                        + cl.getFancyName() + "."));
                Class oldClass = (Class) aPlayer.getData("class").getValue();
                aPlayer.getData("class").setValue(cl);
                Bukkit.getPluginManager().callEvent(new CustomChangeClassEvent(p, oldClass));
                p.closeInventory();
            });
        } else if (cl.getPrice() > 0) {
            if (aPlayer.getPoints() < cl.getPrice()) {
                buyItem = Item.easyCreate("redstoneblock", cl.getFancyName() + "&c&l costs", "&e" + cl.getPrice() + "&c&l " + ServerType.getThisCurrency());
                String finalCurrencyName = ServerType.getThisCurrency();
                row1.setClick(9, () -> p.sendMessage(chat.color(chat.getServer() + "You need " + (cl.getPrice() - aPlayer.getPoints()) + " more " + finalCurrencyName + " before you " + "can buy " + cl.getFancyName() + ".")));
            } else {
                buyItem = Item.easyCreate("goldblock", "&c&lClick to buy", "&c&lfor &e" + cl.getPrice() + "&c&l " + ServerType.getThisCurrency());
                row1.setClick(9, () -> openBuyClassInv(p, cl));
            }
        } else {
            buyItem = Item.easyCreate("redstoneblock", "&c&lClass Not Unlocked");
            row1.setClick(9, () -> p.sendMessage(chat.color(chat.getServer() + "You haven't unlocked " + cl.getFancyName() + "&f class.")));
        }
        row1.setItem(9, buyItem);
        page.setRows(row1, row2, row3);
        InventoryGui gui = new InventoryGui(p, cl.getName() + "ClassMenu", page);
        gui.openPage(1);
    }
}

package serverlink.classes;

import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.inventory.ItemStack;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class WeaponType {
    private ItemStack item;
    private int itemSlot;
    private Set<Ability> abilities;
    private double percentKnockback;
    private double percentProjKnockback = 100;
    // health (1-20)
    private double damageDealt;
    // health (1-20)
    private double projectileDamageDealt = 1;
    private List<Player> cancelNextProjectileDamage = new ArrayList<>();

    public WeaponType(ItemStack item, int itemSlot, Set<Ability> abilities,
                      double damageDealt, double percentKnockback) {
        this.item = item;
        this.itemSlot = itemSlot;
        this.abilities = abilities;
        this.percentKnockback = percentKnockback;
        this.damageDealt = damageDealt;
    }

    // weapon that shoots projectiles (from bow/snowball or from ability)
    public WeaponType(ItemStack item, int itemSlot, Set<Ability> abilities,
                      double damageDealt, double percentKnockback,
                      double projectileDamageDealt, double percentProjKnockback) {
        this.item = item;
        this.itemSlot = itemSlot;
        this.abilities = abilities;
        this.percentKnockback = percentKnockback;
        this.damageDealt = damageDealt;
        this.projectileDamageDealt = projectileDamageDealt;
        this.percentProjKnockback = percentProjKnockback;
    }

    public double getPercentProjKnockback() {
        return percentProjKnockback;
    }

    public double getProjectileDamageDealt() {
        return projectileDamageDealt;
    }

    public ItemStack getItem() {
        if (item == null) return null;
        return item.clone();
    }

    public double getPercentKnockback() {
        return percentKnockback;
    }

    public double getDamageDealt() {
        return damageDealt;
    }

    public int getItemSlot() {
        return itemSlot;
    }

    public Set<Ability> getAbilities() {
        return abilities;
    }

    public void onProjectileLaunch(Player player, Weapon weapon, Projectile proj) {
    }

    public void onProjectileHit(Player player, LivingEntity enemy, Weapon weapon) {
    }

    public void onProjectileLand(Player player, Projectile proj, Weapon weapon) {
    }

    public void cancelNextProjectileDamage(Player shooter) {
        cancelNextProjectileDamage.add(shooter);
    }

    void removeNextProjectileDamagePlayer(Player shooter) {
        cancelNextProjectileDamage.remove(shooter);
    }

    boolean isNextProjectileDamageCancelled(Player shooter) {
        return cancelNextProjectileDamage.contains(shooter);
    }
}

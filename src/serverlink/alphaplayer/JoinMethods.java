package serverlink.alphaplayer;

import org.bukkit.Bukkit;
import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.chat.nametag.Nametag;
import serverlink.classes.ClassManager;
import serverlink.inventory.InventoryManager;
import serverlink.network.JedisPool;
import serverlink.player.Afk;
import serverlink.player.Forums;
import serverlink.player.Permissions;
import serverlink.server.ServerType;

class JoinMethods {
    static void join(Player player) {
        player.setNoDamageTicks(0);
        player.setGameMode(GameMode.SURVIVAL);
        player.setHealth(20);
        player.setFoodLevel(20);
        player.setExp(0);
        InventoryManager.clearInventory(player);
        AlphaPlayer aPlayer = PlayerManager.get(player);
        // The order in which these are called is important
        permissions(aPlayer, player);
        aPlayer.updateNameFormatting();
        Nametag.sendTeams(player);
        aPlayer.getData("class").setValue(ClassManager.getDefaultClass());
        hideAndShowPlayers(aPlayer, player);
        uuid(player);
        Afk.join(player);
        Forums.join(aPlayer);
        aPlayer.alphaBoard = new AlphaScoreboard(player);
        if (ServerLink.getServerType() == ServerType.hub) {
            if ((int) aPlayer.getData("speedSetting").getValue() == 2)
                player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 999999, 0, false, false), true);
            else if ((int) aPlayer.getData("speedSetting").getValue() == 3)
                player.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 999999, 1, false, false), true);
        }
        String messages = chat.getJoinLeaveMessagesStatus();
        if (!messages.equals("none")) {
            if (messages.equals("all") || (messages.equals("donors") && aPlayer.getRank() > 1))
                if (aPlayer.getRank() > 1)
                    Bukkit.getServer().broadcastMessage(chat.color("&8<&a&lJoin&r&8> " +
                            Permissions.getRankName(aPlayer.getRank()) + "&r " + player.getDisplayName()));
                else
                    Bukkit.getServer().broadcastMessage(chat.color("&8<&a&lJoin&r&8> " + player.getDisplayName()));
        }
        player.setFlying(false);
        player.setAllowFlight(false);
        for (PotionEffect effect : player.getActivePotionEffects()) player.removePotionEffect(effect.getType());
        player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 10, 1, false, false), false);
        applySpeed(aPlayer, player);
        PlayerManager.onlineAlphaPlayers.add(aPlayer);
    }

    private static void permissions(AlphaPlayer aPlayer, Player player) {
        aPlayer.attachment = player.addAttachment(ServerLink.plugin);
        for (String s : aPlayer.allowedPerms) aPlayer.attachment.setPermission(s, true);
        for (String s : aPlayer.deniedPerms) aPlayer.attachment.setPermission(s, false);
    }

    private static void uuid(Player p) {
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                jedis.set(p.getName().toLowerCase(), p.getUniqueId().toString());
                jedis.hset(p.getUniqueId().toString(), "username", p.getName());
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    static void hideAndShowPlayers(AlphaPlayer aPlayer, Player p1) {
        if (ServerLink.getServerType() != ServerType.hub) return;
        Integer p1MaxPlayersToShow = aPlayer.getIntegerSetting("playersShownInHub");
        int p1CurrentPlayersShown = 0;
        for (AlphaPlayer p2 : PlayerManager.getOnlineAlphaPlayers()) {
            // hide/show players to p1
            if (p1CurrentPlayersShown >= p1MaxPlayersToShow) {
                p1.hidePlayer(p2.getPlayer());
                p2.hideRankTagFrom(p1);
            } else {
                p1.showPlayer(p2.getPlayer());
                p2.showRankTagTo(p1);
                p1CurrentPlayersShown++;
            }
            // we just handled hiding/showing p2 to p1; now we check to hide p1 from p2
            Integer p2MaxPlayersToShow = p2.getIntegerSetting("playersShownInHub");
            Integer p2CurrentPlayersShown = (Integer) p2.getData("playersCurrentlyShown", 0).getValue();
            if (p2CurrentPlayersShown >= p2MaxPlayersToShow) {
                p2.getPlayer().hidePlayer(p1);
                aPlayer.hideRankTagFrom(p2.getPlayer());
            } else {
                aPlayer.showRankTagTo(p2.getPlayer());
                p2.getData("playersCurrentlyShown").setValue(p2CurrentPlayersShown + 1);
            }
        }
        aPlayer.getData("playersCurrentlyShown").setValue(p1CurrentPlayersShown);
    }

    private static void applySpeed(AlphaPlayer aPlayer, Player p1) {
        if (ServerLink.getServerType() != ServerType.hub) return;
        if (aPlayer.getIntegerSetting("speedSetting") == 2)
            p1.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 999999, 0, false, false), true);
        else if (aPlayer.getIntegerSetting("speedSetting") == 3)
            p1.addPotionEffect(new PotionEffect(PotionEffectType.SPEED, 999999, 1, false, false), true);
    }
}

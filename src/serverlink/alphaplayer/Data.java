package serverlink.alphaplayer;

public class Data {
    private final AlphaPlayer aPlayer;
    private final String id;
    private Object value;

    Data(String id, AlphaPlayer aPlayer) {
        this.id = id;
        this.aPlayer = aPlayer;
    }

    Data(String id, AlphaPlayer aPlayer, Object value) {
        this.id = id;
        this.aPlayer = aPlayer;
        this.value = value;
    }

    public AlphaPlayer getAlphaPlayer() {
        return aPlayer;
    }

    public String getId() {
        return id;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }
}

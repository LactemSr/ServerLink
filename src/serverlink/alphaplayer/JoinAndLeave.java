package serverlink.alphaplayer;

import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByBlockEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.player.AsyncPlayerPreLoginEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.map.MapManager;
import serverlink.network.navail;
import serverlink.network.nstatus;
import serverlink.util.Slack;

public class JoinAndLeave implements Listener {
    @EventHandler(priority = EventPriority.LOWEST)
    void interactBeforeLogin(PlayerInteractEvent e) {
        if (!e.getPlayer().hasMetadata("NPC") && !PlayerManager.get(e.getPlayer().getUniqueId()).isFullyLoggedIn())
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOW)
    void interactBeforeLogin_2(PlayerInteractEvent e) {
        if (!e.getPlayer().hasMetadata("NPC") && !PlayerManager.get(e.getPlayer().getUniqueId()).isFullyLoggedIn())
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOWEST)
    void takeDamageBeforeLogin(EntityDamageEvent e) {
        if (e.getEntityType() != EntityType.PLAYER) return;
        if (!e.getEntity().hasMetadata("NPC") && !PlayerManager.get((Player) e.getEntity()).isFullyLoggedIn())
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOW)
    void takeDamageBeforeLogin_2(EntityDamageEvent e) {
        if (e.getEntityType() != EntityType.PLAYER) return;
        if (!e.getEntity().hasMetadata("NPC") && !PlayerManager.get((Player) e.getEntity()).isFullyLoggedIn())
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOW)
    void takeDamageBeforeLogin_2(EntityDamageByEntityEvent e) {
        if (e.getEntityType() != EntityType.PLAYER) return;
        if (!e.getEntity().hasMetadata("NPC") && !PlayerManager.get((Player) e.getEntity()).isFullyLoggedIn())
            e.setCancelled(true);
    }

    @EventHandler(priority = EventPriority.LOW)
    void takeDamageBeforeLogin_2(EntityDamageByBlockEvent e) {
        if (e.getEntityType() != EntityType.PLAYER) return;
        if (!e.getEntity().hasMetadata("NPC") && !PlayerManager.get((Player) e.getEntity()).isFullyLoggedIn())
            e.setCancelled(true);
    }

    @EventHandler
    void preJoin(AsyncPlayerPreLoginEvent e) {
        if (navail.isClosed()) {
            e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER, "The server you tried to join is full.");
            Slack.sendPost("#errors", "Server " + ServerLink.getServerName()
                    + " has kicked a player because the server is full (this isn't supposed to happen)");
        }
    }

    @EventHandler(priority = EventPriority.LOWEST)
    public void serverNotSetup(AsyncPlayerPreLoginEvent e) {
        if (ServerLink.getServerName().isEmpty()) {
            e.disallow(AsyncPlayerPreLoginEvent.Result.KICK_OTHER,
                    "The serverName needs to be set in the universal.yml file before you can play.");
        }
    }

    // prejoin loads data asynchronously; join applies that data
    @EventHandler(priority = EventPriority.HIGHEST)
    void onPreJoin(final AsyncPlayerPreLoginEvent e) {
        if (e.getLoginResult() != AsyncPlayerPreLoginEvent.Result.ALLOWED) return;
        new AlphaPlayer(e.getUniqueId());
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    void onJoin(PlayerJoinEvent e) {
        e.setJoinMessage(null);
        final Player player = e.getPlayer();
        player.setFireTicks(0);
        player.setNoDamageTicks(40);
        player.addPotionEffect(new PotionEffect(PotionEffectType.BLINDNESS, 999999, 1, false, false), false);
        player.setAllowFlight(true);
        player.setFlying(true);
        if (nstatus.isLobby() || nstatus.isLobbyCountdown())
            player.teleport(MapManager.getLobbyMap().getWorld().getSpawnLocation().add(0, 500, 0));
        else
            player.teleport(MapManager.getCurrentGameMap().getWorld().getSpawnLocation().add(0, 500, 0));
        AlphaPlayer aPlayer = PlayerManager.get(player.getUniqueId());
        if (aPlayer == null) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (e.getPlayer() == null || !e.getPlayer().isOnline()) {
                        cancel();
                        return;
                    }
                    AlphaPlayer aP = PlayerManager.get(player.getUniqueId());
                    if (aP != null) {
                        LoginQueue.add(aP, player);
                        cancel();
                    }
                }
            }.runTaskTimer(ServerLink.plugin, 2, 1);
        } else LoginQueue.add(aPlayer, player);
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    void onLeave(PlayerQuitEvent e) {
        e.setQuitMessage(null);
        if (e.getPlayer() != null && e.getPlayer().getDisplayName() != null) {
            if (e.getPlayer().getVehicle() != null) {
                try {
                    e.getPlayer().getVehicle().eject();
                    e.getPlayer().getVehicle().remove();
                } catch (Exception ignored) {
                }
            }
            LeaveMethods.leave(PlayerManager.get(e.getPlayer()));
        }
    }
}

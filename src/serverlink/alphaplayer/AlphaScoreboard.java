package serverlink.alphaplayer;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import serverlink.ServerLink;
import serverlink.chat.chat;

import java.util.ArrayList;
import java.util.TreeMap;

public class AlphaScoreboard {
    private static final int refreshRate = 4;
    private Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();
    private String title = "AlphaCraft Network";
    private Objective activeSidebar;
    private Objective bufferSidebar;
    private TreeMap<Integer, String> lines = new TreeMap<>();
    private String cachedTitle = "AlphaCraft Network";
    // cachedBody is used to make the API easier
    private ArrayList<String> cachedBody = new ArrayList<>();
    // body is only used for quick/efficient comparing
    private ArrayList<String> body = new ArrayList<>();
    private int id;

    public AlphaScoreboard(Player player) {
        if (player.hasMetadata("NPC")) return;
        activeSidebar = board.registerNewObjective(player.getName(), "dummy");
        activeSidebar.setDisplaySlot(DisplaySlot.SIDEBAR);
        player.setScoreboard(board);
        // update board every refreshRate ticks
        id = new BukkitRunnable() {
            @Override
            public void run() {
                runSidebar();
            }
        }.runTaskTimer(ServerLink.plugin, 2, refreshRate).getTaskId();
    }

    public Scoreboard getBoard() {
        return board;
    }

    // caches info to be sent every time runSidebar() is called
    public void setSidebar(String title, ArrayList<String> body) {
        cachedTitle = title;
        cachedBody = body;
        for (int i = 0; i < body.size(); i++) {
            String line = body.get(i);
            if (line.length() > 40) {
                line = line.substring(0, 40);
                if (line.endsWith("&")) line = line.substring(0, 39);
                body.set(i, line);
            }
        }
    }

    // caches info to be sent every time runSidebar() is called
    public void removeLine(int line) {
        try {
            cachedBody.remove(line);
        } catch (Exception ignored) {
        }
    }

    // caches info to be sent every time runSidebar() is called
    public void addLine(String value) {
        cachedBody.add(value);
    }

    // caches info to be sent every time runSidebar() is called
    public void setLine(int line, String value) {
        cachedBody.set(line, value);
    }

    // caches info to be sent every time runSidebar() is called
    public void setTitle(String title) {
        cachedTitle = title;
    }

    // gets the cached info and displays it
    private void runSidebar() {
        cachedTitle = cachedTitle.trim();
        if (cachedTitle.equals(title) && body.toString().equals(cachedBody.toString())) return;
        body = cachedBody;
        TreeMap<Integer, String> newLines = new TreeMap<>();
        int line = 20;
        for (String s : cachedBody) {
            if (s.toLowerCase().startsWith("blankline")) {
                for (ChatColor color : ChatColor.values()) {
                    if (!newLines.containsValue(color + " ")) {
                        s = color + " ";
                        break;
                    }
                }
            }
            newLines.put(line, s);
            line--;
        }
        bufferSidebar = activeSidebar;
        boolean boardChanged = false;
        // check if title changed
        if (!cachedTitle.equals(title)) {
            boardChanged = true;
            bufferSidebar.setDisplayName(chat.color(cachedTitle));
            title = cachedTitle;
        }
        // new board has more or fewer lines than old
        if (newLines.size() != lines.size()) {
            boardChanged = true;
            // set entire board
            // first clear old lines
            for (int i = 20; i >= 1; i--) {
                if (lines.containsKey(i)) {
                    bufferSidebar.getScoreboard().resetScores(chat.color(lines.get(i)));
                }
            }
            // then add current ones
            for (int i : newLines.keySet()) {
                bufferSidebar.getScore(chat.color(newLines.get(i))).setScore(i);
            }
        } else {
            // update lines that have modified text
            for (int i : newLines.keySet()) {
                if (chat.color(lines.get(i)).equals(chat.color(newLines.get(i)))) continue;
                boardChanged = true;
                bufferSidebar.getScoreboard().resetScores(chat.color(lines.get(i)));
                bufferSidebar.getScore(chat.color(newLines.get(i))).setScore(i);
            }
        }
        if (!boardChanged) return;
        lines = newLines;
        // display info from buffer
        activeSidebar = bufferSidebar;
    }

    public void leave() {
        Bukkit.getScheduler().cancelTask(id);
        activeSidebar.unregister();
        // weird exception - says bufferSidebar is unregistered
        try {
            bufferSidebar.unregister();
        } catch (Exception ignored) {
        }
    }

}

package serverlink.alphaplayer;

import mkremins.fanciful.FancyMessage;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.chat.NameFormat;
import serverlink.chat.chat;
import serverlink.classes.Class;
import serverlink.classes.ClassManager;
import serverlink.classes.PowerClass;
import serverlink.network.JedisPool;
import serverlink.player.*;
import serverlink.player.party.PartyManager;
import serverlink.player.stats.StatsManager;
import serverlink.server.ServerType;
import serverlink.util.CommandText;
import serverlink.util.Probability;
import serverlink.util.TimeUtils;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.time.Instant;
import java.util.*;
import java.util.stream.Collectors;

class PreJoinMethods {
    static void preJoin(AlphaPlayer aPlayer, String uuid) {
        aPlayer.getData("class").setValue(ClassManager.getDefaultClass());
        Jedis jedis = JedisPool.getConn();
        String online = jedis.hget(uuid, "online");
        if (online != null && online.equals("true")) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    String stillOnline = jedis.hget(uuid, "online");
                    if (stillOnline == null || stillOnline.equals("false")) {
                        login(aPlayer, uuid, jedis);
                        JedisPool.close(jedis);
                        return;
                    }
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            String stillOnline = jedis.hget(uuid, "online");
                            if (stillOnline == null || stillOnline.equals("false")) {
                                login(aPlayer, uuid, jedis);
                            }
                            JedisPool.close(jedis);
                        }
                    }.runTaskLaterAsynchronously(ServerLink.plugin, 18);
                }
            }.runTaskLaterAsynchronously(ServerLink.plugin, 5);
        } else {
            login(aPlayer, uuid, jedis);
            JedisPool.close(jedis);
        }
    }

    private static void login(AlphaPlayer aPlayer, String uuid, Jedis jedis) {
        permissions(aPlayer, uuid, jedis);
        rankExpiry(aPlayer, uuid, jedis);
        points(aPlayer, uuid, jedis);
        coins(aPlayer, uuid, jedis);
        level(aPlayer, uuid, jedis);
        mute(aPlayer, uuid, jedis);
        loadSettings(aPlayer, uuid, jedis);
        chat(aPlayer);
        friends(aPlayer, uuid, jedis);
        party(aPlayer, uuid, jedis);
        referrals(aPlayer, uuid, jedis);
        treasure(aPlayer, uuid, jedis);
        playTime(aPlayer, jedis);
        powerclassStatCache(aPlayer);
        try {
            for (Method method : ServerLink.getPreJoinMethods()) {
                method.invoke(new Object(), aPlayer, jedis);
            }
        } catch (IllegalArgumentException | InvocationTargetException | IllegalAccessException e) {
            e.printStackTrace();
        }
        notifications(aPlayer, jedis);
        aPlayer.preLoggedIn.set(true);
    }

    private static void permissions(AlphaPlayer aPlayer, String uuid, Jedis jedis) {
        List<String> perms = jedis.lrange("perms:" + uuid, 0, -1);
        aPlayer.rank = 0;
        // 10 for 10 groups
        for (int i = 10; i > 0; i--) {
            if (!perms.contains("rank." + i)) if (i > 1) continue;
            if (aPlayer.getRank() < i) aPlayer.rank = i;
            for (int j = 1; j <= i; j++) {
                aPlayer.allowedPerms.add("rank." + j);
                for (String perm : Permissions.permissionsGroups.get(j)) {
                    if (perm.startsWith("-")) {
                        aPlayer.deniedPerms.add(perm.substring(1));
                    } else {
                        aPlayer.allowedPerms.add(perm);
                    }
                }
            }
            break;
        }
        for (String perm : perms) {
            if (perm.startsWith("-")) {
                aPlayer.deniedPerms.add(perm.substring(1));
            } else {
                aPlayer.allowedPerms.add(perm);
            }
        }
    }

    private static void rankExpiry(AlphaPlayer aPlayer, String uuid, Jedis jedis) {
        if (jedis.hget(uuid, "rank") == null) jedis.hset(uuid, "rank", "1");
        double currentTime = (System.currentTimeMillis() / 1000.0);
        double rank5;
        try {
            rank5 = Integer.parseInt(jedis.hget(uuid, "rank5Expiry"));
        } catch (NumberFormatException e) {
            jedis.hset(uuid, "rank5Expiry", "0");
            rank5 = 0;
        }
        double rank4;
        try {
            rank4 = Integer.parseInt(jedis.hget(uuid, "rank4Expiry"));
        } catch (NumberFormatException e) {
            jedis.hset(uuid, "rank4Expiry", "0");
            rank4 = 0;
        }
        double rank3;
        try {
            rank3 = Integer.parseInt(jedis.hget(uuid, "rank3Expiry"));
        } catch (NumberFormatException e) {
            jedis.hset(uuid, "rank3Expiry", "0");
            rank3 = 0;
        }
        double rank2;
        try {
            rank2 = Integer.parseInt(jedis.hget(uuid, "rank2Expiry"));
        } catch (NumberFormatException e) {
            jedis.hset(uuid, "rank2Expiry", "0");
            rank2 = 0;
        }
        int rank = aPlayer.getRank();
        if (!(rank5 != 0 && (rank5 - currentTime) < 0 && rank <= 5) &&
                !(rank4 != 0 && (rank4 - currentTime) < 0 && rank <= 4) &&
                !(rank3 != 0 && (rank3 - currentTime) < 0 && rank <= 3) &&
                !(rank2 != 0 && (rank2 - currentTime) < 0 && rank <= 2))
            return;
        jedis.hset(uuid, "rank2Expiry", "0");
        jedis.hset(uuid, "rank3Expiry", "0");
        jedis.hset(uuid, "rank4Expiry", "0");
        jedis.hset(uuid, "rank5Expiry", "0");
        // update local permissions
        aPlayer.allowedPerms.remove("rank.5");
        aPlayer.allowedPerms.remove("rank.4");
        aPlayer.allowedPerms.remove("rank.3");
        aPlayer.allowedPerms.remove("rank.2");
        aPlayer.allowedPerms.add("rank.1");
        // update permissions in the database
        jedis.lrem("perms:" + uuid, 0, "rank.5");
        jedis.lrem("perms:" + uuid, 0, "rank.4");
        jedis.lrem("perms:" + uuid, 0, "rank.3");
        jedis.lrem("perms:" + uuid, 0, "rank.2");
        jedis.lpush("perms:" + uuid, "rank.1");
        // update rank
        aPlayer.rank = 1;
        jedis.hset(uuid, "rankOnForums", "0");
        jedis.hset(uuid, "rank", "1");
        // send a notification so the player can buy another rank
        FancyMessage message = new FancyMessage("Your rank has expired. ").color(ChatColor.RED).then("ALL your" +
                " previous benefits will be restored when you purchase it again from our shop: ")
                .color(ChatColor.GREEN).then("shop.alphacraft.us").color(ChatColor.GOLD).style(ChatColor.BOLD)
                .tooltip("Click to visit the shop").link("http://shop.alphacraft.us");
        Notifications.sendFancyNotification(uuid, message, 60 * 60 * 24 * 30 * 24);
    }

    private static void points(AlphaPlayer aPlayer, String uuid, Jedis jedis) {
        String points = jedis.hget(uuid, ServerLink.getServerType() + "Points");
        Integer iPoints;
        try {
            iPoints = Integer.parseInt(points);
        } catch (NumberFormatException e1) {
            iPoints = 0;
        }
        aPlayer.points = iPoints;

        iPoints = 0;
        int nextExpCheckTime = 0;
        try {
            String s = jedis.hget(uuid, "gainedPoints");
            iPoints = Integer.parseInt(s.split(":")[0]);
            nextExpCheckTime = Integer.parseInt(s.split(":")[1]);
        } catch (Exception ignored) {
        }
        aPlayer.gainedPoints = iPoints;
        aPlayer.nextExpCheckTime = nextExpCheckTime;
    }

    private static void coins(AlphaPlayer aPlayer, String uuid, Jedis jedis) {
        String points = jedis.hget(uuid, "coins");
        Integer iCoins;
        try {
            iCoins = Integer.parseInt(points);
        } catch (NumberFormatException e1) {
            iCoins = 0;
        }
        aPlayer.coins = iCoins;
    }

    private static void level(AlphaPlayer aPlayer, String uuid, Jedis jedis) {
        int iExp = 0;
        try {
            String s = jedis.hget(uuid, "exp");
            iExp = Integer.parseInt(s);
        } catch (Exception ignored) {
        }
        aPlayer.level = Level.calculateLevelFromExp(iExp);
        aPlayer.setExp(iExp);
    }

    private static void mute(AlphaPlayer aPlayer, String uuid, Jedis jedis) {
        String mute;
        mute = jedis.hget(uuid, "mute");
        if (mute == null) aPlayer.muteExpiration = 0;
        else {
            aPlayer.muteReason = mute.split(":")[0];
            aPlayer.muteExpiration = Integer.parseInt(mute.split(":")[1]);
        }
    }

    private static void chat(AlphaPlayer aPlayer) {
        aPlayer.getData("chatRankCooldownActive").setValue(false);
        chat.calculateChatString(aPlayer);

        ArrayList<NameFormat> nameFormats = new ArrayList<>();
        final List<String> nameStrings = aPlayer.getListSetting("nameFormat");
        nameFormats.addAll(nameStrings.stream().map(NameFormat::valueOf).collect(Collectors.toList()));
        if (nameFormats.isEmpty()) {
            nameFormats.add(NameFormat.WHITE);
            nameStrings.add(NameFormat.WHITE.name());
            aPlayer.setSetting("nameFormat", Settings.listToString(nameStrings));
        }
        aPlayer.getData("nameFormat").setValue(nameFormats);
    }

    private static void friends(AlphaPlayer aPlayer, String uuid, Jedis jedis) {
        List<String> friendsStrings = jedis.lrange("friends:" + uuid, 0, -1);
        Map<String, String> sentStrings = jedis.hgetAll("sentFriendRequests:" + uuid);
        Map<String, String> receivedStrings = jedis.hgetAll("receivedFriendRequests:" + uuid);
        if (friendsStrings == null) friendsStrings = new ArrayList<>();
        Friends.getFriendsObject(aPlayer).addAll(friendsStrings.stream().map(UUID::fromString).collect(Collectors.toList()));
        // invites sent by the player
        if (sentStrings == null) sentStrings = new HashMap<>();
        for (String senderUuid : sentStrings.keySet()) {
            int timeSent = Integer.parseInt(sentStrings.get(senderUuid));
            int currentTime = (int) Instant.now().getEpochSecond();
            int timeElapsed = currentTime - timeSent;
            if (timeElapsed >= 60 * 5) {
                jedis.hdel("sentFriendRequests:" + uuid, senderUuid);
                continue;
            }
            Friends.getSentRequestsObject(aPlayer).put(UUID.fromString(senderUuid), (5 * 60) - timeElapsed);
        }
        // invites sent to the player
        if (receivedStrings == null) receivedStrings = new HashMap<>();
        for (String receiverUuid : receivedStrings.keySet()) {
            int timeSent = Integer.parseInt(receivedStrings.get(receiverUuid));
            int currentTime = (int) Instant.now().getEpochSecond();
            int timeElapsed = currentTime - timeSent;
            if (timeElapsed >= 60 * 5) {
                jedis.hdel("receivedFriendRequests:" + uuid, receiverUuid);
                continue;
            }
            Friends.getReceivedRequestsObject(aPlayer).put(UUID.fromString(receiverUuid), (5 * 60) - timeElapsed);
        }
    }

    private static void party(AlphaPlayer aPlayer, String uuid, Jedis jedis) {
        String leader = jedis.hget(uuid, "party");
        if (leader != null && leader.length() > 10) {
            List<String> partyMembers = jedis.lrange("partyMembers:" + leader, 0, -1);
            if (partyMembers == null) partyMembers = new ArrayList<>();
            if (leader.equals(uuid) && !partyMembers.isEmpty()) {
                // if player is the party leader and not everyone has left the party
                PartyManager.safelyAddPartyMember(aPlayer, UUID.fromString(leader));
                for (String member : partyMembers)
                    PartyManager.safelyAddPartyMember(aPlayer, UUID.fromString(member));
            } else if (jedis.hget(leader, "party") != null && jedis.hget(leader, "party").equals(leader)) {
                // if the leader is still the leader of the party he's in
                if (partyMembers.contains(uuid)) {
                    // if the player is still in the leader's party
                    PartyManager.safelyAddPartyMember(aPlayer, UUID.fromString(leader));
                    for (String member : partyMembers)
                        PartyManager.safelyAddPartyMember(aPlayer, UUID.fromString(member));
                } else jedis.hdel(uuid, "party");
            } else {
                jedis.hdel(uuid, "party");
                jedis.del("partyMembers:" + uuid);
            }
        }
        Map<String, String> sentStrings = jedis.hgetAll("sentPartyInvites:" + aPlayer.uuid);
        Map<String, String> receivedStrings = jedis.hgetAll("receivedPartyInvites:" + aPlayer.uuid);
        // invites sent by the player
        if (sentStrings == null) sentStrings = new HashMap<>();
        for (String receiverUuid : sentStrings.keySet()) {
            int timeSent = Integer.parseInt(sentStrings.get(receiverUuid));
            int currentTime = (int) Instant.now().getEpochSecond();
            int timeElapsed = currentTime - timeSent;
            if (timeElapsed >= 60 * 5) {
                jedis.hdel("sentPartyInvites:" + uuid, receiverUuid);
                continue;
            }
            PartyManager.getSentInvitesObject(aPlayer).put(UUID.fromString(receiverUuid), (5 * 60) - timeElapsed);
        }
        // invites sent to the player
        if (receivedStrings == null) receivedStrings = new HashMap<>();
        for (String senderUuid : receivedStrings.keySet()) {
            int timeSent = Integer.parseInt(receivedStrings.get(senderUuid));
            int currentTime = (int) Instant.now().getEpochSecond();
            int timeElapsed = currentTime - timeSent;
            if (timeElapsed >= 60 * 5) {
                jedis.hdel("receivedPartyInvites:" + uuid, senderUuid);
                continue;
            }
            PartyManager.getReceivedInvitesObject(aPlayer).put(UUID.fromString(senderUuid), (5 * 60) - timeElapsed);
        }
    }

    private static void referrals(AlphaPlayer aPlayer, String uuid, Jedis jedis) {
        List<String> acceptedStrings = jedis.lrange("acceptedReferrals:" + uuid, 0, -1);
        List<String> sentStrings = jedis.lrange("sentReferrals:" + uuid, 0, -1);
        List<String> receivedStrings = jedis.lrange("receivedReferrals:" + uuid, 0, -1);
        String referrerSetting = jedis.hget("settings:" + uuid, "referrer");
        if (referrerSetting != null && !referrerSetting.isEmpty()) {
            aPlayer.getData("isReferred").setValue(true);
            aPlayer.getData("referrer").setValue(referrerSetting);
        } else aPlayer.getData("isReferred").setValue(false);
        aPlayer.getData("acceptedReferrals").setValue(acceptedStrings.stream().map(UUID::fromString).collect(Collectors.toList()));
        aPlayer.getData("sentReferrals").setValue(sentStrings.stream().map(UUID::fromString).collect(Collectors.toList()));
        aPlayer.getData("receivedReferrals").setValue(receivedStrings.stream().map(UUID::fromString).collect(Collectors.toList()));
    }

    private static void treasure(AlphaPlayer aPlayer, String uuid, Jedis jedis) {
        List<String> mini = jedis.lrange("miniChests:" + uuid, 0, -1);
        List<String> epic = jedis.lrange("epicChests:" + uuid, 0, -1);
        List<String> legendary = jedis.lrange("legendaryChests:" + uuid, 0, -1);
        int expiredMini = 0;
        for (String chest : mini) {
            int expiration = Integer.parseInt(chest) - TimeUtils.getCurrent();
            if (expiration <= 0) {
                expiredMini++;
                jedis.lrem("miniChests:" + uuid, 1, chest);
            } else aPlayer.miniChests.add(chest);
        }
        if (expiredMini == 1) {
            Notifications.sendNotification(uuid, "A Mini Chest has expired.",
                    60 * 60 * 24 * 30 * 6);
        } else if (expiredMini > 0) {
            Notifications.sendNotification(uuid, expiredMini +
                    " Mini Chest have expired.", 60 * 60 * 24 * 30 * 6);
        }
        int expiredEpic = 0;
        for (String chest : epic) {
            int expiration = Integer.parseInt(chest) - TimeUtils.getCurrent();
            if (expiration <= 0) {
                expiredEpic++;
                jedis.lrem("epicChests:" + uuid, 1, chest);
            } else aPlayer.epicChests.add(chest);
        }
        if (expiredEpic == 1) {
            Notifications.sendNotification(uuid, "An Epic Chest has expired.",
                    60 * 60 * 24 * 30 * 6);
        } else if (expiredEpic > 0) {
            Notifications.sendNotification(uuid, expiredEpic +
                    " Epic Chests have expired.", 60 * 60 * 24 * 30 * 6);
        }
        int expiredLegendary = 0;
        for (String chest : legendary) {
            int expiration = Integer.parseInt(chest) - TimeUtils.getCurrent();
            if (expiration <= 0) {
                expiredLegendary++;
                jedis.lrem("legendaryChests:" + uuid, 1, chest);
            } else aPlayer.legendaryChests.add(chest);
        }
        if (expiredLegendary == 1) {
            Notifications.sendNotification(uuid, "A Legendary Chest has expired.",
                    60 * 60 * 24 * 30 * 6);
        } else if (expiredLegendary > 0) {
            Notifications.sendNotification(uuid, expiredLegendary +
                    " Legendary Chests have expired.", 60 * 60 * 24 * 30 * 6);
        }
        // check for chests that will expire within 24 hours
        int expiringSoon = 0;
        int nextExpiryTime = 0;
        for (String chest : aPlayer.miniChests) {
            int expiryTime = Integer.parseInt(chest);
            if (expiryTime < nextExpiryTime) nextExpiryTime = expiryTime;
            if (expiryTime <= TimeUtils.nDaysLater(1)) expiringSoon++;
        }
        for (String chest : aPlayer.epicChests) {
            int expiryTime = Integer.parseInt(chest);
            if (expiryTime < nextExpiryTime) nextExpiryTime = expiryTime;
            if (expiryTime <= TimeUtils.nDaysLater(1)) expiringSoon++;
        }
        for (String chest : aPlayer.legendaryChests) {
            int expiryTime = Integer.parseInt(chest);
            if (expiryTime < nextExpiryTime) nextExpiryTime = expiryTime;
            if (expiryTime <= TimeUtils.nDaysLater(1)) expiringSoon++;
        }
        if (Notifications.viewedNotificationsOnLastLogin(uuid, jedis)) {
            // notify player if he has any chests expiring soon
            if (expiringSoon == 1) {
                Notifications.sendNotification(uuid, "You have one Treasure Chest expiring " +
                                "in less than 24 hours. Open it using the chest in hub that " +
                                "says \"Open Treasure\"",
                        60 * 60 * 24 * 30 * 6);
            } else if (expiringSoon > 1) {
                Notifications.sendNotification(uuid, "You have " + expiringSoon + " Treasure Chests " +
                                "expiring in less than 24 hours. Open them using the chest in hub that " +
                                "says \"Open Treasure\"",
                        60 * 60 * 24 * 30 * 6);
            }
        }
        // schedule runnable to expire chests every minute
        aPlayer.getData("nextExpiryTime").setValue(nextExpiryTime);
        new BukkitRunnable() {
            @Override
            public void run() {
                if (aPlayer.getPlayer() == null || !aPlayer.getPlayer().isOnline()) {
                    cancel();
                    return;
                }
                int nextExpiryTime = (int) aPlayer.getData("nextExpiryTime").getValue();
                if (nextExpiryTime < TimeUtils.getCurrent()) return;
                nextExpiryTime = 0;
                final int now = TimeUtils.getCurrent();
                for (String chest : new ArrayList<>(aPlayer.miniChests)) {
                    int expiryTime = Integer.parseInt(chest);
                    if (expiryTime <= now) {
                        aPlayer.miniChests.remove(chest);
                        aPlayer.getPlayer().sendMessage(chat.color(chat.getServer() +
                                "&aA &eMini Chest &ajust expired because you haven't " +
                                "opened it! Make sure to open all your other chests " +
                                "in hub."));
                    } else if (expiryTime < nextExpiryTime) nextExpiryTime = expiryTime;
                }
                for (String chest : new ArrayList<>(aPlayer.epicChests)) {
                    int expiryTime = Integer.parseInt(chest);
                    if (expiryTime <= now) {
                        aPlayer.epicChests.remove(chest);
                        aPlayer.getPlayer().sendMessage(chat.color(chat.getServer() +
                                "&aAn &eEpic Chest &ajust expired because you haven't " +
                                "opened it! Make sure to open all your other chests " +
                                "in hub."));
                    } else if (expiryTime < nextExpiryTime) nextExpiryTime = expiryTime;
                }
                for (String chest : new ArrayList<>(aPlayer.legendaryChests)) {
                    int expiryTime = Integer.parseInt(chest);
                    if (expiryTime <= now) {
                        aPlayer.legendaryChests.remove(chest);
                        aPlayer.getPlayer().sendMessage(chat.color(chat.getServer() +
                                "&aA &eLegendary Chest &ajust expired because you haven't " +
                                "opened it! Make sure to open all your other chests " +
                                "in hub."));
                    } else if (expiryTime < nextExpiryTime) nextExpiryTime = expiryTime;
                }
                aPlayer.getData("nextExpiryTime").setValue(nextExpiryTime);
            }
        }.runTaskTimer(ServerLink.plugin, 20 * 60, 20 * 60);
    }

    // This increments play time by 30 every 30 seconds.
    // Play time is used by the treasure system to give chests every so often.
    private static void playTime(AlphaPlayer aPlayer, Jedis jedis) {
        if (ServerLink.getServerType() == ServerType.hub) return;
        String playTime = jedis.hget(aPlayer.getUuid().toString(), "playTime");
        if (playTime == null) aPlayer.getData("playTime").setValue(0);
        else aPlayer.getData("playTime").setValue(Integer.parseInt(playTime));
        new BukkitRunnable() {
            @Override
            public void run() {
                if (aPlayer.getPlayer() == null || !aPlayer.getPlayer().isOnline()) {
                    cancel();
                    return;
                }
                StatsManager.incrIncreasingStat(aPlayer.getUuid(), "playTime", 0.0055555);
                StatsManager.incrIncreasingStat(aPlayer.getUuid(), ServerLink.getServerType() + "PlayTime", 0.0055555);
                Integer playTime = (int) aPlayer.getData("playTime").getValue() + 30;
                aPlayer.getData("playTime").setValue(playTime);
                Jedis j2 = JedisPool.getConn();
                j2.hset(aPlayer.getUuid().toString(), "playTime", playTime.toString());
                JedisPool.close(j2);
                // 40% chance to give a chest every 20 minutes of play
                if (playTime >= 60 * 20) {
                    aPlayer.getData("playTime").setValue(0);
                    j2 = JedisPool.getConn();
                    j2.hset(aPlayer.getUuid().toString(), "playTime", "0");
                    JedisPool.close(j2);
                    if (Probability.get(40)) {
                        double chance = Probability.selector();
                        if (chance <= 5) TreasureManager.findLegendary(aPlayer);
                        else if (chance <= 25) TreasureManager.findEpic(aPlayer);
                        else TreasureManager.findMini(aPlayer);
                    }
                }
            }
        }.runTaskTimerAsynchronously(ServerLink.plugin, 20 * 30, 20 * 30);
    }

    private static void loadSettings(AlphaPlayer aPlayer, String uuid, Jedis jedis) {
        // if one is null, assume all are null. if one is not null, assume all are not null
        String key = "settings:" + uuid;
        String s1 = jedis.hget(key, "chatMessagesEnabled");
        if (s1 == null) {
            jedis.hset(key, "chatMessagesEnabled", "true");
            aPlayer.settings.put("chatMessagesEnabled", "true");

            jedis.hset(key, "privateMessagesEnabled", "true");
            aPlayer.settings.put("privateMessagesEnabled", "true");

            jedis.hset(key, "chatMentionsEnabled", "true");
            aPlayer.settings.put("chatMentionsEnabled", "true");

            jedis.hset(key, "partyInvitesEnabled", "true");
            aPlayer.settings.put("partyInvitesEnabled", "true");

            jedis.hset(key, "friendRequestsEnabled", "true");
            aPlayer.settings.put("friendRequestsEnabled", "true");

            jedis.hset(key, "playersShownInHub", "300");
            aPlayer.settings.put("playersShownInHub", "300");

            jedis.hset(key, "nameFormat", "");
            aPlayer.settings.put("nameFormat", "");

            jedis.hset(key, "chatFormat", "");
            aPlayer.settings.put("chatFormat", "");

            jedis.hset(key, "speedSetting", "1");
            aPlayer.getData("speedSetting").setValue(1);
            aPlayer.settings.put("speedSetting", "1");
        } else {
            String s2 = jedis.hget(key, "privateMessagesEnabled");
            String s3 = jedis.hget(key, "chatMentionsEnabled");
            String s4 = jedis.hget(key, "partyInvitesEnabled");
            String s5 = jedis.hget(key, "friendRequestsEnabled");
            String s6 = jedis.hget(key, "playersShownInHub");
            String s7 = jedis.hget(key, "speedSetting");
            String s8 = jedis.hget(key, "nameFormat");
            String s9 = jedis.hget(key, "chatFormat");
            aPlayer.settings.put("chatMessagesEnabled", s1);
            aPlayer.settings.put("privateMessagesEnabled", s2);
            aPlayer.settings.put("chatMentionsEnabled", s3);
            aPlayer.settings.put("partyInvitesEnabled", s4);
            aPlayer.settings.put("friendRequestsEnabled", s5);
            aPlayer.settings.put("playersShownInHub", s6);
            aPlayer.settings.put("speedSetting", s7);
            aPlayer.getData("speedSetting").setValue(Integer.parseInt(s7));
            aPlayer.settings.put("nameFormat", s8);
            aPlayer.settings.put("chatFormat", s9);
        }
    }

    // show notifications in 2 seconds, delete them from redis 3 seconds after showing them only if the player is still online
    private static void notifications(AlphaPlayer aPlayer, Jedis jedis) {
        List<String> notifications = Notifications.getNotifications(aPlayer.getUuid().toString(), jedis);
        if (notifications.isEmpty()) return;
        jedis.hset(aPlayer.getUuid().toString(), "viewedNotificationsOnLastLogin", "false");
        new BukkitRunnable() {
            @Override
            public void run() {
                Player p = aPlayer.getPlayer();
                if (p == null || !p.isOnline()) return;
                p.sendMessage("");
                p.sendMessage(chat.color("&2    ------- &6&lNotifications&r&2  -------"));
                for (String s : notifications) {
                    if (s.startsWith("F@nS^~")) {
                        String msg = s.substring(6);
                        if (msg.startsWith("[")) CommandText.sendMessage(p, "[\"-\"," + msg.substring(1));
                        else CommandText.sendMessage(p, "[\"- \"," + msg + "]");
                    } else aPlayer.getPlayer().sendMessage("- " + chat.color(s));
                }
                p.sendMessage(chat.color("&2    ------- ------ ------ ------"));
                p.sendMessage("");
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        if (!p.isOnline()) return;
                        Jedis jedis = JedisPool.getConn();
                        jedis.hset(p.getUniqueId().toString(), "viewedNotificationsOnLastLogin", "true");
                        jedis.del("notifications:" + p.getUniqueId().toString());
                        JedisPool.close(jedis);
                    }
                }.runTaskLaterAsynchronously(ServerLink.plugin, 60);
            }
        }.runTaskLaterAsynchronously(ServerLink.plugin, 40);
    }

    private static void powerclassStatCache(AlphaPlayer aPlayer) {
        for (Class cl : ClassManager.getClasses()) {
            if (cl instanceof PowerClass)
                ((PowerClass) cl).addToStatCache(aPlayer);
        }
    }
}

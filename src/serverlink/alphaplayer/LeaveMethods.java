package serverlink.alphaplayer;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.classes.Class;
import serverlink.classes.ClassManager;
import serverlink.customevent.CustomLeaveEvent;
import serverlink.inventory.InventoryManager;
import serverlink.network.JedisPool;
import serverlink.npc.NpcEventListener;
import serverlink.player.Afk;
import serverlink.player.Permissions;
import serverlink.player.damage.PlayerDamage;
import serverlink.player.damage.PlayerDamageEvents;
import serverlink.player.interaction.PlayerInteraction;
import serverlink.player.team.TeamColor;
import serverlink.player.team.TeamManager;
import serverlink.server.ServerType;
import serverlink.util.ConsoleOutput;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

class LeaveMethods {
    static void leave(AlphaPlayer aPlayer) {
        if (aPlayer.fullyLoggedIn) logout(aPlayer, aPlayer.getPlayer());
        else {
            long startTime = System.currentTimeMillis();
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (aPlayer.fullyLoggedIn || startTime + 1000 < System.currentTimeMillis()) {
                        logout(aPlayer, Bukkit.getPlayer(aPlayer.getUuid()));
                        cancel();
                    }
                }
            }.runTaskTimer(ServerLink.plugin, 2, 2);
        }
    }

    private static void logout(AlphaPlayer aPlayer, Player player) {
        if (player == null) return;
        try {
            Bukkit.getPluginManager().callEvent(new CustomLeaveEvent(player));
            PlayerManager.onlineAlphaPlayers.remove(aPlayer);
            String messages = chat.getJoinLeaveMessagesStatus();
            if (!messages.equals("none")) {
                if (messages.equals("all") || (messages.equals("donors") && aPlayer.getRank() > 1))
                    if (aPlayer.getRank() > 1)
                        Bukkit.getServer().broadcastMessage(chat.color("&8<&c&lQuit&r&8> " +
                                Permissions.getRankName(aPlayer.getRank()) + "&r " + player.getDisplayName()));
                    else
                        Bukkit.getServer().broadcastMessage(chat.color("&8<&c&lQuit&r&8> &r" + player.getDisplayName()));
            }
            PlayerDamageEvents.lastDamager.remove(player);
            PlayerDamageEvents.lastDamagerTimeout.remove(player);
            PlayerDamageEvents.lastAssisters.remove(player);
            if (aPlayer.getTeam() != null) {
                TeamManager.teamWaitLists.get(aPlayer.getTeam().getTeamColor()).remove(player);
                // Update deficits and try to allow someone into the leaving player's team
                TeamManager.updateTeamDeficits();
                List<TeamColor> teamsWithWaitLists = TeamManager.teamWaitLists.keySet().stream().filter(teamColor -> !TeamManager.teamWaitLists.get(teamColor).isEmpty()).collect(Collectors.toList());
                if (!teamsWithWaitLists.isEmpty()) {
                    Collections.shuffle(teamsWithWaitLists);
                    for (TeamColor teamColor : teamsWithWaitLists) {
                        Player waiter = TeamManager.teamWaitLists.get(teamColor).get(0);
                        if (TeamManager.getTeamDeficit(TeamManager.getTeam(waiter).getTeamColor())
                                >= TeamManager.getTeamDeficit(teamColor)) continue;
                        TeamManager.teamWaitLists.get(teamColor).remove(waiter);
                        PlayerManager.get(waiter).setTeam(TeamManager.getTeam(teamColor));
                        PlayerManager.get(waiter).updateNameFormatting();
                        break;
                    }
                }
                aPlayer.getTeam().getPlayers().remove(player);
            }
            Object classObject = aPlayer.getData("class").getValue();
            if (classObject != null) {
                ClassManager.unEquipClass(player, (Class) classObject);
            }
            aPlayer.removeRankTag();
            aPlayer.alphaBoard.leave();
            hideAndShowPlayers(player);
            InventoryManager.leave(player);
            Afk.leave(player);
            PlayerInteraction.leave(player);
            PlayerDamage.leave(player);
            NpcEventListener.cooldowns.remove(player);
            new BukkitRunnable() {
                @Override
                public void run() {
                    Jedis jedis = JedisPool.getConn();
                    points(aPlayer, jedis);
                    unsyncedSettings(aPlayer, jedis);
                    JedisPool.close(jedis);
                }
            }.runTaskAsynchronously(ServerLink.plugin);
        } catch (Exception e) {
            ConsoleOutput.printError("Player logout produced an error: " + Arrays.toString(e.getStackTrace()));
        }
        LogoutQueue.add(aPlayer.getUuid().toString());
    }

    private static void points(AlphaPlayer aPlayer, Jedis jedis) {
        jedis.hset(aPlayer.getUuid().toString(), "gainedPoints", aPlayer.getGainedPoints() + ":"
                + aPlayer.getNextExpCheckTime());
    }

    private static void unsyncedSettings(AlphaPlayer aPlayer, Jedis jedis) {
        for (String setting : aPlayer.unsyncedSettings) {
            if (aPlayer.settings.get(setting) == null)
                jedis.hdel("settings:" + aPlayer.getUuid().toString(), setting);
            else jedis.hset("settings:" + aPlayer.getUuid().toString(), setting, aPlayer.settings.get(setting));
        }
    }

    private static void hideAndShowPlayers(Player player) {
        if (ServerLink.getServerType() != ServerType.hub) return;
        for (AlphaPlayer p2 : PlayerManager.getOnlineAlphaPlayers()) {
            if (!p2.getPlayer().canSee(player)) continue;
            // p2 could see p1; check to show another player in his place
            Integer playersCurrentlyShown = (Integer) p2.getData("playersCurrentlyShown").getValue();
            Integer maxPlayersToShow = p2.getIntegerSetting("playersShownInHub");
            playersCurrentlyShown--;
            p2.getData("playersCurrentlyShown").setValue(playersCurrentlyShown);
            if (playersCurrentlyShown < maxPlayersToShow) {
                // try to find someone to show to p2
                for (AlphaPlayer p3 : PlayerManager.getOnlineAlphaPlayers()) {
                    if (p3 == p2) continue;
                    if (!p2.getPlayer().canSee(p3.getPlayer())) {
                        p2.getPlayer().showPlayer(p3.getPlayer());
                        p3.showRankTagTo(p2.getPlayer());
                        break;
                    }
                }
            }
        }
    }
}

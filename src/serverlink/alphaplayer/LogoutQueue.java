package serverlink.alphaplayer;

import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.network.JedisPool;
import serverlink.network.navail;
import serverlink.network.ncount;

import java.util.ArrayList;
import java.util.List;

public class LogoutQueue {
    private final static List<String> queue = new ArrayList<>();

    public static void setup() {
        new BukkitRunnable() {
            @Override
            public void run() {
                synchronized (queue) {
                    final int size = queue.size();
                    for (int i = 0; i < size; i++) {
                        String uuid = queue.get(0);
                        queue.remove(uuid);
                        Jedis jedis = JedisPool.getConn();
                        String online = jedis.hget(uuid, "online");
                        if (online != null && online.equals("true")) {
                            jedis.hset(uuid, "online", "false");
                            ncount.decrCount();
                            if (ncount.getCount() < ncount.getMaxPlayers() && navail.isClosed()) navail.setOpen();
                        }
                        JedisPool.close(jedis);
                    }
                }
            }
        }.runTaskTimerAsynchronously(ServerLink.plugin, 5, 4);
    }

    static void add(String uuid) {
        synchronized (queue) {
            queue.add(uuid);
        }
    }
}

package serverlink.alphaplayer;

import me.libraryaddict.disguise.events.DisguiseEvent;
import me.libraryaddict.disguise.events.UndisguiseEvent;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;

public class DisguiseListener implements Listener {
    @EventHandler(priority = EventPriority.HIGHEST)
    private void unDisguise(UndisguiseEvent e) {
        if (e.isCancelled()) return;
        if (!(e.getEntity() instanceof Player)) return;
        PlayerManager.get((Player) e.getEntity()).updateRankTag();
    }

    @EventHandler(priority = EventPriority.HIGHEST)
    private void disguise(DisguiseEvent e) {
        if (e.isCancelled()) return;
        if (!(e.getEntity() instanceof Player)) return;
        PlayerManager.get((Player) e.getEntity()).removeRankTag();
    }
}

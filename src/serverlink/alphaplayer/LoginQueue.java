package serverlink.alphaplayer;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.network.JedisPool;
import serverlink.network.ncount;
import serverlink.util.ConsoleOutput;
import serverlink.util.Slack;

import java.util.HashMap;

public class LoginQueue {
    private static final Object lock = new Object();
    private static HashMap<AlphaPlayer, Integer> queue = new HashMap<>();
    private static HashMap<AlphaPlayer, Player> players = new HashMap<>();

    public static void setup() {
        new BukkitRunnable() {
            @Override
            public void run() {
                HashMap<AlphaPlayer, Integer> queueClone;
                synchronized (lock) {
                    queueClone = new HashMap<>(queue);
                }
                int processedPlayers = 0;
                for (AlphaPlayer aPlayer : queueClone.keySet()) {
                    if (aPlayer == null) continue;
                    if (processedPlayers == 2) break;
                    processedPlayers++;
                    if (aPlayer.preLoggedIn.get()) {
                        synchronized (lock) {
                            Player player = players.get(aPlayer);
                            if (player == null) {
                                ConsoleOutput.printError("PLAYERS LIST REMOVED PLAYER BEFORE LOGIN FINISHED!!!");
                            }
                            queue.remove(aPlayer);
                            players.remove(aPlayer);
                            // If the server is under heavy load, the player could login
                            // and logout while waiting in the queue for everyone else
                            // to be logged in. In this case, he would be set as "offline"
                            // in the db within a second of logging out (see LogoutQueue),
                            // so we wouldn't want to set him as "online" or process him
                            // as if he were on the server.
                            if (!player.isOnline()) continue;
                            ncount.incrCount();
                            Jedis jedis = JedisPool.getConn();
                            jedis.hset(aPlayer.uuid.toString(), "online", "true");
                            jedis.hset(aPlayer.uuid.toString(), "server", ServerLink.getServerName());
                            JedisPool.close(jedis);
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    aPlayer.join(player);
                                }
                            }.runTask(ServerLink.plugin);
                        }
                    } else {
                        int check = queueClone.get(aPlayer);
                        if (check == 15) {
                            synchronized (lock) {
                                Player player = players.get(aPlayer);
                                Slack.sendPost("#errors", ServerLink.getServerName() + ": " +
                                        player.getName() + " was kicked because his/her data didn't " +
                                        "load fast enough (the database could be down or the server's " +
                                        "network could be overloaded).");
                                new BukkitRunnable() {
                                    @Override
                                    public void run() {
                                        player.kickPlayer(
                                                "Couldn't load your data fast enough (our network is having trouble).");
                                    }
                                }.runTask(ServerLink.plugin);
                                queue.remove(aPlayer);
                                players.remove(aPlayer);
                                continue;
                            }
                        }
                        synchronized (lock) {
                            queue.put(aPlayer, check + 1);
                        }
                    }
                }
            }
        }.runTaskTimerAsynchronously(ServerLink.plugin, 4, 4);
    }

    static void add(AlphaPlayer aPlayer, final Player player) {
        synchronized (lock) {
            queue.put(aPlayer, 0);
            if (player == null) {
                ConsoleOutput.printError("INPUTTING NULL PLAYER TO LOGIN!!!");
            }
            players.put(aPlayer, player);
        }
    }
}

package serverlink.alphaplayer;

import com.gmail.filoghost.holographicdisplays.api.Hologram;
import com.gmail.filoghost.holographicdisplays.api.HologramsAPI;
import com.sun.istack.internal.NotNull;
import com.sun.istack.internal.Nullable;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.permissions.PermissionAttachment;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.Scoreboard;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.bot.BotManager;
import serverlink.chat.ChatFormat;
import serverlink.chat.NameFormat;
import serverlink.chat.chat;
import serverlink.chat.nametag.Nametag;
import serverlink.classes.ClassManager;
import serverlink.classes.PowerClass;
import serverlink.classes.Weapon;
import serverlink.customevent.CustomFullyJoinEvent;
import serverlink.inventory.InventoryGui;
import serverlink.network.JedisPool;
import serverlink.network.nstatus;
import serverlink.player.*;
import serverlink.player.damage.PlayerDamageEvents;
import serverlink.player.stats.StatsManager;
import serverlink.player.team.Team;
import serverlink.player.team.TeamManager;
import serverlink.server.GametypeBooster;
import serverlink.server.Messages;
import serverlink.util.ConsoleOutput;
import serverlink.util.Probability;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

/**
 * You can call any of these methods synchronously or asynchronously (except getTeam), but do not
 * asynchronously iterate over or update lists and maps that are returned from these methods.
 */
public class AlphaPlayer {
    public final HashMap<String, String> settings = new HashMap<>();
    final List<String> unsyncedSettings = new ArrayList<>();
    private final HashMap<String, Data> data = new HashMap<>();
    private final List<Weapon> weapons = new ArrayList<>();
    private final List<InventoryGui> guis = new ArrayList<>();
    private final HashMap<ItemStack, Runnable> hotbarItems = new HashMap<>();
    public List<String> miniChests = new ArrayList<>();
    public List<String> epicChests = new ArrayList<>();
    public List<String> legendaryChests = new ArrayList<>();
    PermissionAttachment attachment;
    AtomicBoolean preLoggedIn = new AtomicBoolean(false);
    Boolean fullyLoggedIn = false;
    UUID uuid;
    int rank = 1;
    int points = 0;
    int gainedPoints = 0;
    int nextExpCheckTime = 0;
    int coins = 0;
    int level = 1;
    int muteExpiration = 0;
    String muteReason = "";
    ArrayList<String> allowedPerms = new ArrayList<>();
    ArrayList<String> deniedPerms = new ArrayList<>();
    AlphaScoreboard alphaBoard;
    private double rankTagHeight = 2.6;
    private int exp;
    private String nametagPrefix = "";
    private String nametagSuffix = "";
    private Player player;
    private Hologram hologram;
    private BukkitRunnable rankTagRunnable = new BukkitRunnable() {
        @Override
        public void run() {
            hologram.teleport(player.getLocation().add(0, rankTagHeight, 0));
        }
    };
    private boolean rankTagRunning = false;

    public AlphaPlayer(UUID uuid) {
        this.uuid = uuid;
        PlayerManager.alphaPlayers.put(uuid, this);
        PreJoinMethods.preJoin(AlphaPlayer.this, uuid.toString());
    }

    // for bots
    public AlphaPlayer(UUID uuid, Player player, boolean announceBotJoin) {
        PlayerManager.alphaPlayers.put(uuid, this);
        PlayerManager.onlineAlphaPlayers.add(this);
        this.player = player;
        // set uuid to a nonexistent one so it doesn't mess up the database
        this.uuid = UUID.fromString("9e45aea6-5fe8-2ffb-9632-a56b0b54779e");
        attachment = player.addAttachment(ServerLink.plugin);
        getAttachment().setPermission("rank.1", true);
        settings.put("chatMessagesEnabled", "true");
        settings.put("privateMessagesEnabled", "true");
        settings.put("chatMentionsEnabled", "false");
        settings.put("partyInvitesEnabled", "false");
        settings.put("friendRequestsEnabled", "false");
        settings.put("playersShownInHub", "300");
        settings.put("kitpvp:lastClass", ClassManager.getDefaultClass().getName());
        getData("class").setValue(ClassManager.getDefaultClass());
        getData("chatRankCooldownActive").setValue(false);
        getData("progressexp", new HashMap<>());
        getData("progresslvlups", new HashMap<>());
        getData("chatRankCooldownActive").setValue(false);
        JoinMethods.hideAndShowPlayers(this, player);
        SplittableRandom r = new SplittableRandom();
        rank = BotManager.botUsers.get(player.getName());
        exp = BotManager.botExp.get(player.getName());
        // add exp as if bots play afk in games an hour a day
        long daysPlayedAfterBotReachedLvl = (long) ((System.currentTimeMillis() - (long) 1482798295639.0) * (1.0 / (1000.0 * 60.0 * 60 * 24.0)));
        exp += daysPlayedAfterBotReachedLvl * 120;
        level = Level.calculateLevelFromExp(exp);
        if (rank == 5) {
            // chat
            if (r.nextBoolean()) settings.put("chatFormat", Settings.listToString(ChatFormat.RED.name()));
            else if (r.nextBoolean()) settings.put("chatFormat", Settings.listToString(ChatFormat.BLUE.name()));
            else settings.put("chatFormat", Settings.listToString(ChatFormat.AQUA.name()));
            // name
            if (r.nextBoolean()) settings.put("nameFormat", Settings.listToString(NameFormat.RAINBOW.name()));
            else if (r.nextBoolean()) settings.put("nameFormat", Settings.listToString(NameFormat.RED.name()));
            else settings.put("nameFormat", Settings.listToString(NameFormat.BLUE.name()));
        } else if (rank == 4) {
            // chat
            if (r.nextBoolean()) settings.put("chatFormat", Settings.listToString(ChatFormat.BLUE.name()));
            else if (r.nextBoolean()) settings.put("chatFormat", Settings.listToString(ChatFormat.AQUA.name()));
            else settings.put("chatFormat", Settings.listToString(ChatFormat.WHITE.name()));
            // name
            if (r.nextBoolean()) settings.put("nameFormat", Settings.listToString(NameFormat.BLUE.name()));
            else if (r.nextBoolean()) settings.put("nameFormat", Settings.listToString(NameFormat.LIGHT_PURPLE.name()));
            else if (r.nextBoolean()) settings.put("nameFormat", Settings.listToString(NameFormat.GREEN.name()));
        } else if (rank == 3) {
            // chat
            if (r.nextBoolean()) settings.put("chatFormat", Settings.listToString(ChatFormat.AQUA.name()));
            else settings.put("chatFormat", Settings.listToString(ChatFormat.WHITE.name()));
            // name
            if (r.nextBoolean()) settings.put("nameFormat", Settings.listToString(NameFormat.LIGHT_PURPLE.name()));
            else if (r.nextBoolean()) settings.put("nameFormat", Settings.listToString(NameFormat.AQUA.name()));
        } else if (rank == 2) {
            // chat
            settings.put("chatFormat", Settings.listToString(ChatFormat.WHITE.name()));
            // name
            if (Probability.get(75)) settings.put("nameFormat", Settings.listToString(NameFormat.GREEN.name()));
        }
        chat.calculateChatString(this);
        updateNameFormatting();
        PlayerDamageEvents.lastAssisters.put(player, new HashMap<>());
        ClassManager.getClasses().stream().filter(cl -> cl instanceof PowerClass).forEach(cl -> {
            ((PowerClass) cl).addToStatCache(this);
            double exp = r.nextDouble(7000);
            getData(ServerLink.getServerType() + ":classExp" + cl.getName()).setValue(exp);
            getData(ServerLink.getServerType() + ":classLevel" + cl.getName()).setValue(((PowerClass) cl).calculateLevelFromExp(exp));
        });
        alphaBoard = new AlphaScoreboard(player) {
            @Override
            public Scoreboard getBoard() {
                return super.getBoard();
            }

            @Override
            public void setSidebar(String title, ArrayList<String> body) {
            }

            @Override
            public void removeLine(int line) {
            }

            @Override
            public void addLine(String value) {
            }

            @Override
            public void setLine(int line, String value) {
            }

            @Override
            public void setTitle(String title) {
            }

            @Override
            public void leave() {
            }
        };
        String messages = chat.getJoinLeaveMessagesStatus();
        if (announceBotJoin && !messages.equals("none")) {
            if (messages.equals("all") || (messages.equals("donors") && rank > 1))
                if (rank > 1)
                    Bukkit.getServer().broadcastMessage(chat.color("&8<&a&lJoin&r&8> " +
                            Permissions.getRankName(rank) + "&r " + player.getDisplayName()));
                else
                    Bukkit.getServer().broadcastMessage(chat.color("&8<&a&lJoin&r&8> " + player.getDisplayName()));
        }
        Bukkit.getPluginManager().callEvent(new CustomFullyJoinEvent(player));
        fullyLoggedIn = true;
    }

    void join(Player player) {
        this.player = player;
        if (this.player == null) {
            ConsoleOutput.printError("PLAYER IS NULL AFTER JOINING SERVER!!!");
        } else if (!this.player.isOnline()) {
            ConsoleOutput.printError("PLAYER IS OFFLINE AFTER JOINING SERVER!!!");
        }
        if (player == null) this.player = Bukkit.getPlayer(uuid);
        if (this.player == null) {
            ConsoleOutput.printError("PLAYER IS STILL NULL AFTER JOINING SERVER!!!");
            return;
        } else if (!this.player.isOnline()) {
            ConsoleOutput.printError("PLAYER IS STILL OFFLINE AFTER JOINING SERVER!!!");
            return;
        }
        JoinMethods.join(this.player);

        Bukkit.getPluginManager().callEvent(new CustomFullyJoinEvent(player));
        fullyLoggedIn = true;
    }

    public UUID getUuid() {
        return uuid;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int newRank) {
        if (Thread.currentThread() == ServerLink.mainThread) rank = newRank;
        else new BukkitRunnable() {
            @Override
            public void run() {
                rank = newRank;
            }
        }.runTask(ServerLink.plugin);
    }

    public int getPoints() {
        return points;
    }

    public String getPointsString() {
        return Points.formatPoints(points);
    }

    public void addPoints(Integer pointsToAdd) {
        StatsManager.incrIncreasingStat(uuid, ServerLink.getServerType() + "Points", pointsToAdd);
        if (Thread.currentThread() == ServerLink.mainThread) points = points + pointsToAdd;
        else new BukkitRunnable() {
            @Override
            public void run() {
                points = points + pointsToAdd;
            }
        }.runTask(ServerLink.plugin);
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                String currentPoints = jedis.hget(uuid.toString(),
                        ServerLink.getServerType() + "Points");
                if (currentPoints != null)
                    jedis.hset(uuid.toString(), ServerLink.getServerType() + "Points",
                            ((Integer) (pointsToAdd + Integer.parseInt(currentPoints))).toString());
                else
                    jedis.hset(uuid.toString(), ServerLink.getServerType() + "Points", pointsToAdd.toString());
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public void addPoints_HandleMultBoostGainedPoints(Integer pointsToAdd) {
        addPoints((int) (pointsToAdd * getMultiplier() * GametypeBooster.getBoosterMultiplier()));
        setGainedPoints(getGainedPoints() + pointsToAdd);
    }

    public void takePoints(int pointsToTake) {
        if (Thread.currentThread() == ServerLink.mainThread) points = Math.max(0, points - pointsToTake);
        else new BukkitRunnable() {
            @Override
            public void run() {
                points = Math.max(0, points - pointsToTake);
            }
        }.runTask(ServerLink.plugin);
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                String currentPoints = jedis.hget(uuid.toString(),
                        ServerLink.getServerType() + "Points");
                if (currentPoints == null || Integer.parseInt(currentPoints) - pointsToTake < 0)
                    jedis.hset(uuid.toString(), ServerLink.getServerType() + "Points", "0");
                else
                    jedis.hset(uuid.toString(), ServerLink.getServerType() + "Points",
                            ((Integer) (Integer.parseInt(currentPoints) - pointsToTake)).toString());
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public int getGainedPoints() {
        return gainedPoints;
    }

    public void setGainedPoints(int newGainedPoints) {
        if (Thread.currentThread() == ServerLink.mainThread) gainedPoints = newGainedPoints;
        else new BukkitRunnable() {
            @Override
            public void run() {
                gainedPoints = newGainedPoints;
            }
        }.runTask(ServerLink.plugin);
    }

    public int getNextExpCheckTime() {
        return nextExpCheckTime;
    }

    public void setNextExpCheckTime(int newNextExpCheckTime) {
        if (Thread.currentThread() == ServerLink.mainThread) nextExpCheckTime = newNextExpCheckTime;
        else new BukkitRunnable() {
            @Override
            public void run() {
                nextExpCheckTime = newNextExpCheckTime;
            }
        }.runTask(ServerLink.plugin);

    }

    public int getCoins() {
        return coins;
    }

    public String getCoinsString() {
        return Coins.formatCoins(coins);
    }

    public void addCoins(Integer coinsToAdd) {
        StatsManager.incrIncreasingStat(uuid, "coins", coinsToAdd);
        if (Thread.currentThread() == ServerLink.mainThread) coins = coins + coinsToAdd;
        else new BukkitRunnable() {
            @Override
            public void run() {
                coins = coins + coinsToAdd;
            }
        }.runTask(ServerLink.plugin);
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                String currentCoins = jedis.hget(uuid.toString(), "coins");
                if (currentCoins != null)
                    jedis.hset(uuid.toString(), "coins",
                            ((Integer) (coinsToAdd + Integer.parseInt(currentCoins))).toString());
                else
                    jedis.hset(uuid.toString(), "coins", coinsToAdd.toString());
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public void takeCoins(int coinsToTake) {
        if (Thread.currentThread() == ServerLink.mainThread) coins = Math.max(0, coins - coinsToTake);
        else new BukkitRunnable() {
            @Override
            public void run() {
                coins = Math.max(0, coins - coinsToTake);
            }
        }.runTask(ServerLink.plugin);
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                String currentCoins = jedis.hget(uuid.toString(), "coins");
                if (currentCoins == null || Integer.parseInt(currentCoins) - coinsToTake < 0)
                    jedis.hset(uuid.toString(), "coins", "0");
                else
                    jedis.hset(uuid.toString(), "coins",
                            ((Integer) (Integer.parseInt(currentCoins) - coinsToTake)).toString());
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public void reloadCoins() {
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                String coinsString = jedis.hget(uuid.toString(), "coins");
                JedisPool.close(jedis);
                if (coinsString == null || coinsString.isEmpty()) coins = 0;
                else coins = Integer.parseInt(coinsString);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public Double getMultiplier() {
        if (level == 100) return 5d;
        else if (level >= 75) return 4d;
        else if (level >= 50) return 3d;
        else if (level >= 25) return 2d;
        else return 1d;
    }

    /**
     * gets a string with only two decimals (double doesn't have a function that does this)
     */
    public String getMultiplierString() {
        return new BigDecimal(getMultiplier()).setScale(2, BigDecimal.ROUND_HALF_DOWN).toString();
    }

    public List<Weapon> getWeapons() {
        return weapons;
    }

    public void addWeapon(Weapon w) {
        if (Thread.currentThread() == ServerLink.mainThread) if (!weapons.contains(w)) weapons.add(w);
        else new BukkitRunnable() {
                @Override
                public void run() {
                    if (!weapons.contains(w)) weapons.add(w);
                }
            }.runTask(ServerLink.plugin);
    }

    public void removeWeapon(Weapon w) {
        if (Thread.currentThread() == ServerLink.mainThread) weapons.remove(w);
        else new BukkitRunnable() {
            @Override
            public void run() {
                weapons.remove(w);
            }
        }.runTask(ServerLink.plugin);
    }

    public void clearWeapons() {
        if (Thread.currentThread() == ServerLink.mainThread) weapons.clear();
        else new BukkitRunnable() {
            @Override
            public void run() {
                weapons.clear();
            }
        }.runTask(ServerLink.plugin);
    }

    public int getExp() {
        return exp;
    }

    void setExp(int newExp) {
        if (Thread.currentThread() == ServerLink.mainThread) {
            exp = newExp;
            final int oldLvl = level;
            final int newLvl = Level.calculateLevelFromExp(exp);
            for (int i = oldLvl + 1; i <= newLvl; i++) levelUp();
        } else new BukkitRunnable() {
            @Override
            public void run() {
                exp = newExp;
                final int oldLvl = level;
                final int newLvl = Level.calculateLevelFromExp(exp);
                for (int i = oldLvl + 1; i <= newLvl; i++) levelUp();
            }
        }.runTask(ServerLink.plugin);
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                jedis.hset(uuid.toString(), "exp", ((Integer) newExp).toString());
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public void addExp(int expToAdd) {
        StatsManager.incrIncreasingStat(uuid, "exp", expToAdd);
        if (Thread.currentThread() == ServerLink.mainThread) {
            exp = exp + expToAdd;
            final int oldLvl = level;
            final int newLvl = Level.calculateLevelFromExp(exp);
            for (int i = oldLvl + 1; i <= newLvl; i++) {
                levelUp();
            }
        } else new BukkitRunnable() {
            @Override
            public void run() {
                exp = exp + expToAdd;
                final int oldLvl = level;
                final int newLvl = Level.calculateLevelFromExp(exp);
                for (int i = oldLvl + 1; i <= newLvl; i++) {
                    levelUp();
                }
            }
        }.runTask(ServerLink.plugin);
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                jedis.hset(uuid.toString(), "exp", ((Integer) exp).toString());
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    public int getLevel() {
        return level;
    }

    // doesn't need to check to run task on main thread since it is called privately on the main thread
    private void levelUp() {
        level++;
        player.sendMessage(chat.color(Messages.levelUp.replace("%level%", ((Integer) level).toString())));
    }

    public PermissionAttachment getAttachment() {
        return attachment;
    }

    public void updatePermissions(PermissionAttachment attachment, ArrayList<String> allowedPerms, ArrayList<String> deniedPerms) {
        this.attachment = attachment;
        this.allowedPerms = allowedPerms;
        this.deniedPerms = deniedPerms;
    }

    public int getMuteExpiration() {
        return muteExpiration;
    }

    public void setMuteExpiration(int newMuteDuration) {
        if (Thread.currentThread() == ServerLink.mainThread) this.muteExpiration = newMuteDuration;
        else new BukkitRunnable() {
            @Override
            public void run() {
                muteExpiration = newMuteDuration;
            }
        }.runTask(ServerLink.plugin);
    }

    public String getMuteReason() {
        return muteReason;
    }

    public void setMuteReason(String newMuteReason) {
        if (Thread.currentThread() == ServerLink.mainThread) muteReason = newMuteReason;
        else new BukkitRunnable() {
            @Override
            public void run() {
                muteReason = newMuteReason;
            }
        }.runTask(ServerLink.plugin);
    }

    public Player getPlayer() {
        return player;
    }

    public Data getData(String id) {
        id = id.toLowerCase();
        Data d = data.get(id);
        if (d != null) return d;
        d = new Data(id.toLowerCase(), this);
        data.put(id, d);
        return d;
    }

    // if you're getting NPEs use this to set default value if data doesn't exist
    public Data getData(String id, Object defaultValue) {
        id = id.toLowerCase();
        Data d = data.get(id);
        if (d != null && d.getValue() != null) return d;
        d = new Data(id.toLowerCase(), this, defaultValue);
        data.put(id, d);
        return d;
    }

    public List<InventoryGui> getGuis() {
        return guis;
    }

    public void addGui(InventoryGui gui) {
        if (Thread.currentThread() == ServerLink.mainThread) if (!guis.contains(gui)) guis.add(gui);
        else new BukkitRunnable() {
                @Override
                public void run() {
                    if (!guis.contains(gui)) guis.add(gui);
                }
            }.runTask(ServerLink.plugin);
    }

    public void removeGui(InventoryGui gui) {
        if (Thread.currentThread() == ServerLink.mainThread) guis.remove(gui);
        else new BukkitRunnable() {
            @Override
            public void run() {
                guis.remove(gui);
            }
        }.runTask(ServerLink.plugin);
    }

    public List<ItemStack> getHotbarItems() {
        return new ArrayList<>(hotbarItems.keySet());
    }

    /**
     * call from main thread only
     */
    public Runnable getHotbarItemAction(ItemStack is) {
        return hotbarItems.get(is);
    }

    /**
     * call from main thread only
     */
    public void addHotbarItem(ItemStack item, Runnable runnable) {
        if (!hotbarItems.containsKey(item)) hotbarItems.put(item, runnable);
    }

    public void removeHotbarItem(ItemStack item) {
        if (Thread.currentThread() == ServerLink.mainThread) hotbarItems.remove(item);
        else new BukkitRunnable() {
            @Override
            public void run() {
                hotbarItems.remove(item);
            }
        }.runTask(ServerLink.plugin);
    }

    public boolean isFullyLoggedIn() {
        return fullyLoggedIn;
    }

    public
    @NotNull
    String getStringSetting(String setting) {
        if (settings.containsKey(setting)) {
            return settings.get(setting);
        }
        Jedis jedis = JedisPool.getConn();
        String value = jedis.hget("settings:" + uuid.toString(), setting);
        JedisPool.close(jedis);
        if (value == null) value = "";
        if (Thread.currentThread() == ServerLink.mainThread) settings.put(setting, value);
        else {
            String finalValue = value;
            new BukkitRunnable() {
                @Override
                public void run() {
                    settings.put(setting, finalValue);
                }
            }.runTask(ServerLink.plugin);
        }
        return value;
    }

    public
    @Nullable
    Boolean getBooleanSetting(String setting) {
        if (settings.containsKey(setting)) {
            String s = settings.get(setting);
            if (s.equals("true")) return true;
            else if (s.equals("false")) return false;
            else return null;
        }
        Jedis jedis = JedisPool.getConn();
        String value = jedis.hget("settings:" + uuid.toString(), setting);
        JedisPool.close(jedis);
        if (value == null) return null;
        if (Thread.currentThread() == ServerLink.mainThread) settings.put(setting, value);
        else new BukkitRunnable() {
            @Override
            public void run() {
                settings.put(setting, value);
            }
        }.runTask(ServerLink.plugin);
        if (value.equals("true")) return true;
        else if (value.equals("false")) return false;
        else return null;
    }

    public
    @Nullable
    Integer getIntegerSetting(String setting) {
        if (settings.containsKey(setting)) {
            String value = settings.get(setting);
            try {
                return Integer.parseInt(value);
            } catch (NumberFormatException e) {
                return null;
            }
        }
        Jedis jedis = JedisPool.getConn();
        String value = jedis.hget("settings:" + uuid.toString(), setting);
        JedisPool.close(jedis);
        if (value == null) return null;
        if (Thread.currentThread() == ServerLink.mainThread) settings.put(setting, value);
        else new BukkitRunnable() {
            @Override
            public void run() {
                settings.put(setting, value);
            }
        }.runTask(ServerLink.plugin);
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return null;
        }
    }

    public
    @NotNull
    List<String> getListSetting(String setting) {
        if (settings.containsKey(setting)) return Settings.stringToList(settings.get(setting));
        Jedis jedis = JedisPool.getConn();
        String value = jedis.hget("settings:" + uuid.toString(), setting);
        JedisPool.close(jedis);
        if (value == null) {
            settings.put(setting, "");
            return new LinkedList<>();
        }
        if (Thread.currentThread() == ServerLink.mainThread) settings.put(setting, value);
        else new BukkitRunnable() {
            @Override
            public void run() {
                settings.put(setting, value);
            }
        }.runTask(ServerLink.plugin);
        return Settings.stringToList(value);
    }

    public void setSetting(String setting, String value) {
        if (settings.containsKey(setting) && settings.get(setting) != null && settings.get(setting).equals(value))
            return;
        if (Thread.currentThread() == ServerLink.mainThread) {
            if (value == null) settings.remove(setting);
            else settings.put(setting, value);
        } else new BukkitRunnable() {
            @Override
            public void run() {
                if (value == null) settings.remove(setting);
                else settings.put(setting, value);
            }
        }.runTask(ServerLink.plugin);
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                if (value == null)
                    jedis.hdel("settings:" + uuid.toString(), setting);
                else
                    jedis.hset("settings:" + uuid.toString(), setting, value);
                JedisPool.close(jedis);
            }
        }.runTaskAsynchronously(ServerLink.plugin);
    }

    /**
     * Exactly the same as setSetting() except it doesn't save setting to db until
     * player logs out.
     */
    public void setSetting_SyncOnLogout(String setting, String value) {
        if (settings.containsKey(setting) && settings.get(setting) != null && settings.get(setting).equals(value))
            return;
        if (Thread.currentThread() == ServerLink.mainThread) {
            if (value == null) settings.remove(setting);
            else settings.put(setting, value);
            if (!unsyncedSettings.contains(setting)) unsyncedSettings.add(setting);
        } else new BukkitRunnable() {
            @Override
            public void run() {
                if (value == null) settings.remove(setting);
                else settings.put(setting, value);
                if (!unsyncedSettings.contains(setting)) unsyncedSettings.add(setting);
            }
        }.runTask(ServerLink.plugin);
    }

    public AlphaScoreboard getAlphaBoard() {
        return alphaBoard;
    }

    public String getNametagPrefix() {
        return nametagPrefix;
    }

    public void setNametagPrefix(String newNametagPrefix) {
        if (Thread.currentThread() == ServerLink.mainThread) {
            this.nametagPrefix = newNametagPrefix;
            String tabName = player.getPlayerListName();
            Nametag.setPrefix(player, newNametagPrefix);
            player.setPlayerListName(tabName);
        } else {
            new BukkitRunnable() {
                @Override
                public void run() {
                    nametagPrefix = newNametagPrefix;
                    String tabName = player.getPlayerListName();
                    Nametag.setPrefix(player, newNametagPrefix);
                    player.setPlayerListName(tabName);
                }
            }.runTask(ServerLink.plugin);
        }
    }

    public String getNametagSuffix() {
        return nametagSuffix;
    }

    public void setNametagSuffix(String newNametagSuffix) {
        if (Thread.currentThread() == ServerLink.mainThread) {
            this.nametagSuffix = newNametagSuffix;
            String tabName = player.getPlayerListName();
            Nametag.setSuffix(player, newNametagSuffix);
            player.setPlayerListName(tabName);
        } else new BukkitRunnable() {
            @Override
            public void run() {
                nametagSuffix = newNametagSuffix;
                String tabName = player.getPlayerListName();
                Nametag.setSuffix(player, newNametagSuffix);
                player.setPlayerListName(tabName);
            }
        }.runTask(ServerLink.plugin);
    }

    // updates nametag, prefix, suffix, tabname, displayname, and ranktag
    public void updateNameFormatting() {
        if (Thread.currentThread() == ServerLink.mainThread) {
            List<String> formatNames = getListSetting("nameFormat");
            ArrayList<NameFormat> formats = new ArrayList<>();
            formats.addAll(formatNames.stream().map(NameFormat::valueOf).collect(Collectors.toList()));
            String colors = "";
            boolean addBold = false;
            boolean addItalic = false;
            for (NameFormat format : formats) {
                if (format == NameFormat.BOLD) {
                    addBold = true;
                    continue;
                }
                if (format == NameFormat.ITALIC) {
                    addItalic = true;
                    continue;
                }
                colors = colors + format.chatColor;
            }
            if (addBold) colors = colors + "&l";
            if (addItalic) colors = colors + "&o";
            if (colors.isEmpty()) colors = "&f";
            if (player == null) {
                ConsoleOutput.printError("PLAYER IS NULL!");
                player = Bukkit.getPlayer(uuid);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        if (!fullyLoggedIn) {
                            ConsoleOutput.printError("PLAYER IS NOT FULLY LOGGED IN!");
                            fullyLoggedIn = true;
                            if (player != null) updateNameFormatting();
                        }
                        if (player == null) {
                            ConsoleOutput.printError("PLAYER IS NULL AGAIN!");
                            player = Bukkit.getPlayer(uuid);
                            updateNameFormatting();
                            if (player == null) {
                                ConsoleOutput.printWarning("PLAYER LEFT OR IS NOT RECOGNIZED BY BUKKIT!");
                                cancel();
                            } else if (!player.isOnline()) {
                                cancel();
                            }
                        }
                    }
                }.runTaskTimer(ServerLink.plugin, 20, 10);
            }
            if (colors.contains("&rainbow")) {
                colors = colors.replace("&rainbow", "");
                String rainbow = "";
                int currentColor = 0;
                int lastColor = 5;
                for (int i = 0; i < player.getName().length(); i++) {
                    Character s = player.getName().charAt(i);
                    if (currentColor == 0) rainbow = rainbow + "&c" + s;
                    else if (currentColor == 1) rainbow = rainbow + "&e" + s;
                    else if (currentColor == 2) rainbow = rainbow + "&a" + s;
                    else if (currentColor == 3) rainbow = rainbow + "&b" + s;
                    else if (currentColor == 4) rainbow = rainbow + "&9" + s;
                    else if (currentColor == 5) rainbow = rainbow + "&d" + s;
                    currentColor++;
                    if (currentColor > lastColor) currentColor = 0;
                }
                if (player == null) {
                    ConsoleOutput.printError("PLAYER IS NULL!");
                    player = Bukkit.getPlayer(uuid);
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            if (!fullyLoggedIn) {
                                ConsoleOutput.printError("PLAYER IS NOT FULLY LOGGED IN!");
                                fullyLoggedIn = true;
                            }
                            if (player == null) {
                                ConsoleOutput.printError("PLAYER IS NULL AGAIN!");
                                player = Bukkit.getPlayer(uuid);
                                if (player == null) {
                                    ConsoleOutput.printWarning("PLAYER LEFT OR IS NOT RECOGNIZED BY BUKKIT!");
                                    cancel();
                                } else if (!player.isOnline()) {
                                    cancel();
                                }
                            }
                        }
                    }.runTaskTimer(ServerLink.plugin, 20, 10);
                }
                player.setDisplayName(chat.color(colors + rainbow));
            } else {
                if (player == null) {
                    ConsoleOutput.printError("PLAYER IS NULL!");
                    player = Bukkit.getPlayer(uuid);
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            if (!fullyLoggedIn) {
                                ConsoleOutput.printError("PLAYER IS NOT FULLY LOGGED IN!");
                                fullyLoggedIn = true;
                            }
                            if (player == null) {
                                ConsoleOutput.printError("PLAYER IS NULL AGAIN!");
                                player = Bukkit.getPlayer(uuid);
                                if (player == null) {
                                    ConsoleOutput.printWarning("PLAYER LEFT OR IS NOT RECOGNIZED BY BUKKIT!");
                                    cancel();
                                } else if (!player.isOnline()) {
                                    cancel();
                                }
                            }
                        }
                    }.runTaskTimer(ServerLink.plugin, 20, 10);
                }
                player.setDisplayName(chat.color(colors +
                        player.getName()));
            }
            if (TeamManager.isTeamGame()) {
                if (getRank() > 1) {
                    if (getTeam() != null && getTeam().getName() != null && getTeam().getName().length() != 0)
                        player.setPlayerListName(chat.color(ChatColor.valueOf(getTeam().getName()) +
                                getTeam().getName() + " &r" + player.getDisplayName() +
                                "&7[" + Permissions.getRankName(getRank()) + "&7]"));
                    else
                        player.setPlayerListName(chat.color("&7SPEC &r" + player.getDisplayName() +
                                "&7[" + Permissions.getRankName(getRank()) + "&7]"));
                } else {
                    if (getTeam() != null && getTeam().getName() != null && getTeam().getName().length() != 0)
                        player.setPlayerListName(chat.color(ChatColor.valueOf(getTeam().getName()) + getTeam().getName() + " &r" + player.getDisplayName()));
                    else
                        player.setPlayerListName(chat.color("&7SPEC &r" + player.getDisplayName()));
                }
            } else {
                if (getRank() == 1) player.setPlayerListName(player.getDisplayName());
                else
                    player.setPlayerListName(chat.color(Permissions.getRankName(getRank()) + "&r ") + player.getDisplayName());
            }
            updateNametag();
            updateRankTag();
            getData("tempDisplayNameToSync").setValue(player.getDisplayName());
            if ((boolean) getData("syncDisplayNameCooldown", false).getValue()) return;
        } else new BukkitRunnable() {
            @Override
            public void run() {
                List<String> formatNames = getListSetting("nameFormat");
                ArrayList<NameFormat> formats = new ArrayList<>();
                formats.addAll(formatNames.stream().map(NameFormat::valueOf).collect(Collectors.toList()));
                String colors = "";
                boolean addBold = false;
                boolean addItalic = false;
                for (NameFormat format : formats) {
                    if (format == NameFormat.BOLD) {
                        addBold = true;
                        continue;
                    }
                    if (format == NameFormat.ITALIC) {
                        addItalic = true;
                        continue;
                    }
                    colors = colors + format.chatColor;
                }
                if (addBold) colors = colors + "&l";
                if (addItalic) colors = colors + "&o";
                if (colors.isEmpty()) colors = "&f";
                if (colors.contains("&rainbow")) {
                    colors = colors.replace("&rainbow", "");
                    String rainbow = "";
                    int lastColor = 6;
                    SplittableRandom r = new SplittableRandom();
                    for (int i = 0; i < player.getName().length(); i++) {
                        Character s = player.getName().charAt(i);
                        int currentColor = r.nextInt(7);
                        while (currentColor == lastColor) currentColor = r.nextInt(6);
                        if (currentColor == 0) rainbow = rainbow + "&a" + s;
                        else if (currentColor == 1) rainbow = rainbow + "&b" + s;
                        else if (currentColor == 2) rainbow = rainbow + "&d" + s;
                        else if (currentColor == 3) rainbow = rainbow + "&3" + s;
                        else if (currentColor == 4) rainbow = rainbow + "&e" + s;
                        else if (currentColor == 5) rainbow = rainbow + "&6" + s;
                        else if (currentColor == 6) rainbow = rainbow + "&c" + s;
                        lastColor = currentColor;
                    }
                    player.setDisplayName(chat.color(colors + rainbow));
                } else player.setDisplayName(chat.color(colors + player.getName()));
                if (TeamManager.isTeamGame()) {
                    if (getRank() > 1) {
                        if (getTeam() != null && getTeam().getName() != null && getTeam().getName().length() != 0)
                            player.setPlayerListName(chat.color(ChatColor.valueOf(getTeam().getName()) +
                                    getTeam().getName() + " &r" + player.getDisplayName() +
                                    "&7[" + Permissions.getRankName(getRank()) + "&7]"));
                        else
                            player.setPlayerListName(chat.color("&7SPEC &r" + player.getDisplayName() +
                                    "&7[" + Permissions.getRankName(getRank()) + "&7]"));
                    } else {
                        if (getTeam() != null && getTeam().getName() != null && getTeam().getName().length() != 0)
                            player.setPlayerListName(chat.color(ChatColor.valueOf(getTeam().getName()) + getTeam().getName() + " &r" + player.getDisplayName()));
                        else
                            player.setPlayerListName(chat.color("&7SPEC &r" + player.getDisplayName()));
                    }
                } else {
                    if (getRank() == 1) player.setPlayerListName(player.getDisplayName());
                    else
                        player.setPlayerListName(chat.color(Permissions.getRankName(getRank()) + "&r ") + player.getDisplayName());
                }
                updateNametag();
                updateRankTag();
                getData("tempDisplayNameToSync").setValue(player.getDisplayName());
                if ((boolean) getData("syncDisplayNameCooldown", false).getValue()) return;
            }
        }.runTask(ServerLink.plugin);
        new BukkitRunnable() {
            @Override
            public void run() {
                Jedis jedis = JedisPool.getConn();
                jedis.hset(uuid.toString(), "displayName", (String) getData("tempDisplayNameToSync").getValue());
                JedisPool.close(jedis);
            }
        }.runTaskLaterAsynchronously(ServerLink.plugin, 30);
    }

    public void updateNametag() {
        if (Thread.currentThread() == ServerLink.mainThread) {
            setNametagPrefix("");
            setNametagSuffix("");
            List<String> formatNames = getListSetting("nameFormat");
            ArrayList<NameFormat> formats = new ArrayList<>();
            formats.addAll(formatNames.stream().map(NameFormat::valueOf).collect(Collectors.toList()));
            boolean bold = false;
            boolean italic = false;
            boolean rainbow = false;
            String color = "&f";
            for (NameFormat f : formats) {
                if (f == NameFormat.BOLD) bold = true;
                else if (f == NameFormat.ITALIC) italic = true;
                else if (f == NameFormat.RAINBOW) rainbow = true;
                else color = f.getChatColor();
            }
            if (rainbow) color = Permissions.getRankName(getRank()).substring(0, 2);
            String prefix = "";
            if (TeamManager.isTeamGame() && getTeam() != null && getTeam().getName() != null && getTeam().getName().length() != 0)
                prefix = "" + ChatColor.valueOf(getTeam().getName()) + getTeam().getName() + "&r";
            prefix = prefix + color;
            if (italic) prefix = prefix + "&o";
            if (bold) prefix = prefix + "&l";
            setNametagPrefix(chat.color(prefix + " "));
            if (rank > 1) {
                if (nstatus.isLobby() || nstatus.isLobbyCountdown()) {
                    if (rank <= 3) {
                        if (TeamManager.isTeamGame())
                            setNametagSuffix(chat.color("&7[" + Permissions.getRankName(rank) + "&7]"));
                        else
                            setNametagPrefix(chat.color(Permissions.getRankName(rank) + " &r" + prefix));
                    } else {
                        // Player has a ranktag, so don't show rank in nametag suffix
                    }
                } else {
                    if (TeamManager.isTeamGame())
                        setNametagSuffix(chat.color("&7[" + Permissions.getRankName(rank) + "&7]"));
                    else
                        setNametagPrefix(chat.color(Permissions.getRankName(rank) + " &r" + prefix));
                }
            }
        } else new BukkitRunnable() {
            @Override
            public void run() {
                setNametagPrefix("");
                setNametagSuffix("");
                List<String> formatNames = getListSetting("nameFormat");
                ArrayList<NameFormat> formats = new ArrayList<>();
                formats.addAll(formatNames.stream().map(NameFormat::valueOf).collect(Collectors.toList()));
                boolean bold = false;
                boolean italic = false;
                boolean rainbow = false;
                String color = "&f";
                for (NameFormat f : formats) {
                    if (f == NameFormat.BOLD) bold = true;
                    else if (f == NameFormat.ITALIC) italic = true;
                    else if (f == NameFormat.RAINBOW) rainbow = true;
                    else color = f.getChatColor();
                }
                if (rainbow) color = Permissions.getRankName(getRank()).substring(0, 2);
                String prefix = "";
                if (TeamManager.isTeamGame() && getTeam() != null && getTeam().getName() != null && getTeam().getName().length() != 0)
                    prefix = "" + ChatColor.valueOf(getTeam().getName()) + getTeam().getName() + "&r";
                prefix = prefix + color;
                if (italic) prefix = prefix + "&o";
                if (bold) prefix = prefix + "&l";
                setNametagPrefix(chat.color(prefix + " "));
                if (rank > 1) {
                    if (nstatus.isLobby() || nstatus.isLobbyCountdown()) {
                        if (rank <= 3) {
                            if (TeamManager.isTeamGame())
                                setNametagSuffix(chat.color("&7[" + Permissions.getRankName(rank) + "&7]"));
                            else
                                setNametagPrefix(chat.color(Permissions.getRankName(rank) + " &r" + prefix));
                        } else {
                            // Player has a ranktag, so don't show rank in nametag suffix
                        }
                    } else {
                        if (TeamManager.isTeamGame())
                            setNametagSuffix(chat.color("&7[" + Permissions.getRankName(rank) + "&7]"));
                        else
                            setNametagPrefix(chat.color(Permissions.getRankName(rank) + " &r" + prefix));
                    }
                }
            }
        }.runTask(ServerLink.plugin);
    }

    public void hideRankTagFrom(Player player) {
        if (Thread.currentThread() == ServerLink.mainThread) {
            if (hologram == null)
                hologram = HologramsAPI.createHologram(ServerLink.plugin, player.getLocation().add(0, rankTagHeight, 0));
            hologram.getVisibilityManager().hideTo(player);
        } else {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (hologram == null)
                        hologram = HologramsAPI.createHologram(ServerLink.plugin, player.getLocation().add(0, rankTagHeight, 0));
                    hologram.getVisibilityManager().hideTo(player);
                }
            }.runTask(ServerLink.plugin);
        }
    }

    public void showRankTagTo(Player player) {
        if (Thread.currentThread() == ServerLink.mainThread) {
            if (hologram == null)
                hologram = HologramsAPI.createHologram(ServerLink.plugin, player.getLocation().add(0, rankTagHeight, 0));
            hologram.getVisibilityManager().showTo(player);
        } else {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (hologram == null)
                        hologram = HologramsAPI.createHologram(ServerLink.plugin, player.getLocation().add(0, rankTagHeight, 0));
                    hologram.getVisibilityManager().showTo(player);
                }
            }.runTask(ServerLink.plugin);
        }
    }

    public String getRankTag() {
        if (!rankTagRunning) return "";
        if (hologram != null)
            if (hologram.size() == 1) return hologram.getLine(0).toString();
        return "";
    }

    public void setRankTag(String tag) {
        if (Thread.currentThread() == ServerLink.mainThread) {
            removeRankTag();
            if (tag == null || tag.trim().equals("")) return;
            if (hologram == null || hologram.isDeleted())
                hologram = HologramsAPI.createHologram(ServerLink.plugin, player.getLocation().add(0, 2.6, 0));
            if (hologram.size() == 1) hologram.removeLine(0);
            hologram.appendTextLine(chat.color(tag));
            // if the player's client thinks there is an armorstand, it won't be able to hit blocks and other players
            hologram.getVisibilityManager().hideTo(player);
            // It says the task is already running don't make a new runnable
            rankTagRunnable = new BukkitRunnable() {
                @Override
                public void run() {
                    hologram.teleport(player.getLocation().add(0, rankTagHeight, 0));
                }
            };
            rankTagRunnable.runTaskTimer(ServerLink.plugin, 0, 3);
            rankTagRunning = true;
        } else {
            new BukkitRunnable() {
                @Override
                public void run() {
                    removeRankTag();
                    if (hologram != null && !hologram.isDeleted()) hologram.delete();
                    hologram = null;
                    if (tag == null || tag.trim().equals("")) return;
                    if (hologram == null)
                        hologram = HologramsAPI.createHologram(ServerLink.plugin, player.getLocation().add(0, 2.6, 0));
                    if (hologram.size() == 1) hologram.removeLine(0);
                    hologram.appendTextLine(chat.color(tag));
                    // if the player's client thinks there is an armorstand, it won't be able to hit blocks and other players
                    hologram.getVisibilityManager().hideTo(player);
                    // It says the task is already running if we don't make a new runnable
                    rankTagRunnable = new BukkitRunnable() {
                        @Override
                        public void run() {
                            hologram.teleport(player.getLocation().add(0, rankTagHeight, 0));
                        }
                    };
                    rankTagRunnable.runTaskTimer(ServerLink.plugin, 0, 3);
                    rankTagRunning = true;
                }
            }.runTask(ServerLink.plugin);
        }
    }

    public void removeRankTag() {
        if (Thread.currentThread() == ServerLink.mainThread) {
            if (rankTagRunning) rankTagRunnable.cancel();
            rankTagRunning = false;
            if (hologram != null && !hologram.isDeleted()) hologram.delete();
            hologram = null;
        } else {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (rankTagRunning) rankTagRunnable.cancel();
                    rankTagRunning = false;
                    if (hologram != null && !hologram.isDeleted()) hologram.delete();
                    hologram = null;
                }
            }.runTask(ServerLink.plugin);
        }
    }

    // this doesn't have to check what thread it's running on because all the methods it calls check before
    // updating variables
    public void updateRankTag() {
        if (!nstatus.isLobby() && !nstatus.isLobbyCountdown()) {
            removeRankTag();
            return;
        }
        if (rank < 4) {
            removeRankTag();
            return;
        }
        String tag = Permissions.getRankName(rank);
        if (rank == 10) tag = "&d✖ &r" + tag + "&r &d✖";
        else if (rank == 9) tag = "&e☣ &r" + tag + "&r &e☣";
        else if (rank == 8) tag = "&3❄ &r" + tag + "&r &3❄";
        else if (rank == 7) tag = "&3❄ &r" + tag + "&r &3❄";
        else if (rank == 6) tag = "&a♫ &r" + tag + "&r &a♫";
        else if (rank == 5) tag = "&4♨ &r" + tag + "&r &4♨";
        else if (rank == 4) tag = "&1☘ &r" + tag + "&r &1☘";
        setRankTag(tag);
    }

    /**
     * Only call this method from the main thread.
     */
    public Team getTeam() {
        return TeamManager.getTeam(player);
    }

    public void setTeam(Team t) {
        if (Thread.currentThread() == ServerLink.mainThread) {
            if (TeamManager.getTeam(player) != null) //noinspection ConstantConditions
                TeamManager.getTeam(player).removePlayer(player);
            if (t != null && !t.isOnTeam(player)) t.addPlayer(player);
        } else {
            new BukkitRunnable() {
                @Override
                public void run() {
                    if (TeamManager.getTeam(player) != null) //noinspection ConstantConditions
                        TeamManager.getTeam(player).removePlayer(player);
                    if (t != null && !t.isOnTeam(player)) t.addPlayer(player);
                }
            }.runTask(ServerLink.plugin);
        }
    }
}
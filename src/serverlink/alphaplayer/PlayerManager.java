package serverlink.alphaplayer;

import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class PlayerManager {
    // do not iterate over alphaPlayers
    public static HashMap<UUID, AlphaPlayer> alphaPlayers = new HashMap<>();
    public static List<AlphaPlayer> onlineAlphaPlayers = new ArrayList<>();

    public static AlphaPlayer get(Player player) {
        return alphaPlayers.get(player.getUniqueId());
    }

    public static AlphaPlayer get(UUID uuid) {
        return alphaPlayers.get(uuid);
    }

    /**
     * You can iterate over this from any thread because it returns a copy
     */
    public static List<AlphaPlayer> getOnlineAlphaPlayers() {
        return new ArrayList<>(onlineAlphaPlayers);
    }
}
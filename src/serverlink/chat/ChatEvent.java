package serverlink.chat;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.ChatColor;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.player.Level;
import serverlink.player.Permissions;
import serverlink.player.Punishments;
import serverlink.player.party.PartyManager;
import serverlink.player.team.TeamManager;
import serverlink.server.Messages;
import serverlink.util.CommandText;
import serverlink.util.TimeUtils;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.UUID;

public class ChatEvent implements Listener {
    @EventHandler
    void onChat(AsyncPlayerChatEvent e) {
        if (e.isCancelled()) return;
        final Player player = e.getPlayer();
        if (e.getMessage().startsWith("@")) {
            e.setCancelled(true);
            AlphaPlayer aPlayer = PlayerManager.get(player);
            if (PartyManager.getPartyObject(aPlayer).isEmpty()) {
                player.sendMessage(chat.color(chat.getServer() + "You must be in a party to use party chat " +
                        "(&5@ &7+ &amessage&f)."));
                return;
            }
            String message = chat.color("&5&lPARTY &r" + player.getDisplayName() + "&r: " +
                    ChatColor.stripColor(chat.color(ChatFilter.filter(e.getMessage().replaceFirst("@", "")))));
            for (UUID uuid : PartyManager.getPartyObject(aPlayer)) {
                ByteArrayDataOutput out = ByteStreams.newDataOutput();
                out.writeUTF("ForwardToPlayer");
                out.writeUTF(ServerLink.getNameFromAlphaPlayer(aPlayer, uuid));
                out.writeUTF("PartyChat");

                ByteArrayOutputStream msgbytes = new ByteArrayOutputStream();
                DataOutputStream msgout = new DataOutputStream(msgbytes);
                try {
                    msgout.writeUTF(message);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
                out.writeShort(msgbytes.toByteArray().length);
                out.write(msgbytes.toByteArray());
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        player.sendPluginMessage(ServerLink.plugin, "BungeeCord", out.toByteArray());
                    }
                }.runTask(ServerLink.plugin);
            }
            return;
        }
        AlphaPlayer aPlayer = PlayerManager.get(player);
        int muteExpiration = aPlayer.getMuteExpiration();
        if (muteExpiration != 0 && TimeUtils.secondsBetween(TimeUtils.getCurrent(), muteExpiration) > 0) {
            player.sendMessage(chat.color(chat.getServer() + "You are currently " +
                    "muted for &c" + Punishments.format(muteExpiration - TimeUtils.getCurrent()) + " &ffor &c" + aPlayer.getMuteReason() + "."));
            e.setCancelled(true);
            return;
        }
        if (!aPlayer.getBooleanSetting("chatMessagesEnabled")) {
            player.sendMessage(chat.color("&cYou have chat messages turned off in your settings.\n" +
                    "You can turn them back on by typing &6/gui ChatSettings &cand clicking the EyeOfEnder."));
            e.setCancelled(true);
            return;
        }
        int rank = aPlayer.getRank();
        if (rank == 1) {
            if ((boolean) aPlayer.getData("chatRankCooldownActive", true).getValue()) {
                e.setCancelled(true);
                CommandText.sendMessage(player, Messages.messaging_cooldown.toJSONString());
                return;
            } else {
                aPlayer.getData("chatRankCooldownActive").setValue(true);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        aPlayer.getData("chatRankCooldownActive").setValue(false);
                    }
                }.runTaskLater(ServerLink.plugin, 20 * 3);
            }
        }
        ChatColor levelColor = ChatColor.GRAY;
        int level = aPlayer.getLevel();
        String levelString = "" + Level.getLevelColor(level) + level;
        if (aPlayer.getRank() == 10) levelString = "&4&l&o100";
        e.setMessage(chat.formatChatMessage(player, ChatFilter.filter(e.getMessage())));
        if (rank > 1) {
            if (TeamManager.isTeamGame()) {
                if (TeamManager.getTeam(player) == null)
                    e.setFormat(chat.color(levelColor + levelString + "&r "
                            + Permissions.getRankName(rank) + " &8[&7SPEC&8]&r " + player.getDisplayName()
                            + "&r>" + " %2$s"));
                else
                    e.setFormat(chat.color(levelColor + levelString + "&r "
                            + Permissions.getRankName(rank) + " &8["
                            + ChatColor.valueOf(TeamManager.getTeam(player).getName())
                            + TeamManager.getTeam(player).getName() + "&8]&r " + player.getDisplayName()
                            + "&r>" + " %2$s"));
            } else {
                e.setFormat(chat.color(levelColor + levelString + " " + "&r"
                        + Permissions.getRankName(rank) + "&r " + player.getDisplayName() + "&r>"
                        + " %2$s"));
            }
        } else {
            if (TeamManager.isTeamGame()) {
                if (TeamManager.getTeam(player) == null)
                    e.setFormat(chat.color(levelColor + levelString + " &8[&7SPEC&8]&r "
                            + player.getDisplayName() + "&r>" + " %2$s"));
                else
                    e.setFormat(chat.color(levelColor + levelString + " &8["
                            + ChatColor.valueOf(TeamManager.getTeam(player).getName())
                            + TeamManager.getTeam(player).getName() + "&8]&r " + player.getDisplayName()
                            + "&r>" + " %2$s"));
            } else {
                e.setFormat(chat.color(levelColor + levelString + "&r " + player.getDisplayName()
                        + "&r>" + " %2$s"));
            }
        }
        Iterator<Player> it = e.getRecipients().iterator();
        while (it.hasNext()) {
            Player p = it.next();
            if (p == player) continue;
            AlphaPlayer p1MP = PlayerManager.get(p);
            if (p1MP == null || !p1MP.isFullyLoggedIn()) continue;
            if (!p1MP.getBooleanSetting("chatMessagesEnabled")) it.remove();
            else if (e.getMessage().toLowerCase().contains(p.getName().toLowerCase()) && p1MP.getBooleanSetting("chatMentionsEnabled"))
                p.playSound(p.getLocation(), Sound.NOTE_PLING, 20, 20);
        }
    }

}

package serverlink.chat;

import com.google.common.io.ByteArrayDataInput;
import com.google.common.io.ByteStreams;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.plugin.messaging.PluginMessageListener;

import java.io.ByteArrayInputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.UUID;

public class ChatPluginMessageListener implements PluginMessageListener {

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public void onPluginMessageReceived(String channel, Player player, byte[] message) {
        if (!channel.equals("BungeeCord")) {
            return;
        }
        ByteArrayDataInput in = ByteStreams.newDataInput(message);
        String subChannel = in.readUTF();
        if (subChannel.equals("PartyChat")) {
            short len = in.readShort();
            byte[] msgbytes = new byte[len];
            in.readFully(msgbytes);

            DataInputStream msgin = new DataInputStream(new ByteArrayInputStream(msgbytes));
            String chatMessage = "";
            try {
                chatMessage = msgin.readUTF();
            } catch (IOException e) {
                e.printStackTrace();
            }
            AlphaPlayer aPlayer = PlayerManager.get(player);
            while (!aPlayer.isFullyLoggedIn()) ;
            player.sendMessage(chatMessage);
            if (aPlayer.getBooleanSetting("chatMentionsEnabled"))
                player.playSound(player.getLocation(), Sound.NOTE_PLING, 20, 20);
        } else if (subChannel.equals("PrivateMessage")) {
            AlphaPlayer aPlayer = PlayerManager.get(player);
            while (!aPlayer.isFullyLoggedIn()) ;
            short len = in.readShort();
            byte[] msgbytes = new byte[len];
            in.readFully(msgbytes);
            DataInputStream msgin = new DataInputStream(new ByteArrayInputStream(msgbytes));
            String senderUUID = "";
            String senderMessage = "";
            try {
                senderUUID = msgin.readUTF();
                senderMessage = msgin.readUTF();
            } catch (IOException e) {
                e.printStackTrace();
            }
            if (aPlayer.getData("lastMessaged").getValue() == null)
                aPlayer.getData("lastMessaged").setValue(UUID.fromString(senderUUID));
            player.sendMessage(senderMessage);
            if (aPlayer.getBooleanSetting("chatMentionsEnabled"))
                player.playSound(player.getLocation(), Sound.NOTE_PLING, 20, 20);
        }
    }
}

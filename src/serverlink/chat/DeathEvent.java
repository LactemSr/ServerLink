package serverlink.chat;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.classes.Class;
import serverlink.classes.PowerClass;
import serverlink.customevent.CustomPlayerDeathEvent;
import serverlink.customevent.CustomPlayerKillLivingEntityEvent;

import java.math.BigDecimal;

public class DeathEvent implements Listener {
    @EventHandler
    private void onPlayerDeath(CustomPlayerDeathEvent e) {
        Player player = e.getPlayer();
        String reason = e.getKiller();
        String deathMessage;

        if (reason.equals("suicide")) {
            // don't announce suicide messages if death messages are disabled
            if (!chat.getDeathMessagesEnabled()) return;
            deathMessage = ("&8<&5&lDeath&8> " + player.getDisplayName()
                    + "&r&d committed suicide.");
        } else {
            if (!ChatColor.stripColor(reason).startsWith("by ") && !ChatColor.stripColor(reason).startsWith("for "))
                reason = "&dby &f" + reason.trim();
            // if death messages are disabled, send message to victim instead of announcing it
            if (!chat.getDeathMessagesEnabled()) {
                player.sendMessage(chat.color("&8<&5&lDeath&8> &dYou were " +
                        "killed &f" + reason.trim() + "."));
                return;
            }
            deathMessage = ("&8<&5&lDeath&8> " + player.getDisplayName()
                    + "&r&d &f" + reason.trim() + ".");
        }
        Bukkit.getServer().broadcastMessage(chat.color(deathMessage));
    }

    @EventHandler
    private void onPlayerKillLivingEntity(CustomPlayerKillLivingEntityEvent e) {

        if (!(e.getEntity() instanceof Player)) return;
        Player killer = e.getPlayer();
        Player victim = (Player) e.getEntity();
        AlphaPlayer killerAlphaPlayer = PlayerManager.get(killer.getUniqueId());
        String deathMessage;
        String killerHealth = "";
        String killerClass;

        AlphaPlayer killerMP = PlayerManager.get(killer.getUniqueId());
        if ((killer.hasMetadata("NPC") || killer.isOnline()) && killerMP != null && killerMP.isFullyLoggedIn()) {
            BigDecimal intKHealth = new BigDecimal(((Double) (killer.getHealth() / 2)).toString()).setScale(1, BigDecimal.ROUND_HALF_UP);
            if (killerMP.getData("class").getValue() != null) {
                Class killerClassVal = (Class) killerMP.getData("class").getValue();
                if (killerClassVal instanceof PowerClass)
                    killerClass = killerClassVal.getFancyName() + "&r&e lvl " + ((PowerClass) killerClassVal).getLevel(killerAlphaPlayer);
                else
                    killerClass = killerClassVal.getFancyName();
                killerHealth = killerHealth.concat("&7(&a").concat(intKHealth.toString() + "&c❤&7, &d" + killerClass.replace("&n", "") + "&7)&d");
            } else {
                killerHealth = killerHealth.concat("&7(&a").concat(intKHealth.toString() + "&c❤&7)&d");
            }
            deathMessage = "&8<&5&lKill&8> " + victim.getDisplayName()
                    + "&r&d by " + killer.getDisplayName() + "&r " + killerHealth;
        } else {
            deathMessage = "&8<&5&lKill&8> " + victim.getDisplayName()
                    + "&r&d by &f" + killer.getName();
        }
        // if death messages are disabled, send message to victim instead of announcing it
        if (chat.getDeathMessagesEnabled())
            Bukkit.getServer().broadcastMessage(chat.color(deathMessage));
        else
            victim.sendMessage(chat.color(deathMessage.replaceFirst(victim.getDisplayName(), "&dYou were killed")));

    }
}

package serverlink.chat;

public enum NameFormat {
    WHITE("&f"),
    GREEN("&a"),
    LIGHT_PURPLE("&d"),
    BLUE("&9"),
    RED("&c"),
    RAINBOW("&rainbow"),
    BOLD("&l"),
    ITALIC("&o"),
    DARK_GREEN("&2"),
    AQUA("&b"),
    DARK_RED("&4"),
    DARK_AQUA("&3"),
    DARK_PURPLE("&5"),
    YELLOW("&e"),
    GOLD("&6");

    public String chatColor;

    NameFormat(String chatColor) {
        this.chatColor = chatColor;
    }

    public static NameFormat getFromString(String s) {
        switch (s) {
            case "bold":
                return BOLD;
            case "italic":
                return ITALIC;
            case "blue":
                return BLUE;
            case "green":
                return GREEN;
            case "dark_green":
                return DARK_GREEN;
            case "red":
                return RED;
            case "dark_red":
                return DARK_RED;
            case "aqua":
                return AQUA;
            case "dark_aqua":
                return DARK_AQUA;
            case "light_purple":
                return LIGHT_PURPLE;
            case "dark_purple":
                return DARK_PURPLE;
            case "yellow":
                return YELLOW;
            case "white":
                return WHITE;
            case "gold":
                return GOLD;
            case "rainbow":
                return RAINBOW;
        }
        return WHITE;
    }

    public String getChatColor() {
        return chatColor;
    }
}

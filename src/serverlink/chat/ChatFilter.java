package serverlink.chat;

import com.comphenix.protocol.PacketType;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.wrappers.WrappedChatComponent;
import org.bukkit.ChatColor;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;
import serverlink.ServerLink;
import serverlink.util.JsonUtil;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class ChatFilter {
    // Words that start with "-" are only matched when a player says starts or ends a word with it
    private static final List<String> bannedWords = new ArrayList<String>() {{
        add("fuck");
        add("fcuk");
        add("bitch");
        add("bicth");
        add("bitcth");
        add("bich");
        add("nigger");
        add("nigga");
        add("pussy");
        add("asshole");
        add("asswipe");
        add("assclown");
        add("dumbass");
        add("shit");
        add("cunt");
        add("whore");
        add("douche");
        add("faggot");
        add("dildo");
        add("goddamn");
        add("handjob");
        add("hand job");
        add("blowjob");
        add("pussies");
        add("penis");
        add("queef");
        add("rimjob");
        add("testicle");
        add("testical");
        add("vagina");
        add("puss");
        add("boob");
        add("breast");
        add("bastard");
        add("damn");
        add("dafuq");
        add("-spick");
        add("-dick");
        add("-dyke");
        add("-fag");
        add("-wank");
        add("-cum");
        add("vgina");
        add("-dik");
        add("pussi");
        add("-fuk");
    }};
    private static boolean oldVersion16x = true;

    public static void setup() {
        ServerLink.protocolManager.addPacketListener(
                new PacketAdapter(ServerLink.plugin, PacketType.Play.Server.CHAT) {
                    public void onPacketSending(PacketEvent event) {
                        String message = "";
                        try {
                            String jsonMessage = event.getPacket().getChatComponents().getValues().get(0).getJson();
                            if ((jsonMessage != null) && (!jsonMessage.isEmpty())) {
                                message = jsonToString(jsonMessage);
                            }
                        } catch (Throwable ignored) {
                        }
                        String oldMessage = "";
                        if (event.getPacket().getStrings().size() > 0) {
                            String jsonMessage = event.getPacket().getStrings().read(0);
                            if (jsonMessage != null) {
                                oldMessage = textToString(jsonMessage);
                            }
                        }
                        boolean newVersion = !message.isEmpty();

                        message = message.isEmpty() ? oldMessage : message;
                        if (message.isEmpty()) return;
                        String resultMessage = replaceCommandUnavailable(message);
                        if (message.equals(resultMessage)) {
                            return;
                        }
                        if (resultMessage.isEmpty()) {
                            event.setCancelled(true);
                        } else if (newVersion) {
                            event.getPacket().getChatComponents().write(0, WrappedChatComponent.fromJson(JsonUtil.toJSON(resultMessage)));
                        } else {
                            event.getPacket().getStrings().write(0, oldVersion16x ? "{\"text\":\"" + resultMessage + "\"}" : resultMessage);
                        }
                    }
                });
    }

    private static String replaceCommandUnavailable(String input) {
        if (input.startsWith(chat.color("&cI'm sorry")) || input.startsWith("Unknown command")
                || input.startsWith(chat.color("&aYou don't have permission to execute"))
                || input.startsWith(chat.color("&cYou are not allowed to use this command"))
                || input.startsWith(chat.color("&cYou don't have permission to use Buycraft ad")))
            return chat.color(chat.getServer() + chat.gcu());
        return input;
    }

    public static String filter(String input) {
        String newInput = input;
        for (String word : bannedWords) {
            newInput = censorAll(newInput, word);
        }
        // Fixes parentheses bug probably related to json conversion
        if (!newInput.equals(input)) newInput = newInput.replaceAll("\"", "\'");
        return newInput;
    }

    private static String censorAll(String input, String wordToCensor) {
        boolean mustStartOrEnd = false;
        if (wordToCensor.startsWith("-")) {
            wordToCensor = wordToCensor.substring(1, wordToCensor.length());
            mustStartOrEnd = true;
        }
        List<String> possibleWords = new ArrayList<>();
        String[] words = input.split(" ");
        for (int i = 0; i < words.length; i++) {
            possibleWords.add(words[i]);
            String possibleWord = words[i];
            for (int j = i + 1; j < words.length; j++) {
                if (words[j].length() < 8)
                    possibleWord = possibleWord + " " + words[j];
                if (possibleWord.replace(" ", "").length() >= 3)
                    if (!possibleWord.equals(words[i]))
                        possibleWords.add(possibleWord);
                if (possibleWord.replace(" ", "").length() >= 8)
                    break;
            }
        }
        for (String word : possibleWords) {
            while (word.startsWith("§")) {
                word = word.substring(2, word.length());
            }
            String before = "";
            while (!word.isEmpty() && word.substring(0, 1).matches("[^A-Za-z]")) {
                before = before + word.substring(0, 1);
                word = word.substring(1, word.length());
            }
            String after = "";
            while (!word.isEmpty() && word.substring(word.length() - 1, word.length()).matches("[^A-Za-z]")) {
                after = after + word.substring(word.length() - 1, word.length());
                word = word.substring(0, word.length() - 1);
            }
            // Word by itself
            String wordVariant = word.toLowerCase().replace(" ", "");
            if ((mustStartOrEnd && (wordVariant.startsWith(wordToCensor)) || wordVariant.endsWith(wordToCensor)) || (!mustStartOrEnd && wordVariant.contains(wordToCensor))) {
                String replacement = word.substring(0, 1);
                while (replacement.length() < word.length()) replacement = replacement + "*";
                input = input.replace(word, before + replacement + after);
                break;
            }
            // Word with common obfuscators replaced
            wordVariant = wordVariant.replace("v", "u").replace("@", "a").replace("!", "i")
                    .replace("1", "i").replace("7", "t").replace("0", "o")
                    .replace("$", "s").replaceAll("[^A-Za-z]", "");
            if ((mustStartOrEnd && (wordVariant.startsWith(wordToCensor)) || wordVariant.endsWith(wordToCensor)) || (!mustStartOrEnd && wordVariant.contains(wordToCensor))) {
                String replacement = word.substring(0, 1);
                while (replacement.length() < word.length()) replacement = replacement + "*";
                input = input.replace(word, before + replacement + after);
                break;
            }
            // Word with duplicate letters stripped
            char[] chars = wordVariant.toCharArray();
            Set<Character> charSet = new LinkedHashSet<>();
            for (char c : chars) {
                charSet.add(c);
            }
            StringBuilder sb = new StringBuilder();
            charSet.forEach(sb::append);
            wordVariant = sb.toString();
            if ((mustStartOrEnd && (wordVariant.startsWith(wordToCensor)) || wordVariant.endsWith(wordToCensor)) || (!mustStartOrEnd && wordVariant.contains(wordToCensor))) {
                String replacement = word.substring(0, 1);
                while (replacement.length() < word.length()) replacement = replacement + "*";
                input = input.replace(word, before + replacement + after);
                break;
            }
        }
        return input;
    }

    private static String jsonToString(JSONObject source) {
        String result = "";
        for (Object key : source.keySet()) {
            Object value = source.get(key);
            if ((value instanceof String)) {
                if ((key instanceof String)) {
                    result = result + getStringValue((String) key, (String) value);
                }
            } else if ((value instanceof Boolean)) {
                if ((key instanceof String)) {
                    result = result + getBooleanValue((String) key, (Boolean) value);
                }
            } else if ((value instanceof JSONObject)) {
                result = result + jsonToString((JSONObject) value);
            } else if ((value instanceof JSONArray)) {
                result = result + jsonToString((JSONArray) value);
            }
        }
        return result;
    }

    private static String jsonToString(JSONArray source) {
        String result = "";
        for (Object value : source) {
            if ((value instanceof String))
                result = result + value;
            else if ((value instanceof JSONObject))
                result = result + jsonToString((JSONObject) value);
            else if ((value instanceof JSONArray))
                result = result + jsonToString((JSONArray) value);
        }
        return result;
    }

    private static String jsonToString(String json) {
        JSONObject jsonObject = (JSONObject) JSONValue.parse(json);
        if ((jsonObject == null) || (json.isEmpty())) return json;
        JSONArray array = (JSONArray) jsonObject.get("extra");
        if ((array == null) || (array.isEmpty())) return json;
        return jsonToString(array);
    }

    private static String textToString(String message) {
        String text = message;
        if (text.matches("^\\{\"text\":\".*\"\\}")) {
            text = text.replaceAll("^\\{\"text\":\"", "");
            text = text.replaceAll("\"\\}$", "");
            oldVersion16x = true;
        } else
            oldVersion16x = false;
        return ChatColor.stripColor(text);
    }

    private static String getStringValue(String key, String value) {
        if (key.equalsIgnoreCase("text")) {
            return value;
        }
        if (key.equalsIgnoreCase("color")) {
            return getStringColor(value);
        }
        return "";
    }

    private static String getBooleanValue(String key, boolean value) {
        if (!value) {
            return "";
        }
        if (key.equalsIgnoreCase("bold")) {
            return ChatColor.BOLD.toString();
        }
        if (key.equalsIgnoreCase("italic")) {
            return ChatColor.ITALIC.toString();
        }
        if (key.equalsIgnoreCase("underlined")) {
            return ChatColor.UNDERLINE.toString();
        }
        if (key.equalsIgnoreCase("strikethrough")) {
            return ChatColor.STRIKETHROUGH.toString();
        }
        if (key.equalsIgnoreCase("obfuscated")) {
            return ChatColor.MAGIC.toString();
        }
        return "";
    }

    private static String getStringColor(String colorName) {
        ChatColor[] arrayOfChatColor;
        int j = (arrayOfChatColor = ChatColor.values()).length;
        for (int i = 0; i < j; i++) {
            ChatColor c = arrayOfChatColor[i];
            if (c.name().equalsIgnoreCase(colorName)) {
                return c.toString();
            }
        }
        return "";
    }
}

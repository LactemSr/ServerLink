package serverlink.chat.nametag;

import serverlink.alphaplayer.PlayerManager;
import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import java.util.*;

public class Nametag {
    private static final HashMap<String, FakeTeam> CACHED_FAKE_TEAMS = new HashMap<>();
    private static final HashMap<FakeTeam, List<String>> FAKE_TEAMS = new HashMap<>();
    private static int id = 0;

    public static void setPrefix(Player player, String prefix) {
        updateNametag(player, FakeTeamUtils.format(prefix), FakeTeamUtils.format(PlayerManager.get(player).getNametagSuffix()));
    }

    public static void setSuffix(Player player, String suffix) {
        updateNametag(player, FakeTeamUtils.format(PlayerManager.get(player).getNametagPrefix()), FakeTeamUtils.format(suffix));
    }

    public static void updateNametag(Player player, String prefix, String suffix) {
        prefix = prefix == null || prefix.isEmpty() ? PlayerManager.get(player).getNametagPrefix() : prefix;
        suffix = suffix == null || suffix.isEmpty() ? PlayerManager.get(player).getNametagSuffix() : suffix;
        addPlayerToTeam(player.getName(), getFakeTeam(prefix, suffix));
    }

    public static FakeTeam getFakeTeam(String prefix, String suffix) {
        for (FakeTeam fakeTeam : FAKE_TEAMS.keySet()) {
            if (fakeTeam.getPrefix().equals(prefix) && fakeTeam.getSuffix().equals(suffix)) {
                return fakeTeam;
            }
        }
        return createFakeTeam("team" + ++id, prefix, suffix);
    }

    private static FakeTeam createFakeTeam(String teamName, String prefix, String suffix) {
        FakeTeam fakeTeam = new FakeTeam(teamName, prefix, suffix);
        FAKE_TEAMS.put(fakeTeam, new ArrayList<>());
        addTeamPackets(fakeTeam);
        return fakeTeam;
    }

    private static void addTeamPackets(FakeTeam fakeTeam) {
        new PacketWrapper(fakeTeam.getName(), fakeTeam.getPrefix(), fakeTeam.getSuffix(), 0, new ArrayList<>()).send();
    }

    private static void removeTeamPackets(FakeTeam fakeTeam) {
        new PacketWrapper(fakeTeam.getName(), fakeTeam.getPrefix(), fakeTeam.getSuffix(), 1, new ArrayList<>()).send();
    }

    private static boolean removePlayerFromTeamPackets(FakeTeam fakeTeam, String... players) {
        return removePlayerFromTeamPackets(fakeTeam, Arrays.asList(players));
    }

    private static boolean removePlayerFromTeamPackets(FakeTeam fakeTeam, List<String> players) {
        new PacketWrapper(fakeTeam.getName(), 4, players).send();
        List<String> members = FAKE_TEAMS.get(fakeTeam);
        members.removeAll(players);
        return members.isEmpty();
    }

    /**
     * Sends packets to every online player that say: the given player is
     * joining the given team
     */
    private static void addPlayerToTeamPackets(FakeTeam fakeTeam, String player) {
        new PacketWrapper(fakeTeam.getName(), 3, Arrays.asList(player)).send();
    }

    public static void addPlayerToTeam(String player, final FakeTeam fakeTeam) {
        FakeTeam previous = CACHED_FAKE_TEAMS.get(player);
        List<String> members = FAKE_TEAMS.get(fakeTeam);
        if (previous != null && previous.isSimilar(fakeTeam) && members != null && members.contains(player)) {
            return;
        }
        reset(player, CACHED_FAKE_TEAMS.remove(player));
        if (members != null) {
            members.add(player);
            Player adding = Bukkit.getPlayerExact(player);
            if (adding != null) {
                addPlayerToTeamPackets(fakeTeam, adding.getName());
                CACHED_FAKE_TEAMS.put(adding.getName(), fakeTeam);
            } else {
                OfflinePlayer offlinePlayer = Bukkit.getOfflinePlayer(player);
                addPlayerToTeamPackets(fakeTeam, offlinePlayer.getName());
                CACHED_FAKE_TEAMS.put(offlinePlayer.getName(), fakeTeam);
            }
        }
    }

    private static FakeTeam reset(String player, FakeTeam fakeTeam) {
        List<String> members = FAKE_TEAMS.get(fakeTeam);
        if (members != null && members.remove(player)) {
            boolean delete;
            Player removing = Bukkit.getPlayerExact(player);
            if (removing != null) {
                delete = removePlayerFromTeamPackets(fakeTeam, removing.getName());
            } else {
                OfflinePlayer toRemoveOffline = Bukkit.getOfflinePlayer(player);
                delete = removePlayerFromTeamPackets(fakeTeam, toRemoveOffline.getName());
            }

            if (delete) {
                removeTeamPackets(fakeTeam);
                FAKE_TEAMS.remove(fakeTeam);
            }
        }
        return fakeTeam;
    }

    public static void sendTeams(Player player) {
        for (Map.Entry<FakeTeam, List<String>> entry : FAKE_TEAMS.entrySet()) {
            FakeTeam fakeTeam = entry.getKey();
            new PacketWrapper(fakeTeam.getName(), fakeTeam.getPrefix(), fakeTeam.getSuffix(), 0, new ArrayList<>()).send(player);
            new PacketWrapper(fakeTeam.getName(), 3, entry.getValue()).send(player);
        }
    }

    public FakeTeam reset(String player) {
        return reset(player, CACHED_FAKE_TEAMS.get(player));
    }
}

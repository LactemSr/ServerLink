package serverlink.chat;

public enum ChatFormat {
    GRAY,
    WHITE,
    AQUA,
    BLUE,
    RED,
    UNICODE,
    GREEN,
    LIGHT_PURPLE,
    DARK_GREEN,
    DARK_RED,
    DARK_AQUA,
    YELLOW,
    BOLD,
    ITALIC,
    GOLD;

    public static ChatFormat getFromString(String s) {
        switch (s) {
            case "bold":
                return BOLD;
            case "italic":
                return ITALIC;
            case "blue":
                return BLUE;
            case "green":
                return GREEN;
            case "dark_green":
                return DARK_GREEN;
            case "red":
                return RED;
            case "dark_red":
                return DARK_RED;
            case "aqua":
                return AQUA;
            case "dark_aqua":
                return DARK_AQUA;
            case "light_purple":
                return LIGHT_PURPLE;
            case "yellow":
                return YELLOW;
            case "white":
                return WHITE;
            case "gray":
            case "":
                return GRAY;
            case "gold":
                return GOLD;
            case "unicode":
            case "fancy":
                return UNICODE;
        }
        return UNICODE;
    }
}

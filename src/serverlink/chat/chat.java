package serverlink.chat;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.player.Settings;
import serverlink.round.Round;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class chat implements Round {
    private static boolean deathMessages = false;
    private static String joinLeaveMessages = "all";
    private static String commandUnavailable;

    public static String color(String s) {
        return ChatColor.translateAlternateColorCodes('&', s);
    }

    public static boolean getDeathMessagesEnabled() {
        return deathMessages;
    }

    public static void setDeathMessagesEnabled(boolean enabled) {
        deathMessages = enabled;
    }

    /**
     * returns "all", "donors", or "none"
     */
    public static String getJoinLeaveMessagesStatus() {
        return joinLeaveMessages;
    }

    public static void setJoinLeaveMessages(String allDonorsOrNone) {
        joinLeaveMessages = allDonorsOrNone;
    }

    public static void setCommandUnavailable(String s) {
        commandUnavailable = s;
    }

    // get commandUnavailable string
    public static String gcu() {
        return commandUnavailable;
    }

    public static String getServer() {
        return ServerLink.getFancyServerName();
    }

    public static String unicode(String s) {
        String returnString = "";
        boolean skip = false;
        boolean skipNext = false;
        for (int t = 0; t < s.length(); t++) {
            if (s.charAt(t) == '�') {
                skip = true;
                skipNext = true;
            }
            if (!skip) {
                if (s.charAt(t) == '/') {
                    returnString = returnString + '?';
                } else if (s.charAt(t) == ' ') {
                    returnString = returnString + ' ';
                } else if (s.charAt(t) == '[') {
                    returnString = returnString + '?';
                } else if (s.charAt(t) == ']') {
                    returnString = returnString + '?';
                } else if (s.charAt(t) == '(') {
                    returnString = returnString + '?';
                } else if (s.charAt(t) == ')') {
                    returnString = returnString + '?';
                } else if ((s.charAt(t) <= '') && (s.charAt(t) >= ' ')) {
                    returnString =
                            returnString + (char) (65248 + s.charAt(t));
                }
            } else {
                returnString = returnString + s.charAt(t);
                if (!skipNext) {
                    skip = false;
                } else {
                    skipNext = false;
                }
            }
        }
        return returnString;
    }

    // colors and unicodes
    public static String formatChatMessage(Player player, String message) {
        // remove player-inputted coloring and formatting
        message = ChatColor.stripColor(chat.color(message));
        if (message.isEmpty()) message = "&";
        // add coloring and formatting from player's settings
        String format = (String) PlayerManager.get(player).getData("chatString", "").getValue();
        if (!format.contains("unicode")) return format + message;
        format = format.replaceAll("unicode", "");
        return format + unicode(message);
    }

    // converts ChatFormat to a readily usable string that is stored for async chat event
    public static void calculateChatString(AlphaPlayer aPlayer) {
        List<String> formatChats = aPlayer.getListSetting("chatFormat");
        ArrayList<ChatFormat> formats = new ArrayList<>();
        formats.addAll(formatChats.stream().map(ChatFormat::valueOf).collect(Collectors.toList()));
        // for first-timers
        if (formats.isEmpty()) {
            formats.add(ChatFormat.GRAY);
            formatChats.add(ChatFormat.GRAY.name());
            aPlayer.setSetting_SyncOnLogout("chatFormat", Settings.listToString(formatChats));
        }
        // for players who upgraded rank since last join
        if (aPlayer.getRank() > 1 && formats.contains(ChatFormat.GRAY)) {
            formats.clear();
            formats.add(ChatFormat.WHITE);
            formatChats.clear();
            formatChats.add(ChatFormat.WHITE.name());
            aPlayer.setSetting_SyncOnLogout("chatFormat", Settings.listToString(formatChats));
        }
        // for players who downgraded rank since last join
        if (aPlayer.getRank() == 1 && (!formats.contains(ChatFormat.GRAY) || formats.size() > 1)) {
            formats.clear();
            formats.add(ChatFormat.GRAY);
            formatChats.clear();
            formatChats.add(ChatFormat.GRAY.name());
            aPlayer.setSetting_SyncOnLogout("chatFormat", Settings.listToString(formatChats));
        }
        // set the data
        aPlayer.getData("chatFormat").setValue(formatChats);
        // now we begin actually calculating the chat string
        // put bold and italics at the end
        if (formats.contains(ChatFormat.BOLD)) {
            formats.remove(ChatFormat.BOLD);
            formats.add(ChatFormat.BOLD);
        }
        if (formats.contains(ChatFormat.ITALIC)) {
            formats.remove(ChatFormat.ITALIC);
            formats.add(ChatFormat.ITALIC);
        }
        String s = "";
        for (ChatFormat format : formats) {
            switch (format) {
                case BOLD:
                    s = s + ChatColor.BOLD;
                    break;
                case ITALIC:
                    s = s + ChatColor.ITALIC;
                    break;
                case BLUE:
                    s = ChatColor.BLUE + s;
                    break;
                case GREEN:
                    s = ChatColor.GREEN + s;
                    break;
                case DARK_GREEN:
                    s = ChatColor.DARK_GREEN + s;
                    break;
                case RED:
                    s = ChatColor.RED + s;
                    break;
                case DARK_RED:
                    s = ChatColor.DARK_RED + s;
                    break;
                case AQUA:
                    s = ChatColor.AQUA + s;
                    break;
                case DARK_AQUA:
                    s = ChatColor.DARK_AQUA + s;
                    break;
                case LIGHT_PURPLE:
                    s = ChatColor.LIGHT_PURPLE + s;
                    break;
                case YELLOW:
                    s = ChatColor.YELLOW + s;
                    break;
                case WHITE:
                    s = ChatColor.WHITE + s;
                    break;
                case GRAY:
                    s = ChatColor.GRAY + s;
                    break;
                case GOLD:
                    s = ChatColor.GOLD + s;
                    break;
                case UNICODE:
                    s = s + "unicode";
                    break;
            }
        }
        aPlayer.getData("chatString").setValue(s);
    }

    @Override
    public void onLobbyCountdownStart() {

    }

    @Override
    public void onLobbyCountdownCancel() {

    }

    @Override
    public void onMapCountdownStart() {
    }

    @Override
    public void onMapCountdownCancel() {
    }

    @Override
    public void onGameStart() {
        for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
            if (!aPlayer.isFullyLoggedIn()) continue;
            aPlayer.updateNameFormatting();
        }
    }

    @Override
    public void onEndCountdownStart() {

    }

    @Override
    public void onRoundEnd() {
        for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
            if (!aPlayer.isFullyLoggedIn()) continue;
            aPlayer.updateNameFormatting();
        }
    }
}

package serverlink.command;

import org.bukkit.entity.Player;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.player.Friends;
import serverlink.player.Settings;
import serverlink.server.Messages;

import java.util.UUID;

public class FriendsCommand {
    @CommandManager.CommandHandler(name = "friends", usage = Messages.friends_help, aliases = {"friend", "f"}, async = true)
    public void base(Player player, String[] args) {
        Friends.displayFriends(player, 1);
    }

    @CommandManager.SubCommandHandler(base = "friends", name = "list", aliases = {"display", "show"}, async = true)
    public void list(Player player, String[] args) {
        if (args.length == 0) {
            Friends.displayFriends(player, 1);
        } else {
            int page = 1;
            try {
                page = Integer.parseInt(args[0]);
            } catch (NumberFormatException ignored) {
            }
            if (page < 1) page = 1;
            Friends.displayFriends(player, page);
        }
    }

    @CommandManager.SubCommandHandler(base = "friends", name = "requests", aliases = {"invites"}, async = true)
    public void requests(Player player, String[] args) {
        Friends.displayRequests(player);
    }

    @CommandManager.SubCommandHandler(base = "friends", name = "add", aliases = {"invite", "request", "accept", "confirm"}, async = false, args = 1)
    public void add(Player player, String[] args) {
        AlphaPlayer aPlayer = PlayerManager.get(player);
        UUID friend = ServerLink.getUUIDFromName(args[0].toLowerCase(), false);
        if (friend == null) {
            player.sendMessage(chat.color(chat.getServer() + Messages.friends_error_invalidPlayer));
            return;
        }
        // the two are already friends
        if (Friends.getFriendsObject(aPlayer).contains(friend)) {
            player.sendMessage(chat.color(chat.getServer() + Messages.friends_error_alreadyFriends));
            return;
        }
        // the player is trying to invite himself
        if (friend == player.getUniqueId()) {
            player.sendMessage(chat.color(chat.getServer() + Messages.friends_error_friendSelf));
            return;
        }
        // accepting a received invite
        if (Friends.getReceivedRequestsObject(aPlayer).get(friend) != null) {
            if (Friends.getFriendsObject(aPlayer).size() >= 50) {
                player.sendMessage(chat.color(chat.getServer() + Messages.friends_error_maxFriendsReached));
                return;
            }
            Friends.actionAcceptFriend(player, friend);
            return;
        }
        // player already sent a request in the last five minutes
        if (Friends.getSentRequestsObject(aPlayer).get(friend) != null) {
            player.sendMessage(chat.color(chat.getServer() + Messages.friends_error_inviteAlreadySent));
            return;
        }
        // sending an invite to another player
        // other player has friend requests disabled
        Boolean invitesEnabled = Settings.getBooleanSetting(friend.toString(), "friendRequestsEnabled");
        if (invitesEnabled == null || !invitesEnabled) {
            player.sendMessage(chat.color(chat.getServer() + Messages.friends_error_notAcceptingRequests));
            return;
        }
        Friends.actionInviteFriend(player, friend);
    }

    @CommandManager.SubCommandHandler(base = "friends", name = "remove", aliases = {"kick", "deny", "cancel", "delete", "erase", "reject"}, async = false, args = 1)
    public void remove(Player player, String[] args) {
        AlphaPlayer aPlayer = PlayerManager.get(player);
        UUID friend = ServerLink.getUUIDFromName(args[0].toLowerCase(), false);
        if (friend == null) {
            player.sendMessage(chat.color(chat.getServer() + Messages.friends_error_invalidPlayer));
            return;
        }
        // denying a friend request
        if (Friends.getReceivedRequestsObject(aPlayer).get(friend) != null) {
            Friends.actionDenyFriendRequest(player, friend);
            return;
        }
        // the two are already not friends
        if (!Friends.getFriendsObject(aPlayer).contains(friend)) {
            player.sendMessage(chat.color(chat.getServer() + Messages.friends_error_cantRemoveOrDeny));
            return;
        }
        // removing a friend
        Friends.actionRemoveFriend(player, friend);
    }
}

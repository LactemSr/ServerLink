package serverlink.command;

import com.google.common.collect.Lists;
import org.apache.commons.lang.StringUtils;
import org.bukkit.command.CommandSender;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.network.ncount;
import serverlink.player.Permissions;
import serverlink.util.TimeUtils;

import java.util.ArrayList;
import java.util.List;

public class ListCommand {
    private static String cachedList = "";
    private long lastCalc = TimeUtils.getCurrent();

    private void calculate() {
        String ownerString = "";
        String adminString = "";
        String modString = "";
        String tmodString = "";
        String ytString = "";
        String fourthString = "";
        String thirdString = "";
        String secondString = "";
        String firstString = "";
        String defString = "";
        for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
            int rank = aPlayer.getRank();
            if (rank == 10)
                ownerString = ownerString.concat(aPlayer.getPlayer().getDisplayName()
                        .replaceFirst(Permissions.getRankName(10) + " ", "") + ",");
            else if (rank == 9)
                adminString = adminString.concat(aPlayer.getPlayer().getDisplayName()
                        .replaceFirst(Permissions.getRankName(9) + " ", "") + ",");
            else if (rank == 8)
                modString = modString.concat(aPlayer.getPlayer().getDisplayName()
                        .replaceFirst(Permissions.getRankName(8) + " ", "") + ",");
            else if (rank == 7)
                tmodString = tmodString.concat(aPlayer.getPlayer().getDisplayName()
                        .replaceFirst(Permissions.getRankName(7) + " ", "") + ",");
            else if (rank == 6)
                ytString = ytString.concat(aPlayer.getPlayer().getDisplayName()
                        .replaceFirst(Permissions.getRankName(6) + " ", "") + ",");
            else if (rank == 5)
                fourthString = fourthString.concat(aPlayer.getPlayer().getDisplayName()
                        .replaceFirst(Permissions.getRankName(5) + " ", "") + ",");
            else if (rank == 4)
                thirdString = thirdString.concat(aPlayer.getPlayer().getDisplayName()
                        .replaceFirst(Permissions.getRankName(4) + " ", "") + ",");
            else if (rank == 3)
                secondString = secondString.concat(aPlayer.getPlayer().getDisplayName()
                        .replaceFirst(Permissions.getRankName(3) + " ", "") + ",");
            else if (rank == 2)
                secondString = secondString.concat(aPlayer.getPlayer().getDisplayName()
                        .replaceFirst(Permissions.getRankName(2) + " ", "") + ",");
            else defString = defString.concat(aPlayer.getPlayer().getDisplayName() + ",");
        }
        if (ownerString.isEmpty()) ownerString = "&7none";
        else {
            ArrayList<String> list = Lists.newArrayList(ownerString.split(","));
            if (list.size() > 5) {
                list.add(5, "&7and &f" + (list.size() - 5) + "&7 more...");
                while (list.size() > 6) list.remove(6);
            }
            ownerString = StringUtils.join(list, "&r, ");
        }
        if (adminString.length() == 0) adminString = "&7none";
        else {
            List<String> list = Lists.newArrayList(adminString.split(","));
            if (list.size() > 5) {
                list.add(5, "&7and &f" + (list.size() - 5) + "&7 more...");
                while (list.size() > 6) list.remove(6);
            }
            adminString = StringUtils.join(list, "&r, ");
        }
        if (modString.length() == 0) modString = "&7none";
        else {
            List<String> list = Lists.newArrayList(modString.split(","));
            if (list.size() > 5) {
                list.add(5, "&7and &f" + (list.size() - 5) + "&7 more...");
                while (list.size() > 6) list.remove(6);
            }
            modString = StringUtils.join(list, "&r, ");
        }
        if (tmodString.length() == 0) tmodString = "&7none";
        else {
            List<String> list = Lists.newArrayList(tmodString.split(","));
            if (list.size() > 5) {
                list.add(5, "&7and &f" + (list.size() - 5) + "&7 more...");
                while (list.size() > 6) list.remove(6);
            }
            tmodString = StringUtils.join(list, "&r, ");
        }
        if (ytString.length() == 0) ytString = "&7none";
        else {
            List<String> list = Lists.newArrayList(ytString.split(","));
            if (list.size() > 5) {
                list.add(5, "&7and &f" + (list.size() - 5) + "&7 more...");
                while (list.size() > 6) list.remove(6);
            }
            ytString = StringUtils.join(list, "&r, ");
        }
        if (fourthString.length() == 0) fourthString = "&7none";
        else {
            List<String> list = Lists.newArrayList(fourthString.split(","));
            if (list.size() > 5) {
                list.add(5, "&7and &f" + (list.size() - 5) + "&7 more...");
                while (list.size() > 6) list.remove(6);
            }
            fourthString = StringUtils.join(list, "&r, ");
        }
        if (thirdString.length() == 0) thirdString = "&7none";
        else {
            List<String> list = Lists.newArrayList(thirdString.split(","));
            if (list.size() > 5) {
                list.add(5, "&7and &f" + (list.size() - 5) + "&7 more...");
                while (list.size() > 6) list.remove(6);
            }
            thirdString = StringUtils.join(list, "&r, ");
        }
        if (secondString.length() == 0) secondString = "&7none";
        else {
            List<String> list = Lists.newArrayList(secondString.split(","));
            if (list.size() > 5) {
                list.add(5, "&7and &f" + (list.size() - 5) + "&7 more...");
                while (list.size() > 6) list.remove(6);
            }
            secondString = StringUtils.join(list, "&r, ");
        }
        if (firstString.length() == 0) firstString = "&7none";
        else {
            List<String> list = Lists.newArrayList(firstString.split(","));
            if (list.size() > 5) {
                list.add(5, "&7and &f" + (list.size() - 5) + "&7 more...");
                while (list.size() > 6) list.remove(6);
            }
            firstString = StringUtils.join(list, "&r, ");
        }
        if (defString.length() == 0) defString = "&7none";
        else {
            List<String> list = Lists.newArrayList(defString.split(","));
            if (list.size() > 5) {
                list.add(5, "&7and &f" + (list.size() - 5) + "&7 more...");
                while (list.size() > 6) list.remove(6);
            }
            defString = StringUtils.join(list, "&r, ");
        }
        cachedList = "&6There are currently &c" + ncount.getCount() + "&6/&c" + ncount.getMaxPlayers()
                + " &6players on your server.\n&6&lOwners&r&7: &f" + ownerString + "\n&4&lAdmins&r&7: &f"
                + adminString + "\n&b&lModerators&r&7: &f" + modString + "\n&b&lTrial Moderators&r&7: &f"
                + tmodString + "\n&2&lYoutubers&r&7: &f" + ytString + "\n&c&lSaints&r&7: &f" + fourthString
                + "\n&9&lElites&r&7: &f" + thirdString + "\n&d&lEpics&r&7: &f" + secondString
                + "\n&a&lBosses&r&7: &f" + firstString + "\n&d&lPlayers&r&7: &f" + defString;
    }

    @CommandManager.CommandHandler(name = "list", aliases = {"who"}, async = false, sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(CommandSender sender, String[] args) {
        long now = TimeUtils.getCurrent();
        if (now - lastCalc > 1) {
            calculate();
            lastCalc = now;
        }
        sender.sendMessage(chat.color(cachedList));
    }
}

package serverlink.command;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.network.JedisPool;
import serverlink.player.Settings;
import serverlink.player.party.PartyManager;
import serverlink.server.Messages;

import java.util.List;
import java.util.UUID;

public class PartyCommand {
    @CommandManager.CommandHandler(name = "party", aliases = {"p", "parties"}, async = false, sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(Player player, String[] args) {
        AlphaPlayer aPlayer = PlayerManager.get(player);
        if (args.length == 0) {
            player.sendMessage(chat.color(Messages.party_help));
            return;
        } else if (args[0].equalsIgnoreCase("help") || args[0].equalsIgnoreCase("?")) {
            player.sendMessage(chat.color(Messages.party_help));
            return;
        }
        if (args[0].equalsIgnoreCase("create")) {
            player.sendMessage(chat.color(chat.getServer() + Messages.party_usage_create));
        } else if (args[0].equalsIgnoreCase("join")) {
            player.sendMessage(chat.color(chat.getServer() + Messages.party_usage_join));
        } else if (args[0].equalsIgnoreCase("invite")) {
            player.sendMessage(chat.color(chat.getServer() + Messages.party_usage_invite));
        } else if (args[0].equalsIgnoreCase("leave")) {
            if (PartyManager.getPartyObject(aPlayer).isEmpty()) {
                // player is not in a party
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_notInParty));
            } else if (PartyManager.getPartyObject(aPlayer).get(0).equals(player.getUniqueId())) {
                // leader leaving own party = disbanding
                PartyManager.actionDisband(player);
            } else {
                // player leaving his party
                PartyManager.actionLeave(player, PartyManager.getPartyObject(aPlayer).get(0));
            }
        } else if (args[0].equalsIgnoreCase("warp") || args[0].equalsIgnoreCase("tp") || args[0].equalsIgnoreCase("teleport") || args[0].equalsIgnoreCase("summon")) {
            if (PartyManager.getPartyObject(aPlayer).isEmpty()) {
                // player is not in a party
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_notInParty));
            } else if (!PartyManager.getPartyObject(aPlayer).get(0).equals(player.getUniqueId())) {
                // a member is trying to warp his leader's party
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_notLeader));
            } else {
                // the leader is warping his party
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        PartyManager.actionWarp(player);
                    }
                }.runTaskAsynchronously(ServerLink.plugin);
            }
        } else if (args[0].equalsIgnoreCase("disband")) {
            if (PartyManager.getPartyObject(aPlayer).isEmpty()) {
                // player is not in a party
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_notInParty));
            } else if (!PartyManager.getPartyObject(aPlayer).get(0).equals(player.getUniqueId())) {
                // a member is trying to disband his leader's party
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_notLeader));
            } else {
                // the leader is disbanding his party
                PartyManager.actionDisband(player);
            }
        } else if (args[0].equalsIgnoreCase("accept")) {
            if (args.length != 2) {
                player.sendMessage(chat.color(chat.getServer() + Messages.party_usage_accept));
                return;
            }
            UUID leader = ServerLink.getUUIDFromName(args[1].toLowerCase(), false);
            if (leader == null) {
                // invalid player
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_playerNotFound));
                return;
            }
            if (!PartyManager.getReceivedInvitesObject(aPlayer).containsKey(leader)) {
                // player is trying to join a party he wasn't invited to
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_notInvited));
                return;
            }
            Jedis jedis = JedisPool.getConn();
            List<String> members = jedis.lrange("partyMembers:" + leader.toString(), 0, -1);
            JedisPool.close(jedis);
            if (members == null || members.size() >= 10) {
                // the party the player is trying to join is full
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_partyFull));
                return;
            }
            PartyManager.actionJoin(player, leader);
        } else if (args[0].equalsIgnoreCase("kick")) {
            if (args.length != 2) {
                player.sendMessage(chat.color(chat.getServer() + Messages.party_usage_kick));
                return;
            }
            if (PartyManager.getPartyObject(aPlayer).isEmpty()) {
                // player is not in a party
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_notInParty));
                return;
            }
            UUID playerToBeKicked = ServerLink.getUUIDFromName(args[1].toLowerCase(), false);
            if (playerToBeKicked == null) {
                // invalid player
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_playerNotFound));
                return;
            }
            if (!PartyManager.getPartyObject(aPlayer).contains(playerToBeKicked)) {
                // the player to be kicked is not in the party
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_targetPlayerNotInParty));
                return;
            }
            if (!PartyManager.getPartyObject(aPlayer).get(0).equals(player.getUniqueId())) {
                // the player trying to kick someone is not the party leader
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_notLeader));
                return;
            }
            if (player.getUniqueId().equals(playerToBeKicked)) {
                // the leader is trying to kick himself
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_kickSelf));
                return;
            }
            // the leader is kicking a player from the party
            PartyManager.actionKick(player, playerToBeKicked);
        } else if (args[0].equalsIgnoreCase("list")) {
            if (PartyManager.getPartyObject(aPlayer).isEmpty()) {
                // player is not in a party
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_notInParty));
            } else new BukkitRunnable() {
                @Override
                public void run() {
                    PartyManager.listPartyMembers(player);
                }
            }.runTaskAsynchronously(ServerLink.plugin);
        } else {
            if (args.length != 1) {
                player.sendMessage(chat.color(Messages.party_help));
                return;
            }
            UUID targetPlayer = ServerLink.getUUIDFromName(args[0].toLowerCase(), false);
            if (targetPlayer == null) {
                // invalid player
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_playerNotFound));
                return;
            }
            if (targetPlayer.equals(player.getUniqueId())) {
                // player is trying to invite himself
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_inviteSelf));
                return;
            }
            // player is trying to join a party
            if (PartyManager.getReceivedInvitesObject(aPlayer).containsKey(targetPlayer)) {
                if (!PartyManager.getPartyObject(aPlayer).isEmpty()) {
                    // player is already in another party
                    player.sendMessage(chat.color(chat.getServer() + Messages.party_error_joinAlreadyInParty));
                    return;
                }
                Jedis jedis = JedisPool.getConn();
                List<String> members = jedis.lrange("partyMembers:" + targetPlayer.toString(), 0, -1);
                JedisPool.close(jedis);
                if (members == null || members.size() >= 10) {
                    // the party the player is trying to join is full
                    player.sendMessage(chat.color(chat.getServer() + Messages.party_error_partyFull));
                    return;
                }
                PartyManager.actionJoin(player, targetPlayer);
                return;
            }
            // player is trying to invite someone he already invited
            else if (PartyManager.getSentInvitesObject(aPlayer).containsKey(targetPlayer)) {
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_inviteCooldown));
                return;
            }
            // creating a party - doesn't require leader because there is no leader
            else if (PartyManager.getPartyObject(aPlayer).isEmpty()) {
                Jedis jedis = JedisPool.getConn();
                boolean targetPlayerOnline = jedis.hget(targetPlayer.toString(), "online").equals("true");
                if (!targetPlayerOnline) {
                    JedisPool.close(jedis);
                    player.sendMessage(chat.color(chat.getServer() + Messages.party_error_targetPlayerNotOnline));
                    return;
                }
                Boolean invitesEnabled = Settings.getBooleanSetting(targetPlayer.toString(), "partyInvitesEnabled");
                if (invitesEnabled == null || !invitesEnabled) {
                    JedisPool.close(jedis);
                    player.sendMessage(chat.color(chat.getServer() + Messages.party_error_notAcceptingInvites));
                    return;
                }
                String targetParty = jedis.hget(targetPlayer.toString(), "party");
                if (targetParty == null || targetParty.isEmpty()) {
                    JedisPool.close(jedis);
                    PartyManager.actionInvite(player, targetPlayer);
                    return;
                }
                JedisPool.close(jedis);
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_inviteAlreadyInOtherParty));
                return;
            }
            // return if not party leader (the only options left are inviting someone to an existing party)
            else if (!PartyManager.getPartyObject(aPlayer).get(0).equals(player.getUniqueId())) {
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_notLeader));
                return;
            }
            if (PartyManager.getPartyObject(aPlayer).contains(targetPlayer)) {
                //player is trying to invite someone who is already in his party
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_inviteAlreadyInParty));
                return;
            }
            if (PartyManager.getPartyObject(aPlayer).size() >= 10) {
                // player's party already has 10 players
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_maxPlayersReached));
                return;
            }
            // player is inviting a valid player
            Jedis jedis = JedisPool.getConn();
            boolean targetPlayerOnline = jedis.hget(targetPlayer.toString(), "online").equals("true");
            if (!targetPlayerOnline) {
                JedisPool.close(jedis);
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_targetPlayerNotOnline));
                return;
            }
            Boolean invitesEnabled = Settings.getBooleanSetting(targetPlayer.toString(), "partyInvitesEnabled");
            if (invitesEnabled == null || !invitesEnabled) {
                JedisPool.close(jedis);
                player.sendMessage(chat.color(chat.getServer() + Messages.party_error_notAcceptingInvites));
                return;
            }
            String targetParty = jedis.hget(targetPlayer.toString(), "party");
            if (targetParty == null || targetParty.isEmpty()) {
                JedisPool.close(jedis);
                PartyManager.actionInvite(player, targetPlayer);
                return;
            }
            JedisPool.close(jedis);
            player.sendMessage(chat.color(chat.getServer() + Messages.party_error_inviteAlreadyInOtherParty));
        }
    }
}
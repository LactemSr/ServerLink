package serverlink.command;


import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.network.JedisOne;

public class GlistCommand {
    private static String cachedList = "";
    private static int globalCount = 0;
    private static int hubCount = 0;
    private static int skyblockCount = 0;
    private static int skybattleCount = 0;
    private static int kitpvpCount = 0;

    public GlistCommand() {
        new BukkitRunnable() {
            @Override
            public void run() {
                refresh();
            }
        }.runTaskTimerAsynchronously(ServerLink.plugin, 0, 18);
    }

    public static int getGlobalCount() {
        return globalCount;
    }

    public static int getHubCount() {
        return hubCount;
    }

    public static int getSkyblockCount() {
        return skyblockCount;
    }

    public static int getSkybattleCount() {
        return skybattleCount;
    }

    public static int getKitpvpCount() {
        return kitpvpCount;
    }

    private void refresh() {
        Jedis jedis = JedisOne.getTwo();
        try {
            globalCount = Integer.parseInt(jedis.get("globalCount"));
        } catch (NumberFormatException ignored) {
        }
        try {
            hubCount = Integer.parseInt(jedis.get("hubCount"));
        } catch (NumberFormatException ignored) {
        }
        try {
            skyblockCount = Integer.parseInt(jedis.get("skyblockmetroCount")) + Integer.parseInt(jedis.get("skyblockislandsCount"));
        } catch (NumberFormatException ignored) {
        }
        try {
            skybattleCount = Integer.parseInt(jedis.get("skybattleCount"));
        } catch (NumberFormatException ignored) {
        }
        try {
            kitpvpCount = Integer.parseInt(jedis.get("kitpvpCount"));
        } catch (NumberFormatException ignored) {
        }
        JedisOne.closeTwo();
        cachedList = chat.color(
                "&6&lNetwork:&r&f " + globalCount
                        + "\n&bHubs:&r&f " + hubCount
                        + "\n&bSkyblock:&r&f " + skyblockCount
                        + "\n&bSkyBattle:&r&f " + skybattleCount
                        + "\n&bKitPvp:&r&f " + kitpvpCount);
    }

    @CommandManager.CommandHandler(name = "glist", aliases = {"wlist", "globallist", "globalist", "worldlist", "nlist", "networklist"}, async = false, sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(CommandSender sender, String[] args) {
        sender.sendMessage(chat.color(cachedList));
    }
}

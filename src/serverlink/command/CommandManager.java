package serverlink.command;

import com.sun.istack.internal.NotNull;
import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerCommandPreprocessEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashMap;

public class CommandManager implements CommandExecutor, Listener {
    // if you use the SubCommand annotation, you also have to use the CommandHandler annotation

    private final static HashMap<Command, Object> handlers = new HashMap<>();
    private final static HashMap<Command, Method> methods = new HashMap<>();
    private final static HashMap<String, SubCommand> subCommands = new HashMap<>();
    private final static HashMap<String, Object> subHandlers = new HashMap<>();
    private final static HashMap<String, Method> subMethods = new HashMap<>();
    private final static HashMap<String, SubCommand> subCommandAliases = new HashMap<>();
    private final static HashMap<String, String> aliases = new HashMap<>();
    private final static HashMap<Player, String> cooldown = new HashMap<>();

    private static boolean isCommandHandler(Method method) {
        return method.getAnnotation(CommandHandler.class) != null;
    }

    private static boolean isSubCommandHandler(Method method) {
        return method.getAnnotation(SubCommandHandler.class) != null;
    }

    public static void resetSpamCooldown(Player player) {
        cooldown.remove(player);
    }

    public void registerCommands(@NotNull JavaPlugin plugin, @NotNull Object handler) {

        for (Method method : handler.getClass().getMethods()) {
            Class<?>[] params = method.getParameterTypes();
            if (params.length == 2 && CommandSender.class.isAssignableFrom(params[0]) && String[].class.equals(params[1])) {

                if (isCommandHandler(method)) {
                    CommandHandler annotation = method.getAnnotation(CommandHandler.class);
                    if (plugin.getCommand(annotation.name()) != null) {
                        plugin.getCommand(annotation.name()).setExecutor(this);
                        if (!Arrays.equals(annotation.aliases(), new String[]{""})) {
                            for (String s : annotation.aliases()) aliases.put(s, annotation.name());
                        }
                        handlers.put(plugin.getCommand(annotation.name()), handler);
                        methods.put(plugin.getCommand(annotation.name()), method);
                    }
                    continue;
                }

                if (isSubCommandHandler(method)) {
                    SubCommandHandler annotation = method.getAnnotation(SubCommandHandler.class);
                    if (plugin.getCommand(annotation.base()) != null) {
                        SubCommand subCommand = new SubCommand(plugin.getCommand(annotation.base()), annotation.name());
                        subCommand.aliases = annotation.aliases();
                        subCommand.permission = annotation.permission();
                        subCommand.async = annotation.async();
                        subCommand.args = annotation.args();
                        for (String s : subCommand.aliases)
                            subCommandAliases.put(annotation.base() + s, subCommand);
                        subCommands.put(subCommand.toString(), subCommand);
                        subHandlers.put(subCommand.toString(), handler);
                        subMethods.put(subCommand.toString(), method);
                    }
                }
            }
        }
    }

    private SubCommand getSubCommand(String subCommand) {
        subCommand = subCommand.toLowerCase();
        SubCommand sub = subCommands.get(subCommand);
        if (sub != null) return sub;
        return subCommandAliases.get(subCommand);
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            if (!PlayerManager.get((Player) sender).isFullyLoggedIn()) return true;
            if (cooldown.get(sender) != null) {
                sender.sendMessage(chat.color(chat.getServer() + "Please do not spam!"));
                return true;
            }
        }
        if (args.length > 0) {
            SubCommand subCommand = new SubCommand(command, args[0].toLowerCase());
            subCommand = getSubCommand(subCommand.toString());
            if (subCommand != null) {
                Object subHandler = subHandlers.get(subCommand.toString());
                Method subMethod = subMethods.get(subCommand.toString());
                if (subHandler != null && subMethod != null) {
                    // Reorder the arguments so we don't resend the subcommand
                    String[] subArgs = new String[args.length - 1];
                    System.arraycopy(args, 1, subArgs, 0, args.length - 1);
                    if (subMethod.getParameterTypes()[0].equals(Player.class) && !(sender instanceof Player)) {
                        sender.sendMessage(ChatColor.RED + "Only players can use this command.");
                        return true;
                    }
                    if (subMethod.getParameterTypes()[0].equals(ConsoleCommandSender.class) && !(sender instanceof ConsoleCommandSender)) {
                        sender.sendMessage(ChatColor.RED + "Only console users can use this command.");
                        return true;
                    }
                    if (!subCommand.permission.isEmpty() && sender instanceof Player && !sender.hasPermission(subCommand.permission)) {
                        sender.sendMessage(chat.color(chat.getServer() + chat.gcu()));
                        return true;
                    }
                    if (subCommand.args != 0 && subCommand.args != subArgs.length) {
                        sender.sendMessage(chat.color(methods.get(command).getAnnotation(CommandHandler.class).usage()));
                        return true;
                    }
                    try {
                        if (subCommand.async) {
                            new BukkitRunnable() {
                                @Override
                                public void run() {
                                    try {
                                        subMethod.invoke(subHandler, sender, subArgs);
                                    } catch (IllegalAccessException | InvocationTargetException e) {
                                        sender.sendMessage(ChatColor.RED + "An error occurred while trying to process the command (0)");
                                        e.printStackTrace();
                                    }
                                }
                            }.runTaskAsynchronously(ServerLink.plugin);
                        } else subMethod.invoke(subHandler, sender, subArgs);
                    } catch (Exception e) {
                        sender.sendMessage(ChatColor.RED + "An error occurred while trying to process the command");
                        e.printStackTrace();
                    }
                    if (sender instanceof Player) {
                        cooldown.put((Player) sender, sender.getName());
                        new BukkitRunnable() {
                            @Override
                            public void run() {
                                cooldown.remove(sender);
                            }
                        }.runTaskLater(ServerLink.plugin, 19);
                    }
                    return true;
                }
            }
        }
        // If a subcommand was successfully executed, the command will not reach this point

        Object handler = handlers.get(command);
        Method method = methods.get(command);
        if (handler != null && method != null) {
            CommandHandler annotation = method.getAnnotation(CommandHandler.class);
            if (method.getParameterTypes()[0].equals(Player.class) && !(sender instanceof Player)) {
                sender.sendMessage(ChatColor.RED + "Only players can use this command.");
                return true;
            }
            if (method.getParameterTypes()[0].equals(ConsoleCommandSender.class) && !(sender instanceof ConsoleCommandSender)) {
                sender.sendMessage(ChatColor.RED + "Only console users can use this command.");
                return true;
            }
            if (!annotation.permission().isEmpty() && sender instanceof Player && !sender.hasPermission(annotation.permission())) {
                sender.sendMessage(chat.color(chat.getServer() + chat.gcu()));
                return true;
            }
            if (annotation.sendUsageMessageOnUnrecognizedSubCommand() && args.length != 0) {
                sender.sendMessage(chat.color(annotation.usage()));
                return true;
            }
            try {
                if (method.getAnnotation(CommandHandler.class).async()) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            try {
                                method.invoke(handler, sender, args);
                            } catch (IllegalAccessException | InvocationTargetException e) {
                                sender.sendMessage(ChatColor.RED + "An error occurred while trying to process the command (0)");
                                e.printStackTrace();
                            }
                        }
                    }.runTaskAsynchronously(ServerLink.plugin);
                } else method.invoke(handler, sender, args);
            } catch (Exception e) {
                sender.sendMessage(ChatColor.RED + "An error occurred while trying to process the command");
                e.printStackTrace();
            }
        }
        if (sender instanceof Player) {
            cooldown.put((Player) sender, sender.getName());
            new BukkitRunnable() {
                @Override
                public void run() {
                    cooldown.remove(sender);
                }
            }.runTaskLater(ServerLink.plugin, 19);
        }
        return true;
    }

    @EventHandler
    private void onPreCommand(PlayerCommandPreprocessEvent e) {
        String args[] = e.getMessage().split(" ");
        String base = aliases.get(args[0].toLowerCase().replace("/", ""));
        if (base == null) return;
        String[] newArray = Arrays.copyOfRange(args, 1, args.length);
        StringBuilder builder = new StringBuilder("");
        for (String s1 : newArray) builder.append(" ").append(s1);
        e.setMessage("/" + base + builder.toString());
    }

    @Retention(RetentionPolicy.RUNTIME)
    public @interface CommandHandler {
        String name();

        String[] aliases() default {""};

        String usage() default "Incorrect usage";

        boolean async();

        String permission() default "";

        // set to false when using dynamic sub commands (like player names)
        // remember that parameters are not sub commands. a sub command is the second argument only
        boolean sendUsageMessageOnUnrecognizedSubCommand() default true;
    }

    /**
     * An annotation interface that may be attached to a method to designate it as a subcommand handler.
     * When registering a handler with this class, only methods marked with this annotation will be considered for subcommand registration.
     */
    @Retention(RetentionPolicy.RUNTIME)
    public @interface SubCommandHandler {
        String base();

        String name();

        // invoke method asynchronously?
        boolean async();

        // how many args must the sender input to trigger the SubCommand (0 means always trigger)?
        int args() default 0;

        String[] aliases() default {""};

        String permission() default "";
    }

    private static class SubCommand {
        final Command superCommand;
        final String subCommand;
        String[] aliases;
        String permission;
        int args;
        boolean async;

        SubCommand(Command superCommand, String subCommand) {
            this.superCommand = superCommand;
            this.subCommand = subCommand.toLowerCase();
        }

        public String toString() {
            return superCommand.getName() + subCommand;
        }
    }
}
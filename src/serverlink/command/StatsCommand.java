package serverlink.command;

import org.bukkit.entity.Player;
import serverlink.alphaplayer.PlayerManager;
import serverlink.inventory.gui.stats.MainStatsGui;

public class StatsCommand {
    @CommandManager.CommandHandler(name = "stats", aliases = {"stat", "statistics", "baltop", "balancetop", "leaderboard", "leaderboards", "topstats"}, async = false, sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(Player player, String[] args) {
        MainStatsGui.openMainStatsGui(PlayerManager.get(player));
    }
}
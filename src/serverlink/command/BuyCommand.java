package serverlink.command;

import mkremins.fanciful.FancyMessage;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import serverlink.util.CommandText;

public class BuyCommand {
    private static final FancyMessage shopLink = new FancyMessage("You can visit our shop by clicking the link:\n   ")
            .color(ChatColor.GREEN).then("shop.alphacraft.us ").color(ChatColor.GOLD).style(ChatColor.BOLD)
            .tooltip("Click to visit the shop").link("http://shop.alphacraft.us");

    @CommandManager.CommandHandler(name = "buy", aliases = {"purchase", "shop", "store", "package", "packages", "buyrank", "ranks", "buycoins"}, async = false, sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(Player player, String[] args) {
        CommandText.sendMessage(player, shopLink.toJSONString());
    }
}

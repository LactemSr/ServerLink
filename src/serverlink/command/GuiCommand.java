package serverlink.command;

import org.bukkit.entity.Player;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.inventory.gui.GameSelector;
import serverlink.inventory.gui.HubSelector;
import serverlink.inventory.gui.profile.ProfileGui;
import serverlink.inventory.gui.profile.ReferralsGui;
import serverlink.inventory.gui.settings.ChatSettingsGui;
import serverlink.inventory.gui.settings.ColorPickerGui;
import serverlink.inventory.gui.settings.SettingsGui;
import serverlink.inventory.gui.stats.MainStatsGui;

public class GuiCommand {
    private static final String help = "\n&aAvailable Guis: &esettings, profile, lobbyselector, gameselector, chatsettings, speedsettings, colorpicker, referrals, stats\n";


    @CommandManager.CommandHandler(name = "gui", usage = help, aliases = {"interface", "menu", "panel"}, async = false)
    public void base(Player player, String[] args) {
        player.sendMessage(chat.color(help));
    }

    @CommandManager.SubCommandHandler(base = "gui", name = "settings", aliases = {"mysettings", "my-settings", "my_settings"}, async = false)
    public void settings(Player player, String[] args) {
        player.openInventory(SettingsGui.inv);
    }

    @CommandManager.SubCommandHandler(base = "gui", name = "profile", aliases = {"myprofile", "my-profile", "my_profile"}, async = false)
    public void profile(Player player, String[] args) {
        ProfileGui.openGui(PlayerManager.get(player));
    }

    @CommandManager.SubCommandHandler(base = "gui", name = "colorpicker", aliases = {"color_picker", "color-picker", "colorchooser", "color-chooser", "color_chooser"}, async = false)
    public void colorPicker(Player player, String[] args) {
        ColorPickerGui.createGui(player).openPage(1);
    }

    @CommandManager.SubCommandHandler(base = "gui", name = "hubselector", aliases = {"hub-selector", "hub_selector", "lobbyselector", "lobby-selector", "lobby_selector"}, async = false)
    public void hubSelector(Player player, String[] args) {
        HubSelector.createGui(player).openPage(1);
    }

    @CommandManager.SubCommandHandler(base = "gui", name = "gameselector", aliases = {"game-selector", "game_selector"}, async = false)
    public void gameSelector(Player player, String[] args) {
        GameSelector.createGui(player).openPage(1);
    }

    @CommandManager.SubCommandHandler(base = "gui", name = "referrals", aliases = {"referralsmenu", "referralmenu", "referals", "referral", "referal", "referralsgui", "referalgui", "referalsgui"}, async = false)
    public void referrals(Player player, String[] args) {
        ReferralsGui.createMainMenu(PlayerManager.get(player)).openPage(1);
    }

    @CommandManager.SubCommandHandler(base = "gui", name = "stats", aliases = {"statsmenu", "statmenu", "leaderboards", "leaderboard"}, async = false)
    public void stats(Player player, String[] args) {
        MainStatsGui.openMainStatsGui(PlayerManager.get(player));
    }

    @CommandManager.SubCommandHandler(base = "gui", name = "boosters", aliases = {"booster", "gametypeboosters", "gametypebooster", "gametype_boosters", "gametype_booster", "gametype-boosters", "gametype-booster", "gameboosters", "gamebooster", "game_booster", "game_boosters", "game-booster", "game-booster", "networkboosters", "networkbooster", "network_boosters", "network_booster", "network-booster", "network-boosters", "boostersmenu", "boostermenu", "boosters_menu", "booster_menu", "booster-menu", "boosters-menu"}, async = false)
    public void boosters(Player player, String[] args) {
        // TODO
    }

    @CommandManager.SubCommandHandler(base = "gui", name = "chatsettings", aliases = {"chat-settings", "chat_settings", "nameandchatsettings", "namesettings", "name-settings", "name_settings", "colorsettings", "color-settings", "color_settings"}, async = false)
    public void chatSettings(Player player, String[] args) {
        ChatSettingsGui.createGui(player).openPage(1);
    }
}

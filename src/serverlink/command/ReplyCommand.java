package serverlink.command;

import com.google.common.io.ByteArrayDataOutput;
import com.google.common.io.ByteStreams;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.bot.Bot;
import serverlink.bot.BotManager;
import serverlink.chat.ChatFilter;
import serverlink.chat.chat;
import serverlink.customevent.CustomPlayerMessageBotEvent;
import serverlink.network.JedisPool;
import serverlink.player.Permissions;
import serverlink.player.Settings;
import serverlink.server.Messages;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.util.UUID;

public class ReplyCommand {
    private static UUID getUUIDFromName(String name, AlphaPlayer aPlayer, Jedis jedis) {
        Object cachedUUID = aPlayer.getData("cachedUUIDOf:" + name.toLowerCase()).getValue();
        if (cachedUUID != null)
            return (UUID) cachedUUID;
        String uuidFromRedis = jedis.get(name.toLowerCase());
        if (uuidFromRedis != null && !uuidFromRedis.isEmpty()) {
            UUID uuid = UUID.fromString(uuidFromRedis);
            new BukkitRunnable() {
                @Override
                public void run() {
                    aPlayer.getData("cachedUUIDOf:" + name.toLowerCase()).setValue(uuid);
                }
            }.runTask(ServerLink.plugin);
            return uuid;
        }
        return null;
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @CommandManager.CommandHandler(name = "reply", aliases = {"r", "re"}, async = true, sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(Player player, String[] args) {
        AlphaPlayer aPlayer = PlayerManager.get(player);
        UUID lastMessaged = (UUID) aPlayer.getData("lastMessaged").getValue();
        if (args.length == 0) {
            player.sendMessage(chat.color(Messages.messaging_help));
            return;
        }
        if (lastMessaged == null) {
            player.sendMessage(chat.color(chat.getServer() + Messages.messaging_noLastReply));
            return;
        }
        if (args[0].toLowerCase().equals(player.getName().toLowerCase())) {
            player.sendMessage(chat.color(chat.getServer() + Messages.messaging_cantMessageSelf));
            return;
        }
        if (!aPlayer.getBooleanSetting("privateMessagesEnabled")) {
            player.sendMessage(chat.color("&cYou have private messages turned off in your settings.\n" +
                    "&cYou can turn them back on by typing &6/gui ChatSettings &cand clicking the Enderpearl."));
            return;
        }
        Player receiverPlayer = Bukkit.getPlayer(lastMessaged);
        Bot bot = null;
        if (receiverPlayer == null || receiverPlayer.hasMetadata("NPC")) {
            for (Bot b : BotManager.getBots()) {
                if (b.getNpc().isSpawned() && b.getNpc().getEntity().getUniqueId() == lastMessaged) {
                    receiverPlayer = (Player) b.getNpc().getEntity();
                    bot = b;
                    break;
                }
            }
        }
        if (receiverPlayer != null && (receiverPlayer.isOnline() || receiverPlayer.hasMetadata("NPC"))) {
            // receiver is on the same server as sender
            if (!aPlayer.getBooleanSetting("privateMessagesEnabled")) {
                player.sendMessage(chat.color("&cYou have private messages turned off in your settings.\n" +
                        "You can turn them back on by typing &6/gui ChatSettings &cand clicking the Enderpearl."));
                return;
            }
            AlphaPlayer rAlphaPlayer = PlayerManager.get(lastMessaged);
            while (!rAlphaPlayer.isFullyLoggedIn()) ;
            if (!rAlphaPlayer.getBooleanSetting("privateMessagesEnabled")) {
                player.sendMessage(chat.color(chat.getServer() + Messages.messaging_notAcceptingPms));
                return;
            }
            // send messages both ways inside this server only
            String message = "";
            for (String s : args) message = message + " " + s;
            message = ChatFilter.filter(message.trim());
            String playerName = aPlayer.getRank() == 1 ? player.getDisplayName() : Permissions.getRankName(aPlayer.getRank()) +
                    "&r " + player.getDisplayName();
            String receiverName = rAlphaPlayer.getRank() == 1 ? receiverPlayer.getDisplayName() : Permissions.getRankName(rAlphaPlayer.getRank()) +
                    "&r " + receiverPlayer.getDisplayName();
            receiverPlayer.sendMessage(chat.color(playerName + " &r&7&l>> &f&rYou: "
                    + chat.formatChatMessage(player, message)));
            player.sendMessage(chat.color("&fYou&7&l >>&f&r " + receiverName + ": "
                    + chat.formatChatMessage(player, message)));
            if (rAlphaPlayer.getBooleanSetting("chatMentionsEnabled"))
                receiverPlayer.playSound(receiverPlayer.getLocation(), Sound.NOTE_PLING, 20, 20);
            if (rAlphaPlayer.getData("lastMessaged").getValue() == null)
                rAlphaPlayer.getData("lastMessaged").setValue(player.getUniqueId());
            if (bot != null) Bukkit.getPluginManager().callEvent(new CustomPlayerMessageBotEvent(player, bot, message));
        } else {
            // Sender and receiver are on different servers
            Jedis jedis = JedisPool.getConn();
            UUID receiver = getUUIDFromName(args[0], aPlayer, jedis);
            if (receiver == null || jedis.hget(receiver.toString(), "online").equals("false")) {
                JedisPool.close(jedis);
                player.sendMessage(chat.color(chat.getServer() + Messages.messaging_lastMessagedOffline));
                return;
            }
            // The setting won't be null since the receiver is online, and the setting is loaded pre-join
            //noinspection ConstantConditions
            if (!Settings.getBooleanSetting(receiver.toString(), "privateMessagesEnabled")) {
                JedisPool.close(jedis);
                player.sendMessage(chat.color(chat.getServer() + Messages.messaging_notAcceptingPms));
                return;
            }
            String message = "";
            for (int i = 1; i < args.length; i++) message = message + " " + args[i];
            message = ChatFilter.filter(message.trim());
            String playerName = aPlayer.getRank() == 1 ? player.getDisplayName() : Permissions.getRankName(aPlayer.getRank()) +
                    "&r " + player.getDisplayName();
            int receiverRank;
            if (aPlayer.getData("receiverRank:" + receiver.toString()).getValue() != null) {
                receiverRank = (int) aPlayer.getData("receiverRank:" + receiver.toString()).getValue();
            } else {
                receiverRank = Integer.parseInt(jedis.hget(receiver.toString(), "rank"));
                aPlayer.getData("receiverRank:" + receiver.toString()).setValue(receiverRank);
            }

            // Send message to receiver through bungeecord
            String finalMessage = message;
            new BukkitRunnable() {
                @Override
                public void run() {
                    ByteArrayDataOutput out = ByteStreams.newDataOutput();
                    out.writeUTF("ForwardToPlayer");
                    out.writeUTF(ServerLink.getNameFromAlphaPlayer(aPlayer, receiver));
                    out.writeUTF("PrivateMessage");

                    ByteArrayOutputStream msgbytes = new ByteArrayOutputStream();
                    DataOutputStream msgout = new DataOutputStream(msgbytes);
                    try {
                        msgout.writeUTF(receiver.toString());
                        msgout.writeUTF(chat.color(playerName + " &r&7&l>> &f&rYou: "
                                + chat.formatChatMessage(player, finalMessage)));
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                    out.writeShort(msgbytes.toByteArray().length);
                    out.write(msgbytes.toByteArray());
                    player.sendPluginMessage(ServerLink.plugin, "BungeeCord", out.toByteArray());
                }
            }.runTask(ServerLink.plugin);

            // Send the sender a message so he knows it was sent
            String receiverDisplayName;
            if (aPlayer.getData("receiverDisplayName:" + receiver.toString()).getValue() != null) {
                receiverDisplayName = (String) aPlayer.getData("receiverDisplayName:" + receiver.toString()).getValue();
            } else {
                receiverDisplayName = jedis.hget(receiver.toString(), "displayName");
                aPlayer.getData("receiverDisplayName:" + receiver.toString()).setValue(receiverDisplayName);
            }
            JedisPool.close(jedis);
            String receiverName = receiverRank == 1 ? receiverDisplayName : Permissions.getRankName(receiverRank) +
                    "&r " + receiverDisplayName;
            player.sendMessage(chat.color("&fYou&7&l >>&f&r " + receiverName + ": "
                    + chat.formatChatMessage(player, message)));
            aPlayer.getData("lastMessaged").setValue(receiver);
        }
    }
}

package serverlink.command.admin;

import serverlink.chat.chat;
import serverlink.command.CommandManager;
import org.bukkit.command.CommandSender;

public class DeathmessagesCommand {
    private final static String usage = "Usage: &6/deathmessages on&7|&6off";

    @CommandManager.CommandHandler(name = "deathmessages", usage = usage, permission = "rank.8", async = false, aliases = {"deathmessage", "diemessages", "diemessage", "deathnotifications", "deathnotificaton"})
    public void base(CommandSender sender, String[] args) {
        sender.sendMessage(chat.color(usage));
    }

    @CommandManager.SubCommandHandler(base = "deathmessages", name = "on", permission = "rank.8", aliases = {"enable", "enabled"}, async = false)
    public void on(CommandSender sender, String[] args) {
        if (chat.getDeathMessagesEnabled())
            sender.sendMessage(chat.color(chat.getServer() + "&aDeath messages already enabled."));
        else {
            sender.sendMessage(chat.color(chat.getServer() + "&aEnabled death messages."));
            chat.setDeathMessagesEnabled(true);
        }
    }

    @CommandManager.SubCommandHandler(base = "deathmessages", name = "off", permission = "rank.8", aliases = {"disable", "disabled"}, async = false)
    public void off(CommandSender sender, String[] args) {
        if (!chat.getDeathMessagesEnabled())
            sender.sendMessage(chat.color(chat.getServer() + "&aDeath messages already disabled"));
        else {
            sender.sendMessage(chat.color(chat.getServer() + "&aDisabled death messages."));
            chat.setDeathMessagesEnabled(false);
        }
    }
}

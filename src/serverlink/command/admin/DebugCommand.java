package serverlink.command.admin;

import serverlink.command.CommandManager;
import serverlink.network.navail;
import serverlink.network.ncount;
import serverlink.network.nstatus;
import serverlink.server.Reset;
import org.bukkit.command.CommandSender;

public class DebugCommand {
    @CommandManager.CommandHandler(name = "debug", async = false, sendUsageMessageOnUnrecognizedSubCommand = false, permission = "rank.9")
    public void base(CommandSender sender, String[] args) {
        if (nstatus.isLobby()) sender.sendMessage("status: lobby");
        else if (nstatus.isLobbyCountdown()) sender.sendMessage("status: lobby countdown");
        else if (nstatus.isGame()) sender.sendMessage("status: game");
        else if (nstatus.isEndCountdown()) sender.sendMessage("status: end countdown");
        sender.sendMessage("ncount:" + ncount.getCount());
        sender.sendMessage("startPlayers: " + ncount.getStartPlayers());
        sender.sendMessage("maxPlayers: " + ncount.getMaxPlayers());
        if (navail.isOpen()) sender.sendMessage("availability: open");
        else if (navail.isClosed()) sender.sendMessage("availability: closed");
        sender.sendMessage("reset:" + Reset.ttl);
    }
}

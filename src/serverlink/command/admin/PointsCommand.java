package serverlink.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import redis.clients.jedis.Jedis;
import serverlink.command.CommandManager;
import serverlink.network.JedisPool;
import serverlink.player.Points;
import serverlink.util.UUIDFetcher;

import java.util.ArrayList;
import java.util.List;

public class PointsCommand {
    private static final String usage = "Usage: /points <add | take> <player> <amount> <gametype>";

    @CommandManager.CommandHandler(name = "points", usage = usage, aliases = {"coin"}, async = false, permission = "rank.10")
    public void base(CommandSender sender, String[] args) {
        sender.sendMessage(usage);
    }

    @CommandManager.SubCommandHandler(base = "points", name = "add", async = true, args = 3, permission = "rank.10")
    public void add(CommandSender sender, String[] args) {
        Integer points;
        try {
            points = Integer.parseInt(args[1]);
        } catch (NumberFormatException e1) {
            sender.sendMessage(usage);
            return;
        }
        if (points < 0) {
            sender.sendMessage("Points must be a positive value.");
            return;
        }
        List<String> uuidAndName = getUUIDAndName(args[0]);
        if (uuidAndName == null) {
            sender.sendMessage("That player does not exist!");
            return;
        }
        String uuid = uuidAndName.get(0);
        String name = uuidAndName.get(1);
        Points.addPoints(uuid, args[2].toLowerCase(), points);
        sender.sendMessage("Points added to " + name + " in " + args[2].toLowerCase() + ".");
    }

    @CommandManager.SubCommandHandler(base = "points", name = "take", aliases = {"remove", "subtract"}, async = true, args = 3, permission = "rank.10")
    public void take(CommandSender sender, String[] args) {
        Integer points;
        try {
            points = Integer.parseInt(args[1]);
        } catch (NumberFormatException e1) {
            sender.sendMessage(usage);
            return;
        }
        if (points < 0) {
            sender.sendMessage("Points must be a positive value.");
            return;
        }
        List<String> uuidAndName = getUUIDAndName(args[0]);
        if (uuidAndName == null) {
            sender.sendMessage("That player does not exist!");
            return;
        }
        String uuid = uuidAndName.get(0);
        String name = uuidAndName.get(1);
        Points.takePoints(uuid, args[2].toLowerCase(), points);
        sender.sendMessage("Points taken from " + name + " in " + args[2].toLowerCase() + ".");
    }

    private List<String> getUUIDAndName(String input) {
        input = input.toLowerCase();
        final Player player = Bukkit.getPlayerExact(input);
        if (player != null && player.isOnline()) {
            return new ArrayList<String>() {{
                add(player.getUniqueId().toString());
                add(player.getName());
            }};
        } else {
            Jedis jedis = JedisPool.getConn();
            String uuid = jedis.get(input.toLowerCase());
            if (uuid != null && uuid.length() > 8) {
                String name = jedis.hget(uuid, "username");
                JedisPool.close(jedis);
                return new ArrayList<String>() {{
                    add(uuid);
                    add(name);
                }};
            } else {
                JedisPool.close(jedis);
                try {
                    List<String> uuidAndName = UUIDFetcher.getOfflineUUIDAndName(input);
                    if (uuidAndName != null) return uuidAndName;
                    else {
                        return null;
                    }
                } catch (Exception e) {
                    return null;
                }
            }
        }
    }
}

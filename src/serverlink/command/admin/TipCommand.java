package serverlink.command.admin;

import org.bukkit.command.CommandSender;
import redis.clients.jedis.Jedis;
import serverlink.chat.chat;
import serverlink.command.CommandManager;
import serverlink.network.JedisOne;

import java.util.List;

public class TipCommand {
    private final String usage = "&aAdding a tip: &6/tip add &e<gametype> &e<tip message>\n" +
            "&aRemoving a tip: &6/tip remove &e<gametype> &e<index>\n" +
            "&aListing tips: &6/tip list &e<gametype>";

    @CommandManager.CommandHandler(name = "tip", aliases = {"tips"}, usage = usage, async = true, permission = "rank.10", sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage(chat.color(usage));
            return;
        }
        if (args[0].equalsIgnoreCase("add") || args[0].equalsIgnoreCase("create")) {
            if (args.length < 3) {
                sender.sendMessage(chat.color(usage));
                return;
            }
            String gametype = args[1].toLowerCase();
            String tip = "";
            for (int i = 2; i < args.length; i++) tip = tip + args[i] + " ";
            tip = tip.trim();
            Jedis jedis = JedisOne.getTwo();
            jedis.lpush(gametype + "Tips", tip);
            JedisOne.closeTwo();
            sender.sendMessage("Added " + gametype + " tip: " + chat.color(tip));
        } else if (args[0].equalsIgnoreCase("remove") || args[0].equalsIgnoreCase("delete")) {
            if (args.length < 3) {
                sender.sendMessage(chat.color(usage));
                return;
            }
            String gametype = args[1].toLowerCase();
            int index;
            try {
                index = Integer.parseInt(args[2]);
            } catch (NumberFormatException e) {
                sender.sendMessage("Index must be a number!");
                return;
            }
            if (index < 0) {
                sender.sendMessage("Index must be positive!");
                return;
            }
            Jedis jedis = JedisOne.getTwo();
            List<String> tips = jedis.lrange(gametype + "Tips", 0, -1);
            if (index > tips.size() - 1) {
                JedisOne.closeTwo();
                sender.sendMessage(gametype + "only has tips through index " + (tips.size() - 1));
                return;
            }
            jedis.lrem(gametype + "Tips", 0, tips.get(index));
            JedisOne.closeTwo();
            sender.sendMessage("Removed " + gametype + " tip: " + chat.color(tips.get(index)));
        } else if (args[0].equalsIgnoreCase("list")) {
            if (args.length == 1) {
                sender.sendMessage(chat.color(usage));
                return;
            }
            String gametype = args[1].toLowerCase();
            Jedis jedis = JedisOne.getTwo();
            List<String> tips = jedis.lrange(gametype + "Tips", 0, -1);
            JedisOne.closeTwo();
            if (tips == null || tips.isEmpty()) sender.sendMessage("No tips for " + gametype + " servers.");
            else {
                sender.sendMessage("Tips for " + gametype + " servers:");
                int index = 0;
                for (String tip : tips) {
                    sender.sendMessage(index + " - " + tip);
                    sender.sendMessage(chat.color("&c" + index + "&r - " + tip));
                    index++;
                }
            }
        } else sender.sendMessage(chat.color(usage));
    }

}

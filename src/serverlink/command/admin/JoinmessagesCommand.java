package serverlink.command.admin;

import serverlink.chat.chat;
import serverlink.command.CommandManager;
import org.bukkit.command.CommandSender;

public class JoinmessagesCommand {
    private final static String usage = "Usage: &6/joinmessages all&7|&6donors&7|&6none";

    @CommandManager.CommandHandler(name = "joinmessages", usage = usage, permission = "rank.8", async = false, aliases = {"joinmessage", "loginmessages", "loginmessage", "joinnotifications", "joinotifications", "joinnotificaton", "joinotification"})
    public void base(CommandSender sender, String[] args) {
        sender.sendMessage(chat.color(usage));
    }

    @CommandManager.SubCommandHandler(base = "joinmessages", name = "all", permission = "rank.8", aliases = {"enable", "enabled", "on"}, async = false)
    public void all(CommandSender sender, String[] args) {
        if (chat.getJoinLeaveMessagesStatus().equals("all"))
            sender.sendMessage(chat.color(chat.getServer() + "&cJoin/leave messages already set to \"all\"."));
        else {
            sender.sendMessage(chat.color(chat.getServer() + "&aJoin/leave messages updated to \"all\"."));
            chat.setJoinLeaveMessages("all");
        }
    }

    @CommandManager.SubCommandHandler(base = "joinmessages", name = "none", permission = "rank.8", aliases = {"disable", "disabled", "off"}, async = false)
    public void none(CommandSender sender, String[] args) {
        if (chat.getJoinLeaveMessagesStatus().equals("none"))
            sender.sendMessage(chat.color(chat.getServer() + "&cJoin/leave messages already set to \"none\"."));
        else {
            sender.sendMessage(chat.color(chat.getServer() + "&aJoin/leave messages updated to \"none\"."));
            chat.setJoinLeaveMessages("none");
        }
    }

    @CommandManager.SubCommandHandler(base = "joinmessages", name = "donors", permission = "rank.8", aliases = {"donor", "rank", "ranked", "ranks"}, async = false)
    public void donors(CommandSender sender, String[] args) {
        if (chat.getJoinLeaveMessagesStatus().equals("donors"))
            sender.sendMessage(chat.color(chat.getServer() + "&cJoin/leave messages already set to \"donors\"."));
        else {
            sender.sendMessage(chat.color(chat.getServer() + "&aJoin/leave messages updated to \"donors\"."));
            chat.setJoinLeaveMessages("donors");
        }
    }
}
package serverlink.command.admin;

import serverlink.command.CommandManager;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class TpallCommand {
    @CommandManager.CommandHandler(name = "tpall", usage = "Usage: /tpall", async = false, permission = "rank.9")
    public void base(Player player, String[] args) {
        Bukkit.getOnlinePlayers().stream().filter(p -> p != player).forEach(p -> p.performCommand("hub"));
    }
}
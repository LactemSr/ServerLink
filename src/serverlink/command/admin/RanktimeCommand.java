package serverlink.command.admin;

import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.command.CommandManager;
import serverlink.network.JedisPool;
import serverlink.player.RankExpiry;
import org.bukkit.command.CommandSender;
import redis.clients.jedis.Jedis;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import java.util.UUID;

public class RanktimeCommand {
    private final static String usage = "Usage: &6/ranktime add &e<player> <rank 2-5> <days> &rOR &6/ranktime" +
            " subtract &e<player> <days> &rOR &6/ranktime get &e<player> &e<rank 2-5>";

    @CommandManager.CommandHandler(name = "ranktime", async = true, permission = "rank.10", usage = usage)
    public void base(CommandSender sender, String[] args) {
        sender.sendMessage(chat.color(usage));
    }

    @CommandManager.SubCommandHandler(base = "ranktime", name = "add", async = true, permission = "rank.10", args = 3, aliases = {"give"})
    public void add(CommandSender sender, String[] args) {
        UUID target = ServerLink.getUUIDFromName(args[0], true);
        if (target == null) {
            sender.sendMessage(chat.color(chat.getServer() + "Invalid player."));
            return;
        }
        int tier;
        try {
            tier = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            sender.sendMessage(chat.color(chat.getServer() + "Your second argument must be tier 2, 3, 4, or 5."));
            return;
        }
        if (tier < 1 || tier > 10) {
            sender.sendMessage(chat.color(chat.getServer() + "Your second argument must be tier 2, 3, 4, or 5."));
            return;
        }
        if (tier < 2 || tier > 5) {
            sender.sendMessage(chat.color(chat.getServer() + "Use the /setrank command for ranks 1 and 6-10 " +
                    "(the permanent ranks)."));
            return;
        }
        int days;
        try {
            days = Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            sender.sendMessage(chat.color(chat.getServer() + "Your third argument should be the number of days to add."));
            return;
        }
        if (days < 1) {
            sender.sendMessage(chat.color(chat.getServer() + "Days to add must be a positive number."));
            return;
        }
        RankExpiry.addTime(target.toString(), tier, days);
        sender.sendMessage("Success");
    }

    @CommandManager.SubCommandHandler(base = "ranktime", name = "subtract", async = true, permission = "rank.10", args = 2, aliases = {"take", "remove"})
    public void subtract(CommandSender sender, String[] args) {
        UUID target = ServerLink.getUUIDFromName(args[0], true);
        if (target == null) {
            sender.sendMessage(chat.color(chat.getServer() + "Invalid player."));
            return;
        }
        int days;
        try {
            days = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            sender.sendMessage(chat.color(chat.getServer() + "Your second argument should be the number of days to subtract."));
            return;
        }
        if (days < 1) {
            sender.sendMessage(chat.color(chat.getServer() + "Days to subtract must be a positive number."));
            return;
        }
        Jedis jedis = JedisPool.getConn();
        int current5;
        try {
            current5 = Integer.parseInt(jedis.hget(target.toString(), "rank5Expiry"));
        } catch (NumberFormatException e) {
            current5 = 0;
        }
        int current4;
        try {
            current4 = Integer.parseInt(jedis.hget(target.toString(), "rank4Expiry"));
        } catch (NumberFormatException e) {
            current4 = 0;
        }
        int current3;
        try {
            current3 = Integer.parseInt(jedis.hget(target.toString(), "rank3Expiry"));
        } catch (NumberFormatException e) {
            current3 = 0;
        }
        int current2;
        try {
            current2 = Integer.parseInt(jedis.hget(target.toString(), "rank2Expiry"));
        } catch (NumberFormatException e) {
            current2 = 0;
        }
        int currentTier;
        int currentExpiry;
        if (current5 > 0) {
            currentTier = 5;
            currentExpiry = current5;
        } else if (current4 > 0) {
            currentTier = 4;
            currentExpiry = current4;
        } else if (current3 > 0) {
            currentTier = 3;
            currentExpiry = current3;
        } else if (current2 > 0) {
            currentTier = 2;
            currentExpiry = current2;
        } else {
            // you can't subtract days if the player doesn't already have a donor rank
            sender.sendMessage("That player doesn't have any time left on a donor rank. Either the player has a " +
                    "permanent rank (rank 1 or one of ranks 6-10) or his/her donor rank has expired, but he/she " +
                    "has not yet logged in to have it fully applied.");
            JedisPool.close(jedis);
            return;
        }
        RankExpiry.subtractTime(target.toString(), currentTier, currentExpiry, days, jedis);
        JedisPool.close(jedis);
        sender.sendMessage("Success");
    }

    @CommandManager.SubCommandHandler(base = "ranktime", name = "get", async = true, permission = "rank.10", args = 2, aliases = {"list", "show", "display"})
    public void get(CommandSender sender, String[] args) {
        UUID target = ServerLink.getUUIDFromName(args[0], true);
        if (target == null) {
            sender.sendMessage(chat.color(chat.getServer() + "Invalid player."));
            return;
        }
        int tier;
        try {
            tier = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            sender.sendMessage(chat.color(chat.getServer() + "Your second argument must be rank 2, 3, 4, or 5."));
            return;
        }
        if (tier < 2 || tier > 5) {
            sender.sendMessage(chat.color(chat.getServer() + "Your second argument must be rank 2, 3, 4, or 5."));
            return;
        }
        Jedis jedis = JedisPool.getConn();
        Date expiryDate = RankExpiry.getExpiryDate(target.toString(), tier, jedis);
        if (expiryDate == null)
            sender.sendMessage("The player specified doesn't have any time left on the specified rank.");
        else {
            DateFormat df = new SimpleDateFormat("MMMM d, yyyy");
            df.setTimeZone(TimeZone.getTimeZone("America/New_York"));
            double days = (Long.parseLong(jedis.hget(target.toString(), "rank" + tier +
                    "Expiry")) - (System.currentTimeMillis() / 1000)) / 86400.0;
            sender.sendMessage("The specified player's rank (" + tier + ") expires in " + days + " days on: " + df.format(expiryDate));
        }
        JedisPool.close(jedis);
    }
}
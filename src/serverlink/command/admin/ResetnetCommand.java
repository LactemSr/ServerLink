package serverlink.command.admin;

import org.bukkit.command.CommandSender;
import redis.clients.jedis.Jedis;
import serverlink.command.CommandManager;
import serverlink.network.JedisOne;

public class ResetnetCommand {
    @CommandManager.CommandHandler(name = "resetnet", async = false, sendUsageMessageOnUnrecognizedSubCommand = false, permission = "rank.9")
    public void base(CommandSender sender, String[] args) {
        Jedis jedis = JedisOne.getOne();
        // clear server statuses
        jedis.del("status");
        // clear server availabilities
        jedis.del("availability");
        // clear server maps
        jedis.del("map");
        // clear individual server counts
        jedis.del("count");
        // clear type counts
        jedis.del("kitpvpCount");
        jedis.del("spleefCount");
        jedis.del("maCount");
        jedis.del("skyblockCount");
        jedis.del("skyblockhubCount");
        jedis.del("skyblockmetroCount");
        jedis.del("skyblockislandsCount");
        jedis.del("hubCount");
        jedis.del("buildCount");
        jedis.del("skybattleCount");
        jedis.del("testingCount");
        // clear globalCount
        jedis.del("globalCount");
        // tell servers to sync their counts; they get 5 minutes to do it (their tasks run every minute)
        jedis.set("reset", "value doesn't matter");
        jedis.expire("reset", 300);
        JedisOne.closeOne();
        sender.sendMessage(
                "Server status data and player count data cleared. Everything should be back to normal in under 2 minutes.");
    }
}

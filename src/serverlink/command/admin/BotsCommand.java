package serverlink.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.alphaplayer.PlayerManager;
import serverlink.bot.Bot;
import serverlink.bot.BotManager;
import serverlink.command.CommandManager;
import serverlink.map.MapManager;
import serverlink.network.ncount;
import serverlink.network.nstatus;

import java.util.SplittableRandom;

public class BotsCommand {
    private SplittableRandom r = new SplittableRandom();


    @CommandManager.CommandHandler(name = "bots", async = false, sendUsageMessageOnUnrecognizedSubCommand = false, permission = "rank.10")
    public void base(CommandSender sender, String[] args) {
        if (args.length == 0) {
            sender.sendMessage("Usage: /bots <#_of_bots>  OR  /bots dynamic");
            return;
        }
        if (BotManager.creatingOrDeletingBots) {
            sender.sendMessage(ChatColor.RED + "Please wait until the current queue of bot creation/deletion is complete.");
            return;
        }
        sender.sendMessage("Success");
        // determine bots to spawn and destroy
        int botsToSpawn = 0;
        int botsToDestroy = 0;
        if (args[0].equalsIgnoreCase("dynamic")) {
            BotManager.setDynamicBotCreationEnabled();
            if (PlayerManager.getOnlineAlphaPlayers().size() < ncount.getMaxPlayers() * 0.15) {
                botsToSpawn = (int) Math.ceil((ncount.getMaxPlayers() * 0.15) - PlayerManager.getOnlineAlphaPlayers().size());
            } else if (Bukkit.getOnlinePlayers().size() / ncount.getMaxPlayers() >= 0.5) {
                botsToDestroy = BotManager.getNumOfBots();
            } else if (PlayerManager.getOnlineAlphaPlayers().size() > ncount.getMaxPlayers() * 0.15 && Bukkit.getOnlinePlayers().size() / BotManager.getNumOfBots() < 2.0) {
                double botsToKeep = PlayerManager.getOnlineAlphaPlayers().size() / 2.0;
                botsToDestroy = (int) Math.ceil(BotManager.getNumOfBots() - botsToKeep);
            }
            if (botsToSpawn + BotManager.getNumOfBots() > 30) botsToSpawn = 30 - BotManager.getNumOfBots();
            if (botsToDestroy > BotManager.getNumOfBots()) botsToDestroy = BotManager.getNumOfBots();
        } else {
            BotManager.setDynamicBotCreationDisabled();
            int newNumOfBots;
            try {
                newNumOfBots = Integer.parseInt(args[0]);
            } catch (NumberFormatException e) {
                sender.sendMessage("Number_of_bots must be a positive number or the word 'dynamic'.");
                return;
            }
            if (newNumOfBots < 0) {
                sender.sendMessage("Number_of_bots must be a positive number or the word 'dynamic'.");
                return;
            }
            if (newNumOfBots > BotManager.getNumOfBots())
                botsToSpawn = newNumOfBots - BotManager.getNumOfBots();
            else if (newNumOfBots == BotManager.getNumOfBots()) return;
            else
                botsToDestroy = (newNumOfBots - BotManager.getNumOfBots()) * -1;
            BotManager.setBotsToManage(newNumOfBots);
        }
        // Determine spawn location
        Location spawnLocation = BotManager.getBotSpawnLoc();
        if (BotManager.getBotSpawnLoc() == null) {
            if (nstatus.isGame()) spawnLocation = MapManager.getCurrentGameMap().getWorld().getSpawnLocation();
            else spawnLocation = MapManager.getLobbyMap().getWorld().getSpawnLocation();
        }
        long delay = 5 + r.nextInt(5);

        // destroy bots
        for (int i = 0; i < botsToDestroy; i++) {
            new BukkitRunnable() {
                @Override
                public void run() {
                    BotManager.getBots().get(r.nextInt(BotManager.getNumOfBots())).destroy(true);
                }
            }.runTaskLater(ServerLink.plugin, delay);
            delay += 21 + r.nextInt(1);
        }

        // spawn bots
        for (int i = 0; i < botsToSpawn; i++) {
            Location finalSpawnLocation = spawnLocation;
            new BukkitRunnable() {
                @Override
                public void run() {
                    // choose each bot's username
                    String username = (String) BotManager.botUsers.keySet().toArray()[r.nextInt(BotManager.botUsers.keySet().size())];
                    while (BotManager.isBotNameDisabled(username)) {
                        username = (String) BotManager.botUsers.keySet().toArray()[r.nextInt(BotManager.botUsers.keySet().size())];
                    }
                    // spawn each bot
                    new Bot(username, finalSpawnLocation, true);
                }
            }.runTaskLater(ServerLink.plugin, delay);
            delay += 21 + r.nextInt(1);
        }
        if (botsToSpawn != 0 || botsToDestroy != 0) {
            BotManager.creatingOrDeletingBots = true;
            new BukkitRunnable() {
                @Override
                public void run() {
                    BotManager.creatingOrDeletingBots = false;
                }
            }.runTaskLater(ServerLink.plugin, (botsToSpawn + botsToDestroy) * 25);
        }
    }
}

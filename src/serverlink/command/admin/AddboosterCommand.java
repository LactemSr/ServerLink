package serverlink.command.admin;

import serverlink.command.CommandManager;
import serverlink.network.JedisPool;
import serverlink.server.GametypeBooster;
import serverlink.util.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.List;

public class AddboosterCommand {
    @CommandManager.CommandHandler(name = "addbooster", async = true, permission = "rank.10", sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(CommandSender sender, String[] args) {
        if (args.length != 5) {
            sender.sendMessage("Usage: /addbooster <player_name> <game_type> <multiplier> <duration_in_minutes> <amount>");
            return;
        }
        List<String> uuidAndName = getUUIDAndName(args[0]);
        if (uuidAndName == null) {
            sender.sendMessage("Invalid player.");
            return;
        }
        String type = args[1];
        Double multiplier;
        try {
            multiplier = Double.parseDouble(args[2]);
        } catch (NumberFormatException e) {
            sender.sendMessage("Your third argument (multiplier) must be a double.");
            return;
        }
        int duration;
        try {
            duration = Integer.parseInt(args[3]);
        } catch (NumberFormatException e) {
            sender.sendMessage("Your fourth argument (duration) must be an integer.");
            return;
        }
        int amount;
        try {
            amount = Integer.parseInt(args[4]);
        } catch (NumberFormatException e) {
            sender.sendMessage("Your fifth argument (amount) must be an integer.");
            return;
        }
        GametypeBooster.buyBooster(uuidAndName.get(1), uuidAndName.get(0), multiplier, duration, amount, type);
        sender.sendMessage("Successfully added to " + uuidAndName.get(1) + "'s booster collection: " + amount +
                " of " + multiplier + "x " + type + " booster for " + duration + " minutes.");
    }

    private List<String> getUUIDAndName(String input) {
        input = input.toLowerCase();
        final Player player = Bukkit.getPlayerExact(input);
        if (player != null && player.isOnline()) {
            return new ArrayList<String>() {{
                add(player.getUniqueId().toString());
                add(player.getName());
            }};
        } else {
            Jedis jedis = JedisPool.getConn();
            String uuid = jedis.get(input.toLowerCase());
            if (uuid != null && uuid.length() > 8) {
                String name = jedis.hget(uuid, "username");
                JedisPool.close(jedis);
                return new ArrayList<String>() {{
                    add(uuid);
                    add(name);
                }};
            } else {
                JedisPool.close(jedis);
                try {
                    List<String> uuidAndName = UUIDFetcher.getOfflineUUIDAndName(input);
                    if (uuidAndName != null) return uuidAndName;
                    else {
                        return null;
                    }
                } catch (Exception e) {
                    return null;
                }
            }
        }
    }
}

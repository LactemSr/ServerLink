package serverlink.command.admin;

import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.command.CommandManager;
import serverlink.npc.NpcManager;
import serverlink.server.Messages;
import net.minecraft.server.v1_8_R3.NBTTagCompound;
import org.bukkit.Location;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftEntity;
import org.bukkit.entity.Entity;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import java.io.File;
import java.io.IOException;

// saves a mob's info to npcoutput.yml; this info can be copied and pasted and adjusted to work in configs
public class NpcCommand {
    private static EntityType getType(String mobName) {
        EntityType type = EntityType.ZOMBIE;
        if (mobName.equalsIgnoreCase("villager")) {
            type = EntityType.VILLAGER;
        } else if (mobName.equalsIgnoreCase("zombiepigman") || mobName.equalsIgnoreCase("pigzombie")
                || mobName.equalsIgnoreCase("zombie_pigman")) {
            type = EntityType.PIG_ZOMBIE;
        } else if (mobName.equalsIgnoreCase("sheep")) {
            type = EntityType.SHEEP;
        } else if (mobName.equalsIgnoreCase("pig")) {
            type = EntityType.PIG;
        } else if (mobName.equalsIgnoreCase("cow")) {
            type = EntityType.COW;
        } else if (mobName.equalsIgnoreCase("mushroomcow") || mobName.equalsIgnoreCase("mooshroom")) {
            type = EntityType.MUSHROOM_COW;
        } else if (mobName.equalsIgnoreCase("horse")) {
            type = EntityType.HORSE;
        } else if (mobName.equalsIgnoreCase("chicken")) {
            type = EntityType.CHICKEN;
        } else if (mobName.equalsIgnoreCase("blaze")) {
            type = EntityType.BLAZE;
        } else if (mobName.equalsIgnoreCase("cavespider") || mobName.equalsIgnoreCase("cave_spider")) {
            type = EntityType.CAVE_SPIDER;
        } else if (mobName.equalsIgnoreCase("spider")) {
            type = EntityType.SPIDER;
        } else if (mobName.equalsIgnoreCase("creeper")) {
            type = EntityType.CREEPER;
        } else if (mobName.equalsIgnoreCase("enderman")) {
            type = EntityType.ENDERMAN;
        } else if (mobName.equalsIgnoreCase("guardian")) {
            type = EntityType.GUARDIAN;
        } else if (mobName.equalsIgnoreCase("irongolem") || mobName.equalsIgnoreCase("iron_golem")) {
            type = EntityType.IRON_GOLEM;
        } else if (mobName.equalsIgnoreCase("rabbit")) {
            type = EntityType.RABBIT;
        } else if (mobName.equalsIgnoreCase("snowman")) {
            type = EntityType.SNOWMAN;
        } else if (mobName.equalsIgnoreCase("bat")) {
            type = EntityType.BAT;
        } else if (mobName.equalsIgnoreCase("ghast")) {
            type = EntityType.GHAST;
        } else if (mobName.equalsIgnoreCase("magmacube") || mobName.equalsIgnoreCase("magma_cube")) {
            type = EntityType.MAGMA_CUBE;
        } else if (mobName.equalsIgnoreCase("slime")) {
            type = EntityType.SLIME;
        } else if (mobName.equalsIgnoreCase("skeleton")) {
            type = EntityType.SKELETON;
        } else if (mobName.equalsIgnoreCase("witherskull") || mobName.equalsIgnoreCase("wither_skull")
                || mobName.equalsIgnoreCase("witherskeleton") || mobName.equalsIgnoreCase("wither_skeleton")) {
            type = EntityType.WITHER_SKULL;
        } else if (mobName.equalsIgnoreCase("squid")) {
            type = EntityType.SQUID;
        } else if (mobName.equalsIgnoreCase("witch")) {
            type = EntityType.WITCH;
        } else if (mobName.equalsIgnoreCase("wolf") || mobName.equalsIgnoreCase("dog")) {
            type = EntityType.WOLF;
        }
        return type;
    }

    private static void tag(Entity entity) {
        net.minecraft.server.v1_8_R3.Entity nmsEntity = ((CraftEntity) entity).getHandle();
        NBTTagCompound tag = nmsEntity.getNBTTag();
        if (tag == null) {
            tag = new NBTTagCompound();
        }
        nmsEntity.c(tag);
        tag.setInt("NoAI", 1);
        nmsEntity.f(tag);
    }

    @CommandManager.CommandHandler(name = "npc", usage = Messages.npc_help, aliases = {"npcs"}, async = false, permission = "rank.10")
    public void base(Player player, String[] args) {
        player.sendMessage(chat.color(Messages.npc_help));
    }

    @CommandManager.SubCommandHandler(base = "npc", name = "add", async = false, args = 6)
    public void add(Player player, String[] args) {
        File configFile = new File(ServerLink.plugin.getDataFolder(), "npcoutput.yml");
        YamlConfiguration config = YamlConfiguration.loadConfiguration(configFile);
        Integer number;
        try {
            number = Integer.parseInt(args[0]);
        } catch (Exception e) {
            player.sendMessage(chat.color(chat.getServer() + Messages.npc_help));
            return;
        }
        if (config.contains(number.toString())) {
            player.sendMessage(chat.color(chat.getServer() + "That number is already in npcoutput.yml"));
            return;
        }
        String mobName = args[1];
        EntityType type = getType(mobName);
        double x;
        double y;
        double z;
        float degree = Integer.parseInt(args[5]);
        if (args[2].equalsIgnoreCase("~")) {
            x = player.getLocation().getX();
        } else {
            x = Integer.parseInt(args[2]);
        }
        if (args[3].equalsIgnoreCase("~")) {
            y = player.getLocation().getY();
        } else {
            y = Integer.parseInt(args[3]);
        }
        if (args[4].equalsIgnoreCase("~")) {
            z = player.getLocation().getZ();
        } else {
            z = Integer.parseInt(args[4]);
        }
        config.set(number + ".type", type.name());
        config.set(number + ".x", (int) x);
        config.set(number + ".y", (int) y);
        config.set(number + ".z", (int) z);
        config.set(number + ".rotation", (int) degree);
        try {
            config.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        Location loc = new Location(player.getWorld(), x, y, z);
        loc.setYaw(degree);
        NpcManager.protectedNpcs.add(loc);
        Entity entity = player.getWorld().spawnEntity(loc, type);
        entity.teleport(loc);
        tag(entity);
        new BukkitRunnable() {
            @Override
            public void run() {
                entity.remove();
            }
        }.runTaskLater(ServerLink.plugin, 100);
    }

    @CommandManager.SubCommandHandler(base = "npc", name = "remove", async = false, args = 1)
    public void remove(Player player, String[] args) {
        File configFile = new File(ServerLink.plugin.getDataFolder(), "npcoutput.yml");
        YamlConfiguration config = YamlConfiguration.loadConfiguration(configFile);
        Integer number;
        try {
            number = Integer.parseInt(args[0]);
        } catch (Exception e) {
            player.sendMessage(chat.color(chat.getServer() + Messages.npc_help));
            return;
        }
        if (!config.contains(number.toString())) {
            player.sendMessage(chat.color(chat.getServer() + "That number doesn't exist in npcoutput.yml"));
            return;
        }
        config.set(number.toString(), null);
        try {
            config.save(configFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @CommandManager.SubCommandHandler(base = "npc", name = "list", async = false)
    public void list(Player player, String[] args) {
        File configFile = new File(ServerLink.plugin.getDataFolder(), "npcoutput.yml");
        YamlConfiguration config = YamlConfiguration.loadConfiguration(configFile);
        boolean dark = true;
        for (Integer i = 0; i < 100; i++) {
            if (!config.contains(i.toString())) continue;
            if (dark) {
                player.sendMessage(chat.color("&7#" + i + ": " + config.get(i + ".type")));
            } else {
                player.sendMessage(chat.color("&3#" + i + ": " + config.get(i + ".type")));
            }
            dark = !dark;
        }
    }

    @CommandManager.SubCommandHandler(base = "npc", name = "show", async = false)
    public void show(Player player, String[] args) {
        File configFile = new File(ServerLink.plugin.getDataFolder(), "npcoutput.yml");
        YamlConfiguration config = YamlConfiguration.loadConfiguration(configFile);
        if (args[0].equalsIgnoreCase("all")) {
            for (Integer i = 0; i < 100; i++) {
                if (!config.contains(i.toString())) continue;
                EntityType type = EntityType.valueOf(config.getString(i + ".type"));
                double x = config.getDouble(i + ".x");
                double y = config.getDouble(i + ".y");
                double z = config.getDouble(i + ".z");
                float degree = config.getInt(i + ".rotation");
                Location loc = new Location(player.getWorld(), x, y, z);
                loc.setYaw(degree);
                NpcManager.protectedNpcs.add(loc);
                Entity entity = player.getWorld().spawnEntity(loc, type);
                entity.teleport(loc);
                tag(entity);
                new BukkitRunnable() {
                    @Override
                    public void run() {
                        entity.remove();
                    }
                }.runTaskLater(ServerLink.plugin, 100);
                return;
            }
        } else {
            Integer number;
            try {
                number = Integer.parseInt(args[0]);
            } catch (Exception e) {
                player.sendMessage(chat.color(chat.getServer() + Messages.npc_help));
                return;
            }
            if (!config.contains(number.toString())) {
                player.sendMessage(chat.color(chat.getServer() + "That number doesn't exist in npcoutput.yml"));
                return;
            }
            EntityType type = EntityType.valueOf(config.getString(number + ".type"));
            double x = config.getDouble(number + ".x");
            double y = config.getDouble(number + ".y");
            double z = config.getDouble(number + ".z");
            float degree = config.getInt(number + ".rotation");
            Location loc = new Location(player.getWorld(), x, y, z);
            loc.setYaw(degree);
            NpcManager.protectedNpcs.add(loc);
            Entity entity = player.getWorld().spawnEntity(loc, type);
            entity.teleport(loc);
            tag(entity);
            new BukkitRunnable() {
                @Override
                public void run() {
                    entity.remove();
                }
            }.runTaskLater(ServerLink.plugin, 100);
        }
    }
}
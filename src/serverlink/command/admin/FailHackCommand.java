package serverlink.command.admin;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import serverlink.command.CommandManager;
import serverlink.player.hacking.HackType;
import serverlink.player.hacking.Hacking;

public class FailHackCommand {
    @CommandManager.CommandHandler(name = "failhack", async = false, permission = "rank.10", sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(CommandSender sender, String[] args) {
        if (args.length < 2) return;
        Player player = Bukkit.getPlayerExact(args[0].toLowerCase());
        if (player == null) return;
        HackType hackType;
        switch (args[1].toLowerCase().replaceAll("_", "")) {
            case "accuracy":
                hackType = HackType.ACCURACY;
                break;
            case "angle":
                hackType = HackType.ANGLE;
                break;
            case "bow":
            case "fastbow":
            case "bowaimbot":
                hackType = HackType.BOW;
                break;
            case "fastbreak":
                hackType = HackType.FAST_BREAK;
                break;
            case "fastclick":
                hackType = HackType.FAST_CLICK;
                break;
            case "fastplace":
                hackType = HackType.FAST_PLACE;
                break;
            case "flight":
                hackType = HackType.FLIGHT;
                break;
            case "headmove":
                hackType = HackType.HEADMOVE;
                break;
            case "nofall":
                hackType = HackType.NO_FALL;
                break;
            case "noswing":
                hackType = HackType.NO_SWING;
                break;
            case "phase":
                hackType = HackType.PHASE_OR_TELEPORT;
                break;
            case "teleport":
                hackType = HackType.PHASE_OR_TELEPORT;
                break;
            case "regen":
            case "fastregen":
                hackType = HackType.REGEN;
                break;
            default:
                hackType = HackType.SPEED_OR_SPIDER_OR_STEP;
                break;
        }
        Hacking.failHack(player, hackType);
    }
}

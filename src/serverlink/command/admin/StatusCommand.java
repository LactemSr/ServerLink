package serverlink.command.admin;

import org.bukkit.command.CommandSender;
import redis.clients.jedis.Jedis;
import serverlink.chat.chat;
import serverlink.command.CommandManager;
import serverlink.network.JedisPool;
import serverlink.player.*;
import serverlink.util.TimeUtils;

public class StatusCommand {
    private static final String usage = "Usage: /status <player>";

    @CommandManager.CommandHandler(name = "status", usage = usage, async = true, sendUsageMessageOnUnrecognizedSubCommand = false, permission = "rank.7")
    public void base(CommandSender sender, String[] args) {
        if (args.length != 1) {
            sender.sendMessage(usage);
            return;
        }
        Jedis jedis = JedisPool.getConn();
        String uuid = jedis.get(args[0].toLowerCase());
        if (uuid == null || uuid.length() < 8) {
            JedisPool.close(jedis);
            sender.sendMessage("That player has never played, so he/she has no info on file.");
            return;
        }
        String username = jedis.hget(uuid, "username");
        String onlineStatus = jedis.hget(uuid, "online");
        if (onlineStatus.equals("true")) {
            onlineStatus = "&aonline";
        } else {
            onlineStatus = "&coffline";
        }
        String server = jedis.hget(uuid, "server");
        JedisPool.close(jedis);
        int kitpvpPoints = Points.getOfflinePointsInt(uuid, "kitpvp");
        int skyblockPoints = Points.getOfflinePointsInt(uuid, "skyblock");
        int coins = Coins.getOfflineCoinsInt(uuid);
        jedis = JedisPool.getConn();
        String rank;
        try {
            rank = Permissions.getRankName(Integer.parseInt(jedis.hget(uuid, "rank")));
        } catch (NumberFormatException e) {
            rank = Permissions.getRankName(1);
        }
        int exp;
        try {
            exp = Integer.parseInt(jedis.hget(uuid, "exp"));
        } catch (NumberFormatException e) {
            exp = 0;
        }
        int level = Level.calculateLevelFromExp(exp);
        String mute = jedis.hget(uuid, "mute");
        String ban = jedis.hget(uuid, "ban");
        JedisPool.close(jedis);
        if (mute != null && TimeUtils.getCurrent() < Integer.parseInt(mute.split(":")[1]))
            mute = "muted for " + Punishments.format(Integer.parseInt(mute.split(":")[1]) - -TimeUtils.getCurrent()) + " for "
                    + mute.split(":")[0];
        else
            mute = "not muted";
        if (ban != null && TimeUtils.getCurrent() < Integer.parseInt(ban.split(":")[1]))
            ban = "banned for " + Punishments.format(Integer.parseInt(ban.split(":")[1]) - TimeUtils.getCurrent()) + " for "
                    + ban.split(":")[0];
        else
            ban = "not banned";
        sender.sendMessage(chat.color("            ---- " + username
                + " ----\n&f&lOnline Status: &r" + onlineStatus
                + "\n&f&lLast Seen On: &r&9" + server
                + "\n&f&lKitPvP Tokens: &e" + kitpvpPoints
                + "\n&f&lSky Gems: &e" + skyblockPoints
                + "\n&f&lCoins: &e" + coins
                + "\n&f&lRank: &r" + rank
                + "\n&f&lLevel: &r" + level
                + "\n&f&lMute Status: &r" + mute
                + "\n&f&lBan Status: &r" + ban));
    }
}
package serverlink.command.admin;

import org.bukkit.command.CommandSender;
import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.command.CommandManager;
import serverlink.player.TreasureManager;

import java.util.UUID;

public class ChestCommands {
    private static final String usage = "&fUsage: &6/addminichests | /addepicchests | " +
            "/addlegendarychests &e<player> <number_of_chests> <days_to_expire>";

    @CommandManager.CommandHandler(name = "addminichests", async = true, sendUsageMessageOnUnrecognizedSubCommand = false, permission = "rank.10")
    public void addMiniTickets(CommandSender sender, String[] args) {
        if (args.length != 3) {
            sender.sendMessage(chat.color(usage));
            return;
        }
        UUID target = ServerLink.getUUIDFromName(args[0], true);
        if (target == null) {
            sender.sendMessage(chat.color(usage));
            return;
        }
        int numberOfChests;
        try {
            numberOfChests = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            sender.sendMessage(chat.color("Second argument (number_of_chests) must be a number."));
            return;
        }
        if (numberOfChests <= 0) {
            sender.sendMessage("Second argument (number_of_chests) must be a positive number.");
            return;
        }
        int daysToExpire;
        try {
            daysToExpire = Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            sender.sendMessage(chat.color("Third argument (days_to_expire) must be a number or 0 for never expire."));
            return;
        }
        if (daysToExpire < 0) {
            sender.sendMessage("Third argument (days_to_expire) must be a positive number or 0 for never expire.");
            return;
        }
        TreasureManager.addMini(target.toString(), numberOfChests, daysToExpire);
        sender.sendMessage("Mini chests added");
    }

    @CommandManager.CommandHandler(name = "addepicchests", async = true, sendUsageMessageOnUnrecognizedSubCommand = false, permission = "rank.10")
    public void addEpicChests(CommandSender sender, String[] args) {
        if (args.length != 3) {
            sender.sendMessage(chat.color(usage));
            return;
        }
        UUID target = ServerLink.getUUIDFromName(args[0], true);
        if (target == null) {
            sender.sendMessage(chat.color(usage));
            return;
        }
        int numberOfChests;
        try {
            numberOfChests = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            sender.sendMessage(chat.color("Second argument (number_of_chests) must be a number."));
            return;
        }
        if (numberOfChests <= 0) {
            sender.sendMessage("Second argument (number_of_chests) must be a positive number.");
            return;
        }
        int daysToExpire;
        try {
            daysToExpire = Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            sender.sendMessage(chat.color("Third argument (days_to_expire) must be a number or 0 for never expire."));
            return;
        }
        if (daysToExpire < 0) {
            sender.sendMessage("Third argument (days_to_expire) must be a positive number or 0 for never expire.");
            return;
        }
        TreasureManager.addEpic(target.toString(), numberOfChests, daysToExpire);
        sender.sendMessage("Epic chests added");
    }

    @CommandManager.CommandHandler(name = "addlegendarychests", async = true, sendUsageMessageOnUnrecognizedSubCommand = false, permission = "rank.10")
    public void addLegendaryChests(CommandSender sender, String[] args) {
        if (args.length != 3) {
            sender.sendMessage(chat.color(usage));
            return;
        }
        UUID target = ServerLink.getUUIDFromName(args[0], true);
        if (target == null) {
            sender.sendMessage(chat.color(usage));
            return;
        }
        int numberOfChests;
        try {
            numberOfChests = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            sender.sendMessage(chat.color("Second argument (number_of_chests) must be a number."));
            return;
        }
        if (numberOfChests <= 0) {
            sender.sendMessage("Second argument (number_of_chests) must be a positive number.");
            return;
        }
        int daysToExpire;
        try {
            daysToExpire = Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            sender.sendMessage(chat.color("Third argument (days_to_expire) must be a number or 0 for never expire."));
            return;
        }
        if (daysToExpire < 0) {
            sender.sendMessage("Third argument (days_to_expire) must be a positive number or 0 for never expire.");
            return;
        }
        TreasureManager.addLegendary(target.toString(), numberOfChests, daysToExpire);
        sender.sendMessage("Legendary chests added");
    }
}

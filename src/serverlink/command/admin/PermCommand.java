package serverlink.command.admin;

import serverlink.chat.chat;
import serverlink.command.CommandManager;
import serverlink.network.JedisPool;
import serverlink.player.Permissions;
import serverlink.util.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import redis.clients.jedis.Jedis;

import java.util.List;

public class PermCommand {
    private static final String usage = "&aUsage: &6/perm add&7/&6remove &e<player> <perm>\n" +
            "          &7OR\n" +
            "&6/perm list &e<player>";

    @CommandManager.CommandHandler(name = "perm", usage = usage, aliases = {"perms", "permission", "permissions"}, async = true, sendUsageMessageOnUnrecognizedSubCommand = false, permission = "rank.10")
    public void base(CommandSender sender, String[] args) {
        if (args.length < 2 || args.length > 3) {
            sender.sendMessage(chat.color(usage));
            return;
        }
        if (args[0].equalsIgnoreCase("add") || args[0].equals("give")) {
            if (args.length != 3) {
                sender.sendMessage("Usage: /perm add <player> <perm>");
                return;
            }
            sender.sendMessage(Permissions.addPerm(args[1], args[2]));
        } else if (args[0].equalsIgnoreCase("remove")) {
            if (args.length != 3) {
                sender.sendMessage("Usage: /perm remove <player> <perm>");
                return;
            }
            sender.sendMessage(Permissions.removePerm(args[1], args[2]));
        } else if (args[0].equalsIgnoreCase("list")) {
            if (args.length != 2) {
                sender.sendMessage("Usage: /perm list <player>");
                return;
            }
            String uuid;
            String name;
            Player player = Bukkit.getPlayerExact(args[1]);
            if (player != null && player.isOnline()) {
                uuid = player.getUniqueId().toString();
                name = player.getName();
            } else {
                Jedis jedis = JedisPool.getConn();
                uuid = jedis.get(args[1].toLowerCase());
                if (uuid != null && uuid.length() > 8) {
                    name = jedis.hget(uuid, "username");
                    JedisPool.close(jedis);
                } else {
                    JedisPool.close(jedis);
                    try {
                        List<String> uuidAndName = UUIDFetcher.getOfflineUUIDAndName(args[1].toLowerCase());
                        if (uuidAndName != null) {
                            uuid = uuidAndName.get(0);
                            name = uuidAndName.get(1);
                        } else {
                            sender.sendMessage("That player does not exist!");
                            return;
                        }
                    } catch (Exception e) {
                        sender.sendMessage("That player does not exist!");
                        return;
                    }
                }
            }
            Jedis jedis = JedisPool.getConn();
            List<String> currentPerms = jedis.lrange("perms:" + uuid, 0, -1);
            JedisPool.close(jedis);
            if (currentPerms.size() == 0) {
                sender.sendMessage(
                        "This player doesn't have any permissions (he hasn't joined the server or he hasn't acquired any permissions).");
                return;
            }
            String displayPerms = "";
            for (String s : currentPerms) displayPerms = displayPerms.concat(s).concat("\n");
            sender.sendMessage("Permissions for " + name + ":\n" + displayPerms);

        } else sender.sendMessage(chat.color(usage));
    }
}

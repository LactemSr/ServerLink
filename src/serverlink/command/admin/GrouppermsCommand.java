package serverlink.command.admin;

import serverlink.chat.chat;
import serverlink.command.CommandManager;
import serverlink.network.JedisPool;
import serverlink.player.Permissions;
import org.bukkit.command.CommandSender;
import redis.clients.jedis.Jedis;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GrouppermsCommand {
    private final static String usage = "Usage: &6/groupperms list &fOR &6/groupperms add &7| &6remove &e<rank> <perm>";

    @CommandManager.CommandHandler(name = "groupperms", async = false, permission = "rank.10")
    public void base(CommandSender sender, String[] args) {
        sender.sendMessage(chat.color(usage));
    }

    @CommandManager.SubCommandHandler(base = "groupperms", name = "list", args = 1, aliases = {"show", "display", "get"}, async = true, permission = "rank.10")
    public void list(CommandSender sender, String[] args) {
        String rankString = args[0];
        int rank;
        try {
            rank = Integer.parseInt(rankString);
        } catch (NumberFormatException e) {
            sender.sendMessage(chat.color(usage));
            return;
        }
        if (rank < 1 || rank > 10) {
            sender.sendMessage("Rank must be 1-10.");
            return;
        }
        Jedis jedis = JedisPool.getConn();
        List<String> perms = jedis.lrange("group:" + rankString, 0, -1);
        JedisPool.close(jedis);
        if (perms == null || perms.isEmpty()) {
            sender.sendMessage("Rank " + rankString + " has no added perms.");
            return;
        }
        sender.sendMessage("Perms for rank " + rankString + ":\n" + Arrays.toString(perms.toArray()));
    }

    @CommandManager.SubCommandHandler(base = "groupperms", name = "add", args = 2, async = true, permission = "rank.10")
    public void add(CommandSender sender, String[] args) {
        String rankString = args[0];
        int rank;
        try {
            rank = Integer.parseInt(rankString);
        } catch (NumberFormatException e) {
            sender.sendMessage(chat.color(usage));
            return;
        }
        if (rank < 1 || rank > 10) {
            sender.sendMessage("Rank must be 1-10.");
            return;
        }
        String perm = args[1].toLowerCase();
        Jedis jedis = JedisPool.getConn();
        List<String> perms = jedis.lrange("group:" + rankString, 0, -1);
        if (perms == null || perms.isEmpty()) perms = new ArrayList<>();
        if (perms.contains(perm)) {
            sender.sendMessage("Rank " + rankString + " already has that perm.");
            JedisPool.close(jedis);
            return;
        }
        Permissions.permissionsGroups.get(rank).add(perm);
        jedis.lpush("group:" + rankString, perm);
        JedisPool.close(jedis);
        sender.sendMessage("Successfully added \"" + perm + "\" to rank " + rankString + ".");
    }

    @CommandManager.SubCommandHandler(base = "groupperms", name = "remove", args = 2, aliases = {"delete"}, async = true, permission = "rank.10")
    public void remove(CommandSender sender, String[] args) {
        String rankString = args[0];
        int rank;
        try {
            rank = Integer.parseInt(rankString);
        } catch (NumberFormatException e) {
            sender.sendMessage(chat.color(usage));
            return;
        }
        if (rank < 1 || rank > 10) {
            sender.sendMessage("Rank must be 1-10.");
            return;
        }
        String perm = args[1].toLowerCase();
        Jedis jedis = JedisPool.getConn();
        List<String> perms = jedis.lrange("group:" + rankString, 0, -1);
        if (perms == null || perms.isEmpty()) perms = new ArrayList<>();
        if (!perms.contains(perm)) {
            sender.sendMessage("Rank " + rankString + " doesn't have that perm.");
            JedisPool.close(jedis);
            return;
        }
        Permissions.permissionsGroups.get(rank).remove(perm);
        jedis.lrem("group:" + rankString, 0, perm);
        JedisPool.close(jedis);
        sender.sendMessage("Successfully removed \"" + perm + "\" from rank " + rankString + ".");
    }
}

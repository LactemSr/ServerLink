package serverlink.command.admin;

import org.bukkit.command.CommandSender;
import serverlink.chat.chat;
import serverlink.command.CommandManager;

public class LeaveMessagesCommand {
    private final static String usage = "Usage: &6/leavemessages all&7|&6off&7|&6staff&7|&6donors";

    @CommandManager.CommandHandler(name = "leavemessages", usage = usage, permission = "rank.8", async = false, aliases = {"leavemessage", "quitmessages", "quitmessage", "quitnotifications", "quitnotificaton"})
    public void base(CommandSender sender, String[] args) {
        sender.sendMessage(chat.color(usage));
    }

    @CommandManager.SubCommandHandler(base = "leavemessages", name = "all", permission = "rank.8", aliases = {"enable", "enabled", "on"}, async = false)
    public void on(CommandSender sender, String[] args) {
        if (chat.getJoinLeaveMessagesStatus().equals("all"))
            sender.sendMessage(chat.color(chat.getServer() + "&aLeave messages already set to \"all\"."));
        else {
            sender.sendMessage(chat.color(chat.getServer() + "&aSet leave messages to display for all users."));
            chat.setJoinLeaveMessages("all");
        }
    }

    @CommandManager.SubCommandHandler(base = "leavemessages", name = "none", permission = "rank.8", aliases = {"disable", "disabled", "off"}, async = false)
    public void off(CommandSender sender, String[] args) {
        if (chat.getJoinLeaveMessagesStatus().equals("none"))
            sender.sendMessage(chat.color(chat.getServer() + "&aLeave messages already set to \"none\"."));
        else {
            sender.sendMessage(chat.color(chat.getServer() + "&aSet leave messages to not display."));
            chat.setJoinLeaveMessages("none");
        }
    }

    @CommandManager.SubCommandHandler(base = "leavemessages", name = "staff", permission = "rank.8", aliases = {"staffs"}, async = false)
    public void staff(CommandSender sender, String[] args) {
        if (chat.getJoinLeaveMessagesStatus().equals("staff"))
            sender.sendMessage(chat.color(chat.getServer() + "&aLeave messages already set to \"staff\"."));
        else {
            sender.sendMessage(chat.color(chat.getServer() + "&aSet leave messages to display for staff members only."));
            chat.setJoinLeaveMessages("staff");
        }
    }

    @CommandManager.SubCommandHandler(base = "leavemessages", name = "donors", permission = "rank.8", aliases = {"donor", "rank", "ranked", "ranks"}, async = false)
    public void donors(CommandSender sender, String[] args) {
        if (chat.getJoinLeaveMessagesStatus().equals("donors"))
            sender.sendMessage(chat.color(chat.getServer() + "&aLeave messages already set to \"donors\"."));
        else {
            sender.sendMessage(chat.color(chat.getServer() + "&aSet leave messages to display for donors and staff members only."));
            chat.setJoinLeaveMessages("donors");
        }
    }
}

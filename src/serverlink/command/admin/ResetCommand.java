package serverlink.command.admin;

import net.citizensnpcs.api.CitizensAPI;
import org.bukkit.command.CommandSender;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.bot.BotBaseTrait;
import serverlink.command.CommandManager;
import serverlink.network.JedisOne;
import serverlink.network.ncount;

public class ResetCommand {
    @CommandManager.CommandHandler(name = "reset", async = false, sendUsageMessageOnUnrecognizedSubCommand = false, permission = "rank.9")
    public void base(CommandSender sender, String[] args) {
        int playersOnline = 0;
        synchronized (ncount.countLock) {
            for (AlphaPlayer aPlayer : PlayerManager.getOnlineAlphaPlayers()) {
                if (aPlayer.getPlayer() == null) {
                    PlayerManager.onlineAlphaPlayers.remove(aPlayer);
                    PlayerManager.alphaPlayers.remove(aPlayer.getUuid());
                    continue;
                } else if (!aPlayer.isFullyLoggedIn()) continue;
                if (aPlayer.getPlayer().isOnline() || (aPlayer.getPlayer().hasMetadata("NPC") && CitizensAPI.getNPCRegistry().getNPC(aPlayer.getPlayer()).getTrait(BotBaseTrait.class).ready))
                    playersOnline++;
                else {
                    if (aPlayer.getPlayer().hasMetadata("NPC") && CitizensAPI.getNPCRegistry().getNPC(aPlayer.getPlayer()).getTrait(BotBaseTrait.class).ready)
                        System.out.println("removing a bot that is no longer ready");
                    else
                        System.out.println("removing a player that is not longer online");
                    PlayerManager.onlineAlphaPlayers.remove(aPlayer);
                    PlayerManager.alphaPlayers.remove(aPlayer.getUuid());
                }
            }
            int ghostPlayers = ncount.getCount() - playersOnline;
            if (ghostPlayers == 0) {
                sender.sendMessage("Server count already accurate. Type /resetnet to reset " +
                        "count and other data for the whole network.");
                return;
            }
            Jedis jedis = JedisOne.getOne();
            // This server
            ncount.setCount(playersOnline);
            // This gametype
            if (jedis.decrBy(ServerLink.getServerType().toString() + "Count", ghostPlayers) < 0)
                jedis.set(ServerLink.getServerType().toString() + "Count", "0");
            // The whole network
            if (jedis.decrBy("globalCount", ghostPlayers) < 0)
                jedis.set("globalCount", "0");
            JedisOne.closeOne();
            sender.sendMessage("Removed " + ghostPlayers + " ghost players from the count.");
        }
    }
}

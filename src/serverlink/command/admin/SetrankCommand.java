package serverlink.command.admin;

import serverlink.ServerLink;
import serverlink.command.CommandManager;
import serverlink.network.JedisPool;
import serverlink.network.nmessage;
import serverlink.player.Permissions;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;

import java.util.UUID;

public class SetrankCommand {
    private static final String usage = "Usage: /setrank <player> <rank 1-10>";

    @CommandManager.CommandHandler(name = "setrank", async = true, usage = usage, permission = "rank.10", sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(CommandSender sender, String[] args) {
        if (args.length != 2) {
            sender.sendMessage(usage);
            return;
        }
        UUID target = ServerLink.getUUIDFromName(args[0], true);
        if (target == null) {
            sender.sendMessage("Invalid player!");
            return;
        }
        int rank;
        try {
            rank = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            sender.sendMessage("Your second argument (rank) must be a number from 1-10.");
            return;
        }
        if (rank < 1) {
            sender.sendMessage("Your second argument (rank) must be a number from 1-10.");
            return;
        }
        if (rank > 1 && rank <= 5) {
            sender.sendMessage("Use the /ranktime command for ranks 2-5 (the buyable ranks).");
            return;
        }
        int formerRank;
        Jedis jedis = JedisPool.getConn();
        try {
            formerRank = Integer.parseInt(jedis.hget(target.toString(), "rank"));
        } catch (NumberFormatException e) {
            // if the player has never joined before
            formerRank = 1;
        }
        if (formerRank == rank) {
            JedisPool.close(jedis);
            sender.sendMessage("That player is already rank " + rank + ".");
            return;
        }
        jedis.lrem("perms:" + target.toString(), 0, "rank." + formerRank);
        jedis.hdel("settings:" + target.toString(), "chatFormat", "nameFormat");
        jedis.lpush("perms:" + target.toString(), "rank." + rank);
        jedis.hset(target.toString(), "rankOnForums", "0");
        jedis.hset(target.toString(), "rank", ((Integer) rank).toString());
        jedis.hset(target.toString(), "rank5Expiry", "0");
        jedis.hset(target.toString(), "rank4Expiry", "0");
        jedis.hset(target.toString(), "rank3Expiry", "0");
        jedis.hset(target.toString(), "rank2Expiry", "0");
        Player player = Bukkit.getPlayer(target);
        if (player != null && player.isOnline())
            new BukkitRunnable() {
                @Override
                public void run() {
                    Permissions.reloadPermissions(player);
                }
            }.runTask(ServerLink.plugin);
        else
            nmessage.sendMessage(jedis.hget(target.toString(), "server"), "reloadPermissions:" + target.toString(), jedis);
        JedisPool.close(jedis);
        sender.sendMessage("Success");
    }
}

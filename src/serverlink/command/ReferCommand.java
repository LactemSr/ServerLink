package serverlink.command;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.inventory.gui.profile.ReferralsGui;
import serverlink.player.Referrals;
import serverlink.player.Settings;
import serverlink.server.Messages;

import java.util.UUID;

public class ReferCommand {
    @CommandManager.CommandHandler(name = "refer", usage = Messages.refer_help, aliases = {"referral", "referrals", "referal", "referals"}, async = false)
    public void base(Player player, String[] args) {
        player.sendMessage(chat.color(Messages.refer_help));
    }

    @CommandManager.SubCommandHandler(base = "refer", name = "menu", aliases = {"gui", "inventory", "panel", "interface"}, async = false)
    public void menu(Player player, String[] args) {
        ReferralsGui.createMainMenu(PlayerManager.get(player)).openPage(1);
    }

    @CommandManager.SubCommandHandler(base = "refer", name = "send", aliases = {"invite"}, async = true, args = 1)
    public void send(Player player, String[] args) {
        UUID targetPlayer = ServerLink.getUUIDFromName(args[0].toLowerCase(), false);
        if (targetPlayer == null) {
            // invalid player
            player.sendMessage(chat.color(chat.getServer() + Messages.party_error_playerNotFound));
            return;
        }
        if (targetPlayer.equals(player.getUniqueId())) {
            // player is trying to refer himself
            player.sendMessage(chat.color(chat.getServer() + Messages.refer_error_referSelf));
            return;
        }
        Boolean isReferred = Settings.getBooleanSetting(targetPlayer.toString(), "isReferred");
        if (isReferred != null && !isReferred) {
            player.sendMessage(chat.color(chat.getServer() + Messages.refer_error_otherAlreadyReferred));
            return;
        }
        AlphaPlayer aPlayer = PlayerManager.get(player);
        if (Referrals.getSentReferrals(aPlayer).contains(targetPlayer)) {
            player.sendMessage(chat.color(chat.getServer() + Messages.refer_error_alreadySentRequest));
            return;
        }

        new BukkitRunnable() {
            @Override
            public void run() {
                Referrals.actionSendReferral(aPlayer, targetPlayer);
            }
        }.runTask(ServerLink.plugin);
    }

    @CommandManager.SubCommandHandler(base = "refer", name = "accept", aliases = {"confirm"}, async = true, args = 1)
    public void accept(Player player, String[] args) {
        AlphaPlayer aPlayer = PlayerManager.get(player);
        if (Referrals.isReferred(aPlayer)) {
            player.sendMessage(chat.color(chat.getServer() + Messages.refer_error_youAlreadyReferred));
            return;
        }
        UUID targetPlayer = ServerLink.getUUIDFromName(args[0].toLowerCase(), false);
        if (targetPlayer == null) {
            // invalid player
            player.sendMessage(chat.color(chat.getServer() + Messages.party_error_playerNotFound));
            return;
        }
        if (targetPlayer.equals(player.getUniqueId())) {
            // player is trying to refer himself
            player.sendMessage(chat.color(chat.getServer() + Messages.refer_error_referSelf));
            return;
        }
        new BukkitRunnable() {
            @Override
            public void run() {
                Referrals.actionAcceptReferral(aPlayer, targetPlayer);
            }
        }.runTask(ServerLink.plugin);
    }
}
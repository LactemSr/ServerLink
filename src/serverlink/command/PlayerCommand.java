package serverlink.command;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.inventory.GuiPage;
import serverlink.inventory.GuiRow;
import serverlink.inventory.InventoryGui;
import serverlink.network.JedisPool;
import serverlink.player.*;
import serverlink.player.party.PartyManager;
import serverlink.util.Item;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class PlayerCommand {
    private static final String usage = "\n&6/player &e<player_name>: &7opens a player menu with options for the specified player\n";
    private static final HashMap<Player, String> cooldown = new HashMap<>();

    @CommandManager.CommandHandler(name = "player", usage = usage, async = true, sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(Player player, String[] args) {
        if (cooldown.get(player) != null) {
            player.sendMessage(chat.color(chat.getServer() + "You can only do that once every 15 seconds."));
            return;
        }
        if (args.length != 1) {
            player.sendMessage(chat.color(usage));
            return;
        }
        UUID target = ServerLink.getUUIDFromName(args[0], false);
        if (target == null) {
            player.sendMessage(chat.color(chat.getServer() + "That player doesn't exist or has never joined AlphaCraft."));
            return;
        }
        if (target == player.getUniqueId()) {
            player.sendMessage(chat.color(chat.getServer() + "You can only view other AlphaCrafters."));
            return;
        }
        AlphaPlayer aPlayer = PlayerManager.get(player);
        String targetName = ServerLink.getNameFromAlphaPlayer(aPlayer, target);
        Jedis jedis = JedisPool.getConn();
        boolean friends = Friends.getFriendsObject(aPlayer).contains(target);

        GuiPage page = new GuiPage("&e&l&n" + targetName);
        GuiRow row = new GuiRow(null, page, 1);

        ItemStack slot1;
        if (friends) {
            String online = "&cOffline";
            if (jedis.hget(target.toString(), "online").equals("true")) online = "&aOnline";
            String server = "&aLast Seen On: &9" + jedis.hget(target.toString(), "server");
            List<String> perms = Permissions.getOfflinePermissions(target);
            int rankInt = 1;
            for (String s : perms) {
                if (!s.startsWith("rank.")) continue;
                if (Integer.parseInt(s.substring(5, s.length())) > rankInt)
                    rankInt = Integer.parseInt(s.substring(5, s.length()));
            }
            String rank;
            if (rankInt == 1) rank = "&c&lRANK: &7none";
            else rank = "&c&lRANK: " + Permissions.getRankName(rankInt);
            int levelInt = Integer.parseInt(jedis.hget(target.toString(), "exp"));
            String level = "&c&lLEVEL: " + Level.getLevelColor(Level.calculateLevelFromExp(levelInt)) + levelInt;
            String kitpvpPoints = "&c&lKitPvP Tokens: &e" + Points.getOfflinePointsString(target.toString(), "kitpvp");
            String skyblockPoints = "&c&lSky Gems: &e" + Points.getOfflinePointsString(target.toString(), "skyblock");
            String coins = "&c&lCOINS: &e" + Coins.getOfflineCoinsString(target.toString());
            if (online.equals("&aOnline")) {
                slot1 = Item.addGlow(Item.easyCreate("nametag", "&e&l" + targetName + "'s Info", "", online, server, rank,
                        level, kitpvpPoints, skyblockPoints, coins, "", "&aClick to teleport to &6" + targetName + "'s &a server."));
                row.setClick(1, () -> {
                    Jedis j1 = JedisPool.getConn();
                    String online1 = j1.hget(target.toString(), "online");
                    String server1 = j1.hget(target.toString(), "server");
                    JedisPool.close(j1);
                    if (online1.equals("false")) {
                        player.closeInventory();
                        player.sendMessage("");
                        player.sendMessage(chat.color(chat.getServer() + "You cannot teleport to your friend's" +
                                " server because they are offline."));
                        return;
                    }
                    player.sendMessage("");
                    player.performCommand("server " + server1);
                    player.closeInventory();
                });
            } else
                slot1 = Item.easyCreate("nametag", "&e&l" + targetName + "'s Info", "", online, server, rank,
                        level, kitpvpPoints, coins);
        } else
            slot1 = Item.easyCreate("nametag", "&e&l" + targetName + "'s Info", "", "&7You can't see this " +
                    "player's info because", "&7you are not friends with each other.");
        ItemStack slot3;
        if (friends) {
            slot3 = Item.easyCreate("jackolantern", "&dYou are friends with &6" + targetName);
        } else if (Friends.getSentRequestsObject(aPlayer).containsKey(target)) {
            slot3 = Item.easyCreate("pumpkin", "&eYou sent a friend request to &6" + targetName);
        } else if (Friends.getReceivedRequestsObject(aPlayer).containsKey(target)) {
            slot3 = Item.easyCreate("pumpkin", "&9Click to accept &6" + targetName + "'s &9friend request");
            row.setClick(3, () -> {
                player.sendMessage("");
                player.performCommand("friends accept " + targetName);
                player.closeInventory();
            });
        } else {
            slot3 = Item.easyCreate("pumpkin", "&aClick to send &6" + targetName + " &aa friend request");
            row.setClick(3, () -> {
                player.sendMessage("");
                player.performCommand("friends add " + targetName);
                player.closeInventory();
            });
        }

        ItemStack slot5;
        boolean targetInParty = true;
        String targetParty = jedis.hget(target.toString(), "party");
        JedisPool.close(jedis);
        if (targetParty == null || targetParty.isEmpty()) targetInParty = false;
        if (PartyManager.getPartyObject(aPlayer).isEmpty()) {
            if (targetInParty) {
                slot5 = Item.easyCreate("cake", "&6" + targetName + "&e is in another party");
            } else {
                slot5 = Item.easyCreate("cake", "&aClick to invite &6" + targetName + "&a to your party.");
                row.setClick(5, () -> {
                    player.sendMessage("");
                    player.performCommand("party " + targetName);
                    player.closeInventory();
                });
            }
        } else if (PartyManager.getPartyObject(aPlayer).get(0).equals(player.getUniqueId())) {
            if (targetInParty) {
                if (targetParty.equals(player.getUniqueId().toString()))
                    slot5 = Item.easyCreate("cake", "&6" + targetName + "&e is in your party");
                else
                    slot5 = Item.easyCreate("cake", "&6" + targetName + "&e is in another party");
            } else {
                slot5 = Item.easyCreate("cake", "&aClick to invite &6" + targetName + "&a to your party.");
                row.setClick(5, () -> {
                    player.sendMessage("");
                    player.performCommand("party " + targetName);
                    player.closeInventory();
                });
            }
        } else {
            if (targetInParty) {
                if (targetParty.equals(PartyManager.getPartyObject(aPlayer).get(0).toString()))
                    slot5 = Item.easyCreate("cake", "&6" + targetName + "&e is in your party");
                else
                    slot5 = Item.easyCreate("cake", "&6" + targetName + "&e is in another party");
            } else
                slot5 = Item.easyCreate("cake", "&6" + targetName + "&e is in another party");
        }

        ItemStack slot7;
        boolean targetReferredPlayer = true;
        String referrer = aPlayer.getStringSetting("referrer");
        if (referrer == null || referrer.isEmpty() || !referrer.equals(target.toString())) targetReferredPlayer = false;
        String line2 = "&aClick to send &6" + targetName + "&a a referral request.";
        row.setClick(7, () -> {
            player.sendMessage("");
            player.performCommand("refer send " + targetName);
            player.closeInventory();
        });
        String line3 = "";
        if (targetReferredPlayer) line3 = "&6" + targetName + "&d referred you.";
        if (Referrals.getSentReferrals(aPlayer).contains(target)) {
            row.setClick(7, () -> {
            });
            line2 = "&9You sent &6" + targetName + "&9 a referral request.";
        }
        if (Referrals.getAcceptedReferrals(aPlayer).contains(target)) {
            row.setClick(7, () -> {
            });
            line2 = "&dYou referred &6" + targetName + ".";
        }
        if (Referrals.getReceivedReferrals(aPlayer).contains(target)) {
            row.setClick(7, () -> {
                player.sendMessage("");
                player.performCommand("refer accept " + targetName);
                player.closeInventory();
            });
            line2 = "&aClick to accept &6" + targetName + "'s &areferral request.";
        }
        if (line3.isEmpty()) slot7 = Item.easyCreate("vines", "&e&lRefer Relation", "", line2);
        else slot7 = Item.easyCreate("vines", "&e&lRefer Relation", "", line2, line3);

        row.setItem(1, slot1);
        row.setItem(3, slot3);
        row.setItem(5, slot5);
        row.setItem(7, slot7);
        row.setItem(9, Item.easyCreate("map", "&e&l" + targetName + "'s Game Stats", "", "&7Coming Soon..."));
        page.setRows(row);
        cooldown.put(player, player.getName());
        new BukkitRunnable() {
            @Override
            public void run() {
                cooldown.remove(player);
            }
        }.runTaskLater(ServerLink.plugin, 20 * 15);
        new BukkitRunnable() {
            @Override
            public void run() {
                new InventoryGui(player, player.getUniqueId().toString() + "/player" + target.toString(), page).openPage(1);
            }
        }.runTask(ServerLink.plugin);
    }
}
package serverlink.command.server;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.command.CommandManager;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.network.JedisOne;
import serverlink.network.NetworkCache;
import serverlink.player.party.PartyManager;
import serverlink.server.Messages;
import serverlink.server.PluginMessaging;

import java.util.List;
import java.util.UUID;

public class ServerCommand {
    @CommandManager.CommandHandler(name = "server", aliases = {"serv"}, async = true, sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(Player player, String[] args) {
        if (args.length != 1) {
            player.sendMessage(chat.color(chat.getServer() + "&7You are currently on server &a" +
                    ServerLink.getServerName() + "."));
            return;
        }
        String serverName = args[0].toLowerCase();
        if (serverName.equalsIgnoreCase(ServerLink.getServerName())) {
            player.sendMessage(chat.color(chat.getServer() + "You are already on that server."));
            return;
        }
        String availability = NetworkCache.getAvailability(serverName);
        if (availability == null) {
            player.sendMessage(chat.color(chat.getServer() + "That server doesn't exist."));
            return;
        } else if (availability.equals("closed")) {
            player.sendMessage(chat.color(chat.getServer() + "That server is full."));
            return;
        } else if (availability.equals("offline")) {
            player.sendMessage(chat.color(chat.getServer() + "That server is offline."));
            return;
        }
        AlphaPlayer aPlayer = PlayerManager.get(player);
        List<UUID> party = PartyManager.getPartyObject(aPlayer);
        // only the leader can join servers
        if (!party.isEmpty() && !party.get(0).toString().equals(player.getUniqueId().toString())) {
            player.sendMessage(chat.color(chat.getServer() + Messages.party_error_notLeader));
            return;
        }
        player.sendMessage(chat.color(chat.getServer() + "&2Connecting..."));
        PluginMessaging.TP(player, serverName);
        Jedis j1 = JedisOne.getTwo();
        for (UUID uuid : party) {
            Player p = Bukkit.getPlayer(uuid);
            if (p == null || !p.isOnline() || p == player) continue;
            if (j1.hget("availability", serverName).equals("open")) {
                p.sendMessage(chat.color(chat.getServer() + Messages.party_notify_tpWithParty));
                PluginMessaging.TP(p, serverName);
            } else {
                p.sendMessage(chat.color(chat.getServer() +
                        "You couldn't be teleported with your party because " +
                        "the server your leader went to didn't have enough open slots."));
            }
        }
        JedisOne.closeTwo();
    }
}

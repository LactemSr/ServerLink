package serverlink.command.server;

import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.command.CommandManager;
import org.bukkit.entity.Player;

public class WhereamiCommand {
    @CommandManager.CommandHandler(name = "whereami", async = false, sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(Player player, String[] args) {
        player.sendMessage(chat.color(chat.getServer() + "&7You are currently on server &a" +
                ServerLink.getServerName() + "."));
    }
}

package serverlink.command.server;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.command.CommandManager;
import serverlink.network.JedisOne;
import serverlink.network.NetworkCache;
import serverlink.network.ServerSelection;
import serverlink.player.party.PartyManager;
import serverlink.server.Messages;
import serverlink.server.PluginMessaging;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

public class SkybattleCommand {
    @CommandManager.CommandHandler(name = "skybattle", aliases = {"sb", "sky-battle"}, async = true, sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(Player player, String[] args) {
        if (ServerLink.getServerName().contains("skybattle") && (args.length == 0 || !args[0].equals("t"))) {
            player.sendMessage(chat.color(chat.getServer() + "You are already playing that gametype."));
            return;
        }
        AlphaPlayer aPlayer = PlayerManager.get(player);
        ArrayList<String> matchedServers = new ArrayList<>();
        HashMap<String, Integer> priorities = new HashMap<>();
        for (String serv : NetworkCache.getAllAvailabilities().keySet()) {
            if (!serv.startsWith("skybattlelobby-")) continue;
            if (!NetworkCache.getAvailability(serv).equals("open")) continue;
            matchedServers.add(serv);
        }
        if (matchedServers.size() == 0) {
            player.sendMessage(chat.color(chat.getServer() + "Couldn't find any open servers!"));
            return;
        }
        List<UUID> party = PartyManager.getPartyObject(aPlayer);
        // if player is not in a party, send him to play to the best open server
        if (party.isEmpty()) {
            matchedServers = ServerSelection.sortyByCount(matchedServers);
            player.sendMessage(chat.color(chat.getServer() + "&2Connecting..."));
            PluginMessaging.TP(player, matchedServers.get(0));
            return;
        }
        // only the leader can join servers
        if (!party.get(0).toString().equals(player.getUniqueId().toString())) {
            player.sendMessage(chat.color(chat.getServer() + Messages.party_error_notLeader));
            return;
        }
        for (String serv : matchedServers) {
            // lower priorities are more preferable
            int priority = 10;
            if (NetworkCache.getStatus(serv).toLowerCase().contains("lobby")) {
                priority -= 6;
            } else if (NetworkCache.getStatus(serv).equals("endCountdown")) {
                priority -= 5;
            }

            if ((NetworkCache.getMaxPlayers(serv) - NetworkCache.getCount(serv)) >= party.size()) {
                priority -= 4;
            } else if ((NetworkCache.getMaxPlayers(serv) - NetworkCache.getCount(serv)) >=
                    (party.size() - 1)) {
                priority -= 3;
            } else if ((NetworkCache.getMaxPlayers(serv) - NetworkCache.getCount(serv)) >=
                    (party.size() - 2)) {
                priority -= 2;
            } else if ((NetworkCache.getMaxPlayers(serv) - NetworkCache.getCount(serv)) >=
                    (party.size() - 5)) {
                priority -= 1;
            }
            priorities.put(serv, priority);
        }
        String chosenServer = "";
        for (String serv : priorities.keySet()) {
            if (chosenServer.equals("")) {
                chosenServer = serv;
                continue;
            }
            if (priorities.get(serv) < priorities.get(chosenServer)) {
                chosenServer = serv;
            }
        }
        player.sendMessage(chat.color(chat.getServer() + "&2Connecting..."));
        PluginMessaging.TP(player, chosenServer);
        Jedis j1 = JedisOne.getTwo();
        for (UUID uuid : party) {
            Player p = Bukkit.getPlayer(uuid);
            if (p == null || !p.isOnline() || p == player) continue;
            if (j1.hget("availability", chosenServer).equals("open")) {
                p.sendMessage(chat.color(chat.getServer() + Messages.party_notify_tpWithParty));
                PluginMessaging.TP(p, chosenServer);
            } else {
                p.sendMessage(chat.color(chat.getServer() +
                        "You couldn't be teleported with your party because there were no servers with enough" +
                        "open slots."));
            }
        }
        JedisOne.closeTwo();
    }
}

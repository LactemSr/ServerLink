package serverlink.command.server;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import redis.clients.jedis.Jedis;
import serverlink.ServerLink;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.chat.chat;
import serverlink.command.CommandManager;
import serverlink.network.JedisOne;
import serverlink.network.NetworkCache;
import serverlink.player.party.PartyManager;
import serverlink.server.Messages;
import serverlink.server.PluginMessaging;

import java.util.*;
import java.util.concurrent.ThreadLocalRandom;

public class HubCommand {
    @CommandManager.CommandHandler(name = "hub", aliases = {"lobby"}, async = true, sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(Player player, String[] args) {
        if (ServerLink.getServerName().startsWith("hub-")) {
            player.sendMessage(chat.color(chat.getServer() + "You are already on that server."));
            return;
        }
        if (ServerLink.getServerName().startsWith("skybattle-")) {
            CommandManager.resetSpamCooldown(player);
            player.performCommand("skybattle t");
            return;
        }
        AlphaPlayer aPlayer = PlayerManager.get(player);
        Map<String, String> serverAvailabilities = NetworkCache.getAllAvailabilities();
        ArrayList<String> hubs = new ArrayList<>();
        List<UUID> party = PartyManager.getPartyObject(aPlayer);
        for (String name : serverAvailabilities.keySet()) {
            if (!name.startsWith("hub-")) {
                continue;
            }
            String availability = serverAvailabilities.get(name);
            if (availability.equals("open")) {
                hubs.add(name);
            }
        }
        if (hubs.size() == 0) {
            player.sendMessage(chat.color(chat.getServer() + "All hubs are currently full."));
            return;
        }
        HashMap<String, Integer> priorities = new HashMap<>();
        for (String serv : hubs) {
            // lower priorities are more preferable
            int priority = 4;
            if ((NetworkCache.getMaxPlayers(serv) - NetworkCache.getCount(serv)) >= party.size() + 20) {
                priority -= 4;
            } else if ((NetworkCache.getMaxPlayers(serv) - NetworkCache.getCount(serv)) >=
                    (party.size() + 10)) {
                priority -= 3;
            } else if ((NetworkCache.getMaxPlayers(serv) - NetworkCache.getCount(serv)) >=
                    (party.size() + 5)) {
                priority -= 2;
            } else if ((NetworkCache.getMaxPlayers(serv) - NetworkCache.getCount(serv)) >= party.size()) {
                priority -= 1;
            }
            priorities.put(serv, priority);
        }
        ArrayList<String> clone = (ArrayList<String>) hubs.clone();
        hubs.clear();
        int lowestPriority = 4;
        for (String serv : priorities.keySet()) {
            if (clone.size() == 0) {
                hubs.add(serv);
                lowestPriority = priorities.get(hubs.get(0));
                continue;
            }
            int priority = priorities.get(serv);
            if (priority < lowestPriority) {
                for (String s : clone) {
                    if (priorities.get(s) >= (lowestPriority)) hubs.remove(s);
                }
                lowestPriority--;
                hubs.add(serv);
            } else if (priorities.get(serv) == lowestPriority) {
                hubs.add(serv);
            }
        }
        // randomly assign player to a hub
        String chosenServer = hubs.get(ThreadLocalRandom.current().nextInt(hubs.size()));
        player.sendMessage(chat.color(chat.getServer() + "&2Connecting..."));
        PluginMessaging.TP(player, chosenServer);
        if (party.isEmpty() || !party.get(0).toString().equals(player.getUniqueId().toString())) return;
        Jedis j1 = JedisOne.getTwo();
        for (UUID uuid : party) {
            Player p = Bukkit.getPlayer(uuid);
            if (p == null || !p.isOnline() || p == player) continue;
            if (j1.hget("availability", chosenServer).equals("open")) {
                p.sendMessage(chat.color(chat.getServer() + Messages.party_notify_tpWithParty));
                PluginMessaging.TP(p, chosenServer);
            } else {
                p.sendMessage(chat.color(chat.getServer() +
                        "You couldn't be teleported with your party because there were no servers " +
                        "with enough open slots."));
            }
        }
        JedisOne.closeTwo();
    }
}

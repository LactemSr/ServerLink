package serverlink.command;

import org.bukkit.entity.Player;
import serverlink.chat.chat;
import serverlink.util.HelpUtils;

public class HelpCommand {
    @CommandManager.CommandHandler(name = "help", aliases = {"?", "helpme", "helpop"}, async = false, sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(Player player, String[] args) {

        // Send the first help page if the player just typed "/help"
        if (args.length == 0) {
            HelpUtils.getInstance().help(player);
            return;
        }

        // Check if the player just did /help <page> instead of a specific book
        try {
            int page = Integer.valueOf(args[0]);
            HelpUtils.getInstance().help(player, "help", page);
            return;
        } catch (NumberFormatException e) {
        } // Ignore

        String book = args[0];
        int page = 1;

        // Go to a specific page number if the player enters one
        if (args.length > 1) {
            try {
                page = Integer.valueOf(args[1]);
            } catch (NumberFormatException e) {
                player.sendMessage(chat.color(chat.getServer() + "&cInvalid number! &aUse &6/help <topic> <page>"));
                return;
            }
        }

        HelpUtils.getInstance().help(player, book, page);
    }
}

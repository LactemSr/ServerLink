package serverlink.command.punishment;

import serverlink.chat.chat;
import serverlink.command.CommandManager;
import serverlink.alphaplayer.PlayerManager;
import serverlink.network.JedisPool;
import serverlink.util.Slack;
import serverlink.util.TimeUtils;
import serverlink.util.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import redis.clients.jedis.Jedis;

import java.util.List;

public class UnmuteCommand {
    @CommandManager.CommandHandler(name = "unmute", async = true, sendUsageMessageOnUnrecognizedSubCommand = false)
    public void base(CommandSender sender, String[] args) {
        if (sender instanceof Player) {
            if (PlayerManager.get((Player) sender).getRank() < 7) {
                sender.sendMessage(chat.gcu());
                return;
            }
            if (PlayerManager.get((Player) sender).getRank() < 9) {
                sender.sendMessage("Only admins may unmute players.");
                return;
            }
        }
        if (args.length != 1) {
            sender.sendMessage("Usage: /unmute <player>");
            return;
        }
        String uuid;
        String name;
        Player player = Bukkit.getPlayerExact(args[0]);
        if (player != null && player.isOnline()) {
            uuid = player.getUniqueId().toString();
            name = player.getName();
        } else {
            Jedis jedis = JedisPool.getConn();
            uuid = jedis.get(args[0].toLowerCase());
            if (uuid != null && uuid.length() > 8) {
                name = jedis.hget(uuid, "username");
                JedisPool.close(jedis);
            } else {
                JedisPool.close(jedis);
                try {
                    List<String> uuidAndName = UUIDFetcher.getOfflineUUIDAndName(args[0].toLowerCase());
                    if (uuidAndName != null) {
                        uuid = uuidAndName.get(0);
                        name = uuidAndName.get(1);
                    } else {
                        sender.sendMessage("That player does not exist!");
                        return;
                    }
                } catch (Exception e) {
                    sender.sendMessage("That player does not exist!");
                    return;
                }
            }
        }
        Jedis jedis = JedisPool.getConn();
        String mute = jedis.hget(uuid, "mute");
        if (mute == null || TimeUtils.getCurrent() > Integer.parseInt(mute.split(":")[1])) {
            JedisPool.close(jedis);
            sender.sendMessage("That player is not muted.");
            return;
        }
        jedis.hdel(uuid, "mute");
        JedisPool.close(jedis);
        if (player != null && player.isOnline()) PlayerManager.get(player.getUniqueId()).setMuteExpiration(0);
        sender.sendMessage(name + " has been unmuted.");
        Slack.sendPost("#punishments", name + " has been *unmuted* by " + sender.getName() + ".");
    }
}

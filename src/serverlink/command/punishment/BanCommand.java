package serverlink.command.punishment;

import serverlink.ServerLink;
import serverlink.chat.chat;
import serverlink.command.CommandManager;
import serverlink.alphaplayer.AlphaPlayer;
import serverlink.alphaplayer.PlayerManager;
import serverlink.network.JedisPool;
import serverlink.server.Messages;
import serverlink.util.Slack;
import serverlink.util.TimeUtils;
import serverlink.util.UUIDFetcher;
import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import redis.clients.jedis.Jedis;

import java.util.List;

public class BanCommand {
    @CommandManager.CommandHandler(name = "ban", aliases = {"banhammer", "tempban"}, async = true, sendUsageMessageOnUnrecognizedSubCommand = false, permission = "rank.7")
    public void base(CommandSender sender, String[] args) {
        if (args.length < 3) {
            sender.sendMessage(
                    "Usage: /ban <player> <duration> <s|m|h|d|w> <reason>");
            return;
        }
        if ((args.length < 4) || ((!args[2].equalsIgnoreCase("s")) && (!args[2].equalsIgnoreCase("m"))
                && (!args[2].equalsIgnoreCase("h")) && (!args[2].equalsIgnoreCase("d"))
                && (!args[2].equalsIgnoreCase("w")))) {
            sender.sendMessage(
                    "Usage: /ban <player> <duration> <s|m|h|d|w> <reason>");
            return;
        }
        int duration;
        try {
            duration = Integer.parseInt(args[1]);
        } catch (NumberFormatException e) {
            sender.sendMessage("Duration must be a number.");
            sender.sendMessage(
                    "Usage: /ban <player> <duration> <s|m|h|d|w> <reason>");
            return;
        }
        String unit = args[2];
        int formattedDuration = duration;
        switch (unit) {
            case "w":
                unit = "weeks";
                if (duration == 1) unit = "week";
                formattedDuration = duration * 604800;
                break;
            case "d":
                unit = "days";
                if (duration == 1) unit = "day";
                formattedDuration = duration * 86400;
                break;
            case "h":
                unit = "hours";
                if (duration == 1) unit = "hour";
                formattedDuration = duration * 3600;
                break;
            case "m":
                unit = "minutes";
                if (duration == 1) unit = "minute";
                formattedDuration = duration * 60;
                break;
            default:
                unit = "seconds";
                break;
        }
        if (sender instanceof Player) {
            AlphaPlayer aPlayer = PlayerManager.get((Player) sender);
            if (aPlayer.getRank() < 8 && duration > 86400) {
                sender.sendMessage("Only moderators can ban for longer than a day.");
                return;
            } else if (aPlayer.getRank() < 9 && duration > 604800) {
                sender.sendMessage("Only admins can ban for longer than a week.");
                return;
            } else if (duration > 604800 * 2) {
                sender.sendMessage("The maximum sentence is 2 weeks.");
                return;
            }
        }
        String reason = args[3];
        for (int i = 4; i < args.length; i++) reason = reason + " " + args[i];
        String uuid;
        String name;
        Player player = Bukkit.getPlayerExact(args[0]);
        if (player != null && player.isOnline()) {
            uuid = player.getUniqueId().toString();
            name = player.getName();
        } else {
            Jedis jedis = JedisPool.getConn();
            uuid = jedis.get(args[0].toLowerCase());
            if (uuid != null && uuid.length() > 8) {
                name = jedis.hget(uuid, "username");
                JedisPool.close(jedis);
            } else {
                JedisPool.close(jedis);
                try {
                    List<String> uuidAndName = UUIDFetcher.getOfflineUUIDAndName(args[0].toLowerCase());
                    if (uuidAndName != null) {
                        uuid = uuidAndName.get(0);
                        name = uuidAndName.get(1);
                    } else {
                        sender.sendMessage("That player does not exist!");
                        return;
                    }
                } catch (Exception e) {
                    sender.sendMessage("That player does not exist!");
                    return;
                }
            }
        }
        Jedis jedis = JedisPool.getConn();
        jedis.hset(uuid, "ban", reason + ":" + ((Integer) (TimeUtils.getCurrent() + formattedDuration)).toString());
        JedisPool.close(jedis);
        if (player != null) {
            String finalReason = reason;
            String finalUnit = unit;
            new BukkitRunnable() {
                @Override
                public void run() {
                    player.kickPlayer(chat.color(Messages.ban_temporary.replace("%duration%", args[1] + " "
                            + finalUnit).replace("%reason%", finalReason)));
                }
            }.runTask(ServerLink.plugin);
        }

        sender.sendMessage(name + " has been banned for " + args[1] + " " + unit
                + ". You can type /status <player> to view more info.");
        Slack.sendPost("#punishments", name + " has been temporarily *banned* for " + args[1] + " " + unit + " by "
                + sender.getName() + " for " + reason + ".");
    }
}
